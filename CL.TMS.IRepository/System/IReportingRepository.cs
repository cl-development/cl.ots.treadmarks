﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Metric;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;

namespace CL.TMS.IRepository.System
{
    public interface IReportingRepository
    {
        Report GetReport(int Id);
        IEnumerable<ReportCategory> GetAllReportCategories(int Id = 0);
        IEnumerable<Report> GetReportsByCategoryId(int ReportingCategoryId);
        IEnumerable<StewardRptRevenueSupplyNewTireVM> GetStewardRevenueSupplyNewTireTypes(ReportDetailsViewModel detail);
        IEnumerable<StewardRptRevenueSupplyNewTireCreditVM> GetStewardRevenueSupplyNewTireCreditTypes(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate);
        IEnumerable<StewardRptRevenueSupplyNewTireCreditVMsp> GetStewardRevenueSupplyNewTireCreditType_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate);
        IEnumerable<StewardRptRevenueSupplyOldTireCreditVMsp> GetStewardRevenueSupplyOldTireCreditType_SP(ReportDetailsViewModel detail, DateTime TSFNegAdjSwitchDate);
        IEnumerable<RegistrantWeeklyReport> GetRegistrantWeeklyReport();
        IEnumerable<SpRptTsfExtractInBatchOnlyReportGp> GetTsfExtrtactInBatchOnlyReportGp();
        IEnumerable<SpRptProcessorTIPIReport> GetProcessorTIPIReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptHaulerCollectorComparisonReport> GetHaulerCollectorComparisonReport(DateTime? startDate, DateTime? endDate, string registrationNumber);
        IEnumerable<StewardRptRevenueSupplyOldTireVM> GetStewardRevenueSupplyOldTireTypes(ReportDetailsViewModel detail);
        IEnumerable<WebListingCollectorReportPostalVM> GetWebListingCollectorReportPostal(string postalCode);
        IEnumerable<WebListingHaulerReport> GetWebListingHaulerReport();

        IEnumerable<HaulerVolumeReport> GetHaulerVolumeReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptDetailHaulerVolumeReportBasedOnScaleWeight> GetDetailHaulerVolumeReportBasedOnScaleWeight(DateTime? startDate, DateTime? endDate);
        IEnumerable<SpRptProcessorVolumeReport> GetProcessorVolumeReport(DateTime? startDate, DateTime? endDate);
        IEnumerable<RpmData> GetRpmData(ReportDetailsViewModel detail);
        IEnumerable<RpmCumulative> GetRpmCumulative(ReportDetailsViewModel detail);
        IEnumerable<ProcessorDispositionOfResidual> GetProcessorDispositionOfResidual(List<TypeDefinition> dispositionReasons, List<TypeDefinition> materialTypeList);
        IEnumerable<StewardNonFilers> GetStewardNonFilers(DateTime? startDate, DateTime? endDate);
        IEnumerable<TSFRptOnlinePaymentsOutstandingVM> GetTSFOnlinePaymentsOutstanding(ReportDetailsViewModel model, DateTime TSFNegAdjSwitchDate);
        IEnumerable<CollectorCumulativeReportVM> GetCollectorCumulativeReport(ReportDetailsViewModel model);
        IEnumerable<WebListingRPMReport> GetWebListingRPMReportPostal(string postalCode);
        IEnumerable<WebListingProcessorReport> GetWebListingProcessorReportPostal(string postalCode);
        IEnumerable<WebListingStewardReport> GetWebListingStewardReportPostal(string postalCode);

        IEnumerable<HaulerTireMovementReportVM> GetHaulerTireMovementReportVM(ReportDetailsViewModel model);

        IEnumerable<HaulerTireMovementReport> GetHaulerTireMovementReport_sp(ReportDetailsViewModel model);

        IEnumerable<CollectorTireOriginReport> GetCollectorTireOriginReport(ReportDetailsViewModel model);

        IEnumerable<HaulerTireCollectionReport> GetHaulerTireCollectionReport(ReportDetailsViewModel model);
        IEnumerable<ReuseCollectorInventoryCount> GetReuseCollectorInventoryCount(ReportDetailsViewModel model);
        #region OTSTM2-215
        List<CollectionGenerationOfTiresFSAProcessing> GetCollectionGenerationOfTiresBasedOnFSAReport(DateTime? fromDate, DateTime? toDate, List<string> allFSAList);
        PaginationDTO<CollectionGenerationOfTiresBasedOnFSAReportDetailsVM, int> GetReportDetailHandler(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, List<string> postalList, string fromDate, string toDate);
        #endregion
        List<StaffUserNameAndEmailViewModel> GetUserNameBySearchText(string searchText);

        #region Metric 
        MetricVM LoadMetricDetailsByID(MetricParams paramModel);
    
        #endregion
    }
}