﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using System.Collections.Generic;

namespace CL.TMS.IRepository.System
{
    public interface IProductCompositionRepository
    {
        PaginationDTO<RecoverableMaterialsVM, int> LoadRecoverableMaterials(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        PaginationDTO<MaterialCompositionsVM, int> LoadMaterialCompositions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        RecoverableMaterial GetRecoverableMaterialByID(int id);   
        bool UpdateRecoverableMaterial(RecoverableMaterialsVM vm, long userID,string userEmail, string userFullName, int activityType);
        bool AddRecoverableMaterial(RecoverableMaterialsVM vm, long userID, string userEmail, string userFullName, int activityType);
        bool CheckMaterialNameUnique(string name);
        bool UpdateMaterialComposition(MaterialCompositionsVM vm, long userID, string userEmail, string userFullName, int activityType);

        //Estimated Weights
        PaginationDTO<RecoveryEstimatedWeightListViewModel, int> LoadRecoveryEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<SupplyEstimatedWeightViewModel, int> LoadSupplyEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        bool IsFutureWeightExists(int category);
        void AddRecoveryEstimatedWeights(WeightTransaction weightTransaction, WeightParamsDTO param);
        bool UpdateRecoveryEstimatedWeights(WeightDetailsVM weightDetailsVM, WeightParamsDTO param);
        bool RemoveWeightTransaction(int weightTransactionID, string categoryName);
        IEnumerable<ItemWeight> GetItemWeights(int transactionID);
        WeightTransactionNote AddTransactionNote(WeightTransactionNote transactionNote);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);
        List<InternalNoteViewModel> LoadWeightTransactionNoteByID(int weightTransactionID);

        SupplyEstimatedWeightViewModel LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category);
        void updateSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM);

    }
}