﻿using System.Collections.Generic;
using System.Security.Claims;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.IRepository.System
{
    public interface IAuthenticationRepository
    {
        AuthenticationUser FindUserByUserName(string userName);

        void SetLoginDateTimeAndResetLockHandler(long useId);

        void LockUser(long userId);

        bool InvalidLoginAttemptHandler(long userId, int maxLoginAttempts);

        IEnumerable<string> GetUserRoles(long userId);

        IEnumerable<Claim> GetUserClaims(long userId);
        string GetAfterLoginRedirectUrl(long userId);

    }
}
