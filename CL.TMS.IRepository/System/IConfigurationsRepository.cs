﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.IRepository.System
{
    public interface IConfigurationsRepository
    {
        #region //loading rates

        List<Rate> LoadRateTransactionByID(int rateTransactionID);
        PayeeList LoadTIPayeeListByID(int tiPayeeID);
        IEnumerable<Rate> GetCurrentRates(int[] iclaimTypes);

        IEnumerable<Rate> GetCurrentCollectorRates(int iclaimType);

        //IEnumerable<ItemWeight> GetItemWeights(int transactionID);

        Period GetLatestPeriod(int iPeriodType);

        PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<RateListViewModel, int> LoadTIPayeeList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);

        List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID);
        List<InternalNoteViewModel> LoadTIPayeeNoteByID(int tiPayeeListID);
        RateTransaction FindLatestFutureRateTransaction();

        bool IsFutureRateExists(int category);
        bool IsFutureRateExists(int category, bool isSpecific);
        bool IsFutureRateExists(int category, int paymentType);
        IEnumerable<ItemModel<string>> LoadAvailabledProcessorIDs();
        IEnumerable<ItemModel<string>> LoadAssignedProcessorIDs(int rateTransactionID);
        IEnumerable<ItemModel<string>> LoadAvailabledHaulerIDs(int tiPayeeListID);
        IEnumerable<ItemModel<string>> LoadAssignedHaulerIDs(int tiPayeeListID);

        #endregion //loading rates

        #region //add rates & PayeeList

        void AddRates(RateTransaction rateTransaction, RateParamsDTO param);
        void AddTIPayeeList(PayeeList tiPayeeList);
        #endregion //add rates

        #region //udpate rates

        bool UpdateRates(RateDetailsVM reteDetailsVM, RateParamsDTO param);

        #endregion //udpate rates

        #region //remove rates

        bool RemoveRateTransaction(int rateTransactionID, string categoryName);

        #endregion //remove rates

        #region //notes

        RateTransactionNote AddTransactionNote(RateTransactionNote transactionNote);

        AdminSectionNotes AddAppSettingNotes(AdminSectionNotes note);
        void AddPayeeNote(PayeeListNote transactionNote);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);

        List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText);
        List<InternalNoteViewModel> ExportPayeeNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);

        Period GetPeriodByDate(DateTime effectiveStartDate, int iPeriodType);

        List<Period> LoadFuturePeriodByType(DateTime effectiveStartDate, int iPeriodType);

        bool IsFutureCollectorRateExists(int category);

        bool IsCollectorRateTransactionExists(int category);

        bool IsFutureCollectorRateTransactionExists(int category);

        List<Rate> LoadRatesForPeriods(IEnumerable<int> periodIDs);

        bool UnBondCollectorRatesToRateTransaction(int rateTransactionID, int category);

        List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType);

        #endregion //notes

        AppSettingsVM LoadAppSettings();

        #region //Account Thresholds
        AccountThresholdsVM LoadAccountThresholds();
        bool UpdateAccountThresholds(AccountThresholdsVM vm, long userID);
        bool updateAppSettings(AppSettingsVM appSettingsVM, long userId);
        #endregion

        #region Transaction Thresholds
        TransactionThresholdsVM LoadTransactionThresholds();
        void UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM);
        #endregion

        #region TCR Service Threshold
        PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO);
        TCRServiceThresholdListViewModel LoadTCRServiceThresholdList();
        void AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool RemoveTCRServiceThresohold(int id, long userId);
        IEnumerable<TCRListViewModel> ExportTCRServiceThresholds();
        #endregion
    }
}