﻿using CL.TMS.DataContracts.ViewModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.IRepository.System
{
    public interface IApplicationBriefRepository
    {
        PaginationDTO<ApplicationViewModel,int> LoadAllApplicationBrief(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        List<ApplicationViewModel> LoadApplications(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText);

        ApplicationViewModel GetFirstSubmitApplication();
    }
}
