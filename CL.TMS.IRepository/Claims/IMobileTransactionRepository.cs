﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Mobile;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;

namespace CL.TMS.IRepository.Claims
{
    public interface IMobileTransactionRepository
    {
        Transaction GetTransaction(Guid transactionId);
        Location GetLocationByAuthorizationId(Guid authorizationId);
        GpsLog GetGps(Guid gpsLogId);
        Company GetCompany(Guid transactionId);
        MobileRegistrant GetRegistrant(decimal registrationNumber);        
    }
}
