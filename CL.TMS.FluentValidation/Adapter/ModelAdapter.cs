﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.FluentValidation.ViewModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.FluentValidation.Adapter
{
    public class ModelAdapter
    {
        public static Invitation ToInvitation(CreateInvitationViewModel createInvitationViewModel, string userIp, DateTime expireDate, string siteUrl)
        {
            return new Invitation
            {
                FirstName = createInvitationViewModel.FirstName,
                LastName = createInvitationViewModel.LastName,
                UserName = createInvitationViewModel.EmailAddress,
                CreatedBy = TMSContext.TMSPrincipal.Identity.Name,
                UserIP = userIp,
                InviteDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                InviteGUID = Guid.NewGuid(),
                SiteUrl = siteUrl,
                EmailAddress = createInvitationViewModel.EmailAddress,
                Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Invited)
            };
        }
    }
}
