﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;


namespace CL.TMS.FluentValidation.ViewModel.System
{
    public class InvitationsViewModel
    {
        public CreateInvitationViewModel CreateInvitationViewModel { get; set; }
        public List<UserInviteModel> UserInvitations { get; set; }

        public int TotalItems { get; set; }
        public int PageSize { get; set; }
    }
}
