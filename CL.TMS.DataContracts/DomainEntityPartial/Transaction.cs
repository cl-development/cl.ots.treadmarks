﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.DomainEntities
{    
    public partial class Transaction 
    { 
        #region Calculated Properties
        [NotMapped]
        public bool IsPaper
        {
            get { return !this.MobileFormat; }
        }

        [NotMapped]
        public bool IsMobile
        {
            get { return this.MobileFormat; }
        }


        [NotMapped]
        public int ActiveAdjustmentId
        {
            get { return this.TransactionAdjustments.Where(i => i.Status == TransactionAdjustmentStatus.Pending.ToString() || i.Status == TransactionAdjustmentStatus.Accepted.ToString()).Select(i => i.ID).Single(); }
        }

        [NotMapped]
        public TransactionAdjustment AcceptedAdjustment
        {
            get { return this.TransactionAdjustments.FirstOrDefault(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()); }
        }        

        [NotMapped]
        public bool IsAdjustmentPending
        {
            get { return this.TransactionAdjustments.Any(i => i.Status == TransactionAdjustmentStatus.Pending.ToString()); }
        }

        [NotMapped]
        public bool IsAdjustmentAccepted
        {
            get { return this.TransactionAdjustments.Any(i => i.Status == TransactionAdjustmentStatus.Accepted.ToString()); }
        }

        [NotMapped]
        public bool ActiveAdjustmentExists
        {
            get { return IsAdjustmentPending || IsAdjustmentAccepted; }
        }

        [NotMapped]
        public TransactionAdjustment AdjustmentRecord { get; set; }

        // Estimated Weight
        [NotMapped]
        public Decimal OriginalEstimatedWeight
        {
            get { return (this.EstimatedOffRoadWeight ?? 0.0000M) + (this.EstimatedOnRoadWeight ?? 0.0000M); }
        }

        [NotMapped]
        public Decimal EffectiveEstimatedWeight
        {
            get { return (this.IsAdjustmentAccepted) ? ((AdjustmentRecord.EstimatedOffRoadWeight ?? 0.0000M) + (AdjustmentRecord.EstimatedOnRoadWeight ?? 0.0000M)) : OriginalEstimatedWeight; }
        }                       

        // Transaction Items
        [NotMapped]
        public List<TransactionItem> OriginalTransactionItems
        {
            get { return this.TransactionItems.Where(i => i.TransactionAdjustmentId == null).ToList(); }
        }

        [NotMapped]
        public List<TransactionItem> EffectiveTransactionItems
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.TransactionItems : OriginalTransactionItems; }
        }

        // IsGenerateTires
        [NotMapped]
        public bool EffectiveGenerateTires
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.IsGenerateTires : this.IsGenerateTires; }
        }

        // InvoiceNumber
        [NotMapped]
        public string EffectiveInvoiceNumber
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.InvoiceNumber : this.InvoiceNumber; }
        }

        // EventNumber
        [NotMapped]
        public string EffectiveEventNumber
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.EventNumber : this.EventNumber; }
        }

        // PaymentEligible
        [NotMapped]
        public int? EffectivePaymentEligible
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.PaymentEligible : this.PaymentEligible; }
        }

        // PaymentEligibleOther
        [NotMapped]
        public string EffectivePaymentEligibleOther
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.PaymentEligibleOther : this.PaymentEligibleOther; }
        }

        // MaterialType
        [NotMapped]
        public int? EffectiveMaterialType
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.MaterialType : this.MaterialType; }
        }        

        // DispositionReason
        [NotMapped]
        public int? EffectiveDispositionReason
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.DispositionReason : this.DispositionReason; }
        }

        // PTRNumber
        [NotMapped]
        public string EffectivePTRNumber
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.PTRNumber : this.PTRNumber; }
        }

        // OnRoadWeight
        [NotMapped]
        public decimal? EffectiveOnRoadWeight
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.OnRoadWeight : this.OnRoadWeight; }
        }

        // OffRoadWeight
        [NotMapped]
        public decimal? EffectiveOffRoadWeight
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.OffRoadWeight : this.OffRoadWeight; }
        }

        // TireMarketType
        [NotMapped]
        public int? EffectiveTireMarketType
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.TireMarketType : this.TireMarketType; }
        }

        // VendorInfoId
        [NotMapped]
        public int? EffectiveVendorInfoId
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.VendorInfoId : this.VendorInfoId; }
        }        

        // VendorInfo
        [NotMapped]
        public Vendor EffectiveVendorInfo
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.VendorInfo : this.VendorInfo; }
        }

        // CompanyInfo
        [NotMapped]
        public CompanyInfo EffectiveCompanyInfo
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.CompanyInfo : this.CompanyInfo; }
        }

        // Supporting Documents
        [NotMapped]
        public List<TransactionSupportingDocument> OriginalTransactionSupportingDocuments
        {
            get { return this.TransactionSupportingDocuments.Where(i => i.TransactionAdjustmentId == null).ToList(); }
        }
               
        [NotMapped]
        public List<TransactionSupportingDocument> EffectiveTransactionSupportingDocuments
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.TransactionSupportingDocuments : OriginalTransactionSupportingDocuments; }
        }

        // Supporting Document Attachments
        [NotMapped]
        public List<TransactionAttachment> OriginalTransactionAttachments
        {
            get { return this.TransactionAttachments.Where(i => i.TransactionAdjustmentId == null).ToList(); }
        }

        [NotMapped]
        public List<TransactionAttachment> EffectiveTransactionAttachments
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.TransactionAttachments : OriginalTransactionAttachments; }
        }

        // Scale Tickets
        [NotMapped]
        public List<ScaleTicket> OriginalScaleTickets
        {
            get { return this.ScaleTickets.Where(i => i.TransactionAdjustmentId == null).ToList(); }
        }

        [NotMapped]
        public List<ScaleTicket> EffectiveScaleTickets
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.ScaleTickets : OriginalScaleTickets; }
        }

        // Photos
        [NotMapped]
        public List<TransactionPhoto> OriginalTransactionPhotos
        {
            get { return this.TransactionPhotos.Where(i => i.TransactionAdjustmentId == null).ToList(); }
        }

        [NotMapped]
        public List<TransactionPhoto> EffectiveTransactionPhotos
        {
            get { return (this.IsAdjustmentAccepted) ? this.AdjustmentRecord.TransactionPhotos : OriginalTransactionPhotos; }
        }

        [NotMapped]
        public VendorReference IncomingVendorRef { get; set; }

        [NotMapped]
        public VendorReference OutgoingVendorRef { get; set; }

        #endregion 
    }
}
