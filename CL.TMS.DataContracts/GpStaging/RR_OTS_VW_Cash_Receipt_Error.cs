//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CL.TMS.DataContracts.GpStaging
{
    using System;
    using System.Collections.Generic;
    
    public partial class RR_OTS_VW_Cash_Receipt_Error
    {
        [Key, Column(Order = 0)]
        public short RMDTYPAL { get; set; }
        public string CUSTNMBR { get; set; }
        [Key, Column(Order = 1)]
        public string DOCNUMBR { get; set; }
        public System.DateTime DOCDATE { get; set; }
        public decimal ORTRXAMT { get; set; }
        public System.DateTime GLPOSTDT { get; set; }
        public string BACHNUMB { get; set; }
        public short CSHRCTYP { get; set; }
        public string CHEKBKID { get; set; }
        public string CHEKNMBR { get; set; }
        public string TRXDSCRN { get; set; }
        [Key, Column(Order = 2)]
        public string INTERID { get; set; }
        public Nullable<int> INTSTATUS { get; set; }
        public Nullable<System.DateTime> INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        public string ErrorDesc { get; set; }
    }
}
