﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpTireItem
    {
        public int PLT { get; set; }
        public decimal PLTWeight
        {
            get
            {
                return PLT * PLT_standeredWeight;
            }
        }
        public int MT { get; set; }
        public decimal MTWeight
        {
            get
            {
                return MT * MT_standeredWeight;
            }
        }

        public int AGLS { get; set; }
        public decimal AGLSWeight
        {
            get
            {
                return AGLS * AGLS_standeredWeight;
            }
        }

        public int IND { get; set; }
        public decimal INDWeight
        {
            get
            {
                return IND * IND_standeredWeight;
            }
        }

        public int SOTR { get; set; }
        public decimal SOTRWeight
        {
            get
            {
                return SOTR * SOTR_standeredWeight;
            }
        }

        public int MOTR { get; set; }
        public decimal MOTRWeight
        {
            get
            {
                return MOTR * MOTR_standeredWeight;
            }
        }
        public int LOTR { get; set; }
        public decimal LOTRWeight
        {
            get
            {
                return LOTR * LOTR_standeredWeight;
            }
        }
        public int GOTR { get; set; }
        public decimal GOTRWeight
        {
            get
            {
                return GOTR * GOTR_standeredWeight;
            }
        }
        public decimal TotalWeight
        {
            get
            {
                return PLTWeight + MTWeight + AGLSWeight + INDWeight + SOTRWeight + MOTRWeight + LOTRWeight + GOTRWeight;
            }
        }
        public decimal PLTPercentage
        {
            get
            {
                return TotalWeight != 0 ? PLTWeight / TotalWeight : 0;
            }
        }
        public decimal MTPercentage
        {
            get
            {
                return TotalWeight != 0 ? MTWeight / TotalWeight : 0;
            }
        }
        public decimal AGLSPercentage
        {
            get
            {
                return TotalWeight != 0 ? AGLSWeight / TotalWeight : 0;
            }
        }

        public decimal INDPercentage
        {
            get
            {
                return TotalWeight != 0 ? INDWeight / TotalWeight : 0;
            }
        }

        public decimal SOTRPercentage
        {
            get
            {
                return TotalWeight != 0 ? SOTRWeight / TotalWeight : 0;
            }
        }

        public decimal MOTRPercentage
        {
            get
            {
                return TotalWeight != 0 ? MOTRWeight / TotalWeight : 0;
            }
        }

        public decimal LOTRPercentage
        {
            get
            {
                return TotalWeight != 0 ? LOTRWeight / TotalWeight : 0;
            }
        }

        public decimal GOTRPercentage
        {
            get
            {
                return TotalWeight != 0 ? GOTRWeight / TotalWeight : 0;
            }
        }

        //for easier debugging purpose
        public decimal PLT_standeredWeight { get; set; }
        public decimal MT_standeredWeight { get; set; }
        public decimal AGLS_standeredWeight { get; set; }
        public decimal IND_standeredWeight { get; set; }
        public decimal SOTR_standeredWeight { get; set; }
        public decimal LOTR_standeredWeight { get; set; }
        public decimal MOTR_standeredWeight { get; set; }
        public decimal GOTR_standeredWeight { get; set; }
    }
}
