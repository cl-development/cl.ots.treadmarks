﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMImportDataRecordModel
    {            
        public string RegistrationNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string Percentage { get; set; }
        public string TotalTDPWeight { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public string ProductSoldDescription { get; set; }
        public string SoldToBusinessName { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ProvinceState { get; set; }
        public string PostalZip { get; set; }
        public string Country { get; set; }

    }
}
