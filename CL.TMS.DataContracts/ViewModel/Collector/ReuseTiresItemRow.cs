﻿using CL.TMS.DataContracts.ViewModel.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    [JsonObject]
    public class ReuseTiresItemRow:ItemRow
    {
        public string ReuseTires { get; set; }


        //OTSTM2-1370
        public DateTime CollectorInventoryPanelStartDisplayDate
        {
            get; set;
        }
        public DateTime ClaimPeriodStartDate
        {
            get; set;
        }
        public bool DisplayCollectorInventoryPanel
        {
            get { return CollectorInventoryPanelStartDisplayDate <= ClaimPeriodStartDate; }
        }

        public bool IsInitialAllZero { get; set; }
        public bool IsStaff { get; set; }
    }
}
