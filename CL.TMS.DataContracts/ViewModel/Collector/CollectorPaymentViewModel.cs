﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorPaymentViewModel
    {
        public CollectorPaymentViewModel()
        {
            HSTBase = 0.13m;
        }
        public decimal PLTRate { get; set; }
        public decimal PLTQty { get; set; }
        public decimal PLTPayment
        {
            get { return UsingOldRounding ? Math.Round(PLTRate * PLTQty, 2) : Math.Round(PLTRate * PLTQty, 2, MidpointRounding.AwayFromZero); }
        }

        //OTSTM2- 922
        public decimal MTRate { get; set; }
        public decimal MTQty { get; set; }
        public decimal MTPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(MTRate * MTQty, 2) : Math.Round(MTRate * MTQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal AGLSRate { get; set; }
        public decimal AGLSQty { get; set; }
        public decimal AGLSPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(AGLSRate * AGLSQty, 2) : Math.Round(AGLSRate * AGLSQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal INDRate { get; set; }
        public decimal INDQty { get; set; }

        public decimal INDPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(INDRate * INDQty, 2) : Math.Round(INDRate * INDQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal SOTRRate { get; set; }
        public decimal SOTRQty { get; set; }
        public decimal SOTRPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(SOTRRate * SOTRQty, 2) : Math.Round(SOTRRate * SOTRQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal MOTRRate { get; set; }
        public decimal MOTRQty { get; set; }
        public decimal MOTRPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(MOTRRate * MOTRQty, 2) : Math.Round(MOTRRate * MOTRQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal LOTRRate { get; set; }
        public decimal LOTRQty { get; set; }
        public decimal LOTRPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(LOTRRate * LOTRQty, 2) : Math.Round(LOTRRate * LOTRQty, 2, MidpointRounding.AwayFromZero);
            }
        }
        public decimal GOTRRate { get; set; }
        public decimal GOTRQty { get; set; }
        public decimal GOTRPayment
        {
            get
            {
                return UsingOldRounding ? Math.Round(GOTRRate * GOTRQty, 2) : Math.Round(GOTRRate * GOTRQty, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal MTGOTRRate { get; set; }
        public decimal MTGOTRQty { get; set; }
        public decimal MTGOTRPayment
        {
            get
            {
                if (UsingOldMTGOTRPayment)
                {
                    return UsingOldRounding ? Math.Round(MTGOTRRate * MTGOTRQty, 2) : Math.Round(MTGOTRRate * MTGOTRQty, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    return MTPayment + AGLSPayment + INDPayment + SOTRPayment + MOTRPayment + LOTRPayment + GOTRPayment;
                }
            }
        }

        public decimal PaymentAdjustment { get; set; }

        public decimal SubTotal
        {
            get { return UsingOldRounding ? Math.Round(PLTPayment + MTGOTRPayment + PaymentAdjustment, 2) : Math.Round(PLTPayment + MTGOTRPayment + PaymentAdjustment, 2, MidpointRounding.AwayFromZero); }
        }

        public bool IsTaxExempt { get; set; }
        public decimal HSTBase { get; set; }
        public decimal HST
        {
            get { return UsingOldRounding ? Math.Round(SubTotal * HSTBase, 2) : Math.Round(SubTotal * HSTBase, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal GrandTotal
        {
            get { return SubTotal + HST; }
        }

        public DateTime? PaymentDueDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public DateTime? MailedDate { get; set; }

        public string BatchNumber { get; set; }

        public bool UsingOldRounding { get; set; }

        public bool UsingOldMTGOTRPayment { get; set; }
    }
}
