﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.DataContracts.ViewModel.EmailSettings
{
    public class EmailElementVM
    {
        public EmailElementVM()
        { }

        public string ElementType { get; set; }

        public string Content { get; set; }
        
    }
}
