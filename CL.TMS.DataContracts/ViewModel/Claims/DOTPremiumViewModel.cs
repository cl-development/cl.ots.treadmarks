﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class DOTPremiumViewModel
    {
        public decimal N1OffRoadWeight { get; set; }
        public decimal N1OffRoadRate { get; set; }
        public decimal N1Premium
        {
            get
            {
                return UsingOldRounding ? Math.Round(N1OffRoadWeight * N1OffRoadRate, 2) : Math.Round(N1OffRoadWeight * N1OffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N2OffRoadWeight { get; set; }
        public decimal N2OffRoadRate { get; set; }
        public decimal N2Premium
        {
            get
            {
                return UsingOldRounding ? Math.Round(N2OffRoadWeight * N2OffRoadRate, 2) : Math.Round(N2OffRoadWeight * N2OffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N3OffRoadWeight { get; set; }
        public decimal N3OffRoadRate { get; set; }
        public decimal N3Premium
        {
            get
            {
                return UsingOldRounding ? Math.Round(N3OffRoadWeight * N3OffRoadRate, 2) : Math.Round(N3OffRoadWeight * N3OffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal N4OffRoadWeight { get; set; }
        public decimal N4OffRoadRate { get; set; }
        public decimal N4Premium
        {
            get
            {
                return UsingOldRounding ? Math.Round(N4OffRoadWeight * N4OffRoadRate, 2) : Math.Round(N4OffRoadWeight * N4OffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal TotalPickupWeight
        {
            get
            {
                return N1OffRoadWeight + N2OffRoadWeight + N3OffRoadWeight + N4OffRoadWeight;
            }
        }
        public decimal TotalPickupPremium
        {
            get
            {
                return N1Premium + N2Premium + N3Premium + N4Premium;
            }
        }

        public decimal MooseCreekOffRoadWeight { get; set; }
        public decimal MooseCreekOffRoadRate { get; set; }
        public decimal MooseCreekPremium
        {
            get
            {
                return UsingOldRounding ? Math.Round(MooseCreekOffRoadWeight * MooseCreekOffRoadRate, 2) : Math.Round(MooseCreekOffRoadWeight * MooseCreekOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal GTAOffRoadWeight { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal GTAPremium
        {
            get
            {
                return UsingOldRounding ? Math.Round(GTAOffRoadWeight * GTAOffRoadRate, 2) : Math.Round(GTAOffRoadWeight * GTAOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal WTCOffRoadWeight { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal WTCPremium
        {
            get
            {
                return UsingOldRounding ? Math.Round(WTCOffRoadWeight * WTCOffRoadRate, 2) : Math.Round(WTCOffRoadWeight * WTCOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal SturgeonFallsOffRoadWeight { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
        public decimal SturgeonFallsPremium
        {
            get
            {
                return UsingOldRounding ? Math.Round(SturgeonFallsOffRoadWeight * SturgeonFallsOffRoadRate, 2) : Math.Round(SturgeonFallsOffRoadWeight * SturgeonFallsOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }
        public decimal TotalSouthWeight
        {
            get
            {
                return MooseCreekOffRoadWeight + GTAOffRoadWeight + WTCOffRoadWeight + SturgeonFallsOffRoadWeight;
            }
        }

        public decimal TotalSouthPremium
        {
            get
            {
                return MooseCreekPremium + GTAPremium + WTCPremium + SturgeonFallsPremium;
            }
        }

        public decimal TotalDOTPremium
        {
            get
            {
                return TotalPickupPremium + TotalSouthPremium;
            }
        }

        public bool UsingOldRounding { get; set; }
    }
}
