﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims.Export
{
    public class CommonExportViewModel
    {
        public string RowName { get; set; }
        public string Actual { get; set; }
    }
}
