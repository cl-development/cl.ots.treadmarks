﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims.Export
{
    public class CommonStatusPanelExportViewModel
    {
        public string Type { get; set; }
        public string firstCol { get; set; }
        public string secondCol { get; set; }
        public string thirdCol { get; set; }
        public string fourthCol { get; set; }
        public string fifthCol { get; set; }
        public string sixthCol { get; set; }
        public string seventhCol { get; set; }
        public string eighthCol { get; set; }      
    }
}
