﻿using CL.Framework.Common;
using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimStatusViewModel
    {
        public int ClaimId { get; set; }

        /// <summary>
        /// this is ONLY related to claim itself, not about current user
        /// </summary>
        public bool IsClaimReadonly { get; set; }
        public bool isPreviousClaimOnHold { get; set; } //OTSTM2-499
        public bool isPreviousAuditOnHold { get; set; }
        public bool isFutureClaimOnHold { get; set; } 
        public bool isFutureAuditOnHold { get; set; }
        public long? AssignToUserId { get; set; }
        public string ReviewedBy { get; set; }

        public string UnderReview { get; set; }
        public string StatusString { get; set; }
        public ClaimStatus Status
        {
            get
            {
                return (string.IsNullOrEmpty(StatusString)) ? ClaimStatus.Unknown : EnumHelper.GetValueFromDescription<ClaimStatus>(StatusString);
            }
        }
        public DateTime? Submitted { get; set; }
        public DateTime? Assigned { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? ReviewDue { get; set; }
        public DateTime? Reviewed { get; set; }
        public DateTime? Approved { get; set; }
        public bool ClaimOnhold { get; set; }
        public DateTime? ClaimOnholdDate { get; set; }
        public DateTime? ClaimOffholdDate { get; set; }
        public int ClaimOnholdDays { get; set; }
        public bool AuditOnhold { get; set; }
        public DateTime? AuditOnholdDate { get; set; }
        public DateTime? AuditOffholdDate { get; set; }
        public int AuditOnholdDays { get; set; }
        public List<TransactionDetail> AllTransactionDetails { get; set; }
        public List<TransactionDetail> TotalTransactionDetailsIn { get; set; }
        public List<TransactionDetail> TotalTransactionDetailsOut { get; set; }
        public int TotalTransactions
        {
            get
            {
                return ((null == AllTransactionDetails) ? 0 : AllTransactionDetails.Count());
            }
        }
        public List<TransactionDetail> PaperTransactionDetailsIn { get; set; }
        public List<TransactionDetail> PaperTransactionDetailsOut { get; set; }

        public int PaperTransactions
        {
            get
            {
                return (null == PaperTransactionDetailsIn) ? 0 : PaperTransactionDetailsIn.Sum(c => c.TransactionCount) + PaperTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }

        public List<TransactionDetail> MobileTransactionDetailsIn { get; set; }
        public List<TransactionDetail> MobileTransactionDetailsOut { get; set; }
        public int MobileTransactions
        {
            get
            {
                return (null == MobileTransactionDetailsIn) ? 0 : MobileTransactionDetailsIn.Sum(c => c.TransactionCount) + MobileTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }
        public List<TransactionDetail> UnreviewedTransactionDetailsIn
        {
            get;
            set;
        }

        public List<TransactionDetail> UnreviewedTransactionDetailsOut
        {
            get;
            set;
        }
        public int UnreviewTransactions
        {
            get
            {
                return (null == UnreviewedTransactionDetailsIn) ? 0 : UnreviewedTransactionDetailsIn.Sum(c => c.TransactionCount) + UnreviewedTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }
        public List<TransactionDetail> AdjustedTransactionDetailsIn
        {
            get;
            set;
        }
        public List<TransactionDetail> AdjustedTransactionDetailsOut
        {
            get;
            set;
        }
        public int AdjustedTransactions
        {
            get
            {
                return (null == AdjustedTransactionDetailsIn) ? 0 : AdjustedTransactionDetailsIn.Sum(c => c.TransactionCount) + AdjustedTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }
        public List<TransactionDetail> ApprovedTransactionDetailsIn
        {
            get;
            set;
        }
        public List<TransactionDetail> ApprovedTransactionDetailsOut
        {
            get;
            set;
        }
        public int ApprovedTransactions
        {
            get
            {
                return (null == ApprovedTransactionDetailsIn) ? 0 : ApprovedTransactionDetailsIn.Sum(c => c.TransactionCount) + ApprovedTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }
        public List<TransactionDetail> ReviewedTransactionDetailsIn
        {
            get;
            set;
        }
        public List<TransactionDetail> ReviewedTransactionDetailsOut
        {
            get;
            set;
        }
        public int ReviewedTransactions
        {
            get
            {
                return (null == ReviewedTransactionDetailsIn) ? 0 : ReviewedTransactionDetailsIn.Sum(c => c.TransactionCount) + ReviewedTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }
        public List<TransactionDetail> InvalidatedTransactionDetailsIn
        {
            get;
            set;
        }
        public List<TransactionDetail> InvalidatedTransactionDetailsOut
        {
            get;
            set;
        }
        public int InvalidatedTransactions
        {
            get
            {
                return (null == InvalidatedTransactionDetailsIn) ? 0 : InvalidatedTransactionDetailsIn.Sum(c => c.TransactionCount) + InvalidatedTransactionDetailsOut.Sum(c => c.TransactionCount);
            }
        }

        //OTSTM2-31 
        public int FaxCount { get; set; }
        public int MailCount { get; set; }
        public int UploadCount { get; set; }

    }

    public class TransactionDetail
    {
        public bool MobileFormat { get; set; }
        public bool Direction { get; set; }
        public string TransactionType { get; set; }
        public string NavigationUrl { get; set; }
        public int TransactionCount { get; set; }
        public string ProcessingStatus { get; set; }
        public bool IsAdjusted { get; set; }
        ///OTSTM2-31
        public List<TransactionSupportDocumentCount> TransactionSupportDocumentCountList { get; set; }
    }

    //OTSTM2-31
    public class TransactionSupportDocumentCount
    {
        public int DocumentFormat { get; set; }
    }
}
