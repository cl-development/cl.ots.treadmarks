﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimsCommonHeaderViewModel
    {
        public ClaimsCommonHeaderViewModel()
        {
            Address = new Address();
            PrimaryContact = new Contact();
        }
        public bool? Generator { get; set; }
        public bool IsActive { get; set; }
        public string PrimaryBusinessActivity { get; set; }
        public string OperatingName { get; set; }
        public bool? CIsGenerator { get; set; }
        public DateTime? CGeneratorDate { get; set; }
        public string RegistrationNumber { get; set; }
        public Address Address { get; set; }
        public Contact PrimaryContact { get; set; }
        public DateTime? ActiveStatusChangeDate { get; set; }

    }
}
