﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimWorkflowUser
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}
