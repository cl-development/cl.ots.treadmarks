﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class DOTPremiumRateVM //PaymentType == 2
    {
        public DOTPremiumRateVM() { }
        public DOTPremiumRateVM(List<Rate> rates,TransportationIncentiveParam param)
        {
            var dotTypes = rates.Where(x => x.PaymentType == param.paymentTypeDOT && x.ClaimType==3);
            var rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.n1ZoneId);
            N1OffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.n2ZoneId);
            N2OffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.n3ZoneId);
            N3OffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.n4ZoneId);
            N4OffRoadRate = rate != null ? rate.ItemRate : 0;

            // For South DOT pick ups, rate will be based on pickup zone and drop off zone.
            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.mooseCreekZoneId);
            MooseCreekOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.gtaZoneId);
            GTAOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.wtcZoneId);
            WTCOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = dotTypes.FirstOrDefault(c =>  c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.sturgeonFallsZoneId);
            SturgeonFallsOffRoadRate = rate != null ? rate.ItemRate : 0;

        }
        public decimal N1OffRoadRate { get; set; }
        public decimal N2OffRoadRate { get; set; }
        public decimal N3OffRoadRate { get; set; }
        public decimal N4OffRoadRate { get; set; }

        public decimal MooseCreekOffRoadRate { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
    }
}
