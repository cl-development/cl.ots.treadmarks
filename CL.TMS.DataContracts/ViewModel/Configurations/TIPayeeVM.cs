﻿using CL.TMS.DataContracts.ViewModel.Common;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{

    public class TIPayeeVM
    {
        public TIPayeeVM()
        {
            AssignedHaulerIDs = new List<ItemModel<string>>();
            AvailabledHaulerIDs = new List<ItemModel<string>>();
        }

        public List<ItemModel<string>> AssignedHaulerIDs { get; set; }
        public List<ItemModel<string>> AvailabledHaulerIDs { get; set; }
    }
}