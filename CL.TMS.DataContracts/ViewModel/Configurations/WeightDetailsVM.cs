﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class WeightDetailsVM
    {
        public WeightDetailsVM()
        {
            Notes = new List<InternalNoteViewModel>();
        }
        public int WeightTransactionID { get; set; }
        public string EffectiveDate { get; set; }
        public int Category { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public DateTime PreviousEffectiveStartDate { get; set; }//during edit Rate, use this value.AddDays(-1), to locate previous Rates 
        public string Note { get; set; }
        public int DecimalSize { get; set; }
        public List<InternalNoteViewModel> Notes { get; set; }
               
        //Recovery Estimated Weights
        public RecoveryEstimatedWeightsVM RecoveryEstimatedWeights { get; set; }

    }
}
