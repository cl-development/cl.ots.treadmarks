﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class ProcessingIncentiveRateVM
    {
        public ProcessingIncentiveRateVM() { }
        public ProcessingIncentiveRateVM(List<Rate> rates,ProcessingIncentiveParam param)
        {
            var SPS = rates.Where(x => x.PaymentType == param.spsPaymentType);
            var PIT = rates.Where(x => x.PaymentType == param.pitPaymentType);
      
            #region //SPS OnRoad
                TDP1OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OnRoad).ItemRate;
                TDP1FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOnRoad).ItemRate;
                TDP2OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OnRoad).ItemRate;
                TDP2FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOnRoad).ItemRate;
                TDP3OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OnRoad).ItemRate;
                TDP3FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOnRoad).ItemRate;
                TDP4OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OnRoad).ItemRate;
                TDP5OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OnRoad).ItemRate;
            #endregion

            #region //SPS OffRoad
                TDP1OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OffRoad).ItemRate;
                TDP1FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOffRoad).ItemRate;
                TDP2OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OffRoad).ItemRate;
                TDP2FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOffRoad).ItemRate;
                TDP3OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OffRoad).ItemRate;
                TDP3FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOffRoad).ItemRate;
                TDP4OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OffRoad).ItemRate;
                TDP5OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OffRoad).ItemRate;
            #endregion

            #region //PIT
                TDP1 = PIT.FirstOrDefault(x => x.ItemID == param.tdp1).ItemRate;
                TDP2 = PIT.FirstOrDefault(x => x.ItemID == param.tdp2).ItemRate;
                TDP3 = PIT.FirstOrDefault(x => x.ItemID == param.tdp3).ItemRate;
                TDP4FF = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FF).ItemRate;
                TDP4FFNoPI = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FFNoPI).ItemRate;
                TDP5FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5FT).ItemRate;
                TDP5NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5NT).ItemRate;
                TDP6NP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6NP).ItemRate;
                TDP6FP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6FP).ItemRate;
                TDP7NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7NT).ItemRate;
                TDP7FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7FT).ItemRate;
                TransferFibreRubber = PIT.FirstOrDefault(x => x.ItemID == param.transferFibreRubber).ItemRate;
            #endregion
           
        }

        #region //SPS OnRoad

        public decimal TDP1OnRoad { get; set; }
        public decimal TDP1FeedstockOnRoad { get; set; }
        public decimal TDP2OnRoad { get; set; }
        public decimal TDP2FeedstockOnRoad { get; set; }
        public decimal TDP3OnRoad { get; set; }
        public decimal TDP3FeedstockOnRoad { get; set; }
        public decimal TDP4OnRoad { get; set; }
        public decimal TDP5OnRoad { get; set; }

        #endregion //SPS OnRoad

        #region //SPS OffRoad

        public decimal TDP1OffRoad { get; set; }
        public decimal TDP1FeedstockOffRoad { get; set; }
        public decimal TDP2OffRoad { get; set; }
        public decimal TDP2FeedstockOffRoad { get; set; }
        public decimal TDP3OffRoad { get; set; }
        public decimal TDP3FeedstockOffRoad { get; set; }
        public decimal TDP4OffRoad { get; set; }
        public decimal TDP5OffRoad { get; set; }

        #endregion //SPS OffRoad

        #region //PIT

        public decimal TDP1 { get; set; }
        public decimal TDP2 { get; set; }
        public decimal TDP3 { get; set; }
        public decimal TDP4FF { get; set; }
        public decimal TDP4FFNoPI { get; set; }
        public decimal TDP5FT { get; set; }
        public decimal TDP5NT { get; set; }
        public decimal TDP6NP { get; set; }
        public decimal TDP6FP { get; set; }
        public decimal TDP7NT { get; set; }
        public decimal TDP7FT { get; set; }
        public decimal TransferFibreRubber { get; set; } //Transfer-Fibre/Rubber

        #endregion //PIT
    }

    public class PISpecificRateVM
    {
        public PISpecificRateVM()
        {
            AssignedProcessIDs = new List<ItemModel<string>>();
            AvailabledProcessIDs = new List<ItemModel<string>>();
        }

        public List<ItemModel<string>> AssignedProcessIDs { get; set; }
        public List<ItemModel<string>> AvailabledProcessIDs { get; set; }
    }
}