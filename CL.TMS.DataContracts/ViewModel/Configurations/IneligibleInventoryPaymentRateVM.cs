﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class IneligibleInventoryPaymentRateVM //PaymentType == 3
    {
        public IneligibleInventoryPaymentRateVM() { }
        public IneligibleInventoryPaymentRateVM(List<Rate> result, TransportationIncentiveParam param)
        {
            var processTITypes = result.Where(x => x.PaymentType == param.paymentTypeProcessorTI && x.ClaimType == 4);
            var rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOnRoad);
            MooseCreakOnRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOffRoad);
            MooseCreakOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOnRoad);
            GTAOnRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOffRoad);
            GTAOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOnRoad);
            WTCOnRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOffRoad);
            WTCOffRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOnRoad);
            SturgeonFallsOnRoadRate = rate != null ? rate.ItemRate : 0;

            rate = processTITypes.FirstOrDefault(c =>  c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOffRoad);
            SturgeonFallsOffRoadRate = rate != null ? rate.ItemRate : 0;
        }
        public decimal MooseCreakOnRoadRate { get; set; }
        public decimal MooseCreakOffRoadRate { get; set; }
        public decimal GTAOnRoadRate { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal WTCOnRoadRate { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal SturgeonFallsOnRoadRate { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
    }
}
