﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class PenaltyRateVM
    {
        public decimal StewardRemittancePenaltyRate { get; set; }     
    }
}
