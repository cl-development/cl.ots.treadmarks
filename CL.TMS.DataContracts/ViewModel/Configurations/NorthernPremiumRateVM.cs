﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class NorthernPremiumRateVM //PaymentType == 1
    {
        public NorthernPremiumRateVM() { }
        public NorthernPremiumRateVM(List<Rate> rates, TransportationIncentiveParam param)
        {
            var northTypes = rates.Where(x => x.PaymentType == param.paymentTypeNorth);
            var rate = northTypes.FirstOrDefault(c => c.ClaimType == param.claimTypeHauler && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            N1SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            N2SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            N3SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            N4SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N1SouthRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N2SouthRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N3SouthRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N4SouthRateOnRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N1SouthRateOffRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N2SouthRateOffRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N3SouthRateOffRoad = rate != null ? rate.ItemRate : 0;
            rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            N4SouthRateOffRoad = rate != null ? rate.ItemRate : 0;
        }
        #region N1 Row
        public decimal N1SturgeonFallRateOnRoad { get; set; }
        public decimal N1SturgeonFallRateOffRoad { get; set; }
        public decimal N1SouthRateOnRoad { get; set; }
        public decimal N1SouthRateOffRoad { get; set; }
        #endregion

        #region N2 Row
        public decimal N2SturgeonFallRateOnRoad { get; set; }
        public decimal N2SturgeonFallRateOffRoad { get; set; }
        public decimal N2SouthRateOnRoad { get; set; }
        public decimal N2SouthRateOffRoad { get; set; }
        #endregion

        #region N3 Row
        public decimal N3SturgeonFallRateOnRoad { get; set; }
        public decimal N3SturgeonFallRateOffRoad { get; set; }
        public decimal N3SouthRateOnRoad { get; set; }
        public decimal N3SouthRateOffRoad { get; set; }
        #endregion

        #region N4 Row
        public decimal N4SturgeonFallRateOnRoad { get; set; }
        public decimal N4SturgeonFallRateOffRoad { get; set; }
        public decimal N4SouthRateOnRoad { get; set; }
        public decimal N4SouthRateOffRoad { get; set; }
        #endregion
    }
}
