﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class SupplyEstimatedWeightViewModel : BaseDTO<int>
    {
        public string ShortName { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public decimal RangeMin { get; set; }
        public decimal RangeMax { get; set; }
        public int Category { get; set; }
        
    }
}
