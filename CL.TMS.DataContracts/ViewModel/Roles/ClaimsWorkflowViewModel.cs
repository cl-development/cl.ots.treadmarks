﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class ClaimsWorkflowViewModel
    {
        public ClaimsWorkflowViewModel(string accountName)
        {
            AccountName = accountName;
        }
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string Workflow { get; set; }
        public bool ActionGear { get; set; }
    }
}
