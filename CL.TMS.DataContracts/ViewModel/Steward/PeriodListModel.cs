﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class PeriodListModel
    {
        public string ShortName { get; set; }
        public DateTime TSFRateDate { get; set; }
    }
}
