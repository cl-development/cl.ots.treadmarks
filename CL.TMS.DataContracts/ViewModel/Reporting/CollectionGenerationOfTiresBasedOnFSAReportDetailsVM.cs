﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresBasedOnFSAReportDetailsVM : BaseDTO<int>
    {
        [Display(Name = "Hauler Registration Number")]
        public string HaulerRegistrationNumber { get; set; }        
        [Display(Name = "Hauler Name")]
        public string HaulerName { get; set; }
        [Display(Name = "Pickup Date")]
        public DateTime? PickupDate { get; set; }
        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }
        [Display(Name = "Form Number")]
        public string FormNumber { get; set; }
        [Display(Name = "Registration Number")]
        public string RegistrationNumber { get; set; }
        [Display(Name = "Collector Name")]
        public string CollectorName { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }     
        [Display(Name = "PLT")]
        public int PLT { get; set; }
        [Display(Name = "MT")]
        public int MT { get; set; }
        [Display(Name = "AG/LS")]
        public int AGLS { get; set; }
        [Display(Name = "IND")]
        public int IND { get; set; }
        [Display(Name = "SOTR")]
        public int SOTR { get; set; }
        [Display(Name = "MOTR")]
        public int MOTR { get; set; }      
        [Display(Name = "LOTR")]
        public int LOTR { get; set; }
        [Display(Name = "GOTR")]
        public int GOTR { get; set; }
    }
}
