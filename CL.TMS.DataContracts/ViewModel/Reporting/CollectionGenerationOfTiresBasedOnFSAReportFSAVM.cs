﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresBasedOnFSAReportFSAVM
    {
        public string RegionName { get; set; }
        public List<string> ZoneNameList { get; set; }
    }
}
