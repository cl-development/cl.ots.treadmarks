﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class HaulerVolumeReport
    {
        [Display(Name = "Reporting Period")]
        public string ReportingPeriod { get; set; }
        [Display(Name = "Registration Number")]
        public decimal HaulerRegistrationNumber { get; set; }
        [Display(Name = "Hauler Status")]
        public string HaulerStatus { get; set; }
        [Display(Name = "Destination Type")]
        public string DestinationType { get; set; }
        [Display(Name = "Processor / Hauler Registration Number")]
        public Nullable<decimal> ProcessorHaulerRegistrationNumber { get; set; }
        [Display(Name = "Processor Status")]
        public string Status { get; set; }
        [Display(Name = "Record State")]
        public string RecordState { get; set; }
        [Display(Name = "Used Tire Destination")]
        public string UsedTireDestionation { get; set; }
        [Display(Name = "Total Actual Weight")]
        public Nullable<decimal> TotalActualWeight { get; set; }
        [Display(Name = "Total (Est. Weight)")]
        public Nullable<decimal> TotalEstWeight { get; set; }
        [Display(Name = "PLT (Est. Weight)")]
        public Nullable<decimal> PltEstWeight { get; set; }
        [Display(Name = "MT (Est. Weight)")]
        public Nullable<decimal> MtEstWeight { get; set; }
        [Display(Name = "AGLS (Est. Weight)")]
        public Nullable<decimal> AglsEstWeight { get; set; }
        [Display(Name = "IND (Est. Weight)")]
        public Nullable<decimal> IndEstWeight { get; set; }
        [Display(Name = "SOTR (Est. Weight)")]
        public Nullable<decimal> SotrEstWeight { get; set; }
        [Display(Name = "MOTR (Est. Weight)")]
        public Nullable<decimal> MotrEstWeight { get; set; }
        [Display(Name = "LOTR (Est. Weight)")]
        public Nullable<decimal> LotrEstWeight { get; set; }
        [Display(Name = "GOTR (Est. Weight)")]
        public Nullable<decimal> GotrEstWeight { get; set; }
        [Display(Name = "Total Payment Earned - (Processor)")]
        public Nullable<decimal> TotalPaymentEarnedProcessor { get; set; }
        [Display(Name = "PLT Total Payment Earned - (Processor)")]
        public Nullable<decimal> PltPaymentEarnedProcessor { get; set; }
        [Display(Name = "MT Total Payment Earned - (Processor)")]
        public Nullable<decimal> MtPaymentEarnedProcessor { get; set; }
        [Display(Name = "AG/LS Total Payment Earned - (Processor)")]
        public Nullable<decimal> AglsPaymentEarnedProcessor { get; set; }
        [Display(Name = "IND Total Payment Earned - (Processor)")]
        public Nullable<decimal> IndPaymentEarnedProcessor { get; set; }
        [Display(Name = "SOTR Total Payment Earned - (Processor)")]
        public Nullable<decimal> SotrPaymentEarnedProcessor { get; set; }
        [Display(Name = "MOTR Total Payment Earned - (Processor)")]
        public Nullable<decimal> MotrPaymentEarnedProcessor { get; set; }
        [Display(Name = "LOTR Total Payment Earned - (Processor)")]
        public Nullable<decimal> LotrPaymenEarnedProcessor { get; set; }
        [Display(Name = "GOTR Total Payment Earned - (Processor)")]
        public Nullable<decimal> GotrPaymentEarnedProcessor { get; set; }
        [Display(Name = "Total Payment Earned - (Hauler)")]
        public Nullable<decimal> TotalPaymentEarnedHauler { get; set; }
        [Display(Name = "PLT Total Payment Earned - (Hauler)")]
        public Nullable<decimal> PltPaymentEarnedHauler { get; set; }
        [Display(Name = "MT Total Payment Earned - (Hauler)")]
        public Nullable<decimal> MtPaymentEarnedHauler { get; set; }
        [Display(Name = "AG/LS Total Payment Earned - (Hauler)")]
        public Nullable<decimal> AglsPaymentEarnedHauler { get; set; }
        [Display(Name = "IND Total Payment Earned - (Hauler)")]
        public Nullable<decimal> IndPaymentEarnedHauler { get; set; }
        [Display(Name = "SOTR Total Payment Earned - (Hauler)")]
        public Nullable<decimal> SotrPaymentEarnedHauler { get; set; }
        [Display(Name = "MOTR Total Payment Earned - (Hauler)")]
        public Nullable<decimal> MotrPaymentEarnedHauler { get; set; }
        [Display(Name = "LOTR Total Payment Earned - (Hauler)")]
        public Nullable<decimal> LotrPaymenEarnedHauler { get; set; }
        [Display(Name = "GOTR Total Payment Earned - (Hauler)")]
        public Nullable<decimal> GotrPaymentEarnedHauler { get; set; }
        public Nullable<decimal> Hst { get; set; }
        [Display(Name = "Total (Payment earned with HST)")]
        public Nullable<decimal> TotalPaymentEarnedWithHst { get; set; }
    }
}
