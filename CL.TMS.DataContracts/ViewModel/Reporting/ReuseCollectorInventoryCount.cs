﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{

    public class ReuseCollectorInventoryCount
    {
        [Display(Name = "Claim Period")]
        public string ClaimPeriod { get; set;}
        [Display(Name = "Reg#")]
        public string CollectorRegNumber { get; set;}
        [Display(Name = "Collector Legal Business Name")]
        public string CollectorLegalName { get; set; }
        [Display(Name = "Collector Business Operating Name")]
        public string CollectorOperatingName { get; set; }
        [Display(Name = "Claim Status")]
        public string Status { get; set; }
        [Display(Name = "Collector Status")]
        public string CollectorStatus { get; set; }




        [Display(Name = "PLT")]
        public int? PLT { get; set; }
        [Display(Name = "MT")]
        public int? MT { get; set; }
        [Display(Name = "AG")]
        public int? AGLS { get; set; }
        [Display(Name = "IND")]
        public int? IND { get; set; }
        [Display(Name = "SOTR")]
        public int? SOTR { get; set; }
        [Display(Name = "MOTR")]
        public int? MOTR { get; set; }
        [Display(Name = "LOTR")]
        public int? LOTR { get; set; }
        [Display(Name = "GOTR")]
        public int? GOTR { get; set; }
       
    }
}
