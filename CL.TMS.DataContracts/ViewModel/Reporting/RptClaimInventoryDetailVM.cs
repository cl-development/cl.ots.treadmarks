﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class RptClaimInventoryDetailVM
    {
        public int ClaimId { get; set; }
        public int ItemId { get; set; }
        public string ItemShortName { get; set; }
        public int QuantityInbound { get; set; }
        public int QuantityOutbound { get; set; }
        public int TransactionId { get; set; }
        public bool Direction { get; set; }
        public int Quantity
        {
            get
            {
                return this.QuantityInbound - this.QuantityOutbound + this.Adj;
            }
        }
        public int Adj { get; set; }
    }
}
