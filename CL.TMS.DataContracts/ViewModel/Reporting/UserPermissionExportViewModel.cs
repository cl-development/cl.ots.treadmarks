﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class UserPermissionExportViewModel
    {
        [Display(Name = "Account")]
        public string Account { get; set; }
        [Display(Name = "Menu")]
        public string Menu { get; set; }
        [Display(Name = "Screen")]
        public string Screen { get; set; }
        [Display(Name = "Panel")]
        public string Panel { get; set; }
        [Display(Name = "No Access")]
        public string NoAccess { get; set; }
        [Display(Name = "Read Only")]
        public string ReadOnly { get; set; }
        [Display(Name = "Edit/Save")]
        public string EditSave { get; set; }
        [Display(Name = "Custom")]
        public string Custom { get; set; }
        [Display(Name = "Workflow")]
        public string Workflow { get; set; }
        [Display(Name = "Action Gear")]
        public string ActionGear { get; set; }

    }
}
