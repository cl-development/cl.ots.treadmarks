﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresBasedOnFSAReportSelectionVM
    {
        public bool IsAllRegion { get; set; }
        public bool IsEastern { get; set; }
        public bool IsCentral { get; set; }
        public bool IsGTA { get; set; }
        public bool IsSouthWestern { get; set; }
        public bool IsNorthern { get; set; }
        public bool IsAllZone { get; set; }
        public bool IsZoneZero { get; set; }
        public bool IsZoneOne { get; set; }
        public bool IsZoneTwo { get; set; }
        public bool IsZoneThree { get; set; }
        public bool IsZoneFour { get; set; }
        public bool IsZoneFive { get; set; }
        public bool IsZoneSix { get; set; }
        public bool IsZoneSeven { get; set; }
        public bool IsZoneEight { get; set; }
        public bool IsZoneNine { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
