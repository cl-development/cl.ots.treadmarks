﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Claims;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class HaulerPaymentViewModel
    {
        #region Payment Adjustment
        public HaulerPaymentViewModel()
        {
            HSTBase = 0.13m;
        }
        public decimal NorthernPremium { get; set; }
        public decimal DOTPremium { get; set; }
        public decimal HaulerPTR { get; set; }
        public DateTime HaulerPTRStartDisplayDate
        {
            get; set;
        }
        public DateTime ClaimPeriodStartDate
        {
            get; set;
        }
        public bool DisplayHaulerPTR
        {
            get { return HaulerPTRStartDisplayDate <= ClaimPeriodStartDate; }
        }
        public decimal PaymentAdjustment { get; set; }

        public decimal SubTotal
        {
            get { return UsingOldRounding ? Math.Round(NorthernPremium + DOTPremium + HaulerPTR + PaymentAdjustment, 2) : Math.Round(NorthernPremium + DOTPremium + HaulerPTR + PaymentAdjustment, 2, MidpointRounding.AwayFromZero); }
        }

        public decimal HSTBase { get; set; }
        public decimal HST
        {
            get { return UsingOldRounding ? Math.Round(SubTotal * HSTBase, 2) : Math.Round(SubTotal * HSTBase, 2, MidpointRounding.AwayFromZero); }
        }

        public bool IsTaxExempt { get; set; }

        public decimal PaymentTotal
        {
            get { return SubTotal + HST; }
        }

        public decimal IneligibleInventoryPayment { get; set; }
        public decimal IneligibleInventoryPaymentAdjust { get; set; }

        public decimal GrandTotal
        {
            get { return PaymentTotal + IneligibleInventoryPayment + IneligibleInventoryPaymentAdjust; }
        }

        public DateTime? PaymentDueDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public DateTime? MailedDate { get; set; }
        public string BatchNumber { get; set; }
        public bool? isStaff { get; set; }
        public bool UsingOldRounding { get; set; }
        #endregion

        #region Premium
        public NorthernPremiumViewModel NorthernPremiumDetail { get; set; }
        public DOTPremiumViewModel DOTPremiumDetail { get; set; }
        public ClawbackViewModel IneligibleInventoryPaymentDetail { get; set; }
        #endregion

    }
}
