﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class TireDetailsRegistrationModel : RegistrationModelBase
    {
        public IList<TireDetailsItemTypeModel> TireDetailsItemType { get; set; }   
        public Nullable<bool> HHasRelatioshipWithProcessor { get; set; }
        public Nullable<int> HRelatedProcessor { get; set; }
    }
}
