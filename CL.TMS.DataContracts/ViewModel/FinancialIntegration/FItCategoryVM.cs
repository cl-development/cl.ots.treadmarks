﻿using CL.TMS.DataContracts.ViewModel.Common;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.FinancialIntegration
{
    public class FICategoryVM
    {
        public IEnumerable<FinancialIntegrationVM> steward { get; set; }
        public IEnumerable<FinancialIntegrationVM> collector { get; set; }
        public IEnumerable<FinancialIntegrationVM> hauler { get; set; }
        public IEnumerable<FinancialIntegrationVM> processor { get; set; }
        public IEnumerable<FinancialIntegrationVM> rpm { get; set; }
        public List<ItemModel<string>> stewardTooltip { get; set; }
    }
}