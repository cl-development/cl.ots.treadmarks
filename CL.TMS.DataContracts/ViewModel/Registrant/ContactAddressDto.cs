﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class ContactAddressDto
    {
        public ContactAddressDto()
        {

        }
        public Contact Contact { get; set; }
        public Address Address { get; set; }
    }
}
