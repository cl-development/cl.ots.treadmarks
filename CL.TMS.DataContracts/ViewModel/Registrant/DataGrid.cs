﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public abstract class DataGrid<T> where T : class
    {
        public virtual IReadOnlyList<T> data { get; set; }
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
    }


    public abstract class Row
    {
    }

    //public class DataGridModel
    //{
    //    public int Length { get; set; }
    //    public int Start { get; set; }
    //    public int Draw { get; set; }
    //    public Dictionary<int, string> Columns { get; set; }
    //    public List<Dictionary<string, string>> Order { get; set; }
    //    public Dictionary<string, string> Search { get; set; }
    //}
}