﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.Validation;
using CL.TMS.Framework.DTO;
using CL.TMS.Resources;
using FluentValidation.Attributes;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [Validator(typeof(AuthenticationUserValidator))]
    public class AuthenticationUser:BaseDTO<long>
    {
        public AuthenticationUser()
        {
            UserVendorClaims = new List<Claim>();
        }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName", ResourceType = typeof(UIResource))]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(UIResource))]
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsLocked { get; set; }
        public bool Inactive { get; set; }
        public int InvalidLoginAttempts { get; set; }
        public DateTime PasswordExpirationDate { get; set; }
        public bool PasswordReset { get; set; }

        public string PasswordRestToken { get; set; }
        public string PasswordRestSecretKey { get; set; }
        public DateTime LastLoginDate { get; set; }

        public string RedirectUserAfterLoginUrl { get; set; }

        [NotMapped]
        public bool RememberMe { get; set; }

        [DefaultValue(true)]
        [NotMapped]
        public bool EnablePasswordReset { get; set; }
        [NotMapped]
        public SignInStatus Status { get; set; }
        
        [NotMapped]
        public string StewardType { get; set; }

        [NotMapped]
        public int VendorId { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

        public List<Claim> UserVendorClaims { get; set; }

    }

    public enum SignInStatus
    {
        // Summary:
        //     Sign in was successful
        Success = 0,
        //
        // Summary:
        //     User is locked out
        LockedOut = 1,
        //
        // Summary:
        //     Sign in requires addition verification (i.e. two factor)
        RequiresVerification = 2,
        //
        // Summary:
        //     Sign in failed
        Failure = 3,
        //
        // Summary:
        //    Invalid User Name
        InvalidUserName = 4,
        //
        // Summary:
        //    Invalid Password
        InvalidPassword = 5,
        //
        // Summary:
        //    Password reset required
        PasswordResetRequired = 6,
        //
        // Summary:
        //    Inactive user account
        Inactive = 7,
    }

}
