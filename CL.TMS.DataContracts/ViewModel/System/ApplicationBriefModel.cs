﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Newtonsoft.Json.Serialization;


namespace CL.TMS.DataContracts.ViewModel.System
{
    public class ApplicationViewModel:BaseDTO<int>
    {
        public string Status { get; set; }
        public DateTime? ApplicationSubmitDate { get; set; }
        public string assignedToName { get; set; }
        public string ParticipantType { get; set; }
        public string BusinessName { get; set; }
        public string ContactName { get; set; }
        public string TokenID { get; set; }

        public int StatusIndex { get; set; }
        public string Email { get; set; }        
        public UserReference CurrentUser { get; set; }

    }
}
