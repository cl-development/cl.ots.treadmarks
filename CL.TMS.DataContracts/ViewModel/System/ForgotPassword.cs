﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Validation;
using CL.TMS.Framework.DTO;
using CL.TMS.Resources;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [Validator(typeof(ForgotpasswordValidator))]
    public class ForgotPassword:BaseDTO<long>
    {
        [DataType(DataType.EmailAddress)]
        [Display(Name = "EmailAddress", ResourceType = typeof(UIResource))]
        public string EmailAddress { get; set; }

    }
}
