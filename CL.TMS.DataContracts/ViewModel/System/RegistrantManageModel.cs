﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class RegistrantManageModel : BaseDTO<long>
    {
        public string Number { get; set; }//Reg #
        public string BusinessName { get; set; }//Company
        public string Contact { get; set; }
        public string Status { get; set; }//Activate/Inactivate
        public int ParticipantType { get; set; }
        public string TokenID { get; set; }
        public int ApplicationID { get; set; }
        public int VendorID { get; set; }
        public string BankInfoReviewStatus { get; set; }
        public int BankInfoId { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
