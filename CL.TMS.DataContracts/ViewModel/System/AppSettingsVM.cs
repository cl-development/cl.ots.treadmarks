﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [JsonObject]
    public class AppSettingsVM //: IEnumerable
    {
        //Email
        //public string Email_BccApplicationPath { get; set; }
        //public string Email_StewardApplicationApproveBCCEmailAddr { get; set; }
        /////public string Email_defaultFrom { get; set; }
        //public bool Email_CBCCEmailClaimAndApplication { get; set; }
        //public bool Email_CBCCEmailStewardAndRemittance { get; set; }

        //Invitations
        public int Invitation_ExpirationDays { get; set; }//3
        public int Application_InvitationExpiryDays { get; set; }//4
        public int Application_PurgeOpenApplicationAfterDays { get; set; }//5; new [key]

        //Passwords
        public int UserPolicy_MaxLoginAttemptBeforeLock { get; set; }
        public int UserPolicy_PasswordExpiresInDays { get; set; }
        public int UserPolicy_PasswordResetTokenExpiresInMinutes { get; set; }
        ///public string UserPolicy_MaxLoginAttemptsBeforePasswordReset { get; set; }

        //Service Levels
        public int Claims_ReviewDueDays { get; set; }
        public int Claims_ChequeDueDays { get; set; }

        //Other
        public int DistanceVariance { get; set; }//11
        public DateTime Settings_UserDate { get; set; }//12

        [JsonIgnore]
        public bool bDataValidationSuccess
        {
            get
            {
                //bool bEmail = IsValidEmail(Email_BccApplicationPath, this.Email_CBCCEmailClaimAndApplication) && IsValidEmail(Email_StewardApplicationApproveBCCEmailAddr, this.Email_CBCCEmailStewardAndRemittance);
                //return ((Invitation_ExpirationDays >= 0) && (Application_InvitationExpiryDays >= 0) && (Application_PurgeOpenApplicationAfterDays >= 0)
                //    && (UserPolicy_MaxLoginAttemptBeforeLock >= 0) && (UserPolicy_PasswordExpiresInDays >= 0) && (UserPolicy_PasswordResetTokenExpiresInMinutes >= 0)
                //    && (Claims_ReviewDueDays >= 0) && (Claims_ChequeDueDays >= 0) && (DistanceVariance >= 0)
                //    && bEmail && otherRules());
                return ((Invitation_ExpirationDays >= 0) && (Application_InvitationExpiryDays >= 0) && (Application_PurgeOpenApplicationAfterDays >= 0)
                    && (UserPolicy_MaxLoginAttemptBeforeLock >= 0) && (UserPolicy_PasswordExpiresInDays >= 0) && (UserPolicy_PasswordResetTokenExpiresInMinutes >= 0)
                    && (Claims_ReviewDueDays >= 0) && (Claims_ChequeDueDays >= 0) && (DistanceVariance >= 0)
                    && otherRules());
            }
        }
        bool IsValidEmail(string emailaddress, bool needEmail)
        {
            try
            {
                if (string.IsNullOrEmpty(emailaddress))
                {
                    if (needEmail)
                    {//need Email, but value is null
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        bool otherRules()
        {
            return Application_PurgeOpenApplicationAfterDays >= Application_InvitationExpiryDays;
        }
    }
}