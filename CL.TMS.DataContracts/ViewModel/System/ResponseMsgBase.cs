﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{

    public abstract class ResponseMsgBase //TODO create class as a Generic <T>
    {
        protected ResponseMsgBase()
        {
            Warnings = new List<string>();
            Errors = new List<string>();
        }
        public int ID { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Warnings { get; set; }
        //public T Single { get; set; }
    }
}
