﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.System
{    public class ItemRecoverableMaterialVM {
        public int? MaterialID { get; set; }
        public string MaterialName { get; set; }
        public decimal? Quantity { get; set; }
        public string Color { get; set; }
    }  
}