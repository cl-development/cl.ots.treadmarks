﻿using System;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ProcessorListModel
    {
        private string number;
        private string name;
        public int ID { get; set; }
        public string Number { set { number = value; } }
        public string Name { set { name = value; } }
        public string Text { get { return string.Format("{0} ({1})", name, number); } }
    }
}