﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ResponseMessage
    {
        private WebServiceResponseStatus responseStatus = WebServiceResponseStatus.NONE;
        private List<string> responseMessages = new List<string>();
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (WebServiceResponseStatus)Enum.Parse(typeof(WebServiceResponseStatus), value.ToString()); }
        }
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
    }
}
