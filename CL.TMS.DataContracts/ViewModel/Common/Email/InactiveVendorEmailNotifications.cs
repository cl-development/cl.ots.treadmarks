﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.Email
{
    public class InactiveVendorEmailNotifications
    {
        public static string Date { get { return "@Date"; } }
        public static string InactiveRegType { get { return "@InactiveRegType"; } }
        public static string InactiveRegNo { get { return "@InactiveRegNo"; } }
        public static string ActiveRegType { get { return "@ActiveRegType"; } }
        public static string InactiveBusinessName { get { return "@InactiveBusinessName"; } }
        public static string InactiveAddress { get { return "@InactiveAddress"; } }
        public static string InactiveDate { get { return "@InactiveDate"; } }
        public static string CompanyLogo { get { return "@CompanyLogo"; } }
        public static string TwitterLogo { get { return "@TwitterLogo"; } }
        public static string TwitterURL { get { return "@TwitterURL"; } }
        public static string ApplicationType { get { return "@ApplicationType"; } }
        public static string ApplicationLogo { get { return "@ApplicationLogo"; } }
        public static string NewDatetimeNowYear { get { return "@NewDatetimeNowYear"; } } //OTSTM2-609
    }
}
