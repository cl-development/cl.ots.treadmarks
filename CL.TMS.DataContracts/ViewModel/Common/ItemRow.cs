﻿using CL.TMS.Common;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ItemRow : IEnumerable
    {
        public ItemRow()
        {
        }
        public ItemRow(List<KeyValuePair<string, int>> array)
        {
            foreach (var item in array)
            {
                switch (item.Key)
                {
                    case TreadMarksConstants.PLT:
                        PLT = item.Value;
                        break;
                    case TreadMarksConstants.MT:
                        MT = item.Value;
                        break;
                    case TreadMarksConstants.AGLS:
                        AGLS = item.Value;
                        break;
                    case TreadMarksConstants.IND:
                        IND = item.Value;
                        break;
                    case TreadMarksConstants.SOTR:
                        SOTR = item.Value;
                        break;
                    case TreadMarksConstants.MOTR:
                        MOTR = item.Value;
                        break;
                    case TreadMarksConstants.LOTR:
                        LOTR = item.Value;
                        break;
                    case TreadMarksConstants.GOTR:
                        GOTR = item.Value;
                        break;
                    default:
                        break;
                }
            }
        }
        public int PLT { get; set; }
        public int MT { get; set; }
        public int AGLS { get; set; }
        public int IND { get; set; }
        public int SOTR { get; set; }
        public int MOTR { get; set; }
        public int LOTR { get; set; }
        public int GOTR { get; set; }
        public bool GenerateTires { get; set; }
        public IEnumerator GetEnumerator()
        {
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.PLT, PLT );
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.MT, MT);
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.AGLS, AGLS);
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.IND, IND );
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.SOTR, SOTR );
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.MOTR, MOTR );
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.LOTR, LOTR );
            yield return new KeyValuePair<string, int> ( TreadMarksConstants.GOTR, GOTR );
        }
        public bool IsAllFieldsZero
        {
            get
            {
                if (PLT == 0 && MT == 0 && AGLS == 0 && IND == 0 && SOTR == 0 && MOTR == 0
                && LOTR == 0 && GOTR == 0)
                {
                    return true;
                }
                return false;
            }

        }
    }
}
