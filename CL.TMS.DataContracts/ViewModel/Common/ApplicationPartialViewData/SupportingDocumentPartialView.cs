﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.ApplicationPartialViewData
{
    public class SupportingDocumentPartialView : CommonPartialView
    {
        public string Key { get { return "SupportingDocument"; } }
        public string status { get; set; }
        public bool? allowFileDownload { get; set; }
        public bool? allowFileDelete { get; set; }
        public string prefix { get; set; }
        
    }
}
