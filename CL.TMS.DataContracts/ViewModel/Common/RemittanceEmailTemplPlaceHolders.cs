﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public static class RemittanceEmailTemplPlaceHolders
    {
        public static string Date { get { return "@Date"; } }
        public static string RegistrationNumber { get { return "@RegistrationNumber"; } }
        public static string BusinessLegalName { get { return "@BusinessLegalName"; } }
        public static string TwitterURL { get { return "@TwitterURL"; } }
        public static string FacebookURL { get { return "@FacebookURL"; } }
        public static string WebsiteURL { get { return "@WebsiteURL"; } }
        public static string Period { get { return "@Period"; } }
        public static string LoginURL { get { return "@LoginURL"; } }
        public static string ApplicationLogo { get { return "@ApplicationLogo"; } }
        public static string CompanyLogo { get { return "@CompanyLogo"; } }
        //public static string TwitterLogo { get { return "@TwitterLogo"; } }

        public static string NewDatetimeNowYear { get { return "@NewDatetimeNowYear"; } } //OTSTM2-609
    }
}
