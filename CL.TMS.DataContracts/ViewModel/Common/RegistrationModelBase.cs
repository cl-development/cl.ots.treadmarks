﻿using System.Collections.Generic;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Hauler;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public abstract class RegistrationModelBase
    {
        public ApplicationStatusEnum Status { get; set; }
        //Added
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        public long? AssignToUserId { get; set; }
       
    }
}
