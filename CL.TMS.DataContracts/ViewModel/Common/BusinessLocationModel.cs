﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class BusinessLocationModel
    {
        public Vendor BusinessLocationVendor { get; set; }
        public Address BusinessLocationAddress { get; set; }
        public bool MailAddressSameAsBusinessAddress { get; set; }
        public Address BusinessMailingAddress { get; set; }
    }
}
