﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class DataGridModel
    {
        public int Length { get; set; }
        public int Start { get; set; }
        public int Draw { get; set; }
        public Dictionary<int, string> Columns { get; set; }
        public List<Dictionary<string, string>> Order { get; set; }
        public Dictionary<string, string> Search { get; set; }
    }
    public class DataGridModelExtend : DataGridModel
    {
        public string DataType { get; set; }
        public string DataSubType { get; set; }
        public int ClaimId { get; set; }
    }
}
