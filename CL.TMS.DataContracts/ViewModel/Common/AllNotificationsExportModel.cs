﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class AllNotificationsExportModel
    {
        [Display(Name ="Date/Time")]
        public DateTime CreatedDate { get; set; }
        [Display(Name ="Details")]
        public string Message { get; set; }
    } 
}
