﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class SpecificRateModel 
    {
        public int ID { get; set; }
        public int? ItemID { get; set; }
        public int? PaymentType { get; set; }
        public int ClaimType { get; set; }
        public int? PeriodID { get; set; }
        public decimal ItemRate { get; set; }
        public DateTime? EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }
        public int? SourceZoneID { get; set; }
        public int? DeliveryZoneID { get; set; }
        public string GreaterZone { get; set; }
        public string RateDescription { get; set; }
        public int? ItemType { get; set; }
        public int? RateTransactionID { get; set; }
        public int? VendorID { get; set; }
        public bool? IsSpecificRate { get; set; }        
    }
}
