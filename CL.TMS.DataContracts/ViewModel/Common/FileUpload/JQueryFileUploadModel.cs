﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class JQueryFileUploadModel
    {
        public JQueryFileUploadModel()
        {
            this.files = new List<JQueryFileUploadFileModel>();
        }

        public List<JQueryFileUploadFileModel> files { get; set; }

    }

    public class JQueryFileUploadFileModel
    {
        public int id { get; set; }
        public int transactionId { get; set; }
        public string encodedFileName { get; set; }
        public string type { get; set; }
        public long size { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string createdOn { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_type { get; set; }
        public bool allowDelete { get; set; }
        public bool allowDownload { get; set; }
        public bool allowSelection { get; set; }
        public bool isValid { get; set; }
        public string validationMessage { get; set; }
    }
}
