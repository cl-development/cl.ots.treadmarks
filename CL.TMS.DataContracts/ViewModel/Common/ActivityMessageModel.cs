﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ActivityMessageModel
    {
        public long ID { get; set; }
        [Display(Name = "Search")]
        public string search { get; set; }
        public IList<ActivityMessageModel> ActivityMessages { get; set; }

    }
}
