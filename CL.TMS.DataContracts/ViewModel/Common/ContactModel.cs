﻿namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ContactModel : InvalidFormFieldModel
    {

        public int ID { get; set; }

        public int AddressID { get; set; }

        //public string Name { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }

        public bool IsPrimary { get; set; }

        public string PhoneNumber { get; set; }

        public string AlternatePhoneNumber { get; set; }

        public string Ext { get; set; }

        public string FaxNumber { get; set; }

        public string PreferredContactMethod { get; set; }

        public bool ContactAddressSameAsBusiness { get; set; }

        //public IAddress Address { get; set; }
    }
}
