﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels
{
    public class PreviousScaleTicketResult
    {
        public string TransactionId { get; set; }
        public string TicketNumber { get; set; }
        public string TransactionTypeCode {get; set;}
        public string IncomingId { get; set; }
        public string OutgoingId { get; set; }
        public string PeriodShortName { get; set; }
        public string TransactionNumber { get; set; }
        public Vendor OutgoingVendor { get; set; }
        public Vendor IncomingVendor { get; set; }
        public string Status { get; set; }
    }
}
