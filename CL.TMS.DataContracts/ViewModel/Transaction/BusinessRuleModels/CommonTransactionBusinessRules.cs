﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels
{
    public class CommonTransactionBusinessRules
    {
        public List<string> PreviousScaleTicketError { get; set; }
    }
}
