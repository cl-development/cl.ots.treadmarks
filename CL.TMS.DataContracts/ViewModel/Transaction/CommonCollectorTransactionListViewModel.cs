﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    class CommonCollectorTransactionListViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        ///this is created from copy of CommonStaffTransactionListViewModel
        ///TCR
        public Guid TransactionId { get; set; }
        public string DeviceName { get; set; }
        public string Notes { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TransactionFriendlyId { get; set; }//TCR #, DOT #, HIT #...
        public string ReviewStatus { get; set; }
        public string Adjustment
        {
            get
            {
                if (0 < IsAdjustByStaff.Count)
                {
                    if (IsAdjustByStaff.All(c => c == true))
                    {
                        return "Staff";
                    }
                    else if (IsAdjustByStaff.All(c => c == false))
                    {
                        return "Participant";
                    }
                    else if (IsAdjustByStaff.Any(c => c == true) && IsAdjustByStaff.Any(c => c == false))
                    {
                        return "Both";
                    }
                }

                return string.Empty;
            }
        }
        public string VendorNumber { get; set; }//Transaction receiver Vendor number
        public string VendorBusinessName { get; set; }//Transaction receiver Business name
        public string Zone { get; set; }
        public string PostalCode { get; set; }
        public string FSA
        {
            get
            {
                if (PostalCode != null)
                {
                    return PostalCode.Substring(0, 3);
                }
                return string.Empty;
            }
        }
        public double OnRoadWeight { get; set; }
        public double OffRoadWeight { get; set; }
        public long CreatedUserId { get; set; }
        public bool IsGenerateTires { get; set; }

        public List<bool> IsAdjustByStaff { get; set; }

        public List<TransactionItem> TransactionItems { get; set; }

        public int PLT
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.PLT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        public int MT
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.MT);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int AGLS
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.AGLS);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int IND
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.IND);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int SOTR
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.SOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int MOTR
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.MOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int LOTR
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.LOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }

        public int GOTR
        {
            get
            {
                var pltItem = TransactionItems.FirstOrDefault(c => c.Item.ShortName == TreadMarksConstants.GOTR);
                if (pltItem != null)
                {
                    return (int)pltItem.Quantity;
                }
                return 0;
            }
        }
        public string ProcessingStatus { get; set; }

        ///DOT
        public double ScaleWeight { get; set; }
        public double EstWeight { get; set; }

        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }
    }
}
