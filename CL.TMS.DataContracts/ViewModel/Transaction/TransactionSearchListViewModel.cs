﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionSearchListViewModel
    {
        public int Id { get; set; }
        public Guid TransactionId { get; set; }
        public string TypeCode { get; set; }       
        public long FriendlyNumber { get; set; }
        public string ProcessingStatus { get; set; }
    }
}
