﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionListViewModel : BaseDTO<int>
    {
        public string TransactionId { get; set; }
        public string DeviceName { get; set; }
        public int Badge { get; set; }
        public DateTime FormDate { get; set; }
        public string FormNumber { get; set; }
        public string IncRegNum { get; set; }
        public string IncRegName { get; set; }
        public string OutRegNum { get; set; }
        public string OutRegName { get; set; }
        public string CompanyName { get; set; }
        public string ScaleTicketNumber { get; set; }
        public string MarketLocation { get; set; }
        public int? TireMarketType { get; set; }
        public string InvoiceNumber { get; set; }
        public string Zone { get; set; }
        public string PostalCode { get; set; }
        public bool IsGenerateTires { get; set; }
        public decimal? OnRoadWeight { get; set; }
        public decimal? OffRoadWeight { get; set; }
        public decimal? TotalWeight { get; set; }
        public decimal? TotaEstimatedlWeight { get; set; }
        public decimal? Variance { get; set; }
        public bool MobileFormat { get; set; }
    }
}
