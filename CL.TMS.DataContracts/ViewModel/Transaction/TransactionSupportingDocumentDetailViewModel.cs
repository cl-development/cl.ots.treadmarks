﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionSupportingDocumentDetailViewModel
    {
        public bool Display { get; set; }
        public string SubmittedBy { get; set; }
        public List<TransactionViewSupportingDocumentAttachmentViewModel> DocumentList { get; set; }
    }
}
