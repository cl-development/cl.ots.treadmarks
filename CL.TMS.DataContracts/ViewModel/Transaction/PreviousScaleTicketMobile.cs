﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class PreviousScaleTicketMobile
    {
        public string FormType { get; set; }
        public VendorReference Inbound { get; set; }
        public VendorReference Outbound { get; set; }
        public string InboundScaleTicket { get; set; }
        public string OutboundScaleTicket { get; set; }
        public string SingleScaleTicket { get; set; }
        public int? PeriodType { get; set; }
        public string TransactionTypeCode { get; set; }
        public bool IsSingleScaleticket { get; set; }
    }
}
