﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionManageInternalNoteViewModel
    {
        public int TransactionId { get; set; }
        public string NewNote { get; set; }

        public List<InternalNoteViewModel> NoteList { get; set; }

        public TransactionManageInternalNoteViewModel()
        {
            NoteList = new List<InternalNoteViewModel>();
        }
    }
}
