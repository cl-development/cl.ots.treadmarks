﻿using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction.RPM
{
    public class RPMCommonTransactionListViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        public RPMCommonTransactionListViewModel()
        {
            this.TransactionListSecurity = new TransactionListSecurityViewModel();
        }
        public int TransactionId { get; set; }
        public string DeviceName { get; set; }
        public string Notes { get; set; }
        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.TransactionNotes)
                    foreach (TransactionNote item in this.TransactionNotes)
                    {
                        if (null != item)
                        {
                            sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        [JsonIgnore]
        public List<TransactionNote> TransactionNotes { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNbr { get; set; }
        public long TransactionFriendlyId { get; set; }
        public string TransNum { get {return TransactionFriendlyId.ToString(); } }//for Void Modal dialog to show Trans#
        public string ReviewStatus { get; set; }
        public string Adjustment
        {
            get
            {
                if ((null != IsAdjustByStaff) && (0 < IsAdjustByStaff.Count))
                {
                    if (IsAdjustByStaff.All(c => c == true))
                    {
                        return "Staff";
                    }
                    else if (IsAdjustByStaff.All(c => c == false))
                    {
                        return "Participant";
                    }
                    else if (IsAdjustByStaff.Any(c => c == true) && IsAdjustByStaff.Any(c => c == false))
                    {
                        return "Both";
                    }
                }

                return string.Empty;
            }
        }
        public string VendorNumber { get; set; }
        public string BusinessName { get; set; }
        public string Destination { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        [JsonIgnore]
        public List<bool> IsAdjustByStaff { get; set; }

        public decimal AmountDollar_Ton//TotalWeight is already Ton
        {
            get
            {
                return Math.Round(((Rate ?? 0) * (Math.Round(TotalWeight,4,MidpointRounding.AwayFromZero))), 2, MidpointRounding.AwayFromZero);
            }
        }
        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }

        ///RPM-SPS
        public string Description { get; set; }
        public decimal Percentage { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal? Rate { get; set; }
        public decimal? RetailPrice { get; set; }
        public bool MobileFormat { get; set; }

        public string MeshRange { get; set; }
        public long CreatedUserId { get; set; }
        //public List<TransactionItem> TransactionItems { get; set; }
        [JsonIgnore]
        public List<TransactionItemListDetailViewModel> TireCountList { get; set; }
    }
}
