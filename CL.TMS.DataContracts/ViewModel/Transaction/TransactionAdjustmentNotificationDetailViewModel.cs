﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionAdjustmentNotificationDetailViewModel
    {
        public string TransactionType { get; set; }
        public string TransactionNumber { get; set; }
        public string InitiatedBy { get; set; }
        public string ClaimPeriod { get; set; }
        public string Email { get; set; }
    }
}
