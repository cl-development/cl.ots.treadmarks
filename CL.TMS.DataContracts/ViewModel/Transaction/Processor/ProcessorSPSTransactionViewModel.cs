﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;

using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorSPSTransactionViewModel : ProcessorTransactionViewModel
    {
        public int? RPMNumber { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public string InvoiceNumber { get; set; }
        public ProcessorSPSProductViewModel ProductDetail { get; set; }
        public string SoldTo { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<TransactionProductViewModel> ProductList { get; set; }
        public List<string> CountryList { get; set; }

        public ProcessorSPSTransactionViewModel() : base()
        {            
        }   

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.SPS;
            FormDate = new TransactionFormDateViewModel();            
            TireTypeList = new List<TransactionTireTypeViewModel>();            
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Scale Ticket", DocumentFormat = 3, Required = true },
                new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = "Invoice", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();
            ProductDetail = new ProcessorSPSProductViewModel();
            ScaleTicket = new TransactionScaleTicketViewModel() { WeightType = TreadMarksConstants.Ton, WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg, TreadMarksConstants.Ton } };
            SoldTo = "registered";
            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "Canada" };

            ProductList = new List<TransactionProductViewModel>();
            CountryList = new List<string>();
        }
    }
}
