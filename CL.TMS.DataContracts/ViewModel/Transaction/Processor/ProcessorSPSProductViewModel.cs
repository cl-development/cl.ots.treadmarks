﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorSPSProductViewModel : TransactionProductBaseViewModel
    {        
        public string ProductCode { get; set; }
        public int? MeshRangeStart { get; set; }
        public int? MeshRangeEnd { get; set; }
        public bool? IsFreeOfSteel { get; set; }
        public string ProductDescription { get; set; }        
    }
}
