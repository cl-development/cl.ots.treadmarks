﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class DORTransactionAdjViewModel : TransactionAdjViewModel
    {
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public string InvoiceNumber { get; set; }
        public ProcessorDORProductViewModel ProductDetail { get; set; }
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }
        public string SoldTo { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<TypeDefinition> MaterialTypeList { get; set; }
        public List<TypeDefinition> DispositionReasonList { get; set; }
        public List<int> WeightTypeList { get; set; }
        public List<string> CountryList { get; set; }
        public ProcessorRateDetailViewModel RateDetail { get; set; }
    }
}
