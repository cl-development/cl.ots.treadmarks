﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class PITTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }
        public TransactionViewProductViewModel ProductDetail { get; set; }

        public PITTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = null;
        }
    }
}
