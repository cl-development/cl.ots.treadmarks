﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewSignatureViewModel
    {
        public bool IncomingExists { get; set; }
        public string IncomingLabel { get; set; }
        public string IncomingName { get; set; }
        public string IncomingSrc { get; set; }

        public bool OutgoingExists { get; set; }
        public string OutgoingLabel { get; set; }
        public string OutgoingName { get; set; }
        public string OutgoingSrc { get; set; }        
    }
}
