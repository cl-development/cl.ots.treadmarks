﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewSupportingDocumentAttachmentViewModel
    {
        public int AttachmentId { get; set; }
        public string Src { get; set; }
        public string ActualSrc { get; set; }
        public string FileName { get; set; }        
        public string Description { get; set; }
        public bool IsRemoved { get; set; }
        public string OriginalDescription { get; set; }
    }
}
