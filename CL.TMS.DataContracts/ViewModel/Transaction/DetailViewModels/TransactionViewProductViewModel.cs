﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewProductViewModel
    {
        public string ShortName { get; set; }
        public string Name { get; set; } 
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }

        public int? MeshRangeStart { get; set; }
        public int? MeshRangeEnd { get; set; }
        public bool? IsFreeOfSteel { get; set; }

        public Decimal? RecyclablePercentage { get; set; }
        public Decimal? TotalTDPWeight { get; set; }

        public Decimal? Rate { get; set; }
        public Decimal? Amount { get; set; }
        public decimal? RetailPrice { get; set; }
    }
}
