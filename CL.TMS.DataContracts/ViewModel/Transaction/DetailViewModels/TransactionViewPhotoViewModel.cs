﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewPhotoViewModel
    {
        public string Src { get; set; }
        public string Comment { get; set; }     
    }
}
