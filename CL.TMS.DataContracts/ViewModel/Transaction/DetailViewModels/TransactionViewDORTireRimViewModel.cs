﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewDORTireRimViewModel
    {
        public string PTRNumber { get; set; }
        public decimal? OnRoadScaleWeight { get; set; }
        public decimal? OffRoadScaleWeight { get; set; }
        public Decimal? OnRoadRate { get; set; }
        public Decimal? OffRoadRate { get; set; }
        public Decimal? Amount { get; set; }
    }
}
