﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class StaffCategoryFormViewModel:BaseDTO<int>
    {
        public string CategoryName { get; set; }
        
        public string InternalNote { get; set; }

        public DateTime InternalNoteAddedOn { get; set; }

        public string InternalNoteAddedBy { get; set; }

        public long UserId { get; set; }
    }
}
