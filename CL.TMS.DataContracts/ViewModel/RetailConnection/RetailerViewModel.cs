﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class RetailerViewModel: BaseDTO<int>
    {
        public DateTime? ApprovedDate { get; set; }
        public string Status { get; set; }
        public string RetailerName { get; set; }
        public string AddedBy { get; set; }
        public bool IsAddedByParticipant { get; set; }        
        public string[] StaffNotesAllText
        {
            get
            {
                var notes = new List<string>();
                if (this.StaffRetailerNotes != null)
                    foreach (RetailerNote item in this.StaffRetailerNotes)
                    {
                        if (item != null)
                        {
                            notes.Add(string.Format("- {0} <hr>", item.Note));
                        }
                    }
                return notes.ToArray();
            }
        }

        [JsonIgnore]
        public List<RetailerNote> StaffRetailerNotes { get; set; }
    }
}
