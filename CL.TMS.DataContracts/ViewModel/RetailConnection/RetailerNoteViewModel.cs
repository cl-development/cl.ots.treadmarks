﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class RetailerNoteViewModel
    {
        public int Id { get; set; }
        public DateTime AddedOn { get; set; }
        public string Note { get; set; }
        public string AddedBy { get; set; }
        public int RetailerId { get; set; }
    }
}
