﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using FluentValidation;
using CL.TMS.DataContracts.ViewModel.EmailSettings;

namespace CL.TMS.DataContracts.Validation
{
    public class EmailSettingsGeneralModelValidator : AbstractValidator<EmailSettingsGeneralVM>
    {
        public EmailSettingsGeneralModelValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
                default:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
            }
        }

        private void BuildOTSValidationRules()
        {
            RuleFor(c => c.Email_BccApplicationPath).NotNull().When(c => c.Email_CBCCEmailClaimAndApplication == true).WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.Email_StewardApplicationApproveBCCEmailAddr).NotNull().When(c => c.Email_CBCCEmailStewardAndRemittance == true).WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.Email_defaultFrom).NotNull().When(c => c.Email_CBDefaultFromEmailAddr == true).WithMessage(MessageResource.RequiredFieldMissing);               
            RuleFor(c => c.Email_BccApplicationPath).Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$").WithMessage(MessageResource.ValidationMsgInvalidEmail);
            RuleFor(c => c.Email_StewardApplicationApproveBCCEmailAddr).Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$").WithMessage(MessageResource.ValidationMsgInvalidEmail);
            RuleFor(c => c.Email_defaultFrom).Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$").WithMessage(MessageResource.ValidationMsgInvalidEmail);         
        }
    }
}
