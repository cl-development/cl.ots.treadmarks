﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpiBatch")]
    public class GpiBatch : BaseDTO<int>
    {
        public string GpiTypeID { get; set; }
        public int GpiBatchCount { get; set; }
        public string GpiStatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string Message { get; set; }
        public virtual ICollection<GpiBatchEntry> GpiBatchEntries { get; set; }

        public void PrependMessage(string message, string userId)
        {
            this.Message = string.Format("[{0}] ({2}) {1}\r\n{3}",
                DateTime.Now,
                message,
                userId,
                this.Message);

        }

    }

}
