﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Activity")]
    public class Activity : BaseDTO<int>
    {
        public string Message { get; set; }
        public DateTime CreatedTime { get; set; }
        public string Initiator { get; set; }
        public string InitiatorName { get; set; }
        public string Assignee { get; set; }
        public string AssigneeName { get; set; }
        public int ActivityType { get; set; }
        public string ActivityArea { get; set; }
        public int ObjectId { get; set; }
    }
}
