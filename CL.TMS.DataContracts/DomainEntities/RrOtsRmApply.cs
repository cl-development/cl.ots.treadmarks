﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsRmApply")]
    public class RrOtsRmApply
    {
        [Key, Column(Order = 0)]
        public string APTODCNM { get; set; }
        [Key, Column(Order = 1)]    
        public string APFRDCNM { get; set; }
    
        public decimal APPTOAMT { get; set; }
        [Key, Column(Order = 2)]    
        public short APFRDCTY { get; set; }
        [Key, Column(Order = 3)]    
        public short APTODCTY { get; set; }
    
        public DateTime? APPLYDATE { get; set; }
    
        public DateTime? GLPOSTDT { get; set; }
    
        public string USERDEF1 { get; set; }
    
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 4)]    
        public string INTERID { get; set; }
    
        public int? INTSTATUS { get; set; }
    
        public DateTime? INTDATE { get; set; }
    
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]    
        public int DEX_ROW_ID { get; set; }
       
    }

}
