﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ItemRecoverableMaterial")]
    public class ItemRecoverableMaterial : BaseDTO<int>
    {
        public int? MaterialID { get; set; }
        public int? ItemID { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long?  ModifiedByID { get; set; }
        [ForeignKey("MaterialID")]
        public virtual RecoverableMaterial RecoverableMaterial { get; set; }
        [ForeignKey("ItemID")]
        public virtual Item Item { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User User { get; set; }
    }
}
