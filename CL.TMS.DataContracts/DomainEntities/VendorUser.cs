﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorUser")]
    public class VendorUser : BaseDTO<int>
    {
        public int VendorID { get; set; }
        public long UserID { get; set; }

        public int RoleId { get; set; }

        public bool IsActive { get; set; }

    }
}
