﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("SystemActivity")]
    public class SystemActivity : BaseDTO<int>
    {
        public string Message { get; set; }
        public string Category { get; set; }
        public int ActivityId { get; set; }
        public long UserId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
