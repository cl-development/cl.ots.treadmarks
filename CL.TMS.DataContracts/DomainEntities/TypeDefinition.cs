﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TypeDefinition")]
    public class TypeDefinition:BaseDTO<int>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int DefinitionValue { get; set; }
        public int DisplayOrder { get; set; }
    }
}
