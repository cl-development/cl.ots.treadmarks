﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("CategoryNote")]
    public class CategoryNote:BaseDTO<int>
    {
        public int CategoryId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User AddedBy { get; set; }

        [ForeignKey("CategoryId")]
        public ProductCategory ProductCategory { get; set; }
    }
}
