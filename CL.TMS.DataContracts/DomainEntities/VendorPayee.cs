using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using System;

namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("VendorPayee")]
    public class VendorPayee : BaseDTO<int>
    {       
        public int PayeeListId { get; set; }       
        public int VendorId { get; set; }
       // public string RegistrationNumber { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }
        [ForeignKey("PayeeListId")]
        public virtual PayeeList PayeeList { get; set; }

        [NotMapped]
        public DateTime? EffectiveStartDate
        {
            get { return PayeeList.EffectiveStartDate; }
        }
        [NotMapped]
        public DateTime? EffectiveEndDate
        {
            get { return PayeeList.EffectiveEndDate; }
        }
    }
}
