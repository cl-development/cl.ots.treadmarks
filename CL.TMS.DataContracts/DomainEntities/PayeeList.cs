﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("PayeeList")]
    public class PayeeList:BaseDTO<int>
    {
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }

        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }

        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }
     
        public virtual List<VendorPayee> VendorPayees { get; set; }
        public virtual List<PayeeListNote> TIPayeeNotes { get; set; }
    }
}
