﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("AppResource")]
    public class AppResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ResourceMenu { get; set; }
        public string ResourceScreen { get; set; }
        public int ResourceLevel { get; set; }
        public int DisplayOrder { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ApplicableLevel { get; set; }
    }
}
