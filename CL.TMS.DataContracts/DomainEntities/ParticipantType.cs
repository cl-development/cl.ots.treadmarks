﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ParticipantType")]
    public class ParticipantType : BaseDTO<int>
    {  
        public string Name { get; set; }
        public string Description { get; set; }
        public string SubType { get; set; }
        public bool IsVendor { get; set; }
        public int? SetupCheckList { get; set; }      
    }
}
