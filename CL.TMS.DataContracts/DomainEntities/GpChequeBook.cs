﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpChequeBook")]
    public class GpChequeBook
    {
        [Key]
        public string account_number { get; set; }
        public string name { get; set; }
        public short is_default { get; set; }
    }
}
