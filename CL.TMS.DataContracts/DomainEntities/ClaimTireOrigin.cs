﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimTireOrigin")]
    public class ClaimTireOrigin:BaseDTO<int>
    {
        [ForeignKey("Claim")]
        public int ClaimId { get; set; }
        public int TireOriginValue { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public decimal AverageWeight { get; set; }
        public decimal ActualWeight { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UnitType { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual Item Item { get; set; }

    }
}
