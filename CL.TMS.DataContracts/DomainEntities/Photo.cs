﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Photo")]
    public class Photo : BaseDTO<int>
    {
        public Photo()
        {
        }
        public string Comments { get; set; }
        public int PhotoType { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string FileName { get; set; }      
    }
}
