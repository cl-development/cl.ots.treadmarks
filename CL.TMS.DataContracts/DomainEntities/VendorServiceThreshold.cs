﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorServiceThreshold")]
    public class VendorServiceThreshold : BaseDTO<int>
    {
        public int VendorID { get; set; }
        public int ThresholdDays { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }

        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }
        [ForeignKey("VendorID")]
        public virtual Vendor Vendor { get; set; }
    }
}
