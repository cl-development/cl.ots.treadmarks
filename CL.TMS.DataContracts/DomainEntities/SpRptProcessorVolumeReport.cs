﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptProcessorVolumeReport
    {
        public DateTime ReportingPeriod‎ { get; set; }
        public string RegNo‎ { get; set; }
        public string RateType { get; set; } //OTSTM2-1225
        public string RegName‎ { get; set; }
        public string Status‎ { get; set; }
        [Display(Name = "Record Status‎")]
        public string RecordStatus { get; set; }
        public string Product‎ { get; set; }
        public string Destination‎ { get; set; }
        public string Description‎ { get; set; }
        //Needs 4 decimal places as per requirements
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public decimal Weight‎ { get; set; }
        [DisplayFormat(DataFormatString="{0:C}")]
        public decimal Amount‎ { get; set; }
    }
}
