﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;



namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("AppSetting")]
    public class Setting : BaseDTO<int>
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
