﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("VendorSupportingDocument")]
    public class VendorSupportingDocument : BaseDTO<int>
    {
        public int VendorID { get; set; }
        public int TypeID { get; set; }
        public string Option { get; set; }

        [ForeignKey("VendorID")]
        public virtual Vendor Vendor { get; set; }
    }
}
