﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Role")]
    public class Role : BaseDTO<int>
    {
        public Role()
        {
            Users = new List<User>();
            RolePermissions = new List<RolePermission>(); //new added
            VendorUsers = new List<VendorUser>(); //new added
        }

        public string Name { get; set; }
        public int RoleType { get; set; }
        public string CreateBy { get; set; } //new added
        public DateTime CreateDate { get; set; } //new added
        public DateTime UpdateDate { get; set; } //new added

        public virtual List<User> Users { get; set; }

        public virtual List<RolePermission> RolePermissions { get; set; } //new added
        public virtual List<VendorUser> VendorUsers { get; set; } //new added
    }
}
