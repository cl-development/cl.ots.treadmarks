﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionNote")]
    public class TransactionNote : BaseDTO<int>
    {
        public int TransactionId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UserId { get; set; }
        public string NoteType { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
