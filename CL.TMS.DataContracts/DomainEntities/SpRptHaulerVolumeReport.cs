﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptHaulerVolumeReport
    {
        public string ReportingPeriod { get; set; }
        public string HaulerRegistrationNumber { get; set; }
        public bool HaulerStatus { get; set; }
        public string DestinationType { get; set; }
        public string ProcessorHaulerRegistrationNumber { get; set; }
        public bool Status { get; set; }
        public string RecordState { get; set; }
        public string UsedTireDestionation { get; set; }
        public Nullable<decimal> TotalActualWeight { get; set; }
        public Nullable<decimal> TotalEstWeight { get; set; }
        public Nullable<decimal> PltEstWeight { get; set; }
        public Nullable<decimal> MtEstWeight { get; set; }
        public Nullable<decimal> AglsEstWeight { get; set; }
        public Nullable<decimal> IndEstWeight { get; set; }
        public Nullable<decimal> SotrEstWeight { get; set; }
        public Nullable<decimal> MotrEstWeight { get; set; }
        public Nullable<decimal> LotrEstWeight { get; set; }
        public Nullable<decimal> GotrEstWeight { get; set; }
        public Nullable<decimal> TotalPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> PltPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> MtPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> AglsPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> IndPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> SotrPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> MotrPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> LotrPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> GotrPaymentEarnedProcessor { get; set; }
        public Nullable<decimal> TotalPaymentEarnedHauler { get; set; }
        public Nullable<decimal> PltPaymentEarnedHauler { get; set; }
        public Nullable<decimal> MtPaymentEarnedHauler { get; set; }
        public Nullable<decimal> AglsPaymentEarnedHauler { get; set; }
        public Nullable<decimal> IndPaymentEarnedHauler { get; set; }
        public Nullable<decimal> SotrPaymentEarnedHauler { get; set; }
        public Nullable<decimal> MotrPaymentEarnedHauler { get; set; }
        public Nullable<decimal> LotrPaymentEarnedHauler { get; set; }
        public Nullable<decimal> GotrPaymentEarnedHauler { get; set; }
        public Nullable<decimal> Hst { get; set; }
        public Nullable<decimal> TotalPaymentEarnedWithHst { get; set; }
    }
}
