﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Country")]
    public class Country : BaseDTO<int>
    {
        public string Name { get; set; }
        public string Alpha2Code { get; set; }
        public string NumericCode { get; set; }
    }
}
