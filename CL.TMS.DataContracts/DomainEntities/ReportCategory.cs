﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ReportCategory")]
    public class ReportCategory //: BaseDTO<int>
    {

        public ReportCategory()
        {
            this.Reports = new HashSet<Report>();
        }

        public int ID { get; set; }
        public string ReportCategoryName { get; set; }
        public virtual ICollection<Report> Reports { get; set; }

    }
}
