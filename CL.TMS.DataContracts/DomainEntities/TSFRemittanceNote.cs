﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TSFRemittanceNote")]
    public class TSFRemittanceNote : BaseDTO<int>
    {
        public int TSFClaimId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UserId { get; set; }
     //   public virtual TSFClaim TSFClaim { get; set; } 
  
    }
}
