﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Notification")]
    public class Notification:BaseDTO<int>
    {
        public string Message { get; set; }
        public DateTime CreatedTime { get; set; }
        public string Assigner { get; set; }
        public string AssignerName { get; set; }
        public string AssignerInitial { get; set; }
        public string Assignee { get; set; }
        public string AssigneeName { get; set; }
        public int NotificationType { get; set; }
        public string NotificationArea { get; set; }
        public int ObjectId { get; set; }
        public string Status { get; set; }

        [NotMapped]
        public string TimeSpanMessage
        {
            get
            {
                var timeSpan = DateTime.Now - CreatedTime;
                var minutesInterval = timeSpan.TotalMinutes;
                var hourInterval = timeSpan.TotalHours;
                var dayInterval = timeSpan.TotalDays;
                if (minutesInterval< 60)
                {
                    if (minutesInterval<=1)
                        return string.Format("{0} minute ago", 1);
                    else
                        return string.Format("{0} minutes ago", timeSpan.Minutes);
                }
                else
                {
                    if (hourInterval<24)
                    {
                        if (hourInterval <= 1)
                            return string.Format("{0} hour ago", 1);
                        else
                            return string.Format("{0} hours ago", timeSpan.Hours);
                    }
                    else
                    {
                        if (dayInterval <= 1)
                            return string.Format("{0} day ago", 1);
                        else
                            return string.Format("{0} days ago", timeSpan.Days);
                    }
                }
            }
        }
    }
}
