﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimInventoryAdjustItem")]
    public class ClaimInventoryAdjustItem : BaseDTO<int>
    {
        [ForeignKey("ClaimInventoryAdjustment")]
        public int ClaimInventoryAdjustmentId { get; set; }
        public int ItemId { get; set; }
        public int QtyAdjustment { get; set; }
        public int? QtyBeforeAdjustment { get; set; }
        public int? QtyAfterAdjustment { get; set; }
        public decimal WeightAdjustment { get; set; }

        public virtual ClaimInventoryAdjustment ClaimInventoryAdjustment { get; set; }
    }
}
