﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Retailer")]
    public class Retailer:BaseDTO<int>
    {
        public Retailer()
        {
            RetailerNotes = new List<RetailerNote>();
        }
        public string Name { get; set; }
        public string Status { get; set; }
        public bool IsOnline { get; set; }
        public string Url { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? AddressId { get; set; }

        public bool IsAddedByParticipant { get; set; }

        public long AddedBy { get; set; }
        [ForeignKey("AddedBy")]
        public virtual User AddedByUser { get; set; }
        public int? VendorId { get; set; }

        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address RetailerAddress { get; set; }

        public virtual List<RetailerNote> RetailerNotes { get; set; }

    }
}
