﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RetailerProduct")]
    public class RetailerProduct:BaseDTO<int>
    {
        public int RetailerId { get; set; }
        public int ProductId { get; set; }
        public string ProductUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public long AddedBy { get; set; }
        [ForeignKey("AddedBy")]
        public virtual User AddedByUser { get; set; }
        [ForeignKey("RetailerId")]
        public virtual Retailer Retailer { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}
