﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    public class SpRptTsfExtractInBatchOnlyReportGp
    {
        public int BatchNumber { get; set; }
        public string BatchDesc { get; set; }
        public int TsfClaimSummaryId { get; set; }
        public System.DateTime ReportingPeriod { get; set; }
        public string RegistrationNumber { get; set; }
        public Nullable<System.DateTime> ReceiptDate { get; set; }
        public Nullable<System.DateTime> DepositDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public string ChequeNumber { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> ChequeAmount { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        //public Nullable<decimal> TSF_Before_HST { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}")]
        public Nullable<decimal> totalRemittancePayable { get; set; }
        //public string GpiStatusID { get; set; }
    }
}
