﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("GpsLog")]
    public class GpsLog : BaseDTO<int>
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime? SyncDate { get; set; }
    }
}
