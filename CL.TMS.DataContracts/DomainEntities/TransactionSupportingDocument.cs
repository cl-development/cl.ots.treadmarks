﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionSupportingDocument")]
    public class TransactionSupportingDocument:BaseDTO<int>
    {
        public int TransactionId { get; set; }
        public string DocumentName { get; set; }
        public int DocumentFormat { get; set; }    
        [ForeignKey("TransactionId")]
        public virtual Transaction Transaction { get; set; }
        public int? TransactionAdjustmentId { get; set; }
    }
}
