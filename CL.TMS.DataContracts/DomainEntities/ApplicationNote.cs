﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ApplicationNote")]
    public class ApplicationNote : BaseDTO<int>
    {
        public int? ApplicationId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserId { get; set; }
        public int? VendorId { get; set; }
        public int? CustomerId { get; set; }
    }
}
