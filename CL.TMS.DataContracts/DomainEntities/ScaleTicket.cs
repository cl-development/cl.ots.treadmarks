﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ScaleTicket")]
    public class ScaleTicket : BaseDTO<int>
    {
        public Decimal? InboundWeight { get; set; }
        public Decimal? OutboundWeight { get; set; }
        public string TicketNumber { get; set; }
        public int SortIndex { get; set; }
        public DateTime CreateDate { get; set; }
        public int PhotoId { get; set; }
        public int ScaleTicketType { get; set; }
        public int TransactionId { get; set; }
        public int UnitType { get; set; }
        public int? TransactionAdjustmentId { get; set; }        
    }
}
