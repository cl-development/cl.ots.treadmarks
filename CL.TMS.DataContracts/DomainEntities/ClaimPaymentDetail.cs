﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimPaymentDetail")]
    public class ClaimPaymentDetail: BaseDTO<int>
    {
        public ClaimPaymentDetail()
        {
            TransactionId = 0;
            Quantity = 0;
            ActualWeight = 0;
            AverageWeight = 0;
        }
        public int ClaimId { get; set; }
        public int TransactionId { get; set; }
        public int ItemId { get; set; }
        public int ItemType { get; set; }
        public string ItemName { get; set; }
        public decimal ActualWeight { get; set; }
        public decimal AverageWeight { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public int UnitType { get; set; }
        public decimal Rate { get; set; }
        public int PaymentType { get; set; }
        public string TransactionTypeCode { get; set; }
        public int ClaimPeriodId { get; set; }
        public string PeriodName { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public int VendorId { get; set; }
        public int VendorType { get; set; }
        public string BusinessName { get; set; }
        public string RegistrationNumber { get; set; } 
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// From Transaction Item table, which is only applicable for certain type of transaction such as RPMSPS
        /// </summary>
        public string ItemDescription { get; set; }
        /// <summary>
        /// From Transaction Item table, which is only applicable for certain type of transaction such as RPMSPS
        /// </summary>
        public string ItemCode { get; set; }
    }
}
