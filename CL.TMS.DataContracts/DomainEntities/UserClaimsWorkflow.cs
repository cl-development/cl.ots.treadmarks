﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("UserClaimsWorkflow")]
    public class UserClaimsWorkflow
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public string AccountName { get; set; }
        public string Workflow { get; set; }
        public bool ActionGear { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
