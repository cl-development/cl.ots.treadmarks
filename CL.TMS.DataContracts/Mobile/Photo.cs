
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("Photo")]
    public class Photo:BaseDTO<Guid>
    {
        public string Comments { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? Date { get; set; }
        public string FileName { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public DateTime? SyncDate { get; set; }
        public Guid TransactionId { get; set; }
        public int PhotoTypeId { get; set; }
        public string UniqueFileName { get; set; }

        public virtual Transaction Transaction { get; set; }

        public virtual ICollection<ScaleTicket> ScaleTickets { get; set; }

    }
}
