
using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{

    [Table("ScaleTicket")]
    public class ScaleTicket:BaseDTO<Guid>
    {
        public DateTime Date { get; set; }
        public decimal? InboundWeight { get; set; }
        public decimal? OutboundWeight { get; set; }
        public string TicketNumber { get; set; }
        public long SortIndex { get; set; }
        public DateTime? SyncDate { get; set; }
        public Guid PhotoId { get; set; }
        public long ScaleTicketTypeId { get; set; }
        public Guid TransactionId { get; set; }
        public long UnitTypeId { get; set; }


        public virtual ScaleTicketType ScaleTicketType { get; set; }
        public virtual Photo Photo { get; set; }
        public Transaction Transaction { get; set; }


    }
}
