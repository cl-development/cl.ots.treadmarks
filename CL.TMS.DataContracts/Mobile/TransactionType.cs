﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("TransactionType")]
    public class TransactionType:BaseDTO<long>
    {
        public string NameKey { get; set; }
        public string ShortNameKey { get; set; }
        public string FileName { get; set; }
        public int SortIndex { get; set; }
        public string OutgoingSignatureKey { get; set; }
        public string IncomingSignatureKey { get; set; }
        public DateTime? SyncDate { get; set; }
        public string TireCountMessageKey { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}
