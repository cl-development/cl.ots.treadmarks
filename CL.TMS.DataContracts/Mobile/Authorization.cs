using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("Authorization")]
    public class Authorization:BaseDTO<Guid>
    {
        public string AuthorizationNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public long UserId { get; set; }
        public Guid? TransactionId { get; set; }
        public long TransactionTypeId { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

    }
}
