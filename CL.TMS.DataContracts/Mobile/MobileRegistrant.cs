
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{
    [Table("Registrant")]
    public class MobileRegistrant:BaseDTO<decimal>
    {
        public string BusinessName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Metadata { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public Guid? LocationId { get; set; }
        public long RegistrantTypeId { get; set; }
        public DateTime? ActivationDate { get; set; }
        public DateTime? ActiveStateChangeDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
