
using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.Mobile
{

    [Table("Transaction_TireType")]
    public class TransactionTireType:BaseDTO<Guid>
    {
        public DateTime? SyncDate { get; set; }
        public long Quantity { get; set; }
        public long TireTypeId { get; set; }
        public Guid TransactionId { get; set; }

        public virtual Transaction Transaction { get; set; }

        public virtual TireType TireType { get; set; }


    }
}
