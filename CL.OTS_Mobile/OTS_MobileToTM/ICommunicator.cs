﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using otscontracts;
using OTS_MobileToTM.CommunicationSrv;

namespace OTS_MobileToTM
{
    public interface ICommunicator
    {
        IEnumerable<AppUserModel> GetUpdatedAppUsersList(DateTime lastUpdated);
        IEnumerable<VendorModel> GetUpdatedVendorsList(DateTime lastUpdated);    
      
    }
}
