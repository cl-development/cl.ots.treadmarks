﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WS = OTS_MobileToTM.HaulerServiceReference;
using OTS_MobileToTM.CommunicationSrv;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;
using otscontracts.Common.Enums;

namespace OTS_MobileToTM.Controllers
{
    public class TiresReceivedSpecialTireCollectionEventsController : ITiresReceivedSpecialTireCollectionEventsController
    {

        #region Properties

        private WS.HaulerCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }

        #endregion

        #region Constructor

        public TiresReceivedSpecialTireCollectionEventsController()
        {
            new TiresReceivedSpecialTireCollectionEventsController(string.Empty, string.Empty);
        }

        public TiresReceivedSpecialTireCollectionEventsController(string username, string password)
        {
            client = new WS.HaulerCommunicationClient();
            cred = new  OTS_MobileToTM.ServiceCredentials();
            cred.HaulerServiceCredentials.UserName = username;
            cred.HaulerServiceCredentials.Password = password;               
        }

        public void Close()
        {
            client.Close();
        }

        #endregion

        #region Methods

        public IResponseMessage<object> Insert(ITransactionModelExtended transaction, int haulerSummaryId)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                ICollection<ITransaction_TireTypeModel> tireTypeList = transaction.Transaction_TireTypes;

                WS.TiresReceivedSpecialTireCollectionEventsSrvModel tr = new WS.TiresReceivedSpecialTireCollectionEventsSrvModel();
                tr.EventNumber = Convert.ToUInt32(transaction.Authorization.authorizationNumber);
                tr.NameOfGroupOrIndividual = transaction.STCLocation.Name;

                var agls = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.AGLS)).SingleOrDefault();
                tr.AGLSCount = Convert.ToInt32(agls == null ? 0 : agls.Quantity);
                tr.ClaimId = haulerSummaryId;
                //r.CreatedDate = transaction.CreatedDate; // is set to date.now in TM
                tr.CreatedUser = Convert.ToString(transaction.CreatedUserId);
                tr.DateCollected = new DateTime(transaction.TransactionDate.Year, transaction.TransactionDate.Month, transaction.TransactionDate.Day);
                var gotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.GOTR)).SingleOrDefault();
                tr.GOTRCount = Convert.ToInt32(gotr == null ? 0 : gotr.Quantity);
                var ind = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.IND)).SingleOrDefault();
                tr.INDCount = Convert.ToInt32(ind.Quantity);
                var lotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.LOTR)).SingleOrDefault();
                tr.LOTRCount = Convert.ToInt32(lotr == null ? 0 : lotr.Quantity);
                tr.ModifiedDate = null;
                tr.ModifiedUser = null;

                var motr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MOTR)).SingleOrDefault();
                tr.MOTRCount = Convert.ToInt32(motr == null ? 0 : motr.Quantity);
                var mt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MT)).SingleOrDefault();
                tr.MTCount = Convert.ToInt32(mt == null ? 0 : mt.Quantity);
                var plt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.PLT)).SingleOrDefault();
                tr.PLTCount  = Convert.ToInt32(plt == null ? 0 : plt.Quantity);
                tr.PostalCode1 = transaction.PostalCode1;
                tr.PostalCode2 = transaction.PostalCode2;
                tr.Rate = null;
                tr.RateMLt = null;
                var sotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.SOTR)).SingleOrDefault();
                tr.SOTRCount = Convert.ToInt32(sotr == null ? 0 : sotr.Quantity);
                tr.STcFormNumber = transaction.FriendlyId;
                tr.DeviceName = transaction.DeviceName;
                WS.TiresReceivedSpecialTireCollectionEventsResponse response = (WS.TiresReceivedSpecialTireCollectionEventsResponse)(client.CreateTiresReceivedSpecialTireCollectionEventsRecord(tr, tr.CreatedUser, cred.HaulerServiceCredentials));
                
                List<object> t = new List<object>();
                foreach (var item in response.ResponseContent)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.SingleEntity;


            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Failed in TiresReceivedSpecialTireCollectionEventsController, Exception: {0}", e.Message) };
            }
            return result;
        }

        public IResponseMessage<object> Delete(int claimId, int tiresReceivedSpecialTireCollectionEventsId)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                WS.TiresReceivedSpecialTireCollectionEventsResponse response = (WS.TiresReceivedSpecialTireCollectionEventsResponse)(client.DeleteTiresReceivedSpecialTireCollectionEventsRecord(claimId, tiresReceivedSpecialTireCollectionEventsId, "mobile", cred.HaulerServiceCredentials));
                object[] tmp = response.ResponseContent;
                List<object> t = new List<object>();
                foreach (var item in tmp)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.SingleEntity;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = false;
            }
            return result;
        }

        #endregion

    }
}
