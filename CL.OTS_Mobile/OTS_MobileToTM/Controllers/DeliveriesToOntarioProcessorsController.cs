﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WS = OTS_MobileToTM.HaulerServiceReference;
using OTS_MobileToTM.CommunicationSrv;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;
using otscontracts.Common.Enums;

namespace OTS_MobileToTM.Controllers
{
    public class DeliveriesToOntarioProcessorsController : IDeliveriesToOntarioProcessorsController
    {

        #region Properties

        private WS.HaulerCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }

        #endregion

        #region Constructor

        public DeliveriesToOntarioProcessorsController()
        {
            new TiresReceivedSpecialTireCollectionEventsController(string.Empty, string.Empty);
        }

        public DeliveriesToOntarioProcessorsController(string username, string password)
        {
            client = new WS.HaulerCommunicationClient();
            cred = new  OTS_MobileToTM.ServiceCredentials();
            cred.HaulerServiceCredentials.UserName = username;
            cred.HaulerServiceCredentials.Password = password;               
        }

        public void Close()
        {
            client.Close();
        }

        #endregion

        #region Methods

        public IResponseMessage<object> Insert(ITransactionModelExtended transaction, int haulerSummaryId)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                ICollection<ITransaction_TireTypeModel> tireTypeList = transaction.Transaction_TireTypes;

                WS.DeliveriesToOntarioProcessorsModel dop = new WS.DeliveriesToOntarioProcessorsModel();
                dop.ProcessorNumber = transaction.OutgoingRegistrationNumber;
                
                var agls = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.AGLS)).SingleOrDefault();
                dop.AGLS = Convert.ToInt32(agls == null ? 0 : agls.Quantity);
                dop.ClaimID = haulerSummaryId;
                dop.DateDelivered = new DateTime(transaction.TransactionDate.Year, transaction.TransactionDate.Month, transaction.TransactionDate.Day);
                var gotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.GOTR)).SingleOrDefault();
                dop.GOTR = Convert.ToInt32(gotr == null ? 0 : gotr.Quantity);
                var ind = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.IND)).SingleOrDefault();
                dop.IND = Convert.ToInt32(ind.Quantity);
                var lotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.LOTR)).SingleOrDefault();
                dop.LOTR = Convert.ToInt32(lotr == null ? 0 : lotr.Quantity);
                
                var motr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MOTR)).SingleOrDefault();
                dop.MOTR = Convert.ToInt32(motr == null ? 0 : motr.Quantity);
                var mt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MT)).SingleOrDefault();
                dop.MT = Convert.ToInt32(mt == null ? 0 : mt.Quantity);
                var plt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.PLT)).SingleOrDefault();
                dop.PLT  = Convert.ToInt32(plt == null ? 0 : plt.Quantity);
                var sotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.SOTR)).SingleOrDefault();
                dop.SOTR = Convert.ToInt32(sotr == null ? 0 : sotr.Quantity);
                dop.ScaleTicketNumber = transaction.InboundScaleTicket.TicketNumber + "/" + transaction.OutboundScaleTicket.TicketNumber;
                dop.PtrFormNumber = transaction.FriendlyId.ToString();
                dop.ScaleWeight = Math.Abs((decimal)(transaction.InboundScaleTicket.InboundWeight - transaction.OutboundScaleTicket.OutboundWeight));
                dop.DeviceName = transaction.DeviceName;
                WS.DeliveriesToOntarioProcessorsSaveResponse response = (WS.DeliveriesToOntarioProcessorsSaveResponse)(client.SaveDeliveryToOntarioProcessor(dop, "mobile", cred.HaulerServiceCredentials));
                
                List<object> t = new List<object>();
                foreach (var item in response.ResponseContent)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.NewClaimID;

            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
            }
            return result;
        }

        public IResponseMessage<object> Delete(int deliveriesToOntarioProcessorsId)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                WS.DeliveriesToOntarioProcessorsSaveResponse response = (WS.DeliveriesToOntarioProcessorsSaveResponse)(client.DeleteDeliveryToOntarioProcessors(deliveriesToOntarioProcessorsId, cred.HaulerServiceCredentials));
                bool[] tmp = response.ResponseContent;
                List<object> t = new List<object>();
                foreach (var item in tmp)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.SingleEntity;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = false;
            }
            return result;
        }

        #endregion

    }
}
