﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WS = OTS_MobileToTM.HaulerServiceReference;
using OTS_MobileToTM.CommunicationSrv;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;
using otscontracts.Common.Enums;

namespace OTS_MobileToTM.Controllers
{
    public class ReceivedFromCollectorsController : IReceivedFromCollectorsController
    {

        #region Properties

        private WS.HaulerCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }

        #endregion

        #region Constructor

        public ReceivedFromCollectorsController()
        {
            new ReceivedFromCollectorsController(string.Empty, string.Empty);
        }

        public ReceivedFromCollectorsController(string username, string password)
        {
            client = new WS.HaulerCommunicationClient();
            cred = new OTS_MobileToTM.ServiceCredentials();
            cred.HaulerServiceCredentials.UserName = username;
            cred.HaulerServiceCredentials.Password = password;               
        }

        #endregion

        #region Methods

        public IResponseMessage<object> Insert(ITransactionModelExtended transaction, int haulerSummaryId)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                ICollection<ITransaction_TireTypeModel> tireTypeList = transaction.Transaction_TireTypes;
                
                decimal collectorNumber = transaction.OutgoingRegistrationNumber;
                WS.ReceivedFromCollectorsModel r = new WS.ReceivedFromCollectorsModel();
                var agls = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.AGLS)).SingleOrDefault();
                r.AGLS = Convert.ToInt32(agls == null ? 0 : agls.Quantity);
                r.Approved = false;
                r.ClaimAmount = null;
                r.ClaimAmountOrig = null;
                r.ClaimID = haulerSummaryId;
                r.CollectorCity = null;
                r.CollectorName = null;
                r.CollectorNumber = Convert.ToString(collectorNumber);
                r.Comments = transaction.OtsComments;
                //r.CreatedDate = transaction.CreatedDate; // is set to date.now in TM
                r.CreatedUser = Convert.ToString(transaction.CreatedUserId);
                r.DateCollected = new DateTime(transaction.TransactionDate.Year, transaction.TransactionDate.Month, transaction.TransactionDate.Day);
                r.DocReceived = false;
                bool isGenerated = false;
                //Generated Tires is opposite of Eligibility
                if(transaction.Transaction_Eligibility != null)
                {
                    if(transaction.Transaction_Eligibility.EligibilityId == 1)
                    {
                        isGenerated = !transaction.Transaction_Eligibility.Value;
                    }
                    else if (transaction.Transaction_Eligibility.EligibilityId == 2)
                    {
                        isGenerated = transaction.Transaction_Eligibility.Value;                    
                    }
                }
                r.GeneratedTires = isGenerated;
                var gotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.GOTR)).SingleOrDefault();
                r.GOTR = Convert.ToInt32(gotr == null ? 0 : gotr.Quantity);
                var ind = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.IND)).SingleOrDefault();
                r.IND = Convert.ToInt32(ind.Quantity);
                r.Invalid = false;
                r.DeviceName = transaction.DeviceName;
                var lotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.LOTR)).SingleOrDefault();
                r.LOTR = Convert.ToInt32(lotr == null ? 0 : lotr.Quantity);
                r.ModifiedDate = null;
                r.ModifiedUser = null;

                var motr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MOTR)).SingleOrDefault();
                r.MOTR = Convert.ToInt32(motr == null ? 0 : motr.Quantity);
                var mt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MT)).SingleOrDefault();
                r.MT = Convert.ToInt32(mt == null ? 0 : mt.Quantity);
                var plt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.PLT)).SingleOrDefault();
                r.PLT = Convert.ToInt32(plt == null ? 0 : plt.Quantity);
                r.PostalCode1 = transaction.PostalCode1;
                r.PostalCode2 = transaction.PostalCode2;
                r.Rate = null;
                r.RateMLt = null;
                r.ReceivedFromCollectorsID = 0;
                r.RecordModified = false;
                r.RemoveCredit = null;
                r.Revised = transaction.Revised ?? false;
                var sotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.SOTR)).SingleOrDefault();
                r.SOTR = Convert.ToInt32(sotr == null ? 0 : sotr.Quantity);
                r.TcRFormNumber = transaction.FriendlyId.ToString();
                r.TotalTireEstWeight = null;
                r.Unifier = null;
                WS.ReceivedFromCollectorsSaveResponse response = (WS.ReceivedFromCollectorsSaveResponse)(client.SaveReceivedFromCollectors(r, r.CreatedUser, cred.HaulerServiceCredentials));
                int receivedFromCollectorsID = response.NewClaimID;
                bool[] tmp = response.ResponseContent;
                List<object> t = new List<object>();
                foreach (var item in tmp)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.NewClaimID;

            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
            }
            return result;
        }

        public IResponseMessage<object> Delete(int receivedFromCollectorsID)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                WS.ReceivedFromCollectorsSaveResponse response = (WS.ReceivedFromCollectorsSaveResponse)(client.DeleteReceivedFromCollectors(receivedFromCollectorsID, cred.HaulerServiceCredentials));
                bool[] tmp = response.ResponseContent;
                List<object> t = new List<object>();
                foreach (var item in tmp)
                {
                    t.Add(item);
                }
                result.ResponseContent = t;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.SingleEntity;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt32(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = false;
            }
            return result;             
        }

        public void Close()
        {
            client.Close();
        }

        #endregion

    }
}
