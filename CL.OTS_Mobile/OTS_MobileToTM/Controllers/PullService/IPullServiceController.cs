﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;

namespace OTS_MobileToTM.Controllers
{
    interface IPullServiceController
    {
        IEnumerable<AppUserModel> GetUpdatedAppUsersList(DateTime lastUpdated, string token);
        IEnumerable<VendorModel> GetUpdatedVendorsList(DateTime lastUpdated, string token);    
    }
}
