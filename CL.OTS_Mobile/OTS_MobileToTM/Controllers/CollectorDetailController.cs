﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using WS = OTS_MobileToTM.CollectorServiceReference;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;
using otscontracts.Common.Enums;



namespace OTS_MobileToTM.Controllers
{
    public class CollectorDetailController : ICollectorDetailController
    {

        #region Properties

        private WS.CollectorCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }


        #endregion

        #region Constructors

        public CollectorDetailController() {
            new CollectorDetailController(string.Empty, string.Empty);
        }

        public CollectorDetailController(string username, string password)
        {
            client = new WS.CollectorCommunicationClient();
            cred = new OTS_MobileToTM.ServiceCredentials();
            cred.CollectorServiceCredentials.UserName = username;
            cred.CollectorServiceCredentials.Password = password;        
        }

        #endregion

        #region Methods

        public IResponseMessage<object> Insert(ITransactionModelExtended transaction, int SummaryID )
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                ICollection<ITransaction_TireTypeModel> tireTypeList = transaction.Transaction_TireTypes;

                WS.CollectorDetailModel record = new WS.CollectorDetailModel();
                record.DateCollected = transaction.TransactionDate.ToString("yyyy-MM-dd");
                record.FormType = transaction.TransactionTypeId == (int)TransactionTypes.TCR ? TransactionTypes.TCR.ToString().ToLower() : string.Empty;
                record.FormNumber = transaction.FriendlyId.ToString();

                record.HaulerNumber = Convert.ToString(transaction.RegistrationNumber);

                bool isGenerated = false;
                //Generated Tires is opposite of Eligibility
                if (transaction.Transaction_Eligibility != null)
                {
                    if (transaction.Transaction_Eligibility.EligibilityId == 1)
                    {
                        isGenerated = !transaction.Transaction_Eligibility.Value;
                    }
                    else if (transaction.Transaction_Eligibility.EligibilityId == 2)
                    {
                        isGenerated = transaction.Transaction_Eligibility.Value;
                    }
                }
                record.EligibleForPayment = (isGenerated ? "n" : "y");
                record.GeneratedTires = (isGenerated ? "y" : "n");

                var plt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.PLT)).SingleOrDefault();                
                record.PLt = Convert.ToInt32(plt == null ? 0 : plt.Quantity);

                var mt = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MT)).SingleOrDefault();
                record.MT = Convert.ToInt32(mt == null ? 0 : mt.Quantity);

                var agls = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.AGLS)).SingleOrDefault();
                record.AgLS = Convert.ToInt32(agls == null ? 0 : agls.Quantity);

                var ind = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.IND)).SingleOrDefault();
                record.Ind = Convert.ToInt32(ind.Quantity);

                var sotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.SOTR)).SingleOrDefault();
                record.SotR = Convert.ToInt32(sotr == null ? 0 : sotr.Quantity);

                var motr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.MOTR)).SingleOrDefault();
                record.MotR = Convert.ToInt32(motr == null ? 0 : motr.Quantity);

                var lotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.LOTR)).SingleOrDefault();
                record.LotR = Convert.ToInt32(lotr == null ? 0 : lotr.Quantity);

                var gotr = tireTypeList.Where(x => x.TireTypeId == Convert.ToUInt16(TireTypes.GOTR)).SingleOrDefault();
                record.GotR = Convert.ToInt32(gotr == null ? 0 : gotr.Quantity);
               
                record.UID = transaction.UID;
                record.ClaimPeriod = "0";
                record.SummaryID = SummaryID;
                record.Comment = transaction.OtsComments;
                record.RegistrationNumber = Convert.ToString(transaction.OutgoingRegistrationNumber);
                record.Approve = "n";
                record.DeviceName = transaction.DeviceName;
                //record.CreatedBy = string.Empty;
                //record.CreatedDate = string.Empty;
                WS.CollectorDetailResponse response = (WS.CollectorDetailResponse)client.SaveClaimDetail(record, transaction.PreparedBy, cred.CollectorServiceCredentials);
                result.ResponseContent = response.ResponseContent;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.NewClaimID;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt16(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = -1;
            }
            return result;
        }

        public void Close()
        {
            client.Close();
        }


        #endregion
    }
}
