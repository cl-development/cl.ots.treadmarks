﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.HaulerServiceReference;

namespace OTS_MobileToTM.Controllers
{
    interface IReceivedFromCollectorsController : IController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="haulerSummaryId"></param>
        /// <param name="options">a dictionary to modify attributes "Revised" and "Comments" fields not found in transaction model</param>
        /// <returns></returns>
        //IResponseMessage<object> Insert(ITransactionModel transaction, int haulerSummaryId, IDictionary<string, string> options);

        IResponseMessage<object> Insert(ITransactionModelExtended transaction, int haulerSummaryId);

        IResponseMessage<object> Delete(int receivedFromCollectorsId);

        void Close();
    }
}
