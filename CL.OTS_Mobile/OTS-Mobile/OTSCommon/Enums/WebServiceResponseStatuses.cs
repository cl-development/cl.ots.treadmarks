﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otscommon.Enums
{
    /// <summary>
    /// Enumeration which defines possible Web Service Response Status values and their labels
    /// </summary>
    public enum MobileWSResponseStatus
    {
        NONE = 0,
        OK = 1, 
        ERROR = 2, 
        WARNING = 3       
    }
}
