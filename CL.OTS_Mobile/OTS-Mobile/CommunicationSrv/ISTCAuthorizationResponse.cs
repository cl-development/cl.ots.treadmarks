﻿using System;
using OTS_Mobile.CommunicationData;
using System.IO;

namespace otscontracts.CommunicationSrv
{
    /// <summary>
    /// Communication entity for ITransactionResponseMsg
    /// * Defines the applicable actions and returned data collection of a web service call(s).
    /// </summary>
    public interface ISTCAuthorizationResponse : IResponseMessage<STCAuthorizationModel>
    {
        void SyncSTCAuthorization(ISTCAuthorizationModel stcAuthorizationInfo);             
    }
}
