﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OTS_Mobile
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                //connection.ConnectionString = "data source=OTSDEV-SQL2.ontariots.ad;initial catalog=TreadMarks-Mobile;user id=asingh;password=Aman@1234;multipleactiveresultsets=True;";
                connection.ConnectionString = string.Format("data source={0};initial catalog=TreadMarks-Mobile;user id=asingh;password=Aman@1234;multipleactiveresultsets=True;",
                    ConfigurationManager.ConnectionStrings["OTS_MobileEntities"].ConnectionString.Split('=')[4].Split(';')[0]);               		

                connection.Open();
                foreach (string table in GetAllTables(connection))
                {
                    if (table != "Location" && table != "Registrant")
                    {
                        SqlCommand sqlCommand = new SqlCommand("select * from [dbo].[" + table + "]", connection);
                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        GridView gv = new GridView();
                        gv.DataSource = reader;
                        gv.DataBind();
                        Label lblName = new Label();
                        lblName.CssClass = "marginClass";
                        lblName.Text = "Table: " + table;
                        gv.CssClass = "EU_DataTable";
                        this.Form.Controls.Add(lblName);
                        this.Form.Controls.Add(gv);
                    }
                }
            }
        }

        string[] GetAllTables(SqlConnection connection)
        {
            List<string> result = new List<string>();
            SqlCommand cmd = new SqlCommand("SELECT name FROM sys.Tables", connection);
            System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                result.Add(reader["name"].ToString());
            return result.ToArray();
        }
    }
}