﻿using System;
using System.Net.Mail;

namespace CustomTraceListeners
{
	public class TraceEmailer : ITraceEmailer
	{
		public TraceEmailer(string from, string to, string subject)
		{
			MailFrom = from;
			MailTo = to;
			Subject = subject;
		}

		public void SendTraceEmail(string message)
		{
			using (MailMessage msg = new MailMessage())
			{
				msg.From = new MailAddress(MailFrom);
				foreach (string s in MailTo.Split(";".ToCharArray()))
				{
					msg.To.Add(s);
				}
				msg.Subject = Subject;
                msg.Body = message;
                SmtpClient smtp = new SmtpClient();
				smtp.Send(msg);
			}
		}

		#region properties
		private string _mailFrom;
		public string MailFrom
		{
			get
			{
				return _mailFrom;
			}
			set
			{
				_mailFrom = value;
			}
		}

		private string _mailTo;
		public string MailTo
		{
			get
			{
				return _mailTo;
			}
			set
			{
				_mailTo = value;
			}
		}

		private string _subject;
		public string Subject
		{
			get
			{
				return _subject;
			}
			set
			{
				_subject = value;
			}
		}
		#endregion
	}
}
