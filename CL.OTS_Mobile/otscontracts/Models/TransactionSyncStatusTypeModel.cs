namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class TransactionSyncStatusTypeModel
    {
        public TransactionSyncStatusTypeModel()
        {}
        public long TransactionSyncStatusTypeId { get; set; }
        public string NameKey { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
