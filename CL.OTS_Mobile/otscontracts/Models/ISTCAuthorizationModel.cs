namespace otscontracts
{
    using otscontracts;
    using System;
    using System.Collections.Generic;
    
    public interface ISTCAuthorizationModel
    {
        IAuthorizationModel Authorization { get; set; }
        ILocationModel Location { get; set; }
    }
}
