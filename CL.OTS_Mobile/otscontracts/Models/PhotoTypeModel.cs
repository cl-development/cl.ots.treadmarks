namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class PhotoTypeModel
    {
        public PhotoTypeModel()
        { }
        public int RecordId { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
