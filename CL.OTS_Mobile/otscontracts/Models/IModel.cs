﻿using System;
using System.Collections.Generic;

namespace otscontracts.Models
{
    public interface IModel
    {
        bool Validate();
        IEnumerable<string> GetErrors();
    }
}
