﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Caching
{
    /// <summary>
    /// An untility used to manage in-memory caching
    /// </summary>
    public static class CachingHelper
    {
        #region Fields
        private static ObjectCache cache = MemoryCache.Default;
        #endregion 

        #region Public Methods
        /// <summary>
        /// Add an item with a callback
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="timeSpan"></param>
        /// <param name="updateCallback"></param>
        public static void AddItem<T>(string key, T obj, TimeSpan timeSpan, CacheEntryUpdateCallback updateCallback)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.SlidingExpiration = timeSpan;
            policy.UpdateCallback = updateCallback;
            cache.Add(key, obj, policy);
        }
        /// <summary>
        /// Add an item  with timespan
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="timeSpan"></param>
        public static void AddItem<T>(string key, T obj, TimeSpan timeSpan)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.SlidingExpiration = timeSpan;
            cache.Add(key, obj, policy);
        }

        /// <summary>
        /// Add an item with a specified absolute time
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="timeOffset"></param>
        public static void AddItem<T>(string key, T obj, DateTimeOffset timeOffset)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = timeOffset;
            cache.Add(key, obj, policy);
        }

        /// <summary>
        /// Add an item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public static void AddItem<T>(string key, T obj)
        {
            cache.Add(key, obj, null);
        }

        /// <summary>
        /// Get an item by key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetItem<T>(string key)
        {
            return (T)cache.Get(key);
        }

        /// <summary>
        /// Update an item to cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public static void SaveItem<T>(string key, T obj)
        {
            cache.Set(key, obj, null);
        }

        /// <summary>
        /// Remove an item
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveItem(string key)
        {
            cache.Remove(key);
        }

        /// <summary>
        /// Check if it is existing item
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool ContainsKey(string key)
        {
            return cache.Contains(key);
        }
        #endregion 
    }
}
