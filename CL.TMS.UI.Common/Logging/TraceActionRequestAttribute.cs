﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CL.Framework.Logging;
using CL.TMS.Security.Authorization;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.UI.Common.Logging
{
    public class TraceActionRequestAttribute : ActionFilterAttribute, IActionFilter
    {
        private long startTicks;
        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (LogManager.IsTracingEnabled)
            {
                var elapsedTime = (DateTime.Now.Ticks - startTicks) / 10000;
                var message = string.Format("Action ends, and elasped time: {0} ms", elapsedTime);
                LogManager.LogDebug(message, new List<LogArg>
                {
                    new LogArg{Name = "Controller Name", Value = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName},
                    new LogArg{Name = "Action Name", Value = filterContext.ActionDescriptor.ActionName},
                    new LogArg{Name = "Action Result", Value = filterContext.Result}
                });
            }
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (LogManager.IsTracingEnabled)
            {
                this.startTicks = DateTime.Now.Ticks;

                var logArgs = new List<LogArg>();
                logArgs.Add(new LogArg { Name = "Controller Name", Value = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName });
                logArgs.Add(new LogArg { Name = "Action Name", Value = filterContext.ActionDescriptor.ActionName });
                foreach(var kvp in filterContext.ActionParameters)
                {
                    logArgs.Add(new LogArg { Name = kvp.Key, Value = kvp.Value });
                }
                logArgs.Add(new LogArg { Name = "UserName", Value = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Name) });
                logArgs.Add(new LogArg { Name = "Email", Value = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Email) });
                logArgs.Add(new LogArg { Name = "UserId", Value = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Sid) });


                LogManager.LogDebug(string.Format("Action starts: {0}",DateTime.Now), logArgs);
            }
        }
    }
}
