﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace CL.TMS.UI.Common.UserInterface
{
    /// <summary>
    /// Extension methods
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Generic evaluate order by statement
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="query"></param>
        /// <param name="propertyName"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        public static IOrderedQueryable<TSource> GenericEvaluateOrderBy<TSource>(this IQueryable<TSource> query, string propertyName, string methodName)
        {
            var type = typeof(TSource);

            var property = type.GetProperty(propertyName);
            var parameter = Expression.Parameter(type, "p");
            //var propertyReference = Expression.Property(parameter, propertyName);
            try
            {
                //var typedProperty = Expression.Lambda<Func<TSource, property.PropertyType >>(propertyReference, new[] { parameter });
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var typedProperty = Expression.Lambda(propertyAccess, parameter);
                var sortExpression = Expression.Call(
                    typeof(Queryable),
                    methodName,
                    new Type[] { type, property.PropertyType },
                    query.Expression,
                    typedProperty
                    );


                return (IOrderedQueryable<TSource>)query.Provider.CreateQuery<TSource>(sortExpression);

            }
            catch (Exception)
            {
                return (IOrderedQueryable<TSource>)query;
            }
        }

        public static string ToYesNoString(this bool value)
        {
            return value ? "Yes" : "No";
        }

        public static string ToString(this Decimal? value, params object[] param)
        {
            Decimal val = value ?? 0;
            if (param.Count() > 0)
            {
                //return val.ToString("#.##");
                return val.ToString(param[0].ToString());
            }
            return val.ToString();
        }

        public static bool ToBoolean(this string value)
        {
            return ((string.Compare(value, "1", true) == 0) || (string.Compare(value, "true", true) == 0)) ? true : false;
        }

        public static string ToDatetimeNullString(this DateTime? value, string format)
        {
            DateTime dt = value ?? DateTime.MinValue;
            if (DateTime.MinValue == dt)
            {
                return string.Empty;
            }
            else
            {
                return dt.ToString(format);
            }
        }
        /// <summary>
        /// Converts a collection of IModel instances to a CSV.
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="source"></param>
        /// <param name="separator">Separator string.</param>
        /// <param name="columnNames">Column names.</param>
        /// <param name="funcs">Func delegates applied to each item in the collection. Used to format the values.</param>
        /// <returns>CSV formatted string.</returns>
        public static string ToCSV<TClass>(this IEnumerable<TClass> source, string separator, IEnumerable<string> columnNames, IEnumerable<Func<TClass, string>> funcs)
        {
            var type = typeof(TClass);
            if (columnNames.Count() != funcs.Count())
                throw new ArgumentException("The parameters columnNames and funcs must be of the same length.");

            string escapedSeparator = string.Concat("\\", separator);
            StringBuilder strBuilder = new StringBuilder();

            foreach (string column in columnNames)
                strBuilder.AppendFormat("{0}{1}", column, separator);

            strBuilder.Remove(strBuilder.Length - 1, 1);
            strBuilder.Append("\r\n");

            foreach (var item in source)
            {
                foreach (var f in funcs)
                {
                    strBuilder.AppendFormat("\"{0}\"{1}", (f(item) != null) ? f(item).Replace("\"", "\"\"") : string.Empty, separator);
                }

                strBuilder.Remove(strBuilder.Length - 1, 1);
                strBuilder.Append("\r\n");
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// Converts a collection of Array instances to a CSV.
        /// </summary>
        /// <typeparam name="TArray"></typeparam>
        /// <param name="source"></param>
        /// <param name="separator">Separator string.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>CSV formatted string.</returns>
        public static string ToCSV<TArray>(this TArray[,] source, string separator, IEnumerable<string> columnNames)
        {
            var type = typeof(TArray);
            if (columnNames.Count() != source.GetLength(1))
                throw new ArgumentException("The parameters columnNames and funcs must be of the same length.");

            string escapedSeparator = string.Concat("\\", separator);
            StringBuilder strBuilder = new StringBuilder();

            foreach (string column in columnNames)
                strBuilder.AppendFormat("{0}{1}", column, separator);

            strBuilder.Remove(strBuilder.Length - 1, 1);
            strBuilder.Append("\r\n");

            for (int row = 0; row < source.GetLength(0); row++)
            {
                if (source[row, 0] != null)//if row leading with null, skip this line
                {
                    for (int clm = 0; clm < source.GetLength(1); clm++)
                    {
                        strBuilder.AppendFormat("\"{0}\"{1}", source[row, clm], separator);
                    }

                    strBuilder.Remove(strBuilder.Length - 1, 1);
                    strBuilder.Append("\r\n");
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// Generates a sql contains expression tree
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="valueSelector"></param>
        /// <param name="valuesToCompare"></param>
        /// <returns></returns>
        public static Expression<Func<TElement, bool>> CreateContainsExpression<TElement, TValue>(
            Expression<Func<TElement, TValue>> valueSelector,
            IEnumerable<TValue> valuesToCompare
            )
        {
            if (valueSelector == null)
                throw new ArgumentNullException("valueSelector");

            if (valuesToCompare == null)
                throw new ArgumentNullException("valuesToCompare");

            if (valuesToCompare.Count() == 0)
                return ex => false;

            ParameterExpression pe = valueSelector.Parameters.Single();
            var equals = valuesToCompare.Select(value =>
                (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.OrElse(accumulate, equal));

            return Expression.Lambda<Func<TElement, bool>>(body, pe);
        }

        /// <summary>
        /// Get Display Name
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString GetDisplayName<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression
        )
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = metaData.DisplayName ?? (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
            return MvcHtmlString.Create(value);
        }

        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="source"></param>
        /// <param name="controllerContext"></param>
        /// <param name="viewData"></param>
        /// <param name="tempData"></param>
        /// <param name="viewName"></param>
        /// <param name="myModel"></param>
        /// <returns></returns>
        public static string RenderPartialViewToString(this Controller source, ControllerContext controllerContext,
            ViewDataDictionary viewData, TempDataDictionary tempData, string viewName, object myModel)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controllerContext.RouteData.GetRequiredString("action");

            viewData.Model = myModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, sw);
                viewResult.View.Render(viewContext, sw);

                var s = sw.GetStringBuilder().ToString();
                return s;
            }
        }

        public static RouteValueDictionary ConditionalDisable(bool disabled, object htmlAttributes = null)
        {
            var dictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (disabled)
                dictionary.Add("disabled", "disabled");
            return dictionary;
        }
    }
}
