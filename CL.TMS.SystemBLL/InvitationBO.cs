﻿using CL.Framework.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.RuleEngine.InvitationBusinessRules;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Email;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
namespace CL.TMS.SystemBLL
{
    public class InvitationBO : BaseBO
    {
        private IApplicationRepository applicationRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;

        public InvitationBO(IApplicationRepository appRepository, IApplicationInvitationRepository appInvitationRepository)
        {
            applicationInvitationRepository = appInvitationRepository;
            applicationRepository = appRepository;
        }
        public ApplicationInvitation GetApplicationInvitationByEmailID(string email, string participantType)
        {
            return applicationInvitationRepository.GetApplicationInvitationByEmailID(email, participantType);
        }
        public int GetParticipantTypeID(string participantName)
        {
            return applicationInvitationRepository.GetParticipantTypeID(participantName);
        }

        public ApplicationInvitation AddApplicationInvitation(ApplicationInvitation appInvitation)
        {
            return applicationInvitationRepository.AddApplicationInvitation(appInvitation);
        }

        public Guid GetTokenByApplicationId(int applicationId)
        {
            return applicationInvitationRepository.GetTokenByApplicationId(applicationId);
        }

        public ApplicationInvitation GetById(Guid ID)
        {
            return applicationInvitationRepository.GetById(ID);
        }

        public void Update(ApplicationInvitation appInvitation)
        {
            applicationInvitationRepository.Update(appInvitation);

        }

        public string GetEmailByApplicationId(int applicationId)
        {
            return applicationInvitationRepository.GetEmailByApplicationId(applicationId);
        }

        public ApplicationInvitation UpdateInvitationDatetime(int applicationId)
        {
            return applicationInvitationRepository.UpdateInvitationDatetime(applicationId);
        }

    }
}
