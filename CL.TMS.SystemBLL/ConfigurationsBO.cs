﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using CL.TMS.IRepository.Claims;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common;
using CL.TMS.Communication.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.ClaimCalculator;
using System.Threading.Tasks;
using CL.TMS.DataContracts;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Caching;
using System.Diagnostics;

namespace CL.TMS.SystemBLL
{
    public class ConfigurationsBO
    {
        private IUserRepository userRepository;
        private IVendorRepository vendorRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository; //OTSTM2-400
        private IConfigurationsRepository configurationsRepository;
        private ISettingRepository settingRepository;
        public ConfigurationsBO(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator,
            IMessageRepository messageRepository, IConfigurationsRepository configurationsRepository, ISettingRepository settingRepository)
        {
            this.userRepository = userRepository;
            this.vendorRepository = vendorRepository;
            this.eventAggregator = eventAggregator;
            this.messageRepository = messageRepository; //OTSTM2-400   
            this.configurationsRepository = configurationsRepository;
            this.settingRepository = settingRepository;
        }

        #region //loading rates
        public PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return configurationsRepository.LoadRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }
        public PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return configurationsRepository.LoadPIRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }
        public PaginationDTO<RateListViewModel, int> LoadTIPayeeList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return configurationsRepository.LoadTIPayeeList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }
        public RateDetailsVM LoadRateDetailsByTransactionID(int rateTransactionID, int category, bool isSpecific)
        {
            RateDetailsVM ratesVM = new RateDetailsVM()
            {
                category = category,
                RateTransactionID = rateTransactionID,
                isSpecific = isSpecific
            };

            var param = new RateParamsDTO()
            {
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name
            };

            LoadRateDetails(param, ratesVM);

            return ratesVM;
        }

        public bool updateAppSettings(AppSettingsVM appSettingsVM, long userId)
        {
            if (appSettingsVM.bDataValidationSuccess)
            {
                if (configurationsRepository.updateAppSettings(appSettingsVM, userId))
                {
                    CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType)
        {
            return configurationsRepository.LoadAppSettingNotesByType(noteType);
        }

        public AppSettingsVM LoadAppSettings()
        {
            return configurationsRepository.LoadAppSettings();
        }

        public bool IsCollectorRateTransactionExists(int category)
        {
            return configurationsRepository.IsCollectorRateTransactionExists(category);
        }

        public bool IsFutureCollectorRateTransactionExists(int category)
        {
            return configurationsRepository.IsFutureCollectorRateTransactionExists(category);
        }

        public bool IsFutureCollectorRateExists(int category)
        {
            return configurationsRepository.IsFutureCollectorRateExists(category);
        }

        public bool IsFutureRateExists(int category)
        {
            return configurationsRepository.IsFutureRateExists(category);
        }
        public bool IsFutureRateExists(int category, bool isSpecific)
        {
            return configurationsRepository.IsFutureRateExists(category, isSpecific);
        }

        /// <summary>
        /// Loading logic from: CL.TMS.HaulerBLL.HaulerClaimBO.PopulateNorthernPremiumRate(HaulerPaymentViewModel haulerPayment, DateTime claimPeriodDate)
        /// </summary>
        /// <param name="resut"></param>
        /// <returns></returns>
        private RateDetailsVM PopulateTransportationIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, TransportationIncentiveParam param)
        {
            ratesVM.northernPremiumRate = new NorthernPremiumRateVM(result, param);
            ratesVM.ineligibleInventoryPaymentRate = new IneligibleInventoryPaymentRateVM(result, param);
            ratesVM.DOTPremiumRate = new DOTPremiumRateVM(result, param);
            #region to be remove

            //Northern premium Rate
            // var northTypes = result.Where(x => x.PaymentType == param.paymentTypeNorth);
            //var rate = northTypes.FirstOrDefault(c => c.ClaimType == param.claimTypeHauler && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            //ratesVM.northernPremiumRate.N1SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c => c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            //ratesVM.northernPremiumRate.N2SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            //ratesVM.northernPremiumRate.N3SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            //ratesVM.northernPremiumRate.N4SturgeonFallRateOnRoad = rate != null ? rate.ItemRate : 0;


            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N1SouthRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N2SouthRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N3SouthRateOnRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N4SouthRateOnRoad = rate != null ? rate.ItemRate : 0;


            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N1SouthRateOffRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N2SouthRateOffRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N3SouthRateOffRoad = rate != null ? rate.ItemRate : 0;

            //rate = northTypes.FirstOrDefault(c =>  c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            //ratesVM.northernPremiumRate.N4SouthRateOffRoad = rate != null ? rate.ItemRate : 0;

            //DOTPreminum Rate
            //ClaimType == 3 : Hauler
            //PaymentType == 2 : DOT Premium

            //For northern pick ups, rate will be based on Pick up zone only.
            //Rate n1ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n1ZoneId);
            //ratesVM.DOTPremiumRate.N1OffRoadRate = n1ZoneItemRate != null ? n1ZoneItemRate.ItemRate : 0;

            //Rate n2ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n2ZoneId);
            //ratesVM.DOTPremiumRate.N2OffRoadRate = n2ZoneItemRate != null ? n2ZoneItemRate.ItemRate : 0;

            //Rate n3ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n3ZoneId);
            //ratesVM.DOTPremiumRate.N3OffRoadRate = n3ZoneItemRate != null ? n3ZoneItemRate.ItemRate : 0;

            //Rate n4ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n4ZoneId);
            //ratesVM.DOTPremiumRate.N4OffRoadRate = n4ZoneItemRate != null ? n4ZoneItemRate.ItemRate : 0;

            //// For South DOT pick ups, rate will be based on pickup zone and drop off zone.
            //Rate mooseCreekZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.mooseCreekZoneId);
            //ratesVM.DOTPremiumRate.MooseCreekOffRoadRate = mooseCreekZoneItemRate != null ? mooseCreekZoneItemRate.ItemRate : 0;

            //Rate gtaZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.gtaZoneId);
            //ratesVM.DOTPremiumRate.GTAOffRoadRate = gtaZoneItemRate != null ? gtaZoneItemRate.ItemRate : 0;

            //Rate wtcZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.wtcZoneId);
            //ratesVM.DOTPremiumRate.WTCOffRoadRate = wtcZoneItemRate != null ? wtcZoneItemRate.ItemRate : 0;

            //Rate sturgeonFallsZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.sturgeonFallsZoneId);
            //ratesVM.DOTPremiumRate.SturgeonFallsOffRoadRate = sturgeonFallsZoneItemRate != null ? sturgeonFallsZoneItemRate.ItemRate : 0;

            //Processor TI Rate
            //ClaimType == 3 : Hauler
            //PaymentType == 3 : Processor TI
            //ItemType == 1 : On-Road
            //ItemType == 2 : Off-Road
            //Rate mooseCreekZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOnRoad);
            //ratesVM.ineligibleInventoryPaymentRate.MooseCreakOnRoadRate = mooseCreekZoneOnRoadItemRate != null ? mooseCreekZoneOnRoadItemRate.ItemRate : 0;

            //Rate mooseCreekZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOffRoad);
            //ratesVM.ineligibleInventoryPaymentRate.MooseCreakOffRoadRate = mooseCreekZoneOffRoadItemRate != null ? mooseCreekZoneOffRoadItemRate.ItemRate : 0;

            //Rate gtaZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOnRoad);
            //ratesVM.ineligibleInventoryPaymentRate.GTAOnRoadRate = gtaZoneOnRoadItemRate != null ? gtaZoneOnRoadItemRate.ItemRate : 0;

            //Rate gtaZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOffRoad);
            //ratesVM.ineligibleInventoryPaymentRate.GTAOffRoadRate = gtaZoneOffRoadItemRate != null ? gtaZoneOffRoadItemRate.ItemRate : 0;

            //Rate wtcZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOnRoad);
            //ratesVM.ineligibleInventoryPaymentRate.WTCOnRoadRate = wtcZoneOnRoadItemRate != null ? wtcZoneOnRoadItemRate.ItemRate : 0;

            //Rate wtcZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOffRoad);
            //ratesVM.ineligibleInventoryPaymentRate.WTCOffRoadRate = wtcZoneOffRoadItemRate != null ? wtcZoneOffRoadItemRate.ItemRate : 0;

            //Rate sturgeonFallsZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOnRoad);
            //ratesVM.ineligibleInventoryPaymentRate.SturgeonFallsOnRoadRate = sturgeonFallsZoneOnRoadItemRate != null ? sturgeonFallsZoneOnRoadItemRate.ItemRate : 0;

            //Rate sturgeonFallsZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOffRoad);
            //ratesVM.ineligibleInventoryPaymentRate.SturgeonFallsOffRoadRate = sturgeonFallsZoneOffRoadItemRate != null ? sturgeonFallsZoneOffRoadItemRate.ItemRate : 0;
            #endregion

            return ratesVM;
        }
        private RateDetailsVM PopulateCollectorAllowanceRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            //int colloctorTireType = (int)ClaimType.Collector;
            int itemCategory = (int)ItemCategoryEnum.CollectorTireType; //5
            ratesVM.collectorAllowanceRate = new CollectorAllowanceRateVM()
            {
                PLT = result.Where(i => i.item.ShortName == "PLT" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                MT = result.Where(i => i.item.ShortName == "MT" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                AGLS = result.Where(i => i.item.ShortName == "AGLS" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                IND = result.Where(i => i.item.ShortName == "IND" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                SOTR = result.Where(i => i.item.ShortName == "SOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                MOTR = result.Where(i => i.item.ShortName == "MOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                LOTR = result.Where(i => i.item.ShortName == "LOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                GOTR = result.Where(i => i.item.ShortName == "GOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
            };
            return ratesVM;
        }
        private RateDetailsVM PopulateManufacturingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            var tempRPM = DataLoader.RPMSPSTransactionProducts;
            ratesVM.manufacturingIncentiveRate = new ManufacturingIncentiveRateVM()
            {
                Calendared = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Calendared")).ID).ItemRate,
                Extruded = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Extruded")).ID).ItemRate,
                Molded = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Molded")).ID).ItemRate
            };
            return ratesVM;
        }
        private RateDetailsVM PopulateProcessingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, ProcessingIncentiveParam param)
        {
            #region to be remove

            //var SPS = result.Where(x => x.PaymentType == param.spsPaymentType);
            //var PIT = result.Where(x => x.PaymentType == param.pitPaymentType);
            //ratesVM.processingIncentiveRate = new ProcessingIncentiveRateVM()
            //{
            //    #region //SPS OnRoad
            //    TDP1OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OnRoad).ItemRate,
            //    TDP1FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOnRoad).ItemRate,
            //    TDP2OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OnRoad).ItemRate,
            //    TDP2FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOnRoad).ItemRate,
            //    TDP3OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OnRoad).ItemRate,
            //    TDP3FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOnRoad).ItemRate,
            //    TDP4OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OnRoad).ItemRate,
            //    TDP5OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OnRoad).ItemRate,
            //    #endregion

            //    #region //SPS OffRoad
            //    TDP1OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OffRoad).ItemRate,
            //    TDP1FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOffRoad).ItemRate,
            //    TDP2OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OffRoad).ItemRate,
            //    TDP2FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOffRoad).ItemRate,
            //    TDP3OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OffRoad).ItemRate,
            //    TDP3FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOffRoad).ItemRate,
            //    TDP4OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OffRoad).ItemRate,
            //    TDP5OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OffRoad).ItemRate,
            //    #endregion

            //    #region //PIT
            //    TDP1 = PIT.FirstOrDefault(x => x.ItemID == param.tdp1).ItemRate,
            //    TDP2 = PIT.FirstOrDefault(x => x.ItemID == param.tdp2).ItemRate,
            //    TDP3 = PIT.FirstOrDefault(x => x.ItemID == param.tdp3).ItemRate,
            //    TDP4FF = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FF).ItemRate,
            //    TDP4FFNoPI = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FFNoPI).ItemRate,
            //    TDP5FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5FT).ItemRate,
            //    TDP5NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5NT).ItemRate,
            //    TDP6NP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6NP).ItemRate,
            //    TDP6FP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6FP).ItemRate,
            //    TDP7NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7NT).ItemRate,
            //    TDP7FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7FT).ItemRate,
            //    TransferFibreRubber = PIT.FirstOrDefault(x => x.ItemID == param.transferFibreRubber).ItemRate,
            //    #endregion
            //};
            #endregion

            ratesVM.processingIncentiveRate = new ProcessingIncentiveRateVM(result, param);
            return ratesVM;
        }
        private RateDetailsVM PopulateTireStewardshipFeesRateVM(List<Rate> result, RateDetailsVM ratesVM, TireStewardshipFeeParam param)
        {
            #region to be remove

            //if ((result == null) || (result.Count == 0))
            //{
            //    ratesVM.tireStewardshipFeesRate = new TireStewardshipFeesRateVM(param);
            //}
            //else
            //{
            //    ratesVM.tireStewardshipFeesRate = new TireStewardshipFeesRateVM(param)
            //    {
            //        C1 = result.FirstOrDefault(i => i.ItemID == param.c1) != null ? result.FirstOrDefault(i => i.ItemID == param.c1).ItemRate : 0,
            //        C2 = result.FirstOrDefault(i => i.ItemID == param.c2) != null ? result.FirstOrDefault(i => i.ItemID == param.c2).ItemRate : 0,
            //        C3 = result.FirstOrDefault(i => i.ItemID == param.c3) != null ? result.FirstOrDefault(i => i.ItemID == param.c3).ItemRate : 0,
            //        C4 = result.FirstOrDefault(i => i.ItemID == param.c4) != null ? result.FirstOrDefault(i => i.ItemID == param.c4).ItemRate : 0,
            //        C5 = result.FirstOrDefault(i => i.ItemID == param.c5) != null ? result.FirstOrDefault(i => i.ItemID == param.c5).ItemRate : 0,
            //        C6 = result.FirstOrDefault(i => i.ItemID == param.c6) != null ? result.FirstOrDefault(i => i.ItemID == param.c6).ItemRate : 0,
            //        C7 = result.FirstOrDefault(i => i.ItemID == param.c7) != null ? result.FirstOrDefault(i => i.ItemID == param.c7).ItemRate : 0,
            //        C8 = result.FirstOrDefault(i => i.ItemID == param.c8) != null ? result.FirstOrDefault(i => i.ItemID == param.c8).ItemRate : 0,
            //        C9 = result.FirstOrDefault(i => i.ItemID == param.c9) != null ? result.FirstOrDefault(i => i.ItemID == param.c9).ItemRate : 0,
            //        C10 = result.FirstOrDefault(i => i.ItemID == param.c10) != null ? result.FirstOrDefault(i => i.ItemID == param.c10).ItemRate : 0,
            //        C11 = result.FirstOrDefault(i => i.ItemID == param.c11) != null ? result.FirstOrDefault(i => i.ItemID == param.c11).ItemRate : 0,
            //        C12 = result.FirstOrDefault(i => i.ItemID == param.c12) != null ? result.FirstOrDefault(i => i.ItemID == param.c12).ItemRate : 0,
            //        C13 = result.FirstOrDefault(i => i.ItemID == param.c13) != null ? result.FirstOrDefault(i => i.ItemID == param.c13).ItemRate : 0,
            //        C14 = result.FirstOrDefault(i => i.ItemID == param.c14) != null ? result.FirstOrDefault(i => i.ItemID == param.c14).ItemRate : 0,
            //        C15 = result.FirstOrDefault(i => i.ItemID == param.c15) != null ? result.FirstOrDefault(i => i.ItemID == param.c15).ItemRate : 0,
            //        C16 = result.FirstOrDefault(i => i.ItemID == param.c16) != null ? result.FirstOrDefault(i => i.ItemID == param.c16).ItemRate : 0,
            //        C17 = result.FirstOrDefault(i => i.ItemID == param.c17) != null ? result.FirstOrDefault(i => i.ItemID == param.c17).ItemRate : 0,
            //        C18 = result.FirstOrDefault(i => i.ItemID == param.c18) != null ? result.FirstOrDefault(i => i.ItemID == param.c18).ItemRate : 0,
            //    };
            //}
            #endregion
            ratesVM.tireStewardshipFeesRate = new TireStewardshipFeesRateVM(result, param);
            return ratesVM;
        }
        //private RateDetailsVM PopulateEstimateWeightRateVM(IEnumerable<ItemWeight> result, RateDetailsVM ratesVM, RateParamsDTO param)
        //{
        //    getEstimateWeightParam(param);
        //    ratesVM.estimatedWeightsRate = new EstimatedWeightsRateVM()
        //    {
        //        AGLS = result.FirstOrDefault(x => x.ItemID == param.SW.AGLS).StandardWeight,
        //        GOTR = result.FirstOrDefault(x => x.ItemID == param.SW.GOTR).StandardWeight,
        //        IND = result.FirstOrDefault(x => x.ItemID == param.SW.IND).StandardWeight,
        //        LOTR = result.FirstOrDefault(x => x.ItemID == param.SW.LOTR).StandardWeight,
        //        MOTR = result.FirstOrDefault(x => x.ItemID == param.SW.MOTR).StandardWeight,
        //        MT = result.FirstOrDefault(x => x.ItemID == param.SW.MT).StandardWeight,
        //        PLT = result.FirstOrDefault(x => x.ItemID == param.SW.PLT).StandardWeight,
        //        SOTR = result.FirstOrDefault(x => x.ItemID == param.SW.SOTR).StandardWeight,
        //    };
        //    return ratesVM;
        //}
        private RateDetailsVM PopulatePenaltyRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            int iclaimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue;
            int ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue;//6
            ratesVM.penaltyRate = new PenaltyRateVM()
            {
                StewardRemittancePenaltyRate = result.FirstOrDefault(x => x.PaymentType == ratePaymentType && x.ClaimType == iclaimType) != null ? result.FirstOrDefault(x => x.PaymentType == ratePaymentType && x.ClaimType == iclaimType).ItemRate : 0
            };
            return ratesVM;
        }

        #endregion

        #region AddRates       
        public bool AddNewRate(RateDetailsVM commonRateVM, long userId, int category)
        {
            if (category == 8) // add TIPayeeList
            {
                if (
                    //commonRateVM.tiPayee.AssignedHaulerIDs==null || commonRateVM.tiPayee.AssignedHaulerIDs.Count == 0 || 
                    configurationsRepository.IsFutureRateExists(category))
                {
                    return false;
                }
                return AddTIPayeeList(commonRateVM, userId, category);
            }

            if (category == (int)ClaimType.Processor)
            {
                //check global processor exists future rate only.
                if (!commonRateVM.isSpecific && configurationsRepository.IsFutureRateExists(category, false))
                {
                    return false;
                }
            }
            else
            {
                if (configurationsRepository.IsFutureRateExists(category))//already exists future rate, not allow add more.
                {
                    return false;
                }
            }
            DateTime effectiveStartDate = new DateTime(commonRateVM.effectiveStartDate.Year, commonRateVM.effectiveStartDate.Month, 1);
            // save effectiveStartDate Date only. 
            commonRateVM.effectiveStartDate = effectiveStartDate.Date;

            if (effectiveStartDate.Date <= DateTime.Today.Date)//new rate effective date must be future date
            {
                return false;///QATEST
            }

            var rateTransaction = new RateTransaction();
            rateTransaction.CreatedDate = DateTime.UtcNow;
            rateTransaction.CreatedByID = userId;

            rateTransaction.EffectiveStartDate = commonRateVM.effectiveStartDate;
            if (commonRateVM.isSpecific)
            {
                rateTransaction.EffectiveEndDate = commonRateVM.effectiveEndDate.AddMonths(1).AddDays(-1).Date;
            }
            else
            {
                rateTransaction.EffectiveEndDate = DateTime.MaxValue.Date;
            }

            rateTransaction.Category = category;

            if (!string.IsNullOrEmpty(commonRateVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = commonRateVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = userId;
                rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }

            var param = new RateParamsDTO()
            {
                userId = userId,
                categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, commonRateVM.category).Name
            };

            switch (param.categoryName)
            {
                case TreadMarksConstants.TransportationIncentiveRates:
                    AddHaulerRatesToRateTransaction(commonRateVM, rateTransaction, param);
                    break;
                case TreadMarksConstants.CollectionAllowanceRates:
                    if (!BondCollectorRatesToRateTransaction(commonRateVM, rateTransaction, param))
                    {
                        return false;//failed adding rate;
                    }
                    break;
                case TreadMarksConstants.ProcessingIncentiveRates:
                    rateTransaction.IsSpecificRate = commonRateVM.isSpecific;
                    AddProcessingIncentiveRates(commonRateVM, rateTransaction, param);
                    if (commonRateVM.isSpecific && commonRateVM.piSpecificRate != null && commonRateVM.piSpecificRate.AssignedProcessIDs != null)
                    {
                        AddPIRateMapping(commonRateVM, rateTransaction);
                    }
                    break;
                case TreadMarksConstants.ManufacturingIncentiveRates:
                    AddManufacturingIncentiveRates(commonRateVM, rateTransaction, param);
                    break;
                case TreadMarksConstants.TireStewardshipFeeRates:
                    AddStewardRatesToRateTransaction(commonRateVM, rateTransaction, param);
                    break;
                case TreadMarksConstants.RemittancePenaltyRates:
                    AddStewardRemittancePenaltyRates(commonRateVM, rateTransaction, param);
                    break;
            }

            configurationsRepository.AddRates(rateTransaction, param);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.RateKey);

            return true;
        }

        private void AddHaulerRatesToRateTransaction(RateDetailsVM transactionIncentiveRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            getTransactionIncentiveParam(param);
            rateTransaction.Rates = new List<Rate>();
            #region adding Northern Premium Rate

            var n1SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n1SturgeonFallRateOnRoad);

            var n1SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n1SouthRateOnRoad);

            var n1SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n1SouthRateOffRoad);

            var n2SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n2SturgeonFallRateOnRoad);

            var n2SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n2SouthRateOnRoad);

            var n2SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n2SouthRateOffRoad);

            var n3SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n3SturgeonFallRateOnRoad);

            var n3SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n3SouthRateOnRoad);

            var n3SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n3SouthRateOffRoad);

            var n4SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n4SturgeonFallRateOnRoad);

            var n4SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n4SouthRateOnRoad);

            var n4SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n4SouthRateOffRoad);

            #endregion

            #region adding DOT Premium Rate
            //N1 - N4
            var n1DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N1OffRoadRate
            };
            rateTransaction.Rates.Add(n1DOTRate);

            var n2DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N2OffRoadRate
            };
            rateTransaction.Rates.Add(n2DOTRate);

            var n3DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N3OffRoadRate
            };
            rateTransaction.Rates.Add(n3DOTRate);

            var n4DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N4OffRoadRate
            };
            rateTransaction.Rates.Add(n4DOTRate);

            var mooseCreekDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.MooseCreekOffRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekDOTRate);

            var gtaDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.GTAOffRoadRate
            };
            rateTransaction.Rates.Add(gtaDOTRate);

            var wtcDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.WTCOffRoadRate
            };
            rateTransaction.Rates.Add(wtcDOTRate);

            var sturgeonFallsDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.SturgeonFallsOffRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsDOTRate);

            #endregion

            #region adding TI Rate

            var mooseCreekTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOnRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekTIOnRoadRate);

            var mooseCreekTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOffRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekTIOffRoadRate);

            var gtaTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOnRoadRate
            };
            rateTransaction.Rates.Add(gtaTIOnRoadRate);

            var gtaTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOffRoadRate
            };
            rateTransaction.Rates.Add(gtaTIOffRoadRate);

            var wtcTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOnRoadRate
            };
            rateTransaction.Rates.Add(wtcTIOnRoadRate);

            var wtcTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOffRoadRate
            };
            rateTransaction.Rates.Add(wtcTIOffRoadRate);

            var sturgeonFallsTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOnRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsTIOnRoadRate);

            var sturgeonFallsTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOffRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsTIOffRoadRate);

            #endregion
        }
        private void AddStewardRatesToRateTransaction(RateDetailsVM tireStewardshipFeeVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            getTireStewardshipFeeParam(param);

            var tsfItems = tireStewardshipFeeVM.tireStewardshipFeesRate.GetType().GetProperties().Where(x => x.PropertyType.Name == "Decimal");

            var paramItems = param.TFS.GetType().GetProperties();
            foreach (var property in tsfItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.TFS);
                var tsfRate = new Rate
                {
                    ItemID = paramID,
                    ClaimType = param.TFS.claimType,
                    EffectiveStartDate = rateTransaction.EffectiveStartDate,
                    EffectiveEndDate = DateTime.MaxValue.Date,
                    RateTransaction = rateTransaction,
                    ItemRate = (decimal)property.GetValue(tireStewardshipFeeVM.tireStewardshipFeesRate),
                };
                rateTransaction.Rates.Add(tsfRate);
            }
        }
        private void AddManufacturingIncentiveRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            getMaunfacturingIncentiveParam(param);
            var calendared = new Rate
            {
                ItemID = param.MI.calendaredId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Calendared,
            };
            var extruded = new Rate
            {
                ItemID = param.MI.extrudedId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Extruded,
            };
            var molded = new Rate
            {
                ItemID = param.MI.moldedId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Molded,
            };
            rateTransaction.Rates.Add(calendared);
            rateTransaction.Rates.Add(extruded);
            rateTransaction.Rates.Add(molded);
        }

        //assume all rate exists if period exists, those are created by year-end script together
        private bool BondCollectorRatesToRateTransaction(RateDetailsVM collectorRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            List<Period> futurePeriodList = configurationsRepository.LoadFuturePeriodByType(DateTime.Now, ((int)ClaimType.Collector));
            List<Period> bondPeriodList = configurationsRepository.LoadFuturePeriodByType(collectorRateVM.effectiveStartDate, ((int)ClaimType.Collector));
            List<Rate> rates = configurationsRepository.LoadRatesForPeriods(bondPeriodList.AsEnumerable().Select(r => r.ID));

            Period startingPeriod = configurationsRepository.GetPeriodByDate(collectorRateVM.effectiveStartDate, ((int)ClaimType.Collector));

            rates.ForEach(i =>
            {
                i.RateTransactionID = rateTransaction.ID;
                switch (i.item.ShortName)
                {
                    case "PLT":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.PLT;
                        break;
                    case "MT":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.MT;
                        break;
                    case "AGLS":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.AGLS;
                        break;
                    case "IND":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.IND;
                        break;
                    case "SOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.SOTR;
                        break;
                    case "MOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.MOTR;
                        break;
                    case "LOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.LOTR;
                        break;
                    case "GOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.GOTR;
                        break;
                    default:
                        break;
                }
            });
            return true;
        }
        private void AddProcessingIncentiveRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            var piRate = new Rate();
            getProcessingIncentiveParam(param);


            var piItems = commonRateVM.processingIncentiveRate.GetType().GetProperties();
            var paramItems = param.PI.GetType().GetProperties();
            foreach (var property in piItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.PI);
                if (property.Name.IndexOf("OnRoad") > -1)
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.spsPaymentType,
                        ItemType = param.PI.onRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
                else if (property.Name.IndexOf("OffRoad") > -1)
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.spsPaymentType,
                        ItemType = param.PI.offRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
                else
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.pitPaymentType,
                        ItemType = param.PI.offRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
            }
        }
        private void AddPIRateMapping(RateDetailsVM commonRateVM, RateTransaction rateTransaction)
        {
            rateTransaction.PIRateMappings = new List<VendorRate>();
            var piRate = new VendorRate();
            foreach (var item in commonRateVM.piSpecificRate.AssignedProcessIDs)
            {
                piRate = new VendorRate()
                {
                    RateTransactionID = rateTransaction.ID,
                    VendorID = item.ItemID
                };
                rateTransaction.PIRateMappings.Add(piRate);
            }
        }

        private void AddStewardRemittancePenaltyRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            var piRate = new Rate();
            getStewareRemittancePenaltyParam(param);
            var penaltyRate = new Rate
            {
                PaymentType = param.PE.ratePaymentType,
                ClaimType = param.PE.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.penaltyRate.StewardRemittancePenaltyRate,
            };
            rateTransaction.Rates.Add(penaltyRate);
        }

        private bool AddTIPayeeList(RateDetailsVM commonRateVM, long userId, int category)
        {
            DateTime effectiveStartDate = new DateTime(commonRateVM.effectiveStartDate.Year, commonRateVM.effectiveStartDate.Month, 1);
            // save effectiveStartDate Date only. 
            commonRateVM.effectiveStartDate = effectiveStartDate.Date;
            if (effectiveStartDate.Date <= DateTime.Today.Date)//new effective date must be future date
            {
                return false;
            }
            var tiPayeeList = new PayeeList()
            {
                CreatedDate = DateTime.UtcNow,
                CreatedByID = userId,
                EffectiveStartDate = commonRateVM.effectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date
            };


            if (!string.IsNullOrEmpty(commonRateVM.note))
            {
                //add note
                var tiPayeeListNote = new PayeeListNote();

                tiPayeeListNote.PayeeListID = tiPayeeList.ID;
                tiPayeeListNote.Note = commonRateVM.note;
                tiPayeeListNote.CreatedDate = DateTime.UtcNow;
                tiPayeeListNote.UserID = userId;

                tiPayeeList.TIPayeeNotes = new List<PayeeListNote>();
                tiPayeeList.TIPayeeNotes.Add(tiPayeeListNote);
            }
            //allow add empty assigned haulers
            if (commonRateVM.tiPayee != null)
            {
                tiPayeeList.VendorPayees = new List<VendorPayee>();
                var piRate = new VendorPayee();
                foreach (var item in commonRateVM.tiPayee.AssignedHaulerIDs)
                {
                    piRate = new VendorPayee();
                    piRate.PayeeListId = tiPayeeList.ID;
                    piRate.VendorId = item.ItemID;

                    tiPayeeList.VendorPayees.Add(piRate);
                }
            }
            configurationsRepository.AddTIPayeeList(tiPayeeList);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.DirectPayeeVendorsKey);
            return true;
        }

        //private void AddEstimateWeightRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        //{
        //    getEstimateWeightParam(param);
        //    //rateTransaction.ItemWeights = new List<ItemWeight>();

        //    var weightItems = commonRateVM.estimatedWeightsRate.GetType().GetProperties();
        //    var paramItems = param.SW.GetType().GetProperties();
        //    foreach (var property in weightItems)
        //    {
        //        int paramID = (int)paramItems.FirstOrDefault(x => x.Name == property.Name).GetValue(param.SW);
        //        var itemWeight = new ItemWeight()
        //        {
        //            ItemID = paramID,
        //            EffectiveStartDate = rateTransaction.EffectiveStartDate,
        //            EffectiveEndDate = DateTime.MaxValue.Date,
        //            //RateTransactionID = rateTransaction.ID, //for remove RateTransactionId in ItemWeight Table
        //            StandardWeight = (decimal)property.GetValue(commonRateVM.estimatedWeightsRate),
        //            CreatedBy = "System",
        //            ModifiedBy = "System",
        //            CreatedDate = DateTime.UtcNow,
        //            ModifiedDate = DateTime.UtcNow
        //        };
        //        //rateTransaction.ItemWeights.Add(itemWeight);
        //    }
        //}
        #endregion

        #region Update Rates
        public bool updateRate(RateDetailsVM commonRateVM, long userId)
        {
            string categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, commonRateVM.category).Name;
            var param = new RateParamsDTO()
            {
                userId = userId,
                categoryName = categoryName
            };
            if (categoryName == TreadMarksConstants.TransportationIncentiveRates)
            {
                getTransactionIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.CollectionAllowanceRates)
            {
                getCollectorRateParam(param);
            }
            else if (categoryName == TreadMarksConstants.ProcessingIncentiveRates)
            {
                getProcessingIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.ManufacturingIncentiveRates)
            {
                getMaunfacturingIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.TireStewardshipFeeRates)
            {
                getTireStewardshipFeeParam(param);
            }
            else if (param.categoryName == TreadMarksConstants.RemittancePenaltyRates)
            {
                getStewareRemittancePenaltyParam(param);
            }
            else if (param.categoryName == TreadMarksConstants.TIPayee)
            {

            }
            //else if (categoryName == TreadMarksConstants.EstimatedWeightsRates)
            //{
            //    getEstimateWeightParam(param);
            //    //CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);
            //}

            commonRateVM.effectiveEndDate = commonRateVM.isSpecific ? commonRateVM.effectiveEndDate.AddMonths(1).AddDays(-1).Date : DateTime.MaxValue.Date;
            commonRateVM.effectiveStartDate = commonRateVM.effectiveStartDate.Date;
            configurationsRepository.UpdateRates(commonRateVM, param);
            //Refresh Cache
            if (param.categoryName == TreadMarksConstants.TIPayee)
            {
                CachingHelper.RemoveItem(CachKeyManager.DirectPayeeVendorsKey);
            }
            else
            {
                CachingHelper.RemoveItem(CachKeyManager.RateKey);
            }
            return true;
        }
        #endregion

        #region prepare parameters and load data
        private void getCollectorRateParam(RateParamsDTO param)
        {
            param.CA = new CollectorAllowanceParam()
            {
            };
        }
        private void getTransactionIncentiveParam(RateParamsDTO param)
        {
            param.TI = new TransportationIncentiveParam()
            {
                //zone ID
                n1ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N1).ID,
                n2ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N2).ID,
                n3ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N3).ID,
                n4ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N4).ID,

                southZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.South).ID,
                mooseCreekZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.MooseCreek).ID,
                gtaZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.GTA).ID,
                wtcZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.WTC).ID,
                sturgeonFallsZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.SturgeonFalls).ID,
                //payment Type
                paymentTypeNorth = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.NorthernPremium).DefinitionValue,//1
                paymentTypeDOT = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.DOTPremium).DefinitionValue,//2
                paymentTypeProcessorTI = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue,//3
                //claim type
                claimTypeHauler = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Hauler).DefinitionValue,
                claimTypeProcessor = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                //item type
                itemTypeOnRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue,
                itemTypeOffRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue,
            };
        }
        private void getMaunfacturingIncentiveParam(RateParamsDTO param)
        {
            var tempRPM = DataLoader.RPMSPSTransactionProducts;
            param.MI = new ManufacturingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.RPM).DefinitionValue,
                calendaredId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Calendared")).ID,
                extrudedId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Extruded")).ID,
                moldedId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Molded")).ID,
            };
        }
        private void getTireStewardshipFeeParam(RateParamsDTO param)
        {
            param.TFS = new TireStewardshipFeeParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,
                c1 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C1").ID,
                c2 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C2").ID,
                c3 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C3").ID,
                c4 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C4").ID,
                c5 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C5").ID,
                c6 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C6").ID,
                c7 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C7").ID,
                c8 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C8").ID,
                c9 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C9").ID,
                c10 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C10").ID,
                c11 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C11").ID,
                c12 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C12").ID,
                c13 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C13").ID,
                c14 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C14").ID,
                c15 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C15").ID,
                c16 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C16").ID,
                c17 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C17").ID,
                c18 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C18").ID,
                cn1 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C1").Name,
                cn2 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C2").Name,
                cn3 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C3").Name,
                cn4 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C4").Name,
                cn5 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C5").Name,
                cn6 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C6").Name,
                cn7 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C7").Name,
                cn8 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C8").Name,
                cn9 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C9").Name,
                cn10 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C10").Name,
                cn11 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C11").Name,
                cn12 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C12").Name,
                cn13 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C13").Name,
                cn14 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C14").Name,
                cn15 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C15").Name,
                cn16 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C16").Name,
                cn17 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C17").Name,
                cn18 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C18").Name,
            };
        }
        private void getProcessingIncentiveParam(RateParamsDTO param)
        {
            var itemParams = new
            {
                pitItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue,
                spsItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue,
                onRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OnRoad").DefinitionValue,
                offRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OffRoad").DefinitionValue,
                spsPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorSPS").DefinitionValue,//4
                pitPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorPITOutbound").DefinitionValue//5
            };

            var itemList = new
            {
                spsOnRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.onRoad),
                spsOffRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.offRoad),
                pit = DataLoader.Items.Where(i => i.ItemCategory == itemParams.pitItemCategory)
            };

            param.PI = new ProcessingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                pitItemCategory = itemParams.pitItemCategory,
                spsItemCategory = itemParams.spsItemCategory,
                onRoad = itemParams.onRoad,
                offRoad = itemParams.offRoad,
                spsPaymentType = itemParams.spsPaymentType,
                pitPaymentType = itemParams.pitPaymentType,

                spsOffRoad = itemList.spsOffRoad,
                spsOnRoad = itemList.spsOnRoad,
                pit = itemList.pit,

                #region //SPS OnRoad
                tdp1OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //SPS OffRoad
                tdp1OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //PIT
                tdp1 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP1")).ID,
                tdp2 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP2")).ID,
                tdp3 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP3")).ID,
                tdp4FF = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && !i.Description.Contains("PI")).ID,
                tdp4FFNoPI = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && i.Description.Contains("PI")).ID,
                tdp5FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5FT")).ID,
                tdp5NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5NT")).ID,
                tdp6NP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6NP")).ID,
                tdp6FP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6FP")).ID,
                tdp7NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7NT")).ID,
                tdp7FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7FT")).ID,
                transferFibreRubber = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TransferFiberRubber")).ID,
                #endregion
            };
            param.PI.items = new int[] {
                #region // SPS On-Road
                param.PI.tdp1OnRoad,
                param.PI.tdp1FeedstockOnRoad,
                param.PI.tdp2OnRoad,
                param.PI.tdp2FeedstockOnRoad,
                param.PI.tdp3OnRoad,
                param.PI.tdp3FeedstockOnRoad,
                param.PI.tdp4OnRoad,
                param.PI.tdp5OnRoad,
                #endregion

                #region //SPS OffRoad
                param.PI.tdp1OffRoad,
                param.PI.tdp1FeedstockOffRoad,
                param.PI.tdp2OffRoad,
                param.PI.tdp2FeedstockOffRoad,
                param.PI.tdp3OffRoad,
                param.PI.tdp3FeedstockOffRoad,
                param.PI.tdp4OffRoad,
                param.PI.tdp5OffRoad,
                #endregion

                #region //PIT
                param.PI.tdp1,
                param.PI.tdp2,
                param.PI.tdp3,
                param.PI.tdp4FF,
                param.PI.tdp4FFNoPI,
                param.PI.tdp5FT,
                param.PI.tdp5NT,
                param.PI.tdp6NP,
                param.PI.tdp6FP,
                param.PI.tdp7NT,
                param.PI.tdp7FT,
                param.PI.transferFibreRubber,
                #endregion
            };
        }
        //private void getEstimateWeightParam(RateParamsDTO param)
        //{
        //    var itemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "TransactionItem").DefinitionValue;
        //    param.SW = new EstimatedWeightsParam()
        //    {
        //        claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,

        //        PLT = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "PLT" && x.ItemCategory == itemCategory).ID,
        //        MT = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "MT" && x.ItemCategory == itemCategory).ID,
        //        AGLS = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "AGLS" && x.ItemCategory == itemCategory).ID,
        //        IND = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "IND" && x.ItemCategory == itemCategory).ID,
        //        SOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "SOTR" && x.ItemCategory == itemCategory).ID,
        //        MOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "MOTR" && x.ItemCategory == itemCategory).ID,
        //        LOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "LOTR" && x.ItemCategory == itemCategory).ID,
        //        GOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "GOTR" && x.ItemCategory == itemCategory).ID,
        //    };
        //}
        private void getStewareRemittancePenaltyParam(RateParamsDTO param)
        {
            param.PE = new PenaltyParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,
                ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue//6
            };
        }
        private void getTIPayeeParam(RateParamsDTO param)
        {
            param.TIPayee = new TIPayeeParam() { };
        }
        private void loadTI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            getTransactionIncentiveParam(param);
            ratesVM.decimalsize = 3;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var tiRates = configurationsRepository.GetCurrentRates(new int[] { param.TI.claimTypeHauler, param.TI.claimTypeProcessor })
                    .Where(c => c.PaymentType == param.TI.paymentTypeNorth || c.PaymentType == param.TI.paymentTypeProcessorTI || c.PaymentType == param.TI.paymentTypeDOT).ToList();
                ratesVM = PopulateTransportationIncentiveRateVM(tiRates, ratesVM, param.TI);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateTransportationIncentiveRateVM(rates, ratesVM, param.TI);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }
        private void loadCollector(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                int iclaimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Collector).DefinitionValue;
                var tiRates = configurationsRepository.GetCurrentCollectorRates(iclaimType).ToList();
                ratesVM = PopulateCollectorAllowanceRateVM(tiRates, ratesVM);
                ratesVM.collectorAllowanceRate.selectableTransactionPeriods = configurationsRepository.LoadFuturePeriodByType(DateTime.Now, (int)ClaimType.Collector);
                ratesVM.effectiveStartDate = (DateTime)tiRates.FirstOrDefault().period.StartDate;//new DateTime(DateTime.Now.Year + 1, 1, 1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
                ratesVM.collectorAllowanceRate.AvailableLatestPeriod = configurationsRepository.GetLatestPeriod(ratesVM.category);
                ratesVM.collectorAllowanceRate.CurrentPeriod = configurationsRepository.GetPeriodByDate(DateTime.Now, (int)ClaimType.Collector);
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateCollectorAllowanceRateVM(rates, ratesVM);
                ratesVM.collectorAllowanceRate.selectableTransactionPeriods = configurationsRepository.LoadFuturePeriodByType(DateTime.Now, (int)ClaimType.Collector);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().period.StartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().period.EndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().period.StartDate;
                ratesVM.collectorAllowanceRate.AvailableLatestPeriod = configurationsRepository.GetLatestPeriod(ratesVM.category);
                ratesVM.collectorAllowanceRate.CurrentPeriod = configurationsRepository.GetPeriodByDate(ratesVM.effectiveStartDate, (int)ClaimType.Collector);
            }
        }
        private void loadPI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            getProcessingIncentiveParam(param);
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue };
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).ToList();
                ratesVM = PopulateProcessingIncentiveRateVM(tiRates, ratesVM, param.PI);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = ratesVM.isSpecific ? DateTime.Now.AddMonths(1) : DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateProcessingIncentiveRateVM(rates, ratesVM, param.PI);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
            if (ratesVM.isSpecific)
            {
                ratesVM.piSpecificRate = new PISpecificRateVM()
                {
                    AssignedProcessIDs = configurationsRepository.LoadAssignedProcessorIDs(ratesVM.RateTransactionID).OrderBy(x => x.ItemName).ToList(),
                    AvailabledProcessIDs = configurationsRepository.LoadAvailabledProcessorIDs().OrderBy(x => x.ItemName).ToList()
                };
            }
        }
        private void loadMI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.RPM).DefinitionValue };
                var rpmItemIDs = DataLoader.Items.Where(x => x.ItemCategory == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue).Select(x => x.ID).ToList(); // == 8
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(x => x.ItemID.HasValue && rpmItemIDs.Contains(x.ItemID.Value)).ToList();
                ratesVM = PopulateManufacturingIncentiveRateVM(tiRates, ratesVM);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateManufacturingIncentiveRateVM(rates, ratesVM);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }
        private void loadTSF(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            getTireStewardshipFeeParam(param);
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue };
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(x => x.ItemID.HasValue && param.TFS.items.Contains(x.ItemID.Value)).ToList();
                ratesVM = PopulateTireStewardshipFeesRateVM(tiRates, ratesVM, param.TFS);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateTireStewardshipFeesRateVM(rates, ratesVM, param.TFS);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }
        private void loadTIPayee(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;

            }
            else
            {
                ratesVM.notes = configurationsRepository.LoadTIPayeeNoteByID(ratesVM.RateTransactionID);
                var payeeList = configurationsRepository.LoadTIPayeeListByID(ratesVM.RateTransactionID);
                ratesVM.effectiveStartDate = (DateTime)payeeList.EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)payeeList.EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)payeeList.EffectiveStartDate;
            }
            ratesVM.tiPayee = new TIPayeeVM()
            {
                AssignedHaulerIDs = configurationsRepository.LoadAssignedHaulerIDs(ratesVM.RateTransactionID).OrderBy(x => x.ItemName).ToList(),
                AvailabledHaulerIDs = configurationsRepository.LoadAvailabledHaulerIDs(ratesVM.RateTransactionID).OrderBy(x => x.ItemName).ToList()
            };
        }
        //private void loadWeight(RateParamsDTO param, RateDetailsVM ratesVM)
        //{
        //    ratesVM.decimalsize = 0;
        //    var rates = configurationsRepository.GetItemWeights(ratesVM.RateTransactionID);
        //    ratesVM = PopulateEstimateWeightRateVM(rates, ratesVM, param);
        //    ratesVM.effectiveStartDate = ratesVM.RateTransactionID == 0 ? DateTime.Now.AddMonths(1) : (DateTime)rates.FirstOrDefault().EffectiveStartDate;
        //    ratesVM.effectiveEndDate = ratesVM.RateTransactionID == 0 ? DateTime.MaxValue.Date : (DateTime)rates.FirstOrDefault().EffectiveEndDate;
        //    ratesVM.previousEffectiveStartDate = ratesVM.effectiveStartDate;
        //}
        private void loadPenalty(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 0;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue };
                int ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue;//6
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(c => c.PaymentType.HasValue && c.PaymentType.Value == ratePaymentType).ToList();
                ratesVM = PopulatePenaltyRateVM(tiRates, ratesVM);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulatePenaltyRateVM(rates, ratesVM);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }

        private void LoadRateDetails(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            switch (param.categoryName)
            {
                case TreadMarksConstants.TransportationIncentiveRates:
                    loadTI(param, ratesVM);
                    break;
                case TreadMarksConstants.CollectionAllowanceRates:
                    loadCollector(param, ratesVM);
                    break;
                case TreadMarksConstants.ProcessingIncentiveRates:
                    loadPI(param, ratesVM);
                    break;
                case TreadMarksConstants.ManufacturingIncentiveRates:
                    loadMI(param, ratesVM);
                    break;
                case TreadMarksConstants.TireStewardshipFeeRates:
                    loadTSF(param, ratesVM);
                    break;
                //case TreadMarksConstants.EstimatedWeightsRates:
                //    loadWeight(param, ratesVM);
                //    break;
                case TreadMarksConstants.RemittancePenaltyRates:
                    loadPenalty(param, ratesVM);
                    break;
                case TreadMarksConstants.TIPayee:
                    Stopwatch time = Stopwatch.StartNew();
                    loadTIPayee(param, ratesVM);
                    time.Stop();
                    TimeSpan timespan = time.Elapsed;
                    var timer = String.Format("{0:00}:{1:00}:{2:00}", timespan.Minutes, timespan.Seconds, timespan.Milliseconds / 10);
                    break;
            }
        }
        #endregion

        #region Remove Rates
        public bool RemoveRateTransaction(int rateTransactionID, int category)
        {
            var categoryName = AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, category).Name;
            if (category != (int)ClaimType.Collector)
            {
                if (configurationsRepository.RemoveRateTransaction(rateTransactionID, categoryName))
                {
                    if (category == 8)
                    {
                        CachingHelper.RemoveItem(CachKeyManager.DirectPayeeVendorsKey);
                    }
                    else
                    {
                        CachingHelper.RemoveItem(CachKeyManager.RateKey);
                    }
                }
            }
            else
            {
                if (configurationsRepository.UnBondCollectorRatesToRateTransaction(rateTransactionID, category))
                {
                    CachingHelper.RemoveItem(CachKeyManager.RateKey);
                }
            }
            return true;
        }
        #endregion

        #region //Notes
        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            var transactionNote = new RateTransactionNote
            {
                RateTransactionID = Convert.ToInt32(transactionID),
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            configurationsRepository.AddTransactionNote(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }

        public InternalNoteViewModel AddTransactionNote(string transactionID, string notes)
        {
            var transactionNote = new AdminSectionNotes
            {
                Category = transactionID,
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            configurationsRepository.AddAppSettingNotes(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }
        public InternalNoteViewModel AddPayeeNote(int transactionID, string notes)
        {
            var transactionNote = new PayeeListNote
            {
                PayeeListID = Convert.ToInt32(transactionID),
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            configurationsRepository.AddPayeeNote(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }
        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            return configurationsRepository.LoadRateTransactionNoteByID(rateTransactionID);
        }
        public List<InternalNoteViewModel> LoadTIPayeeNotesByID(int rateTransactionID)
        {
            return configurationsRepository.LoadTIPayeeNoteByID(rateTransactionID);
        }
        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return configurationsRepository.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return configurationsRepository.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }
        public List<InternalNoteViewModel> ExportPayeeNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return configurationsRepository.ExportPayeeNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }
        #endregion

        #region //Account Thresholds
        public AccountThresholdsVM LoadAccountThresholds()
        {
            return configurationsRepository.LoadAccountThresholds();
        }
        public bool UpdateAccountThresholds(AccountThresholdsVM vm)
        {
            configurationsRepository.UpdateAccountThresholds(vm, SecurityContextHelper.CurrentUser.Id);

            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
            return true;
        }
        #endregion

        #region Transaction Thresholds
        public TransactionThresholdsVM LoadTransactionThresholds()
        {
            return configurationsRepository.LoadTransactionThresholds();
        }

        public bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            configurationsRepository.UpdateTransactionThresholds(transactionThresholdsVM);

            //refresh cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);

            return true;
        }
        #endregion

        #region TCR Service Threshold
        public PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO)
        {
            return configurationsRepository.LoadTCRServiceThresholds(paramDTO);
        }
        public TCRServiceThresholdListViewModel LoadTCRServiceThresholdList()
        {
            return configurationsRepository.LoadTCRServiceThresholdList();
        }
        public bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            configurationsRepository.AddTCRServiceThreshold(vm, userId);

            return true;
        }

        public bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            return configurationsRepository.UpdateTCRServiceThreshold(vm, userId);
        }

        public bool RemoveTCRServiceThresohold(int id, long userId)
        {
            return configurationsRepository.RemoveTCRServiceThresohold(id, userId);
        }
        public IEnumerable<TCRListViewModel> ExportTCRServiceThresholds()
        {
            return configurationsRepository.ExportTCRServiceThresholds();
        }
        #endregion
    }
}
