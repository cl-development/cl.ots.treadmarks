﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    /// <summary>
    /// A view model for helping northern premium calculation
    /// </summary>
    public class NorthernPremiumDelivery
    {
        public decimal SturgeonFallOnRoad { get; set; }
        public decimal SouthOnRoad { get; set; }

        public decimal SturgeonFallOffRoad { get; set; }
        public decimal SouthOffRoad { get; set; }

        public decimal TotoalOnRoad
        {
            get; set;
        }

        public decimal TotalOffRoad
        {
            get; set;
        }
        public decimal SturgonFallOnRoadPercentage
        {
            get
            {
                return TotoalOnRoad == 0 ? 0 : SturgeonFallOnRoad / TotoalOnRoad;
            }
        }
        public decimal SouthOnRoadPercentage
        {
            get
            {
                return TotoalOnRoad == 0 ? 0 : SouthOnRoad / TotoalOnRoad;
            }
        }

        public decimal SouthOffRoadPercentage
        {
            get
            {
                return TotalOffRoad == 0 ? 0 : SouthOffRoad / TotalOffRoad;
            }
        }
    }
}
