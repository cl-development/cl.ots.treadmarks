﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    public class ClawbackDelivery
    {
        public decimal MooseCreekOnRoadWeight { get; set; }
        public decimal GTAOnRoadWeight { get; set; }
        public decimal WTCOnRoadWeight { get; set; }
        public decimal SturgeonFallsOnRoadWeight { get; set; }

        public decimal TotalOnRoadWeight
        {
            get; set;
        }


        public decimal MooseCreekOffRoadWeight { get; set; }
        public decimal GTAOffRoadWeight { get; set; }
        public decimal WTCOffRoadWeight { get; set; }
        public decimal SturgeonFallsOffRoadWeight { get; set; }

        public decimal TotalOffRoadWeight
        {
            get; set;
        }


        public decimal AverageMooseCreekOnRoadWeight { get; set; }
        public decimal AverageGTAOnRoadWeight { get; set; }
        public decimal AverageWTCOnRoadWeight { get; set; }
        public decimal AverageSturgeonFallsOnRoadWeight { get; set; }

        public decimal TotalAverageOnRoadWeight
        {
            get; set;
        }


        public decimal AverageMooseCreekOffRoadWeight { get; set; }
        public decimal AverageGTAOffRoadWeight { get; set; }
        public decimal AverageWTCOffRoadWeight { get; set; }
        public decimal AverageSturgeonFallsOffRoadWeight { get; set; }

        public decimal TotalAverageOffRoadWeight
        {
            get; set;
        }
    }
}
