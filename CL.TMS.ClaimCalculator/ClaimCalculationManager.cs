﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Communication.Events;
using CL.TMS.IRepository.Claims;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using System.Transactions;
using CL.TMS.Configuration;
using CL.TMS.Common;
using CL.TMS.Repository.Claims;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.Framework.Logging;
using System.Collections.Concurrent;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.ClaimCalculator
{
    /// <summary>
    /// Singleton object to listen ClaimCalculation event
    /// </summary>
    public class ClaimCalculationManager
    {
        private static readonly Lazy<ClaimCalculationManager> lazy = new Lazy<ClaimCalculationManager>(() => new ClaimCalculationManager());
        private IEventAggregator eventAggregator;
        private List<int> claimIds;

        private ClaimCalculationManager()
        {

        }

        public static ClaimCalculationManager Instance
        {
            get { return lazy.Value; }
        }

        public void InitializeClaimCalculationManager(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<ClaimCalculationEvent>().Subscribe(StartCalculateClaim, true);
            claimIds = new List<int>();
        }

        private void StartCalculateClaim(ClaimCalculationPayload claimCalculationPayload)
        {
            if (claimIds.Contains(claimCalculationPayload.ClaimId))
            {
                //Do nothing if the claim calculation is still in processing)
                LogManager.LogWarning(string.Format("Claim {0} is in calculation", claimCalculationPayload.ClaimId));
            }
            else
            {
                claimIds.Add(claimCalculationPayload.ClaimId);
                if (claimCalculationPayload.IsSynchronous)
                {
                    var task = Task.Run(() =>
                    {
                        try
                        {
                            //CalculateClaim(claimCalculationPayload.ClaimId);
                            ClaimCalculationHelper.CalculateClaim(claimCalculationPayload.ClaimId);
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimCalculationPayload", Value = claimCalculationPayload } });
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                    });
                    task.Wait();
                }
                else
                {
                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            //CalculateClaim(claimCalculationPayload.ClaimId);
                            ClaimCalculationHelper.CalculateClaim(claimCalculationPayload.ClaimId);
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                        catch (Exception ex)
                        {
                            LogManager.LogException(ex, new List<LogArg> { new LogArg { Name = "ClaimCalculationPayload", Value = claimCalculationPayload } });
                            claimIds.Remove(claimCalculationPayload.ClaimId);
                        }
                    });
                }
            }
        }

        private void CalculateClaim(int claimId)
        {
            //Prepare data for calculation
            var claimsRepository = new ClaimsRepository();
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetailsForCalculation(claimId, items);

            //Result tables: Claim, ClaimSummary and Claim Payments, claim payment details
            var claimSummary = new ClaimSummary();
            var claimPayments = new List<ClaimPayment>();
            var claimPaymentDetails = new List<ClaimPaymentDetail>();

            //Load inventory opening result
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);

            //Load inventory adjustments
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);

            //Populate Claim Summary Common properties
            claimSummary.Claim = claim;
            claimSummary.UpdatedDate = DateTime.UtcNow;

            //Calculate claim payment
            switch (claim.ClaimType)
            {
                case (int)ClaimType.Collector: //2
                    {
                        //No claim summary need to update
                        claimPayments = CalculateCollectorClaimPayments(claim, claimDetails, inventoryAdjustments, claimPaymentDetails);
                        break;
                    }
                case (int)ClaimType.Hauler: //3
                    {
                        PopulateHaulerClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        claimPayments = CalculateHaulerClaimPayments(claim, claimDetails, inventoryOpeningResult, inventoryAdjustments, claimPaymentDetails);
                        break;
                    }
                case (int)ClaimType.Processor: //4
                    {
                        PopulateProcessClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        claimPayments = CalculateProcessClaimPayments(claim, claimDetails, claimPaymentDetails);
                        break;
                    }
                case (int)ClaimType.RPM: //5
                    {
                        PopulateRPMClaimSummary(claimSummary, inventoryOpeningResult, claimDetails, inventoryAdjustments);
                        claimPayments = CalculateRPMClaimPayments(claim, claimDetails, claimPaymentDetails);
                        break;
                    }
            }

            //Calculate Claim
            var isClaimTaxExempt = false;
            if ((claim.ClaimType == (int)ClaimType.Collector) || (claim.ClaimType == (int)ClaimType.Hauler))
            {
                isClaimTaxExempt = claimsRepository.IsTaxExempt(claimId);
            }
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            CalculateClaim(claim, claimPayments, claimPaymentAdjustments, isClaimTaxExempt);

            //Save to database
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //Fixed -480: Exclude approved and in batch claim calculation
                if (!((claim.Status == "Approved") || (claim.Status == "Receive Payment")))
                {
                    claimsRepository.UpdateClaimSummary(claimSummary);
                    claimsRepository.UpdateClaimPayments(claim.ID, claimPayments);
                    claimsRepository.UpdateClaimPaymentDetails(claim.ID, claimPaymentDetails);
                    claimsRepository.UpdateClaimForCalculation(claim);
                }
                else
                {
                    claimsRepository.UpdateClaimPaymentDetails(claim.ID, claimPaymentDetails);
                }

                transactionScope.Complete();
            }
        }

        private void PopulateProcessClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;

            var query = claimDetails
                .GroupBy(c => new { c.TransactionType, c.Direction })
                .Select(
                    c =>
                        new
                        {
                            Name = c.Key,
                            ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero)),
                            extraDOR = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)i.ExtraDORTotal), 4, MidpointRounding.AwayFromZero))
                        });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight + c.extraDOR;
                }

            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateRPMClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustOverallOnRoad = 0;
            decimal adjustOverallOffRoad = 0;
            decimal adjustInOnRoad = 0;
            decimal adjustInOffRoad = 0;
            decimal adjustOutOnRoad = 0;
            decimal adjustOutOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 0)
                {
                    adjustOverallOnRoad = c.OnRoad;
                    adjustOverallOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 1)
                {
                    adjustInOnRoad = c.OnRoad;
                    adjustInOffRoad = c.OffRoad;
                }
                if (c.Name.Direction == 2)
                {
                    adjustOutOnRoad = c.OnRoad;
                    adjustOutOffRoad = c.OffRoad;
                }
            });

            var totalAdjustOnRoad = adjustOverallOnRoad + adjustInOnRoad - adjustOutOnRoad;
            var totalAdjustOffRoad = adjustOverallOffRoad + adjustInOffRoad - adjustOutOffRoad;

            decimal totalIn = 0;
            decimal totalOut = 0;
            var query = claimDetails
             .GroupBy(c => new { c.TransactionType, c.Direction })
             .Select(c => new
             {
                 Name = c.Key,
                 ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero))
             });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && c.Name.Direction)
                {
                    totalIn += c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPSR && !c.Name.Direction)
                {
                    totalOut += c.ActualWeight;
                }
            });

            claimSummary.TotalClosing = claimSummary.TotalOpening + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);
            claimSummary.TotalClosingEstimated = claimSummary.TotalOpeningEstimated + totalAdjustOnRoad + totalAdjustOffRoad + DataConversion.ConvertTonToKg((double)totalIn) - DataConversion.ConvertTonToKg((double)totalOut);

        }

        private void PopulateHaulerClaimSummary(ClaimSummary claimSummary, InventoryOpeningSummary inventoryOpeningResult, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            claimSummary.EligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoad;
            claimSummary.EligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoad;
            claimSummary.IneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoad;
            claimSummary.IneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoad;
            claimSummary.TotalOpening = inventoryOpeningResult.TotalOpening;

            claimSummary.EligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            claimSummary.EligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            claimSummary.IneligibleOpeningOnRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            claimSummary.IneligibleOpeningOffRoadEstimated = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;
            claimSummary.TotalOpeningEstimated = inventoryOpeningResult.TotalOpeningEstimated;

            var adjustQuery = inventoryAdjustments
                .GroupBy(c => new { c.IsEligible, c.Direction })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.AdjustmentWeightOnroad), OffRoad = c.Sum(i => i.AdjustmentWeightOffroad) });

            decimal adjustEligibleOverallOnRoad = 0;
            decimal adjustEligibleOverallOffRoad = 0;
            decimal adjustIneligibleOverallOnRoad = 0;
            decimal adjustIneligibleOverallOffRoad = 0;
            decimal eligibleAdjustOnRoad = 0;
            decimal eligibleAdjustOffRoad = 0;
            decimal ineligibleAdjustOnRoad = 0;
            decimal ineligibleAdjustOffRoad = 0;
            decimal outEligibleAdjustOnRoad = 0;
            decimal outEligibleAdjustOffRoad = 0;
            decimal outIneligibleAdjustOnRoad = 0;
            decimal outIneligibleAdjustOffRoad = 0;

            adjustQuery.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustEligibleOverallOnRoad = c.OnRoad;
                    adjustEligibleOverallOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 0)
                {
                    adjustIneligibleOverallOnRoad = c.OnRoad;
                    adjustIneligibleOverallOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 1)
                {
                    eligibleAdjustOnRoad = c.OnRoad;
                    eligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 1)
                {
                    ineligibleAdjustOnRoad = c.OnRoad;
                    ineligibleAdjustOffRoad = c.OffRoad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outEligibleAdjustOnRoad = c.OnRoad;
                    outEligibleAdjustOffRoad = c.OffRoad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 2)
                {
                    outIneligibleAdjustOnRoad = c.OnRoad;
                    outIneligibleAdjustOffRoad = c.OffRoad;
                }
            });
            var totalEligibleAdjustOnRoad = adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - outEligibleAdjustOnRoad;
            var totalEligibleAdjustOffRoad = adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - outEligibleAdjustOffRoad;
            var totalIneligibleAdjustOnRoad = adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - outIneligibleAdjustOnRoad;
            var totalIneligibleAdjustOffRoad = adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - outIneligibleAdjustOffRoad;

            decimal eligibleOnRoad = 0;
            decimal eligibleOffRoad = 0;
            decimal ineligibleOnRoad = 0;
            decimal ineligibleOffRoad = 0;
            decimal outEligibleOnRoad = 0;
            decimal outEligibleOffRoad = 0;
            decimal outIneligibleOnRoad = 0;
            decimal outIneligibleOffRoad = 0;
            decimal eligibleOnRoadEstimated = 0;
            decimal eligibleOffRoadEstimated = 0;
            decimal ineligibleOnRoadEstimated = 0;
            decimal ineligibleOffRoadEstimated = 0;
            decimal outEligibleOnRoadEstimated = 0;
            decimal outEligibleOffRoadEstimated = 0;
            decimal outIneligibleOnRoadEstimated = 0;
            decimal outIneligibleOffRoadEstimated = 0;

            claimDetails.ForEach(c =>
            {
                //Inbound Eligible
                if (c.Direction && (c.TransactionType == "TCR" || c.TransactionType == "DOT" || c.TransactionType == "STC"))
                {
                    eligibleOnRoad += c.OnRoad;
                    eligibleOffRoad += c.OffRoad;
                    eligibleOnRoadEstimated += c.EstimatedOnRoad;
                    eligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Inbound Ineligible
                if (c.Direction && (c.TransactionType == "UCR" || c.TransactionType == "HIT"))
                {
                    ineligibleOnRoad += c.OnRoad;
                    ineligibleOffRoad += c.OffRoad;
                    ineligibleOnRoadEstimated += c.EstimatedOnRoad;
                    ineligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Eligible
                if (!c.Direction && c.TransactionType == "PTR")
                {
                    outEligibleOnRoad += c.OnRoad;
                    outEligibleOffRoad += c.OffRoad;
                    outEligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outEligibleOffRoadEstimated += c.EstimatedOffRoad;
                }

                //Outbound Ineligible
                if (!c.Direction && (c.TransactionType == "RTR" || c.TransactionType == "HIT"))
                {
                    outIneligibleOnRoad += c.OnRoad;
                    outIneligibleOffRoad += c.OffRoad;
                    outIneligibleOnRoadEstimated += c.EstimatedOnRoad;
                    outIneligibleOffRoadEstimated += c.EstimatedOffRoad;
                }
            });

            //Calculate outbound total eligible and ineligible
            var totalInboundOnroad = inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad + inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad;
            var eligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoad + eligibleAdjustOnRoad + eligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutboundOnroad = outEligibleOnRoad + outIneligibleOnRoad + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroad = Math.Round(totalOutboundOnroad * eligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentage = (totalInboundOnroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoad + ineligibleAdjustOnRoad + ineligibleOnRoad) / totalInboundOnroad : 0;
            var totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad * ineligibleOnroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroad == 0)
            {
                totalOutIneligibleOnroad = Math.Round(totalOutboundOnroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOnroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated;
            var eligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + eligibleAdjustOnRoad + eligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutboundOnroadEstimated = outEligibleOnRoadEstimated + outIneligibleOnRoadEstimated + outEligibleAdjustOnRoad + outIneligibleAdjustOnRoad;
            var totalOutEligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * eligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOnroadPercentageEstimated = (totalInboundOnroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + ineligibleAdjustOnRoad + ineligibleOnRoadEstimated) / totalInboundOnroadEstimated : 0;
            var totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated * ineligibleOnroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOnroadEstimated == 0)
            {
                totalOutIneligibleOnroadEstimated = Math.Round(totalOutboundOnroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroad = inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad + inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad;
            var eligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoad + eligibleAdjustOffRoad + eligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutboundOffroad = outEligibleOffRoad + outIneligibleOffRoad + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroad = Math.Round(totalOutboundOffroad * eligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentage = (totalInboundOffroad > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoad + ineligibleAdjustOffRoad + ineligibleOffRoad) / totalInboundOffroad : 0;
            var totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad * ineligibleOffroadPercentage, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroad == 0)
            {
                totalOutIneligibleOffroad = Math.Round(totalOutboundOffroad, 4, MidpointRounding.AwayFromZero);
            }

            var totalInboundOffroadEstimated = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated;
            var eligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + eligibleAdjustOffRoad + eligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutboundOffroadEstimated = outEligibleOffRoadEstimated + outIneligibleOffRoadEstimated + outEligibleAdjustOffRoad + outIneligibleAdjustOffRoad;
            var totalOutEligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * eligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);

            var ineligibleOffroadPercentageEstimated = (totalInboundOffroadEstimated > 0) ? (inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + ineligibleAdjustOffRoad + ineligibleOffRoadEstimated) / totalInboundOffroadEstimated : 0;
            var totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated * ineligibleOffroadPercentageEstimated, 4, MidpointRounding.AwayFromZero);
            if (totalInboundOffroadEstimated == 0)
            {
                totalOutIneligibleOffroadEstimated = Math.Round(totalOutboundOffroadEstimated, 4, MidpointRounding.AwayFromZero);
            }

            claimSummary.EligibleClosingOnRoad = claimSummary.EligibleOpeningOnRoad + eligibleOnRoad + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroad;
            claimSummary.EligibleClosingOffRoad = claimSummary.EligibleOpeningOffRoad + eligibleOffRoad + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroad;
            claimSummary.IneligibleClosingOnRoad = claimSummary.IneligibleOpeningOnRoad + ineligibleOnRoad + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroad;
            claimSummary.IneligibleClosingOffRoad = claimSummary.IneligibleOpeningOffRoad + ineligibleOffRoad + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroad;

            claimSummary.EligibleClosingOnRoadEstimated = claimSummary.EligibleOpeningOnRoadEstimated + eligibleOnRoadEstimated + adjustEligibleOverallOnRoad + eligibleAdjustOnRoad - totalOutEligibleOnroadEstimated;
            claimSummary.EligibleClosingOffRoadEstimated = claimSummary.EligibleOpeningOffRoadEstimated + eligibleOffRoadEstimated + adjustEligibleOverallOffRoad + eligibleAdjustOffRoad - totalOutEligibleOffroadEstimated;
            claimSummary.IneligibleClosingOnRoadEstimated = claimSummary.IneligibleOpeningOnRoadEstimated + ineligibleOnRoadEstimated + adjustIneligibleOverallOnRoad + ineligibleAdjustOnRoad - totalOutIneligibleOnroadEstimated;
            claimSummary.IneligibleClosingOffRoadEstimated = claimSummary.IneligibleOpeningOffRoadEstimated + ineligibleOffRoadEstimated + adjustIneligibleOverallOffRoad + ineligibleAdjustOffRoad - totalOutIneligibleOffroadEstimated;

            claimSummary.TotalClosing = claimSummary.EligibleClosingOnRoad + claimSummary.EligibleClosingOffRoad + claimSummary.IneligibleClosingOnRoad + claimSummary.IneligibleClosingOffRoad;
            claimSummary.TotalClosingEstimated = claimSummary.EligibleClosingOnRoadEstimated + claimSummary.EligibleClosingOffRoadEstimated + claimSummary.IneligibleClosingOnRoadEstimated + claimSummary.IneligibleClosingOffRoadEstimated;

        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, ClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoad = currentClaimSummary.EligibleOpeningOnRoad != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoad = currentClaimSummary.EligibleOpeningOffRoad != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = currentClaimSummary.IneligibleOpeningOnRoad != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = currentClaimSummary.IneligibleOpeningOffRoad != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = currentClaimSummary.TotalOpeningEstimated != null ? (decimal)currentClaimSummary.TotalOpeningEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = currentClaimSummary.TotalOpening != null ? (decimal)currentClaimSummary.TotalOpening : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoad = previousClaimSummary.EligibleClosingOnRoad != null ? (decimal)previousClaimSummary.EligibleClosingOnRoad : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoad = previousClaimSummary.EligibleClosingOffRoad != null ? (decimal)previousClaimSummary.EligibleClosingOffRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = previousClaimSummary.IneligibleClosingOnRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoad : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = previousClaimSummary.IneligibleClosingOffRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoad : 0;
                    inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
                }
            }
            return inventoryOpeningSummary;
        }

        private void CalculateClaim(Claim claim, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments, bool isClaimTaxExempt)
        {
            var taxRate = 0.13m;
            if (claim.ClaimType == (int)ClaimType.Collector)
            {
                if (claim.Participant.IsTaxExempt)
                {
                    //No HST
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                claim.IsTaxApplicable = !claim.Participant.IsTaxExempt;
            }
            if (claim.ClaimType == (int)ClaimType.Hauler)
            {
                //OTSTM2-1344 Hauler PTR changes
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.HaulerPTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                if (claim.Participant.IsTaxExempt)
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var paymentAmount = ptrAmount + paymentAmountPositive - paymentAmountNegative;
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
                else
                {
                    var paymentAmountPositive = claimPayments.Where(c => c.PaymentType != (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var paymentAmountNegative = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var noTaxAdjustmentAmount = Math.Round(claimPaymentAdjustments.Where(c => c.PaymentType == (int)ClaimPaymentType.IneligibleInventoryPayment).Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
                    var taxAdjustment = adjustmentAmount - noTaxAdjustmentAmount;
                    var taxAmount = Math.Round((ptrAmount + paymentAmountPositive + taxAdjustment) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = ptrAmount + paymentAmountPositive - paymentAmountNegative + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                claim.IsTaxApplicable = !claim.Participant.IsTaxExempt;
            }
            if (claim.ClaimType == (int)ClaimType.Processor)
            {
                var ptrAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var spsAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var pitAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var dorAmount = claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var paymentAmount = ptrAmount + spsAmount + pitAmount + dorAmount;
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
            }

            if (claim.ClaimType == (int)ClaimType.RPM)
            {
                var paymentAmount = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                var adjustmentAmount = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

                if (claim.IsTaxApplicable ?? false)
                {
                    var taxAmount = Math.Round((paymentAmount + adjustmentAmount) * taxRate, 2, MidpointRounding.AwayFromZero);
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount + taxAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = taxAmount;
                }
                else
                {
                    claim.ClaimsAmountTotal = paymentAmount + adjustmentAmount;
                    claim.AdjustmentTotal = adjustmentAmount;
                    claim.TotalTax = 0;
                }
            }
        }

        private List<ClaimPayment> CalculateRPMClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            //SPS MI
            var spsClaimDetails = claimDetails.Where(c => c.TransactionType == "SPSR").ToList();
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.RPMSPS,
                    ItemType = transactionItem.Item.ItemType,
                    Rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            });

            AddClaimPaymentDetailForRPM(claim, spsClaimDetails, claimPaymentDetails);
            return claimPayments;
        }

        private void AddClaimPaymentDetailForRPM(Claim claim, List<ClaimDetailViewModel> spsClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var item = transactionItem.Item;
                var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.AverageWeight), 4, MidpointRounding.AwayFromZero);
                var rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m;
                var qty = transactionItem.Quantity ?? 0;
                var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.RPMSPS);
                //Fixed RPM report issue
                claimPaymentDetail.ItemDescription = transactionItem.ItemDescription;
                claimPaymentDetail.ItemCode = transactionItem.ItemCode;
                claimPaymentDetails.Add(claimPaymentDetail);
            });
        }

        private List<ClaimPayment> CalculateProcessClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            //PTR TI Calculation
            var vendorPrimaryAddress = claim.Participant.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
            if (vendorPrimaryAddress != null)
            {
                var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(vendorPrimaryAddress.PostalCode, 2);
                var claimPeriodDate = claim.ClaimPeriod.StartDate;
                //load whole payee list, since any Hauler can be Payee in this claim.
                List<VendorPayeeVM> VendorPayeeVMList = DataLoader.LoadDirectPayeeVendors.Select(c => new VendorPayeeVM { VendorId = c.VendorId, EffectiveStartDate = c.EffectiveStartDate, EffectiveEndDate = c.EffectiveEndDate }).ToList();

                //OTSTM2-1343 processor PTR changes
                var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR" && !VendorPayeeVMList.Any(x => x.VendorId == c.Transaction.OutgoingId && x.EffectiveStartDate <= c.Transaction.TransactionDate && x.EffectiveEndDate >= c.Transaction.TransactionDate)).ToList();
                ptrClaimDetails.ForEach(c =>
                {
                    var query = c.TransactionItems.GroupBy(i => i.Item.ItemType)
                                .Select(o => new { ItemType = o.Key, Weight = o.Sum(p => p.ActualWeight) });
                    query.ToList().ForEach(m =>
                    {
                        var ptrItemRate = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == m.ItemType && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                        var ptrRate = ptrItemRate != null ? ptrItemRate.ItemRate : 0;
                        var claimPayment = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(m.Weight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.PTR,
                            ItemType = m.ItemType,
                            Rate = ptrRate,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPayment);
                    });
                });

                AddClaimPaymentDetailForPTRTI(claim, deliveryZoneId, claimPeriodDate, ptrClaimDetails, claimPaymentDetails);
            }
            //SPS PI
            var spsClaimDetails = claimDetails.Where(c => c.TransactionType == "SPS").ToList();
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.SPS,
                    ItemType = transactionItem.Item.ItemType,
                    Rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            });

            AddClaimPaymentDetailForSPSPI(claim, spsClaimDetails, claimPaymentDetails);

            //PIT Outbound
            var pitClaimDetails = claimDetails.Where(c => c.TransactionType == "PIT" && !c.Direction).ToList();
            pitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    var claimPayment = new ClaimPayment
                    {
                        ClaimID = claim.ID,
                        Weight = Math.Round(DataConversion.ConvertKgToTon((double)i.ActualWeight), 4, MidpointRounding.AwayFromZero),
                        PaymentType = (int)ClaimPaymentType.PITOutbound,
                        ItemType = i.Item.ItemType,
                        Rate = i.Rate != null ? (decimal)i.Rate : 0m,
                        CreatedDate = DateTime.UtcNow
                    };
                    claimPayments.Add(claimPayment);
                });
            });

            AddClaimPaymentDetailForPIT(claim, pitClaimDetails, claimPaymentDetails);

            //DOR PI
            if (vendorPrimaryAddress != null)
            {
                var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(vendorPrimaryAddress.PostalCode, 2);
                var claimPeriodDate = claim.ClaimPeriod.StartDate;
                var dorClaimDetails = claimDetails.Where(c => c.TransactionType == "DOR").ToList();
                dorClaimDetails.ForEach(c =>
                {
                    //When tire rims selected
                    if (c.Transaction.MaterialType == 4)
                    {
                        var ptrItemRate = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 1 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                        var ptrRate = ptrItemRate != null ? ptrItemRate.ItemRate : 0;
                        var onroadWeight = c.OnroadExtraDORForTireRims;
                        var claimPayment = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(onroadWeight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.DOR,
                            ItemType = 1,
                            Rate = 0 - ptrRate,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPayment);

                        var ptrItemRateOffroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 2 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                        var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.ItemRate : 0;
                        var offroadWeight = c.OffroadExtraDORForTireRims;
                        var claimPaymentOffroad = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(offroadWeight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.DOR,
                            ItemType = 2,
                            Rate = 0 - ptrRateOffroad,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPaymentOffroad);


                    }
                    //When ‘Used Tire Sale’ selected
                    if (c.Transaction.MaterialType == 5)
                    {
                        var query = c.TransactionItems.GroupBy(i => i.Item.ItemType)
                            .Select(o => new { ItemType = o.Key, Weight = o.Sum(p => p.ActualWeight) });
                        query.ToList().ForEach(m =>
                        {
                            var ptrItemRate = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == m.ItemType && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                            var ptrRate = ptrItemRate != null ? ptrItemRate.ItemRate : 0;
                            var claimPayment = new ClaimPayment
                            {
                                ClaimID = claim.ID,
                                Weight = Math.Round(m.Weight, 4, MidpointRounding.AwayFromZero),
                                PaymentType = (int)ClaimPaymentType.DOR,
                                ItemType = m.ItemType,
                                Rate = 0 - ptrRate,
                                CreatedDate = DateTime.UtcNow
                            };
                            claimPayments.Add(claimPayment);
                        });
                    }
                });

                AddClaimPaymentDetailForDORPI(claim, deliveryZoneId, claimPeriodDate, dorClaimDetails, claimPaymentDetails);
            }
            return claimPayments;
        }

        private void AddClaimPaymentDetailForDORPI(Claim claim, int deliveryZoneId, DateTime claimPeriodDate, List<ClaimDetailViewModel> dorClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var ptrItemRateOnroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 1 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
            var ptrRateOnroad = ptrItemRateOnroad != null ? ptrItemRateOnroad.ItemRate : 0;
            var ptrItemRateOffroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 2 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
            var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.ItemRate : 0;
            ptrRateOnroad = 0 - ptrRateOnroad;
            ptrRateOffroad = 0 - ptrRateOffroad;

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dorClaimDetails.ForEach(c =>
            {
                // Fixed OTSTM2-458  Trash, Steel and Fiber
                if ((c.Transaction.MaterialType == 1) || (c.Transaction.MaterialType == 2) || (c.Transaction.MaterialType == 3))
                {
                    //Add claim payment detail at transaction level with itemid=0
                    var item = new Item();
                    item.ID = 0;
                    item.ItemType = 1;
                    item.ShortName = "";
                    var dorTotal = c.ExtraDORTotal;
                    var onroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item, 0, dorTotal, dorTotal, 0, 0, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(onroadClaimPaymentDetail);
                }

                if (c.Transaction.MaterialType == 4)
                {
                    //Add claim payment detail at transaction level with itemid=0
                    var item = new Item();
                    item.ID = 0;
                    item.ItemType = 1;
                    item.ShortName = "";
                    var onroadWeight = c.OnroadExtraDORForTireRims;
                    var onroadAverageWeight = onroadWeight;
                    var amount = Math.Round(onroadWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var onroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item, ptrRateOnroad, onroadWeight, onroadAverageWeight, 0, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(onroadClaimPaymentDetail);

                    var item1 = new Item();
                    item1.ID = 0;
                    item1.ItemType = 2;
                    item1.ShortName = "";
                    var offroadWeight = c.OffroadExtraDORForTireRims;
                    var offroadAverageWeight = offroadWeight;
                    var offroadAmount = Math.Round(offroadWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var offroadClaimPaymentDetail = AddClaimPaymentDetail(claim, c, item1, ptrRateOffroad, offroadWeight, offroadAverageWeight, 0, offroadAmount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                    claimPaymentDetails.Add(offroadClaimPaymentDetail);

                }
                //When ‘Used Tire Sale’ selected
                if (c.Transaction.MaterialType == 5)
                {
                    if (c.PLTActualWeight > 0)
                    {
                        var amount = Math.Round(c.PLTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                        var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, ptrRateOnroad, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(pltClaimPaymentDetail);
                    }
                    if (c.MTActualWeight > 0)
                    {
                        var amount = Math.Round(c.MTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                        var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, ptrRateOnroad, c.MTActualWeight, c.MTAverageWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(mtClaimPaymentDetail);
                    }
                    if (c.AGLSActualWeight > 0)
                    {
                        var amount = Math.Round(c.AGLSActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, ptrRateOffroad, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(aglsClaimPaymentDetail);
                    }
                    if (c.INDActualWeight > 0)
                    {
                        var amount = Math.Round(c.INDActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, ptrRateOffroad, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(indClaimPaymentDetail);
                    }
                    if (c.SOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.SOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, ptrRateOffroad, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(sotrClaimPaymentDetail);
                    }
                    if (c.MOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.MOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, ptrRateOffroad, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(motrClaimPaymentDetail);
                    }
                    if (c.LOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.LOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, ptrRateOffroad, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(lotrClaimPaymentDetail);
                    }
                    if (c.GOTRActualWeight > 0)
                    {
                        var amount = Math.Round(c.GOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                        var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, ptrRateOffroad, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOR);
                        claimPaymentDetails.Add(gotrClaimPaymentDetail);
                    }
                }

            });
        }

        private void AddClaimPaymentDetailForPIT(Claim claim, List<ClaimDetailViewModel> pitClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            pitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    var item = i.Item;
                    var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)i.ActualWeight), 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)i.AverageWeight), 4, MidpointRounding.AwayFromZero);
                    var rate = i.Rate != null ? (decimal)i.Rate : 0m;
                    var qty = i.Quantity ?? 0;
                    var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.PITOutbound);
                    claimPaymentDetails.Add(claimPaymentDetail);
                });
            });
        }

        private void AddClaimPaymentDetailForSPSPI(Claim claim, List<ClaimDetailViewModel> spsClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            spsClaimDetails.ForEach(c =>
            {
                var transactionItem = c.TransactionItems.FirstOrDefault();
                var item = transactionItem.Item;
                var actualWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.ActualWeight), 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(DataConversion.ConvertKgToTon((double)transactionItem.AverageWeight), 4, MidpointRounding.AwayFromZero);
                var rate = transactionItem.Rate != null ? (decimal)transactionItem.Rate : 0m;
                var qty = transactionItem.Quantity ?? 0;
                var amount = Math.Round(actualWeight * rate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, item, rate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Ton, (int)ClaimPaymentType.SPS);
                claimPaymentDetails.Add(claimPaymentDetail);
            });
        }

        private void AddClaimPaymentDetailForPTRTI(Claim claim, int deliveryZoneId, DateTime claimPeriodDate, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var ptrItemRateOnroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 1 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
            var ptrRateOnroad = ptrItemRateOnroad != null ? ptrItemRateOnroad.ItemRate : 0;
            var ptrItemRateOffroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 2 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
            var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.ItemRate : 0;
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            ptrClaimDetails.ForEach(c =>
            {
                if (c.PLTActualWeight > 0)
                {
                    var amount = Math.Round(c.PLTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, ptrRateOnroad, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(pltClaimPaymentDetail);
                }
                if (c.MTActualWeight > 0)
                {
                    var amount = Math.Round(c.MTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, ptrRateOnroad, c.MTActualWeight, c.MTAverageWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(mtClaimPaymentDetail);
                }
                if (c.AGLSActualWeight > 0)
                {
                    var amount = Math.Round(c.AGLSActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, ptrRateOffroad, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(aglsClaimPaymentDetail);
                }
                if (c.INDActualWeight > 0)
                {
                    var amount = Math.Round(c.INDActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, ptrRateOffroad, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(indClaimPaymentDetail);
                }
                if (c.SOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.SOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, ptrRateOffroad, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(sotrClaimPaymentDetail);
                }
                if (c.MOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.MOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, ptrRateOffroad, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(motrClaimPaymentDetail);
                }
                if (c.LOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.LOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, ptrRateOffroad, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(lotrClaimPaymentDetail);
                }
                if (c.GOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.GOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, ptrRateOffroad, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PTR);
                    claimPaymentDetails.Add(gotrClaimPaymentDetail);
                }
            });
        }

        #region Collector Claim Calculation
        private List<ClaimPayment> CalculateCollectorClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            var eligibleClaimDetails = claimDetails.Where(c => c.IsEligible).ToList();
            var eligibleClaimInventoryAdjustments = claimInventoryAdjustments.Where(c => c.IsEligible).ToList();
            //Add PLT Payment
            //Get PLT Payment Rate
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var pltItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == pltItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var pltRate = pltItemRate != null ? pltItemRate.ItemRate : 0;
            var pltItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
                pltItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * pltRate, 3, MidpointRounding.AwayFromZero);
                    var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, pltRate, c.PLTActualWeight, c.PLTAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.PLT);
                    claimPaymentDetails.Add(pltClaimPaymentDetail);
                }
            });

            var pltClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = pltItemQty,
                PaymentType = (int)ClaimPaymentType.PLT,
                ItemType = 1,
                Rate = pltRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(pltClaimPayment);

            var pltItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                pltItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == pltItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (pltItemAdjustQty != 0)
            {
                var pltAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = pltItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.PLT,
                    ItemType = 1,
                    Rate = pltRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(pltAdjustClaimPayment);
            }

            //Add MT to GOTR payment
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var mtItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == mtItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var mtRate = mtItemRate != null ? mtItemRate.ItemRate : 0;
            var mtItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
                mtItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * mtRate, 3, MidpointRounding.AwayFromZero);
                    var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, mtRate, c.MTActualWeight, c.MTAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.MT);
                    claimPaymentDetails.Add(mtClaimPaymentDetail);
                }
            });

            var mtClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = mtItemQty,
                PaymentType = (int)ClaimPaymentType.MT,
                ItemType = 1,
                Rate = mtRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(mtClaimPayment);

            var mtItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                mtItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == mtItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (mtItemAdjustQty != 0)
            {
                var mtAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = mtItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.MT,
                    ItemType = 1,
                    Rate = mtRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(mtAdjustClaimPayment);
            }


            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var aglsItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == aglsItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var aglsRate = aglsItemRate != null ? aglsItemRate.ItemRate : 0;
            var aglsItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
                aglsItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * aglsRate, 3, MidpointRounding.AwayFromZero);
                    var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, aglsRate, c.AGLSActualWeight, c.AGLSAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.AGLS);
                    claimPaymentDetails.Add(aglsClaimPaymentDetail);
                }
            });


            var aglsClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = aglsItemQty,
                PaymentType = (int)ClaimPaymentType.AGLS,
                ItemType = 1,
                Rate = aglsRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(aglsClaimPayment);

            var aglsItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                aglsItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == aglsItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (aglsItemAdjustQty != 0)
            {
                var aglsAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = aglsItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.AGLS,
                    ItemType = 1,
                    Rate = aglsRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(aglsAdjustClaimPayment);
            }

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var indItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == indItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var indRate = indItemRate != null ? indItemRate.ItemRate : 0;
            var indItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
                indItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * indRate, 3, MidpointRounding.AwayFromZero);
                    var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, indRate, c.INDActualWeight, c.INDAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IND);
                    claimPaymentDetails.Add(indClaimPaymentDetail);
                }
            });


            var indClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = indItemQty,
                PaymentType = (int)ClaimPaymentType.IND,
                ItemType = 1,
                Rate = indRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(indClaimPayment);

            var indItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                indItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == indItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (indItemAdjustQty != 0)
            {
                var indAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = indItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.IND,
                    ItemType = 1,
                    Rate = indRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(indAdjustClaimPayment);
            }

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var sotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == sotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var sotrRate = sotrItemRate != null ? sotrItemRate.ItemRate : 0;
            var sotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
                sotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * sotrRate, 3, MidpointRounding.AwayFromZero);
                    var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, sotrRate, c.SOTRActualWeight, c.SOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.SOTR);
                    claimPaymentDetails.Add(sotrClaimPaymentDetail);
                }
            });


            var sotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = sotrItemQty,
                PaymentType = (int)ClaimPaymentType.SOTR,
                ItemType = 1,
                Rate = sotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(sotrClaimPayment);

            var sotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                sotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == sotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (sotrItemAdjustQty != 0)
            {
                var sotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = sotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.SOTR,
                    ItemType = 1,
                    Rate = sotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(sotrAdjustClaimPayment);
            }

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var motrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == motrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var motrRate = motrItemRate != null ? motrItemRate.ItemRate : 0;
            var motrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
                motrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * motrRate, 3, MidpointRounding.AwayFromZero);
                    var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, motrRate, c.MOTRActualWeight, c.MOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.MOTR);
                    claimPaymentDetails.Add(motrClaimPaymentDetail);
                }
            });


            var motrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = motrItemQty,
                PaymentType = (int)ClaimPaymentType.MOTR,
                ItemType = 1,
                Rate = motrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(motrClaimPayment);

            var motrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                motrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == motrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (motrItemAdjustQty != 0)
            {
                var motrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = motrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.MOTR,
                    ItemType = 1,
                    Rate = motrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(motrAdjustClaimPayment);
            }

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var lotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == lotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var lotrRate = lotrItemRate != null ? lotrItemRate.ItemRate : 0;
            var lotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
                lotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * lotrRate, 3, MidpointRounding.AwayFromZero);
                    var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, lotrRate, c.LOTRActualWeight, c.LOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.LOTR);
                    claimPaymentDetails.Add(lotrClaimPaymentDetail);
                }
            });


            var lotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = lotrItemQty,
                PaymentType = (int)ClaimPaymentType.LOTR,
                ItemType = 1,
                Rate = lotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(lotrClaimPayment);

            var lotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                lotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == lotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (lotrItemAdjustQty != 0)
            {
                var lotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = lotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.LOTR,
                    ItemType = 1,
                    Rate = lotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(lotrAdjustClaimPayment);
            }

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            var gotrItemRate = DataLoader.Rates.FirstOrDefault(c => c.ClaimType == 2 && c.ItemID == gotrItem.ID && c.PeriodID == claim.ClaimPeriodId);
            var gotrRate = gotrItemRate != null ? gotrItemRate.ItemRate : 0;
            var gotrItemQty = 0;
            eligibleClaimDetails.ForEach(c =>
            {
                var qty = (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
                gotrItemQty += qty;

                //Add claimpaymentdetail
                if (qty > 0)
                {
                    var amount = Math.Round(qty * gotrRate, 3, MidpointRounding.AwayFromZero);
                    var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, gotrRate, c.GOTRActualWeight, c.GOTRAverageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.GOTR);
                    claimPaymentDetails.Add(gotrClaimPaymentDetail);
                }
            });


            var gotrClaimPayment = new ClaimPayment
            {
                ClaimID = claim.ID,
                Weight = gotrItemQty,
                PaymentType = (int)ClaimPaymentType.GOTR,
                ItemType = 1,
                Rate = gotrRate,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(gotrClaimPayment);

            var gotrItemAdjustQty = 0;
            eligibleClaimInventoryAdjustments.ForEach(c =>
            {
                gotrItemAdjustQty += c.ClaimInventoryAdjustItems.Where(i => i.ItemId == gotrItem.ID).Sum(q => q.QtyAdjustment);
            });

            if (gotrItemAdjustQty != 0)
            {
                var gotrAdjustClaimPayment = new ClaimPayment
                {
                    ClaimID = claim.ID,
                    Weight = gotrItemAdjustQty,
                    PaymentType = (int)ClaimPaymentType.GOTR,
                    ItemType = 1,
                    Rate = gotrRate,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(gotrAdjustClaimPayment);
            }

            return claimPayments;
        }

        private ClaimPaymentDetail AddClaimPaymentDetail(Claim claim, ClaimDetailViewModel c, Item item, decimal rate, decimal actualWeight, decimal averageWeight, int qty, decimal amount, int unitType, int paymentType)
        {
            var claimPaymentDetail = new ClaimPaymentDetail();
            claimPaymentDetail.ClaimId = claim.ID;
            claimPaymentDetail.TransactionId = c.Transaction.ID;
            claimPaymentDetail.ItemId = item.ID;
            claimPaymentDetail.ItemType = item.ItemType;
            claimPaymentDetail.ItemName = item.ShortName;
            claimPaymentDetail.Quantity = qty;
            claimPaymentDetail.AverageWeight = averageWeight;
            claimPaymentDetail.ActualWeight = actualWeight;
            claimPaymentDetail.Rate = rate;
            claimPaymentDetail.Amount = amount;
            claimPaymentDetail.UnitType = unitType;
            claimPaymentDetail.PaymentType = paymentType;
            claimPaymentDetail.TransactionTypeCode = c.Transaction.TransactionTypeCode;
            claimPaymentDetail.ClaimPeriodId = claim.ClaimPeriodId;
            claimPaymentDetail.PeriodName = claim.ClaimPeriod.ShortName;
            claimPaymentDetail.PeriodStartDate = claim.ClaimPeriod.StartDate;
            claimPaymentDetail.PeriodEndDate = claim.ClaimPeriod.EndDate;
            claimPaymentDetail.VendorId = claim.ParticipantId;
            claimPaymentDetail.VendorType = claim.Participant.VendorType;
            claimPaymentDetail.BusinessName = claim.Participant.BusinessName;
            claimPaymentDetail.RegistrationNumber = claim.Participant.Number;
            claimPaymentDetail.CreatedDate = DateTime.UtcNow;
            return claimPaymentDetail;
        }
        #endregion

        #region Hauler Claim Calculation
        private List<ClaimPayment> CalculateHaulerClaimPayments(Claim claim, List<ClaimDetailViewModel> claimDetails, InventoryOpeningSummary inventoryOpeningResult, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimPayments = new List<ClaimPayment>();
            var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR").ToList();
            if (ptrClaimDetails.Count > 0)
            {
                var claimPeriodDate = claim.ClaimPeriod.StartDate;

                //Northern Premium Calculation
                NorthernPremiumCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails);

                //DOT Premium Calculation
                DOTPremiumCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails);

                //TI Claw-back Calculation
                TIClawbackCalculation(claim, claimPayments, ptrClaimDetails, claimDetails, inventoryOpeningResult, claimPeriodDate, claimInventoryAdjustments, claimPaymentDetails);
            }

            //RTR ClaimPaymentDetail (weight only)
            var rtrClaimDetails = claimDetails.Where(c => c.TransactionType == "RTR").ToList();

            if (rtrClaimDetails.Count > 0)
            {
                AddClaimPaymentDetailForRTRTransactions(claim, rtrClaimDetails, claimPaymentDetails);
            }

            //HIT ClaimPaymentDetail (weight only)
            var hitClaimDetails = claimDetails.Where(c => c.TransactionType == "HIT" && !c.Direction).ToList();
            if (hitClaimDetails.Count > 0)
            {
                AddClaimPaymentDetailForHITTransactions(claim, hitClaimDetails, claimPaymentDetails);
            }

            //OTSTM2-1344 Hauler PTR changes
            List<VendorPayeeVM> VendorPayeeVMList = DataLoader.LoadDirectPayeeVendors.Where(i => i.VendorId == claim.ParticipantId) //load for current Hauler only
                .Select(c => new VendorPayeeVM { VendorId = c.VendorId, EffectiveStartDate = c.EffectiveStartDate, EffectiveEndDate = c.EffectiveEndDate }).ToList();
            List<ClaimDetailViewModel> haulerPTRClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR" && VendorPayeeVMList.Any(x => x.EffectiveStartDate <= c.Transaction.TransactionDate && x.EffectiveEndDate >= c.Transaction.TransactionDate)).ToList(); //transaction list of direct pay to HAULER in this claim (base on Transaction.TransactionDate)

            //PTR TI Calculation
            if (haulerPTRClaimDetails.Count > 0)
            {
                var claimPeriodDate = claim.ClaimPeriod.StartDate;
                haulerPTRClaimDetails.ForEach(c =>
                {
                    var query = c.TransactionItems.GroupBy(i => i.Item.ItemType)
                                .Select(o => new { ItemType = o.Key, Weight = o.Sum(p => p.ActualWeight) });
                    query.ToList().ForEach(m =>
                    {
                        var vendorAddress = c.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
                        var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(vendorAddress.PostalCode, 2);

                        var ptrItemRate = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == m.ItemType && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                        var ptrRate = ptrItemRate != null ? ptrItemRate.ItemRate : 0;
                        var claimPayment = new ClaimPayment
                        {
                            ClaimID = claim.ID,
                            Weight = Math.Round(m.Weight, 4, MidpointRounding.AwayFromZero),
                            PaymentType = (int)ClaimPaymentType.HaulerPTR,
                            ItemType = m.ItemType,
                            Rate = ptrRate,
                            CreatedDate = DateTime.UtcNow
                        };
                        claimPayments.Add(claimPayment);
                    });
                });
                AddClaimPaymentDetailForHaulerPTRTI(claim, claimPeriodDate, haulerPTRClaimDetails, claimPaymentDetails);
            }
            return claimPayments;
        }

        private void AddClaimPaymentDetailForHaulerPTRTI(Claim claim, DateTime claimPeriodDate, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            ptrClaimDetails.ForEach(c =>
            {
                var vendorAddress = c.Transaction.IncomingVendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
                var deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(vendorAddress.PostalCode, 2);

                var ptrItemRateOnroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 1 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                var ptrRateOnroad = ptrItemRateOnroad != null ? ptrItemRateOnroad.ItemRate : 0;
                var ptrItemRateOffroad = DataLoader.Rates.FirstOrDefault(r => r.PaymentType == 3 && r.ClaimType == 4 && r.ItemType == 2 && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriodDate && r.EffectiveEndDate >= claimPeriodDate);
                var ptrRateOffroad = ptrItemRateOffroad != null ? ptrItemRateOffroad.ItemRate : 0;

                if (c.PLTActualWeight > 0)
                {
                    var amount = Math.Round(c.PLTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var pltClaimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, ptrRateOnroad, c.PLTActualWeight, c.PLTAverageWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(pltClaimPaymentDetail);
                }
                if (c.MTActualWeight > 0)
                {
                    var amount = Math.Round(c.MTActualWeight * ptrRateOnroad, 3, MidpointRounding.AwayFromZero);
                    var mtClaimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, ptrRateOnroad, c.MTActualWeight, c.MTAverageWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(mtClaimPaymentDetail);
                }
                if (c.AGLSActualWeight > 0)
                {
                    var amount = Math.Round(c.AGLSActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var aglsClaimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, ptrRateOffroad, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(aglsClaimPaymentDetail);
                }
                if (c.INDActualWeight > 0)
                {
                    var amount = Math.Round(c.INDActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var indClaimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, ptrRateOffroad, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(indClaimPaymentDetail);
                }
                if (c.SOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.SOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var sotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, ptrRateOffroad, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(sotrClaimPaymentDetail);
                }
                if (c.MOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.MOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var motrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, ptrRateOffroad, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(motrClaimPaymentDetail);
                }
                if (c.LOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.LOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var lotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, ptrRateOffroad, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(lotrClaimPaymentDetail);
                }
                if (c.GOTRActualWeight > 0)
                {
                    var amount = Math.Round(c.GOTRActualWeight * ptrRateOffroad, 3, MidpointRounding.AwayFromZero);
                    var gotrClaimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, ptrRateOffroad, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.HaulerPTR);
                    claimPaymentDetails.Add(gotrClaimPaymentDetail);
                }
            });
        }

        private void AddClaimPaymentDetailForHITTransactions(Claim claim, List<ClaimDetailViewModel> hitClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            hitClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    if (i.ActualWeight > 0)
                    {
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, i.Item, 0, i.ActualWeight, i.AverageWeight, i.Quantity ?? 0, 0, TreadMarksConstants.Kg, 0);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                });
            });
        }

        private void AddClaimPaymentDetailForRTRTransactions(Claim claim, List<ClaimDetailViewModel> rtrClaimDetails, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            rtrClaimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    if (i.ActualWeight > 0)
                    {
                        var claimPaymentDetail = AddClaimPaymentDetail(claim, c, i.Item, 0, i.ActualWeight, i.AverageWeight, i.Quantity ?? 0, 0, TreadMarksConstants.Kg, 0);
                        claimPaymentDetails.Add(claimPaymentDetail);
                    }
                });
            });
        }

        private void TIClawbackCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, InventoryOpeningSummary inventoryOpeningResult, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimId = claim.ID;
            //1. Get delivery weight percentage for onroad nad offroad based on delivery zone
            var clawbackDelivery = GetClawbackDelivery(ptrClaimDetails, claimInventoryAdjustments);

            //2. Get ineligible transactions ---Fix TI calculation with using estimated weight for percentage 
            var ineligibleClaimDetails = claimDetails.Where(c => c.Direction && (c.TransactionType == "UCR" || c.TransactionType == "HIT")).ToList();
            var totalIneligibleOnRoad = ineligibleClaimDetails.Sum(c => c.EstimatedOnRoad);
            var totalIneligibleOffRoad = ineligibleClaimDetails.Sum(c => c.EstimatedOffRoad);

            var inboundOnroadAdjust = claimInventoryAdjustments.Where(c => c.Direction == 1).Sum(c => c.AdjustmentWeightOnroad);
            var inboundOffroadAdjust = claimInventoryAdjustments.Where(c => c.Direction == 1).Sum(c => c.AdjustmentWeightOffroad);

            var inboundOnroadAdjustIneligible = claimInventoryAdjustments.Where(c => c.Direction == 1 && !c.IsEligible).Sum(c => c.AdjustmentWeightOnroad);
            var inboundOffroadAdjustIneligible = claimInventoryAdjustments.Where(c => c.Direction == 1 && !c.IsEligible).Sum(c => c.AdjustmentWeightOffroad);

            var ineligibleOnRoad = totalIneligibleOnRoad + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + inboundOnroadAdjustIneligible;
            var ineligibleOffRoad = totalIneligibleOffRoad + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + inboundOffroadAdjustIneligible;

            var inboundTotalOnRoad = claimDetails.Where(c => c.Direction).Sum(c => c.EstimatedOnRoad);
            var inboundTotalOffRoad = claimDetails.Where(c => c.Direction).Sum(c => c.EstimatedOffRoad);
            var totalOnRoad = inboundTotalOnRoad + inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated + inboundOnroadAdjust;
            var totalOffRoad = inboundTotalOffRoad + inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated + inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated + inboundOffroadAdjust;

            decimal onroadWeightPercentage = 0m;
            if (totalOnRoad > 0)
                onroadWeightPercentage = ineligibleOnRoad / totalOnRoad;
            decimal offroadWeightPercentage = 0m;
            if (totalOffRoad > 0)
                offroadWeightPercentage = ineligibleOffRoad / totalOffRoad;

            //3. Get Rate
            var mooseCreekZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.MooseCreek).ID;
            var gtaZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.GTA).ID;
            var wtcZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.WTC).ID;
            var sturgeonFallsZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.SturgeonFalls).ID;

            var mooseCreekZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == mooseCreekZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneOnRoadRate = mooseCreekZoneOnRoadItemRate != null ? mooseCreekZoneOnRoadItemRate.ItemRate : 0;
            var mooseCreekZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == mooseCreekZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneOffRoadRate = mooseCreekZoneOffRoadItemRate != null ? mooseCreekZoneOffRoadItemRate.ItemRate : 0;

            var gtaZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == gtaZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneOnRoadRate = gtaZoneOnRoadItemRate != null ? gtaZoneOnRoadItemRate.ItemRate : 0;
            var gtaZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == gtaZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneOffRoadRate = gtaZoneOffRoadItemRate != null ? gtaZoneOffRoadItemRate.ItemRate : 0;

            var wtcZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == wtcZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneOnRoadRate = wtcZoneOnRoadItemRate != null ? wtcZoneOnRoadItemRate.ItemRate : 0;
            var wtcZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == wtcZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneOffRoadRate = wtcZoneOffRoadItemRate != null ? wtcZoneOffRoadItemRate.ItemRate : 0;

            var sturgeonFallsZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == sturgeonFallsZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneOnRoadRate = sturgeonFallsZoneOnRoadItemRate != null ? sturgeonFallsZoneOnRoadItemRate.ItemRate : 0;
            var sturgeonFallsZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == sturgeonFallsZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneOffRoadRate = sturgeonFallsZoneOffRoadItemRate != null ? sturgeonFallsZoneOffRoadItemRate.ItemRate : 0;

            AddClawbackClaimPayment(claimId, clawbackDelivery, onroadWeightPercentage, offroadWeightPercentage, mooseCreekZoneOnRoadRate, mooseCreekZoneOffRoadRate, gtaZoneOnRoadRate, gtaZoneOffRoadRate, wtcZoneOnRoadRate, wtcZoneOffRoadRate, sturgeonFallsZoneOnRoadRate, sturgeonFallsZoneOffRoadRate, claimPayments);

            //Add claim payment detail
            AddClawbackClaimPaymentDetails(claim, ineligibleClaimDetails, clawbackDelivery, totalOnRoad, totalOffRoad, mooseCreekZoneOnRoadRate, mooseCreekZoneOffRoadRate, gtaZoneOnRoadRate, gtaZoneOffRoadRate, wtcZoneOnRoadRate, wtcZoneOffRoadRate, sturgeonFallsZoneOnRoadRate, sturgeonFallsZoneOffRoadRate, claimPaymentDetails);

        }

        private void AddClawbackClaimPaymentDetails(Claim claim, List<ClaimDetailViewModel> ineligibleClaimDetails, ClawbackDelivery clawbackDelivery, decimal totalOnRoad, decimal totalOffRoad, decimal mooseCreekZoneOnRoadRate, decimal mooseCreekZoneOffRoadRate, decimal gtaZoneOnRoadRate, decimal gtaZoneOffRoadRate, decimal wtcZoneOnRoadRate, decimal wtcZoneOffRoadRate, decimal sturgeonFallsZoneOnRoadRate, decimal sturgeonFallsZoneOffRoadRate, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            ineligibleClaimDetails.ForEach(c =>
            {
                if (clawbackDelivery.MooseCreekOnRoadWeight > 0)
                {
                    AddTIOnroadClaimPaymentDetails(claim, clawbackDelivery.AverageMooseCreekOnRoadWeight, clawbackDelivery.MooseCreekOnRoadWeight, totalOnRoad, mooseCreekZoneOnRoadRate, claimPaymentDetails, c, pltItem, mtItem);
                }
                if (clawbackDelivery.MooseCreekOffRoadWeight > 0)
                {
                    ADDTIOffroadClaimPaymentDetails(claim, clawbackDelivery.AverageMooseCreekOffRoadWeight, clawbackDelivery.MooseCreekOffRoadWeight, totalOffRoad, mooseCreekZoneOffRoadRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
                if (clawbackDelivery.GTAOnRoadWeight > 0)
                {
                    AddTIOnroadClaimPaymentDetails(claim, clawbackDelivery.AverageGTAOnRoadWeight, clawbackDelivery.GTAOnRoadWeight, totalOnRoad, gtaZoneOnRoadRate, claimPaymentDetails, c, pltItem, mtItem);
                }
                if (clawbackDelivery.GTAOffRoadWeight > 0)
                {
                    ADDTIOffroadClaimPaymentDetails(claim, clawbackDelivery.AverageGTAOffRoadWeight, clawbackDelivery.GTAOffRoadWeight, totalOffRoad, gtaZoneOffRoadRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
                if (clawbackDelivery.WTCOnRoadWeight > 0)
                {
                    AddTIOnroadClaimPaymentDetails(claim, clawbackDelivery.AverageWTCOnRoadWeight, clawbackDelivery.WTCOnRoadWeight, totalOnRoad, wtcZoneOnRoadRate, claimPaymentDetails, c, pltItem, mtItem);
                }
                if (clawbackDelivery.WTCOffRoadWeight > 0)
                {
                    ADDTIOffroadClaimPaymentDetails(claim, clawbackDelivery.AverageWTCOffRoadWeight, clawbackDelivery.WTCOffRoadWeight, totalOffRoad, wtcZoneOffRoadRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
                if (clawbackDelivery.SturgeonFallsOnRoadWeight > 0)
                {
                    AddTIOnroadClaimPaymentDetails(claim, clawbackDelivery.AverageSturgeonFallsOnRoadWeight, clawbackDelivery.SturgeonFallsOnRoadWeight, totalOnRoad, sturgeonFallsZoneOnRoadRate, claimPaymentDetails, c, pltItem, mtItem);
                }
                if (clawbackDelivery.SturgeonFallsOffRoadWeight > 0)
                {
                    ADDTIOffroadClaimPaymentDetails(claim, clawbackDelivery.AverageSturgeonFallsOffRoadWeight, clawbackDelivery.SturgeonFallsOffRoadWeight, totalOffRoad, sturgeonFallsZoneOffRoadRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                }
            });
        }

        private void ADDTIOffroadClaimPaymentDetails(Claim claim, decimal offRoadEstimatedWeight, decimal offRoadActualWeight, decimal totalOffRoad, decimal zoneOffRoadRate, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            if (c.AGLSAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.AGLSAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, zoneOffRoadRate, weight, estimatedWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.INDAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.INDAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, zoneOffRoadRate, weight, estimatedWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.SOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.SOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, zoneOffRoadRate, weight, estimatedWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.MOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, zoneOffRoadRate, weight, estimatedWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.LOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.LOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, zoneOffRoadRate, weight, estimatedWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.GOTRAverageWeight > 0)
            {
                decimal offroadWeightPercentage = 0m;
                if (totalOffRoad > 0)
                    offroadWeightPercentage = c.GOTRAverageWeight / totalOffRoad;
                var weight = Math.Round(offRoadActualWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOffRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(offRoadEstimatedWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, zoneOffRoadRate, weight, estimatedWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddTIOnroadClaimPaymentDetails(Claim claim, decimal onroadEstimatedWeight, decimal onroadActualWeight, decimal totalOnRoad, decimal zoneOnRoadRate, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item pltItem, Item mtItem)
        {
            if (c.PLTAverageWeight > 0)
            {
                decimal onroadWeightPercentage = 0m;
                if (totalOnRoad > 0)
                    onroadWeightPercentage = c.PLTAverageWeight / totalOnRoad;
                var weight = Math.Round(onroadActualWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOnRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(onroadEstimatedWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, zoneOnRoadRate, weight, estimatedWeight, c.PLTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MTAverageWeight > 0)
            {
                decimal onroadWeightPercentage = 0m;
                if (totalOnRoad > 0)
                    onroadWeightPercentage = c.MTAverageWeight / totalOnRoad;
                var weight = Math.Round(onroadActualWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var amount = Math.Round(weight * zoneOnRoadRate, 3, MidpointRounding.AwayFromZero);
                var estimatedWeight = Math.Round(onroadEstimatedWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, zoneOnRoadRate, weight, estimatedWeight, c.MTQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.IneligibleInventoryPayment);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddClawbackClaimPayment(int claimId, ClawbackDelivery clawbackDelivery, decimal onroadWeightPercentage, decimal offroadWeightPercentage, decimal mooseCreekZoneOnRoadRate, decimal mooseCreekZoneOffRoadRate, decimal gtaZoneOnRoadRate, decimal gtaZoneOffRoadRate, decimal wtcZoneOnRoadRate, decimal wtcZoneOffRoadRate, decimal sturgeonFallsZoneOnRoadRate, decimal sturgeonFallsZoneOffRoadRate, List<ClaimPayment> claimPayments)
        {
            if (clawbackDelivery.MooseCreekOnRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.MooseCreekOnRoadWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = mooseCreekZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.MooseCreek,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = mooseCreekZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.MooseCreek,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.MooseCreekOffRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.MooseCreekOffRoadWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = mooseCreekZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.MooseCreek,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = mooseCreekZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.MooseCreek,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.GTAOnRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.GTAOnRoadWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = gtaZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.GTA,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = gtaZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.GTA,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.GTAOffRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.GTAOffRoadWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = gtaZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.GTA,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = gtaZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.GTA,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.WTCOnRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.WTCOnRoadWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = wtcZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.WTC,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = wtcZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.WTC,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.WTCOffRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.WTCOffRoadWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = wtcZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.WTC,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = wtcZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.WTC,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.SturgeonFallsOnRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.SturgeonFallsOnRoadWeight * onroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = sturgeonFallsZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 1,
                    Rate = sturgeonFallsZoneOnRoadRate,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (clawbackDelivery.SturgeonFallsOffRoadWeight > 0)
            {
                var weight = Math.Round(clawbackDelivery.SturgeonFallsOffRoadWeight * offroadWeightPercentage, 4, MidpointRounding.AwayFromZero);
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = weight,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = sturgeonFallsZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.IneligibleInventoryPayment,
                    ItemType = 2,
                    Rate = sturgeonFallsZoneOffRoadRate,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
        }

        private ClawbackDelivery GetClawbackDelivery(List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimInventoryAdjustment> claimInventoryAdjustments)
        {
            var result = new ClawbackDelivery();
            var query = ptrClaimDetails
            .Where(c => c.Transaction.ToPostalCode != null)
            .GroupBy(c => new { c.Transaction.ToPostalCode })
            .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.OnRoad), OffRoad = c.Sum(i => i.OffRoad), AverageOnRoad = c.Sum(i => i.EstimatedOnRoad), AverageOffRoad = c.Sum(i => i.EstimatedOffRoad) });

            var secondQuery = query.Select(c => new
            {
                DeliveryZone = ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2) != null ? ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2).Name : string.Empty,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad,
                AverageOnRoad = c.AverageOnRoad,
                AverageOffRoad = c.AverageOffRoad
            }).GroupBy(c => new { c.DeliveryZone })
              .Select(c => new
              {
                  Name = c.Key,
                  OnRoad = c.Sum(i => i.OnRoad),
                  OffRoad = c.Sum(i => i.OffRoad),
                  AverageOnRoad = c.Sum(i => i.AverageOnRoad),
                  AverageOffRoad = c.Sum(i => i.AverageOffRoad)
              });
            secondQuery.ToList().ForEach(c =>
            {
                if (c.Name.DeliveryZone == TreadMarksConstants.MooseCreek)
                {
                    result.MooseCreekOnRoadWeight = c.OnRoad;
                    result.MooseCreekOffRoadWeight = c.OffRoad;
                    result.AverageMooseCreekOnRoadWeight = c.AverageOnRoad;
                    result.AverageMooseCreekOffRoadWeight = c.AverageOffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.GTA)
                {
                    result.GTAOnRoadWeight = c.OnRoad;
                    result.GTAOffRoadWeight = c.OffRoad;
                    result.AverageGTAOnRoadWeight = c.AverageOnRoad;
                    result.AverageGTAOffRoadWeight = c.AverageOffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.WTC)
                {
                    result.WTCOnRoadWeight = c.OnRoad;
                    result.WTCOffRoadWeight = c.OffRoad;
                    result.AverageWTCOnRoadWeight = c.AverageOnRoad;
                    result.AverageWTCOffRoadWeight = c.AverageOffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.SturgeonFalls)
                {
                    result.SturgeonFallsOnRoadWeight = c.OnRoad;
                    result.SturgeonFallsOffRoadWeight = c.OffRoad;
                    result.AverageSturgeonFallsOnRoadWeight = c.AverageOnRoad;
                    result.AverageSturgeonFallsOffRoadWeight = c.AverageOffRoad;
                }
            });

            result.TotalOnRoadWeight = ptrClaimDetails.Sum(c => c.OnRoad);
            result.TotalOffRoadWeight = ptrClaimDetails.Sum(c => c.OffRoad);

            result.TotalAverageOnRoadWeight = ptrClaimDetails.Sum(c => c.EstimatedOnRoad);
            result.TotalAverageOffRoadWeight = ptrClaimDetails.Sum(c => c.EstimatedOffRoad);

            //Add inventory adjustment
            var outboundIneligibleOnroadAdjust = claimInventoryAdjustments.Where(c => !c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOnroad);
            var outboundIneligibleOffroadAdjust = claimInventoryAdjustments.Where(c => !c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);
            result.TotalOnRoadWeight = result.TotalOnRoadWeight + outboundIneligibleOnroadAdjust;
            result.TotalOffRoadWeight = result.TotalOffRoadWeight + outboundIneligibleOffroadAdjust;

            result.TotalAverageOnRoadWeight = result.TotalAverageOnRoadWeight + outboundIneligibleOnroadAdjust;
            result.TotalAverageOffRoadWeight = result.TotalAverageOffRoadWeight + outboundIneligibleOffroadAdjust;
            return result;
        }

        private void DOTPremiumCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimId = claim.ID;
            //1. Get delivery weight percentage for offroad based on delivery zone
            var dotPremiumDelivery = GetDOTPremiumDelivery(ptrClaimDetails, claimInventoryAdjustments);

            //2. Get DOT transactions
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == "DOT").ToList();
            var query = dotClaimDetails.GroupBy(c => new { c.Transaction.FromPostalCode })
                .Select(c => new { Name = c.Key, OffRoad = c.Sum(i => i.OffRoad) });
            var secondQuery = query.Select(c => new
            {
                PickupZone = ClaimHelper.GetZoneByPostCode(c.Name.FromPostalCode, 1) != null ? ClaimHelper.GetZoneByPostCode(c.Name.FromPostalCode, 1).Name : string.Empty,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.PickupZone })
            .Select(c => new
            {
                Name = c.Key,
                OffRoad = c.Sum(i => i.OffRoad)
            });

            //2. Get Rate
            var n1ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N1).ID;
            var n2ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N2).ID;
            var n3ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N3).ID;
            var n4ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N4).ID;

            var southZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.South).ID;
            var mooseCreekZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.MooseCreek).ID;
            var gtaZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.GTA).ID;
            var wtcZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.WTC).ID;
            var sturgeonFallsZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.SturgeonFalls).ID;

            //For northern pick ups, rate will be based on Pick up zone only.
            var n1ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n1ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1ZoneRate = n1ZoneItemRate != null ? n1ZoneItemRate.ItemRate : 0;
            var n2ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n2ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2ZoneRate = n2ZoneItemRate != null ? n2ZoneItemRate.ItemRate : 0;
            var n3ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n3ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3ZoneRate = n3ZoneItemRate != null ? n3ZoneItemRate.ItemRate : 0;
            var n4ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n4ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4ZoneRate = n4ZoneItemRate != null ? n4ZoneItemRate.ItemRate : 0;

            // For South DOT pick ups, rate will be based on pickup zone and drop off zone.
            var mooseCreekZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == mooseCreekZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneRate = mooseCreekZoneItemRate != null ? mooseCreekZoneItemRate.ItemRate : 0;
            var gtaZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == gtaZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneRate = gtaZoneItemRate != null ? gtaZoneItemRate.ItemRate : 0;
            var wtcZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == wtcZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneRate = wtcZoneItemRate != null ? wtcZoneItemRate.ItemRate : 0;
            var sturgeonFallsZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == sturgeonFallsZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneRate = sturgeonFallsZoneItemRate != null ? sturgeonFallsZoneItemRate.ItemRate : 0;

            //3. Calculating
            //Adding N1 row
            var n1PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N1);
            if (n1PickupZone != null)
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N1, claimId, n1PickupZone.OffRoad, n1ZoneRate, claimPayments);
            }
            else
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N1, claimId, 0, n1ZoneRate, claimPayments);
            }

            //Adding N2 row
            var n2PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N2);
            if (n2PickupZone != null)
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N2, claimId, n2PickupZone.OffRoad, n2ZoneRate, claimPayments);
            }
            else
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N2, claimId, 0, n2ZoneRate, claimPayments);
            }

            //Adding N3 row
            var n3PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N3);
            if (n3PickupZone != null)
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N3, claimId, n3PickupZone.OffRoad, n3ZoneRate, claimPayments);
            }
            else
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N3, claimId, 0, n3ZoneRate, claimPayments);
            }

            //Adding N4 row
            var n4PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N4);
            if (n4PickupZone != null)
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N4, claimId, n4PickupZone.OffRoad, n4ZoneRate, claimPayments);
            }
            else
            {
                AddDOTPremiumClaimPayment(TreadMarksConstants.N4, claimId, 0, n4ZoneRate, claimPayments);
            }

            //Adding South row
            var southPickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.South);
            if (southPickupZone != null)
            {
                AddDOTPremiumClaimPayment(claimId, southPickupZone.OffRoad, mooseCreekZoneRate, gtaZoneRate, wtcZoneRate, sturgeonFallsZoneRate, claimPayments, dotPremiumDelivery);
            }
            else
            {
                AddDOTPremiumClaimPayment(claimId, 0, mooseCreekZoneRate, gtaZoneRate, wtcZoneRate, sturgeonFallsZoneRate, claimPayments, dotPremiumDelivery);
            }

            //Adding claim payment details
            AddDOTPremiumClaimPaymentDetails(claim, dotClaimDetails, n1ZoneRate, n2ZoneRate, n3ZoneRate, n4ZoneRate, mooseCreekZoneRate, gtaZoneRate, wtcZoneRate, sturgeonFallsZoneRate, dotPremiumDelivery, claimPaymentDetails);
        }

        private void AddDOTPremiumClaimPaymentDetails(Claim claim, List<ClaimDetailViewModel> dotClaimDetails, decimal n1ZoneRate, decimal n2ZoneRate, decimal n3ZoneRate, decimal n4ZoneRate, decimal mooseCreekZoneRate, decimal gtaZoneRate, decimal wtcZoneRate, decimal sturgeonFallsZoneRate, DOTPremiumDelivery dotPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dotClaimDetails.ForEach(c =>
            {
                var pickupZone = ClaimHelper.GetZoneByPostCode(c.Transaction.FromPostalCode, 1);
                if (pickupZone != null)
                {
                    //N1
                    if (pickupZone.Name == TreadMarksConstants.N1)
                    {
                        AddDOTPickupZoneClaimPaymentDetails(claim, n1ZoneRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //N2
                    if (pickupZone.Name == TreadMarksConstants.N2)
                    {
                        AddDOTPickupZoneClaimPaymentDetails(claim, n2ZoneRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //N3
                    if (pickupZone.Name == TreadMarksConstants.N3)
                    {
                        AddDOTPickupZoneClaimPaymentDetails(claim, n3ZoneRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //N4
                    if (pickupZone.Name == TreadMarksConstants.N4)
                    {
                        AddDOTPickupZoneClaimPaymentDetails(claim, n4ZoneRate, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //South
                    if (pickupZone.Name == TreadMarksConstants.South)
                    {
                        if (dotPremiumDelivery.MooseCreekOffRoadWeight > 0)
                        {
                            AddDOTSouthClaimPaymentDetails(claim, mooseCreekZoneRate, dotPremiumDelivery.MooseCreakOffRoadPercentage, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);

                        }
                        if (dotPremiumDelivery.GTAOffRoadWeight > 0)
                        {
                            AddDOTSouthClaimPaymentDetails(claim, gtaZoneRate, dotPremiumDelivery.GTAOffRoadPercentage, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                        }
                        if (dotPremiumDelivery.WTCOffRoadWeight > 0)
                        {
                            AddDOTSouthClaimPaymentDetails(claim, wtcZoneRate, dotPremiumDelivery.WTCOffRoadPercentage, claimPaymentDetails, c, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                        }
                    }
                }
            });
        }

        private void AddDOTSouthClaimPaymentDetails(Claim claim, decimal zoneRate, decimal dotPremiumDeliveryOffRoadPercentage, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            if (c.AGLSActualWeight > 0)
            {
                var actualWeight = Math.Round(c.AGLSActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.AGLSAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.AGLSQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.INDActualWeight > 0)
            {
                var actualWeight = Math.Round(c.INDActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.INDAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.INDQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.SOTRActualWeight > 0)
            {
                var actualWeight = Math.Round(c.SOTRActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.SOTRAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.SOTRQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MOTRActualWeight > 0)
            {
                var actualWeight = Math.Round(c.MOTRActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.MOTRAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.MOTRQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.LOTRActualWeight > 0)
            {
                var actualWeight = Math.Round(c.LOTRActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.LOTRAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.LOTRQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.GOTRActualWeight > 0)
            {
                var actualWeight = Math.Round(c.GOTRActualWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var averageWeight = Math.Round(c.GOTRAverageWeight * dotPremiumDeliveryOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                var qty = (int)Math.Round(c.GOTRQty * dotPremiumDeliveryOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                var amount = Math.Round(actualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, zoneRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddDOTPickupZoneClaimPaymentDetails(Claim claim, decimal zoneRate, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            if (c.AGLSActualWeight > 0)
            {
                var amount = Math.Round(c.AGLSActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, zoneRate, c.AGLSActualWeight, c.AGLSAverageWeight, c.AGLSQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.INDActualWeight > 0)
            {
                var amount = Math.Round(c.INDActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, zoneRate, c.INDActualWeight, c.INDAverageWeight, c.INDQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.SOTRActualWeight > 0)
            {
                var amount = Math.Round(c.SOTRActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, zoneRate, c.SOTRActualWeight, c.SOTRAverageWeight, c.SOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
            if (c.MOTRActualWeight > 0)
            {
                var amount = Math.Round(c.MOTRActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, zoneRate, c.MOTRActualWeight, c.MOTRAverageWeight, c.MOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }

            if (c.LOTRActualWeight > 0)
            {
                var amount = Math.Round(c.LOTRActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, zoneRate, c.LOTRActualWeight, c.LOTRAverageWeight, c.LOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }

            if (c.GOTRActualWeight > 0)
            {
                var amount = Math.Round(c.GOTRActualWeight * zoneRate, 3, MidpointRounding.AwayFromZero);
                var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, zoneRate, c.GOTRActualWeight, c.GOTRAverageWeight, c.GOTRQty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.DOTPremium);
                claimPaymentDetails.Add(claimPaymentDetail);
            }
        }

        private void AddDOTPremiumClaimPayment(int claimId, decimal offRoad, decimal mooseCreekZoneRate, decimal gtaZoneRate, decimal wtcZoneRate, decimal sturgeonFallsZoneRate, List<ClaimPayment> claimPayments, DOTPremiumDelivery dotPremiumDelivery)
        {
            var tempClaimPayments = new List<ClaimPayment>();
            if (dotPremiumDelivery.MooseCreekOffRoadWeight > 0)
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(offRoad * dotPremiumDelivery.MooseCreakOffRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.DOTPremium,
                    ItemType = 2,
                    Rate = mooseCreekZoneRate,
                    SourceZone = TreadMarksConstants.South,
                    DeliveryZone = TreadMarksConstants.MooseCreek,
                    CreatedDate = DateTime.UtcNow
                };
                tempClaimPayments.Add(claimPayment);
            }
            if (dotPremiumDelivery.GTAOffRoadWeight > 0)
            {
                var claimPayment1 = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(offRoad * dotPremiumDelivery.GTAOffRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.DOTPremium,
                    ItemType = 2,
                    Rate = gtaZoneRate,
                    SourceZone = TreadMarksConstants.South,
                    DeliveryZone = TreadMarksConstants.GTA,
                    CreatedDate = DateTime.UtcNow
                };
                tempClaimPayments.Add(claimPayment1);
            }
            if (dotPremiumDelivery.WTCOffRoadWeight > 0)
            {
                var claimPayment2 = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(offRoad * dotPremiumDelivery.WTCOffRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.DOTPremium,
                    ItemType = 2,
                    Rate = wtcZoneRate,
                    SourceZone = TreadMarksConstants.South,
                    DeliveryZone = TreadMarksConstants.WTC,
                    CreatedDate = DateTime.UtcNow
                };
                tempClaimPayments.Add(claimPayment2);
            }
            if (dotPremiumDelivery.SturgeonFallsOffRoadWeight > 0)
            {
                var claimPayment3 = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(offRoad * dotPremiumDelivery.SturgeonFallsOffRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.DOTPremium,
                    ItemType = 2,
                    Rate = sturgeonFallsZoneRate,
                    SourceZone = TreadMarksConstants.South,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                tempClaimPayments.Add(claimPayment3);
            }

            //Fixed DOT Distributions Rounding issues
            var distributedWeight = tempClaimPayments.Sum(c => c.Weight);
            var difference = (distributedWeight - offRoad);
            var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.0001M);
            if (numberOfDifferenceDistribution < tempClaimPayments.Count)
            {
                for (var i = 0; i < numberOfDifferenceDistribution; i++)
                {
                    tempClaimPayments[i].Weight -= (difference / numberOfDifferenceDistribution);
                }
            }

            claimPayments.AddRange(tempClaimPayments);
        }

        private void AddDOTPremiumClaimPayment(string sourceZone, int claimId, decimal offRoad, decimal rate, List<ClaimPayment> claimPayments)
        {
            var claimPayment = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = Math.Round(offRoad, 4, MidpointRounding.AwayFromZero),
                PaymentType = (int)ClaimPaymentType.DOTPremium,
                ItemType = 2,
                Rate = rate,
                SourceZone = sourceZone,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment);
        }

        private void NorthernPremiumCalculation(Claim claim, List<ClaimPayment> claimPayments, List<ClaimDetailViewModel> ptrClaimDetails, List<ClaimDetailViewModel> claimDetails, DateTime claimPeriodDate, List<ClaimInventoryAdjustment> claimInventoryAdjustments, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var claimId = claim.ID;
            //1. Get delivery weight percentage for onroad and offroad based on greaterzone
            var northernPremiumDelivery = GetNorthernPremiumDelivery(ptrClaimDetails, claimInventoryAdjustments);

            //2. Get inbound transactions
            var inboundClaimDetails = claimDetails.Where(c => (c.TransactionType == "TCR" || c.TransactionType == "DOT" || c.TransactionType == "STC") && c.Direction && !string.IsNullOrWhiteSpace(c.Transaction.FromPostalCode)).ToList();
            var query = inboundClaimDetails.GroupBy(c => new { c.Transaction.FromPostalCode })
                .Select(c => new
                {
                    Name = c.Key,
                    OnRoad = c.Sum(i => i.OnRoad),
                    OffRoad = c.Sum(i => i.OffRoad)
                });
            var secondQuery = query.Select(c => new
            {
                PickupZone = ClaimHelper.GetZoneByPostCode(c.Name.FromPostalCode, 1) != null ? ClaimHelper.GetZoneByPostCode(c.Name.FromPostalCode, 1).Name : string.Empty,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.PickupZone })
             .Select(c => new
             {
                 Name = c.Key,
                 OnRoad = c.Sum(i => i.OnRoad),
                 OffRoad = c.Sum(i => i.OffRoad)
             });

            //2. Get rate based on pickupzone and delivery zone
            var n1ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N1).ID;
            var n2ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N2).ID;
            var n3ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N3).ID;
            var n4ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N4).ID;

            var n1SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SturgeonFallRate = n1SturgeonFallItemRate != null ? n1SturgeonFallItemRate.ItemRate : 0;
            var n2SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SturgeonFallRate = n2SturgeonFallItemRate != null ? n2SturgeonFallItemRate.ItemRate : 0;
            var n3SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SturgeonFallRate = n3SturgeonFallItemRate != null ? n3SturgeonFallItemRate.ItemRate : 0;
            var n4SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SturgeonFallRate = n4SturgeonFallItemRate != null ? n4SturgeonFallItemRate.ItemRate : 0;

            var n1SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SouthRate = n1SouthItemRate != null ? n1SouthItemRate.ItemRate : 0;
            var n2SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SouthRate = n2SouthItemRate != null ? n2SouthItemRate.ItemRate : 0;
            var n3SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SouthRate = n3SouthItemRate != null ? n3SouthItemRate.ItemRate : 0;
            var n4SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SouthRate = n4SouthItemRate != null ? n4SouthItemRate.ItemRate : 0;

            var n1SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SouthRateOffRoad = n1SouthItemRateOffRoad != null ? n1SouthItemRateOffRoad.ItemRate : 0;
            var n2SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SouthRateOffRoad = n2SouthItemRateOffRoad != null ? n2SouthItemRateOffRoad.ItemRate : 0;
            var n3SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SouthRateOffRoad = n3SouthItemRateOffRoad != null ? n3SouthItemRateOffRoad.ItemRate : 0;
            var n4SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SouthRateOffRoad = n4SouthItemRateOffRoad != null ? n4SouthItemRateOffRoad.ItemRate : 0;

            //3. Calculating
            //Each pickup zone must have record event the weight is zero
            //Adding N1 row
            var n1PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N1);
            if (n1PickupZone != null)
            {
                AddNorthernPremiumClaimPayment(TreadMarksConstants.N1, claimId, n1PickupZone.OnRoad, n1PickupZone.OffRoad, n1SturgeonFallRate, n1SouthRate, n1SouthRateOffRoad, northernPremiumDelivery, claimPayments);
            }
            else
            {
                AddNorthernPremiumZeroClaimPayment(TreadMarksConstants.N1, claimId, n1SturgeonFallRate, n1SouthRate, n1SouthRateOffRoad, claimPayments);
            }

            //Adding N2 row
            var n2PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N2);
            if (n2PickupZone != null)
            {
                AddNorthernPremiumClaimPayment(TreadMarksConstants.N2, claimId, n2PickupZone.OnRoad, n2PickupZone.OffRoad, n2SturgeonFallRate, n2SouthRate, n2SouthRateOffRoad, northernPremiumDelivery, claimPayments);
            }
            else
            {
                AddNorthernPremiumZeroClaimPayment(TreadMarksConstants.N2, claimId, n2SturgeonFallRate, n2SouthRate, n2SouthRateOffRoad, claimPayments);
            }

            //Adding N3 row
            var n3PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N3);
            if (n3PickupZone != null)
            {
                AddNorthernPremiumClaimPayment(TreadMarksConstants.N3, claimId, n3PickupZone.OnRoad, n3PickupZone.OffRoad, n3SturgeonFallRate, n3SouthRate, n3SouthRateOffRoad, northernPremiumDelivery, claimPayments);
            }
            else
            {
                AddNorthernPremiumZeroClaimPayment(TreadMarksConstants.N3, claimId, n3SturgeonFallRate, n3SouthRate, n3SouthRateOffRoad, claimPayments);
            }

            //Adding N4 row
            var n4PickupZone = secondQuery.FirstOrDefault(c => c.Name.PickupZone == TreadMarksConstants.N4);
            if (n4PickupZone != null)
            {
                AddNorthernPremiumClaimPayment(TreadMarksConstants.N4, claimId, n4PickupZone.OnRoad, n4PickupZone.OffRoad, n4SturgeonFallRate, n4SouthRate, n4SouthRateOffRoad, northernPremiumDelivery, claimPayments);
            }
            else
            {
                AddNorthernPremiumZeroClaimPayment(TreadMarksConstants.N4, claimId, n4SturgeonFallRate, n4SouthRate, n4SouthRateOffRoad, claimPayments);
            }

            //4. Add claim payment details
            AddNorthernPremiumClaimPaymentDetais(claim, inboundClaimDetails, n1SturgeonFallRate, n1SouthRate, n1SouthRateOffRoad, n2SturgeonFallRate, n2SouthRate, n2SouthRateOffRoad, n3SturgeonFallRate, n3SouthRate, n3SouthRateOffRoad, n4SturgeonFallRate, n4SouthRate, n4SouthRateOffRoad, northernPremiumDelivery, claimPaymentDetails);
        }

        private void AddNorthernPremiumClaimPaymentDetais(Claim claim, List<ClaimDetailViewModel> inboundClaimDetails, decimal n1SturgeonFallRate, decimal n1SouthRate, decimal n1SouthRateOffRoad, decimal n2SturgeonFallRate, decimal n2SouthRate, decimal n2SouthRateOffRoad, decimal n3SturgeonFallRate, decimal n3SouthRate, decimal n3SouthRateOffRoad, decimal n4SturgeonFallRate, decimal n4SouthRate, decimal n4SouthRateOffRoad, NorthernPremiumDelivery northernPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails)
        {
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            inboundClaimDetails.ForEach(c =>
            {
                var pickupZone = ClaimHelper.GetZoneByPostCode(c.Transaction.FromPostalCode, 1);
                if (pickupZone != null)
                {
                    //N1
                    if (pickupZone.Name == TreadMarksConstants.N1)
                    {
                        AddPickupZoneClaimPaymentDetails(claim, n1SturgeonFallRate, n1SouthRate, n1SouthRateOffRoad, northernPremiumDelivery, claimPaymentDetails, c, pltItem, mtItem, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }

                    //N2
                    if (pickupZone.Name == TreadMarksConstants.N2)
                    {
                        AddPickupZoneClaimPaymentDetails(claim, n2SturgeonFallRate, n2SouthRate, n2SouthRateOffRoad, northernPremiumDelivery, claimPaymentDetails, c, pltItem, mtItem, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //N3
                    if (pickupZone.Name == TreadMarksConstants.N3)
                    {
                        AddPickupZoneClaimPaymentDetails(claim, n3SturgeonFallRate, n3SouthRate, n3SouthRateOffRoad, northernPremiumDelivery, claimPaymentDetails, c, pltItem, mtItem, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }
                    //N4
                    if (pickupZone.Name == TreadMarksConstants.N4)
                    {
                        AddPickupZoneClaimPaymentDetails(claim, n4SturgeonFallRate, n4SouthRate, n4SouthRateOffRoad, northernPremiumDelivery, claimPaymentDetails, c, pltItem, mtItem, aglsItem, indItem, sotrItem, motrItem, lotrItem, gotrItem);
                    }

                }
            });
        }

        private void AddPickupZoneClaimPaymentDetails(Claim claim, decimal sturgeonFallRate, decimal southRate, decimal southRateOffRoad, NorthernPremiumDelivery northernPremiumDelivery, List<ClaimPaymentDetail> claimPaymentDetails, ClaimDetailViewModel c, Item pltItem, Item mtItem, Item aglsItem, Item indItem, Item sotrItem, Item motrItem, Item lotrItem, Item gotrItem)
        {
            if (northernPremiumDelivery.SturgeonFallOnRoad > 0)
            {
                if (c.PLTActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.PLTActualWeight * northernPremiumDelivery.SturgonFallOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.PLTAverageWeight * northernPremiumDelivery.SturgonFallOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.PLTQty * northernPremiumDelivery.SturgonFallOnRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * sturgeonFallRate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, sturgeonFallRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
                if (c.MTActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.MTActualWeight * northernPremiumDelivery.SturgonFallOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.MTAverageWeight * northernPremiumDelivery.SturgonFallOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.MTQty * northernPremiumDelivery.SturgonFallOnRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * sturgeonFallRate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, sturgeonFallRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
            }
            if (northernPremiumDelivery.SouthOnRoad > 0)
            {
                if (c.PLTActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.PLTActualWeight * northernPremiumDelivery.SouthOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.PLTAverageWeight * northernPremiumDelivery.SouthOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.PLTQty * northernPremiumDelivery.SouthOnRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, pltItem, southRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
                if (c.MTActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.MTActualWeight * northernPremiumDelivery.SouthOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.MTAverageWeight * northernPremiumDelivery.SouthOnRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.MTQty * northernPremiumDelivery.SouthOnRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRate, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, mtItem, southRate, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
            }
            if (northernPremiumDelivery.SouthOffRoad > 0)
            {
                if (c.AGLSActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.AGLSActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.AGLSAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.AGLSQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, aglsItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
                if (c.INDActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.INDActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.INDAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.INDQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, indItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }

                if (c.SOTRActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.SOTRActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.SOTRAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.SOTRQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, sotrItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }

                if (c.MOTRActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.MOTRActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.MOTRAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.MOTRQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, motrItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }

                if (c.LOTRActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.LOTRActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.LOTRAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.LOTRQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, lotrItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }

                if (c.GOTRActualWeight > 0)
                {
                    var actualWeight = Math.Round(c.GOTRActualWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var averageWeight = Math.Round(c.GOTRAverageWeight * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero);
                    var qty = (int)Math.Round(c.GOTRQty * northernPremiumDelivery.SouthOffRoadPercentage, 0, MidpointRounding.AwayFromZero);
                    var amount = Math.Round(actualWeight * southRateOffRoad, 3, MidpointRounding.AwayFromZero);
                    var claimPaymentDetail = AddClaimPaymentDetail(claim, c, gotrItem, southRateOffRoad, actualWeight, averageWeight, qty, amount, TreadMarksConstants.Kg, (int)ClaimPaymentType.NorthernPremium);
                    claimPaymentDetails.Add(claimPaymentDetail);
                }
            }
        }

        private void AddNorthernPremiumZeroClaimPayment(string sourceZone, int claimId, decimal sturgeonFallRate, decimal southRate, decimal southOffRoadRate, List<ClaimPayment> claimPayments)
        {
            var claimPayment = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = 0,
                PaymentType = (int)ClaimPaymentType.NorthernPremium,
                ItemType = 1,
                Rate = sturgeonFallRate,
                SourceZone = sourceZone,
                DeliveryZone = TreadMarksConstants.SturgeonFalls,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment);

            var claimPayment1 = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = 0,
                PaymentType = (int)ClaimPaymentType.NorthernPremium,
                ItemType = 1,
                Rate = southRate,
                SourceZone = sourceZone,
                DeliveryZone = TreadMarksConstants.South,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment1);

            var claimPayment2 = new ClaimPayment
            {
                ClaimID = claimId,
                Weight = 0,
                PaymentType = (int)ClaimPaymentType.NorthernPremium,
                ItemType = 2,
                Rate = southOffRoadRate,
                SourceZone = sourceZone,
                DeliveryZone = TreadMarksConstants.South,
                CreatedDate = DateTime.UtcNow
            };
            claimPayments.Add(claimPayment2);
        }

        private void AddNorthernPremiumClaimPayment(string sourceZone, int claimId, decimal onRoad, decimal offRoad, decimal sturgeonFallRate, decimal southRate, decimal southOffRoadRate, NorthernPremiumDelivery northernPremiumDelivery, List<ClaimPayment> claimPayments)
        {
            if (northernPremiumDelivery.SturgeonFallOnRoad > 0)
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(onRoad * northernPremiumDelivery.SturgonFallOnRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 1,
                    Rate = sturgeonFallRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 1,
                    Rate = sturgeonFallRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.SturgeonFalls,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (northernPremiumDelivery.SouthOnRoad > 0)
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(onRoad * northernPremiumDelivery.SouthOnRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 1,
                    Rate = southRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.South,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 1,
                    Rate = southRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.South,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }

            if (northernPremiumDelivery.SouthOffRoad > 0)
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = Math.Round(offRoad * northernPremiumDelivery.SouthOffRoadPercentage, 4, MidpointRounding.AwayFromZero),
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 2,
                    Rate = southOffRoadRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.South,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
            else
            {
                var claimPayment = new ClaimPayment
                {
                    ClaimID = claimId,
                    Weight = 0,
                    PaymentType = (int)ClaimPaymentType.NorthernPremium,
                    ItemType = 2,
                    Rate = southOffRoadRate,
                    SourceZone = sourceZone,
                    DeliveryZone = TreadMarksConstants.South,
                    CreatedDate = DateTime.UtcNow
                };
                claimPayments.Add(claimPayment);
            }
        }
        private NorthernPremiumDelivery GetNorthernPremiumDelivery(List<ClaimDetailViewModel> ptrClaimDetials, List<ClaimInventoryAdjustment> claimInventoryAdjustments)
        {
            var result = new NorthernPremiumDelivery();
            var query = ptrClaimDetials
                .Where(c => c.Transaction.ToPostalCode != null)
                .GroupBy(c => new { c.Transaction.ToPostalCode })
                .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.OnRoad), OffRoad = c.Sum(i => i.OffRoad) });

            var secondQuery = query.Select(c => new
            {
                GreaterZone = ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2) != null ? ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2).GreaterZone : string.Empty,
                OnRoad = c.OnRoad,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.GreaterZone })
            .Select(c => new
            {
                Name = c.Key,
                OnRoad = c.Sum(i => i.OnRoad),
                OffRoad = c.Sum(i => i.OffRoad)
            });
            secondQuery.ToList().ForEach(c =>
            {
                if (c.Name.GreaterZone == TreadMarksConstants.SturgeonFalls)
                {
                    result.SturgeonFallOnRoad = c.OnRoad;
                    result.SturgeonFallOffRoad = c.OffRoad;
                }
                if (c.Name.GreaterZone == TreadMarksConstants.South)
                {
                    result.SouthOnRoad = c.OnRoad;
                    result.SouthOffRoad = c.OffRoad;
                }
            });

            result.TotoalOnRoad = ptrClaimDetials.Sum(c => c.OnRoad);
            result.TotalOffRoad = ptrClaimDetials.Sum(c => c.OffRoad);

            //Add inventory adjustment
            var outboundEligibleOnroadAdjust = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOnroad);
            var outboundEligibleOffroadAdjust = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);

            result.TotoalOnRoad = result.TotoalOnRoad + outboundEligibleOnroadAdjust;
            result.TotalOffRoad = result.TotalOffRoad + outboundEligibleOffroadAdjust;

            return result;
        }

        private DOTPremiumDelivery GetDOTPremiumDelivery(List<ClaimDetailViewModel> ptrClaimDetials, List<ClaimInventoryAdjustment> claimInventoryAdjustments)
        {
            var result = new DOTPremiumDelivery();
            var query = ptrClaimDetials
            .Where(c => c.Transaction.ToPostalCode != null)
            .GroupBy(c => new { c.Transaction.ToPostalCode })
            .Select(c => new { Name = c.Key, OffRoad = c.Sum(i => i.OffRoad) });

            var secondQuery = query.Select(c => new
            {
                DeliveryZone = ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2) != null ? ClaimHelper.GetZoneByPostCode(c.Name.ToPostalCode, 2).Name : string.Empty,
                OffRoad = c.OffRoad
            }).GroupBy(c => new { c.DeliveryZone })
              .Select(c => new
              {
                  Name = c.Key,
                  OffRoad = c.Sum(i => i.OffRoad)
              });
            secondQuery.ToList().ForEach(c =>
            {
                if (c.Name.DeliveryZone == TreadMarksConstants.MooseCreek)
                {
                    result.MooseCreekOffRoadWeight = c.OffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.GTA)
                {
                    result.GTAOffRoadWeight = c.OffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.WTC)
                {
                    result.WTCOffRoadWeight = c.OffRoad;
                }
                if (c.Name.DeliveryZone == TreadMarksConstants.SturgeonFalls)
                {
                    result.SturgeonFallsOffRoadWeight = c.OffRoad;
                }
            });

            result.TotalOffRoadWeight = ptrClaimDetials.Sum(c => c.OffRoad);

            //Inventory adjustment
            var eligibleOutboundOffroad = claimInventoryAdjustments.Where(c => c.IsEligible && c.Direction == 2).Sum(c => c.AdjustmentWeightOffroad);
            result.TotalOffRoadWeight = result.TotalOffRoadWeight + eligibleOutboundOffroad;
            return result;
        }

        #endregion
    }
}
