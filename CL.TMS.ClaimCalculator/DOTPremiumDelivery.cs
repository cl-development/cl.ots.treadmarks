﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    public class DOTPremiumDelivery
    {
        public decimal MooseCreekOffRoadWeight { get; set; }
        public decimal GTAOffRoadWeight { get; set; }
        public decimal WTCOffRoadWeight { get; set; }
        public decimal SturgeonFallsOffRoadWeight { get; set; }

        public decimal TotalOffRoadWeight
        {
            get; set;
        }

        public decimal MooseCreakOffRoadPercentage
        {
            get
            {
                return TotalOffRoadWeight == 0 ? 0 : MooseCreekOffRoadWeight / TotalOffRoadWeight;
            }
        }

        public decimal GTAOffRoadPercentage
        {
            get
            {
                return TotalOffRoadWeight == 0 ? 0 : GTAOffRoadWeight / TotalOffRoadWeight;
            }
        }

        public decimal WTCOffRoadPercentage
        {
            get
            {
                return TotalOffRoadWeight == 0 ? 0 : WTCOffRoadWeight / TotalOffRoadWeight;
            }
        }

        public decimal SturgeonFallsOffRoadPercentage
        {
            get
            {
                return TotalOffRoadWeight == 0 ? 0 : SturgeonFallsOffRoadWeight / TotalOffRoadWeight;
            }
        }
    }
}
