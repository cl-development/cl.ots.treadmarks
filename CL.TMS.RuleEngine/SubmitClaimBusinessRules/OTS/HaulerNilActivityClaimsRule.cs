﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class HaulerNilActivityClaimsRule : BaseBusinessRule
    {
        public HaulerNilActivityClaimsRule() : base("HaulerNilActivityClaimsRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            //this is continuation of nilactivity rule as isNilActivity boolean is already checked on client side
            bool nilActivityClaimsRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            var submitClaimModel = ruleContext["submitClaimModel"] as HaulerSubmitClaimViewModel;

            //checking for any transaction adjustment
            var hasTransAdj= claimRepository.HasTransactionAdjustment(claim.ID);

            if (!hasTransAdj && submitClaimModel.TCROnRoad == 0 && submitClaimModel.TCROffRoad == 0 && submitClaimModel.DOTOnRoad == 0 && submitClaimModel.DOTOffRoad == 0 &&
                submitClaimModel.STCOnRoad == 0 &&
                submitClaimModel.STCOffRoad == 0 && submitClaimModel.UCROnRoad == 0 && submitClaimModel.UCROffRoad == 0 && submitClaimModel.HITOnRoad == 0 && submitClaimModel.HITOffRoad == 0 &&
                submitClaimModel.PTROnRoad == 0 && submitClaimModel.PTROffRoad == 0 && submitClaimModel.HITOutboundOnRoad == 0 && submitClaimModel.HITOutboundOffRoad == 0 && submitClaimModel.RTROnRoad == 0 &&
                submitClaimModel.RTROffRoad == 0 && submitClaimModel.PLT == 0 && submitClaimModel.MT == 0 && submitClaimModel.AGLS == 0 && submitClaimModel.IND == 0 &&
                submitClaimModel.SOTR == 0 && submitClaimModel.MOTR == 0 && submitClaimModel.LOTR == 0 && submitClaimModel.GOTR == 0)
            {
                nilActivityClaimsRule = true;
            }                

            if (nilActivityClaimsRule)
            {
                var error = new ValidationFailure("NilActivityClaims", "This claim has no transactions in it. Are you sure you want to proceed with this submission?");
                error.ErrorCode = "NilActivityClaims";
                validationResults.Errors.Add(error);
            }
        }
    }
}
