﻿using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class CollectorNilActivityClaimsRule : BaseBusinessRule
    {
        public CollectorNilActivityClaimsRule() : base("CollectorNilActivityClaimsRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool nilActivityClaimsRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            var submitClaimModel = ruleContext["submitClaimModel"] as CollectorSubmitClaimViewModel;

            if (submitClaimModel.InboundList == null)
            {
                //OTSTM2-588
                if (submitClaimModel.TCR.IsAllFieldsZero && submitClaimModel.DOT.IsAllFieldsZero && submitClaimModel.TotalEligible.IsAllFieldsZero)
                {
                    nilActivityClaimsRule = true;
                }
            }

            if (nilActivityClaimsRule)
            {
                var error = new ValidationFailure("NilActivityClaims", "This claim has no transactions in it.");
                error.ErrorCode = "NilActivityClaims";
                validationResults.Errors.Add(error);
            }
        }
    }
}
