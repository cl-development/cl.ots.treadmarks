﻿using CL.Framework.BLL;
using CL.Framework.RuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class SubmitClaimRuleSetOTSStrategy : IBusinessRuleSetStrategy
    {
        public BaseRuleSet CreateBusinessRuleSet()
        {
            return new OTSSubmitClaimRuleSet();
        }
    }
}
