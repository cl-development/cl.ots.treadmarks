﻿using CL.Framework.RuleEngine;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.TransactionBusinessRules.BusinessRules
{
    public class PreviousScaleTicketMobileRule : BaseBusinessRule
    {
        public PreviousScaleTicketMobileRule(): base("PreviousScaleTicketMobileRule")
        {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            List<PreviousScaleTicketResult> result = null;

            var transactionRepository = ruleContext["transactionRepository"] as ITransactionRepository;
            var model = target as PreviousScaleTicketMobile;

            if (model.IsSingleScaleticket)
            {
                if (!string.IsNullOrEmpty(model.SingleScaleTicket))
                {
                    result = transactionRepository.GetDuplicateScaleTicket(model.FormType, model.Inbound.VendorId, model.SingleScaleTicket, model.PeriodType);
                }
            }
            else
            {
                string inboundScaleTicket = model.InboundScaleTicket;
                string outboundScaleTicket = model.OutboundScaleTicket;
                result = transactionRepository.GetDuplicateScaleTicket(model.FormType, model.Inbound.VendorId, model.InboundScaleTicket, model.PeriodType);
                result.AddRange(transactionRepository.GetDuplicateScaleTicket(model.FormType, model.Inbound.VendorId, model.OutboundScaleTicket, model.PeriodType));
            }

            if (result != null && result.Count > 0)
            {
                result.ForEach(c =>
                {
                    var error = new ValidationFailure("PreviousScaleTicketNumber", string.Format("{0} has used the scale ticket #{1} before with {2} in {3} claim Trans # {4}",
                            c.IncomingVendor.Number, c.TicketNumber, c.OutgoingVendor.Number, c.PeriodShortName, c.TransactionNumber));
                    error.ErrorCode = "PreviousScaleTicketNumber";
                    validationResults.Errors.Add(error);
                });
            }
        }


        private object getValuefromKey(Dictionary<string, object> result, string val)
        {
            var keyValuePair = result.Single(x => x.Key == val);
            return (keyValuePair.Value == null ? string.Empty : keyValuePair.Value);
        }        
    }
}
