﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.BLL;
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.PasswordBusinessRules.OTS
{
    public class PasswordRuleSetOTSStrategy : IBusinessRuleSetStrategy
    {
        public BaseRuleSet CreateBusinessRuleSet()
        {
            return new OTSPasswordRuleSet();
        }
    }
}
