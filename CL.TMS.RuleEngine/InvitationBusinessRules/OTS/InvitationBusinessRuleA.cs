﻿using System;
using System.Collections.Generic;
using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using FluentValidation.Results;

namespace CL.TMS.RuleEngine.InvitationBusinessRules.OTS
{
    public class InvitationBusinessRuleA:BaseBusinessRule
    {
        public InvitationBusinessRuleA() : base("InvitationBusinessRuleA")
        {
            
        }

        public override void Execute<T>(T target, ValidationResult validationResult, IDictionary<string, object> ruleContext = null)
        {
            //T was passed when business object execute business rules
            //Check business rule heres
            //if invalid, add new validation failure to validationResult Errors collection
            var invitation = target as Invitation;
            var error = new ValidationFailure("Name", "Invalida Name");
            error.ErrorCode = "InvalidNameKey";
            validationResult.Errors.Add(error);
            var errorA = new ValidationFailure("NameA", "Invalida NameA");
            errorA.ErrorCode = "InvalidNameAKey";
            errorA.FormattedMessagePlaceholderValues = new Dictionary<string, object>();
            errorA.FormattedMessagePlaceholderValues.Add("FirstName",invitation.FirstName);
            errorA.FormattedMessagePlaceholderValues.Add("LastName", invitation.LastName);
            validationResult.Errors.Add(errorA);
        }
    }
}
