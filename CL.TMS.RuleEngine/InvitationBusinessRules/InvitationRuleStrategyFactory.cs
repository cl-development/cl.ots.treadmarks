﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.BLL;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.InvitationBusinessRules.OTS;

namespace CL.TMS.RuleEngine.InvitationBusinessRules
{
    public class InvitationRuleStrategyFactory
    {
        public static IBusinessRuleSetStrategy LoadInvitationRuleStrategy()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    return new InvitationRuleSetOTSStrategy();
                default:
                    return new InvitationRuleSetOTSStrategy();
            }
        }
    }
}
