﻿using System;
using System.Collections.Generic;
using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using FluentValidation.Results;
using MobileTransaction = CL.TMS.DataContracts.Mobile.Transaction;

namespace CL.TMS.RuleEngine.MobileTransactionBusinessRules.OTS
{
    public class MaxTireCountRule:BaseBusinessRule
    {
        public MaxTireCountRule()
            : base("MaxTireCountRule")
        {
            
        }

        public override void Execute<T>(T target, ValidationResult validationResult, IDictionary<string, object> ruleContext = null)
        {
            //T was passed when business object execute business rules
            //Check business rule heres
            //if invalid, add new validation failure to validationResult Errors collection
            var mobileTransaction = target as MobileTransaction;
            var error = new ValidationFailure("InvalidTireCount", "InvalidTireCount");
            error.ErrorCode = "InvalidTireCount";
            validationResult.Errors.Add(error);            
        }
    }
}
