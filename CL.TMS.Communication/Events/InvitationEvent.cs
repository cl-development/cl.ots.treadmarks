﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;

namespace CL.TMS.Communication.Events
{
    /// <summary>
    /// Invitation event
    /// </summary>
    public class InvitationEvent:PubSubEvent<Invitation>
    {
    }
}
