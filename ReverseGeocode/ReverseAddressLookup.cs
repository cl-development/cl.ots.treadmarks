﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts;
using CL.TMS.DataContracts.Communication;
using CL.TMS.DataContracts.DomainEntities;
using System.Configuration;
using CL.Framework.Logging;


namespace CL.TMS.ReverseGeocode
{
    /// <summary>
    /// This class gives you address components based on lat and long
    /// </summary>
    public static class ReverseAddressLookup
    {
        //static string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false";
        
        static string baseUriAddresslookUp = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&key={2}";
        static string baseUriCoordinatesLookUp = "https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}";

        static string key = ConfigurationManager.AppSettings["GoogleAPIKey"];

        /// <summary>
        /// This method gives you address object based on lat and long
        /// </summary>
        public static Address RetrieveAddress(decimal lat, decimal lng)
        {
            var address = new Address();
            address.GPSCoordinate1 = lat;
            address.GPSCoordinate2 = lng;

            string requestUri = string.Format(baseUriAddresslookUp, lat, lng, key);

            try
            {
                using (WebClient wc = new WebClient())
                {
                    string result = wc.DownloadString(requestUri);

                    var json = JsonConvert.DeserializeObject<GoogleGeocodeAPI.RootObject>(result);

                    //OTSTM2-380, for couple of cases address_components is not able provide these fields so default them to ""
                    address.Address1 = address.City = "";

                    if (json.status.ToUpper() == "OK" && json.results.Count > 0)
                    {
                        foreach (var address_component in json.results[0].address_components)
                        {
                            switch (address_component.types[0])
                            {
                                case "street_number":
                                    address.Address1 = address_component.long_name;
                                    break;
                                case "route":
                                    address.Address2 = address_component.long_name;
                                    break;
                                case "locality":
                                    address.City = address_component.long_name;
                                    break;
                                case "administrative_area_level_1":
                                    address.Province = address_component.long_name;
                                    break;
                                case "country":
                                    address.Country = address_component.long_name;
                                    break;
                                case "postal_code":
                                    address.PostalCode = address_component.long_name;
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
            }
            return address;
        }


        /// <summary>
        /// This method gives you coordinates (Location) object based on postal code
        /// </summary>
        public static GoogleGeocodeAPI.Location RetrieveCoordinates(string address)
        {
            string requestUri = string.Format(baseUriCoordinatesLookUp, address, key);

            GoogleGeocodeAPI.Location location = new GoogleGeocodeAPI.Location();
             
            try
            {
                using (WebClient wc = new WebClient())
                {

                    string result = wc.DownloadString(requestUri);

                    var json = JsonConvert.DeserializeObject<GoogleGeocodeAPI.RootObject>(result);

                    location = json.status == "OK" ? json.results[0].geometry.location : null;
                    
                }
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
            }

            return location;
        }


        /// <summary>
        /// This method gives you root object based on lat and long
        /// </summary>
        public static GoogleGeocodeAPI.RootObject RetrieveGoogleRootObject(string baseUri, decimal lat, decimal lng)
        {           
            string requestUri = string.Format(baseUri, lat, lng, key);

            using (WebClient wc = new WebClient())
            {
                string result = wc.DownloadString(requestUri);
                return JsonConvert.DeserializeObject<GoogleGeocodeAPI.RootObject>(result);                              
            }
        }
    }
}


