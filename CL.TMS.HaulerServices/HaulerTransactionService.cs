﻿using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.HaulerBLL;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Claims;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Common;


namespace CL.TMS.HaulerServices
{
    public class HaulerTransactionService : IHaulerTransactionService
    {
        private HaulerTransactionBO haulerTransactionBO;

        public HaulerTransactionService(ITransactionRepository transactionsRepository)
        {
            haulerTransactionBO = new HaulerTransactionBO(transactionsRepository);
        }       
    }
}
