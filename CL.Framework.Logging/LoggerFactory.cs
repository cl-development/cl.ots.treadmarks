﻿using CL.Framework.Logging.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    public static class LoggerFactory
    {
        public static ILogger GetLogger()
        {
            return new DBLogger();
        }
    }
}
