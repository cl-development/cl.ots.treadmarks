﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CL.TMS.Common.Helper;
using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Mvc;
using FluentValidation.Results;

namespace CL.TMS.ValidationEngine
{
    /// <summary>
    /// A interceptor for selecting a rule set based on context
    /// </summary>
    public class RuleSetSelectionValidatorInterceptor : IValidatorInterceptor
    {
        /// <summary>
        /// After MVC validation
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="validationContext"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ValidationResult AfterMvcValidation(ControllerContext controllerContext, ValidationContext validationContext, ValidationResult result)
        {
            return result;
        }

        /// <summary>
        /// Before MVC validation
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public ValidationContext BeforeMvcValidation(ControllerContext controllerContext, ValidationContext validationContext)
        {
            //base on context to apply rule set
            var ruleSet = RuleSetHelper.GetRuleSetName();
            return new ValidationContext(validationContext.InstanceToValidate,validationContext.PropertyChain,new RulesetValidatorSelector(ruleSet));
        }
    }
}
