﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Configuration
{
    public static class GpHelper
    {
        public static GpStatus GetGpiStatus(string gpiStatusId)
        {
            var result = GpStatus.StatusUnknown;
            switch (gpiStatusId.ToUpper())
            {
                case "P":
                    result = GpStatus.PostedQueued;
                    break;
                case "P2":
                    result = GpStatus.PostedImported;
                    break;
                case "X":
                    result = GpStatus.FailedStaging;
                    break;
                case "X2":
                    result = GpStatus.FailedImport;
                    break;
                case "H":
                    result = GpStatus.Hold;
                    break;
                case "E":
                    result = GpStatus.Extract;
                    break;
                default:
                    result = GpStatus.StatusUnknown;
                    break;
            }
            return result;
        }

        public static string GetGpiStatusString(GpStatus gpStatus)
        {
            var result = string.Empty;
            switch (gpStatus)
            {
                case GpStatus.PostedQueued:
                    result = "P";
                    break;
                case GpStatus.PostedImported:
                    result = "P2";
                    break;
                case GpStatus.FailedStaging:
                    result = "X";
                    break;
                case GpStatus.FailedImport:
                    result = "X2";
                    break;
                case GpStatus.Hold:
                    result = "H";
                    break;
                case GpStatus.Extract:
                    result = "E";
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            return result;
        }

        public static string GetGpiTypeID(GpManager gpManager)
        {
            var result = string.Empty;
            switch (gpManager)
            {
                case GpManager.Collector:
                    result = "C";
                    break;
                case GpManager.Hauler:
                    result = "H";
                    break;
                case GpManager.Participant:
                    result = "R";
                    break;
                case GpManager.Processor:
                    result = "P";
                    break;
                case GpManager.RPM:
                    result = "M";
                    break;
                case GpManager.Steward:
                    result = "S";
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            return result;
        }
    }
}
