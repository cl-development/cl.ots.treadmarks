﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Caching;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.Repository;
using CL.TMS.Repository.System;

namespace CL.TMS.Configuration
{
    /// <summary>
    /// Singleton AppSettings
    /// </summary>
    public class AppSettings
    {
        private static readonly Lazy<AppSettings> lazy = new Lazy<AppSettings>(() => new AppSettings());
        private ISettingRepository repository;

        private AppSettings()
        {
            repository = new SettingRepository();
        }

        public static AppSettings Instance
        {
            get { return lazy.Value; }
        }

        public List<Setting> AllSettings {
            get
            {
                return DataLoader.GetAppSettings;
            }
        }

        public string GetSettingValue(string key)
        {
            var setting = AllSettings.FirstOrDefault(c => c.Key == key);
            return setting == null ? string.Empty : setting.Value;
        } 

        public User SystemUser
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.SystemUser))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<User>(CachKeyManager.SystemUser);
                }
                var result = repository.GetSystemUser();
                CachingHelper.AddItem(CachKeyManager.SystemUser, result);
                return result;
            }
        }
    }
}
