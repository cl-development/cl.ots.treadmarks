﻿using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Transaction.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Configuration
{
    public static class ClaimHelper
    {
        public static int GetZoneIdByPostalCode(string postalCode, int zoneType = 1)
        {
            if (string.IsNullOrEmpty(postalCode) || postalCode.Length < 3) return -1;

            var postalCodePrefix = postalCode.Substring(0, 3);
            var result =
                DataLoader.PostalCodes.FirstOrDefault(
                    c => string.Compare(c.PostalCodePrefix, postalCodePrefix, StringComparison.OrdinalIgnoreCase) == 0 && c.ZoneType == zoneType);
            return result != null ? result.ZoneID : -1;
        }

        public static Zone GetZoneByPostCode(string postalCode, int zoneType)
        {
            if (string.IsNullOrWhiteSpace(postalCode))
            {
                if (zoneType == 1)
                {
                    //return defualt South pickup zone
                    var southPickupZone = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == "South");
                    return southPickupZone;
                }
                else
                {
                    return null;
                }
            }

            var postalCodePrefix = postalCode.Substring(0, 3);
            var findPostalCode = DataLoader.PostalCodes.FirstOrDefault(
                    c => string.Compare(c.PostalCodePrefix, postalCodePrefix, StringComparison.OrdinalIgnoreCase) == 0 && c.ZoneType == zoneType);
            if (findPostalCode != null)
            {
                return DataLoader.Zones.FirstOrDefault(c => c.ID == findPostalCode.ZoneID);
            }
            else
            {
                if (zoneType == 1)
                {
                    //return defualt South pickup zone
                    var southPickupZone = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == "South");
                    return southPickupZone;
                }
            }
            return null;
        }

        public static decimal GetPTRClaimAmount(List<TransactionItemListDetailViewModel> TransactionItems, Period claimPeriod, string ToPostalCode)
        {
            Item item;
            Rate OnRoadRate, OffRoadRate;
            int deliveryZoneId, claimTypeId, paymentTypeId;
            decimal returnVal;

            var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
            var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;

            deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(ToPostalCode, 2);//transaction.ToPostalCode
            claimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
            paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;

            OnRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == claimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OnRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriod.StartDate && r.EffectiveEndDate >= claimPeriod.StartDate);
            OffRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == claimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OffRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriod.StartDate && r.EffectiveEndDate >= claimPeriod.StartDate);

            var calculationDetailListOnroad = new List<ProcessorCalculationDetailViewModel>();
            var calculationDetailListOffroad = new List<ProcessorCalculationDetailViewModel>();

            foreach (var ti in TransactionItems)
            {
                item = DataLoader.TransactionItems.Single(c => c.ID == ti.ItemID);

                var calculationDetailItem = new CL.TMS.DataContracts.ViewModel.Transaction.ProcessorCalculationDetailViewModel()
                {
                    Name = ti.ShortName,
                    EstWeight = ti.AverageWeight,//Math.Round(DataConversion.ConvertKgToTon((double)ti.AverageWeight), 4, MidpointRounding.AwayFromZero),
                    ScaleWeight = ti.ActualWeight//Math.Round(DataConversion.ConvertKgToTon((double)ti.ActualWeight), 4, MidpointRounding.AwayFromZero)
                };

                //Fixing rounding issue based on item type
                if (item.ItemType == OnRoadItemTypeId)
                {
                    calculationDetailListOnroad.Add(calculationDetailItem);
                }
                else
                {
                    calculationDetailListOffroad.Add(calculationDetailItem);
                }
            }

            var onroadWeight = Math.Round(calculationDetailListOnroad.Sum(c => c.ScaleWeight), 4, MidpointRounding.AwayFromZero);
            var onroadRate = (OnRoadRate != null) ? OnRoadRate.ItemRate : 0.00M;
            var offroadWeight = Math.Round(calculationDetailListOffroad.Sum(c => c.ScaleWeight), 4, MidpointRounding.AwayFromZero);
            var offroadRate = (OffRoadRate != null) ? OffRoadRate.ItemRate : 0.00M;

            returnVal = Math.Round(onroadRate * onroadWeight, 2) + Math.Round(offroadRate * offroadWeight, 2, MidpointRounding.AwayFromZero);
            return returnVal;
        }

        public static decimal GetDORClaimAmount(ProcessorCommonTransactionListViewModel transaction, Period claimPeriod)
        {
            decimal? returnVal = 0;
            Item item;
            Rate OnRoadRate, OffRoadRate;
            int deliveryZoneId, claimTypeId, paymentTypeId;

            deliveryZoneId = ClaimHelper.GetZoneIdByPostalCode(transaction.PostalCode, 2);
            claimTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue;
            paymentTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue;

            var OnRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue;
            var OffRoadItemTypeId = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue;

            OnRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == claimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OnRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriod.StartDate && r.EffectiveEndDate >= claimPeriod.StartDate);
            OffRoadRate = DataLoader.Rates.FirstOrDefault(r => r.ClaimType == claimTypeId && r.PaymentType == paymentTypeId && r.ItemType == OffRoadItemTypeId && r.DeliveryZoneID == deliveryZoneId && r.EffectiveStartDate <= claimPeriod.StartDate && r.EffectiveEndDate >= claimPeriod.StartDate);

            if (transaction.MaterialType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "TireRims").DefinitionValue)
            {
                transaction.OnRoadRate = (OnRoadRate != null) ? OnRoadRate.ItemRate : 0.00M;
                transaction.OffRoadRate = (OffRoadRate != null) ? OffRoadRate.ItemRate : 0.00M;

                returnVal = (decimal)(Math.Round((double)(transaction.OnRoadWeight * transaction.OnRoadRate), 2, MidpointRounding.AwayFromZero) + Math.Round((double)(transaction.OffRoadWeight * transaction.OffRoadRate), 2, MidpointRounding.AwayFromZero));
            }

            if (transaction.MaterialType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.MaterialType, "UsedTireSale").DefinitionValue)
            {
                var calculationDetailListOnroad = new List<ProcessorCalculationDetailViewModel>();
                var calculationDetailListOffroad = new List<ProcessorCalculationDetailViewModel>();

                var transactionItems = transaction.TireCountList;
                foreach (var ti in transactionItems)
                {
                    item = DataLoader.TransactionItems.Single(c => c.ID == ti.ItemID);

                    var calculationDetailItem = new ProcessorCalculationDetailViewModel()
                    {
                        Name = ti.ShortName,
                        EstWeight = ti.AverageWeight,//Math.Round(DataConversion.ConvertKgToTon((double)ti.AverageWeight), 4, MidpointRounding.AwayFromZero),
                        ScaleWeight = ti.ActualWeight//Math.Round(DataConversion.ConvertKgToTon((double)ti.ActualWeight), 4, MidpointRounding.AwayFromZero)
                    };

                    //Fixing rounding issue 
                    if (item.ItemType == OnRoadItemTypeId)
                    {
                        calculationDetailListOnroad.Add(calculationDetailItem);
                    }
                    else
                    {
                        calculationDetailListOffroad.Add(calculationDetailItem);
                    }
                }

                var onroadWeight = Math.Round(calculationDetailListOnroad.Sum(c => c.ScaleWeight), 4, MidpointRounding.AwayFromZero);
                var onroadRate = (OnRoadRate != null) ? OnRoadRate.ItemRate : 0.00M;
                var offroadWeight = Math.Round(calculationDetailListOffroad.Sum(c => c.ScaleWeight), 4, MidpointRounding.AwayFromZero);
                var offroadRate = (OffRoadRate != null) ? OffRoadRate.ItemRate : 0.00M;

                returnVal = Math.Round(onroadRate * onroadWeight, 2) + Math.Round(offroadRate * offroadWeight, 2, MidpointRounding.AwayFromZero);
            }
            return (returnVal * -1) ?? 0;
        }
        public static decimal GetSPSClaimAmount(ProcessorCommonTransactionListViewModel transaction)
        {
            var weight = Math.Round(DataConversion.ConvertKgToTon((double)transaction.TotalWeight), 4, MidpointRounding.AwayFromZero);
            var rate = transaction.Rate ?? 0;
            return Math.Round(rate * weight, 2, MidpointRounding.AwayFromZero);
        }

        public static void PopulateItemsForClaimDetails(List<ClaimDetailViewModel> claimDetails)
        {
            claimDetails.ForEach(c =>
            {
                c.TransactionItems.ForEach(i =>
                {
                    i.Item = DataLoader.Items.FirstOrDefault(q => q.ID == i.ItemID);
                });
            });
        }

        public static decimal GetItemStandardWeight(int itemId, DateTime dateTime)
        {
            return DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == itemId && i.EffectiveStartDate <= dateTime && i.EffectiveEndDate >= dateTime).StandardWeight;

        }

        public static GpTireItem GetTireItem(List<TransactionItem> transactionItems, DateTime claimStartDate)
        {
            var result = new GpTireItem();
            var query = transactionItems.GroupBy(c => c.ItemID).Select(c => new { ItemId = c.Key, Qty = c.Sum(i => i.Quantity) });
            query.ToList().ForEach(c =>
            {
                var item = DataLoader.TransactionItems.FirstOrDefault(i => i.ID == c.ItemId);
                if (item.ShortName == "PLT")
                {
                    result.PLT_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.PLT = c.Qty ?? 0;
                    //result.PLTWeight = result.PLT * itemStandardWeight;
                }
                if (item.ShortName == "MT")
                {
                    result.MT_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.MT = c.Qty ?? 0;
                    //result.MTWeight = result.MT * itemStandardWeight;
                }
                if (item.ShortName == "AGLS")
                {
                    result.AGLS_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.AGLS = c.Qty ?? 0;
                    //result.AGLSWeight = result.AGLS * itemStandardWeight;
                }
                if (item.ShortName == "IND")
                {
                    result.IND_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.IND = c.Qty ?? 0;
                    //result.INDWeight = result.IND * itemStandardWeight;
                }
                if (item.ShortName == "SOTR")
                {
                    result.SOTR_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.SOTR = c.Qty ?? 0;
                    //result.SOTRWeight = result.SOTR * itemStandardWeight;
                }
                if (item.ShortName == "MOTR")
                {
                    result.MOTR_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.MOTR = c.Qty ?? 0;
                    //result.MOTRWeight = result.MOTR * itemStandardWeight;
                }
                if (item.ShortName == "LOTR")
                {
                    result.LOTR_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.LOTR = c.Qty ?? 0;
                    //result.LOTRWeight = result.LOTR * itemStandardWeight;
                }
                if (item.ShortName == "GOTR")
                {
                    result.GOTR_standeredWeight = ClaimHelper.GetItemStandardWeight(c.ItemId, claimStartDate);
                    result.GOTR = c.Qty ?? 0;
                    //result.GOTRWeight = result.GOTR * itemStandardWeight;
                }
            });
            return result;
        }
    }
}
