﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using CL.TMS.ExceptionHandling.Exceptions;

namespace CL.TMS.ExceptionHandling.Exceptions
{
    [Serializable]
    public class CLDuplicateWarningException : BaseException
    {
        public CLDuplicateWarningException()
        { 
        }

        public CLDuplicateWarningException(string message) 
            : base(message)
        { 
        }

        public CLDuplicateWarningException(string message, ExceptionType type)
            : base(message)
        {
            Type = type;
        }

        public CLDuplicateWarningException(string message, Exception innerException)
            : base(message, innerException)
        { 
        }

        protected CLDuplicateWarningException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
            {
                this.Type = (ExceptionType)Enum.Parse(typeof(ExceptionType), info.GetString("OtsExceptionType"));
            }

        }

        public ExceptionType Type { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info != null)
            {
                info.AddValue("OtsExceptionType", this.Type);
            }
        }

        public enum ExceptionType : byte
        {
            [Description("Error")]
            Error = 2,

            [Description("Warning")]
            Warning = 3,
        }
    }
}
