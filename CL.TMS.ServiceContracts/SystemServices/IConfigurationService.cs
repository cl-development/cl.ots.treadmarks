﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.ServiceContracts.ConfigurationsServices
{
    public interface IConfigurationsServices
    {
        RateDetailsVM LoadRateDetailsByTransactionID(int RateTransactionID, int category, bool isSpecific);
        PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<RateListViewModel, int> LoadTIPayeeList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        bool AddNewRate(RateDetailsVM newRate, long userId, int category);
        bool updateRate(RateDetailsVM rateVM, long userId);
        bool RemoveRateTransaction(int rateTransactionID, int category);
        bool IsFutureRateExists(int category);
        bool IsFutureRateExists(int category, bool isSpecific);
        bool IsFutureCollectorRateExists(int category);
        InternalNoteViewModel AddTransactionNote(int transactionID, string notes);
        InternalNoteViewModel AddTransactionNote(string transactionID, string notes);
        InternalNoteViewModel AddPayeeNote(int transactionID, string notes);
        List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID);
        List<InternalNoteViewModel> LoadTIPayeeNotesByID(int rateTransactionID);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);
        List<InternalNoteViewModel> ExportPayeeNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText);
        bool IsCollectorRateTransactionExists(int category);
        bool IsFutureCollectorRateTransactionExists(int category);
        AppSettingsVM LoadAppSettings();
        List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType);

        #region //Account Thresholds
        AccountThresholdsVM LoadAccountThresholds();
        bool UpdateAccountThresholds(AccountThresholdsVM vm);
        bool updateAppSettings(AppSettingsVM appSettingsVM, long userId);
        #endregion

        #region Transaction Thresholds
        TransactionThresholdsVM LoadTransactionThresholds();
        bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM);
        #endregion

        #region TCR Service Threshold
        PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO);
        TCRServiceThresholdListViewModel LoadTCRServiceThresholdList();
        bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool RemoveTCRServiceThresohold(int id, long userId);
        IEnumerable<TCRListViewModel> ExportTCRServiceThresholds();
        #endregion
    }
}
