﻿using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using System.Collections.Generic;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IProductCompositionServices
    {

        PaginationDTO<RecoverableMaterialsVM, int> LoadRecoverableMaterials(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        PaginationDTO<MaterialCompositionsVM, int> LoadMaterialCompositions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        bool CheckMaterialNameUnique(string name);
   
        bool SaveRecoverableMaterial(RecoverableMaterialsVM vm);

        bool SaveMaterialComposition(MaterialCompositionsVM vm);

        //Estimated Weights
        PaginationDTO<RecoveryEstimatedWeightListViewModel, int> LoadRecoveryEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<SupplyEstimatedWeightViewModel, int> LoadSupplyEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        bool AddNewRecoveryEstimatedWeight(WeightDetailsVM newWeight, long userId, int category);
        bool updateRecoveryEstimatedWeight(WeightDetailsVM weightVM, long userId);
        bool RemoveWeightTransaction(int weightTransactionID, int category);
        WeightDetailsVM LoadRecoveryEstimatedWeightDetailsByTransactionID(int WeightTransactionID, int category);
        SupplyEstimatedWeightViewModel LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category);
        bool updateSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM, long userId);

        InternalNoteViewModel AddTransactionNote(int transactionID, string notes);
        List<InternalNoteViewModel> LoadWeightTransactionNoteByID(int weightTransactionID);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);

    }
}