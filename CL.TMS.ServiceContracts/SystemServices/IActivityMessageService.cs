﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IActivityMessageService<T> 
        where T: class
    {


        IList<T> GetActivityMessages(long? applicationId);

        IList<T> GetAllActivityMessages(long? applicationId, string searchValue);


    }
}
