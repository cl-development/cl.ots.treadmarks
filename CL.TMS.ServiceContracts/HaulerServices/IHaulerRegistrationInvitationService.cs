﻿using CL.TMS.DataContracts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.ServiceContracts.HaulerServices
{
    public interface IHaulerRegistrationInvitationService// : IRegistrationService<HaulerRegistrationModel>
    {
        ApplicationInvitation GetApplicationInvitationByEmail(string emailID, string participantType);
        int GetParticipantTypeID(string participantName);
        bool ComposeAndSendEmail(ApplicationInvitation appInvitation);
        ApplicationInvitation GetByTokenID(Guid guid);
        void Update(ApplicationInvitation appInvitation);
        Guid GetTokenByApplicationId(int applicationId);
        string GetEmailByApplicationId(int applicationId);
        //IList<Vendor> GetVendorsByApplicationId(int applicationId);
    }
}
