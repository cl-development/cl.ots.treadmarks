﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.PDFGenerator.Models
{
    public class DeviceLabelModel
    {
        public string DeviceID
        {
            get;
            set;
        }

        public string BusinessName
        {
            get;
            set;
        }
        public string RegistrationNumber
        {
            get;
            set;
        }

        public string BarcodeGroup
        {
            get;
            set;
        }


        public DeviceLabelModel Clone()
        {
            return (DeviceLabelModel)this.MemberwiseClone();
        }
    }
}
