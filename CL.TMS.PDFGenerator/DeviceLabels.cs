﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.PDFGenerator.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace CL.TMS.PDFGenerator
{
    public class DeviceLabels
    {
        #region Fields
        byte[] Content;
        string TemplatePath;
        int LabelsPerPage = 6;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates an object based on PDF tempalte file path
        /// </summary>     
        public DeviceLabels(string templatePath)
        {
            TemplatePath = templatePath;
        }
        public DeviceLabels(byte[] content)
        {
            this.Content = content;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Create labels from PDF tempalte
        /// Fills form fields and stamps Barcode
        /// </summary>     
        #region CreateLabelsFromTempalte
        private List<byte[]> CreateLabelsFromTempalte(List<DeviceLabelModel> deviceList)
        {
            List<byte[]> list = new List<byte[]>();

            //Inserts 6 labels in byte stream list
            for (int index = 0; index < deviceList.Count; index++)
            {
                using (PdfReader reader = new PdfReader(TemplatePath))
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        using (PdfStamper stamper = new PdfStamper(reader, stream))
                        {
                            PdfContentByte cb = stamper.GetOverContent(1);

                            //Identify form field and insert values
                            Dictionary<string, string> formValues = new Dictionary<string, string>();
                            formValues.Add("AssignedTo", deviceList[index].RegistrationNumber + ", " + deviceList[index].BusinessName);
                            formValues.Add("DeviceID", "Device ID: " + deviceList[index].DeviceID);
                            formValues.Add("BarcodeGroup", string.Format("“Bar Code” Group “{0}”", deviceList[index].BarcodeGroup));

                            foreach (string key in formValues.Keys)
                            {
                                stamper.AcroFields.SetField(key, formValues[key]);
                            }

                            //Form - no longer fillable
                            stamper.FormFlattening = true;

                            //Insert barcode now
                            Barcode39 code39 = new Barcode39();
                            string code = deviceList[index].DeviceID;
                            code39.Code = code.ToUpper();
                            code39.StartStopText = false;
                            code39.BarHeight = 35;

                            Image image = code39.CreateImageWithBarcode(cb, BaseColor.BLACK, BaseColor.BLACK);
                            //stupid image takes absolute position
                            image.SetAbsolutePosition(70, 345);

                            //needs to align/scale it properly with different sizes (depends on size of device name)                          
                            image.ScaleAbsolute(158, 50);

                            //White Rectangle for barcode background 
                            cb.Rectangle(image.AbsoluteX - 10, image.AbsoluteY, image.ScaledWidth + 20, image.ScaledHeight + 10);
                            cb.SetColorFill(BaseColor.WHITE);
                            cb.Fill();

                            cb.AddImage(image);
                        }

                        list.Add(stream.ToArray());
                    }
                }
            }
            return list;
        }
        #endregion

        /// <summary>        
        /// Merges the labels byte stream on single page
        /// shows only desired no. of labels - saving printer ink
        /// </summary>     
        #region MergeOnSinglePage
        private byte[] MergeOnSinglePage(List<byte[]> sourceFiles)
        {
            float LabelWidth = 225;
            float LabelHeight = 255;
            float PageTop = 220;
            float X = 0;
            float Y = 0;
            float LineXHor = 25;
            float LineXHorOffset = 50;
            float LineYHor = 795;

            Document document = new Document();
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter copy = PdfWriter.GetInstance(document, ms);
                //Open the document
                document.Open();
                PdfContentByte pdfContentByte = copy.DirectContent;
                document.NewPage();
                // Iterate through all pdf documents
                for (int fileCounter = 0; fileCounter < sourceFiles.Count; fileCounter++)
                {
                    // Create pdf reader
                    PdfReader reader = new PdfReader(sourceFiles[fileCounter]);

                    PdfImportedPage importedPage = copy.GetImportedPage(reader, 1);
                    //document.SetPageSize(reader.GetPageSizeWithRotation(1));

                    switch (fileCounter)
                    {
                        case 0:
                            X = 0; Y = PageTop;
                            break;
                        case 1:
                            X = LabelWidth; Y = PageTop;
                            break;
                        case 2:
                            X = 0; Y = PageTop - LabelHeight;
                            //3rd Horizontal Line Required only if more than 2 labels
                            pdfContentByte.MoveTo(LineXHor, LineYHor - (LabelHeight * 2));
                            pdfContentByte.LineTo((LabelWidth * 2) + LineXHorOffset, LineYHor - (LabelHeight * 2));
                            break;
                        case 3:
                            X = LabelWidth; Y = PageTop - LabelHeight;
                            break;
                        case 4:
                            X = 0; Y = PageTop - (LabelHeight * 2);
                            break;
                        case 5:
                            X = LabelWidth; Y = PageTop - (LabelHeight * 2);
                            break;
                    }


                    //Horizontal Lines
                    pdfContentByte.MoveTo(LineXHor, LineYHor);
                    pdfContentByte.LineTo((LabelWidth * 2) + LineXHorOffset, LineYHor);

                    pdfContentByte.MoveTo(LineXHor, LineYHor - LabelHeight);
                    pdfContentByte.LineTo((LabelWidth * 2) + LineXHorOffset, LineYHor - LabelHeight);

                    float LineXVerOffset = 36.5f;
                    float LineYVerOffset = 42;
                    float LineYVer = 18;

                    //Vertical Line
                    pdfContentByte.MoveTo(LabelWidth + LineXVerOffset, LineYVer);
                    pdfContentByte.LineTo(LabelWidth + LineXVerOffset, (LabelHeight * 3) + LineYVerOffset);

                    pdfContentByte.Stroke();
                    pdfContentByte.AddTemplate(importedPage, X, Y);

                    copy.FreeReader(reader);
                    reader.Close();
                }
                document.Close();
                return ms.ToArray();
            }
        }
        #endregion

        /// <summary>        
        /// Concat one page behind another 
        /// </summary>     
        public void Concat(byte[] secondContent)
        {
            int pageOffset = 0;
            ArrayList master = new ArrayList();
            int f = 0;
            Document document = null;
            PdfCopy writer = null;

            using (MemoryStream firstPDF = new MemoryStream(this.Content))
            {
                using (MemoryStream secondPDF = new MemoryStream(secondContent))
                {
                    Stream[] fileNames = new Stream[] { firstPDF, secondPDF };

                    using (MemoryStream outFile = new MemoryStream())
                    {
                        while (f < fileNames.Length)
                        {
                            // we create a reader for a certain document
                            PdfReader reader = new PdfReader(fileNames[f]);
                            reader.ConsolidateNamedDestinations();
                            // we retrieve the total number of pages
                            int n = reader.NumberOfPages;
                            pageOffset += n;
                            if (f == 0)
                            {
                                // step 1: creation of a document-object
                                document = new Document(reader.GetPageSizeWithRotation(1));
                                // step 2: we create a writer that listens to the document
                                writer = new PdfCopy(document, outFile);
                                // step 3: we open the document
                                document.Open();
                            }
                            // step 4: we add content
                            for (int i = 0; i < n; )
                            {
                                ++i;
                                if (writer != null)
                                {
                                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                                    writer.AddPage(page);
                                }
                            }

                            //Error: Additional information: Document iTextSharp.text.pdf.PdfReader has already been added.
                            //PRAcroForm form = reader.AcroForm;
                            //if ( form != null && writer != null )
                            //{
                            //    writer.AddDocument( reader );
                            //}

                            f++;
                        }
                        // step 5: we close the document
                        if (document != null)
                        {
                            document.Close();
                        }

                        this.Content = outFile.ToArray();
                    }
                }
            }
        }

        /// <summary>        
        /// Generates device labels based on list provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateDeviceLabels(List<DeviceLabelModel> deviceLabelList)
        {
            int lastPageItemsCount = deviceLabelList.Count % LabelsPerPage;
            int totalPageCount = (int)Math.Ceiling((double)deviceLabelList.Count / LabelsPerPage);

            for (int index = 0; index < totalPageCount; index++)
            {
                if (index == 0)
                    this.Content = MergeOnSinglePage(CreateLabelsFromTempalte(deviceLabelList.GetRange(index, deviceLabelList.Count < LabelsPerPage ? deviceLabelList.Count : LabelsPerPage)));
                else if ((index + 1 == totalPageCount && lastPageItemsCount > 0))
                    Concat(MergeOnSinglePage(CreateLabelsFromTempalte(deviceLabelList.GetRange(index * LabelsPerPage, lastPageItemsCount))));
                else
                    Concat(MergeOnSinglePage(CreateLabelsFromTempalte(deviceLabelList.GetRange(index * LabelsPerPage, LabelsPerPage))));
            }
        }

        /// <summary>        
        /// Generates device label based on model provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateSingleDeviceLabel(DeviceLabelModel deviceLabel)
        {
            this.Content = MergeOnSinglePage(CreateLabelsFromTempalte(new List<DeviceLabelModel>() { deviceLabel }));
        }

        public byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                this.Save(stream);

                return stream.ToArray();
            }
        }
        public void Save(Stream stream)
        {
            if (this.Content.Length > 0)
            {
                stream.Write(this.Content, 0, this.Content.Length);
            }
        }

        #endregion
    }
}
