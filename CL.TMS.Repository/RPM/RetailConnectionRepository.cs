﻿using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.RPM;
using System;
using System.Data.Entity;
using System.Linq;
using CL.TMS.Common.Extension;
using System.Collections.Generic;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Export;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Email;
using CL.TMS.DataContracts.RetailConnectionService;

namespace CL.TMS.Repository.RPM
{
    public class RetailConnectionRepository : BaseRepository<RPMBoundedContext, Product, int>, IRetailConnectionRepository
    {
        public void AddProductAttachment(AttachmentModel attachmentModel)
        {
            var dbContext = context as RPMBoundedContext;
            var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
            dbContext.Attachments.Add(attachment);
            dbContext.SaveChanges();

            attachmentModel.ID = attachment.ID;
        }

        public List<RetailerReference> LoadParticipantRetailer(int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from retailer in dbContext.Retailers
                        where (retailer.VendorId == vendorId || retailer.VendorId == null) && retailer.Status == "Active"
                        select new RetailerReference
                        {
                            Id = retailer.ID,
                            Name = retailer.Name
                        };
            return query.ToList();
        }

        public List<RetailerReference> LoadAllActiveRetailer()
        {
            var dbContext = context as RPMBoundedContext;
            var query = from retailer in dbContext.Retailers
                        where retailer.Status == "Active"
                        select new RetailerReference
                        {
                            Id = retailer.ID,
                            Name = retailer.Name
                        };
            return query.ToList();
        }

        public PaginationDTO<RetailerViewModel, int> LoadParticipantRetailer(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from retailer in dbContext.Retailers
                        where (retailer.VendorId == vendorId || (retailer.VendorId == null && retailer.Status!=TreadMarksConstants.InactiveStatus)) && retailer.Status!=TreadMarksConstants.RejectedStatus
                        select new RetailerViewModel
                        {
                            ID = retailer.ID,
                            ApprovedDate = retailer.ApprovedDate,
                            Status = retailer.Status,
                            RetailerName = retailer.Name,
                            AddedBy = retailer.IsAddedByParticipant ? retailer.AddedByUser.FirstName + " " + retailer.AddedByUser.LastName : "STAFF",
                            IsAddedByParticipant = retailer.IsAddedByParticipant,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.ApprovedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ApprovedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.ApprovedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.RetailerName.ToString().ToLower().Contains(searchText)
                                            || c.AddedBy.ToString().ToLower().Contains(searchText)
                                       );
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "RetailerName" : orderBy;
            if (sortDirection == "asc")
                query = query.AsQueryable<RetailerViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<RetailerViewModel>().OrderBy(orderBy + " descending"); ;

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RetailerViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<ParticipantProductsViewModel, int> LoadParticipantProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from product in dbContext.Products
                        where (product.VendorId == vendorId) && product.Status != TreadMarksConstants.RejectedStatus
                        select new ParticipantProductsViewModel
                        {
                            ID = product.ID,
                            ApprovedDate = product.ApprovedDate,
                            Status = product.Status,
                            ProductName = product.Name,
                            BrandName = product.BrandName,
                            AddedBy = product.AddedUser.FirstName + " " + product.AddedUser.LastName
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.ApprovedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ApprovedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.ApprovedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.ProductName.ToString().Contains(searchText)
                                            || c.BrandName.ToString().ToLower().Contains(searchText)
                                            || c.AddedBy.ToString().ToLower().Contains(searchText)
                                       );
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "ProductName" : orderBy;
            if (sortDirection == "asc")
                query = query.AsQueryable<ParticipantProductsViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<ParticipantProductsViewModel>().OrderBy(orderBy + " descending"); ;

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ParticipantProductsViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<StaffCategoryViewModel, int> LoadProductCategory(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from category in dbContext.ProductCategoris
                        select new StaffCategoryViewModel
                        {
                            ID = category.ID,
                            AddedDate = category.CreatedDate,
                            Status = category.Status,
                            CategoryName = category.Name,
                            StaffCategoryNotes = category.CategoryNotes.OrderByDescending(c => c.CreatedDate).ToList()
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.CategoryName.ToString().Contains(searchText)
                                       );
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "ProductName" : orderBy;
            if (sortDirection == "asc")
                query = query.AsQueryable<StaffCategoryViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<StaffCategoryViewModel>().OrderBy(orderBy + " descending"); ;

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<StaffCategoryViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<StaffProductViewModel, int> LoadStaffProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            IQueryable<StaffProductViewModel> query;
            if (vendorId == 0)
            {
                query = from product in dbContext.Products
                        where product.Status!=TreadMarksConstants.RejectedStatus
                        select new StaffProductViewModel
                        {
                            ID = product.ID,
                            ApprovedDate = product.ApprovedDate,
                            Status = product.Status,
                            ProductName = product.Name,
                            BrandName = product.BrandName,
                            Category = product.ProductCategory.Name,
                            BusinessName = product.ProductVendor.BusinessName,
                            RegistrationNumber = product.ProductVendor.Number,
                            AddedBy = product.AddedUser.FirstName + " " + product.AddedUser.LastName
                        };
            }
            else
            {
                query = from product in dbContext.Products
                        where product.VendorId == vendorId
                        select new StaffProductViewModel
                        {
                            ID = product.ID,
                            ApprovedDate = product.ApprovedDate,
                            Status = product.Status,
                            ProductName = product.Name,
                            BrandName = product.BrandName,
                            Category = product.ProductCategory.Name,
                            BusinessName = product.ProductVendor.BusinessName,
                            RegistrationNumber = product.ProductVendor.Number,
                            AddedBy = product.AddedUser.FirstName + " " + product.AddedUser.LastName
                        };
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.ApprovedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ApprovedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.ApprovedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.Category.ToString().Contains(searchText)
                                            || c.ProductName.ToString().Contains(searchText)
                                            || c.BrandName.ToString().ToLower().Contains(searchText)
                                            || c.AddedBy.ToString().ToLower().Contains(searchText)
                                            || c.RegistrationNumber.ToString().Contains(searchText)
                                            || c.BusinessName.ToLower().Contains(searchText)
                                       );
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "ProductName" : orderBy;
            if (sortDirection == "asc")
                query = query.AsQueryable<StaffProductViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<StaffProductViewModel>().OrderBy(orderBy + " descending"); ;

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<StaffProductViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<RetailerViewModel, int> LoadStaffRetailers(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            IQueryable<RetailerViewModel> query;
            if (vendorId == 0)
            {
                query = from retailer in dbContext.Retailers
                        where retailer.Status!=TreadMarksConstants.RejectedStatus
                        select new RetailerViewModel
                        {
                            ID = retailer.ID,
                            ApprovedDate = retailer.ApprovedDate,
                            Status = retailer.Status,
                            RetailerName = retailer.Name,
                            AddedBy = retailer.AddedByUser.FirstName + " " + retailer.AddedByUser.LastName,
                            StaffRetailerNotes = retailer.RetailerNotes.OrderByDescending(c => c.CreatedDate).ToList()
                        };
            }
            else
            {
                query = from retailer in dbContext.Retailers
                        where retailer.VendorId == vendorId
                        select new RetailerViewModel
                        {
                            ID = retailer.ID,
                            ApprovedDate = retailer.ApprovedDate,
                            Status = retailer.Status,
                            RetailerName = retailer.Name,
                            AddedBy = retailer.AddedByUser.FirstName + " " + retailer.AddedByUser.LastName,
                            StaffRetailerNotes = retailer.RetailerNotes.OrderByDescending(c => c.CreatedDate).ToList()
                        };

            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.ApprovedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ApprovedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.ApprovedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Status.ToLower().Contains(searchText)
                                            || c.RetailerName.ToString().Contains(searchText)
                                            || c.AddedBy.ToString().ToLower().Contains(searchText)
                                       );
                }
            }

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "ProductName" : orderBy;
            if (sortDirection == "asc")
                query = query.AsQueryable<RetailerViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<RetailerViewModel>().OrderBy(orderBy + " descending"); ;

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RetailerViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public AttachmentModel GetProductAttachment(int attachmentId)
        {
            var dbContext = context as RPMBoundedContext;

            var attachment = dbContext.Attachments.FirstOrDefault(a => a.ID == attachmentId);
            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveTemporaryProductAttachment(int attachmentId)
        {
            var dbContext = (context as RPMBoundedContext);

            var attachment = dbContext.Attachments.FirstOrDefault(a => a.ID == attachmentId);
            if (attachment != null)
            {
                dbContext.Attachments.Remove(attachment);
                dbContext.SaveChanges();
            }
        }

        public List<StaffCategoryViewModel> LoadProductCategory()
        {
            var dbContext = context as RPMBoundedContext;
            var query = from productCategory in dbContext.ProductCategoris
                        where productCategory.Status == "Active"
                        select new StaffCategoryViewModel
                        {
                            ID = productCategory.ID,
                            CategoryName = productCategory.Name
                        };
            return query.ToList();
        }

        public void AddProduct(Product product)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.Products.Add(product);
            dbContext.SaveChanges();
        }

        public void AddCategory(ProductCategory category)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.ProductCategoris.Add(category);
            dbContext.SaveChanges();
        }

        public void AddCategoryNote(CategoryNote note)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.CategoryNotes.Add(note);
            dbContext.SaveChanges();
        }

        public bool CheckRetailerName(string retailerName)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(r => r.Name == retailerName);
            if (retailer != null)
            {
                return true;
            }
            return false;
        }
        public void AddRetailer(Retailer retailer)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.Retailers.Add(retailer);
            dbContext.SaveChanges();
        }

        public void AddRetailerNote(RetailerNote note)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.RetailerNotes.Add(note);
            dbContext.SaveChanges();
        }

        public int GetCategoryIdByCategoryName(string categoryName)
        {
            var dbContext = context as RPMBoundedContext;
            var category = dbContext.ProductCategoris.FirstOrDefault(c => c.Name == categoryName);
            return category.ID;
        }

        public int GetRetailerIdByRetailerName(string retailerName)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(c => c.Name == retailerName);
            return retailer.ID;
        }

        public void AddRetailerProducts(List<RetailerProduct> retailerProducts)
        {
            var dbContext = context as RPMBoundedContext;
            dbContext.RetailerProducts.AddRange(retailerProducts);
            dbContext.SaveChanges();
        }

        public void UpdateStaffProduct(ProductsFormViewModel productViewModel, long userId)
        {
            var dbContext = context as RPMBoundedContext;
            var dbProduct = dbContext.Products.FirstOrDefault(c => c.ID == productViewModel.Id);
            //Update product
            if (dbProduct != null)
            {
                dbProduct.Name = productViewModel.ProductName;
                dbProduct.CategoryId = productViewModel.CategoryId;
                dbProduct.BrandName = productViewModel.BrandName;
                dbProduct.Description = productViewModel.Description;
                if (productViewModel.UploadDocument != null)
                {
                    dbProduct.AttachmentId = productViewModel.UploadDocument.AttachmentId;                   
                }                    
            }

            //Update retailer product
            var dbRetailerProducts = dbContext.RetailerProducts.Where(c => c.ProductId == productViewModel.Id).ToList();
            dbContext.RetailerProducts.RemoveRange(dbRetailerProducts);

            var retailerProducts = new List<RetailerProduct>();
            productViewModel.RetailerProducts.ForEach(c =>
            {
                var retailerProduct = new RetailerProduct();
                retailerProduct.RetailerId = c.RetailerId;
                retailerProduct.ProductId = productViewModel.Id;
                retailerProduct.ProductUrl = c.ProductUrl;
                retailerProduct.CreatedDate = DateTime.UtcNow;
                retailerProduct.AddedBy = userId;
                retailerProducts.Add(retailerProduct);
            });
            dbContext.RetailerProducts.AddRange(retailerProducts);

            dbContext.SaveChanges();
        }

        public void UpdateRetailerProduct(ProductsFormViewModel productViewModel, int productId, long userId)
        {
            var dbContext = context as RPMBoundedContext;

            var dbProduct = dbContext.Products.FirstOrDefault(c => c.ID == productViewModel.Id);
            //Update product attachment only
            if (dbProduct != null)
            {
                if (productViewModel.UploadDocument == null)
                {
                    dbProduct.AttachmentId = null;
                }  
            }

            var dbRetailerProducts = dbContext.RetailerProducts.Where(c => c.ProductId == productId).ToList();
            dbContext.RetailerProducts.RemoveRange(dbRetailerProducts);

            var retailerProducts = new List<RetailerProduct>();
            productViewModel.RetailerProducts.ForEach(c =>
            {
                var retailerProduct = new RetailerProduct();
                retailerProduct.RetailerId = c.RetailerId;
                retailerProduct.ProductId = productId;
                retailerProduct.ProductUrl = c.ProductUrl;
                retailerProduct.CreatedDate = DateTime.UtcNow;
                retailerProduct.AddedBy = userId;
                retailerProducts.Add(retailerProduct);
            });
            dbContext.RetailerProducts.AddRange(retailerProducts);
            dbContext.SaveChanges();
        }

        public List<RetailerNote> LoadRetailerInternalNotes(int retailerId)
        {
            var dbContext = context as RPMBoundedContext;

            var query = dbContext.RetailerNotes.AsNoTracking().Include(c => c.AddedBy).Where(cl => cl.RetailerId == retailerId);

            return query.ToList();

        }

        public Product GetProduct(int productId)
        {
            var dbContext = context as RPMBoundedContext;
            return dbContext.Products.Include(c => c.ProductCategory).Include(c => c.ProductAttachment).FirstOrDefault(c => c.ID == productId);
        }

        public List<RetailerProductFormViewModel> GetProductRetailers(int productId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from retailerProduct in dbContext.RetailerProducts
                        where retailerProduct.ProductId == productId
                        select new RetailerProductFormViewModel
                        {
                            RetailerId = retailerProduct.RetailerId,
                            RetailerName = retailerProduct.Retailer.Name,
                            ProductUrl = retailerProduct.ProductUrl,
                            DateAdded = retailerProduct.CreatedDate,
                            AddedBy = retailerProduct.AddedByUser.FirstName + " " + retailerProduct.AddedByUser.LastName
                        };
            return query.ToList();

        }

        public void ChangeProductStatus(int productId, string fromStatus, string toStatus)
        {
            var dbContext = context as RPMBoundedContext;
            var product = dbContext.Products.FirstOrDefault(c => c.ID == productId && c.Status == fromStatus);
            if (product != null)
            {
                product.Status = toStatus;
                if (toStatus == TreadMarksConstants.ActiveStatus)
                {
                    product.ApprovedDate = DateTime.UtcNow;
                }
                if (toStatus == TreadMarksConstants.RejectedStatus)
                {
                    //remove product
                    var retailerProducts = dbContext.RetailerProducts.Where(r => r.ProductId == productId).ToList();
                    dbContext.RetailerProducts.RemoveRange(retailerProducts);
                    dbContext.Products.Remove(product);
                }
                dbContext.SaveChanges();
            }
        }

        public ProductRetailerApproveEmailModel GetEmailInforByProductId(int productId)
        {
            var dbContext = context as RPMBoundedContext;
            var product = dbContext.Products.FirstOrDefault(p => p.ID == productId);
            if (product != null)
            {
                var emailInfor = new ProductRetailerApproveEmailModel();
                var primaryContact = RepositoryHelper.FindPrimaryContact(product.ProductVendor.VendorAddresses.ToList());
                emailInfor.EmailAddress = primaryContact != null ? primaryContact.Email : product.ProductVendor.LocationEmailAddress;
                emailInfor.Name = product.Name;
                return emailInfor;
            }

            return null;
        }

        public ProductRetailerApproveEmailModel GetEmailInforByRetailerId(int retailerId)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(r => r.ID == retailerId);
            if (retailer != null)
            {
                if (retailer.IsAddedByParticipant)
                {
                    var emailInfor = new ProductRetailerApproveEmailModel();
                    
                    var primaryContact = RepositoryHelper.FindPrimaryContact(retailer.Vendor.VendorAddresses.ToList());
                    emailInfor.EmailAddress = primaryContact != null ? primaryContact.Email : retailer.Vendor.LocationEmailAddress;
                    emailInfor.Name = retailer.Name;
                    return emailInfor;
                }
            }
            return null;
        }

        public void ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus)
        {
            var dbContext = context as RPMBoundedContext;
            var category = dbContext.ProductCategoris.FirstOrDefault(c => c.ID == categoryId && c.Status == fromStatus);
            if (category != null)
            {
                category.Status = toStatus;
                dbContext.SaveChanges();
            }
        }

        public void ChangeRetailerStatus(int retailerId, string fromStatus, string toStatus)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(c => c.ID == retailerId && c.Status == fromStatus);
            if (retailer != null)
            {
                retailer.Status = toStatus;
                if (toStatus == TreadMarksConstants.ActiveStatus)
                {
                    retailer.ApprovedDate = DateTime.UtcNow;
                }

                if (toStatus == TreadMarksConstants.RejectedStatus)
                {
                    dbContext.Retailers.Remove(retailer);
                }
                dbContext.SaveChanges();
            }
        }

        public RetailersFormViewModel LoadRetailerById(int retailerId)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(c => c.ID == retailerId);
            if (retailer != null)
            {
                var result = new RetailersFormViewModel();
                result.Id = retailer.ID;
                result.RetailerName = retailer.Name;
                result.Url = retailer.Url;
                result.ChainOnline = retailer.IsOnline;
                result.VendorId = retailer.VendorId;
                if ((!retailer.IsOnline) && (retailer.RetailerAddress != null))
                {
                    result.AddressLine1 = retailer.RetailerAddress.Address1;
                    result.AddressLine2 = retailer.RetailerAddress.Address2;
                    result.City = retailer.RetailerAddress.City;
                    result.Province = retailer.RetailerAddress.Province;
                    result.PostalCode = retailer.RetailerAddress.PostalCode;
                    result.Country = retailer.RetailerAddress.Country;
                    result.PhoneNumber = retailer.RetailerAddress.Phone;
                }

                retailer.RetailerNotes.ForEach(c =>
                {
                    var retailerNote = new RetailerNoteViewModel();
                    retailerNote.Id = c.ID;
                    retailerNote.AddedOn = c.CreatedDate;
                    retailerNote.AddedBy = c.AddedBy.FirstName + " " + c.AddedBy.LastName;
                    retailerNote.Note = c.Note;
                    retailerNote.RetailerId = retailer.ID;
                    result.RetailerNotes.Add(retailerNote);
                });
                return result;
            }
            return null;
        }

        public void EditRetailer(RetailersFormViewModel retailerFormViewModel)
        {
            var dbContext = context as RPMBoundedContext;
            var retailer = dbContext.Retailers.FirstOrDefault(c => c.ID == retailerFormViewModel.Id);
            if (retailer != null)
            {
                var originalIsOnline = retailer.IsOnline;
                retailer.Name = retailerFormViewModel.RetailerName;
                retailer.IsOnline = retailerFormViewModel.ChainOnline;
                retailer.Url = retailerFormViewModel.Url;
                if (!retailer.IsOnline)
                {
                    if (retailer.RetailerAddress != null)
                    {
                        retailer.RetailerAddress.Address1 = retailerFormViewModel.AddressLine1;
                        retailer.RetailerAddress.Address2 = retailerFormViewModel.AddressLine2;
                        retailer.RetailerAddress.City = retailerFormViewModel.City;
                        retailer.RetailerAddress.Province = retailerFormViewModel.Province;
                        retailer.RetailerAddress.PostalCode = retailerFormViewModel.PostalCode;
                        retailer.RetailerAddress.Country = retailerFormViewModel.Country;
                        retailer.RetailerAddress.Phone = retailerFormViewModel.PhoneNumber;
                    }
                    else
                    {
                        //Add a new address
                        var address = new Address();
                        address.Address1 = retailerFormViewModel.AddressLine1;
                        address.Address2 = retailerFormViewModel.AddressLine2;
                        address.AddressType = 1;
                        address.City = retailerFormViewModel.City;
                        address.Province = retailerFormViewModel.Province;
                        address.PostalCode = retailerFormViewModel.PostalCode;
                        address.Country = retailerFormViewModel.Country;
                        address.Phone = retailerFormViewModel.PhoneNumber;
                        retailer.RetailerAddress = address;
                    }
                }
                else
                {
                    if ((originalIsOnline) && (retailer.RetailerAddress != null))
                    {
                        //remove old address
                        dbContext.Addresses.Remove(retailer.RetailerAddress);
                    }
                }
                dbContext.SaveChanges();
            }

        }

        public List<ParticipantRetailerExportModel> GetParticipantRetailerExport(int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var result = from retailer in dbContext.Retailers
                         where (retailer.VendorId == vendorId || retailer.VendorId == null) && retailer.Status!=TreadMarksConstants.RejectedStatus
                         select new ParticipantRetailerExportModel
                         {
                             //RetailerId = retailer.ID.ToString(),
                             DateAdded = retailer.ApprovedDate,
                             Status = retailer.Status,
                             RetailerName = retailer.Name,
                             AddedBy = retailer.AddedByUser != null ? (retailer.IsAddedByParticipant ? retailer.AddedByUser.FirstName + " " + retailer.AddedByUser.LastName : "STAFF") : string.Empty,

                             Url = retailer.Url,
                             ChainOnline = retailer.IsOnline,
                             AddressLine1 = retailer.RetailerAddress.Address1 != null ? retailer.RetailerAddress.Address1 : string.Empty,
                             AddressLine2 = retailer.RetailerAddress.Address2 != null ? retailer.RetailerAddress.Address2 : string.Empty,
                             City = retailer.RetailerAddress.City != null ? retailer.RetailerAddress.City : string.Empty,
                             Province = retailer.RetailerAddress.Province != null ? retailer.RetailerAddress.Province : string.Empty,
                             PostalCode = retailer.RetailerAddress.PostalCode != null ? retailer.RetailerAddress.PostalCode : string.Empty,
                             Country = retailer.RetailerAddress.Country != null ? retailer.RetailerAddress.Country : string.Empty,
                             PhoneNumber = retailer.RetailerAddress.Phone != null ? retailer.RetailerAddress.Phone : string.Empty,
                         };

            return result.ToList();
        }

        public List<StaffRetailerExportModel> GetStaffRetailerExport(int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var staffRetailerModel = new List<StaffRetailerExportModel>();
            List<Retailer> retailers;
            if (vendorId == 0)
            {
                retailers = dbContext.Retailers.Where(c=>c.Status!=TreadMarksConstants.RejectedStatus).ToList();
            }
            else
            {
                retailers = dbContext.Retailers.Where(c=>(c.VendorId==vendorId || c.VendorId==null) && c.Status!=TreadMarksConstants.RejectedStatus).ToList();
            }

            retailers.ForEach(retailer =>
            {
                var result = new StaffRetailerExportModel()
                {
                    ApprovedDate = retailer.ApprovedDate,
                    Status = retailer.Status,
                    RetailerName = retailer.Name,
                    AddedBy = retailer.AddedByUser != null ? (retailer.AddedByUser.FirstName + " " + retailer.AddedByUser.LastName) : string.Empty,
                    Url = retailer.Url,
                    ChainOnline = retailer.IsOnline
                };

                if (retailer.RetailerAddress != null)
                {
                    result.AddressLine1 = retailer.RetailerAddress.Address1;
                    result.AddressLine2 = retailer.RetailerAddress.Address2;
                    result.City = retailer.RetailerAddress.City;
                    result.Province = retailer.RetailerAddress.Province;
                    result.PostalCode = retailer.RetailerAddress.PostalCode;
                    result.Country = retailer.RetailerAddress.Country;
                    result.PhoneNumber = retailer.RetailerAddress.Phone;
                }

                retailer.RetailerNotes.ToList().ForEach(note =>
                {
                    if (note != null)
                    {
                        result.InternalNote += note.Note + Environment.NewLine;
                    }

                });

                staffRetailerModel.Add(result);
            });

            return staffRetailerModel;
        }

        public List<ParticipantProductExportModel> GetParticipantProductExport(int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var participantProductModel = new List<ParticipantProductExportModel>();

            dbContext.Products.Where(c => c.VendorId == vendorId).ToList().ForEach(product =>
            {
                var participantProdModel = new ParticipantProductExportModel()
                {
                    ApprovedDate = product.ApprovedDate,
                    Status = product.Status,
                    ProductName = product.Name,
                    BrandName = product.BrandName,
                    AddedBy = product.AddedUser != null ? (product.AddedUser.FirstName + " " + product.AddedUser.LastName) : string.Empty,
                    CategoryName = product.ProductCategory != null ? product.ProductCategory.Name : string.Empty,
                    Description = product.Description,
                };

                int totalRetailerProdCount = product.RetailerProducts.Count;
                int count = 0;
                product.RetailerProducts.ForEach(retailer =>
                {
                    if (count == totalRetailerProdCount - 1)
                    {
                        participantProdModel.RetailerNameAndUrl += string.Format("{0} - {1}", retailer.Retailer.Name, retailer.ProductUrl);
                    }
                    else
                    {
                        participantProdModel.RetailerNameAndUrl += string.Format("{0} - {1}, ", retailer.Retailer.Name, retailer.ProductUrl);
                    }
                    
                    count++;
                });
                if (!string.IsNullOrEmpty(participantProdModel.RetailerNameAndUrl)) {
                    participantProdModel.RetailerNameAndUrl = participantProdModel.RetailerNameAndUrl.Substring(0, participantProdModel.RetailerNameAndUrl.Length - 1);
                }
                participantProductModel.Add(participantProdModel);

            });

            return participantProductModel;
        }

        public List<StaffProductExportModel> GetStaffProductExport(int vendorId)
        {
            var dbContext = context as RPMBoundedContext;
            var staffProductModel = new List<StaffProductExportModel>();

            List<Product> products;
            if (vendorId == 0)
            {
                products = dbContext.Products.ToList();
            }
            else
            {
                products = dbContext.Products.Where(c=>c.VendorId==vendorId).ToList();
            }

            products.ForEach(product =>
            {
                var staffProdModel = new StaffProductExportModel()
                {
                    ApprovedDate = product.ApprovedDate,
                    Status = product.Status,
                    ProductName = product.Name,
                    BrandName = product.BrandName,
                    BusinessName = product.ProductVendor.BusinessName,
                    RegistrationNumber = product.ProductVendor.Number,
                    AddedBy = product.AddedUser != null ? (product.AddedUser.FirstName + " " + product.AddedUser.LastName) : string.Empty,
                    CategoryName = product.ProductCategory != null ? product.ProductCategory.Name : string.Empty,
                    Description = product.Description,
                };

                int totalRetailerProdCount = product.RetailerProducts.Count;
                int count = 0;
                product.RetailerProducts.ForEach(retailer =>
                {
                    if (count == totalRetailerProdCount - 1)
                    {
                        staffProdModel.RetailerNameAndUrl += string.Format("{0} - {1}", retailer.Retailer.Name, retailer.ProductUrl);
                    }
                    else
                    {
                        staffProdModel.RetailerNameAndUrl += string.Format("{0} - {1}, ", retailer.Retailer.Name, retailer.ProductUrl);
                    }
                    count++;
                });

                if (!string.IsNullOrEmpty(staffProdModel.RetailerNameAndUrl))
                {
                    staffProdModel.RetailerNameAndUrl = staffProdModel.RetailerNameAndUrl.Substring(0, staffProdModel.RetailerNameAndUrl.Length - 1);
                }
                staffProductModel.Add(staffProdModel);

            });

            return staffProductModel;
        }

        public List<ProductCategoryExportModel> GetProductCategoryExport()
        {
            var dbContext = context as RPMBoundedContext;
            var productCategoryModel = new List<ProductCategoryExportModel>();

            dbContext.ProductCategoris.ToList().ForEach(category =>
            {
                var result = new ProductCategoryExportModel()
                {
                    CategoryId = category.ID.ToString(),
                    AddedDate = category.CreatedDate,
                    Status = category.Status,
                    CategoryName = category.Name
                };

                category.CategoryNotes.ToList().ForEach(note =>
                {
                    if (note != null)
                    {
                        result.Notes += note.Note + Environment.NewLine;
                    }

                });

                productCategoryModel.Add(result);

            });

            return productCategoryModel;
        }

        public StaffCategoryFormViewModel LoadCategoryById(int categoryId)
        {
            var dbContext = context as RPMBoundedContext;
            var category = dbContext.ProductCategoris.FirstOrDefault(c => c.ID == categoryId);
            if (category != null)
            {
                var result = new StaffCategoryFormViewModel();
                result.CategoryName = category.Name;

                category.CategoryNotes.ForEach(c =>
                {
                    result.InternalNote = c.Note;
                    result.UserId = c.UserId;
                    result.InternalNoteAddedBy = c.AddedBy != null ? (c.AddedBy.FirstName + " " + c.AddedBy.LastName) : string.Empty;
                    result.InternalNoteAddedOn = c.CreatedDate;
                });
                return result;
            }
            return null;
        }

        public List<CategoryModel> LoadActiveCategory()
        {
            var dbContext = context as RPMBoundedContext;
            var query = dbContext.ProductCategoris.Where(c => c.Status == TreadMarksConstants.ActiveStatus).Select(c => new CategoryModel
            {
                Id = c.ID,
                Name = c.Name
            });
            return query.ToList();
        }

        public List<CollectorModel> LoadActiveCollectors()
        {
            var dbContext = context as RPMBoundedContext;
            var collectorModel = new List<CollectorModel>();
            List<Vendor> collectors = dbContext.Vendors.Include(c=>c.VendorAddresses).Where(v => v.VendorType == 2 && v.IsActive).ToList();
            collectors.ForEach(collector =>
            {
                var result = new CollectorModel()
                {
                    RegistrationNumber=collector.Number,
                    BusinessName=collector.BusinessName,
                };
                var location=collector.VendorAddresses.FirstOrDefault(a=>a.AddressType==1);
                if (location!=null)
                {
                    result.Address = location.Address1 + "" + location.Address2;
                    result.City = location.City;
                    result.Province = location.Province;
                    result.PostalCode = location.PostalCode;
                    result.Country = location.Country;
                    result.PhoneNumber = location.Phone;
                }
                collectorModel.Add(result);
            });
            return collectorModel;
        }

        public List<ProductListModel> LoadActiveProductList(int categoryId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = dbContext.Products.Where(p => p.CategoryId == categoryId && p.Status == TreadMarksConstants.ActiveStatus).Select(p => new ProductListModel
            {
                Id=p.ID,
                CategoryId=p.CategoryId,
                Title=p.Name,
                Brand=p.BrandName,
                ImageUrl = p.ProductAttachment.FilePath
            });
            return query.ToList();
        }

        public Product LoadProductDetails(int productId)
        {
            var dbContext = context as RPMBoundedContext;
            return dbContext.Products.FirstOrDefault(p => p.ID == productId);
        }

        public List<RetailerModel> LoadProductRetailers(int productId)
        {
            var dbContext = context as RPMBoundedContext;
            var query = from retailerProduct in dbContext.RetailerProducts
                        where retailerProduct.ProductId == productId
                        select new RetailerModel
                        {
                            Id=retailerProduct.ID,
                            Name=retailerProduct.Retailer.Name,
                            Url=retailerProduct.ProductUrl
                        };
            return query.ToList();
        }
    }
}
