﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using CL.Framework.Common;
using System.Data.Entity;
using System.Net.Mail;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Common.Extension;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Common;
using System.Reflection;
using System.Diagnostics;

namespace CL.TMS.Repository.System
{
    public class ConfigurationsRepository : BaseRepository<ConfigurationsBoundedContext, Rate, int>, IConfigurationsRepository
    {
        #region //Loading Rates
        public List<Rate> LoadRateTransactionByID(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.Rates.Where(c => c.RateTransactionID == rateTransactionID).ToList();

        }
        public PayeeList LoadTIPayeeListByID(int tiPayeeID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.PayeeLists.FirstOrDefault(c => c.ID == tiPayeeID);

        }
        //Loads all currently effective rates
        public IEnumerable<Rate> GetCurrentRates(int[] iclaimTypes)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var today = DateTime.Today;

            return dbContext.Rates.Where(c => iclaimTypes.Contains(c.ClaimType) && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today);
        }

        public IEnumerable<Rate> GetCurrentCollectorRates(int iclaimType)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var today = DateTime.Today;
            return dbContext.Rates.Where(c => c.ClaimType == iclaimType && (c.period.StartDate > today && c.period.PeriodType == (int)ClaimType.Collector) && c.EffectiveStartDate <= today && c.EffectiveEndDate >= today).OrderByDescending(x => x.EffectiveStartDate);
        }
        public Period GetLatestPeriod(int iPeriodType)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var today = DateTime.Today;
            return dbContext.Periods.Where(c => c.PeriodType == iPeriodType).OrderByDescending(x => x.StartDate).First();
        }

        public PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            var dbContext = context as ConfigurationsBoundedContext;
           
            var query = dbContext.RateTransactions.Where(c => c.Category == categoryId).AsNoTracking()
                        .Select(c => new RateListViewModel()
                        {
                            ID = c.ID,
                            EffectiveStartDate = c.EffectiveStartDate,
                            EffectiveEndDate = c.EffectiveEndDate,
                            DateAdded = c.CreatedDate,
                            AddedBy = c.CreatedByUser.FirstName + " " + c.CreatedByUser.LastName,
                            ModifiedBy = (c.ModifiedByUser != null) ? c.ModifiedByUser.FirstName + " " + c.ModifiedByUser.LastName : "",
                            DateModified = c.ModifiedDate,
                            RateNotes = c.RateTransactionNotes.OrderByDescending(i => i.CreatedDate).ToList(),
                            Category = c.Category,
                        });

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "EffectiveStartDate" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RateListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }
        public PaginationDTO<RateListViewModel, int> LoadTIPayeeList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var query = dbContext.PayeeLists.AsNoTracking()
                        .Select(c => new RateListViewModel()
                        {
                            ID = c.ID,
                            EffectiveStartDate = c.EffectiveStartDate,
                            EffectiveEndDate = c.EffectiveEndDate,
                            DateAdded = c.CreatedDate,
                            AddedBy = c.CreatedByUser.FirstName + " " + c.CreatedByUser.LastName,
                            ModifiedBy = (c.ModifiedByUser != null) ? c.ModifiedByUser.FirstName + " " + c.ModifiedByUser.LastName : "",
                            DateModified = c.ModifiedDate,
                            TIPayeeNotes = c.TIPayeeNotes.OrderByDescending(i => i.CreatedDate).ToList(),
                            Category = categoryId// TreadMarksConstants.Ra //c.Tr..Category,
                        });

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "EffectiveStartDate" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<RateListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }
        public PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var query = dbContext.RateTransactions
                        .Where(c => c.Category == categoryId)
                        .Select(c => new PIRateListViewModel()
                        {
                            ID = c.ID,
                            PIType = c.IsSpecificRate ?? false,
                            EffectiveStartDate = c.EffectiveStartDate,
                            EffectiveEndDate = c.EffectiveEndDate,
                            DateAdded = c.CreatedDate,
                            AddedBy = c.CreatedByUser.FirstName + " " + c.CreatedByUser.LastName,
                            ModifiedBy = (c.ModifiedByUser != null) ? c.ModifiedByUser.FirstName + " " + c.ModifiedByUser.LastName : "",
                            DateModified = c.ModifiedDate,
                            RateNotes = c.RateTransactionNotes.OrderByDescending(i => i.CreatedDate).ToList(),
                            Category = c.Category,
                            AssociateVendorIDs = dbContext.VendorRates
                                                .Where(p => p.RateTransactionID == c.ID && c.IsSpecificRate.HasValue && c.IsSpecificRate.Value)
                                               .Select(pv => pv.VendorID).ToList(),
                        });


            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "DateAdded" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();
            foreach (var item in result)
            {
                if (item.AssociateVendorIDs != null)
                {
                    item.AssociatedVendorsList = dbContext.Vendors
                        .Where(v => item.AssociateVendorIDs.Contains(v.ID))
                        .Select(x => x.Number + " " + x.BusinessName).ToList();
                }
            }
            return new PaginationDTO<PIRateListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }
        public RateTransaction FindLatestFutureRateTransaction()
        {
            var dbContext = context as ConfigurationsBoundedContext;
            if (!dbContext.RateTransactions.Any())
            {
                return new RateTransaction();
            }
            var maxValue = dbContext.RateTransactions.Max(x => x.EffectiveEndDate);

            return (context as ConfigurationsBoundedContext).RateTransactions.Where(c => c.EffectiveEndDate.Date == maxValue.Date).OrderByDescending(x => x.EffectiveStartDate).FirstOrDefault();
        }
        public bool IsFutureRateExists(int category)
        {
            if (category == 8) //checking future payeelist exist
            {
                return (context as ConfigurationsBoundedContext).PayeeLists.Any(c => c.EffectiveStartDate > DateTime.Now);              
            } else {
                return (context as ConfigurationsBoundedContext).RateTransactions.Any(c => c.EffectiveStartDate > DateTime.Now && c.Category == category);
            }
        }
        public bool IsFutureRateExists(int category, bool isSpecific)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            if (isSpecific)
            {
                var noAvailableVendorIDs = dbContext.VendorRates.Include(x => x.RateTransaction)
                                           .Where(x => x.RateTransaction.EffectiveEndDate > DateTime.Today.Date)
                                           .Select(x => x.VendorID);

                var result = dbContext.Vendors.Where(x => x.VendorType == (int)ClaimType.Processor && !noAvailableVendorIDs.Contains(x.ID))
                                         .Select(x => new ItemModel<string>()
                                         {
                                             ItemID = x.ID,
                                             ItemName = x.Number + " " + x.BusinessName
                                         });
                return !result.Any();
            }
            else
            {
                if (category == 8) //checking future payeelist exist
                {
                    return dbContext.PayeeLists.Any(c => c.EffectiveStartDate > DateTime.Now);
                }
                else
                {
                    return dbContext.RateTransactions.Any(c => c.EffectiveStartDate > DateTime.Now && c.Category == category && (!c.IsSpecificRate.HasValue || !c.IsSpecificRate.Value));
                }
            }
        }
        public bool IsFutureRateExists(int category, int paymentType)
        {
            return (context as ConfigurationsBoundedContext).Rates.Any(c => c.EffectiveStartDate > DateTime.Now && c.ClaimType == category && c.PaymentType == paymentType);
        }
        public bool IsFutureCollectorRateExists(int category)
        {
            return (context as ConfigurationsBoundedContext).Rates.Any(c => c.period.StartDate.Year > DateTime.Now.Year && c.ClaimType == ((int)ClaimType.Collector));
        }
        public bool IsCollectorRateTransactionExists(int category)
        {
            return (context as ConfigurationsBoundedContext).RateTransactions.Any(c => c.Category == ((int)ClaimType.Collector));
        }

        public bool IsFutureCollectorRateTransactionExists(int category)
        {
            return (context as ConfigurationsBoundedContext).RateTransactions.Any(c => c.Category == ((int)ClaimType.Collector) && c.EffectiveStartDate > DateTime.Now);
        }


        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.RateNotes.Where(c => c.RateTransactionID == rateTransactionID).Select(i => new InternalNoteViewModel
            {
                AddedOn = i.CreatedDate,
                Note = i.Note,
                AddedBy = i.User.FirstName + " " + i.User.LastName
            }).OrderByDescending(i => i.AddedOn).ToList();
        }
        public List<InternalNoteViewModel> LoadTIPayeeNoteByID(int tiPayeeListID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.PayeeListNotes.Where(c => c.PayeeListID == tiPayeeListID)
                .Select(i => new InternalNoteViewModel()
                {
                    AddedOn = i.CreatedDate,
                    Note = i.Note,
                    AddedBy = i.User.FirstName + " " + i.User.LastName
                }).OrderByDescending(i => i.AddedOn).ToList();
        }

        public List<Rate> LoadRatesForPeriods(IEnumerable<int> periodIDs)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.Rates.Where(c => periodIDs.Contains((int)c.PeriodID)).ToList();
        }

        public IEnumerable<ItemModel<string>> LoadAvailabledProcessorIDs()
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var lastDateOfMonth = DateTime.Today.Date.AddDays(1 - DateTime.Today.Day).AddMonths(1).AddDays(-1);

            var noAvailableVendorIDs = dbContext.VendorRates.Include(x => x.RateTransaction)
                                       .Where(x => x.RateTransaction.EffectiveEndDate > lastDateOfMonth.Date)
                                       .Select(x => x.VendorID);

            return dbContext.Vendors.Where(x => x.VendorType == (int)ClaimType.Processor && !noAvailableVendorIDs.Contains(x.ID))
                                     .Select(x => new ItemModel<string>()
                                     {
                                         ItemID = x.ID,
                                         ItemName = x.Number + " " + x.BusinessName
                                     });
        }
        public IEnumerable<ItemModel<string>> LoadAssignedProcessorIDs(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            return dbContext.VendorRates.Where(x => x.RateTransactionID == rateTransactionID)
                .Select(x => new ItemModel<string>()
                {
                    ItemID = x.VendorID,
                    ItemName = dbContext.Vendors.FirstOrDefault(v => v.ID == x.VendorID).Number + " " + dbContext.Vendors.FirstOrDefault(v => v.ID == x.VendorID).BusinessName,
                });
        }
        public IEnumerable<ItemModel<string>> LoadAvailabledHaulerIDs(int tiPayeeListID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var lastPayeelist = dbContext.PayeeLists.Find(tiPayeeListID);   
            var lastDateOfMonth = DateTime.Today.Date.AddDays(1); // 1 - DateTime.Today.Day).AddMonths(1).AddDays(-1);
            if (tiPayeeListID>0)
            {
                lastDateOfMonth = lastPayeelist.EffectiveStartDate;                
            } else
            {
                lastPayeelist = dbContext.PayeeLists.OrderByDescending(x => x.ID).FirstOrDefault();
                tiPayeeListID = lastPayeelist !=null ? lastPayeelist.ID:0;
            }

            var noAvailableVendorIDs = dbContext.VendorPayees.Include(c=>c.PayeeList)
                                       .Where(x => x.PayeeList.ID==tiPayeeListID)                                       
                                       .Select(x => x.VendorId);
            return dbContext.Vendors.Where(x => x.VendorType == (int)ClaimType.Hauler 
                                        && !noAvailableVendorIDs.Contains(x.ID)
                                        && x.CreatedDate<lastDateOfMonth
                                        )
                                     .Select(x => new ItemModel<string>()
                                     {
                                         ItemID = x.ID,
                                         ItemName = x.Number + " " + x.BusinessName
                                     });
        }
        public IEnumerable<ItemModel<string>> LoadAssignedHaulerIDs(int tiPayeeListID)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            if (tiPayeeListID > 0)
            {
                return dbContext.VendorPayees.Include(x => x.Vendor).Where(x => x.PayeeListId == tiPayeeListID)
                    .Select(x => new ItemModel<string>()
                    {
                        ItemID = x.VendorId,
                        ItemName = x.Vendor.Number + " " + x.Vendor.BusinessName,
                    });
            }
            else
            {
                var lastPayeelist = dbContext.PayeeLists.OrderByDescending(x => x.ID).FirstOrDefault();
                if (lastPayeelist != null)
                {
                    tiPayeeListID = lastPayeelist.ID;
                }
                return dbContext.VendorPayees.Include(x => x.Vendor).Include(c => c.PayeeList)
                        .Where(x => x.PayeeList.ID== tiPayeeListID)
                        .Select(x => new ItemModel<string>()
                        {
                            ItemID = x.VendorId,
                            ItemName = x.Vendor.Number + " " + x.Vendor.BusinessName,
                        });
            }
        }
        #endregion

        #region //Add Rates
        public void AddRates(RateTransaction rateTransaction, RateParamsDTO param)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            switch (param.categoryName)
            {
                case TreadMarksConstants.TransportationIncentiveRates:
                    processTransactionIncentiveRate(rateTransaction, dbContext, param.TI);
                    break;
                case TreadMarksConstants.CollectionAllowanceRates:
                    rateTransaction.EffectiveEndDate = GetLatestPeriod((int)ClaimType.Collector).EndDate;
                    break;
                case TreadMarksConstants.ProcessingIncentiveRates:
                    if (!rateTransaction.IsSpecificRate.HasValue || !rateTransaction.IsSpecificRate.Value)
                    {
                        processProcessingIncentiveRate(rateTransaction, dbContext, param.PI);
                    }
                    break;
                case TreadMarksConstants.ManufacturingIncentiveRates:
                    processManufacturingIncentiveRate(rateTransaction, dbContext, param.MI);
                    break;
                case TreadMarksConstants.TireStewardshipFeeRates:
                    processTireStewardshipRate(rateTransaction, dbContext, param.TFS);
                    break;
                case TreadMarksConstants.RemittancePenaltyRates:
                    processStewardRemittancePenaltyRate(rateTransaction, dbContext, param.PE);
                    break;
            }

            //if (param.categoryName == TreadMarksConstants.TransportationIncentiveRates)
            //{
            //    processTransactionIncentiveRate(rateTransaction, dbContext, param);
            //}
            //else if (param.categoryName == TreadMarksConstants.CollectionAllowanceRates)
            //{
            //    rateTransaction.EffectiveEndDate = GetLatestPeriod((int)ClaimType.Collector).EndDate;
            //}
            //else if (param.categoryName == TreadMarksConstants.ProcessingIncentiveRates)
            //{
            //    if (!rateTransaction.IsSpecificRate.HasValue || !rateTransaction.IsSpecificRate.Value)
            //    {
            //        processProcessingIncentiveRate(rateTransaction, dbContext, param.PI);
            //    }
            //}
            //else if (param.categoryName == TreadMarksConstants.ManufacturingIncentiveRates)
            //{
            //    processManufacturingIncentiveRate(rateTransaction, dbContext, param.MI);
            //}
            //else if (param.categoryName == TreadMarksConstants.TireStewardshipFeeRates)
            //{
            //    processTireStewardshipRate(rateTransaction, dbContext, param.TFS);
            //}
            ////else if (param.categoryName == TreadMarksConstants.EstimatedWeightsRates)
            ////{
            ////    processEstimateWeightRate(rateTransaction, dbContext, param.SW);
            ////}
            //else if (param.categoryName == TreadMarksConstants.RemittancePenaltyRates)
            //{
            //    processStewardRemittancePenaltyRate(rateTransaction, dbContext, param.PE);
            //}
            //NOTE: update the latest previous transaction effectiveEndDate;
            if (!rateTransaction.IsSpecificRate.HasValue || !rateTransaction.IsSpecificRate.Value)
            {

                var previousTransaction = dbContext.RateTransactions
                                        .Where(x => x.Category == rateTransaction.Category &&
                                        (!x.IsSpecificRate.HasValue || !x.IsSpecificRate.Value))
                                        .OrderByDescending(i => i.EffectiveStartDate).FirstOrDefault();

                if (previousTransaction != null)
                {
                    previousTransaction.EffectiveEndDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
                }
            }

            dbContext.RateTransactions.Add(rateTransaction);

            dbContext.SaveChanges();
        }

        public void AddTIPayeeList(PayeeList tiPayeeList)
        {
            var dbContext = context as ConfigurationsBoundedContext;      
            dbContext.PayeeLists.Add(tiPayeeList);
            var previousPayeeLists = dbContext.PayeeLists
                        .OrderByDescending(i => i.EffectiveStartDate).FirstOrDefault();
            if (previousPayeeLists != null)
            {
                previousPayeeLists.EffectiveEndDate = tiPayeeList.EffectiveStartDate.Date.AddDays(-1);
            }
            dbContext.SaveChanges();
        }
        private void processTransactionIncentiveRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, TransportationIncentiveParam param)
        {
            //Update effectiveenddate for the previous records
            //EffectiveEndDate for the previous rate records
            var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
            var previousRates = dbContext.Rates.Where(c => (c.ClaimType == param.claimTypeHauler || c.ClaimType == param.claimTypeProcessor)
                    && (c.PaymentType == param.paymentTypeNorth || c.PaymentType == param.paymentTypeDOT || c.PaymentType == param.paymentTypeProcessorTI)
                    && c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
            previousRates.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });

        }
        private void processManufacturingIncentiveRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, ManufacturingIncentiveParam param)
        {
            //Update effective-end-date for the previous records   
            var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
            var previousRates = dbContext.Rates.Where(c => c.ClaimType == param.claimType
                && c.ItemID.HasValue && param.items.Contains(c.ItemID.Value)
                && c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
            previousRates.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });

        }
        private void processTireStewardshipRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, TireStewardshipFeeParam param)
        {
            //Update effectiveenddate for the previous records
            //EffectiveEndDate for the previous rate records
            var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
            var previousRates = dbContext.Rates.Where(c => c.ClaimType == param.claimType
                && c.ItemID.HasValue && param.items.Contains(c.ItemID.Value)
                && c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
            previousRates.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });
        }
        private void processProcessingIncentiveRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, ProcessingIncentiveParam param)
        {
            var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
            var rates = dbContext.Rates.Where(c => c.ClaimType == param.claimType &&
                                        c.ItemID.HasValue &&
                                        param.items.Contains(c.ItemID.Value) &&
                                        (!c.IsSpecificRate.HasValue || !c.IsSpecificRate.Value) &&
                                        c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
            rates.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });
        }
        private void processStewardRemittancePenaltyRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, PenaltyParam param)
        {
            //Update effective-end-date for the previous records   
            var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
            var previousRates = dbContext.Rates.Where(c => c.ClaimType == param.claimType
                && c.PaymentType == param.ratePaymentType
                && c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
            previousRates.ForEach(c =>
            {
                c.EffectiveEndDate = previousEffectiveEndtDate;
            });
        }
        //private void processEstimateWeightRate(RateTransaction rateTransaction, ConfigurationsBoundedContext dbContext, EstimatedWeightsParam param)
        //{
        //    var previousEffectiveEndtDate = rateTransaction.EffectiveStartDate.Date.AddDays(-1);
        //    var previousRates = dbContext.ItemWeights.Where(c => param.items.Contains(c.ItemID)
        //        && c.EffectiveEndDate > rateTransaction.EffectiveStartDate).ToList();
        //    previousRates.ForEach(c =>
        //    {
        //        c.EffectiveEndDate = previousEffectiveEndtDate;
        //    });
        //}

        #endregion

        #region //Update Rates
        public bool UpdateRates(RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            if (param.categoryName == TreadMarksConstants.TransportationIncentiveRates)
            {
                updateTransactionIncentiveRates(dbContext, rateDetailsVM, param);
            }
            else if (param.categoryName == TreadMarksConstants.CollectionAllowanceRates)
            {
                updateCollectorRates(dbContext, rateDetailsVM, param);
            }
            else if (param.categoryName == TreadMarksConstants.ProcessingIncentiveRates)
            {
                updateProcessingIncentiveRatess(dbContext, rateDetailsVM, param);
            }
            else if (param.categoryName == TreadMarksConstants.ManufacturingIncentiveRates)
            {
                updateManufacturingIncentiveRate(dbContext, rateDetailsVM, param);
            }
            else if (param.categoryName == TreadMarksConstants.TireStewardshipFeeRates)
            {
                updateTireStewardshipFeeRates(dbContext, rateDetailsVM, param);
            }
            else if (param.categoryName == TreadMarksConstants.RemittancePenaltyRates)
            {
                updateStewardRemittancePenaltyRate(dbContext, rateDetailsVM, param);
            } else if (param.categoryName == TreadMarksConstants.TIPayee)
            {
                updateTIPayeeLists(dbContext, rateDetailsVM, param);
                dbContext.SaveChanges();
                return true;
            }
            //else if (param.categoryName == TreadMarksConstants.EstimatedWeightsRates)
            //{
            //    updateEstimateWeightRate(dbContext, rateDetailsVM, param);
            //}

            //NOTE: update the latest previous transaction effectiveEndDate; processor specific rate don't change previous rates start date time.
            if (!rateDetailsVM.isSpecific)
            {
                DateTime previousEndDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
                var previousTransactions = dbContext.RateTransactions.Where(x => x.Category == rateDetailsVM.category &&
                                                                            (!x.IsSpecificRate.HasValue || !x.IsSpecificRate.Value)
                                                                           )
                                                                .OrderByDescending(i => i.EffectiveStartDate)
                                                                .Skip(1)
                                                                .Take(1)
                                                                .FirstOrDefault();
                if (previousTransactions != null)
                {
                    previousTransactions.EffectiveEndDate = rateDetailsVM.effectiveStartDate.Date.AddDays(-1);
                }
            }

            dbContext.SaveChanges();

            return true;
        }

        private void updateCollectorRates(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {
            //1. Get the current period based on selected effectiveStartDate from UI
            var period = this.GetPeriodByDate(rateDetailsVM.effectiveStartDate, ((int)ClaimType.Collector));
            var dbPeriodId = dbContext.Rates.Where(c => c.RateTransactionID == rateDetailsVM.RateTransactionID).OrderBy(c => c.PeriodID).Select(c => c.PeriodID).FirstOrDefault();

            //update all rate after period.id
            var rates = dbContext.Rates.Include(c => c.item).Where(c => c.period.PeriodType == 2 && c.PeriodID >= period.ID).ToList();
            rates.ForEach(i =>
            {
                i.RateTransactionID = rateDetailsVM.RateTransactionID;
                switch (i.item.ShortName)
                {
                    case "PLT":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.PLT;
                        break;
                    case "MT":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.MT;
                        break;
                    case "AGLS":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.AGLS;
                        break;
                    case "IND":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.IND;
                        break;
                    case "SOTR":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.SOTR;
                        break;
                    case "MOTR":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.MOTR;
                        break;
                    case "LOTR":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.LOTR;
                        break;
                    case "GOTR":
                        i.ItemRate = rateDetailsVM.collectorAllowanceRate.GOTR;
                        break;
                    default:
                        break;
                }
            });

            if (dbPeriodId < period.ID)
            {
                //rollback rate between dbperiodid and period
                var previousPeriodId = dbContext.Periods.Where(c => c.PeriodType == 2 && c.ID < dbPeriodId).OrderByDescending(c => c.ID).Select(c => c.ID).FirstOrDefault();
                if (previousPeriodId != 0)
                {
                    var previousPeriodRates = dbContext.Rates.Where(c => c.PeriodID == previousPeriodId).ToList();
                    var rollbackRates = dbContext.Rates.Include(c => c.item).Where(c => c.period.PeriodType == 2 && c.PeriodID >= dbPeriodId && c.PeriodID < period.ID).ToList();
                    var previousRateTransactionId = previousPeriodRates.FirstOrDefault().RateTransactionID;
                    rollbackRates.ForEach(i =>
                    {
                        i.RateTransactionID = previousRateTransactionId;
                        switch (i.item.ShortName)
                        {
                            case "PLT":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "PLT").FirstOrDefault().ItemRate;
                                break;
                            case "MT":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "MT").FirstOrDefault().ItemRate;
                                break;
                            case "AGLS":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "AGLS").FirstOrDefault().ItemRate;
                                break;
                            case "IND":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "IND").FirstOrDefault().ItemRate;
                                break;
                            case "SOTR":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "SOTR").FirstOrDefault().ItemRate;
                                break;
                            case "MOTR":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "MOTR").FirstOrDefault().ItemRate;
                                break;
                            case "LOTR":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "LOTR").FirstOrDefault().ItemRate;
                                break;
                            case "GOTR":
                                i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "GOTR").FirstOrDefault().ItemRate;
                                break;
                            default:
                                break;
                        }
                    });
                }
            }

            var rateTransaction = dbContext.RateTransactions.FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

            rateTransaction.RateTransactionNotes.Add(new RateTransactionNote() { Note = rateDetailsVM.note, UserID = param.userId, CreatedDate = DateTime.UtcNow });
            rateTransaction.EffectiveStartDate = period.StartDate;
            rateTransaction.ModifiedByID = param.userId;
            rateTransaction.ModifiedDate = DateTime.UtcNow;
        }
        private void updateTransactionIncentiveRates(ConfigurationsBoundedContext dbContext, RateDetailsVM transactionIncentiveRateVM, RateParamsDTO param)
        {
            var rateTransaction = dbContext.RateTransactions.Include(c => c.Rates).Include(c => c.RateTransactionNotes).FirstOrDefault(c => c.ID == transactionIncentiveRateVM.RateTransactionID);

            //Update rate transaction
            rateTransaction.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
            rateTransaction.ModifiedDate = DateTime.UtcNow;
            rateTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(transactionIncentiveRateVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = transactionIncentiveRateVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = param.userId;
                if (rateTransaction.RateTransactionNotes == null)
                    rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }

            #region Update NorthernPremium Rates
            var n1SturgeonFallRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            if (n1SturgeonFallRateOnRoad != null)
            {
                n1SturgeonFallRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n1SturgeonFallRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n1SturgeonFallRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SturgeonFallRateOnRoad;
            }

            var n1SouthRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n1SouthRateOnRoad != null)
            {
                n1SouthRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n1SouthRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n1SouthRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOnRoad;
            }

            var n2SturgeonFallRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            if (n2SturgeonFallRateOnRoad != null)
            {
                n2SturgeonFallRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n2SturgeonFallRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n2SturgeonFallRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SturgeonFallRateOnRoad;
            }

            var n2SouthRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n2SouthRateOnRoad != null)
            {
                n2SouthRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n2SouthRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n2SouthRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOnRoad;
            }

            var n3SturgeonFallRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            if (n3SturgeonFallRateOnRoad != null)
            {
                n3SturgeonFallRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n3SturgeonFallRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n3SturgeonFallRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SturgeonFallRateOnRoad;
            }

            var n3SouthRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n3SouthRateOnRoad != null)
            {
                n3SouthRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n3SouthRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n3SouthRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOnRoad;
            }

            var n4SturgeonFallRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            if (n4SturgeonFallRateOnRoad != null)
            {
                n4SturgeonFallRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n4SturgeonFallRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n4SturgeonFallRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SturgeonFallRateOnRoad;
            }

            var n4SouthRateOnRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n4SouthRateOnRoad != null)
            {
                n4SouthRateOnRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n4SouthRateOnRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n4SouthRateOnRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOnRoad;
            }

            var n1SouthRateOffRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n1SouthRateOffRoad != null)
            {
                n1SouthRateOffRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n1SouthRateOffRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n1SouthRateOffRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOffRoad;
            }

            var n2SouthRateOffRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n2SouthRateOffRoad != null)
            {
                n2SouthRateOffRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n2SouthRateOffRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n2SouthRateOffRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOffRoad;
            }


            var n3SouthRateOffRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n3SouthRateOffRoad != null)
            {
                n3SouthRateOffRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n3SouthRateOffRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n3SouthRateOffRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOffRoad;
            }

            var n4SouthRateOffRoad = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType == param.TI.paymentTypeNorth && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            if (n4SouthRateOffRoad != null)
            {
                n4SouthRateOffRoad.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n4SouthRateOffRoad.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n4SouthRateOffRoad.ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOffRoad;
            }
            #endregion

            #region Update DOT Rates
            var n1OffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n1ZoneId);
            if (n1OffRoadRate != null)
            {
                n1OffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n1OffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n1OffRoadRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N1OffRoadRate;
            }

            var n2OffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n2ZoneId);
            if (n2OffRoadRate != null)
            {
                n2OffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n2OffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n2OffRoadRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N2OffRoadRate;
            }

            var n3OffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n3ZoneId);
            if (n3OffRoadRate != null)
            {
                n3OffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n3OffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n3OffRoadRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N3OffRoadRate;
            }

            var n4OffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.n4ZoneId);
            if (n4OffRoadRate != null)
            {
                n4OffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                n4OffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                n4OffRoadRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N4OffRoadRate;
            }

            var mooseCreekDOTRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.southZoneId && c.DeliveryZoneID == param.TI.mooseCreekZoneId);
            if (mooseCreekDOTRate != null)
            {
                mooseCreekDOTRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                mooseCreekDOTRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                mooseCreekDOTRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.MooseCreekOffRoadRate;
            }

            var gtaDOTRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.southZoneId && c.DeliveryZoneID == param.TI.gtaZoneId);
            if (gtaDOTRate != null)
            {
                gtaDOTRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                gtaDOTRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                gtaDOTRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.GTAOffRoadRate;
            }

            var wtcDOTRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.southZoneId && c.DeliveryZoneID == param.TI.wtcZoneId);
            if (wtcDOTRate != null)
            {
                wtcDOTRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                wtcDOTRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                wtcDOTRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.WTCOffRoadRate;
            }

            var sturgeonFallsDOTRate = rateTransaction.Rates.FirstOrDefault(c => c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeDOT && c.ClaimType == param.TI.claimTypeHauler && c.SourceZoneID == param.TI.southZoneId && c.DeliveryZoneID == param.TI.sturgeonFallsZoneId);
            if (sturgeonFallsDOTRate != null)
            {
                sturgeonFallsDOTRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                sturgeonFallsDOTRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                sturgeonFallsDOTRate.ItemRate = transactionIncentiveRateVM.DOTPremiumRate.SturgeonFallsOffRoadRate;
            }

            #endregion

            #region Update TI Rates
            var mooseCreekTIOnRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.mooseCreekZoneId);
            if (mooseCreekTIOnRoadRate != null)
            {
                mooseCreekTIOnRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                mooseCreekTIOnRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                mooseCreekTIOnRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOnRoadRate;
            }

            var mooseCreekTIOffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.mooseCreekZoneId);
            if (mooseCreekTIOffRoadRate != null)
            {
                mooseCreekTIOffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                mooseCreekTIOffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                mooseCreekTIOffRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOffRoadRate;
            }

            var gtaTIOnRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.gtaZoneId);
            if (gtaTIOnRoadRate != null)
            {
                gtaTIOnRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                gtaTIOnRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                gtaTIOnRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOnRoadRate;
            }

            var gtaTIOffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.gtaZoneId);
            if (gtaTIOffRoadRate != null)
            {
                gtaTIOffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                gtaTIOffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                gtaTIOffRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOffRoadRate;
            }

            var wtcTIOnRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.wtcZoneId);
            if (wtcTIOnRoadRate != null)
            {
                wtcTIOnRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                wtcTIOnRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                wtcTIOnRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOnRoadRate;
            }

            var wtcTIOffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.wtcZoneId);
            if (wtcTIOffRoadRate != null)
            {
                wtcTIOffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                wtcTIOffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                wtcTIOffRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOffRoadRate;
            }

            var sturgeonFallsTIOnRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOnRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.sturgeonFallsZoneId);
            if (sturgeonFallsTIOnRoadRate != null)
            {
                sturgeonFallsTIOnRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                sturgeonFallsTIOnRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                sturgeonFallsTIOnRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOnRoadRate;
            }

            var sturgeonFallsTIOffRoadRate = rateTransaction.Rates.FirstOrDefault(c => c.ItemType == param.TI.itemTypeOffRoad && c.PaymentType.HasValue && c.PaymentType == param.TI.paymentTypeProcessorTI && c.ClaimType == param.TI.claimTypeProcessor && c.DeliveryZoneID == param.TI.sturgeonFallsZoneId);
            if (sturgeonFallsTIOffRoadRate != null)
            {
                sturgeonFallsTIOffRoadRate.EffectiveStartDate = transactionIncentiveRateVM.effectiveStartDate;
                sturgeonFallsTIOffRoadRate.EffectiveEndDate = transactionIncentiveRateVM.effectiveEndDate;
                sturgeonFallsTIOffRoadRate.ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOffRoadRate;
            }
            #endregion

            //update previous Rate-effective-start-date
            DateTime effectiveStartDate = transactionIncentiveRateVM.previousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.Rates.Where(i => (i.ClaimType == param.TI.claimTypeHauler || i.ClaimType == param.TI.claimTypeProcessor)
                && i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
            });
        }
        private void updateManufacturingIncentiveRate(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {
            var rateTransaction = dbContext.RateTransactions.Include(c => c.Rates).Include(c => c.RateTransactionNotes).FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

            //Update rate transaction
            rateTransaction.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
            rateTransaction.ModifiedDate = DateTime.UtcNow;
            rateTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(rateDetailsVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = rateDetailsVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = param.userId;
                if (rateTransaction.RateTransactionNotes == null)
                    rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }
            #region Update MI Rates
            var calendared = rateTransaction.Rates.FirstOrDefault(c => c.ItemID == param.MI.calendaredId && c.RateTransactionID == rateDetailsVM.RateTransactionID);
            if (calendared != null)
            {
                calendared.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
                calendared.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
                calendared.ItemRate = rateDetailsVM.manufacturingIncentiveRate.Calendared;
            }
            var extruded = rateTransaction.Rates.FirstOrDefault(c => c.ItemID == param.MI.extrudedId && c.RateTransactionID == rateDetailsVM.RateTransactionID);
            if (extruded != null)
            {
                extruded.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
                extruded.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
                extruded.ItemRate = rateDetailsVM.manufacturingIncentiveRate.Extruded;
            }
            var molded = rateTransaction.Rates.FirstOrDefault(c => c.ItemID == param.MI.moldedId && c.RateTransactionID == rateDetailsVM.RateTransactionID);
            if (molded != null)
            {
                molded.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
                molded.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
                molded.ItemRate = rateDetailsVM.manufacturingIncentiveRate.Molded;
            }
            #endregion

            //update previous Rate-effective-start-date
            DateTime effectiveStartDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.Rates.Where(i => i.ClaimType == param.MI.claimType
                && i.ItemID.HasValue && param.MI.items.Contains(i.ItemID.Value)
                && i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
            });
        }
        private void updateTireStewardshipFeeRates(ConfigurationsBoundedContext dbContext, RateDetailsVM StewardrateDetailsVM, RateParamsDTO param)
        {
            var rateTransaction = dbContext.RateTransactions.Include(c => c.Rates).Include(c => c.RateTransactionNotes).FirstOrDefault(c => c.ID == StewardrateDetailsVM.RateTransactionID);

            //Update rate transaction
            rateTransaction.EffectiveStartDate = StewardrateDetailsVM.effectiveStartDate;
            rateTransaction.ModifiedDate = DateTime.UtcNow;
            rateTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(StewardrateDetailsVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = StewardrateDetailsVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = param.userId;
                if (rateTransaction.RateTransactionNotes == null)
                    rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }
            var weightItems = StewardrateDetailsVM.tireStewardshipFeesRate.GetType().GetProperties().Where(x => x.PropertyType.Name == "Decimal");
            var paramItems = param.TFS.GetType().GetProperties();
            foreach (var property in weightItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.TFS);
                var item = rateTransaction.Rates.FirstOrDefault(c => c.ItemID == paramID && c.RateTransactionID == StewardrateDetailsVM.RateTransactionID);
                if (item != null)
                {
                    item.EffectiveStartDate = StewardrateDetailsVM.effectiveStartDate;
                    item.EffectiveEndDate = StewardrateDetailsVM.effectiveEndDate;
                    item.ItemRate = (decimal)property.GetValue(StewardrateDetailsVM.tireStewardshipFeesRate);
                }
            }
            //update previous Rate-effective-start-date
            DateTime effectiveStartDate = StewardrateDetailsVM.previousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.Rates.Where(i => i.ClaimType == param.TFS.claimType
                && i.ItemID.HasValue && param.TFS.items.Contains(i.ItemID.Value)
                && i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
            });
        }
        private void updateProcessingIncentiveRatess(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {

            var rateTransaction = dbContext.RateTransactions
                                .Include(c => c.Rates)
                                .Include(c => c.PIRateMappings)
                                .Include(c => c.RateTransactionNotes)
                                .FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

            //Update rate transaction
            rateTransaction.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
            if (rateDetailsVM.isSpecific)
            {
                rateTransaction.EffectiveEndDate = rateDetailsVM.effectiveEndDate.Date;
            }

            rateTransaction.ModifiedDate = DateTime.UtcNow;
            rateTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(rateDetailsVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = rateDetailsVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = param.userId;
                if (rateTransaction.RateTransactionNotes == null)
                    rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }

            var weightItems = rateDetailsVM.processingIncentiveRate.GetType().GetProperties();
            var paramItems = param.PI.GetType().GetProperties();
            foreach (var property in weightItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.PI);
                var item = rateTransaction.Rates.FirstOrDefault(c => c.ItemID == paramID && c.RateTransactionID == rateDetailsVM.RateTransactionID);
                if (item != null)
                {
                    item.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
                    item.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
                    item.ItemRate = (decimal)property.GetValue(rateDetailsVM.processingIncentiveRate);
                }
            }

            //update previous Rate-effective-start-date. processor specific rate don't change previous rates start date time.
            if (!rateDetailsVM.isSpecific)
            {
                DateTime effectiveStartDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
                var previousRates = dbContext.Rates.Where(i => i.ClaimType == param.PI.claimType
                    && i.ItemID.HasValue && param.PI.items.Contains(i.ItemID.Value)
                    && i.EffectiveEndDate == effectiveStartDate).ToList();
                previousRates.ForEach(i =>
                {
                    i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
                });
            }
            else
            {
                //delete old assignedProcessors;
                var assignedProcessors = rateTransaction.PIRateMappings.ToList();
                if (assignedProcessors != null && assignedProcessors.Count > 0)
                {
                    dbContext.VendorRates.RemoveRange(assignedProcessors);
                }
                //add new assigned processors
                if (rateDetailsVM.piSpecificRate != null)
                {
                    rateTransaction.PIRateMappings = new List<VendorRate>();
                    var piRate = new VendorRate();
                    foreach (var item in rateDetailsVM.piSpecificRate.AssignedProcessIDs)
                    {
                        piRate = new VendorRate()
                        {
                            RateTransactionID = rateDetailsVM.RateTransactionID,
                            VendorID = item.ItemID
                        };
                        rateTransaction.PIRateMappings.Add(piRate);
                    }
                }
            }
        }
        private void updateStewardRemittancePenaltyRate(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {
            var rateTransaction = dbContext.RateTransactions.Include(c => c.Rates).Include(c => c.RateTransactionNotes).FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

            //Update rate transaction
            rateTransaction.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
            rateTransaction.ModifiedDate = DateTime.UtcNow;
            rateTransaction.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(rateDetailsVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = rateDetailsVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = param.userId;
                if (rateTransaction.RateTransactionNotes == null)
                    rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }
            #region Update Penalty Rates
            var stewardRemittancePenalty = rateTransaction.Rates.FirstOrDefault(c => c.RateTransactionID == rateDetailsVM.RateTransactionID);
            if (stewardRemittancePenalty != null)
            {
                stewardRemittancePenalty.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
                stewardRemittancePenalty.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
                stewardRemittancePenalty.ItemRate = rateDetailsVM.penaltyRate.StewardRemittancePenaltyRate;
            }

            #endregion

            //update previous Rate-effective-start-date
            DateTime effectiveStartDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.Rates.Where(i => i.ClaimType == param.PE.claimType
                && i.PaymentType == param.PE.ratePaymentType
                && i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
            });
        }
        //private void updateEstimateWeightRate(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        //{
        //    var rateTransaction = dbContext.RateTransactions.Include(c => c.ItemWeights).Include(c => c.RateTransactionNotes).FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

        //    //Update rate transaction
        //    rateTransaction.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
        //    rateTransaction.ModifiedDate = DateTime.UtcNow;
        //    rateTransaction.ModifiedByID = param.userId;

        //    //Add rate transaction note
        //    if (!string.IsNullOrEmpty(rateDetailsVM.note))
        //    {
        //        //add note
        //        var rateTransactionNote = new RateTransactionNote();
        //        rateTransactionNote.RateTransaction = rateTransaction;
        //        rateTransactionNote.Note = rateDetailsVM.note;
        //        rateTransactionNote.CreatedDate = DateTime.UtcNow;
        //        rateTransactionNote.UserID = param.userId;
        //        if (rateTransaction.RateTransactionNotes == null)
        //            rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
        //        rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
        //    }
        //    #region Update Estimate Weight Rates
        //    var weightItems = rateDetailsVM.estimatedWeightsRate.GetType().GetProperties();
        //    var paramItems = param.SW.GetType().GetProperties();
        //    foreach (var property in weightItems)
        //    {
        //        int paramID = (int)paramItems.FirstOrDefault(x => x.Name == property.Name).GetValue(param.SW);
        //        //var item = rateTransaction.ItemWeights.FirstOrDefault(c => c.ItemID == paramID && c.RateTransactionID == rateDetailsVM.RateTransactionID);
        //        var item = rateTransaction.ItemWeights.FirstOrDefault(c => c.ItemID == paramID ); //for remove RateTransactionId in ItemWeight Table
        //        if (item != null)
        //        {
        //            item.EffectiveStartDate = rateDetailsVM.effectiveStartDate;
        //            item.EffectiveEndDate = rateDetailsVM.effectiveEndDate;
        //            item.StandardWeight = (decimal)property.GetValue(rateDetailsVM.estimatedWeightsRate); //rateDetailsVM.estimatedWeightsRate.AGLS;
        //        }
        //    }

        //    #endregion

        //    //update previous Rate-effective-start-date
        //    DateTime effectiveStartDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
        //    var previousRates = dbContext.ItemWeights.Where(i => param.SW.items.Contains(i.ItemID)
        //        && i.EffectiveEndDate == effectiveStartDate).ToList();
        //    previousRates.ForEach(i =>
        //    {
        //        i.EffectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
        //    });
        //}
        private void updateTIPayeeLists(ConfigurationsBoundedContext dbContext, RateDetailsVM rateDetailsVM, RateParamsDTO param)
        {
            var tiPayeeList = dbContext.PayeeLists                              
                            .Include(c => c.VendorPayees)
                            .Include(c => c.TIPayeeNotes)
                            .FirstOrDefault(c => c.ID == rateDetailsVM.RateTransactionID);

            //Update rate transaction
            tiPayeeList.EffectiveStartDate = rateDetailsVM.effectiveStartDate;         

            tiPayeeList.ModifiedDate = DateTime.UtcNow;
            tiPayeeList.ModifiedByID = param.userId;

            //Add rate transaction note
            if (!string.IsNullOrEmpty(rateDetailsVM.note))
            {
                //add note
                var tiPayeeListNote = new PayeeListNote();
                tiPayeeListNote.PayeeList = tiPayeeList;
                tiPayeeListNote.Note = rateDetailsVM.note;
                tiPayeeListNote.CreatedDate = DateTime.UtcNow;
                tiPayeeListNote.UserID = param.userId;
                if (tiPayeeList.TIPayeeNotes == null)
                {
                    tiPayeeList.TIPayeeNotes = new List<PayeeListNote>();
                }
                tiPayeeList.TIPayeeNotes.Add(tiPayeeListNote);
            }  

            //update previous payeelist effective-start-date. 
            DateTime effectiveStartDate = rateDetailsVM.previousEffectiveStartDate.AddDays(-1);
            var previousRates = dbContext.PayeeLists.Where(i => i.EffectiveEndDate == effectiveStartDate).ToList();
            previousRates.ForEach(i =>
            {
                i.EffectiveEndDate = tiPayeeList.EffectiveStartDate.AddDays(-1);
            });

            //delete old assigned haulers;
            var assignedHaulers = tiPayeeList.VendorPayees.ToList();
            if (assignedHaulers != null && assignedHaulers.Count > 0)
            {
                dbContext.VendorPayees.RemoveRange(assignedHaulers);
            }
            //add new assigned haulers. allow empty assign haulers
            if (rateDetailsVM.tiPayee != null)
            {
                tiPayeeList.VendorPayees = new List<VendorPayee>();
                var piRate = new VendorPayee();
                foreach (var item in rateDetailsVM.tiPayee.AssignedHaulerIDs)
                {
                    piRate = new VendorPayee()
                    {
                        PayeeListId = rateDetailsVM.RateTransactionID,
                        VendorId = item.ItemID
                    };
                    tiPayeeList.VendorPayees.Add(piRate);
                }
            }
        }
        #endregion

        #region //Remove Rates
        public bool RemoveRateTransaction(int rateTransactionID, string categoryName)
        {     
            switch (categoryName)
            {
                case TreadMarksConstants.ProcessingIncentiveRates:
                    removePIRateTransaction(rateTransactionID);
                    break;
                case TreadMarksConstants.TIPayee:
                    removeTIPayeeList(rateTransactionID);
                    break;
                default:
                    removeRateTransaction(rateTransactionID);
                    break;
            }  
            return true;
        }
        private void removePIRateTransaction(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var rateTransaction = dbContext.RateTransactions.Where(m => m.ID == rateTransactionID).FirstOrDefault();
            if (rateTransaction != null)
            {
                //1.remove existing data.
                var notes = rateTransaction.RateTransactionNotes.ToList();
                if ((notes != null) && (notes.Count > 0))
                {
                    dbContext.RateNotes.RemoveRange(notes);
                }

                var rates = rateTransaction.Rates.ToList();
                if ((rates != null) && (rates.Count > 0))
                {
                    dbContext.Rates.RemoveRange(rates);
                }

                var assignedProcessors = rateTransaction.PIRateMappings.ToList();
                if (assignedProcessors != null && assignedProcessors.Count > 0)
                {
                    dbContext.VendorRates.RemoveRange(assignedProcessors);
                }
                // update global rate previous rate and ratetransaction
                if (!rateTransaction.IsSpecificRate.HasValue || !rateTransaction.IsSpecificRate.Value)
                {
                    // 2.1 update previous rates
                    var previousEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);
                    var previousRates = dbContext.Rates.Where(i => i.ClaimType == rateTransaction.Category && i.EffectiveEndDate == previousEndDate).ToList();
                    previousRates.ForEach(i =>
                    {
                        i.EffectiveEndDate = DateTime.MaxValue.Date;
                    });
                    // 2.2 update previous ratetransaction
                    var previousRateTrans = dbContext.RateTransactions.Where(c => c.Category == rateTransaction.Category && (!c.IsSpecificRate.HasValue || !c.IsSpecificRate.Value))
                                .OrderByDescending(x => x.EffectiveStartDate).Skip(1).FirstOrDefault();

                    if (previousRateTrans != null)
                    {
                        previousRateTrans.EffectiveEndDate = DateTime.MaxValue.Date;
                    }
                }
                //3. remove ratetransaction
                dbContext.RateTransactions.Remove(rateTransaction);
                dbContext.SaveChanges();
            }
        }
        private void removeTIPayeeList(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var tiPayeeList = dbContext.PayeeLists.Where(m => m.ID == rateTransactionID).FirstOrDefault();
            if (tiPayeeList != null)
            {
                //1.remove existing data.
                var notes = tiPayeeList.TIPayeeNotes.ToList();
                if ((notes != null) && (notes.Count > 0))
                {
                    dbContext.PayeeListNotes.RemoveRange(notes);
                }
 
                var assignedHaulers = tiPayeeList.VendorPayees.ToList();
                if (assignedHaulers != null && assignedHaulers.Count > 0)
                {
                    dbContext.VendorPayees.RemoveRange(assignedHaulers);
                }
                // update tiPayeeList       
               
                var previousEndDate = tiPayeeList.EffectiveStartDate.AddDays(-1);
                var previousRates = dbContext.PayeeLists.Where(i => i.EffectiveEndDate == previousEndDate).ToList();
                previousRates.ForEach(i =>
                {
                    i.EffectiveEndDate = DateTime.MaxValue.Date;
                });
             
                //3. remove tiPayeeList
                dbContext.PayeeLists.Remove(tiPayeeList);
                dbContext.SaveChanges();
            }
        }
        private void removeRateTransaction(int rateTransactionID)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var rateTransaction = dbContext.RateTransactions.Where(m => m.ID == rateTransactionID).FirstOrDefault();
            if (rateTransaction != null)
            {
                //1.remove existing data.
                var notes = rateTransaction.RateTransactionNotes.ToList();
                if ((notes != null) && (notes.Count > 0))
                {
                    dbContext.RateNotes.RemoveRange(notes);
                }

                var rates = rateTransaction.Rates.ToList();
                if ((rates != null) && (rates.Count > 0))
                {
                    dbContext.Rates.RemoveRange(rates);
                }

                if (rateTransaction.Category != (int)ClaimType.Collector)//collector rate doesn't need update previous rate
                {
                    //2.Update effectiveenddate for the previous records
                    DateTime effectiveEndDate = rateTransaction.EffectiveStartDate.AddDays(-1);

                    if (rateTransaction.Category == 3)//Hauler TI rate has two claimTypes, 3 and 4
                    {
                        var previousRates = dbContext.Rates.Where(i => ((i.ClaimType == 4 && i.PaymentType == 3) || i.ClaimType == 3) && i.EffectiveEndDate == effectiveEndDate).ToList();
                        previousRates.ForEach(i =>
                        {
                            i.EffectiveEndDate = DateTime.MaxValue.Date;
                        });
                    }
                    else
                    {
                        var previousRates = dbContext.Rates.Where(i => i.ClaimType == rateTransaction.Category && i.EffectiveEndDate == effectiveEndDate).ToList();
                        if (rateTransaction.Category == 7)
                        {
                            previousRates = dbContext.Rates.Where(i => i.ClaimType == (int)ClaimType.Steward && i.PaymentType == 6 && i.EffectiveEndDate == effectiveEndDate).ToList();
                        }
                        previousRates.ForEach(i =>
                        {
                            i.EffectiveEndDate = DateTime.MaxValue.Date;
                        });
                    }
                }
                //}

                //3. remove ratetransaction
                dbContext.RateTransactions.Remove(rateTransaction);

                if (rateTransaction.Category != (int)ClaimType.Collector)//collector rate doesn't need update previous rate
                {
                    var previousRateTrans = dbContext.RateTransactions.Where(c => c.Category == rateTransaction.Category && c.EffectiveStartDate <= DateTime.Today && c.EffectiveEndDate >= DateTime.Today).FirstOrDefault();
                    if (previousRateTrans != null)
                    {
                        previousRateTrans.EffectiveEndDate = DateTime.MaxValue.Date;
                    }
                }

                dbContext.SaveChanges();
            }
        }
        public bool UnBondCollectorRatesToRateTransaction(int rateTransactionID, int category)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            RateTransaction transaction = dbContext.RateTransactions.Where(c => c.ID == rateTransactionID).First();//if not exists, throw error
            var transactionRates = dbContext.Rates.Where(c => c.RateTransactionID == rateTransactionID);

            DateTime earliestPeriodDate = transactionRates.OrderBy(i => i.period.StartDate).Min(c => c.period.StartDate);
            DateTime previousPeriodDate = earliestPeriodDate.AddDays(-1);//linq to sql cannot addDay(-1)
            Period previousPeriod = dbContext.Periods.First(c => c.PeriodType == category && (c.StartDate <= previousPeriodDate && c.EndDate >= previousPeriodDate));

            List<Rate> previousPeriodRates = dbContext.Rates.Where(c => c.PeriodID == previousPeriod.ID).ToList();
            int? transactionID = previousPeriodRates.First().RateTransactionID;
            transactionRates.ToList().ForEach(i =>
            {
                i.RateTransactionID = transactionID;
                switch (i.item.ShortName)
                {
                    case "PLT":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "PLT").FirstOrDefault().ItemRate;
                        break;
                    case "MT":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "MT").FirstOrDefault().ItemRate;
                        break;
                    case "AGLS":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "AGLS").FirstOrDefault().ItemRate;
                        break;
                    case "IND":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "IND").FirstOrDefault().ItemRate;
                        break;
                    case "SOTR":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "SOTR").FirstOrDefault().ItemRate;
                        break;
                    case "MOTR":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "MOTR").FirstOrDefault().ItemRate;
                        break;
                    case "LOTR":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "LOTR").FirstOrDefault().ItemRate;
                        break;
                    case "GOTR":
                        i.ItemRate = previousPeriodRates.Where(c => c.item.ShortName == "GOTR").FirstOrDefault().ItemRate;
                        break;
                    default:
                        break;
                }
            });
            var notes = transaction.RateTransactionNotes.ToList();
            if ((notes != null) && (notes.Count > 0))
            {
                dbContext.RateNotes.RemoveRange(notes);
            }

            dbContext.RateTransactions.Remove(transaction);

            //NOTE:update the latest previous rateTransaction
            Period latestPeriod = GetLatestPeriod(category);

            var previousTransaction = dbContext.RateTransactions.Where(x => x.Category == category).OrderByDescending(i => i.EffectiveStartDate).Skip(1).Take(1).FirstOrDefault();
            if (previousTransaction != null)
            {
                previousTransaction.EffectiveEndDate = latestPeriod.EndDate;
            }

            context.SaveChanges();
            return true;
        }
        #endregion

        #region //notes
        public RateTransactionNote AddTransactionNote(RateTransactionNote transactionNote)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            dbContext.RateNotes.Add(transactionNote);
            dbContext.SaveChanges();
            return transactionNote;
        }
        public AdminSectionNotes AddAppSettingNotes(AdminSectionNotes note)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            dbContext.AdminSectionNotes.Add(note);
            dbContext.SaveChanges();
            return note;
        }
        public void AddPayeeNote(PayeeListNote transactionNote)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            dbContext.PayeeListNotes.Add(transactionNote);
            dbContext.SaveChanges();           
        }
        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var query = from note in dbContext.RateNotes.AsNoTracking()
                        where note.RateTransactionID == parentId
                        select new InternalNoteViewModel()
                        {
                            AddedOn = note.CreatedDate,
                            Note = note.Note,
                            AddedBy = note.User.FirstName + " " + note.User.LastName,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Note.ToLower().Contains(searchText)
                                        || c.AddedBy.Contains(searchText)
                                        );
                }

            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;
            query = !sortReverse ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");
            var totalRecords = query.Count();
            //var result = query.Skip(pageIndex).Take(pageSize).ToList();
            return query.ToList();
        }
        public List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var query = from note in dbContext.AdminSectionNotes.AsNoTracking()
                        where note.Category == parentId
                        select new InternalNoteViewModel()
                        {
                            AddedOn = note.CreatedDate,
                            Note = note.Note,
                            AddedBy = note.User.FirstName + " " + note.User.LastName,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Note.ToLower().Contains(searchText)
                                        || c.AddedBy.Contains(searchText)
                                        );
                }

            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;
            query = !sortReverse ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");
            var totalRecords = query.Count();
            //var result = query.Skip(pageIndex).Take(pageSize).ToList();
            return query.ToList();
        }
        public List<InternalNoteViewModel> ExportPayeeNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var query = from note in dbContext.PayeeListNotes.AsNoTracking()
                        where note.PayeeListID == parentId
                        select new InternalNoteViewModel()
                        {
                            AddedOn = note.CreatedDate,
                            Note = note.Note,
                            AddedBy = note.User.FirstName + " " + note.User.LastName,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.AddedOn) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.AddedOn) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.AddedOn) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Note.ToLower().Contains(searchText)
                                        || c.AddedBy.Contains(searchText)
                                        );
                }

            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "AddedOn" : sortcolumn;
            query = !sortReverse ? query.OrderBy(sortcolumn) : query.OrderBy(sortcolumn + " descending");
            var totalRecords = query.Count();
            //var result = query.Skip(pageIndex).Take(pageSize).ToList();
            return query.ToList();
        }

        public Period GetPeriodByDate(DateTime effectiveStartDate, int iPeriodType)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            if (dbContext.Periods.Any(c => c.PeriodType == iPeriodType && (c.StartDate <= effectiveStartDate && c.EndDate >= effectiveStartDate)))
            {
                return dbContext.Periods.First(c => c.PeriodType == iPeriodType && (c.StartDate <= effectiveStartDate && c.EndDate >= effectiveStartDate));
            }
            else
            {
                return null;
            }
        }
        public List<Period> LoadFuturePeriodByType(DateTime effectiveStartDate, int iPeriodType)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            return dbContext.Periods.Where(c => c.PeriodType == iPeriodType && (c.StartDate >= effectiveStartDate)).OrderBy(c => c.StartDate).ToList();
        }

        #endregion

        public AppSettingsVM LoadAppSettings()
        {
            AppSettingsVM vm = new AppSettingsVM();
            int iVal = 0;
            List<string> lValues = new List<string>() {
                //"Email.BccApplicationPath",
                //"Email.StewardApplicationApproveBCCEmailAddr",
                "Invitation.ExpirationDays",
                "Application.InvitationExpiryDays",
                "Application.PurgeOpenApplicationAfterDays",

                "UserPolicy.MaxLoginAttemptBeforeLock",
                "UserPolicy.PasswordExpiresInDays",
                "UserPolicy.PasswordResetTokenExpiresInMinutes",
                "Claims.ReviewDueDays",
                "Claims.ChequeDueDays",

                "DistanceVariance",
                "Settings.UserDate",
                //"Email.CBCCEmailClaimAndApplication",
                //"Email.CBCCEmailStewardAndRemittance",
            };
            var dbContext = context as ConfigurationsBoundedContext;
            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();
            //Setting settingValue = lSettings.FirstOrDefault(i => i.Key == "Email.BccApplicationPath");//1
            //vm.Email_BccApplicationPath = (null != settingValue) ? settingValue.Value.ToString().Trim() : string.Empty;//leading trailing space cause ng-valid failure
            //settingValue = lSettings.FirstOrDefault(i => i.Key == "Email.StewardApplicationApproveBCCEmailAddr");//2
            //vm.Email_StewardApplicationApproveBCCEmailAddr = (null != settingValue) ? settingValue.Value.ToString().Trim() : string.Empty;

            Setting settingValue = lSettings.FirstOrDefault(i => i.Key == "Invitation.ExpirationDays");//3
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.Invitation_ExpirationDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Application.InvitationExpiryDays");//4
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.Application_InvitationExpiryDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Application.PurgeOpenApplicationAfterDays");//5
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.Application_PurgeOpenApplicationAfterDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "UserPolicy.MaxLoginAttemptBeforeLock");//6
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.UserPolicy_MaxLoginAttemptBeforeLock = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "UserPolicy.PasswordExpiresInDays");//7
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.UserPolicy_PasswordExpiresInDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "UserPolicy.PasswordResetTokenExpiresInMinutes");//8
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.UserPolicy_PasswordResetTokenExpiresInMinutes = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Claims.ReviewDueDays");//9
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.Claims_ReviewDueDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Claims.ChequeDueDays");//10
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.Claims_ChequeDueDays = iVal;
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "DistanceVariance");//11
            if (settingValue != null)
            {
                int.TryParse(settingValue.Value, out iVal);
            }
            vm.DistanceVariance = iVal / 1000;//m => km
            iVal = 0;

            DateTime d = new DateTime();
            settingValue = lSettings.FirstOrDefault(i => i.Key == "Settings.UserDate");//12
            if (settingValue != null)
            {
                DateTime.TryParse(settingValue.Value, out d);
            }
            vm.Settings_UserDate = d;
            iVal = 0;

            //bool b = false;
            //settingValue = lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailClaimAndApplication");//1 checkbox
            //if (settingValue != null)
            //{
            //    b = settingValue.Value.Equals("1");
            //}
            //vm.Email_CBCCEmailClaimAndApplication = b;
            //b = false;

            //settingValue = lSettings.FirstOrDefault(i => i.Key == "Email.CBCCEmailStewardAndRemittance");//2 checkbox
            //if (settingValue != null)
            //{
            //    b = settingValue.Value.Equals("1");
            //}
            //vm.Email_CBCCEmailStewardAndRemittance = b;

            return vm;
        }

        public List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            return dbContext.AdminSectionNotes.Where(c => c.Category == noteType).Select(i => new InternalNoteViewModel
            {
                AddedOn = i.CreatedDate,
                Note = i.Note,
                AddedBy = i.User.FirstName + " " + i.User.LastName
            }).OrderByDescending(i => i.AddedOn).ToList();
        }

        public bool updateAppSettings(AppSettingsVM appSettingsVM, long userId)
        {
            PropertyInfo[] properties = typeof(AppSettingsVM).GetProperties();

            using (var dbContext = context as ConfigurationsBoundedContext)
            {
                foreach (PropertyInfo property in properties)
                {
                    object[] attrs = property.GetCustomAttributes(true);
                    if ((attrs.Length > 0) && (attrs[0].ToString() == "Newtonsoft.Json.JsonIgnoreAttribute"))
                    {
                        continue;
                    }
                    var settingValInDb = dbContext.AppSettings.Where(i => i.Key == property.Name.Replace("_", ".")).SingleOrDefault();
                    if (settingValInDb != null)
                    {
                        settingValInDb.Value = (property.GetValue(appSettingsVM) != null) ? property.GetValue(appSettingsVM).ToString().Trim() : string.Empty;
                        if (property.PropertyType.FullName == "System.Boolean")
                        {
                            settingValInDb.Value = (bool)property.GetValue(appSettingsVM) ? "1" : "0";
                        }
                        if (property.PropertyType.FullName == "System.DateTime")
                        {
                            settingValInDb.Value = ((DateTime)property.GetValue(appSettingsVM)).Date.ToString("yyyy-MM-dd");
                        }
                        if (property.Name == "DistanceVariance")
                        {
                            settingValInDb.Value = property.GetValue(appSettingsVM) + "000";
                        }
                    }
                    else //if not exists, insert
                    {
                        Setting setting = new Setting { Key = property.Name, Value = property.GetValue(appSettingsVM).ToString() };
                        if (property.PropertyType.FullName == "System.Boolean")
                        {
                            setting.Value = (bool)property.GetValue(appSettingsVM) ? "1" : "0";
                        }
                        if (property.PropertyType.FullName == "System.DateTime")
                        {
                            setting.Value = ((DateTime)property.GetValue(appSettingsVM)).Date.ToString("yyyy-MM-dd");
                        }
                        if (property.Name == "DistanceVariance")
                        {
                            settingValInDb.Value = property.GetValue(appSettingsVM) + "000";
                        }
                        dbContext.AppSettings.Add(setting);
                    }
                }
                dbContext.SaveChanges();
            }
            return true;
        }

        #region //Account Thresholds
        public AccountThresholdsVM LoadAccountThresholds()
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var vm = new AccountThresholdsVM();
            vm.Items = dbContext.AppSettings.Where(i => i.Key.IndexOf(vm.applicationTypeID) > -1
            && i.Key.IndexOf("Threshold.IndividualTireCount") < 0 && i.Key.IndexOf("Threshold.TotalTireCount") < 0
            && i.Key.IndexOf("Threshold.GPPaymentTerms") < 0 && i.Key.IndexOf("Threshold.CBTotalTireCount") < 0 && i.Key.IndexOf("Threshold.CBIndividualTireCount") < 0)
                .Select(x => new ItemModel<string>
                {
                    ItemID = x.ID,
                    ItemName = x.Key,
                    ItemValue = x.Value
                });
            return vm;
        }

        public bool UpdateAccountThresholds(AccountThresholdsVM vm, long userID)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            //ensure only save loaded AccountThresholds items back to [AppSetting] table by apply Where clause
            var accountThresholds = dbContext.AppSettings.Where(i => i.Key.IndexOf(vm.applicationTypeID) > -1
            && i.Key.IndexOf("Threshold.IndividualTireCount") < 0 && i.Key.IndexOf("Threshold.TotalTireCount") < 0
            && i.Key.IndexOf("Threshold.GPPaymentTerms") < 0 && i.Key.IndexOf("Threshold.CBTotalTireCount") < 0 && i.Key.IndexOf("Threshold.CBIndividualTireCount") < 0);

            foreach (var item in vm.Items)
            {
                var temp = accountThresholds.FirstOrDefault(x => x.ID == item.ItemID);
                if (temp != null)
                {
                    temp.Value = item.ItemValue;
                }
            }

            if (!string.IsNullOrEmpty(vm.note))
            {
                var note = new AdminSectionNotes
                {
                    Category = vm.applicationTypeID,
                    Note = vm.note,
                    CreatedDate = DateTime.UtcNow,
                    UserID = userID
                };
                dbContext.AdminSectionNotes.Add(note);
            }

            dbContext.SaveChanges();
            return true;
        }
        #endregion

        #region Transaction Thresholds
        public TransactionThresholdsVM LoadTransactionThresholds()
        {
            var dbContext = context as ConfigurationsBoundedContext;
            int iVal = 0;
            List<string> lValues = new List<string>() {
                "Threshold.IndividualTireCountPLT",
                "Threshold.IndividualTireCountMT",
                "Threshold.IndividualTireCountAGLS",
                "Threshold.IndividualTireCountIND",
                "Threshold.IndividualTireCountSOTR",
                "Threshold.IndividualTireCountMOTR",
                "Threshold.IndividualTireCountLOTR",
                "Threshold.IndividualTireCountGOTR",
                "Threshold.TotalTireCountDOR",
                "Threshold.TotalTireCountDOT",
                "Threshold.TotalTireCountHIT",
                "Threshold.TotalTireCountPTR",
                "Threshold.TotalTireCountRTR",
                "Threshold.TotalTireCountSTC",
                "Threshold.TotalTireCountTCR",
                "Threshold.TotalTireCountUCR",
                "Threshold.CBIndividualTireCount",
                "Threshold.CBTotalTireCount",
                "Threshold.CBAutoAppDOT",
                "Threshold.CBAutoAppHIT",
                "Threshold.CBAutoAppPIT",
                "Threshold.CBAutoAppPTR",
                "Threshold.CBAutoAppSTC",
                "Threshold.CBAutoAppTCR",
                "Threshold.CBAutoAppUCR"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();
            TransactionThresholdsVM transactionThresholds = new TransactionThresholdsVM();

            Setting settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountPLT");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountPLT = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountPLT = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountMT");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountMT = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountMT = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountAGLS");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountAGLS = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountAGLS = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountIND");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountIND = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountIND = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountSOTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountSOTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountSOTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountMOTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountMOTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountMOTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountLOTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountLOTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountLOTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountGOTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_IndividualTireCountGOTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_IndividualTireCountGOTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountDOR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountDOR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountDOR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountDOT");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountDOT = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountDOT = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountHIT");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountHIT = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountHIT = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountPTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountPTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountPTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountRTR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountRTR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountRTR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountSTC");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountSTC = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountSTC = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountTCR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountTCR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountTCR = null;
                }
            }
            iVal = 0;

            settingValue = lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountUCR");
            if (settingValue != null)
            {
                if (int.TryParse(settingValue.Value, out iVal))
                {
                    transactionThresholds.Threshold_TotalTireCountUCR = iVal;
                }
                else
                {
                    transactionThresholds.Threshold_TotalTireCountUCR = null;
                }
            }
            iVal = 0;

            transactionThresholds.Threshold_CBIndividualTireCount = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBIndividualTireCount") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBIndividualTireCount").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBTotalTireCount = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBTotalTireCount") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBTotalTireCount").Value.Equals("1") : false;

            transactionThresholds.Threshold_CBAutoAppDOT = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppDOT") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppDOT").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppHIT = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppHIT") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppHIT").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppPIT = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPIT") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPIT").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppPTR = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPTR") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPTR").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppSTC = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppSTC") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppSTC").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppTCR = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppTCR") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppTCR").Value.Equals("1") : false;
            transactionThresholds.Threshold_CBAutoAppUCR = lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppUCR") != null ? lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppUCR").Value.Equals("1") : false;

            return transactionThresholds;
        }

        public void UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            var dbContext = context as ConfigurationsBoundedContext;
            List<string> lValues = new List<string>() {
                "Threshold.IndividualTireCountPLT",
                "Threshold.IndividualTireCountMT",
                "Threshold.IndividualTireCountAGLS",
                "Threshold.IndividualTireCountIND",
                "Threshold.IndividualTireCountSOTR",
                "Threshold.IndividualTireCountMOTR",
                "Threshold.IndividualTireCountLOTR",
                "Threshold.IndividualTireCountGOTR",
                "Threshold.TotalTireCountDOR",
                "Threshold.TotalTireCountDOT",
                "Threshold.TotalTireCountHIT",
                "Threshold.TotalTireCountPTR",
                "Threshold.TotalTireCountRTR",
                "Threshold.TotalTireCountSTC",
                "Threshold.TotalTireCountTCR",
                "Threshold.TotalTireCountUCR",
                "Threshold.CBIndividualTireCount",
                "Threshold.CBTotalTireCount",
                "Threshold.CBAutoAppDOT",
                "Threshold.CBAutoAppHIT",
                "Threshold.CBAutoAppPIT",
                "Threshold.CBAutoAppPTR",
                "Threshold.CBAutoAppSTC",
                "Threshold.CBAutoAppTCR",
                "Threshold.CBAutoAppUCR"
            };

            List<Setting> lSettings = dbContext.AppSettings.Where(i => lValues.Contains(i.Key)).ToList();

            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountPLT").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountPLT);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountMT").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountMT);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountAGLS").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountAGLS);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountIND").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountIND);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountSOTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountSOTR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountMOTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountMOTR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountLOTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountLOTR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.IndividualTireCountGOTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_IndividualTireCountGOTR);

            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountDOR").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountDOR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountDOT").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountDOT);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountHIT").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountHIT);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountPTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountPTR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountRTR").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountRTR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountSTC").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountSTC);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountTCR").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountTCR);
            lSettings.FirstOrDefault(i => i.Key == "Threshold.TotalTireCountUCR").Value = Convert.ToString(transactionThresholdsVM.Threshold_TotalTireCountUCR);

            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBIndividualTireCount").Value = transactionThresholdsVM.Threshold_CBIndividualTireCount ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBTotalTireCount").Value = transactionThresholdsVM.Threshold_CBTotalTireCount ? "1" : "0";

            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppDOT").Value = transactionThresholdsVM.Threshold_CBAutoAppDOT ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppHIT").Value = transactionThresholdsVM.Threshold_CBAutoAppHIT ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPIT").Value = transactionThresholdsVM.Threshold_CBAutoAppPIT ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppPTR").Value = transactionThresholdsVM.Threshold_CBAutoAppPTR ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppSTC").Value = transactionThresholdsVM.Threshold_CBAutoAppSTC ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppTCR").Value = transactionThresholdsVM.Threshold_CBAutoAppTCR ? "1" : "0";
            lSettings.FirstOrDefault(i => i.Key == "Threshold.CBAutoAppUCR").Value = transactionThresholdsVM.Threshold_CBAutoAppUCR ? "1" : "0";

            dbContext.SaveChanges();
        }
        #endregion

        #region TCR Service Threshold
        public PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            //var vendorID = 0;
            var query = dbContext.Sp_GetAllServiceThresholds().Where(x=>!x.IsDeleted.HasValue || !x.IsDeleted.Value).ToList().AsQueryable();
            if (paramDTO.searchText == "true")
            {
                query = query.Where(x => x.DaysLeft == 0);
            }
            paramDTO.orderBy = string.IsNullOrWhiteSpace(paramDTO.orderBy) ? "RegNumber" : paramDTO.orderBy;

            if (paramDTO.sortDirection == "asc")
            {
                if (paramDTO.orderBy=="DaysOver")
                {
                    query = query.OrderBy(x => x.DaysLeft==0 ? (x.DaysSinceLastServiced - x.ThresholdDays) : 0);
                } else
                {
                    query = query.OrderBy(paramDTO.orderBy);
                }
               
            }
            else
            {
                if (paramDTO.orderBy == "DaysOver")
                {
                    query = query.OrderByDescending(x => x.DaysLeft == 0 ? (x.DaysSinceLastServiced - x.ThresholdDays) : 0);
                }
                else
                {
                    query = query.OrderBy(paramDTO.orderBy + " descending");
                }
            }
            var totalRecords = query.Count();
           
            return new PaginationDTO<TCRListViewModel, int>
            {
                DTOCollection = query.ToList(),
                TotalRecords = totalRecords,
                PageNumber = paramDTO.pageIndex + 1,
                PageSize = paramDTO.pageSize,
            };
        }

        public TCRServiceThresholdListViewModel LoadTCRServiceThresholdList()
        {
            var dbContext = context as ConfigurationsBoundedContext;

            //var vendorID = 0;
            var result = dbContext.Sp_GetAllServiceThresholds().Where(x => !x.IsDeleted.HasValue || !x.IsDeleted.Value).OrderByDescending(c => c.ID).ToList();
            var query = new TCRServiceThresholdListViewModel();   
            query.tcrServiceThresholds = result;
            query.reachedThresholds = result.Where(x=>x.DaysLeft==0).Count();
            query.withThresholds = result.Count(); 
            var withThresholdIDs = result.Select(v => v.VendorID);
            query.withoutThresholdVendors = dbContext.Vendors.Where(x => x.VendorType == (int)ClaimType.Collector &&
                                            x.IsActive && 
                                            !withThresholdIDs.Contains(x.ID))                                           
                                            .Select(c=>new WithoutThesholdVendor() {
                                                ID=0,
                                                BusinessName=c.BusinessName,
                                                RegNumber=c.Number,
                                                VendorID=c.ID,
                                                ThresholdDays=0,
                                                Description=string.Empty,
                                                IsActive=c.IsActive
                                            }).ToList();
            query.withoutThresholds = query.withoutThresholdVendors.Count();

            //add withThresholdVendors           
            foreach (var item in query.tcrServiceThresholds)
            {
                var temp = new WithoutThesholdVendor()
                {
                    ID = item.ID,
                    BusinessName = item.BusinessName,
                    RegNumber = item.RegNumber,
                    VendorID = item.VendorID,
                    ThresholdDays = item.ThresholdDays,
                    Description = item.Description,
                    IsActive=true
                };
                query.withoutThresholdVendors.Add(temp);
            }
            // add Inactive collectors
            var inactiveCollectors= dbContext.Vendors.Where(x => x.VendorType == (int)ClaimType.Collector && !x.IsActive)
                 .Select(c => new WithoutThesholdVendor()
                 {
                     ID = 0,
                     BusinessName = c.BusinessName,
                     RegNumber = c.Number,
                     VendorID = c.ID,
                     ThresholdDays = 0,
                     Description = string.Empty,
                     IsActive = c.IsActive
                 }).ToList();
            query.withoutThresholdVendors.AddRange(inactiveCollectors);
            query.tcrServiceThresholds = null;
            return query;
        }  

        public void AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {   
            //check exist TCRServiceThreshold;   
            var dbContext = context as ConfigurationsBoundedContext; 
            if (vm.ID == 0)
            {
                var exist = dbContext.VendorServiceThresholds.Where(x => x.VendorID == vm.VendorID && (!x.IsDeleted.HasValue || !x.IsDeleted.Value)).FirstOrDefault();
                if (exist !=null)
                {
                    vm.ID = exist.ID;
                }
            }

            if (vm.ID > 0)
            {
                UpdateTCRServiceThreshold(vm, userId);
            }
            else
            {
                var tcrServiceThreshold = new VendorServiceThreshold()
                {
                    ThresholdDays = vm.ThresholdDays,
                    IsDeleted = false,
                    VendorID = vm.VendorID,
                    CreatedDate = DateTime.UtcNow,
                    CreatedByID = userId,
                    Description = vm.Description ?? "added"
                };
                dbContext.VendorServiceThresholds.Add(tcrServiceThreshold);
                dbContext.SaveChanges();
            }
        }

        public bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var tmpTCR = dbContext.VendorServiceThresholds.FirstOrDefault(c => c.ID == vm.ID);

            if (tmpTCR != null)
            {                     
                tmpTCR.ThresholdDays = vm.ThresholdDays;
                tmpTCR.Description = vm.Description;
                tmpTCR.ModifiedDate = DateTime.UtcNow;
                tmpTCR.ModifiedByID = userId;
                dbContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RemoveTCRServiceThresohold(int id, long userId)
        {
            var dbContext = context as ConfigurationsBoundedContext;

            var tmpTCR = dbContext.VendorServiceThresholds.FirstOrDefault(c => c.ID == id);

            if (tmpTCR != null)
            {                
                tmpTCR.IsDeleted = true;
                tmpTCR.ModifiedDate = DateTime.UtcNow;
                tmpTCR.ModifiedByID = userId;

                dbContext.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
        public IEnumerable<TCRListViewModel> ExportTCRServiceThresholds()
        {
            var dbContext = context as ConfigurationsBoundedContext;
            var query=dbContext.Sp_GetAllServiceThresholds().ToList();
            return query;
        }
        #endregion
    }
}
