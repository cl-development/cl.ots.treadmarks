﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Security.Interfaces;

namespace CL.TMS.Security.Implementations
{
    public static class PBKDF2PasswordHasher
    {

        /// <summary>
        /// Hash the specified password using PBKDF2-SHA256 Hash
        /// </summary>
        /// <param name="password">Password</param>
        /// <returns>The Salted PBKDF2-SHA256 Hash</returns>
        public static string Create(string password)
        {
            return PBKDF2HashHandler.CreateSaltedHashedPassword(password);
        }

        /// <summary>
        /// Compare a password to it's hashed equilevent
        /// </summary>
        /// <param name="password">The password to verify.</param>
        /// <param name="correctHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public static bool Compare(string password, string correctHash)
        {
            return PBKDF2HashHandler.ValidateSaltedHashedPassword(password, correctHash);
        }
    }
}
