﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Internal;
using CL.Framework.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.Security.Interfaces;
using CL.TMS.DataContracts.ViewModel.System;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.Security.Implementations
{
    public static class ClaimsIdentityFactory 
    {
        internal const string IdentityProviderClaimType = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
        internal const string DefaultIdentityProviderClaimValue = "ASP.NET Identity";
        internal const string DefaultXMLSchema = "http://www.w3.org/2001/XMLSchema#string";

        /// <summary>
        /// Create a ClaimsIdentity from a authenticationUser
        /// 
        /// </summary>
        /// <param name="manager"/><param name="authenticationUser"/><param name="authenticationType"/>
        /// <returns/>
        public static ClaimsIdentity Create(AuthenticationUser authenticationUser, string authenticationType, IEnumerable<string> roles, IEnumerable<Claim> userClaims)
        {
            ConditionCheck.Null(authenticationUser, "authenticationUser");

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(authenticationType, ClaimTypes.Name, ClaimTypes.Role);
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, authenticationUser.UserName));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, authenticationUser.UserName, DefaultXMLSchema));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Sid, authenticationUser.ID.ToString()));
            claimsIdentity.AddClaim(new Claim(IdentityProviderClaimType, DefaultIdentityProviderClaimValue, DefaultXMLSchema));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.GivenName, authenticationUser.FirstName));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Surname, authenticationUser.LastName));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Email, authenticationUser.Email));
            claimsIdentity.AddClaim(new Claim("RedirectUserAfterLoginUrl", authenticationUser.RedirectUserAfterLoginUrl ?? string.Empty));

            foreach (string role in roles)
            {
                claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role, DefaultXMLSchema));
            }

            claimsIdentity.AddClaims(userClaims);

            return claimsIdentity;
        }
    }
}
