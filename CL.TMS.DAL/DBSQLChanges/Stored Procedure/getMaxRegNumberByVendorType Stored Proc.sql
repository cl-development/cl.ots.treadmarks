USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_getMaxRegNumByVendorType]    Script Date: 9/2/2015 11:25:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[sp_getMaxRegNumByVendorType]
	@vendorTypeID int, @maxNum int OUTPUT
AS
SET NOCOUNT ON
BEGIN
	UPDATE VendorService
	SET MaxNumber = (
		SELECT MAX(CAST (NUMBER AS INT)) as MaxNumber
		FROM VENDOR 
		WHERE VendorTypeID = @vendorTypeID
		GROUP BY VendorTypeID) + 1
	WHERE VendorTypeID = @vendorTypeID
	SET @maxNum = (SELECT MaxNumber FROM VendorService WHERE VendorTypeID = @vendorTypeID) 
	RETURN 
END

SET NOCOUNT OFF






GO