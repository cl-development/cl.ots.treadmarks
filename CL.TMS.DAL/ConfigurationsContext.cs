﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.DAL
{
    public class ConfigurationsBoundedContext : TMBaseContext<ConfigurationsBoundedContext>
    {
        public ConfigurationsBoundedContext()
        {

        }

        public DbSet<Rate> Rates { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<RateTransactionNote> RateNotes { get; set; }
        public DbSet<RateTransaction> RateTransactions { get; set; }
        public DbSet<ItemWeight> ItemWeights { get; set; }
        public DbSet<Setting> AppSettings { get; set; }
        public DbSet<AdminSectionNotes> AdminSectionNotes { get; set; }

        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorRate> VendorRates { get; set; }
        public DbSet<VendorPayee> VendorPayees { get; set; }
        public DbSet<PayeeList> PayeeLists { get; set; }
        public DbSet<PayeeListNote> PayeeListNotes { get; set; }
        public DbSet<VendorServiceThreshold> VendorServiceThresholds { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Rates
            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);
            modelBuilder.Entity<ItemWeight>().Property(c => c.StandardWeight).HasPrecision(18, 4);
            //modelBuilder.Ignore<Item>();

            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<UserClaimsWorkflow>();
            modelBuilder.Ignore<UserApplicationsWorkflow>();
            modelBuilder.Ignore<Claim>();
            modelBuilder.Ignore<ClaimPaymentSummary>();
        }
        //this method using at RegistrantBoundedContext too.
        public virtual ObjectResult<TCRListViewModel> Sp_GetAllServiceThresholds()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TCRListViewModel>("sp_GetAllServiceThresholds");
        }
    }
}
