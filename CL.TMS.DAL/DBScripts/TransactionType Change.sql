//add TransationTypeCode to transaction and eligibility table
//Add it to transaction Type
//Update relationships

update [Transaction]
set [Transaction].TransactionTypeCode = TransactionType.Code
from [Transaction]
    inner join [TransactionType] on
        [Transaction].TransactionTypeId=TransactionType.ID;