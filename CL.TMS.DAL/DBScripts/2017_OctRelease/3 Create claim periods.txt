USE [tmdb]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2018
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2019
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2020
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2021
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2022
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2023
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2024
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2025
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2026
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod]
		@claimYear = 2027


GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2018
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2019
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2020
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2021
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2022
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2023
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2024
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2025
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2026
EXEC	@return_value = [dbo].[sp_InitializeClaimPeriod_Collector]
		@claimYear = 2027


GO



