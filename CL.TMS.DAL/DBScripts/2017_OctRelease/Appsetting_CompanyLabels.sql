IF NOT EXISTS (SELECT * FROM AppSetting WHERE [Key] = 'Company.Name') --use first entry as flag
BEGIN
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Name', 'OTS');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.AddressLine1', '300 The East Mall');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.AddressLine2', 'Suite 100');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.City', 'Toronto');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Province', 'Ontario');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.PostalCode', 'M9B 6B7');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Country', 'Canada');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Phone', '1-888-687-2202');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.PhoneExt', 'Company.PhoneExt');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Email', 'info@rethinktires.ca');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.WebsiteURL', 'http://rethinktires.ca');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.FacebookURL', 'https://www.facebook.com/RethinkTires/');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.TwitterURL', 'https://twitter.com/rethinktires?lang=en');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.Fax', '1-866-884-7372');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Company.LogoURL', 'CompanyLogo\OTS_Email_Logo.jpg');

INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.RemittanceVarianceAlert', '30');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CollectorAutoApproveAmount', '1000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CollectorApprover1ReqdAmount', '4000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CollectorApprover2ReqdAmount', '5000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.HaulerApprover2ReqdAmount', '75000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.ProcessorApprover2ReqdAmount', '75000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.RPMApprover2ReqdAmount', '75000');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.GPPaymentTerms', '35');

-- threshold checkboxes
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBRemittanceNilAutoApprove', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBRemittanceVarianceAlert', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBCollectorAutoApprove', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBCollectorApprover1Reqd', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBCollectorApprover2Reqd', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBHaulerApprover2Reqd', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBProcessorApprover2Reqd', '1');
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Threshold.CBRPMApprover2Reqd', '1');

END
-- request account email template
INSERT INTO "AppSetting" ("Key", "Value") VALUES ('Email.RequestAccountEmailTempl', 'Templates\RequestAccountEmailTempl.html');
Insert into AppSetting ([Key],[Value]) values ('Application.PurgeOpenApplicationAfterDays','8');

INSERT INTO [AppSetting] ([Key], [Value]) VALUES ('Email.CBCCEmailClaimAndApplication', '1');
INSERT INTO [AppSetting] ([Key], [Value]) VALUES ('Email.CBCCEmailStewardAndRemittance', '1');
go