﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-938 for add rate categories to TypeDefinition
Purpose     :   add/update rate categories to TypeDefinition			
Created By  :   Joe Zhou Aug 14th, 2017
*****************************************************************/

USE [tmdb]
GO

IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='RateCategory' and Name='Tire Stewardship Fees Rates')--if first line not exists, then insert whole batch
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,								[Code] ,							[Description] ,								[Category] ,[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Tire Stewardship Fees Rates',			'TireStewardshipFeesRates',			'Tire Stewardship Fees Rates',				'RateCategory',				1 ,				1),
		('Remittance Penalty Rates',			'RemittancePenaltyRates',			'Remittance Penalty Rates',					'RateCategory',				7 ,				2),
		('Collection Allowances Rates',			'CollectionAllowancesRates',		'Collection Allowances Rates',				'RateCategory',				2 ,				3),
		('Transportation Incentives Rates',		'TransportationIncentivesRates',	'Transportation Incentives Rates',			'RateCategory',				3 ,				4),		
		('Processing Incentives Rates',			'ProcessingIncentivesRates',		'Processing Incentives Rates',				'RateCategory',				4 ,				5),
		('Manufacturing Incentives Rates',		'ManufacturingIncentivesRates',		'Manufacturing Incentives Rates',			'RateCategory',				5 ,				6),
		('Estimated Weights Rates',				'EstimatedWeightsRates',			'Estimated Weights Rates',					'RateCategory',				6 ,				7)
END
GO

--modify column to int
IF NOT EXISTS (select DATA_TYPE from information_schema.columns where TABLE_NAME='RateTransaction' and COLUMN_NAME='Category' and DATA_TYPE = 'int')
BEGIN
	update RateTransaction set Category='3' where Category='Transportation Incentive Rates'
	ALTER TABLE RateTransaction ALTER COLUMN Category int NOT NULL
END

update RateTransaction set CreatedByID =  (select ID from [User] where Email = 'test@test.com')   where CreatedByID is null
ALTER TABLE RateTransaction ALTER COLUMN CreatedByID bigint NOT NULL 
go

--user access permission control
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Admin/Rates And Weights' and Name = 'Admin/Rates And Weights' and DisplayOrder = 230000)
BEGIN
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()
update AppResource set Name = 'Admin/Rates And Weights', ResourceMenu = 'Admin/Rates And Weights', ResourceScreen = 'Admin/Rates And Weights' where ResourceMenu='Admin/Rates' and ResourceScreen = 'Admin/Rates' and Name = 'Admin/Rates' and DisplayOrder = 230000

insert into [AppResource]	(Name,						ResourceMenu,			ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES			
				('Rates And Weights',				'Admin/Rates And Weights',	'Rates And Weights',		1,				231000,			@CreateBy,	@dt,		@dt),
				('Tire Stewardship Fees',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231010,			@CreateBy,	@dt,		@dt),
				('Remittance Penalty',				'Admin/Rates And Weights',	'Rates And Weights',		2,				231011,			@CreateBy,	@dt,		@dt),
				('Collection Allowances',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231012,			@CreateBy,	@dt,		@dt),
				('Transportation Incentives',		'Admin/Rates And Weights',	'Rates And Weights',		2,				231013,			@CreateBy,	@dt,		@dt),
				('Processing Incentives',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231014,			@CreateBy,	@dt,		@dt),
				('Manufacturing Incentives',		'Admin/Rates And Weights',	'Rates And Weights',		2,				231015,			@CreateBy,	@dt,		@dt),
				('Estimated Weights',				'Admin/Rates And Weights',	'Rates And Weights',		2,				231016,			@CreateBy,	@dt,		@dt)
END
go
-- Add RemittnacePenalty to RatePaymentType
if NOT EXISTS (select * from TypeDefinition where Category = 'RatePaymentType' and Code = 'RemittancePenalty')
BEGIN
INSERT INTO TypeDefinition 
		([Name] ,								[Code] ,				[Description] ,					[Category] ,	[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Remittance Penalty',			'RemittancePenalty',			'Remittance Penalty',			'RatePaymentType',	6,				6)
END
go

IF COL_LENGTH('[ItemWeight]', 'RateTransactionID') IS NULL
BEGIN
    ALTER TABLE [ItemWeight]
    ADD [RateTransactionID] int null DEFAULT 0;
    ALTER TABLE [ItemWeight] WITH CHECK ADD  CONSTRAINT [FK_ItemWeight_RateTransaction] FOREIGN KEY([RateTransactionID])
	REFERENCES [dbo].[RateTransaction] ([ID])
END
go

Update TypeDefinition set Code='ProcessorSPS' where Code='ProcessoSPS' and Category = 'RatePaymentType' 
go

