﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

--add new appResource, "Sub-Collector Toggle"
if NOT EXISTS (select * from AppResource where Name = 'Sub-Collector Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application')
BEGIN

update [AppResource] set DisplayOrder = 131213 where Name = 'Submission Information' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131214 where Name = 'Internal Notes' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131215 where Name = 'Business Location' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131216 where Name = 'Contact Information' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131217 where Name = 'Tire Details' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131218 where Name = 'Collector Details' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131219 where Name = 'Banking Information' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131220 where Name = 'Supporting Documents' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131221 where Name = 'Terms And Conditions' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'
update [AppResource] set DisplayOrder = 131222 where Name = 'Re-send Welcome Letter' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'

insert into [AppResource] 
 (Name,      ResourceMenu,     ResourceScreen,    ResourceLevel, DisplayOrder, CreateBy, CreateDate, UpdateDate)
 VALUES   
 ('Sub-Collector Toggle', 'Applications',     'Collector Application', 2,    131212,   @CreateBy, @dt,  @dt) 
END
go


--add new appResource, "Sub-Collector Toggle"
Declare @newResourceID int, @iCounter int, @RoleId int, @parentPermission int, @maxChildPermission int, @insertPermission int
DECLARE @getRoleIds CURSOR
SET @getRoleIds = CURSOR FOR select ID from [Role]
select @newResourceID = Id from AppResource where Name = 'Sub-Collector Toggle' and ResourceMenu = 'Applications' and ResourceScreen = 'Collector Application'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
 select @parentPermission = per.Id from [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
 WHERE res.Name = 'Collector Application' and res.ResourceMenu = 'Applications' and res.ResourceScreen = 'Collector Application' and rp.RoleId = @RoleId
 if (@parentPermission <> 3)
  begin
	set @insertPermission = @parentPermission;
  end 
 else --parent is Custom
  begin
   select @maxChildPermission = Max(per.Id) from [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
   WHERE res.DisplayOrder like '1312%' and res.DisplayOrder <> 131200 and rp.RoleId = @RoleId
   set @insertPermission = @maxChildPermission;
  end

   IF not EXISTS (SELECT rp.Id FROM [RolePermission] rp WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
	BEGIN
		insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES (@RoleId, @newResourceID, @insertPermission)
		--select  @RoleId, @newResourceID,  @iCounter[counter], @insertPermission[use parent]
	END
 set @iCounter = @iCounter + 1;
 FETCH NEXT
 FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go


--add two columns for sub-collector in Vendor Table 
IF COL_LENGTH('[Vendor]', 'IsSubCollector') IS NULL
BEGIN
    ALTER TABLE [Vendor]
    ADD [IsSubCollector] bit null
END
go

IF COL_LENGTH('[Vendor]', 'SubCollectorDate') IS NULL
BEGIN
    ALTER TABLE [Vendor]
    ADD [SubCollectorDate] datetime null
END
go


