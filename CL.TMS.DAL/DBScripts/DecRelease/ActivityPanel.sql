﻿/****** Object:  Table [dbo].[Activity]    Script Date: 14/11/2016 11:40:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[Activity]',N'U') IS NOT NULL
	DROP TABLE [dbo].[Activity];
GO

IF OBJECT_ID(N'[dbo].[ActivityLevel]',N'U') IS NOT NULL
	DROP TABLE [dbo].[ActivityLevel];
GO

CREATE TABLE [dbo].[Activity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](1024) NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[Initiator] [nvarchar](50) NOT NULL,
	[InitiatorName] [nvarchar](100) NOT NULL,
	[Assignee] [nvarchar](50) NOT NULL,
	[AssigneeName] [nvarchar](100) NOT NULL,
	[ActivityType] [int] NOT NULL,
	[ActivityArea] [nvarchar](30) NOT NULL,
	[ObjectId] [int] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO