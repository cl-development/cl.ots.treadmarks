/*
-- NEW TABLES
GpiBatch
GpiBatchEntry
GpiAccount (and data)
GpFiscalYear (and data)
GpChequeBook (and data)

RrOtsPmVendor
RrOtsRmCustomer
RrOtsPmTransaction
RrOtsRmTransDistribution
RrOtsRmCashReceipt
RrOtsRmApply
RrOtsPmTransaction
RrOtsPmTransDistribution

-- Modified Tables
Customer
                GpUpdate
                InBatch
Vendor
                GpUpdate
                InBatch 
TSFClaim
                InBatch
                
-- Data to be added
insert into [tmdb].[dbo].[AppSetting]([Key], Value)
values ('GP:INTERID','TEST1'); 
-- should be OTS for live
-- Fri 4/8/2016 9:27 AM
*/

-- NEW TABLES
/*
GpiBatch
GpiBatchEntry
RrOtsPmVendor
RrOtsRmCustomer
RrOtsPmTransaction
RrOtsOmTransDistribution


-- Modified Tables
Customer
	GpUpdate
	InBatch
Vendor
	GpUpdate
	InBatch 
	

*/
-- Data to be added
insert into [tmdb].[dbo].[AppSetting]([Key], Value)
values ('GP:INTERID','TEST1');
--Apr 04

--------!!!!!!!!!! Set the default value for any new records to be false and GpUpdate to be false. !!!!!!!!!!--------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[TestTable]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[TestTable]


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpiBatch]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].GpiBatch(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__GpiBatch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[GpiBatchEntry]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].GpiBatchEntry(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__GpiBatchEntry] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsPmVendor]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].RrOtsPmVendor(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__RrOtsPmVendor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsRmCustomer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].RrOtsRmCustomer(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__RrOtsRmCustomer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsPmTransaction]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].RrOtsPmTransaction(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__RrOtsPmTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N'[dbo].[RrOtsOmTransDistribution]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
CREATE TABLE [dbo].RrOtsOmTransDistribution(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK__RrOtsOmTransDistribution] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--ALTER TABLE Vendor ALTER COLUMN InBatch bit NULL

--ALTER TABLE Customer ALTER COLUMN InBatch bit NULL
