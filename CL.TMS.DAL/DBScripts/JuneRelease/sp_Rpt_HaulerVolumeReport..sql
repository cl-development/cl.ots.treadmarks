USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_HaulerVolumeReport]    Script Date: 6/24/2016 4:30:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_HaulerVolumeReport] 
	@startDate DateTime, @endDate DateTime
AS
BEGIN

	select
	main.ReportingPeriod, main.HaulerRegistrationNumber, 
	main.HaulerRegistrationNumber, 
	main.HaulerStatus, 
	main.DestinationType, 
	main.ProcessorHaulerRegistrationNumber, 
	main.[Status], 
	main.RecordState, 
	main.UsedTireDestination as 'UsedTireDestionation',
	sum(main.TotalActualWeight) as TotalActualWeight,
	sum(main.TotalEstWeight) as TotalEstWeight,
	sum(main.PltEstWeight) as 'PltEstWeight',
	sum(main.MtEstWeight) as 'MtEstWeight',
	sum(main.AglsEstWeight) as 'AglsEstWeight',
	sum(main.IndEstWeight) as 'IndEstWeight',
	sum(main.SotrEstWeight) as 'SotrEstWeight',	
	sum(main.MotrEstWeight) as 'MotrEstWeight',
	sum(main.LotrEstWeight) as 'LotrEstWeight',
	sum(main.GotrEstWeight) as 'GotrEstWeight',
	ROUND(sum(main.TotalPaymentEarned),2) as 'TotalPaymentEarned',
	sum(main.PltPaymentEarned) as 'PltPaymentEarned',
	sum(main.MtPaymentEarned) as 'MtPaymentEarned',
	sum(main.AglsPaymentEarned) as 'AglsPaymentEarned',
	sum(main.IndPaymentEarned) as 'IndPaymentEarned',
	sum(main.SotrPaymentEarned) as 'SotrPaymentEarned',
	sum(main.MotrPaymentEarned) as 'MotrPaymentEarned',
	sum(main.LotrPaymentEarned) as 'LotrPaymentEarned',
	sum(main.GotrPaymentEarned) as 'GotrPaymentEarned',
	sum(main.Hst) as 'Hst',
	ROUND(sum(main.TotalPaymentEarnedWithHst),2) as 'TotalPaymentEarnedWithHst'
	from
	(

		select 
		p.ShortName as 'ReportingPeriod',
		v.Number as 'HaulerRegistrationNumber',
		v.IsActive as 'HaulerStatus',
		coalesce((select t.TransactionTypeCode from tmdb.dbo.[Transaction] t where t.ID = cd.TransactionId), '') as 'DestinationType',
		(select [Number] from tmdb.dbo.Vendor v where v.ID = t.IncomingId  ) as 'ProcessorHaulerRegistrationNumber',
		(select [IsActive] from tmdb.dbo.Vendor v where v.ID = t.IncomingId   )  as 'Status',
		c.[Status] as 'RecordState',	
		coalesce((select t.MarketLocation from tmdb.dbo.[Transaction] tt where tt.ID = cd.TransactionId), '') as 'UsedTireDestination',		
		(coalesce(transactionActualWeight.PLT,0) + coalesce(transactionActualWeight.MT,0) + coalesce(transactionActualWeight.AGLS,0) + coalesce(transactionActualWeight.IND,0) + coalesce(transactionActualWeight.SOTR,0) + coalesce(transactionActualWeight.MOTR,0) + coalesce(transactionActualWeight.LOTR,0) + coalesce(transactionActualWeight.GOTR,0)) as 'TotalActualWeight',
		(coalesce(transactionAvgWeight.PLT,0) + coalesce(transactionAvgWeight.MT,0) + coalesce(transactionAvgWeight.AGLS,0) + coalesce(transactionAvgWeight.IND,0) + coalesce(transactionAvgWeight.SOTR,0) + coalesce(transactionAvgWeight.MOTR,0) + coalesce(transactionAvgWeight.LOTR,0) + coalesce(transactionAvgWeight.GOTR,0)) as 'TotalEstWeight',
		coalesce(transactionAvgWeight.PLT,0) as 'PltEstWeight',
		coalesce(transactionAvgWeight.MT,0) as 'MtEstWeight',
		coalesce(transactionAvgWeight.AGLS,0) as 'AglsEstWeight',
		coalesce(transactionAvgWeight.IND,0) as 'IndEstWeight',
		coalesce(transactionAvgWeight.SOTR,0) as 'SotrEstWeight',	
		coalesce(transactionAvgWeight.MOTR,0) as 'MotrEstWeight',
		coalesce(transactionAvgWeight.LOTR,0) as 'LotrEstWeight',
		coalesce(transactionAvgWeight.GOTR,0) as 'GotrEstWeight',
		(coalesce(transactionAmount.PLT,0)+ coalesce(transactionAmount.MT,0) + coalesce(transactionAmount.AGLS,0) + coalesce(transactionAmount.IND,0) + coalesce(transactionAmount.SOTR,0) + coalesce(transactionAmount.MOTR,0) + coalesce(transactionAmount.LOTR,0) + coalesce(transactionAmount.GOTR,0)) as 'TotalPaymentEarned',
		coalesce(transactionAmount.PLT, 0) as 'PltPaymentEarned',
		coalesce(transactionAmount.MT, 0) as 'MtPaymentEarned',
		coalesce(transactionAmount.AGLS, 0) as 'AglsPaymentEarned',
		coalesce(transactionAmount.IND, 0) as 'IndPaymentEarned',
		coalesce(transactionAmount.SOTR, 0) as 'SotrPaymentEarned',
		coalesce(transactionAmount.MOTR, 0) as 'MotrPaymentEarned',
		coalesce(transactionAmount.LOTR, 0) as 'LotrPaymentEarned',
		coalesce(transactionAmount.GOTR, 0) as 'GotrPaymentEarned',
		0.0 as 'Hst',
		(coalesce(transactionAmount.PLT,0)+ coalesce(transactionAmount.MT,0) + coalesce(transactionAmount.AGLS,0) + coalesce(transactionAmount.IND,0) + coalesce(transactionAmount.SOTR,0) + coalesce(transactionAmount.MOTR,0) + coalesce(transactionAmount.LOTR,0) + coalesce(transactionAmount.GOTR,0)) as 'TotalPaymentEarnedWithHst'
		from 
		Claim c inner join
		[Period] p on p.ID = c.ClaimPeriodID inner join 
		[ClaimDetail] cd on c.id = cd.claimid inner join
		[transaction] t on t.id = cd.TransactionId inner join
		[Vendor] v on v.ID = c.ParticipantID inner join 
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select cpd.* --  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber
					from tmdb.dbo.ClaimPaymentDetail cpd 	
				) src
				PIVOT
				(
					sum([AverageWeight])
					for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName
		) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID left join 
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName,
				[PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber
					from tmdb.dbo.ClaimPaymentDetail cpd 	
				) src
				PIVOT
				(
					sum(ActualWeight)
					for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName
		) as transactionActualWeight on transactionActualWeight.TransactionId = t.ID left join 
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName,
				[PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber
					from tmdb.dbo.ClaimPaymentDetail cpd 	
					where cpd.TransactionTypeCode = 'PTR'
				) src
				PIVOT
				(
					sum(Amount)
					for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName
		) as transactionAmount on transactionAmount.TransactionId = t.ID left join 
		[Tax] on tax.EffectiveStartDate <= p.StartDate and p.StartDate <= tax.EffectiveEndDate
		WHERE (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate)
		and v.VendorType = 3 and t.TransactionTypeCode in ('PTR')

		UNION ALL

		select 
		p.ShortName as 'ReportingPeriod',
		v.Number as 'HaulerRegistrationNumber',
		v.IsActive as 'HaulerStatus',
		coalesce((select t.TransactionTypeCode from tmdb.dbo.[Transaction] t where t.ID = cd.TransactionId), '') as 'DestinationType',
		coalesce((select [Number] from tmdb.dbo.Vendor v where v.ID = t.IncomingId  ),'') as 'ProcessorHaulerRegistrationNumber',
		(select [IsActive] from tmdb.dbo.Vendor v where v.ID = t.OutgoingId )  as 'Status',
		c.[Status] as 'RecordState',	
		Coalesce((select t.MarketLocation from tmdb.dbo.[Transaction] tt where tt.ID = cd.TransactionId), '') as 'UsedTireDestination',
		0 as 'TotalActualWeight',
		(coalesce(transactionAvgWeight.PLT,0) + coalesce(transactionAvgWeight.MT,0) + coalesce(transactionAvgWeight.AGLS,0) + coalesce(transactionAvgWeight.IND,0) + coalesce(transactionAvgWeight.SOTR,0) + coalesce(transactionAvgWeight.MOTR,0) + coalesce(transactionAvgWeight.LOTR,0) + coalesce(transactionAvgWeight.GOTR,0)) as 'TotalEstWeight',
		coalesce(transactionAvgWeight.PLT,0) as 'PltEstWeight',
		coalesce(transactionAvgWeight.MT,0) as 'MtEstWeight',
		coalesce(transactionAvgWeight.AGLS,0) as 'AglsEstWeight',
		coalesce(transactionAvgWeight.IND,0) as 'IndEstWeight',
		coalesce(transactionAvgWeight.SOTR,0) as 'SotrEstWeight',	
		coalesce(transactionAvgWeight.MOTR,0) as 'MotrEstWeight',
		coalesce(transactionAvgWeight.LOTR,0) as 'LotrEstWeight',
		coalesce(transactionAvgWeight.GOTR,0) as 'GotrEstWeight',
		0 as 'TotalPaymentEarned',
		0 as 'PltPaymentEarned',
		0 as 'MtPaymentEarned',
		0 as 'AglsPaymentEarned',
		0 as 'IndPaymentEarned',
		0 as 'SotrPaymentEarned',
		0 as 'MotrPaymentEarned',
		0 as 'LotrPaymentEarned',
		0 as 'GotrPaymentEarned',
		0 as 'Hst',
		0 as 'TotalPaymentEarnedWithHst'
		from 
		Claim c inner join
		[Period] p on p.ID = c.ClaimPeriodID inner join 
		[ClaimDetail] cd on c.id = cd.claimid inner join
		[transaction] t on t.id = cd.TransactionId inner join
		[Vendor] v on v.ID = c.ParticipantID inner join 
		(
			select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR
			from
			(
				select piv.ClaimId, piv.TransactionId, piv.PeriodName,
				[PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]
				from
				(
					select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber
					from tmdb.dbo.ClaimPaymentDetail cpd 	
					where (cpd.TransactionTypeCode = 'RTR' OR (cpd.TransactionTypeCode = 'HIT' and cpd.Amount = 0))
				) src
				PIVOT
				(
					sum(AverageWeight)
					for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
				) piv	 	
			) piv
			group by piv.ClaimId, piv.TransactionId, piv.PeriodName
		) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID 	
		WHERE (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate)
		and v.VendorType = 3 and t.TransactionTypeCode in ('RTR' ,'HIT')
	) main
	GROUP BY main.ReportingPeriod, main.HaulerRegistrationNumber, main.ProcessorHaulerRegistrationNumber, main.DestinationType, main.HaulerStatus, main.[Status], main.RecordState, main.UsedTireDestination
	ORDER BY main.UsedTireDestination, main.ReportingPeriod

END

