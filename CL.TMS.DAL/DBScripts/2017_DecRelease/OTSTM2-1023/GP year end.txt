UPDATE GpFiscalYear
SET EffectiveEndDate = '2018-01-28'
WHERE EffectiveEndDate = '2999-12-31';

INSERT INTO GpFiscalYear (MinimumDocDate, EffectiveStartDate, EffectiveEndDate)
VALUES('2018-01-01', '2018-01-29', '2999-12-31');