﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUserRole]    Script Date: 10/27/2017 3:38:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hefen Zhou
-- Create date: 10/27/2017
-- Description:	delete UserRole record base on the roleId
-- =============================================
IF EXISTS (select 1 from dbo.sysobjects where id = object_id(N'sp_DeleteUserRole') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
begin
    DROP PROCEDURE [dbo].[sp_DeleteUserRole]
end
go

Create PROCEDURE [dbo].[sp_DeleteUserRole] 
	@roleId as int
AS
begin
	delete from userrole where roleid=@roleId
	delete from rolepermission where roleid=@roleId
	delete from [role] where ID=@roleId
End
GO