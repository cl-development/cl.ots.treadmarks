﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

--user access permission control for Admin/Email Settings --display order:27xxxx
if NOT EXISTS (select * from AppResource where Name = 'Admin/Email Settings' and ResourceMenu = 'Admin/Email Settings' and ResourceScreen = 'Admin/Email Settings')
BEGIN

update [AppResource] set DisplayOrder = 280000 where Name = 'Admin/Announcements' and ResourceMenu = 'Admin/Announcements' and ResourceScreen = 'Admin/Announcements'

insert into [AppResource]	
	(Name,						ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Email Settings',	'Admin/Email Settings',		    'Admin/Email Settings',	    0,				270000,			@CreateBy,	@dt,		@dt)
	
END
go

--create permission for new appResource new Admin menu items

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int, @iEditSave int
Declare @getRoleIds cursor
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]
set @newResourceID=(select id from AppResource where Name = 'Admin/Email Settings' and ResourceMenu = 'Admin/Email Settings' and ResourceScreen = 'Admin/Email Settings')
select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
	(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
	WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
	BEGIN
		if (@RoleId = @superAdminID) 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
				--select  @RoleId, @newResourceID,  @iCounter[counter],'admin' 
			end 
		else 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iNoAccess)
				--select  @RoleId, @newResourceID,  @iCounter[counter] 
			end
	END
	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go