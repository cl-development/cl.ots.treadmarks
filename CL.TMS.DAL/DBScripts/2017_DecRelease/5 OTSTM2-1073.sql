﻿USE tmdb
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Email.CBHomepageURL')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Email.CBHomepageURL', '1');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Email.CBFacebookURL')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Email.CBFacebookURL', '1');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Email.CBTwitterURL')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Email.CBTwitterURL', '1');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Email.CBDefaultFromEmailAddr')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Email.CBDefaultFromEmailAddr', '0');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Email.defaultFrom')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Email.defaultFrom', null);
END
go