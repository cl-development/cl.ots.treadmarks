USE [tmdb]
GO

/****** Object:  Table [dbo].[PayeeList]    Script Date: 8/20/2018 1:42:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PayeeList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[EffectiveStartDate] [datetime] NOT NULL,
	[EffectiveEndDate] [datetime] NOT NULL,
	[Rowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_PayeeList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PayeeList]  WITH CHECK ADD  CONSTRAINT [FK_PayeeList_CreatedByUser] FOREIGN KEY([CreatedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[PayeeList] CHECK CONSTRAINT [FK_PayeeList_CreatedByUser]
GO

ALTER TABLE [dbo].[PayeeList]  WITH CHECK ADD  CONSTRAINT [FK_PayeeList_ModifiedUser] FOREIGN KEY([ModifiedByID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[PayeeList] CHECK CONSTRAINT [FK_PayeeList_ModifiedUser]
GO

CREATE TABLE [dbo].[PayeeListNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayeeListID] int null,
	[Note] varchar(MAX) null,
	[CreatedDate] [datetime] NULL,
	[UserID] [bigint]  NULL,	
 CONSTRAINT [PK_PayeeListNote] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PayeeListNote]  WITH CHECK ADD  CONSTRAINT [FK_PayeeListNote_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[PayeeListNote] CHECK CONSTRAINT [FK_PayeeListNote_User]
GO

ALTER TABLE [dbo].[PayeeListNote]  WITH CHECK ADD  CONSTRAINT [FK_PayeeListNote_PayeeList] FOREIGN KEY([PayeeListID])
REFERENCES [dbo].[PayeeList] ([ID])
GO

ALTER TABLE [dbo].[PayeeListNote] CHECK CONSTRAINT [FK_PayeeListNote_PayeeList]
GO
