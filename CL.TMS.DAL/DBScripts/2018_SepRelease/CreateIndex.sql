﻿--If IndexProperty(Object_Id('TransactionItem'), 'IX_TransactionItem_Quantity', 'IndexId') Is Null CREATE NONCLUSTERED INDEX IX_TransactionItem_Quantity ON [dbo].[TransactionItem] (Quantity);

--CREATE NONCLUSTERED INDEX IX_ScaleTicket_ScaleTicketType ON [dbo].[ScaleTicket] (ScaleTicketType)
--CREATE NONCLUSTERED INDEX IX_ScaleTicket_TransactionId ON [dbo].[ScaleTicket] (TransactionId)

--CREATE NONCLUSTERED INDEX IX_Transaction_TransactionTypeCode ON [dbo].[Transaction] (TransactionTypeCode)
--CREATE NONCLUSTERED INDEX IX_Transaction_FriendlyId ON [dbo].[Transaction] (FriendlyId)

--Drop index
--If IndexProperty(Object_Id('TransactionItem'), 'IX_TransactionItem_Quantity', 'IndexId') Is not Null Drop Index IX_TransactionItem_Quantity On dbo.[TransactionItem];