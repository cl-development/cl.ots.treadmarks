USE [tmdb]
GO

--OTSTM2-1349 AccountLable change from 'AG.LS' to 'AG/LS'
update FIAccount set AccountLabel = 'AG/LS' where AdminFICategoryID=4 and DisplayOrder = 4 and Name = 'AG/LS Stabilization'


IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Settings.HaulerClaimSummaryPTRDisplayDate')
BEGIN
	insert into AppSetting ([Key], [Value]) values ('Settings.HaulerClaimSummaryPTRDisplayDate', '2018-10-01')
END
