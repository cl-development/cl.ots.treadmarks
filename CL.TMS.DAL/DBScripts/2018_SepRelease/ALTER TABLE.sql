USE [tmdb]
GO

/****** Object:  Table [dbo].[PayeeList]    Script Date: 8/20/2018 1:42:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF (OBJECT_ID('dbo.FK_ClaimInventory_Claim', 'F') IS NULL)
BEGIN
	ALTER TABLE [dbo].[ClaimInventory] WITH CHECK ADD  CONSTRAINT [FK_ClaimInventory_Claim] FOREIGN KEY([ClaimId])
	REFERENCES [dbo].[Claim] ([ID])

	ALTER TABLE [dbo].[ClaimInventory] WITH CHECK ADD  CONSTRAINT [FK_ClaimInventory_Vendor] FOREIGN KEY([VendorId])
	REFERENCES [dbo].[Vendor] ([ID])

	ALTER TABLE [dbo].[ClaimInventory] WITH CHECK ADD  CONSTRAINT [FK_ClaimInventory_Item] FOREIGN KEY([ItemId])
	REFERENCES [dbo].[Item] ([ID])
END
GO