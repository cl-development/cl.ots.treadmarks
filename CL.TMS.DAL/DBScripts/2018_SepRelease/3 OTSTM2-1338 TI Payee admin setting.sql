﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1338 add TI payee to admin section
Purpose	    :        
Created By  :   Joe Zhou July 09th, 2018
*****************************************************************/
USE [tmdb]
GO

update TypeDefinition 
set DisplayOrder=10 
where Name='Processing Incentives Rates' and Category='RateCategory'

update TypeDefinition 
set DisplayOrder=12 
where Name='Manufacturing Incentives Rates' and Category='RateCategory'

-- insert TI Payee into TypeDefinition
if not exists (select * from typedefinition where name='TI Payee' and category='RateCategory')
begin
	insert into typedefinition (Name,Code,[Description],Category,DefinitionValue,DisplayOrder)
	values ('TI Payee','TIPayee','TI Payee','RateCategory',8,8)
end
go

update  AppResource set displayorder=231018
where ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'Processing Incentives'

update  AppResource set displayorder=231020
where ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'Manufacturing Incentives'

-- delete Name=TI Payee AppResource
 delete from RolePermission 
 where  AppResourceId in (
  select id from AppResource
  where  ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'TI Payee'
  )
  delete from AppResource
   where  ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'TI Payee'

-- insert Specific Hauler TI Payment into AppResource
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'Specific Hauler TI Payment')
	BEGIN
	declare @CreateBy nvarchar(1000);
	declare @dt datetime;
	set @CreateBy = 'System'
	set @dt = GETDATE()

	insert into [AppResource]	(Name,							ResourceMenu,					ResourceScreen,			ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
		VALUES					('Specific Hauler TI Payment',	'Admin/Fees and Incentives',	'Fees and Incentives',	2,				231016,			@CreateBy,	@dt,		@dt)		
	END
else 
	begin
		update AppResource set Name='Specific Hauler TI Payment'
		where ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives' and Name = 'TI Payee'
	end
go


--create permission for new appResource Admin/Fees and Incentives/TI Payee

Declare @superAdminID int, @newTIPayee int, @iCounter int, @RoleId int, @iNoAccess int,  @iReadOnly int, @iEditSave int
Declare @getRoleIds cursor
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]

set @newTIPayee=(select id from AppResource	where Name = 'Specific Hauler TI Payment' and ResourceMenu = 'Admin/Fees and Incentives' and ResourceScreen = 'Fees and Incentives')

select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iReadOnly = Id from AppPermission where Name = 'ReadOnly'
select @iEditSave = Id from AppPermission where Name = 'EditSave'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
	(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
	WHERE rp.AppResourceId = @newTIPayee and rp.RoleId = @RoleId)
	BEGIN
		if (@RoleId = @superAdminID) 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newTIPayee, @iEditSave)
			end 
		else 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newTIPayee, @iNoAccess)
			end
	END

	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go