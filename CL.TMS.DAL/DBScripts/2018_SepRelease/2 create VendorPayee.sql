USE [tmdb]
GO

/****** Object:  Table [dbo].[VendorPayee]    Script Date: 8/20/2018 1:47:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VendorPayee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayeeListId] [int] NOT NULL,
	[VendorId] [int] NOT NULL,
	[Rowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_VendorPayee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VendorPayee]  WITH CHECK ADD  CONSTRAINT [FK_VendorPayee_PayeeList] FOREIGN KEY([PayeeListId])
REFERENCES [dbo].[PayeeList] ([ID])
GO

ALTER TABLE [dbo].[VendorPayee] CHECK CONSTRAINT [FK_VendorPayee_PayeeList]
GO

ALTER TABLE [dbo].[VendorPayee]  WITH CHECK ADD  CONSTRAINT [FK_VendorPayee_Vendor] FOREIGN KEY([VendorId])
REFERENCES [dbo].[Vendor] ([ID])
GO


ALTER TABLE [dbo].[VendorPayee] ADD CONSTRAINT UQ_VendorPayee_PayeeListId_VendorId UNIQUE (PayeeListId, VendorId); 
GO
