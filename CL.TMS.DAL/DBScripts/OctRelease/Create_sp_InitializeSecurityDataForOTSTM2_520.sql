﻿USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_InitializeSecurityDataForOTSTM2_520]    Script Date: 10/24/2016 3:07:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Tony Cai>
-- Create date: <Feb 27, 2016>
-- Description:	<Initialize security meta data>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InitializeSecurityDataForOTSTM2_520]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
				
	--Permission id
	declare @UsersViewId int;
	declare @UsersEditId int;

	set @UsersViewId=(select top 1 Id from [Permission] where Name='ViewUsers');
	set @UsersEditId=(select top 1 Id from [Permission] where Name='EditUsers');

	--RoleId
	declare @RoleCallCentreId int;
	declare @RoleTSFClerkId int;
	declare @RoleClaimsRepresentativeId int;
	declare @RoleClaimsLeadId int;

	set @RoleCallCentreId=(select top 1 Id from [Role] where Name='CallCentre');
	set @RoleTSFClerkId=(select top 1 Id from [Role] where Name='TSFClerk');
	set @RoleClaimsRepresentativeId=(select top 1 Id from [Role] where Name='ClaimsRepresentative');
	set @RoleClaimsLeadId=(select top 1 Id from [Role] where Name='ClaimsLead');

	--Add Role Permission
	-- Staff CallCentre
	insert into PermissionRole(PermissionID,RoleID) values(@UsersEditId,@RoleCallCentreId);

	--Staff TSFClerk
	insert into PermissionRole(PermissionID,RoleID) values(@UsersViewId,@RoleTSFClerkId);
	insert into PermissionRole(PermissionID,RoleID) values(@UsersEditId,@RoleTSFClerkId);

	-- Staff Claims representative
	insert into PermissionRole(PermissionID,RoleID) values(@UsersEditId,@RoleClaimsRepresentativeId);

	--Staff Claims Lead
	insert into PermissionRole(PermissionID,RoleID) values(@UsersEditId,@RoleClaimsLeadId);

END

GO

