IF NOT EXISTS (select * from TypeDefinition  where Category='GSModuleType')
insert into TypeDefinition (Name,	Code,	[Description],	Category,	DefinitionValue,	DisplayOrder) values 
('Collector',	'Collector',	'Collector',	'GSModuleType',2,2 ),
('Hauler',		'Hauler',		'Hauler',		'GSModuleType',3,3 ),
('Processor',	'Processor',	'Processor',	'GSModuleType',4,4 ),
('RPM',			'RPM',			'RPM',			'GSModuleType',5,5 ),
('Steward',		'Steward',		'Steward',		'GSModuleType',1,1 ),
('Participant',	'Participant',	'Participant',	'GSModuleType',6,0 ),
('Transactions','',				'Transactions',	'GSModuleType',7,0 )
go