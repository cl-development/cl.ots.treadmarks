﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-905 for rename QR codes
Purpose     :   OTSTM2-905 Role and Permission Matrix issues
			:   1. please remove level #3 QRCODE under QRCODE menu and page , keep it similar to All Transactions and All users page
			:   2. Remove custom options for this resource , follow All Transactions page
Created By  :   Joe Zhou Aug 3th, 2017
*****************************************************************/

use tmdb
go
--update [tmdb].[dbo].[AppResource]
--set name='All QR Codes',ResourceScreen='QR Codes' where  name like '%QR Codes%' and ResourceLevel=1

--update AppResource set Name='All QR Codes' , ResourceMenu='QR Codes' , ResourceScreen='All QR Codes'
--where  name like '%QR Codes%' and ResourceLevel=1
go