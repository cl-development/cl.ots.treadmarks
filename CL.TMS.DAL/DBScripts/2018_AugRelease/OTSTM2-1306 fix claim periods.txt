--select [PeriodType]
--      ,[ShortName]
--      ,[PeriodDesc]
--      ,[StartDate]
--      ,dateadd(second, -1, DATEAdd(month, 2, [StartDate]))
--      ,[SubmitEnd]			  	  
--	  ,DATEDIFF(month, [SubmitEnd], [StartDate])
--	  ,[EndDate]
--      ,[ShowStart]
--      ,[SubmitStart]
--      ,[SubmitEnd]
--	  ,[EnableSubmit]
--      ,[ShowPeriod]
--      ,[IsSpecialEvent]
--	   from Period p 
--where year(p.StartDate)>2018
--and p.PeriodType in (3)
----and DATEDIFF(month, [SubmitEnd], [StartDate]) < -1


update Period set [SubmitEnd]=dateadd(second, -1, DATEAdd(month, 2, [StartDate]))
where year(StartDate)>2018
and PeriodType in (3)
and DATEDIFF(month, [SubmitEnd], [StartDate]) < -1

--select [PeriodType]
--      ,[ShortName]
--      ,[PeriodDesc]
--      ,[StartDate]
--      ,dateadd(second, -1, DATEAdd(month, 3, [StartDate]))
--      ,[SubmitEnd]			  	  
--	  ,DATEDIFF(month, [SubmitEnd], [StartDate])
--	  ,[EndDate]
--      ,[ShowStart]
--      ,[SubmitStart]
--      ,[SubmitEnd]
--	  ,[EnableSubmit]
--      ,[ShowPeriod]
--      ,[IsSpecialEvent]
--	   from Period p 
--where 
--year(p.StartDate)>2015 and
--p.PeriodType in (4,5)
--and DATEDIFF(month, [SubmitEnd], [StartDate]) = -1


update Period set [SubmitEnd]=dateadd(second, -1, DATEAdd(month, 3, [StartDate]))
where 
year(StartDate)>2015 and
PeriodType in (4,5)
and DATEDIFF(month, [SubmitEnd], [StartDate]) = -1