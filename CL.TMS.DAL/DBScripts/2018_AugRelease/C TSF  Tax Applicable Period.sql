﻿USE [tmdb]
GO
if not exists (select * from AppSetting where [Key] ='Settings.TSFTaxApplicablePeriod')
begin
insert into AppSetting ([Key], Value) values ('Settings.TSFTaxApplicablePeriod', '2018-08-01');
end

