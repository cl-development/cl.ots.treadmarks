IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_Rpt_HaulerVolumeReportPayee]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_Rpt_HaulerVolumeReportPayee]
END

/****** Object:  StoredProcedure [dbo].[sp_AssignRoleToAllStaffUsersByRoleId]    Script Date:  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_HaulerVolumeReportPayee]
 @startDate DateTime, @endDate DateTime    
AS    
BEGIN    
    
 select    
 main.ReportingPeriod, main.HaulerRegistrationNumber,     
 main.HaulerRegistrationNumber,     
 main.HaulerStatus,     
 main.DestinationType,     
 main.ProcessorHaulerRegistrationNumber,     
 main.[Status],     
 main.RecordState,     
 main.UsedTireDestination as 'UsedTireDestionation',    
 sum(main.TotalActualWeight) as TotalActualWeight,    
 sum(main.TotalEstWeight) as TotalEstWeight,    
 sum(main.PltEstWeight) as 'PltEstWeight',    
 sum(main.MtEstWeight) as 'MtEstWeight',    
 sum(main.AglsEstWeight) as 'AglsEstWeight',    
 sum(main.IndEstWeight) as 'IndEstWeight',    
 sum(main.SotrEstWeight) as 'SotrEstWeight',     
 sum(main.MotrEstWeight) as 'MotrEstWeight',    
 sum(main.LotrEstWeight) as 'LotrEstWeight',    
 sum(main.GotrEstWeight) as 'GotrEstWeight',    
 ROUND(sum(main.TotalPaymentEarnedProcessor),2) as 'TotalPaymentEarnedProcessor',    
 sum(main.PltPaymentEarnedProcessor) as 'PltPaymentEarnedProcessor',
 sum(main.MtPaymentEarnedProcessor) as 'MtPaymentEarnedProcessor',
 sum(main.AglsPaymentEarnedProcessor) as 'AglsPaymentEarnedProcessor',
 sum(main.IndPaymentEarnedProcessor) as 'IndPaymentEarnedProcessor',
 sum(main.SotrPaymentEarnedProcessor) as 'SotrPaymentEarnedProcessor',
 sum(main.MotrPaymentEarnedProcessor) as 'MotrPaymentEarnedProcessor',
 sum(main.LotrPaymentEarnedProcessor) as 'LotrPaymentEarnedProcessor',
 sum(main.GotrPaymentEarnedProcessor) as 'GotrPaymentEarnedProcessor',

 ROUND(sum(main.TotalPaymentEarnedHauler),2) as 'TotalPaymentEarnedHauler',
 sum(main.PltPaymentEarnedHauler) as 'PltPaymentEarnedHauler',
 sum(main.MtPaymentEarnedHauler) as 'MtPaymentEarnedHauler',
 sum(main.AglsPaymentEarnedHauler) as 'AglsPaymentEarnedHauler',
 sum(main.IndPaymentEarnedHauler) as 'IndPaymentEarnedHauler',    
 sum(main.SotrPaymentEarnedHauler) as 'SotrPaymentEarnedHauler',    
 sum(main.MotrPaymentEarnedHauler) as 'MotrPaymentEarnedHauler',    
 sum(main.LotrPaymentEarnedHauler) as 'LotrPaymentEarnedHauler',    
 sum(main.GotrPaymentEarnedHauler) as 'GotrPaymentEarnedHauler',    
 sum(main.Hst) as 'Hst',    
 ROUND(sum(main.TotalPaymentEarnedWithHst),2) as 'TotalPaymentEarnedWithHst'    
 from    
 (    
    
  select     
  p.ShortName as 'ReportingPeriod',    
  v.Number as 'HaulerRegistrationNumber',    
  v.IsActive as 'HaulerStatus',    
  coalesce((select t.TransactionTypeCode from dbo.[Transaction] t where t.ID = cd.TransactionId), '') as 'DestinationType',    
  (select [Number] from dbo.Vendor v where v.ID = t.IncomingId  ) as 'ProcessorHaulerRegistrationNumber',    
  (select [IsActive] from dbo.Vendor v where v.ID = t.IncomingId   )  as 'Status',    
  c.[Status] as 'RecordState',     
  coalesce((select t.MarketLocation from dbo.[Transaction] tt where tt.ID = cd.TransactionId), '') as 'UsedTireDestination',      
  (coalesce(transactionActualWeight.PLT,0) + coalesce(transactionActualWeight.MT,0) + coalesce(transactionActualWeight.AGLS,0) + coalesce(transactionActualWeight.IND,0) + coalesce(transactionActualWeight.SOTR,0) + coalesce(transactionActualWeight.MOTR,0) + coalesce(transactionActualWeight.LOTR,0) + coalesce(transactionActualWeight.GOTR,0)) as 'TotalActualWeight',    
  (coalesce(transactionAvgWeight.PLT,0) + coalesce(transactionAvgWeight.MT,0) + coalesce(transactionAvgWeight.AGLS,0) + coalesce(transactionAvgWeight.IND,0) + coalesce(transactionAvgWeight.SOTR,0) + coalesce(transactionAvgWeight.MOTR,0) + coalesce(transactionAvgWeight.LOTR,0) + coalesce(transactionAvgWeight.GOTR,0)) as 'TotalEstWeight',    
  coalesce(transactionAvgWeight.PLT,0) as 'PltEstWeight',    
  coalesce(transactionAvgWeight.MT,0) as 'MtEstWeight',    
  coalesce(transactionAvgWeight.AGLS,0) as 'AglsEstWeight',    
  coalesce(transactionAvgWeight.IND,0) as 'IndEstWeight',    
  coalesce(transactionAvgWeight.SOTR,0) as 'SotrEstWeight',     
  coalesce(transactionAvgWeight.MOTR,0) as 'MotrEstWeight',    
  coalesce(transactionAvgWeight.LOTR,0) as 'LotrEstWeight',    
  coalesce(transactionAvgWeight.GOTR,0) as 'GotrEstWeight',    

  (coalesce(transactionAmountProcessorPayee.PLT,0)+ coalesce(transactionAmountProcessorPayee.MT,0) + coalesce(transactionAmountProcessorPayee.AGLS,0) + coalesce(transactionAmountProcessorPayee.IND,0) + coalesce(transactionAmountProcessorPayee.SOTR,0) + coalesce(transactionAmountProcessorPayee.MOTR,0) + coalesce(transactionAmountProcessorPayee.LOTR,0) + coalesce(transactionAmountProcessorPayee.GOTR,0)) as 'TotalPaymentEarnedProcessor',
  coalesce(transactionAmountProcessorPayee.PLT, 0) as 'PltPaymentEarnedProcessor',    
  coalesce(transactionAmountProcessorPayee.MT, 0) as 'MtPaymentEarnedProcessor',
  coalesce(transactionAmountProcessorPayee.AGLS, 0) as 'AglsPaymentEarnedProcessor',
  coalesce(transactionAmountProcessorPayee.IND, 0) as 'IndPaymentEarnedProcessor',
  coalesce(transactionAmountProcessorPayee.SOTR, 0) as 'SotrPaymentEarnedProcessor',    
  coalesce(transactionAmountProcessorPayee.MOTR, 0) as 'MotrPaymentEarnedProcessor',    
  coalesce(transactionAmountProcessorPayee.LOTR, 0) as 'LotrPaymentEarnedProcessor',    
  coalesce(transactionAmountProcessorPayee.GOTR, 0) as 'GotrPaymentEarnedProcessor',    

  (coalesce(transactionAmountHaulerPayee.PLT,0)+ coalesce(transactionAmountHaulerPayee.MT,0) + coalesce(transactionAmountHaulerPayee.AGLS,0) + coalesce(transactionAmountHaulerPayee.IND,0) + coalesce(transactionAmountHaulerPayee.SOTR,0) + coalesce(transactionAmountHaulerPayee.MOTR,0) + coalesce(transactionAmountHaulerPayee.LOTR,0) + coalesce(transactionAmountHaulerPayee.GOTR,0)) as 'TotalPaymentEarnedHauler',
  coalesce(transactionAmountHaulerPayee.PLT, 0) as 'PltPaymentEarnedHauler',    
  coalesce(transactionAmountHaulerPayee.MT, 0) as 'MtPaymentEarnedHauler',    
  coalesce(transactionAmountHaulerPayee.AGLS, 0) as 'AglsPaymentEarnedHauler',
  coalesce(transactionAmountHaulerPayee.IND, 0) as 'IndPaymentEarnedHauler',
  coalesce(transactionAmountHaulerPayee.SOTR, 0) as 'SotrPaymentEarnedHauler',    
  coalesce(transactionAmountHaulerPayee.MOTR, 0) as 'MotrPaymentEarnedHauler',    
  coalesce(transactionAmountHaulerPayee.LOTR, 0) as 'LotrPaymentEarnedHauler',    
  coalesce(transactionAmountHaulerPayee.GOTR, 0) as 'GotrPaymentEarnedHauler',    
  0.0 as 'Hst',    
  (coalesce(transactionAmountProcessorPayee.PLT,0)+ coalesce(transactionAmountProcessorPayee.MT,0) + coalesce(transactionAmountProcessorPayee.AGLS,0) + coalesce(transactionAmountProcessorPayee.IND,0) + coalesce(transactionAmountProcessorPayee.SOTR,0) + coalesce(transactionAmountProcessorPayee.MOTR,0) + coalesce(transactionAmountProcessorPayee.LOTR,0) + coalesce(transactionAmountProcessorPayee.GOTR,0))+
  (coalesce(transactionAmountHaulerPayee.PLT,0)+ coalesce(transactionAmountHaulerPayee.MT,0) + coalesce(transactionAmountHaulerPayee.AGLS,0) + coalesce(transactionAmountHaulerPayee.IND,0) + coalesce(transactionAmountHaulerPayee.SOTR,0) + coalesce(transactionAmountHaulerPayee.MOTR,0) + coalesce(transactionAmountHaulerPayee.LOTR,0) + coalesce(transactionAmountHaulerPayee.GOTR,0)) as 'TotalPaymentEarnedWithHst'    
  from     
  Claim c inner join    
  [Period] p on p.ID = c.ClaimPeriodID inner join     
  [ClaimDetail] cd on c.id = cd.claimid inner join    
  [transaction] t on t.id = cd.TransactionId inner join    
  [Vendor] v on v.ID = c.ParticipantID inner join     
  (    
   select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR    
   from    
   (    
    select piv.ClaimId, piv.TransactionId, piv.PeriodName, [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]    
    from    
    (    
     select cpd.* --  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber    
     from dbo.ClaimPaymentDetail cpd      
    ) src    
    PIVOT    
    (    
     sum([AverageWeight])    
     for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])    
    ) piv       
   ) piv    
   group by piv.ClaimId, piv.TransactionId, piv.PeriodName    
  ) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID left join     
  (    
   select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR    
   from    
   (    
    select piv.ClaimId, piv.TransactionId, piv.PeriodName,    
    [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]    
    from    
    (    
     select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber    
     from dbo.ClaimPaymentDetail cpd      
    ) src    
    PIVOT    
    (    
     sum(ActualWeight)    
     for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])    
    ) piv       
   ) piv    
   group by piv.ClaimId, piv.TransactionId, piv.PeriodName    
  ) as transactionActualWeight on transactionActualWeight.TransactionId = t.ID left join     
  (    
   select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR    
   from    
   (    
    select piv.ClaimId, piv.TransactionId, piv.PeriodName,    
    [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]    
    from    
    (    
     select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber    
     from dbo.ClaimPaymentDetail cpd      
     where cpd.TransactionTypeCode = 'PTR' and cpd.PaymentType != 19 --19:HaulerPayee   
    ) src    
    PIVOT    
    (    
     sum(Amount)    
     for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])    
    ) piv       
   ) piv    
   group by piv.ClaimId, piv.TransactionId, piv.PeriodName    
  ) as transactionAmountProcessorPayee on transactionAmountProcessorPayee.TransactionId = t.ID left join     
  (    
   select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR    
   from    
   (    
    select piv.ClaimId, piv.TransactionId, piv.PeriodName,    
    [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]    
    from    
    (    
     select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber    
     from dbo.ClaimPaymentDetail cpd      
     where cpd.TransactionTypeCode = 'PTR' and cpd.PaymentType = 19 --19:HaulerPayee   
    ) src    
    PIVOT    
    (    
     sum(Amount)    
     for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])    
    ) piv       
   ) piv    
   group by piv.ClaimId, piv.TransactionId, piv.PeriodName    
  ) as transactionAmountHaulerPayee on transactionAmountHaulerPayee.TransactionId = t.ID left join    [Tax] on tax.EffectiveStartDate <= p.StartDate and p.StartDate <= tax.EffectiveEndDate    
  WHERE (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate)    
  and v.VendorType = 3 and t.TransactionTypeCode in ('PTR')    
    
  UNION ALL    
    
  select     
  p.ShortName as 'ReportingPeriod',    
  v.Number as 'HaulerRegistrationNumber',    
  v.IsActive as 'HaulerStatus',    
  coalesce((select t.TransactionTypeCode from dbo.[Transaction] t where t.ID = cd.TransactionId), '') as 'DestinationType',    
  coalesce((select [Number] from dbo.Vendor v where v.ID = t.IncomingId  ),'') as 'ProcessorHaulerRegistrationNumber',    
  (select [IsActive] from dbo.Vendor v where v.ID = t.OutgoingId )  as 'Status',    
  c.[Status] as 'RecordState',     
  Coalesce((select t.MarketLocation from dbo.[Transaction] tt where tt.ID = cd.TransactionId), '') as 'UsedTireDestination',    
  0 as 'TotalActualWeight',    
  (coalesce(transactionAvgWeight.PLT,0) + coalesce(transactionAvgWeight.MT,0) + coalesce(transactionAvgWeight.AGLS,0) + coalesce(transactionAvgWeight.IND,0) + coalesce(transactionAvgWeight.SOTR,0) + coalesce(transactionAvgWeight.MOTR,0) + coalesce(transactionAvgWeight.LOTR,0) + coalesce(transactionAvgWeight.GOTR,0)) as 'TotalEstWeight',    
  coalesce(transactionAvgWeight.PLT,0) as 'PltEstWeight',    
  coalesce(transactionAvgWeight.MT,0) as 'MtEstWeight',    
  coalesce(transactionAvgWeight.AGLS,0) as 'AglsEstWeight',    
  coalesce(transactionAvgWeight.IND,0) as 'IndEstWeight',    
  coalesce(transactionAvgWeight.SOTR,0) as 'SotrEstWeight',     
  coalesce(transactionAvgWeight.MOTR,0) as 'MotrEstWeight',    
  coalesce(transactionAvgWeight.LOTR,0) as 'LotrEstWeight',    
  coalesce(transactionAvgWeight.GOTR,0) as 'GotrEstWeight',    
  0 as 'TotalPaymentEarnedProcessor',
  0 as 'PltPaymentEarnedProcessor',    
  0 as 'MtPaymentEarnedProcessor',
  0 as 'AglsPaymentEarnedProcessor',    
  0 as 'IndPaymentEarnedProcessor',
  0 as 'SotrPaymentEarnedProcessor',    
  0 as 'MotrPaymentEarnedProcessor',    
  0 as 'LotrPaymentEarnedProcessor',    
  0 as 'GotrPaymentEarnedProcessor',  
    
  0 as 'TotalPaymentEarnedHauler',    
  0 as 'PltPaymentEarnedHauler',    
  0 as 'MtPaymentEarnedHauler',
  0 as 'AglsPaymentEarnedHauler',    
  0 as 'IndPaymentEarnedHauler',    
  0 as 'SotrPaymentEarnedHauler',    
  0 as 'MotrPaymentEarnedHauler',    
  0 as 'LotrPaymentEarnedHauler',    
  0 as 'GotrPaymentEarnedHauler',    
  0 as 'Hst',    
  0 as 'TotalPaymentEarnedWithHst'    
  from     
  Claim c inner join    
  [Period] p on p.ID = c.ClaimPeriodID inner join     
  [ClaimDetail] cd on c.id = cd.claimid inner join    
  [transaction] t on t.id = cd.TransactionId inner join    
  [Vendor] v on v.ID = c.ParticipantID inner join     
  (    
   select piv.ClaimId, piv.TransactionId, piv.PeriodName, sum([PLT]) as PLT, sum([MT]) as MT, sum([AGLS]) as AGLS, sum([IND]) as IND, sum([SOTR]) as SOTR, sum([MOTR]) as MOTR, sum([LOTR]) as LOTR, sum([GOTR]) as GOTR    
   from    
   (    
    select piv.ClaimId, piv.TransactionId, piv.PeriodName,    
    [PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR]    
    from    
    (    
     select cpd.*--  cpd.TransactionId, cpd.ClaimId, cpd.[Weight], cpd.ItemName, cpd.RegistrationNumber    
     from dbo.ClaimPaymentDetail cpd      
     where (cpd.TransactionTypeCode = 'RTR' OR (cpd.TransactionTypeCode = 'HIT' and cpd.Amount = 0))    
    ) src    
    PIVOT    
    (    
     sum(AverageWeight)    
     for ItemName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])    
    ) piv       
   ) piv    
   group by piv.ClaimId, piv.TransactionId, piv.PeriodName    
  ) as transactionAvgWeight on transactionAvgWeight.TransactionId = t.ID      
  WHERE (@startDate is null or @startDate <= p.StartDate) AND (@endDate is null or p.StartDate  <= @endDate)    
  and v.VendorType = 3 and t.TransactionTypeCode in ('RTR' ,'HIT')    
 ) main    
 GROUP BY main.ReportingPeriod, main.HaulerRegistrationNumber, main.ProcessorHaulerRegistrationNumber, main.DestinationType, main.HaulerStatus, main.[Status], main.RecordState, main.UsedTireDestination    
 ORDER BY main.UsedTireDestination, main.ReportingPeriod    
    
END