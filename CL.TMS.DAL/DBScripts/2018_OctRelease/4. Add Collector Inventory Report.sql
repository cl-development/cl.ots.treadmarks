﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

-- insert FIAccount initial data
IF NOT EXISTS (select * from Report where ID=33 and ReportCategoryID = 2)
BEGIN

	INSERT INTO Report 
		(ID,	[ReportName],								[ReportCategoryID],	[Description])
		VALUES 
        (33,	'Collector Reuse/Collection Inventory Count', 2,					'This report pulls data related to tire counts entered by the Collector in their claim for either Reuse Tires and/or the Collection Inventory Count panel . If the report displays Collector Claim Periods prior to October - December 2018 (i.e July-Sept 2018 CA), this data in the report is related to tires reported by the Collector in Reuse Tire panel that were not collected in that claim period. If the Collector Claim Period is from October - December 2018 or after, the data in this report is related tires reporting in the Collection Inventory Count panel for tires not collected by a hauler at the end of the claim period. ')
End
Else 
begin 
	update Report set 
	ReportName='Collector Reuse/Collection Inventory Count',
	[Description]='This report pulls data related to tire counts entered by the Collector in their claim for either Reuse Tires and/or the Collection Inventory Count panel . If the report displays Collector Claim Periods prior to October - December 2018, this data in the report is related to tires reported by the Collector in Reuse Tire panel that were not collected in that claim period. If the Collector Claim Period is from October - December 2018 or after, the data in this report is related tires reporting in the Collection Inventory Count panel for tires not collected by a hauler at the end of the claim period. '
	where ID=33 and ReportCategoryID = 2
end
go