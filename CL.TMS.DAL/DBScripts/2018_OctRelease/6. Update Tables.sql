USE [tmdb]
GO

--OTSTM2-1370
if not exists (select * from AppSetting where [Key] ='Settings.CollectorInventoryPanelStartDate')
begin
	insert into AppSetting ([Key], [Value]) values ('Settings.CollectorInventoryPanelStartDate', '2018-10-01');
end

update appResource set Name = 'Reuse Tires / Collection Inventory Count' where ResourceMenu='Claims' and ResourceScreen = 'Collector Claim Summary' and DisplayOrder = 101115

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Settings.LastPickupEffectiveDate')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Settings.LastPickupEffectiveDate', '2019-01-25');
END

--OTSTM2-1360
if not exists (select * from AppSetting where [Key] ='Settings.StewardHstNumber')
begin
	insert into AppSetting ([Key], [Value]) values ('Settings.StewardHstNumber', '874044803');
end

go