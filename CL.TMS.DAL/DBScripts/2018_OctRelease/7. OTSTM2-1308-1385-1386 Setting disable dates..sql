USE [tmdb]
GO

--OTSTM2-1308 setting disable remittance effective date
if not exists (select * from AppSetting where [Key] ='Settings.DisableRemittanceEffectiveDate')
	begin
		insert into AppSetting ([Key], Value) values ('Settings.DisableRemittanceEffectiveDate', '2019-01-01');
	end
else 
	begin
		update AppSetting  set [Value]='2019-01-01' where [Key]='Settings.DisableRemittanceEffectiveDate';
	end

--OTSTM2-1385 setting disable transaction effective date
if not exists (select * from AppSetting where [Key] ='Settings.DisableTransactionCalendarEffectiveDate')
	begin
		insert into AppSetting ([Key], Value) values ('Settings.DisableTransactionCalendarEffectiveDate', '2019-02-01');
	end
else 
	begin
		update AppSetting  set [Value]='2019-02-01' where [Key]='Settings.DisableTransactionCalendarEffectiveDate';
	end

--OTSTM2-1386 hidden collector claim effective date
if not exists (select * from AppSetting where [Key] ='Settings.HideCollectorClaimEffectiveDate')
	begin
		insert into AppSetting ([Key], Value) values ('Settings.HideCollectorClaimEffectiveDate', '2019-03-01');
	end
else 
	begin
		update AppSetting  set [Value]='2019-03-01' where [Key]='Settings.HideCollectorClaimEffectiveDate';
	end