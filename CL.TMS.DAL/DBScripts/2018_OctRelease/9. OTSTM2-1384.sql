--USE [tmdb_Prod_20180814]
GO

update [Period] set ShowPeriod = 0 where PeriodType = 2 and StartDate = '2019-4-1' and EndDate = '2019-6-30'

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Verify that the stored procedure does not already exist.  
IF OBJECT_ID ( 'sp_GetErrorInfo', 'P' ) IS NOT NULL   
    DROP PROCEDURE sp_GetErrorInfo;  
GO  

-- Create procedure to retrieve error information.  
CREATE PROCEDURE sp_GetErrorInfo  
AS  
SELECT  
    ERROR_NUMBER() AS ErrorNumber  
    ,ERROR_SEVERITY() AS ErrorSeverity  
    ,ERROR_STATE() AS ErrorState  
    ,ERROR_PROCEDURE() AS ErrorProcedure  
    ,ERROR_LINE() AS ErrorLine  
    ,ERROR_MESSAGE() AS ErrorMessage;  
GO 

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_AutoSubmitClaim]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_AutoSubmitClaim]
END
GO

CREATE PROCEDURE [dbo].[sp_AutoSubmitClaim] -- submit with nil claim
	@claimId as int, @periodDate as datetime
AS
BEGIN
	SET NOCOUNT ON;
	declare @pId as int, @itemId as int
	--set @periodDate = '2018-10-1';
	declare @dateNow as datetime, @addressId as int, @vendorType as int, @periodName as varchar(150), @ItemRate as float, @sysUser as varchar(150), @assignedUser as varchar(150), @assignedUserName as varchar(150), @assignedUserId as int, @systemUserId as int, @vendorNbr as varchar(150);
	set @assignedUser = 'kcarreira@rethinktires.ca';
	set @assignedUserName = (select FirstName + ' ' + LastName from[User] where UserName = @assignedUser);
	set @assignedUserId = (select ID from[User] where UserName = @assignedUser);
	set @pId = (select ID from Period where PeriodType = 2 and StartDate <= @periodDate and EndDate >= @periodDate);
	set @periodName = (select ShortName from Period where PeriodType = 2 and StartDate <= @periodDate and EndDate >= @periodDate);
	set @sysUser = 'test@test.com';
	set @systemUserId = (select ID from [User] where UserName = @sysUser);
	set @vendorNbr = (select v.Number from Claim c join Vendor v on c.ParticipantID = v.ID where c.ID = @claimId);
	select @dateNow = GETDATE();

	set @itemId = 65
	WHILE @itemId <=72
	BEGIN 
		set @ItemRate = (select ItemRate from Rate where ClaimType = 2 and EffectiveStartDate <= @periodDate and EffectiveEndDate >= @periodDate and PeriodID = @pId and ItemID = @itemId);
				
		insert into ClaimPayment (ClaimID,Weight,Rate,ItemType,PaymentType,CreatedDate) values
		( @claimId, 0, @ItemRate, 1, case when @itemId = 65 then 4 
		when @itemId = 66 then 12
		when @itemId = 67 then 13
		when @itemId = 68 then 14
		when @itemId = 69 then 15
		when @itemId = 70 then 16
		when @itemId = 71 then 17
		when @itemId = 72 then 18
		end, @dateNow);

		set @itemId = @itemId +1;
	END

	BEGIN TRY  
		insert into ClaimSummary (ClaimID, UpdatedDate) values (@claimId, @dateNow);
		insert into SystemLog (Category, [Message], CreatedDate) values ('Information', 'Auto submit claim for collector claimID:' +  CONVERT(varchar(10), @claimId), @dateNow);
	
		update Claim set Status = 'Under Review', AssignToUserId = @assignedUserId, ReviewedBy = @assignedUserId, AssignDate= @dateNow, ClaimOnhold = 1, ClaimOnHoldBy = @systemUserId, ClaimOnHoldDate = @dateNow where ID = @claimId

		--insert into Activity ([Message], CreatedTime, Initiator, InitiatorName, Assignee, AssigneeName, ActivityType, ActivityArea, ObjectId) values
		--('System <strong>assigned</strong> this claim to <strong>' + @assignedUserName + '</strong> (Representative).', @dateNow, @sysUser, '', @sysUser, '', 1, 'Collector', @claimId);

		--insert into [Notification] 
		--([Message], 
		--CreatedTime,	Assigner,	AssignerInitial,	AssignerName,	Assignee,	AssigneeName,	NotificationType,	NotificationArea,	ObjectId,	[Status]) values
		--('<strong>System</strong> assigned <strong> claim</strong> to you for <strong>review</strong>.',	
		--@dateNow,		@sysUser,	'S',				'System',		@sysUser,	'System',		1,					'Collector',		@claimId,	'Unread');
	END TRY  
	BEGIN CATCH  
		 EXECUTE sp_GetErrorInfo;
	END CATCH 
END
GO

/****** Object:  StoredProcedure [dbo].[sp_AddClaimForVendors]    Script Date: 04/10/2018 1:35:09 PM ******/
--ALTER PROCEDURE [dbo].[sp_AddClaimForVendors]
--	@periodDate as datetime
--AS
--BEGIN
--	SET NOCOUNT ON;
--	declare @vendorId as int;
--	declare @addressId as int;
--	declare @vendorType as int;
--	declare @claimId as int;
--	declare @effectiveDate as datetime;
--	set @effectiveDate = '2019-4-1';

--	declare @collectorPeriodId as int;
--	set @collectorPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 2);

--	declare @haulerPeriodId as int;
--	set @haulerPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 3);

--	declare @processorPeriodId as int;
--	set @processorPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 4);

--	declare @rpmPeriodId as int;
--	set @rpmPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 5);

--	declare @vendorCursor as Cursor;
--	set @vendorCursor = cursor for
--	select a.VendorType, a.ID as vendorId, c.id as addressid from vendor a, VendorAddress b, address c where a.ID = b.VendorID and b.AddressID = c.ID and a.IsActive = 1 and c.AddressType = 1 and a.VendorType != 2; --VendorType is the different between two sp

--	open @vendorCursor;
--	fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;

--	WHILE @@FETCH_STATUS  = 0
--	BEGIN
--		if NOT EXISTS(select top 1 * from claim a, Period b where a.ClaimPeriodID = b.ID and b.StartDate <= @periodDate and b.EndDate >= @periodDate and a.ParticipantID = @vendorId)
--		begin
--		   if (@vendorType = 2)
--				if @periodDate = @effectiveDate -- OTSTM2-1384
--					begin 
--						IF EXISTS (			--previous claim(month-3) status is in ('Approved', 'Submitted', 'Under Review')
--							select top 1 c.Status, DATEADD(month, -3, p.StartDate)[p.StartDate-3], p.StartDate from Claim c 
--							join Period p on c.ClaimPeriodID = p.ID 
--							where c.ClaimType = 2 and c.ParticipantID = @vendorId and c.Status in ('Approved', 'Submitted', 'Under Review', 'Hold', 'Audit Review', 'Audit Hold', 'Receive Payment') and c.ClaimPeriodID in (select ID from [Period] where PeriodType=2 and StartDate = DATEADD(month, -3, @periodDate)))
--							begin
--								insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType, IsTaxApplicable) values(@collectorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
--								set @claimId = (select ID from Claim where ClaimPeriodID = @collectorPeriodId and ParticipantID = @vendorId);
--								exec [dbo].[sp_AutoSubmitClaim] @claimId = @claimId, @periodDate = @periodDate;
--							end
--					end
--				else
--					begin
--						insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType, IsTaxApplicable) values(@collectorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
--					end
--			if (@vendorType = 3)
--				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@haulerPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
--			if (@vendorType = 4)
--				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@processorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
--			if (@vendorType = 5)
--				insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@rpmPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
--		end
--		fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;
--	END
--	CLOSE @vendorCursor;
--	DEALLOCATE @vendorCursor;	
--END
--GO

IF OBJECT_ID ( 'sp_AddClaimForCollectors', 'P' ) IS NOT NULL   
    DROP PROCEDURE sp_AddClaimForCollectors;  
GO  

CREATE PROCEDURE [dbo].[sp_AddClaimForCollectors]
	@periodDate as datetime
AS
BEGIN
	SET NOCOUNT ON;
	declare @vendorId as int;
	declare @addressId as int;
	declare @vendorType as int;
	declare @claimId as int;
	declare @effectiveDate as datetime;
	set @effectiveDate = '2019-4-1';

	declare @collectorPeriodId as int;
	set @collectorPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 2);

	declare @haulerPeriodId as int;
	set @haulerPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 3);

	declare @processorPeriodId as int;
	set @processorPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 4);

	declare @rpmPeriodId as int;
	set @rpmPeriodId = (select top 1 id from period a where a.StartDate <= @periodDate and a.EndDate >= @periodDate and a.PeriodType = 5);

	declare @vendorCursor as Cursor;
	set @vendorCursor = cursor for
	select a.VendorType, a.ID as vendorId, c.id as addressid from vendor a, VendorAddress b, address c where a.ID = b.VendorID and b.AddressID = c.ID and a.IsActive = 1 and c.AddressType = 1 and a.VendorType = 2; --VendorType is the different between two sp

	open @vendorCursor;
	fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;

	WHILE @@FETCH_STATUS  = 0
	BEGIN
		if NOT EXISTS(select top 1 * from claim a, Period b where a.ClaimPeriodID = b.ID and b.StartDate <= @periodDate and b.EndDate >= @periodDate and a.ParticipantID = @vendorId)
		begin
		   if (@vendorType = 2)
				if @periodDate = @effectiveDate -- OTSTM2-1384
					begin 
						--IF EXISTS (			--previous claim(month-3) status is in ('Approved', 'Submitted', 'Under Review')
						--	select top 1 c.Status, DATEADD(month, -3, p.StartDate)[p.StartDate-3], p.StartDate from Claim c 
						--	join Period p on c.ClaimPeriodID = p.ID 
						--	where c.ClaimType = 2 and c.ParticipantID = @vendorId and c.Status in ('Approved', 'Submitted', 'Under Review', 'Hold', 'Audit Review', 'Audit Hold', 'Receive Payment') and c.ClaimPeriodID in (select ID from [Period] where PeriodType=2 and StartDate = DATEADD(month, -3, @periodDate)))
							begin
								insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType, IsTaxApplicable) values(@collectorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
								set @claimId = (select ID from Claim where ClaimPeriodID = @collectorPeriodId and ParticipantID = @vendorId);
								exec [dbo].[sp_AutoSubmitClaim] @claimId = @claimId, @periodDate = @periodDate;
							end
					end
				else
					begin
						insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType, IsTaxApplicable) values(@collectorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
					end
			--if (@vendorType = 3)
			--	insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@haulerPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
			--if (@vendorType = 4)
			--	insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@processorPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
			--if (@vendorType = 5)
			--	insert into Claim(ClaimPeriodId, ParticipantID, ParticipantAddressID, [status], ClaimType,IsTaxApplicable) values(@rpmPeriodId, @vendorId, @addressId, 'Open', @vendorType,1);
		end
		fetch next from @vendorCursor into @vendorType, @vendorId, @addressId;
	END
	CLOSE @vendorCursor;
	DEALLOCATE @vendorCursor;	
END
GO