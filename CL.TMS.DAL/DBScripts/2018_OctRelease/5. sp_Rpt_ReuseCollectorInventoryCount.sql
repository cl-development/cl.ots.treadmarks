﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_ReuseCollectorInventoryCount]    Script Date: 9/24/18 2:31:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[sp_Rpt_ReuseCollectorInventoryCount] 
    @startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
BEGIN
declare  @startDateVar DateTime, @endDateVar DateTime, @registrationNumberVar nvarchar(256)
set @startDateVar = @startDate
set @endDateVar = @endDate
set @registrationNumberVar = @registrationNumber

select 
v.Number CollectorRegNumber,
case v.IsActive when 1 then 'Active' else 'Inactive' end CollectorStatus,
v.BusinessName CollectorLegalName,
v.OperatingName CollectorOperatingName,
p.ShortName ClaimPeriod,
c.Status,
tires.PLT,
tires.MT,
tires.AGLS,
tires.IND,
tires.SOTR,
tires.MOTR,
tires.LOTR,
tires.GOTR
from Claim c 
left outer join
(
	select 
	cto.ClaimId,
	SUM(case when itemid = 65 then quantity end)  'PLT',
	SUM(case when itemid = 66 then quantity end)  'MT',
	SUM(case when itemid = 67 then quantity end)  'AGLS',
	SUM(case when itemid = 68 then quantity end)  'IND',
	SUM(case when itemid = 69 then quantity end)  'SOTR',
	SUM(case when itemid = 70 then quantity end)  'MOTR',
	SUM(case when itemid = 71 then quantity end)  'LOTR',
	SUM(case when itemid = 72 then quantity end)  'GOTR'	
	from ClaimReuseTires cto	
	group by cto.ClaimId
) as tires on tires.ClaimId=c.ID
inner join Period p
on c.ClaimPeriodID=p.ID
inner join Vendor v
on c.ParticipantID=v.ID
where 
(@startDateVar is null or @startDateVar <= p.StartDate or @startDateVar <= p.EndDate) 
AND (@endDateVar is null or p.StartDate  <= @endDateVar)  
AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
order by v.Number, p.StartDate
END
