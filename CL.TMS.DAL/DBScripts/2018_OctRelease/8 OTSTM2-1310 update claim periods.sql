select [PeriodType]
      ,[ShortName]
      ,[PeriodDesc]
      ,[StartDate]
      ,dateadd(second, -1, DATEAdd(month, 2, [StartDate]))	  
      ,[SubmitEnd]			  	  
	  ,DATEDIFF(month, [SubmitEnd], [StartDate])
	  ,[EndDate]
      ,[ShowStart]
      ,[SubmitStart]
      ,[SubmitEnd]
	  ,[EnableSubmit]
      ,[ShowPeriod]
      ,[IsSpecialEvent]
	   from Period p 
where 
p.StartDate>='2018-12-01' and
p.PeriodType in (4,5)
--and DATEDIFF(month, [SubmitEnd], [StartDate]) = -2


update Period set [SubmitEnd]=dateadd(second, -1, DATEAdd(month, 2, [StartDate]))
where 
StartDate>='2018-12-01' and
PeriodType in (4,5)
