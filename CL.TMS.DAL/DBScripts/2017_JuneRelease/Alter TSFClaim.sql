--drop the dependency in constraints 

ALTER TABLE [dbo].[TSFClaim] DROP CONSTRAINT [DF__TSFClaimS__IsTax__2116E6DF];

ALTER TABLE "TSFClaim"
	DROP COLUMN "IsTaxOverride";

ALTER TABLE "TSFClaim"
	ADD "IsTaxApplicable" TINYINT NULL;

ALTER TABLE [dbo].[TSFClaim] ADD  CONSTRAINT [DF__TSFClaimS__IsTax__0]  DEFAULT ('0') FOR [IsTaxApplicable];
go
update TSFClaim set IsTaxApplicable=0;
