﻿USE [tmdb]
GO

/****** Object:  Table [dbo].[ClaimSupportingDocument]    Script Date: 5/16/2017 10:56:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClaimSupportingDocument](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimId] [int] NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[TypeCode] [nvarchar](50) NOT NULL,
	[DefinitionValue] [int] NOT NULL,
	[SelectedOption] [nvarchar](50) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_ClaimSupportingDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClaimSupportingDocument]  WITH CHECK ADD  CONSTRAINT [FK_ClaimSupportingDocument_Claim] FOREIGN KEY([ClaimId])
REFERENCES [dbo].[Claim] ([ID])
GO

ALTER TABLE [dbo].[ClaimSupportingDocument] CHECK CONSTRAINT [FK_ClaimSupportingDocument_Claim]
GO
