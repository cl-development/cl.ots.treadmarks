﻿use tmdb

IF EXISTS (select * from [AppResource]  where ResourceMenu='Remittance' and ResourceScreen like 'TSF Remittance%' and ResourceScreen!='TSF Remittances')
BEGIN
	update [AppResource] set ResourceScreen = 'TSF Remittance Summary' 
	where ResourceMenu='Remittance' and ResourceScreen like 'TSF Remittance%' and ResourceScreen!='TSF Remittances'
END

