﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1085 1. remove GP from staff financial integration 
            :   2. add admin financial integration section
Purpose     :   		
Created By  :   Joe Zhou Jan 17th, 2018
*****************************************************************/
USE [tmdb]
GO

-- remove GP from role screen
update [AppResource] set Name='Staff Financial Integration' ,  resourcescreen=SUBSTRING(resourceScreen,4,len(resourcescreen)) where ResourceMenu='Financial Integration' and ResourceLevel=1
update [AppResource] set Name = SUBSTRING(name,4,len(name)) , resourcescreen=SUBSTRING(resourceScreen,4,len(resourcescreen))
where name like 'GP%'

go

--add admin finical integration category
IF NOT EXISTS (SELECT * FROM TypeDefinition WHERE Category='AdminFICategory')--if first line not exists, then insert whole batch
BEGIN
	INSERT INTO TypeDefinition 
		([Name] ,								[Code] ,			[Description] ,															[Category] ,		[DefinitionValue] ,[DisplayOrder])
		VALUES 
		('Stewards - Tire Stewardship Fees',	'AdminFISteward',	'Admin Financial Integration Stewards - Tire Stewardship Fees',			'AdminFICategory',	1 ,					1),
		('Collectors - Collection Allowances',	'AdminFICollector',	'Admin Financial Integration Collectors - Collection Allowances',		'AdminFICategory',	2 ,					2),
		('Haulers - Transportation Premiums',	'AdminFIHauler',	'Admin Financial Integration Haulers - Transportation Premiums',		'AdminFICategory',	3 ,					3),		
		('Processors - Processing Incentives',	'AdminFIProcessor',	'Admin Financial Integration Processors - Processing Incentives',		'AdminFICategory',	4 ,					4),
		('RPMs - Manufacturing Incentives',		'AdminFIRPM',		'Admin Financial Integration RPMs - Manufacturing Incentives',			'AdminFICategory',	5 ,					5)
END
GO

--user access permission control
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Financial Integration' and ResourceScreen = 'Admin/Financial Integration' and Name = 'Admin/Financial Integration')
BEGIN
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

insert into [AppResource]	(Name,					ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES	
			('Admin/Financial Integration',			'Admin/Financial Integration',	'Admin/Financial Integration',		0,				350000,			@CreateBy,	@dt,		@dt)
END
go

--create permission for new appResource Admin/Financial Integration

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int, @iEditSave int
Declare @getRoleIds cursor
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]
set @newResourceID=(select id from AppResource where Name = 'Admin/Financial Integration' and ResourceMenu = 'Admin/Financial Integration' and ResourceScreen = 'Admin/Financial Integration')
select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
	(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
	WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
	BEGIN
		if (@RoleId = @superAdminID) 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
			end 
		else 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iNoAccess)
			end
	END
	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go

-- create FIAccount table


IF OBJECT_ID(N'[dbo].[FIAccount]',N'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[FIAccount](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[AdminFICategoryID] [int] not null, --value get from TypeDefinition Category='AdminFICategory'
		[AccountLabel] [varchar](500) not NULL,
		[AccountNumber] [varchar](100) not NULL,
		[DisplayOrder] [int],
		[AccountDescription] [varchar](max) NULL,	
		[MappingNote] [varchar](max) null,		
		[ModifiedDate] [datetime] NULL,
		[ModifiedByID] [bigint] NULL,
		[RowVersion] [timestamp] NOT NULL,
	 CONSTRAINT [PK_FIAccount] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	
	ALTER TABLE [dbo].[FIAccount]  WITH CHECK ADD  CONSTRAINT [FK_FIAccount_ModifiedByUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])

	ALTER TABLE [dbo].[FIAccount] CHECK CONSTRAINT [FK_FIAccount_ModifiedByUser]
END

go

-- insert FIAccount initial data
IF NOT EXISTS (SELECT * FROM FIAccount WHERE [AdminFICategoryID]=1)--if first line not exists, then insert whole records
BEGIN
	INSERT INTO FIAccount 
		([AdminFICategoryID],[AccountLabel],[AccountNumber],[DisplayOrder],[AccountDescription],[MappingNote])
		VALUES 
		(1,'Stewards Receivable','1220-00-00-00',1,'Stewards Receivable','Total TSF Due + Total Payment Adjustments (when positive)'),
		(1,'Class 1','4011-10-00-00',2,'PLT Class 1','The TSF Due output as Credit'),
		(1,'Class 2','4016-20-00-00',3,'MT Class 2','The TSF Due output as Credit'),
		(1,'Class 3','4071-30-00-00',4,'OTR Class 3','The TSF Due output as Credit'),
		(1,'Class 4','4072-30-00-00',5,'OTR Class 4','The TSF Due output as Credit'),
		(1,'Class 5','4073-30-00-00',6,'OTR Class 5','The TSF Due output as Credit'),
		(1,'Class 6','4074-30-00-00',7,'OTR Class 6','The TSF Due output as Credit'),
		(1,'Class 7','4075-30-00-00',8,'OTR Class 7','The TSF Due output as Credit'),
		(1,'Class 8','4076-30-00-00',9,'OTR Class 8','The TSF Due output as Credit'),
        (1,'Class 9','4077-30-00-00',10,'OTR Class 9','The TSF Due output as Credit'),
        (1,'Class 10','4078-30-00-00',11,'OTR Class 10','The TSF Due output as Credit'),
        (1,'Class 11','4079-30-00-00',12,'OTR Class 11','The TSF Due output as Credit'),
        (1,'Class 12','4080-30-00-00',13,'Solid&Resilient Class 12','The TSF Due output as Credit'),
        (1,'Class 13','4081-30-00-00',14,'Solid&Resilient Class 13','The TSF Due output as Credit'),
        (1,'Class 14','4082-30-00-00',15,'Solid&Resilient Class 14','The TSF Due output as Credit'),
        (1,'Class 15','4083-30-00-00',16,'Solid&Resilient Class 15','The TSF Due output as Credit'),
        (1,'Class 16','4084-30-00-00',17,'Solid&Resilient Class 16','The TSF Due output as Credit'),
        (1,'Class 17','4085-30-00-00',18,'Solid&Resilient Class 17','The TSF Due output as Credit'),
        (1,'Class 18','4086-30-00-00',19,'Solid&Resilient Class 18','The TSF Due output as Credit'),
        (1,'Internal Payment Adjustments','4005-90-00-99',20,'Overall adjustment - Payments Internal Adjustments','If -ve adjustment: abs(amount) output as Credit  Note: output absolute value'),
        (1,'Audit Payment Adjustments','4004-90-00-99',21,'Audit Assessment - Payments Internal Adjustments','If -ve adjustment: abs(amount) output as Credit  Note: output absolute value'),
        (1,'Audit Penalties / TSF Penalty','4065-90-00-00',22,'Audit Penalty will - Payments Internal Adjustments','If -ve adjustment: abs(amount) output as Credit  Note: output absolute value'),
        (1,'Tax Receivable ','2200-00-00-00',23,'Tax Payable (HST)','HST(Tax) amount output as Credit'),
        (2,'Collector Payable','2000-00-00-00',1,'Accounts (Collector) Payable','Invoice Amount including Tax'),
        (2,'PLT','4110-10-20-40',2,'CA - PLT','On Road Premium - PLT'),
        (2,'MT','4120-20-20-40',3,'CA - MT','On Road Premium - MT'),
        (2,'AG/LS','4130-30-20-40',4,'CA - AG/LS','Off Road Premium - AG/LS'),
        (2,'IND','4140-40-20-40',5,'CA - IND','Off Road Premium - IND'),
        (2,'SOTR','4150-50-20-40',6,'CA - SOTR','Off Road Premium - SOTR'),
        (2,'MOTR','4160-60-20-40',7,'CA - MOTR','Off Road Premium - MOTR'),
        (2,'LOTR','4170-70-20-40',8,'CA - LOTR','Off Road Premium - LOTR'),
        (2,'GOTR','4180-80-20-40',9,'CA - GOTR','Off Road Premium - GOTR'),
        (2,'Internal Payment Adjustments','4195-90-20-40',10,'TI- TM Adjustments','If +ve adjustment: amount output as Debit.'),
        (2,'PLT','4115-10-20-40',11,'CA - PLT HST','HST On Road Premium - PLT'),
        (2,'MT','4125-20-20-40',12,'CA - MT HST','HST On Road Premium - MT'),
        (2,'AG/LS','4135-30-20-40',13,'CA - AG/LS HST','HST Off Road Premium - AG/LS'),
        (2,'IND','4145-40-20-40',14,'CA - IND HST','HST Off Road Premium - IND'),
        (2,'SOTR','4155-50-20-40',15,'CA - SOTR HST','HST Off Road Premium - SOTR'),
        (2,'MOTR','4165-60-20-40',16,'CA - MOTR HST','HST Off Road Premium - MOTR'),
        (2,'LOTR','4175-70-20-40',17,'CA - LOTR HST','HST Off Road Premium - LOTR'),
        (2,'GOTR','4185-80-20-40',18,'CA - GOTR HST','HST Off Road Premium - GOTR'),
        (2,'Internal Payment Adjustments',' 4196-90-20-40',19,'TI- TM Adjustment HST','If tax on +ve adjustment: tax amount output as Debit.'),
        (3,'Hauler Payable','2000-00-00-00',1,'Accounts (Hauler) Payable','Invoice Amount including Tax'),
        (3,'PLT','4366-10-30-40',2,'TI- Premium PLT','On Road Premium - PLT'),
        (3,'MT','4367-20-30-40',3,'TI- Premium MT','On Road Premium - MT'),
        (3,'AG/LS','4368-30-30-40',4,'TI- Premium AG/LS','Off Road Premium - AG/LS'),
        (3,'IND','4369-40-30-40',5,'TI- Premium IND','Off Road Premium - IND'),
        (3,'SOTR','4371-50-30-40',6,'TI- Premium SOTR','Off Road Premium - SOTR'),
        (3,'MOTR','4372-60-30-40',7,'TI- Premium MOTR','Off Road Premium - MOTR'),
        (3,'LOTR','4373-70-30-40',8,'TI- Premium LOTR','Off Road Premium - LOTR'),
        (3,'GOTR','4374-80-30-40',9,'TI- Premium GOTR','Off Road Premium - GOTR'),
        (3,'MOTR','4376-60-30-40',10,'TI-DOT MOTR','MOTR DOT Premium'),
        (3,'LOTR','4377-70-30-40',11,'TI-DOT LOTR','LOTR DOT Premium'),
        (3,'GOTR','4378-80-30-40',12,'TI-DOT GOTR','GOTR DOT Premium'),
        (3,'Internal Payment Adjustments','4475-90-30-40',13,'TI- TM Adjustments','If +ve adjustment: amount output as Debit.'),
        (3,'PLT','4382-10-30-40',14,'TI- PLT HST','HST On Road Premium - PLT'),
        (3,'MT','4384-20-30-40',15,'TI- MT HST','HST On Road Premium - MT'),
        (3,'AG/LS','4387-30-30-40',16,'TI- AG/LS HST','HST Off Road Premium - AG/LS'),
        (3,'IND','4389-40-30-40',17,'TI- IND HST','HST Off Road Premium - IND'),
        (3,'SOTR','4398-50-30-40',18,'TI- SOTR HST','HST Off Road Premium - SOTR'),
        (3,'MOTR','4401-60-30-40',19,'TI- MOTR HST','HST Off Road Premium - MOTR'),
        (3,'LOTR','4403-70-30-40',20,'TI-LOTR HST','HST Off Road Premium - LOTR'),
        (3,'GOTR','4406-80-30-40',21,'TI- GOTR HST','HST Off Road Premium - GOTR'),
        (3,'DOT-OTR','4390-90-30-40',22,'TI- DOT OTR HST','Combined tax from DOT MOTR , DOT LOTR and DOT GOTR'),
        (3,'Internal Payment Adjustments','4470-90-30-40',23,'TI- TM Adjustment HST','If tax on +ve adjustment: tax amount output as Debit.'),
        (4,'Processor Payable','2000-00-00-00',1,'Accounts (Processor) Payable','Invoice Amount'),
        (4,'PLT','4241-10-40-40',2,'PI- Stabilization Pyt ON ROAD PLT ','TI for PLT tires'),
        (4,'MT','4242-20-40-40',3,'PI- Stabilization Pyt ON ROAD MT','TI for MT tires'),
        (4,'AG.LS','4243-30-40-40',4,'PI- Stabilization Pyt OFF ROAD AG&LS','TI for AG&LS tires'),
        (4,'IND','4244-40-40-40',5,'PI- Stabilization Pyt OFF ROAD IND','TI for IND tires'),
        (4,'SOTR','4361-50-40-40',6,'PI- Stabilization Pyt OFF ROAD SOTR','TI for SOTR tires'),
        (4,'MOTR','4362-60-40-40',7,'PI- Stabilization Pyt OFF ROAD MOTR','TI for MOTR tires'),
        (4,'LOTR','4363-70-40-40',8,'PI- Stabilization Pyt OFF ROAD LOTR','TI for LOTR tires'),
        (4,'GOTR','4364-80-40-40',9,'PI- Stabilization Pyt OFF ROAD GOTR','TI for GOTR tires'),
        (4,'PLT','4246-10-40-40',10,'PI - PLT','PI - PLT'),
        (4,'MT','4248-20-40-40',11,'PI - MT','PI - MT'),
        (4,'AG/LS','4251-30-40-40',12,'PI - AG/LS','PI - AG/LS'),
        (4,'IND','4253-40-40-40',13,'PI - IND','PI - IND'),
        (4,'SOTR','4256-50-40-40',14,'PI - SOTR','PI - SOTR'),
        (4,'MOTR','4258-60-40-40',15,'PI - MOTR','PI - MOTR'),
        (4,'LOTR','4261-70-40-40',16,'PI - LOTR','PI - LOTR'),
        (4,'GOTR','4263-80-40-40',17,'PI - GOTR','PI - GOTR'),
        (4,'Internal Payment Adjustments','4365-90-40-40',18,'PI TM Adjustments','+ve Payment Adjustment'),
        (5,'Manufacturer Payable','2000-00-00-00',1,'Accounts (Manufacturer) Payable','Invoice Amount'),
        (5,'PLT','4201-10-50-40',2,'MI- Stabilization Pyt ON ROAD PLT ','MI for PLT tires'),
        (5,'MT','4203-20-50-40',3,'MI- Stabilization Pyt ON ROAD MT','MI for MT tires'),
        (5,'AG/LS','4206-30-50-40',4,'MI- Stabilization Pyt OFF ROAD AG&LS','MI for AG&LS tires'),
        (5,'IND','4208-40-50-40',5,'MI- Stabilization Pyt OFF ROAD IND','MI for IND tires'),
        (5,'SOTR','4211-50-50-40',6,'MI- Stabilization Pyt OFF ROAD SOTR','MI for SOTR tires'),
        (5,'MOTR','4213-60-50-40',7,'MI- Stabilization Pyt OFF ROAD MOTR','MI for MOTR tires'),
        (5,'LOTR','4216-70-50-40',8,'MI- Stabilization Pyt OFF ROAD LOTR','MI for LOTR tires'),
        (5,'GOTR','4218-80-50-40',9,'MI- Stabilization Pyt OFF ROAD GOTR','MI for GOTR tires'),
        (5,'Internal Payment Adjustments','4210-90-50-40',10,'MI TM Adjustments','+ve Payment Adjustment')		
End
go

-- reset display order of admin menu items
update [AppResource] set DisplayOrder='310000' where DisplayOrder='260000'
update [AppResource] set DisplayOrder='330000' where DisplayOrder='250000'
update [AppResource] set DisplayOrder='350000' where DisplayOrder='255000'
update [AppResource] set DisplayOrder='370000' where DisplayOrder='270000'

update [AppResource] set DisplayOrder='390000' where DisplayOrder='240000'
update [AppResource] set DisplayOrder='391000' where DisplayOrder='241000'
update [AppResource] set DisplayOrder='391100' where DisplayOrder='241100'

update [AppResource] set DisplayOrder='410000' where DisplayOrder='220000'

update [AppResource] set DisplayOrder='990000' where DisplayOrder='280000'
go