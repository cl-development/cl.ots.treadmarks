﻿use tmdb

IF EXISTS (select * from [Report] where ReportName ='TSF Extract - In Batch Only Report (GP)')
BEGIN
	update [Report] set ReportName = 'TSF Extract - In Batch Only Report' 
	where ReportName ='TSF Extract - In Batch Only Report (GP)'
END

