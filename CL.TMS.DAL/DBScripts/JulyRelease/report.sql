USE [tmdb];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Report]([ID], [ReportName], [ReportCategoryID])
SELECT 21, N'Processor Disposition of Residual', 4 UNION ALL
SELECT 20, N'RPM Data', 5
COMMIT;
RAISERROR (N'[dbo].[Report]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

