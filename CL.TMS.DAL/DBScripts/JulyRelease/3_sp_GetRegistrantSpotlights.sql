USE [tmdb]
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitString]    Script Date: 7/13/2016 9:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnSplitSpotlight] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
	IF(LEN(LTRIM(RTRIM(@string))) > 0)
	BEGIN
		INSERT INTO @output (splitdata)  		
		VALUES(LTRIM(RTRIM(@string)))
	END

    WHILE @start < LEN(@string) + 1 
	BEGIN 
		IF @end = 0  
		BEGIN
			SET @end = LEN(@string) + 1
		END
       
		IF LEN(SUBSTRING(@string, @start, @end - @start)) >= 2
		BEGIN
			INSERT INTO @output (splitdata)  
			VALUES(SUBSTRING(@string, @start, @end - @start)) 
		END
		SET @start = @end + 1 
		SET @end = CHARINDEX(@delimiter, @string, @start)        
	END 
    RETURN 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetRegistrantSpotlights]    Script Date: 7/7/2016 12:18:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetRegistrantSpotlights] 
	@keyWords nvarchar(255)
AS
BEGIN
	DECLARE @wordCount int = (SELECT count(*) FROM [dbo].[fnSplitSpotlight](@keyWords, ' '));
	DECLARE @moduleType nvarchar(50);

	DECLARE @tmp TABLE (
		ID int,
		[Number] nvarchar(50),
		LegalName nvarchar(65),
		OperatingName nvarchar(65),
		ModuleType int,
		Active bit
	);

	DECLARE @result TABLE (
		ID int,
		[Number] nvarchar(50),
		LegalName nvarchar(65),
		OperatingName nvarchar(65),
		ModuleType int,
		Active bit
	);

	DECLARE MY_CURSOR CURSOR 
	  LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
	SELECT DISTINCT ModuleType
	FROM @tmp;

	INSERT INTO @tmp ([ID], [Number], LegalName, OperatingName, ModuleType, Active)
	SELECT *
	FROM
	(
		SELECT 
			v.ID 'ID',
			v.Number 'Number',
			v.BusinessName 'LegalName',
			v.OperatingName	'OperatingName',
			v.VendorType 'ModuleType',
			v.IsActive 'Active'
		FROM [dbo].[Vendor] v 
		WHERE ((
			SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount and @wordCount > 0) OR
		v.Keywords like '%'+@keyWords+'%'

		UNION  

		SELECT 
			c.ID 'ID',
			c.RegistrationNumber 'Number',
			c.BusinessName 'LegalName',
			c.OperatingName	'OperatingName',
			c.CustomerType 'ModuleType',
			c.IsActive 'Active'
		FROM [dbo].[Customer] c
		WHERE ((
			SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount AND @wordCount > 0) OR
		c.Keywords like '%'+@keyWords+'%'
	) main
	ORDER BY main.ModuleType, main.Active desc, main.Number;


	OPEN MY_CURSOR
	FETCH NEXT FROM MY_CURSOR INTO @moduleType
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		INSERT INTO @result
		SELECT TOP (@pageLimit) t.*
		FROM @tmp t
		WHERE t.ModuleType = @moduleType
		ORDER BY t.Active desc, t.Number;

		FETCH NEXT FROM MY_CURSOR INTO @moduleType
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR

	SELECT *
	FROM @result;
END
