﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_ProcessorVolumeReport]    Script Date: 5/1/2018 10:00:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[sp_Rpt_ProcessorVolumeReport] @startDate datetime, @endDate datetime
AS
BEGIN
  -- with new schema change
  SELECT
    t.ReportingPeriod,
    t.RegNo,
	t.RateType, --OTSTM2-1225
    t.BusinessName RegName,
    t.Reg_Status Status,
    t.RecordStatus,
    t.Product,
    t.Destination,
    t.Description,
    ROUND(SUM(t.Weight),4) Weight,
    ROUND(SUM(t.Amount),2) Amount

  FROM (SELECT
    p.StartDate ReportingPeriod,
    v.Number RegNo,
    v.BusinessName,
    CASE v.IsActive
      WHEN 1 THEN 'A'
      ELSE 'I'
    END Reg_Status,
    c.Status RecordStatus,

    (SELECT TOP 1
      i.ShortName
    FROM TransactionItem ti
    INNER JOIN item i
      ON i.id = ti.ItemID
    WHERE ti.TransactionId = t.id) 
    Product,

    (SELECT TOP 1
      i.Description
    FROM TransactionItem ti
    INNER JOIN item i
      ON i.id = ti.ItemID
    WHERE ti.TransactionId = t.id)
    Description,

    CASE
      WHEN a.Province IS NOT NULL THEN concat(a.Province, ', ', a.country)
      ELSE concat(ca.Province, ', ', ca.country)
    END Destination,
    SUM(ROUND(coalesce(cp.ActualWeight, 0),4)) Weight,
    SUM(ROUND(coalesce(cp.ActualWeight, 0) * cp.Rate,2)) Amount,
	CASE --OTSTM2-1225
		WHEN rt.IsSpecificRate = 0 or rt.IsSpecificRate is null THEN 'Global'
		ELSE 'Specific' 
	END AS 'RateType'

  FROM claim c

  INNER JOIN ClaimPaymentDetail cp
    ON cp.ClaimID = c.ID
  INNER JOIN [Transaction] t
    ON cp.TransactionId = t.ID
  INNER JOIN Period p
    ON c.ClaimPeriodID = p.ID
  INNER JOIN Vendor v
    ON c.ParticipantID = v.id
  LEFT OUTER JOIN CompanyInfo cm
    ON t.CompanyInfoId = cm.id
  LEFT OUTER JOIN Address ca
    ON ca.id = cm.AddressId
  LEFT OUTER JOIN Vendor vi
    ON t.IncomingId = vi.ID
  LEFT JOIN (SELECT
    a.id,
    va.VendorID,
    a.City,
    a.Province,
    a.Country
  FROM VendorAddress va
  LEFT JOIN Address a
    ON va.AddressID = a.ID
  WHERE a.AddressType = 1) a
    ON vi.id = a.VendorID
  left join VendorRate vr on vr.VendorId=v.ID --OTSTM2-1225
  left join RateTransaction rt on rt.ID=vr.RateTransactionId and p.StartDate >= rt.EffectiveStartDate and p.StartDate <= rt.EffectiveEndDate --OTSTM2-1225

  WHERE t.TransactionTypeCode = 'SPS'
  AND cp.PaymentType = 7
  AND (@startDate <= p.StartDate
  OR @startDate IS NULL)
  AND (@endDate >= p.StartDate
  OR @endDate IS NULL)

  GROUP BY p.StartDate,
           CASE
             WHEN a.Province IS NOT NULL THEN concat(a.Province, ', ', a.country)
             ELSE concat(ca.Province, ', ', ca.country)
           END,
           c.Status,
           v.IsActive,
           v.Number,
           v.BusinessName,
           t.id,
		   rt.IsSpecificRate) t --OTSTM2-1225

  GROUP BY t.ReportingPeriod,
           t.RegNo,
           t.BusinessName,
           t.Reg_Status,
           t.RecordStatus,
           t.Product,
           t.Description,
           t.Destination,
		   t.RateType --OTSTM2-1225

  ORDER BY t.RegNo, t.ReportingPeriod, t.Product;

END