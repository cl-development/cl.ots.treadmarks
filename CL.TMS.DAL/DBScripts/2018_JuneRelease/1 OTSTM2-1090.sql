﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1090 New menu of Dashboard
Purpose	    :   1. add Dashboard menu to appresource
            :   2. create permission for new AppResource Menu Dashboard   		
Created By  :   Joe Zhou April 26th, 2018
*****************************************************************/
USE [tmdb]
GO

--user Dashboard menu item Display Order set as 400000 (after company branding before report) 
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard' and Name = 'Dashboard')

BEGIN
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

insert into [AppResource]	(Name,		ResourceMenu,		ResourceScreen,		ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES	

			('Dashboard',				'Dashboard',		'Dashboard',			0,				80000,			@CreateBy,	@dt,		@dt),
			('StaffDashboard',			'Dashboard',		'Dashboard',			1,				81000,			@CreateBy,	@dt,		@dt),
			('Announcements',			'Dashboard',		'Dashboard',		    2,				82100,			@CreateBy,	@dt,		@dt),
			('Year to Date TSF Dollar',	'Dashboard',		'Dashboard',		    2,				83100,			@CreateBy,	@dt,		@dt),
			('Year to Date TSF Units',	'Dashboard',		'Dashboard',		    2,				84100,			@CreateBy,	@dt,		@dt),
			('TSF Status Overview',		'Dashboard',		'Dashboard',		    2,				85100,			@CreateBy,	@dt,		@dt),		
			('Top Ten Stewards',		'Dashboard',		'Dashboard',		    2,				86100,			@CreateBy,	@dt,		@dt)
		--,  for OTS don't have STC events
		--	('STC Events',				'Dashboard',		'Dashboard',		    2,				87100,			@CreateBy,	@dt,		@dt)
		
END
go

--create permission for new appResource Dashboard  Integration and Admin/Announcements

Declare @superAdminID int,  @iCounter int, @RoleId int, @iNoAccess int, @iReadOnly int, @iEditSave int
Declare @getRoleIds cursor
Declare @newResourceID int, @newAnnounce int, @newMetric1 int, @newMetric2 int, @newMetric3 int, 
		@newMetric4 int, @newSTC int,@newDashboard int, @newAdminAnnouncements int

set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]

set @newResourceID=(select id from AppResource	where Name = 'Dashboard'				and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newDashboard=(select id from AppResource	where Name = 'StaffDashboard'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newAnnounce=(select id from AppResource	where Name = 'Announcements'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric1=(select id from AppResource		where Name = 'Year to Date TSF Dollar'	and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric2=(select id from AppResource		where Name = 'Year to Date TSF Units'	and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric3=(select id from AppResource		where Name = 'TSF Status Overview'		and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric4=(select id from AppResource		where Name = 'Top Ten Stewards'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')

set @newAdminAnnouncements=(select id from AppResource	where Name = 'Admin/Announcements' and ResourceMenu = 'Admin/Announcements' and ResourceScreen = 'Admin/Announcements')

--for OTS don't have STC events
--set @newSTC=(select id from AppResource			where Name = 'STC Events'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')

select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iReadOnly = Id from AppPermission where Name = 'ReadOnly'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
		BEGIN
			if (@RoleId = @superAdminID) 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newDashboard, @iEditSave)
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAnnounce, @iEditSave)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric1, @iEditSave)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric2, @iEditSave)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric3, @iEditSave)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric4, @iEditSave)
				
				end 
			else 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iReadOnly)
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newDashboard,  @iReadOnly)
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAnnounce,   @iReadOnly)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric1,    @iReadOnly)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric2,    @iReadOnly)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric3,    @iReadOnly)
     				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric4,    @iReadOnly)
				end
		END
	else 
		begin
			if (@RoleId = @superAdminID) 
				begin 
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newResourceID
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newDashboard
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newAnnounce
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newMetric1
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newMetric2
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newMetric3
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newMetric4
				end 
			else 
				begin 
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newResourceID
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newDashboard
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newAnnounce
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newMetric1
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newMetric2
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newMetric3
					update [RolePermission] set AppPermissionId=@iReadOnly where RoleId=@RoleId and AppResourceId=@newMetric4
				end
		end
	
	if not exists (SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newAdminAnnouncements and rp.RoleId = @RoleId)
		begin
			if (@RoleId = @superAdminID)
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAdminAnnouncements, @iEditSave)
				end
			else 
				begin
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAdminAnnouncements, @iNoAccess)
				end
		end
	else
		begin
			if (@RoleId = @superAdminID)
				begin 
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newAdminAnnouncements
				end
			else 
				begin
					update [RolePermission] set AppPermissionId=@iNoAccess  where RoleId=@RoleId and AppResourceId=@newAdminAnnouncements
				end
		end
	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go

-- OTSTM2-1081 announcements admin menu
update [AppResource] set DisplayOrder = 900000 where Name = 'Admin/Reports' and ResourceMenu = 'Admin/Reports' and ResourceScreen = 'Admin/Reports'
update [AppResource] set DisplayOrder = 800000 where Name = 'Admin/Announcements' and ResourceMenu = 'Admin/Announcements' and ResourceScreen = 'Admin/Announcements'
IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Site.ApplicationInstance')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Site.ApplicationInstance', 'OTS');
END
ELSE 
Begin
	Update AppSetting set [Value]='OTS' where [Key]='Site.ApplicationInstance';
End
go