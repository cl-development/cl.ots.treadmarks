﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using CL.TMS.IRepository.Claims;
using CL.TMS.Common.Enum;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.Framework.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Repository.System;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.Configuration;

namespace CL.TMS.SystemServices
{
    public class ConfigurationsServices : IConfigurationsServices
    {
        private readonly ConfigurationsBO configurationsBO;

        public ConfigurationsServices(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator, IMessageRepository messageRepository, IConfigurationsRepository configurationsRepository, ISettingRepository settingRepository)
        {
            this.configurationsBO = new ConfigurationsBO(userRepository, vendorRepository, eventAggregator, messageRepository, configurationsRepository, settingRepository);
        }

        public PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return configurationsBO.LoadRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return configurationsBO.LoadPIRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<RateListViewModel, int> LoadTIPayeeList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return configurationsBO.LoadTIPayeeList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public RateDetailsVM LoadRateDetailsByTransactionID(int rateTransactionID, int category,bool isSpecific)
        {
            try
            {
                return configurationsBO.LoadRateDetailsByTransactionID(rateTransactionID, category,isSpecific);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public bool AddNewRate(RateDetailsVM newRate, long userId, int category)
        {
            try
            {
                return configurationsBO.AddNewRate(newRate, userId, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool updateRate(RateDetailsVM rateVM, long userId)
        {
            try
            {
                return configurationsBO.updateRate(rateVM, userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool RemoveRateTransaction(int rateTransactionID, int category)
        {
            try
            {
                return configurationsBO.RemoveRateTransaction(rateTransactionID, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsFutureRateExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureRateExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsFutureRateExists(int category,bool isSpecific)
        {
            try
            {
                return configurationsBO.IsFutureRateExists(category,isSpecific);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsFutureCollectorRateExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureCollectorRateExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool IsCollectorRateTransactionExists(int category)
        {
            try
            {
                return configurationsBO.IsCollectorRateTransactionExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool IsFutureCollectorRateTransactionExists(int category)
        {
            try
            {
                return configurationsBO.IsFutureCollectorRateTransactionExists(category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }


        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            try
            {
                return configurationsBO.AddTransactionNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public InternalNoteViewModel AddTransactionNote(string transactionID, string notes)
        {
            try
            {
                return configurationsBO.AddTransactionNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public InternalNoteViewModel AddPayeeNote(int transactionID, string notes)
        {
            try
            {
                return configurationsBO.AddPayeeNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            try
            {
                return configurationsBO.LoadRateTransactionNoteByID(rateTransactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> LoadTIPayeeNotesByID(int rateTransactionID)
        {
            try
            {
                return configurationsBO.LoadTIPayeeNotesByID(rateTransactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return configurationsBO.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<InternalNoteViewModel> ExportPayeeNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return configurationsBO.ExportPayeeNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return configurationsBO.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public AppSettingsVM LoadAppSettings()
        {
            {
                try
                {
                    return configurationsBO.LoadAppSettings();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }

        public List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType)
        {
            {
                try
                {
                    return configurationsBO.LoadAppSettingNotesByType(noteType);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }

        public bool updateAppSettings(AppSettingsVM appSettingsVM, long userId)
        {
            {
                try
                {
                    return configurationsBO.updateAppSettings(appSettingsVM, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }


        #region // Account Thresholds
        public AccountThresholdsVM LoadAccountThresholds()
        {
            {
                try
                {
                    return configurationsBO.LoadAccountThresholds();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }
        public bool UpdateAccountThresholds(AccountThresholdsVM vm)
        {
            try
            {
                return configurationsBO.UpdateAccountThresholds(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        #endregion

        #region Transaction Thresholds
        public TransactionThresholdsVM LoadTransactionThresholds()
        {
            try
            {
                return configurationsBO.LoadTransactionThresholds();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            try
            {
                return configurationsBO.UpdateTransactionThresholds(transactionThresholdsVM);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        #endregion

        #region TCR Service Threshold
        public PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO)
        {
            try
            {
                return configurationsBO.LoadTCRServiceThresholds(paramDTO);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public TCRServiceThresholdListViewModel LoadTCRServiceThresholdList()
        {
            try
            {
                return configurationsBO.LoadTCRServiceThresholdList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            {
                try
                {
                    return configurationsBO.AddTCRServiceThreshold(vm, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            {
                try
                {
                    return configurationsBO.UpdateTCRServiceThreshold(vm, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public bool RemoveTCRServiceThresohold(int id, long userId)
        {
            {
                try
                {
                    return configurationsBO.RemoveTCRServiceThresohold(id, userId);
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return false;
            }
        }

        public IEnumerable<TCRListViewModel> ExportTCRServiceThresholds()
        {
            {
                try
                {
                    return configurationsBO.ExportTCRServiceThresholds();
                }
                catch (Exception ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                    if (rethrow)
                    {
                        throw;
                    }
                }
                return null;
            }
        }
        #endregion

    }
}
