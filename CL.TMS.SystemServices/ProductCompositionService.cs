﻿using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.SystemServices
{
    public class ProductCompositionService : IProductCompositionServices
    {
        private readonly ProductCompositionBO  productCompositionBO;
      
        public ProductCompositionService(IUserRepository userRepository, IProductCompositionRepository productCompositionRepository)
        {
            productCompositionBO = new ProductCompositionBO(userRepository, productCompositionRepository);
        }
        public PaginationDTO<RecoverableMaterialsVM, int> LoadRecoverableMaterials(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return productCompositionBO.LoadRecoverableMaterials(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<MaterialCompositionsVM, int> LoadMaterialCompositions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return productCompositionBO.LoadMaterialCompositions(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool CheckMaterialNameUnique(string name)
        {
            try
            {
                return productCompositionBO.CheckMaterialNameUnique(name);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool SaveRecoverableMaterial(RecoverableMaterialsVM vm)
        {
            try
            {
                return productCompositionBO.SaveRecoverableMaterial(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        
        public bool SaveMaterialComposition(MaterialCompositionsVM vm)
        {
            try
            {
                return productCompositionBO.SaveMaterialComposition(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        #region Estimated Weights
        public PaginationDTO<RecoveryEstimatedWeightListViewModel, int> LoadRecoveryEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return productCompositionBO.LoadRecoveryEstimatedWeightList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<SupplyEstimatedWeightViewModel, int> LoadSupplyEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            try
            {
                return productCompositionBO.LoadSupplyEstimatedWeightList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public bool AddNewRecoveryEstimatedWeight(WeightDetailsVM newWeight, long userId, int category)
        {
            try
            {
                return productCompositionBO.AddNewRecoveryEstimatedWeight(newWeight, userId, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public bool updateRecoveryEstimatedWeight(WeightDetailsVM weightVM, long userId)
        {
            try
            {
                return productCompositionBO.updateRecoveryEstimatedWeight(weightVM, userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool RemoveWeightTransaction(int weightTransactionID, int category)
        {
            try
            {
                return productCompositionBO.RemoveWeightTransaction(weightTransactionID, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public WeightDetailsVM LoadRecoveryEstimatedWeightDetailsByTransactionID(int weightTransactionID, int category)
        {
            try
            {
                return productCompositionBO.LoadRecoveryEstimatedWeightDetailsByTransactionID(weightTransactionID, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public SupplyEstimatedWeightViewModel LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category)
        {
            try
            {
                return productCompositionBO.LoadSupplyEstimatedWeightDetailsByItemID(itemID, category);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool updateSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM, long userId)
        {
            try
            {
                return productCompositionBO.updateSupplyEstimatedWeight(supplyEstimatedWeightVM, userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            try
            {
                return productCompositionBO.AddTransactionNote(transactionID, notes);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            try
            {
                return productCompositionBO.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> LoadWeightTransactionNoteByID(int weightTransactionID)
        {
            try
            {
                return productCompositionBO.LoadWeightTransactionNoteByID(weightTransactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #endregion

    }
}