﻿using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.SystemServices
{
    public class UserService : IUserService
    {
        private readonly UserBO userBO;

        public UserService(IUserRepository userRepository, IInvitationRepository invitationReposiotry, IMessageRepository messageRepository, IEventAggregator eventAggregator)
        {
            userBO = new UserBO(userRepository, invitationReposiotry, messageRepository, eventAggregator);
        }

        public User FindUserByEmailAddress(string emailAddress)
        {
            try
            {
                return userBO.FindUserByEmailAddress(emailAddress);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public User FindUserByUserId(long userId)
        {
            try
            {
                return userBO.FindUserByUserId(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public User FindUserByUserName(string username)
        {
            try
            {
                return userBO.FindUserByUserName(username);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddPasswordResetToken(long userId, string token, string secretKey)
        {
            try
            {
                userBO.AddPasswordResetToken(userId, token, secretKey);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public IReadOnlyList<string> GetPasswordResetToken(long userId)
        {
            try
            {
                return userBO.GetPasswordResetToken(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public string FindUserNameByUserId(long userId)
        {
            try
            {
                return userBO.FindUserNameByUserId(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public string FindEmailAddressByUsername(string Username)
        {
            try
            {
                return userBO.FindEmailAddressByUsername(Username);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public User ResetPassword(string userName, string password, int passwordExpirationInDays)
        {
            try
            {
                return userBO.ResetPassword(userName, password, passwordExpirationInDays);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool EmailIsExisting(string emailAddress)
        {
            try
            {
                return userBO.EmailIsExisting(emailAddress);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public bool EmailIsExistingForParticipant(string emailAddress, int participantId, bool isVendor)
        {
            try
            {
                return userBO.EmailIsExistingForParticipant(emailAddress, participantId, isVendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void CreateUserInvitation(Invitation invitation)
        {
            try
            {
                userBO.CreateUserInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Invitation FindInvitation(Guid guid)
        {
            try
            {
                return userBO.FindInvitation(guid);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool ConfirmInvitaion(string password, string userName, DateTime passwordExpiationInDays, string redirectUrl, Guid GUID)
        {
            try
            {
                return userBO.ConfirmInvitaion(password, userName, passwordExpiationInDays, redirectUrl, GUID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void UpdateInvitation(Invitation invitation)
        {
            try
            {
                userBO.UpdateInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateInvitationRedirectUrl(int VendorId, string Url)
        {
            try
            {
                userBO.UpdateInvitationRedirectUrl(VendorId, Url);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Invitation FindInvitationByUserName(string userName)
        {
            try
            {
                return userBO.FindInvitationByUserName(userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Invitation FindInvitationByUserName(string userName, int vendorId, bool isVendor)
        {
            try
            {
                return userBO.FindInvitationByUserName(userName, vendorId, isVendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void EditUserAndInvitation(Invitation invitation)
        {
            try
            {
                userBO.EditUserAndInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void EditParticipantUserAndInvitation(Invitation invitation)
        {
            try
            {
                userBO.EditParticipantUserAndInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public List<UserInviteModel> LoadInvitations(string searchText, int vendorId, string orderBy, string sortDirection)
        {
            try
            {
                return userBO.LoadInvitations(searchText, vendorId, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IDictionary<long, string> GetUsersByGroup(string[] groupName)
        {
            try
            {
                return userBO.GetUsersByGroup(groupName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }

                return null;
            }
        }
        public IDictionary<long, string> GetUsersByApplicationsWorkflow(string accountName)
        {
            try
            {
                return userBO.GetUsersByApplicationsWorkflow(accountName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }

                return null;
            }
        }
        public Dictionary<long, string> GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType)
        {
            try
            {
                return userBO.GetUsersByClaimsWorkflow(claimId, accountName, claimType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }

                return null;
            }
        }
        public bool RemoveAfterLoginRedirectUrl(int UserId)
        {
            try
            {
                userBO.RemoveAfterLoginRedirectUrl(UserId);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public bool ResetloginRedirectUrl(long userId, string redirectURL)
        {
            try
            {
                userBO.ResetloginRedirectUrl(userId, redirectURL);
                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }

        public void CreateParticipantDefaultUserInvitation(Invitation invitation)
        {
            try
            {
                userBO.CreateParticipantDefaultUserInvitation(invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ResetRedirectUrlForSecondInvitation(Invitation invitation, User user)
        {
            try
            {
                userBO.ResetRedirectUrlForSecondInvitation(invitation, user);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public CreateInvitationViewModel GetParticipantUserDetails(string email, int particiapntId, bool isVendor)
        {
            try
            {
                var user = userBO.FindParticipantUserDetailsByEmail(email, particiapntId, isVendor);
                var result = new CreateInvitationViewModel();
                result.FirstName = user.FirstName;
                result.EmailAddress = user.Email;
                result.LastName = user.LastName;
                result.RowversionString = Convert.ToBase64String(user.Rowversion);
                user.Roles.ToList().ForEach(c =>
                {
                    if (c.Name == "Read")
                        result.IsRead = true;
                    if (c.Name == "Write")
                        result.IsWrite = true;
                    if (c.Name == "Submit")
                        result.IsSubmit = true;
                    if (c.Name == "ParticipantAdmin")
                        result.IsParticipantAdmin = true;
                });
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }
        public CreateInvitationViewModel GetParticipantUserDetailsView(string email, int particiapntId, bool isVendor)
        {
            try
            {
                var user = userBO.FindParticipantUserDetailsByEmailView(email, particiapntId, isVendor);
                var result = new CreateInvitationViewModel();
                result.FirstName = user.FirstName;
                result.EmailAddress = user.Email;
                result.LastName = user.LastName;
                result.RowversionString = Convert.ToBase64String(user.Rowversion);
                user.Roles.ToList().ForEach(c =>
                {
                    if (c.Name == "Read")
                        result.IsRead = true;
                    if (c.Name == "Write")
                        result.IsWrite = true;
                    if (c.Name == "Submit")
                        result.IsSubmit = true;
                    if (c.Name == "ParticipantAdmin")
                        result.IsParticipantAdmin = true;
                });
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }

        public List<Claim> LoadParticipantUserRoles(long userId, int participantId, bool isVendor)
        {
            try
            {
                return userBO.LoadParticipantUserRolesClaims(userId, participantId, isVendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public void ActiveUserByEmail(string email, bool isActive)
        {
            try
            {
                userBO.ActiveUserByEmail(email, isActive);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        //OTSTM2-1013
        public void UnlockStaffUserByEmail(string email)
        {
            try
            {
                userBO.UnlockStaffUserByEmail(email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UnlockParticipantUserById(long userId)
        {
            try
            {
                userBO.UnlockParticipantUserById(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public PaginationDTO<UserInviteModel, long> LoadInvitations(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            try
            {
                return userBO.LoadInvitations(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<User> GetClaimWorkflowUsers(string accountName, string workflowName)
        {
            try
            {
                return userBO.GetClaimWorkflowUsers(accountName, workflowName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Invitation FindInvitationByApplicationId(int applicationId)
        {
            try
            {
                return userBO.FindInvitationByApplicationId(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Invitation FindInvitationByVendorId(int vendorId)
        {
            try
            {
                return userBO.FindInvitationByVendorId(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddAdditonalUser(long userId, int participantId, bool isVendor, Invitation invitation)
        {
            try
            {
                userBO.AddAdditonalUser(userId, participantId, isVendor, invitation);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Invitation FindInvitation(int vendorId, string email, bool isVendor)
        {
            try
            {
                return userBO.FindInvitation(vendorId, email, isVendor);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<Claim> LoadUserVendorClaims(long userId)
        {
            try
            {
                return userBO.LoadUserVendorClaims(userId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void ActiveParticipantUser(long userId, int vendorId, bool isVendor, bool isActive)
        {
            try
            {
                userBO.ActiveParticipantUser(userId, vendorId, isVendor, isActive);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }


        #region //User Roles
        public PaginationDTO<RoleListViewModel, int> GetAllRoles(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return userBO.GetAllRoles(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<string> GetUserNameByRoleName(string roleName)
        {
            try
            {
                return userBO.GetUserNameByRoleName(roleName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RolePermissionViewModel> GetRolePermissions(string roleName)
        {
            try
            {
                return userBO.GetRolePermissions(roleName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RolePermissionViewModel> GetDefaultRolePermissions()
        {
            try
            {
                return userBO.GetDefaultRolePermissions();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddNewUserRole(RoleViewModel roleViewModel, string userName)
        {
            try
            {
                userBO.AddNewUserRole(roleViewModel, userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public bool RoleNameExists(string roleName)
        {
            try
            {
                return userBO.RoleNameExists(roleName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void UpdateUserRole(RoleViewModel roleViewModel, string userName)
        {
            try
            {
                userBO.UpdateUserRole(roleViewModel, userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public bool DeleteUserRole(int roleID) {
            try
            {
                return userBO.DeleteUserRole(roleID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }
        public List<RoleSelectionViewModel> GetStaffUserRoles()
        {
            try
            {
                return userBO.GetStaffUserRoles();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        public void CreateStaffUserInvitation(Invitation invitation, StaffUserViewModel staffUserViewModel)
        {
            try
            {
                userBO.CreateStaffUserInvitation(invitation, staffUserViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public StaffUserViewModel LoadStaffUserDetails(string email)
        {
            try
            {
                return userBO.LoadStaffUserDetails(email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void EditStaffUserDetail(StaffUserViewModel staffUserViewModel)
        {
            try
            {
                userBO.EditStaffUserDetail(staffUserViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
    }
}