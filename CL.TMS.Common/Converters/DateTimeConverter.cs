﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CL.TMS.Common.Converters
{
    /// <summary>
    /// Datatime Converter used in AutoMapper mapping configuration
    /// </summary>
    public class DateTimeConverter : ITypeConverter<DateTime?, DateTime>
    {
        public DateTime Convert(ResolutionContext context)
        {
            var sourceDate = context.SourceValue as DateTime?;
            if (sourceDate.HasValue)
                return sourceDate.Value;
            else
                return default(DateTime);
        }
        public static DateTime? TruncateTime(DateTime? date)
        {
            return date.HasValue ? date.Value.Date : (DateTime?)null;
        }
    }

}
