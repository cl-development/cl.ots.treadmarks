﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum WebServiceResponseStatus
    {
        NONE = 0,
        OK = 1,
        ERROR = 2,
        WARNING = 3,
    }
}
