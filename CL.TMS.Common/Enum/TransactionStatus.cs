﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum TransactionStatus
    {
        Pending,
        Rejected,
        Completed,
        Error,
        Incomplete,
        Voided
    }
}
