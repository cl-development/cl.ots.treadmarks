﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ScaleTicketTypeEnum
    {
        [Description("Inbound")]
        Inbound = 1,
        [Description("Outbound")]
        Outbound = 2,
        [Description("Both")]
        Both = 3,       
    }
}
