﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum BankInfoReviewStatus
    {
        Open = 0,
        Submitted,
        Approved,
    }
}