﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum PhotoTypeEnum
    {
        [Description("Photo")]
        Photo = 1,
        [Description("IncomingSignature")]
        IncomingSignature = 2,
        [Description("OutgoingSignature")]
        OutgoingSignature = 3,
        [Description("Scale Ticket")]
        ScaleTicket = 4,
        [Description("Document")]
        Document = 5,
    }
}

