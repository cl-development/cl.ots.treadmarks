﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum InternalAdjustmentType
    {
        [Description("Weight")]
        Weight=1,
        [Description("Tire Counts")]
        TireCount,
        [Description("Yard Count")]
        YardCount,
        [Description("Payment")]
        Payment
    }
}
