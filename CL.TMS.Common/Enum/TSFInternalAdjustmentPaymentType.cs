﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum TSFInternalAdjustmentPaymentType
    {
        [Description("Overall")]
        Overall = 1,
        [Description("Audit Assessment")]
        AuditAssessment = 2,
        [Description("Audit Penalty")]
        AuditPenalty = 3
    }
}
