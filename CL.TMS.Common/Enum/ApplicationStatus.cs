﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ApplicationStatusEnum
    {
        None = 0,
        Open = 1,                 //Incomplete Application: Open
        Submitted = 2,            //Submitted Application(in-Q to asign): Submitted
        Assigned = 3,             //Assgned to Reps: Under Review
        BackToApplicant = 4,      //Back to Applicant: Open
        OnHold = 5,               //On-Hold: Under Review
        Denied = 6,               //Deny :Rejected
        Approved = 7,             //Approve: Approved
        OffHold = 8,              //OffHold: Under Review
        Completed = 9,            //?
        BankInformationSubmitted = 10,
        Resend = 11,              //Approved
        ReAssignToRep = 12,        //re-assign to other rep
        UnderReview = 13,
        BankInformationApproved = 14,
    }
}
