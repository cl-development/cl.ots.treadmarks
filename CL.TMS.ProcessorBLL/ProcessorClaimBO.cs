﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.Registrant;
using CL.TMS.Repository.System;
using System.Configuration;
using CL.TMS.Security;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.ProcessorBLL
{
    public class ProcessorClaimBO : BaseBO
    {
        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        private readonly IGpStagingRepository gpStagingRepository;
        public ProcessorClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository, IGpStagingRepository gpStagingRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;
            this.gpStagingRepository = gpStagingRepository;
        }

        #region Public Methods

        public ProcessorClaimSummaryModel LoadProcessorClaimSummary(int vendorId, int claimId)
        {
            var processorClaimSummary = new ProcessorClaimSummaryModel();

            //Load status
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            processorClaimSummary.ClaimCommonModel.Status = status;
            processorClaimSummary.ClaimCommonModel.RegistrationNumber = claim.Participant.Number;

            //Load ClaimPeriod
            processorClaimSummary.ClaimCommonModel.ClaimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (processorClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;

            //Load Inventory Opening data
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);
            processorClaimSummary.TotalOpening =
                DataConversion.ConvertKgToTon((double)inventoryOpeningResult.TotalOpening);

            //Load Inbound and Outbound Transactions
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);

            PopulateInboundOutbound(processorClaimSummary, claimDetails);

            //Load Inventory Adjustment
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateInventoryAdjustment(processorClaimSummary, inventoryAdjustments);

            //Load sort yard capacity
            processorClaimSummary.TotalCapacity =
                DataConversion.ConvertKgToTon((double)claimsRepository.LoadVendorSortYardCapacity(vendorId));

            //Load payment and adjustment
            UpdateGpBatchForClaim(claimId);
            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    processorClaimSummary.ProcessorPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(processorClaimSummary.ProcessorPayment, claimPaymentSummay, claimPayments,
                claimPaymentAdjustments);

            processorClaimSummary.ProcessorPayment.PaymentDueDate = claim.ChequeDueDate;
            // set tax flag
            processorClaimSummary.ProcessorPayment.IsTaxApplicable = claim.IsTaxApplicable ?? false;
            // calculate HST
            processorClaimSummary.ProcessorPayment.HST = 0;
            if (processorClaimSummary.ProcessorPayment.IsTaxApplicable)
            {
                var temp = processorClaimSummary.ProcessorPayment.PTR + processorClaimSummary.ProcessorPayment.SPS + processorClaimSummary.ProcessorPayment.PITOutbound + processorClaimSummary.ProcessorPayment.DOR + processorClaimSummary.ProcessorPayment.PaymentAdjustment;
                //if (temp > 0) //OTSTM2-1296 HST needs to calculate for negative too.
                //{
                processorClaimSummary.ProcessorPayment.HST = Math.Round(temp * processorClaimSummary.ProcessorPayment.HSTBase, 2, MidpointRounding.AwayFromZero);
                //}
            }
            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                LoadStaffClaimSummary(processorClaimSummary, claimId, vendorId,
                    processorClaimSummary.ClaimCommonModel.ClaimPeriod);

                if (status == ClaimStatus.Open)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}",
                        claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }
                if (status == ClaimStatus.Approved)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = "Approved";
                }
                processorClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499

                processorClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                processorClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                processorClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            }

            ApplyUserSecurity(status, processorClaimSummary);

            //OTSTM2-1198 Check if processor specific rate defined - Processor claim summary page
            processorClaimSummary.AssociatedRateTransactionId = claimsRepository.GetAssociatedRateTrnsactionId(processorClaimSummary.ClaimCommonModel.ClaimPeriod.StartDate, processorClaimSummary.ClaimCommonModel.ClaimPeriod.EndDate, vendorId);
            processorClaimSummary.HasSpecificRate = processorClaimSummary.AssociatedRateTransactionId != 0;

            return processorClaimSummary;
        }

        private void UpdateGpBatchForClaim(int claimId)
        {
            var needPullBatch = claimsRepository.IsClaimPosted(claimId);
            if (needPullBatch)
            {
                var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);
                if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
                {
                    var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
                    var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
                    if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
                    {
                        claimsRepository.UpdateClaimPaymentSummary(gpPaymentSummary, claimId);
                    }
                }
            }
        }
        private void ApplyUserSecurity(ClaimStatus status, ProcessorClaimSummaryModel processorClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    processorClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    processorClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        public ProcessorSubmitClaimViewModel CheckClaimSubmitBusinessRule(ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>
            {
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel}
            };

            var updatedSubmitClaimForError = ProcessorClaimSubmitErrors(claim, ruleContext, submitClaimModel);
            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = ProcessorClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;

        }
        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                var claimType = (int)ClaimType.Processor;
                var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
                var gpiAccountsAll = DataLoader.GPIAccounts;
                var gpiAccounts = gpiAccountsAll.Where(i => i.registrant_type_ind == "P");
                //var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, false);              
                var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, true); //OTSTM2-16
                var allClaimIds = allClaims.Select(c => c.ID).ToList();
                //Remove claim if it is banking status is not approved
                var claims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

                if (!claims.Any())
                {
                    msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
                }
                else
                {

                    // STEP 1 INSERT INTO gpibatch
                    var gpiBatch = new GpiBatch()
                    {
                        GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Processor),
                        GpiBatchCount = claims.Count,
                        GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                    };
                    gpRepository.AddGpBatch(gpiBatch);

                    msg.ID = gpiBatch.ID;
                    // STEP 2 INSERT INTO gpibatch_entry
                    var gpiBatchEntries = claims.Select(claim => new GpiBatchEntry()
                    {
                        GpiBatchID = gpiBatch.ID,
                        GpiTxnNumber = string.Format("{0}-{1:yyyy-MM-dd}-{2}", claim.Participant.Number, claim.ClaimPeriod.EndDate, claim.ID),
                        GpiBatchEntryDataKey = claim.ID.ToString(),
                        GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                        CreatedDate = DateTime.Now,
                        CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedDate = DateTime.Now,
                        ClaimId = claim.ID
                    }).ToList();
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    var minPostDate = gpRepository.GetAllGpFiscalYears()
                        .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                        .Select(d => d.MinimumDocDate).FirstOrDefault();

                    // STEP 3 INSERT INTO rr_ots_pm_transactions
                    var rrOtsPmTransactions = this.GetTransactions(gpiBatch.ID, minPostDate, INTERID);
                    gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransactions);

                    //list of claims and batch entries
                    var claimIds = rrOtsPmTransactions.Select(i => i.ClaimId).ToList();
                    var items = DataLoader.Items;
                    var claimDetailsAll = claimsRepository.LoadClaimDetailsForGP(claimIds, new List<string> { "PTR" }, items);
                    var rrOtsPmTransDistributions = this.GetTransDistributions(rrOtsPmTransactions, claimDetailsAll, gpiAccountsAll, claims, INTERID);
                    //var debits = rrOtsPmTransDistributions.Sum(i => i.DEBITAMT);
                    //var credits = rrOtsPmTransDistributions.Sum(i => i.CRDTAMNT);
                    gpRepository.AddGpRrOtsPmTransDistributions(rrOtsPmTransDistributions);

                    // STEP 8 UPDATE processor_claim_summary
                    var claimIdsForUpdate = claims.Select(c => c.ID).ToList();
                    claimsRepository.UpdateClaimsForGPBatch(claimIdsForUpdate, gpiBatch.ID.ToString());
                    transationScope.Complete();
                }
            }
            return msg;
        }

        #endregion

        public RateDetailsVM LoadPIRatesByTransactionID(int RateTransactionID)
        {
            RateDetailsVM ratesVM = new RateDetailsVM()
            {
                RateTransactionID = RateTransactionID
            };

            var param = new RateParamsDTO()
            {
                categoryName = TreadMarksConstants.ProcessingIncentiveRates
            };


            LoadPIRates(param, ratesVM);

            return ratesVM;
        }

        private void LoadPIRates(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            getProcessingIncentiveParam(param);
            ratesVM.decimalsize = 2;
            var rates = claimsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
            ratesVM.notes = claimsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
            ratesVM = PopulateProcessingIncentiveRateVM(rates, ratesVM, param.PI);
            ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
        }

        private RateDetailsVM PopulateProcessingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, ProcessingIncentiveParam param)
        {
            var SPS = result.Where(x => x.PaymentType == param.spsPaymentType);
            var PIT = result.Where(x => x.PaymentType == param.pitPaymentType);
            ratesVM.processingIncentiveRate = new ProcessingIncentiveRateVM()
            {
                #region //SPS OnRoad
                TDP1OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OnRoad).ItemRate,
                TDP1FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOnRoad).ItemRate,
                TDP2OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OnRoad).ItemRate,
                TDP2FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOnRoad).ItemRate,
                TDP3OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OnRoad).ItemRate,
                TDP3FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOnRoad).ItemRate,
                TDP4OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OnRoad).ItemRate,
                TDP5OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OnRoad).ItemRate,
                #endregion

                #region //SPS OffRoad
                TDP1OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OffRoad).ItemRate,
                TDP1FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOffRoad).ItemRate,
                TDP2OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OffRoad).ItemRate,
                TDP2FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOffRoad).ItemRate,
                TDP3OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OffRoad).ItemRate,
                TDP3FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOffRoad).ItemRate,
                TDP4OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OffRoad).ItemRate,
                TDP5OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OffRoad).ItemRate,
                #endregion

                #region //PIT
                TDP1 = PIT.FirstOrDefault(x => x.ItemID == param.tdp1).ItemRate,
                TDP2 = PIT.FirstOrDefault(x => x.ItemID == param.tdp2).ItemRate,
                TDP3 = PIT.FirstOrDefault(x => x.ItemID == param.tdp3).ItemRate,
                TDP4FF = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FF).ItemRate,
                TDP4FFNoPI = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FFNoPI).ItemRate,
                TDP5FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5FT).ItemRate,
                TDP5NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5NT).ItemRate,
                TDP6NP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6NP).ItemRate,
                TDP6FP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6FP).ItemRate,
                TDP7NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7NT).ItemRate,
                TDP7FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7FT).ItemRate,
                TransferFibreRubber = PIT.FirstOrDefault(x => x.ItemID == param.transferFibreRubber).ItemRate,
                #endregion
            };
            return ratesVM;
        }

        private void getProcessingIncentiveParam(RateParamsDTO param)
        {
            var itemParams = new
            {
                pitItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue,
                spsItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue,
                onRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OnRoad").DefinitionValue,
                offRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OffRoad").DefinitionValue,
                spsPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorSPS").DefinitionValue,//4
                pitPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorPITOutbound").DefinitionValue//5
            };

            var itemList = new
            {
                spsOnRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.onRoad),
                spsOffRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.offRoad),
                pit = DataLoader.Items.Where(i => i.ItemCategory == itemParams.pitItemCategory)
            };

            param.PI = new ProcessingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                pitItemCategory = itemParams.pitItemCategory,
                spsItemCategory = itemParams.spsItemCategory,
                onRoad = itemParams.onRoad,
                offRoad = itemParams.offRoad,
                spsPaymentType = itemParams.spsPaymentType,
                pitPaymentType = itemParams.pitPaymentType,

                spsOffRoad = itemList.spsOffRoad,
                spsOnRoad = itemList.spsOnRoad,
                pit = itemList.pit,

                #region //SPS OnRoad
                tdp1OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //SPS OffRoad
                tdp1OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //PIT
                tdp1 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP1")).ID,
                tdp2 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP2")).ID,
                tdp3 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP3")).ID,
                tdp4FF = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && !i.Description.Contains("PI")).ID,
                tdp4FFNoPI = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && i.Description.Contains("PI")).ID,
                tdp5FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5FT")).ID,
                tdp5NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5NT")).ID,
                tdp6NP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6NP")).ID,
                tdp6FP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6FP")).ID,
                tdp7NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7NT")).ID,
                tdp7FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7FT")).ID,
                transferFibreRubber = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TransferFiberRubber")).ID,
                #endregion
            };
            param.PI.items = new int[] {
                #region // SPS On-Road
                param.PI.tdp1OnRoad,
                param.PI.tdp1FeedstockOnRoad,
                param.PI.tdp2OnRoad,
                param.PI.tdp2FeedstockOnRoad,
                param.PI.tdp3OnRoad,
                param.PI.tdp3FeedstockOnRoad,
                param.PI.tdp4OnRoad,
                param.PI.tdp5OnRoad,
                #endregion

                #region //SPS OffRoad
                param.PI.tdp1OffRoad,
                param.PI.tdp1FeedstockOffRoad,
                param.PI.tdp2OffRoad,
                param.PI.tdp2FeedstockOffRoad,
                param.PI.tdp3OffRoad,
                param.PI.tdp3FeedstockOffRoad,
                param.PI.tdp4OffRoad,
                param.PI.tdp5OffRoad,
                #endregion

                #region //PIT
                param.PI.tdp1,
                param.PI.tdp2,
                param.PI.tdp3,
                param.PI.tdp4FF,
                param.PI.tdp4FFNoPI,
                param.PI.tdp5FT,
                param.PI.tdp5NT,
                param.PI.tdp6NP,
                param.PI.tdp6FP,
                param.PI.tdp7NT,
                param.PI.tdp7FT,
                param.PI.transferFibreRubber,
                #endregion
            };
        }

        #region Private Methods

        private List<RrOtsPmTransaction> GetTransactions(int gpiBatchId, DateTime minPostDate, string INTERID)
        {
            var result = new List<RrOtsPmTransaction>();
            //var tmpClaimsDto = gpRepository.GetClaimsByGpBatchId(gpiBatchId).Where(i => i.TotalClaimAmount > 0);
            var tmpClaimsDto = gpRepository.GetClaimsByGpBatchId(gpiBatchId).Where(i => i.TotalClaimAmount != 0); //OTSTM2-16
            foreach (var t in tmpClaimsDto)
            {
                //OTSTM2-16
                var processorPaymentViewModel = LoadProcessorPaymentForGP(t.ClaimId);
                var prchAmount1 = Math.Round(processorPaymentViewModel.PTR + processorPaymentViewModel.SPS + processorPaymentViewModel.PITOutbound + (processorPaymentViewModel.PaymentAdjustment > 0 ? processorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                //var distributedPaymentAdjustment = Math.Round(processorPaymentViewModel.PaymentAdjustment>0?processorPaymentViewModel.PaymentAdjustment:0, 2, MidpointRounding.AwayFromZero);
                var prchAmount5 = Math.Round(processorPaymentViewModel.DOR + (processorPaymentViewModel.PaymentAdjustment < 0 ? processorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                //var gpTotal = prchAmount1 + prchAmount5;

                //OTSTM2-1294            
                var prchTax1 = (t.IsTaxApplicable ?? false) ? Math.Round(prchAmount1 * processorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero) : 0;
                var prchTax5 = (t.IsTaxApplicable ?? false) ? Math.Round(prchAmount5 * processorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero) : 0;

                //Fixed 0.01 difference for HST caused by rounding
                var hstTotal = prchTax1 + prchTax5;
                if (hstTotal != processorPaymentViewModel.HST)
                {
                    var difference = (hstTotal - processorPaymentViewModel.HST);
                    //put all difference into prchTax1
                    prchTax1 -= difference;
                }

                var positiveTotal = prchAmount1 + prchTax1;
                var gpTotal = positiveTotal + prchAmount5 + prchTax5;

                //Fixed cent difference caused by rounding
                if (gpTotal != processorPaymentViewModel.GroundTotal)
                {
                    var difference = (gpTotal - processorPaymentViewModel.GroundTotal);
                    //put all difference in prchAmount1
                    prchAmount1 -= difference;
                }

                var submissionNumberForTypeOne = "1";
                var submissionNumberForTypeFive = "5";
                //var submissionNumber = "1";

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                if (prchAmount1 != 0) //OTSTM2-16
                {
                    var rowForTypeOne = new RrOtsPmTransaction()
                    {
                        BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                        VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        VENDORID = t.RegistrationNumber,
                        DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        DOCTYPE = 1,
                        //DOCAMNT = t.TotalClaimAmount,
                        DOCAMNT = prchAmount1 + prchTax1, //OTSTM2-1294
                        DOCDATE = t.Period.EndDate < minPostDate ? minPostDate : t.Period.EndDate,
                        PSTGDATE = null,
                        VADDCDPR = "PRIMARY",
                        PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                        TAXSCHID = (processorPaymentViewModel.HST == 0 && !processorPaymentViewModel.IsTaxApplicable) ? "P-EXEMPT" : "P-HST",
                        DUEDATE = null,
                        //PRCHAMNT = t.TotalClaimAmount,
                        PRCHAMNT = prchAmount1,
                        TAXAMNT = prchTax1, //OTSTM2-1294
                        PORDNMBR = null,
                        USERDEF1 = t.GpiBatchId.ToString(), //t.GpiBatch.ID.ToString(),
                        USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = null,
                        GpiBatchEntryId = t.GpiBatchEntryId, //t.GpiBatchEntry.ID,
                        ClaimId = t.ClaimId
                    };
                    result.Add(rowForTypeOne);
                }

                //OTSTM2-16 -- add type 5 transaction
                if (prchAmount5 != 0)
                {
                    var rowForTypeFive = new RrOtsPmTransaction()
                    {
                        BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                        VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        VENDORID = t.RegistrationNumber,
                        DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        DOCTYPE = 5,
                        //DOCAMNT = t.TotalClaimAmount,
                        DOCAMNT = prchAmount5 + prchTax5, //OTSTM2-1294
                        DOCDATE = t.Period.EndDate < minPostDate ? minPostDate : t.Period.EndDate,
                        PSTGDATE = null,
                        VADDCDPR = "PRIMARY",
                        PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                        TAXSCHID = (processorPaymentViewModel.HST == 0 && !processorPaymentViewModel.IsTaxApplicable) ? "P-EXEMPT" : "P-HST",
                        DUEDATE = null,
                        //PRCHAMNT = t.TotalClaimAmount,
                        PRCHAMNT = prchAmount5,
                        TAXAMNT = prchTax5, //OTSTM2-1294
                        PORDNMBR = null,
                        USERDEF1 = t.GpiBatchId.ToString(), //t.GpiBatch.ID.ToString(),
                        USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = null,
                        GpiBatchEntryId = t.GpiBatchEntryId, //t.GpiBatchEntry.ID,
                        ClaimId = t.ClaimId
                    };
                    result.Add(rowForTypeFive);
                }
            }
            return result;
        }

        //OTSTM2-16
        private ProcessorPaymentViewModel LoadProcessorPaymentForGP(int claimId)
        {
            var processorPayment = new ProcessorPaymentViewModel();
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.PTR)
                {
                    processorPayment.PTR = c.Amount;
                } else if (c.Name == (int)ClaimPaymentType.SPS)
                {
                    processorPayment.SPS = c.Amount;
                } else if (c.Name == (int)ClaimPaymentType.PITOutbound)
                {
                    processorPayment.PITOutbound = c.Amount;
                } else if (c.Name == (int)ClaimPaymentType.DOR)
                {
                    processorPayment.DOR = c.Amount;
                }
            });

            processorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

            //OTSTM2-1294
            processorPayment.IsTaxApplicable = claimsRepository.FindClaimByClaimId(claimId).IsTaxApplicable ?? false;
            processorPayment.HST = 0;
            if (processorPayment.IsTaxApplicable)
            {
                processorPayment.HST = Math.Round((processorPayment.PTR + processorPayment.SPS + processorPayment.PITOutbound + processorPayment.DOR + processorPayment.PaymentAdjustment) * processorPayment.HSTBase, 2, MidpointRounding.AwayFromZero);
            }

            return processorPayment;

        }

        private List<RrOtsPmTransDistribution> GetTransDistributions(List<RrOtsPmTransaction> transactions, List<ClaimDetailViewModel> claimDetails, List<GpiAccount> gpiAccounts, List<Claim> claims, string INTERID)
        {
            var result = new List<RrOtsPmTransDistribution>();
            var gpiAccParam = new gpiTypeOneAccountParameterDTO(gpiAccounts);
            //var accountsPayable = gpiAccounts.Single(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "P");
            //var pltPi = gpiAccounts.Single(a => a.account_number == "4246-10-40-40" && a.registrant_type_ind.Trim() == "P");
            //var pltPiHST = gpiAccounts.Single(a => a.account_number == "4247-10-40-40" && a.registrant_type_ind.Trim() == "P"); //OTSTM2-1294
            ////OTSTM2-16 GP distribution for positive adjustment
            //var positiveAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");
            //var positiveAdjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P"); //OTSTM2-1294

            //var HSTBase = 0.13m; //OTSTM2-1294

            foreach (var transaction in transactions)
            {
                var seqNum = 1;
                //OTSTM2-16 Ti:PTR, Pi: SPS + PIT Outbound, and positive adj
                if (transaction.DOCTYPE.ToString() == "1")
                {
                    var rowCredit = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        //DISTTYPE = (int)accountsPayable.distribution_type_id,
                        DISTTYPE = (int)gpiAccParam.accountsPayable.distribution_type_id,
                        DistRef = null,
                        //ACTNUMST = accountsPayable.account_number,
                        ACTNUMST = gpiAccParam.accountsPayable.account_number,
                        DEBITAMT = 0,
                        CRDTAMNT = transaction.DOCAMNT,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    result.Add(rowCredit);

                    AddTypeOneDistributions(claimDetails, claims, INTERID, ref result, gpiAccParam, transaction, ref seqNum);

                    //Fixed cent difference caused by rounding
                    //var VCHNUMWKs = result.Select(i => i.VCHNUMWK).ToList();
                    //foreach (var vchnumwk in VCHNUMWKs)
                    //{
                    //    var totalAmountDebit = result.Where(i => i.VCHNUMWK == vchnumwk).Sum(c => c.DEBITAMT);
                    //    var totalAmountCredit = result.Where(i => i.VCHNUMWK == vchnumwk).Sum(c => c.CRDTAMNT);
                    //    if (totalAmountDebit != totalAmountCredit)
                    //    {
                    //        var difference = (totalAmountDebit - totalAmountCredit);
                    //        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    //        var debitRows = result.Where(i => i.VCHNUMWK == vchnumwk && i.DISTTYPE == 6 && i.ACTNUMST != positiveAdjAcc.account_number && i.ACTNUMST != positiveAdjAccHST.account_number).ToList(); //OTSTM2-16 exclude the positiveAdj and OTSTM2-1294 the positiveAdjHST
                    //        var counter = 0;
                    //        while (counter < numberOfDifferenceDistribution)
                    //        {
                    //            debitRows[counter % debitRows.Count()].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    //            counter++;
                    //        }
                    //    }
                    //}
                }
                //OTSTM2-16 DOR + Negative Adj
                if (transaction.DOCTYPE.ToString() == "5")
                {
                    var rowCredit = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)gpiAccParam.accountsPayable.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = gpiAccParam.accountsPayable.account_number,
                        DEBITAMT = Math.Abs(transaction.DOCAMNT),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    result.Add(rowCredit);

                    //OTSTM2-1294
                    var claim = claims.Single(i => i.ID == transaction.ClaimId);
                    var isTaxApplicable = claim.IsTaxApplicable ?? false;

                    this.AddTypeFiveDistributions(gpiAccParam, transaction, INTERID, ref result, ref seqNum, isTaxApplicable);
                }

            }
            return result;
        }

        private void AddTypeOneDistributions(List<ClaimDetailViewModel> claimDetails, List<Claim> claims, string INTERID, ref List<RrOtsPmTransDistribution> result, gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, ref int seqNum)
        {
            var calcClaimDetails = claimDetails.Where(i => i.ClaimId == transaction.ClaimId).ToList();
            var totalEstWeight = calcClaimDetails.Sum(i => i.EstimatedOffRoad + i.EstimatedOnRoad);

            var claim = claims.Single(i => i.ID == transaction.ClaimId);
            var totalTiAmount = claim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            var totalPiAmount = claim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS || c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claim.ID);
            var positivePaymentAdj = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount) > 0 ? Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero) : 0;

            var gpTireItem = FindTirePercentage(calcClaimDetails, claim.ClaimPeriod.StartDate);
            var isTaxApplicable = claim.IsTaxApplicable ?? false; //OTSTM2-1294
            var prchTax1 = transaction.TAXAMNT ?? 0.00m; //OTSTM2-1294 to fix rounding difference for positive HST                   

            if (totalEstWeight <= 0)
            {
                var prevClaim = claimsRepository.GetPreviousApprovedClaim((int)ClaimType.Processor, claim.ClaimPeriod.StartDate, claim.ParticipantId);
                var prevClaimId = prevClaim != null ? prevClaim.ID : -1;
                var items = DataLoader.Items;      
                calcClaimDetails= claimsRepository.LoadClaimDetailsForGP(new List<int> { prevClaimId }, new List<string> { "PTR" }, items);
                totalEstWeight= calcClaimDetails != null ? calcClaimDetails.Sum(i => i.EstimatedOnRoad + i.EstimatedOffRoad) : 0;
                totalTiAmount=prevClaim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));              
                gpTireItem =FindTirePercentage(calcClaimDetails, prevClaim.ClaimPeriod.StartDate);
            }

            if (totalEstWeight > 0)
            {               
                if (positivePaymentAdj > 0)
                {
                    seqNum = AddTypeOnePaymentAdj(INTERID, result, gpiAccParam, transaction, seqNum, positivePaymentAdj, isTaxApplicable);
                }
                if (totalTiAmount > 0)
                {
                    seqNum = AddTypeOneTIDistributions(gpiAccParam, transaction, gpTireItem, totalTiAmount, INTERID, result, seqNum);
                    if (isTaxApplicable)
                    {
                        seqNum = AddTypeOneTIHstDistributions(gpiAccParam, transaction, gpTireItem, totalTiAmount, INTERID, result, seqNum);
                    }
                }
                if (totalPiAmount > 0)
                {
                    seqNum = AddTypeOnePIDistributions(gpiAccParam, transaction, gpTireItem, totalPiAmount, INTERID, result, seqNum);
                    if (isTaxApplicable)
                    {
                        seqNum = AddTypePIHstDistributions(gpiAccParam, transaction, gpTireItem, totalPiAmount, INTERID, result, seqNum);
                    }
                }

                //HST rounding one more time, to fix HST 0.01 issue            
                if (isTaxApplicable && prchTax1 > 0)
                {
                    var totalHstTmp = result.Where(c => c.USERDEF2 == "TI HST" || c.USERDEF2 == "PI HST" || c.USERDEF2 == "PADJ HST").Sum(c => c.DEBITAMT);
                    var hstDistributionsTmp = new List<RrOtsPmTransDistribution>();
                    if (totalTiAmount > 0)
                    {
                        hstDistributionsTmp = result.Where(c => c.USERDEF2 == "TI HST").Select(c => c).ToList();
                    }
                    else if (totalPiAmount > 0)
                    {
                        hstDistributionsTmp = result.Where(c => c.USERDEF2 == "PI HST").Select(c => c).ToList();
                    }
                    else if (positivePaymentAdj > 0)
                    {
                        hstDistributionsTmp = result.Where(c => c.USERDEF2 == "PADJ HST").Select(c => c).ToList();
                    }
                    if (totalHstTmp != prchTax1 && hstDistributionsTmp.Count > 0)
                    {
                        var difference = totalHstTmp - prchTax1;
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < hstDistributionsTmp.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                hstDistributionsTmp[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                }
                //Remove the temporary flag for rounding calculation here
                result.ForEach(c => c.USERDEF2 = null);
            } else  {
                if (totalPiAmount > 0) 
                {
                    seqNum = AddTypeOneDebitPIDistributions(INTERID, result, gpiAccParam, transaction, seqNum, totalPiAmount, isTaxApplicable);
                }               
                if (positivePaymentAdj > 0)
                {
                    seqNum = AddTypeOnePaymentAdj(INTERID, result, gpiAccParam, transaction, seqNum, positivePaymentAdj, isTaxApplicable);
                }
            }
        }

        private int AddTypeOneDebitPIDistributions(string INTERID, List<RrOtsPmTransDistribution> result, gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, int seqNum, decimal totalPiAmount, bool isTaxApplicable)
        {
            var rowDebitPi = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = transaction.VCHNUMWK,
                DOCTYPE = transaction.DOCTYPE,
                VENDORID = transaction.VENDORID,
                DSTSQNUM = seqNum++,
                //DISTTYPE = (int)pltPi.distribution_type_id,
                DISTTYPE = (int)gpiAccParam.pltPi.distribution_type_id,
                DistRef = null,
                ACTNUMST = gpiAccParam.pltPi.account_number,
                DEBITAMT = totalPiAmount,
                CRDTAMNT = 0,
                USERDEF1 = transaction.USERDEF1,
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null,
                //NULL DEX_ROW_ID                        
            };
            result.Add(rowDebitPi);

            //OTSTM2-1294
            if (isTaxApplicable)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.pltPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.pltPiHST.account_number,
                    DEBITAMT = totalPiAmount * gpiAccParam.HSTBase,
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                result.Add(rowDebitPiHST);
            }

            return seqNum;
        }

        private int AddTypeOnePaymentAdj(string INTERID, List<RrOtsPmTransDistribution> result, gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, int seqNum, decimal positivePaymentAdj, bool isTaxApplicable)
        {           
            var rowPositiveAdj = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = transaction.VCHNUMWK,
                DOCTYPE = transaction.DOCTYPE,
                VENDORID = transaction.VENDORID,
                DSTSQNUM = seqNum++,
                DISTTYPE = (int)gpiAccParam.adjAcc.distribution_type_id,
                DistRef = null,
                ACTNUMST = gpiAccParam.adjAcc.account_number,
                DEBITAMT = Math.Round(positivePaymentAdj, 2, MidpointRounding.AwayFromZero),
                CRDTAMNT = 0,
                USERDEF1 = transaction.USERDEF1,
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null,
                //NULL DEX_ROW_ID                        
            };
            result.Add(rowPositiveAdj);

            //OTSTM2-1294
            if (isTaxApplicable)
            {
                var rowPositiveAdjHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    //DISTTYPE = (int)positiveAdjAccHST.distribution_type_id,
                    DISTTYPE = (int)gpiAccParam.adjAccHST.distribution_type_id,
                    DistRef = null,
                    //ACTNUMST = positiveAdjAccHST.account_number,
                    ACTNUMST = gpiAccParam.adjAccHST.account_number,
                    DEBITAMT = Math.Round(positivePaymentAdj * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                result.Add(rowPositiveAdjHST);
            }    
            return seqNum;
        }

        //OTSTM2-16 GP distribution for Negative Adjustment
        //private void AddTypeFiveDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, List<GpiAccount> gpiAccounts, string INTERID, ref List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, ref int seqNum, bool isTaxApplicable)
        private void AddTypeFiveDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, string INTERID, ref List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, ref int seqNum, bool isTaxApplicable)
        {
            //var negativeAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");

            ////OTSTM2-1294
            //var negativeAdjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P");
            //var HSTBase = 0.13m;

            var rowDebitTi = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = transaction.VCHNUMWK,
                DOCTYPE = transaction.DOCTYPE,
                VENDORID = transaction.VENDORID,
                DSTSQNUM = seqNum++,
                //DISTTYPE = (int)negativeAdjAcc.distribution_type_id,
                DISTTYPE = (int)gpiAccParam.adjAcc.distribution_type_id,
                DistRef = null,
                //ACTNUMST = negativeAdjAcc.account_number,
                ACTNUMST = gpiAccParam.adjAcc.account_number,
                DEBITAMT = 0,
                CRDTAMNT = Math.Abs(transaction.PRCHAMNT),
                USERDEF1 = transaction.USERDEF1,
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null,
                //NULL DEX_ROW_ID                        
            };
            rrOtsPmTransDistributions.Add(rowDebitTi);

            //OTSTM2-1294
            if (isTaxApplicable)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    //DISTTYPE = (int)negativeAdjAccHST.distribution_type_id,
                    DISTTYPE = (int)gpiAccParam.adjAccHST.distribution_type_id,
                    DistRef = null,
                    //ACTNUMST = negativeAdjAccHST.account_number,
                    ACTNUMST = gpiAccParam.adjAccHST.account_number,
                    DEBITAMT = 0,
                    CRDTAMNT = Math.Abs(transaction.TAXAMNT.Value),
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }
        }

        //OTSTM2-16 one more param "positivePaymentAdj"
        private void AddTireTypes(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, GpTireItem gpTireItem, decimal totalTiAmount, decimal totalPiAmount, decimal positivePaymentAdj, bool includePi, string INTERID, ref List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, ref int seqNum, bool isTaxApplicable, decimal prchTax1)
        {
            //var pltTi = gpiAccounts.Single(a => a.account_number == "4241-10-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var pltPi = gpiAccounts.Single(a => a.account_number == "4246-10-40-40" && a.registrant_type_ind.Trim() == "P");
            //var mtTi = gpiAccounts.Single(a => a.account_number == "4242-20-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var mtPi = gpiAccounts.Single(a => a.account_number == "4248-20-40-40" && a.registrant_type_ind.Trim() == "P");
            //var aglsTi = gpiAccounts.Single(a => a.account_number == "4243-30-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var aglsPi = gpiAccounts.Single(a => a.account_number == "4251-30-40-40" && a.registrant_type_ind.Trim() == "P");
            //var indTi = gpiAccounts.Single(a => a.account_number == "4244-40-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var indPi = gpiAccounts.Single(a => a.account_number == "4253-40-40-40" && a.registrant_type_ind.Trim() == "P");
            //var sotrTi = gpiAccounts.Single(a => a.account_number == "4361-50-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var sotrPi = gpiAccounts.Single(a => a.account_number == "4256-50-40-40" && a.registrant_type_ind.Trim() == "P");
            //var motrTi = gpiAccounts.Single(a => a.account_number == "4362-60-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var motrPi = gpiAccounts.Single(a => a.account_number == "4258-60-40-40" && a.registrant_type_ind.Trim() == "P");
            //var lotrTi = gpiAccounts.Single(a => a.account_number == "4363-70-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var lotrPi = gpiAccounts.Single(a => a.account_number == "4261-70-40-40" && a.registrant_type_ind.Trim() == "P");
            //var gotrTi = gpiAccounts.Single(a => a.account_number == "4364-80-40-40" && a.registrant_type_ind.Trim() == "PT");
            //var gotrPi = gpiAccounts.Single(a => a.account_number == "4263-80-40-40" && a.registrant_type_ind.Trim() == "P");

            ////OTSTM2-1294
            //var pltTiHST = gpiAccounts.Single(a => a.account_number == "4382-10-30-40" && a.registrant_type_ind.Trim() == "P");
            //var pltPiHST = gpiAccounts.Single(a => a.account_number == "4247-10-40-40" && a.registrant_type_ind.Trim() == "P");
            //var mtTiHST = gpiAccounts.Single(a => a.account_number == "4384-20-30-40" && a.registrant_type_ind.Trim() == "P");
            //var mtPiHST = gpiAccounts.Single(a => a.account_number == "4249-20-40-40" && a.registrant_type_ind.Trim() == "P");
            //var aglsTiHST = gpiAccounts.Single(a => a.account_number == "4387-30-30-40" && a.registrant_type_ind.Trim() == "P");
            //var aglsPiHST = gpiAccounts.Single(a => a.account_number == "4252-30-40-40" && a.registrant_type_ind.Trim() == "P");
            //var indTiHST = gpiAccounts.Single(a => a.account_number == "4389-40-30-40" && a.registrant_type_ind.Trim() == "P");
            //var indPiHST = gpiAccounts.Single(a => a.account_number == "4254-40-40-40" && a.registrant_type_ind.Trim() == "P");
            //var sotrTiHST = gpiAccounts.Single(a => a.account_number == "4398-50-30-40" && a.registrant_type_ind.Trim() == "P");
            //var sotrPiHST = gpiAccounts.Single(a => a.account_number == "4257-50-40-40" && a.registrant_type_ind.Trim() == "P");
            //var motrTiHST = gpiAccounts.Single(a => a.account_number == "4401-60-30-40" && a.registrant_type_ind.Trim() == "P");
            //var motrPiHST = gpiAccounts.Single(a => a.account_number == "4259-60-40-40" && a.registrant_type_ind.Trim() == "P");
            //var lotrTiHST = gpiAccounts.Single(a => a.account_number == "4403-70-30-40" && a.registrant_type_ind.Trim() == "P");
            //var lotrPiHST = gpiAccounts.Single(a => a.account_number == "4262-70-40-40" && a.registrant_type_ind.Trim() == "P");
            //var gotrTiHST = gpiAccounts.Single(a => a.account_number == "4406-80-30-40" && a.registrant_type_ind.Trim() == "P");
            //var gotrPiHST = gpiAccounts.Single(a => a.account_number == "4264-80-40-40" && a.registrant_type_ind.Trim() == "P");

            ////OTSTM2-16 GP distribution for positive adjustment
            //var positiveAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");

            ////OTSTM2-1294
            //var positiveAdjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P");
            //var HSTBase = 0.13m;

            if (positivePaymentAdj > 0)
            {
                //var rowPositiveAdj = new RrOtsPmTransDistribution()
                //{
                //    VCHNUMWK = transaction.VCHNUMWK,
                //    DOCTYPE = transaction.DOCTYPE,
                //    VENDORID = transaction.VENDORID,
                //    DSTSQNUM = seqNum++,
                //    //DISTTYPE = (int)gpiAccParam.positiveAdjAcc.distribution_type_id,
                //    DISTTYPE = (int)gpiAccParam.adjAcc.distribution_type_id,
                //    DistRef = null,
                //    //ACTNUMST = gpiAccParam.positiveAdjAcc.account_number,
                //    ACTNUMST = gpiAccParam.adjAcc.account_number,
                //    DEBITAMT = Math.Round(positivePaymentAdj, 2, MidpointRounding.AwayFromZero),
                //    CRDTAMNT = 0,
                //    USERDEF1 = transaction.USERDEF1,
                //    USERDEF2 = "PADJ", //temporary flag for rounding calculation, will be removed at the end of the method
                //    INTERID = INTERID,
                //    INTSTATUS = null,
                //    INTDATE = null,
                //    ERRORCODE = null,
                //    //NULL DEX_ROW_ID                        
                //};
                //rrOtsPmTransDistributions.Add(rowPositiveAdj);

                ////OTSTM2-1294
                //if (isTaxApplicable)
                //{
                //    var rowPositiveAdjHST = new RrOtsPmTransDistribution()
                //    {
                //        VCHNUMWK = transaction.VCHNUMWK,
                //        DOCTYPE = transaction.DOCTYPE,
                //        VENDORID = transaction.VENDORID,
                //        DSTSQNUM = seqNum++,
                //        //DISTTYPE = (int)gpiAccParam.positiveAdjAccHST.distribution_type_id,
                //        DISTTYPE = (int)gpiAccParam.adjAccHST.distribution_type_id,
                //        DistRef = null,
                //        //ACTNUMST = gpiAccParam.positiveAdjAccHST.account_number,
                //        ACTNUMST = gpiAccParam.adjAccHST.account_number,
                //        DEBITAMT = Math.Round(positivePaymentAdj * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                //        CRDTAMNT = 0,
                //        USERDEF1 = transaction.USERDEF1,
                //        USERDEF2 = "PADJ HST", //temporary flag for rounding calculation, will be removed at the end of the method
                //        INTERID = INTERID,
                //        INTSTATUS = null,
                //        INTDATE = null,
                //        ERRORCODE = null,
                //        //NULL DEX_ROW_ID                        
                //    };
                //    rrOtsPmTransDistributions.Add(rowPositiveAdjHST);
                //}
                seqNum = AddTypeOnePaymentAdj(INTERID, rrOtsPmTransDistributions, gpiAccParam, transaction, seqNum, positivePaymentAdj, isTaxApplicable);
            }

            if (totalTiAmount > 0)
            {
                seqNum = AddTypeOneTIDistributions(gpiAccParam, transaction, gpTireItem, totalTiAmount, INTERID, rrOtsPmTransDistributions, seqNum);
            }

            if (includePi)
            {
                seqNum = AddTypeOnePIDistributions(gpiAccParam, transaction, gpTireItem, totalPiAmount, INTERID, rrOtsPmTransDistributions, seqNum);
            }

            //HST rounding one more time, to fix HST 0.01 issue            
            if (isTaxApplicable && prchTax1 > 0)
            {
                //var totalHst = positivePaymentAdj >= 0 ? Math.Round((totalTiAmount + totalPiAmount + positivePaymentAdj) * HSTBase, 2, MidpointRounding.AwayFromZero) : Math.Round((totalTiAmount + totalPiAmount) * HSTBase, 2, MidpointRounding.AwayFromZero);
                var totalHst = prchTax1;
                var totalHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST" || c.USERDEF2 == "PI HST" || c.USERDEF2 == "PADJ HST").Sum(c => c.DEBITAMT);
                var hstDistributionsTmp = new List<RrOtsPmTransDistribution>();
                if (totalTiAmount > 0)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Select(c => c).ToList();
                }
                else if (includePi)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Select(c => c).ToList();
                }
                else if (positivePaymentAdj > 0)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PADJ HST").Select(c => c).ToList();
                }
                if (totalHstTmp != totalHst && hstDistributionsTmp.Count > 0)
                {
                    var difference = totalHstTmp - totalHst;
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < hstDistributionsTmp.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            hstDistributionsTmp[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }
            }

            //Remove the temporary flag for rounding calculation here
            rrOtsPmTransDistributions.ForEach(c => c.USERDEF2 = null);

        }
        private int AddTypeOnePIDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, GpTireItem gpTireItem, decimal totalPiAmount, string INTERID, List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, int seqNum)
        {
            if (gpTireItem.PLTPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.pltPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.pltPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.MTPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.mtPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.mtPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.AGLSPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.aglsPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.aglsPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.INDPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.indPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.indPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.SOTRPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.sotrPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.sotrPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.MOTRPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.motrPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.motrPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.LOTRPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.lotrPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.lotrPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            if (gpTireItem.GOTRPercentage > 0)
            {
                var rowDebitPi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.gotrPi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.gotrPi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPi);
            }

            //Fixed cent difference caused by rounding
            var totalPiAmountTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI").Sum(c => c.DEBITAMT);
            var PiDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI").Select(c => c).ToList();
            if (totalPiAmountTmp != totalPiAmount)
            {
                var difference = (totalPiAmountTmp - totalPiAmount);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < PiDistributions.Count)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        PiDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }

            //rrOtsPmTransDistributions.ForEach(c => c.USERDEF2 = null);
            return seqNum;
        }

        private static int AddTypePIHstDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, GpTireItem gpTireItem, decimal totalPiAmount, string INTERID, List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, int seqNum)
        {
            if (gpTireItem.PLTPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.pltPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.pltPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.MTPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.mtPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.mtPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.AGLSPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.aglsPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.aglsPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.INDPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.indPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.indPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.SOTRPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.sotrPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.sotrPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.MOTRPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.motrPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.motrPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.LOTRPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.lotrPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.lotrPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            if (gpTireItem.GOTRPercentage > 0)
            {
                var rowDebitPiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.gotrPiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.gotrPiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitPiHST);
            }

            //Fixed cent difference caused by rounding
            var totalPiAmountHst = Math.Round(totalPiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero);
            var totalPiAmountHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Sum(c => c.DEBITAMT);
            var PiHstDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Select(c => c).ToList();
            if (totalPiAmountHstTmp != totalPiAmountHst)
            {
                var difference = (totalPiAmountHstTmp - totalPiAmountHst);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < PiHstDistributions.Count)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        PiHstDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }

            return seqNum;
        }

        private int AddTypeOneTIDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, GpTireItem gpTireItem, decimal totalTiAmount, string INTERID, List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, int seqNum)
        {
            if (gpTireItem.PLTPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.pltTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.pltTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.MTPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.mtTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.mtTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.AGLSPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.aglsTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.aglsTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.INDPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.indTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.indTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.SOTRPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.sotrTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.sotrTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.MOTRPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.motrTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.motrTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.LOTRPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.lotrTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.lotrTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            if (gpTireItem.GOTRPercentage > 0)
            {
                var rowDebitTi = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.gotrTi.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.gotrTi.account_number,
                    DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTi);
            }

            //Fixed cent difference caused by rounding
            var totalTiAmountTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI").Sum(c => c.DEBITAMT);
            var TiDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI").Select(c => c).ToList();
            if (totalTiAmountTmp != totalTiAmount)
            {
                var difference = (totalTiAmountTmp - totalTiAmount);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < TiDistributions.Count)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        TiDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }
            return seqNum;
        }

        private int AddTypeOneTIHstDistributions(gpiTypeOneAccountParameterDTO gpiAccParam, RrOtsPmTransaction transaction, GpTireItem gpTireItem, decimal totalTiAmount, string INTERID, List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, int seqNum)
        {
            if (gpTireItem.PLTPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.pltTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.pltTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.MTPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.mtTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.mtTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.AGLSPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.aglsTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.aglsTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.INDPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.indTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.indTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.SOTRPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.sotrTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.sotrTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.MOTRPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.motrTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.motrTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.LOTRPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.lotrTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.lotrTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            if (gpTireItem.GOTRPercentage > 0)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)gpiAccParam.gotrTiHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = gpiAccParam.gotrTiHST.account_number,
                    DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }

            //Fixed cent difference caused by rounding
            var totalTiAmountHst = Math.Round(totalTiAmount * gpiAccParam.HSTBase, 2, MidpointRounding.AwayFromZero);
            var totalTiAmountHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Sum(c => c.DEBITAMT);
            var TiHstDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Select(c => c).ToList();
            if (totalTiAmountHstTmp != totalTiAmountHst)
            {
                var difference = (totalTiAmountHstTmp - totalTiAmountHst);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < TiHstDistributions.Count)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        TiHstDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }

            return seqNum;
        }

        private GpTireItem FindTirePercentage(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private ProcessorSubmitClaimViewModel ProcessorClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            BusinessRuleSet.ClearBusinessRules();

            BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);

            return submitClaimModel;

        }
        private void ExecuteBusinessRuleForErrors(ProcessorSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }
        private ProcessorSubmitClaimViewModel ProcessorClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new NilActivityClaimRule());
            this.BusinessRuleSet.AddBusinessRule(new NegativeClosingInventoryRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(
                    BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }
        private void LoadStaffClaimSummary(ProcessorClaimSummaryModel processorClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            processorClaimSummary.ClaimStatus = claimStatus;
            processorClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy)
                ? string.Empty
                : string.Format("Under Review by {0}", claimStatus.ReviewedBy);

            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);
        }


        private void PopulatPaymentAdjustments(ProcessorPaymentViewModel processorPayment,
            ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments,
            List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            processorPayment.PTR =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.SPS =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.PITOutbound =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.DOR =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));

            processorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

            processorPayment.isStaff = SecurityContextHelper.IsStaff();

            if (claimPaymentSummay != null)
            {
                processorPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                processorPayment.PaidDate = claimPaymentSummay.PaidDate;
                processorPayment.MailedDate = claimPaymentSummay.MailDate;
                processorPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                processorPayment.BatchNumber = claimPaymentSummay.BatchNumber;
            }
        }

        private void PopulateInventoryAdjustment(ProcessorClaimSummaryModel processorClaimSummary,
            List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            var query = inventoryAdjustments.GroupBy(c => new { c.Direction }).Select(c => new
            {
                Name = c.Key,
                Adjustmment = c.Sum(i => i.AdjustmentWeightOnroad + i.AdjustmentWeightOffroad)
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 1)
                {
                    processorClaimSummary.AdjustIn = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

                if (c.Name.Direction == 2)
                {
                    processorClaimSummary.AdjustOut = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

                if (c.Name.Direction == 0)
                {
                    processorClaimSummary.OverallAdjustments = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

            });
        }

        private void PopulateInboundOutbound(ProcessorClaimSummaryModel processorClaimSummary,
            List<ClaimDetailViewModel> claimDetails)
        {
            var spslist = claimDetails.Where(c => c.TransactionType == "SPS").ToList();
            var query = claimDetails
                .GroupBy(c => new { c.TransactionType, c.Direction })
                .Select(
                    c =>
                        new
                        {
                            Name = c.Key,
                            ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero)),
                            extraDOR = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)i.ExtraDORTotal), 4, MidpointRounding.AwayFromZero))
                        });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && c.Name.Direction)
                {
                    processorClaimSummary.PTR = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && c.Name.Direction)
                {
                    processorClaimSummary.PIT = c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && !c.Name.Direction)
                {
                    processorClaimSummary.SPS = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && !c.Name.Direction)
                {
                    processorClaimSummary.PITOut = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOR && !c.Name.Direction)
                {
                    processorClaimSummary.DOR = c.ActualWeight + c.extraDOR;
                }

            });
        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = currentClaimSummary.TotalOpeningEstimated != null ? (decimal)currentClaimSummary.TotalOpeningEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = currentClaimSummary.TotalOpening != null ? (decimal)currentClaimSummary.TotalOpening : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                }
            }
            return inventoryOpeningSummary;
        }

        #endregion
    }
    public class gpiTypeOneAccountParameterDTO
    {
        public decimal HSTBase = 0.13m;
        public GpiAccount accountsPayable { get; set; }
        // TI account
        public GpiAccount pltTi { get; set; }
        public GpiAccount mtTi { get; set; }
        public GpiAccount aglsTi { get; set; }
        public GpiAccount indTi { get; set; }
        public GpiAccount sotrTi { get; set; }
        public GpiAccount motrTi { get; set; }
        public GpiAccount lotrTi { get; set; }
        public GpiAccount gotrTi { get; set; }
        //TI HST
        public GpiAccount pltTiHST { get; set; }
        public GpiAccount mtTiHST { get; set; }
        public GpiAccount aglsTiHST { get; set; }
        public GpiAccount indTiHST { get; set; }
        public GpiAccount sotrTiHST { get; set; }
        public GpiAccount motrTiHST { get; set; }
        public GpiAccount lotrTiHST { get; set; }
        public GpiAccount gotrTiHST { get; set; }

        // PI account
        public GpiAccount pltPi { get; set; }
        public GpiAccount mtPi { get; set; }
        public GpiAccount aglsPi { get; set; }
        public GpiAccount indPi { get; set; }
        public GpiAccount sotrPi { get; set; }
        public GpiAccount motrPi { get; set; }
        public GpiAccount lotrPi { get; set; }
        public GpiAccount gotrPi { get; set; }
        //PI HST
        public GpiAccount pltPiHST { get; set; }
        public GpiAccount mtPiHST { get; set; }
        public GpiAccount aglsPiHST { get; set; }
        public GpiAccount indPiHST { get; set; }
        public GpiAccount sotrPiHST { get; set; }
        public GpiAccount motrPiHST { get; set; }
        public GpiAccount lotrPiHST { get; set; }
        public GpiAccount gotrPiHST { get; set; }

        // Adjustment Account (include positiveAdjAcc and negativeAdjAcc)
        public GpiAccount adjAcc { get; set; }
        public GpiAccount adjAccHST { get; set; }

        public gpiTypeOneAccountParameterDTO(List<GpiAccount> gpiAccounts)
        {
            HSTBase = 0.13m;
            accountsPayable = gpiAccounts.Single(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "P");

            pltTi = gpiAccounts.Single(a => a.account_number == "4241-10-40-40" && a.registrant_type_ind.Trim() == "PT");
            mtTi = gpiAccounts.Single(a => a.account_number == "4242-20-40-40" && a.registrant_type_ind.Trim() == "PT");
            aglsTi = gpiAccounts.Single(a => a.account_number == "4243-30-40-40" && a.registrant_type_ind.Trim() == "PT");
            indTi = gpiAccounts.Single(a => a.account_number == "4244-40-40-40" && a.registrant_type_ind.Trim() == "PT");
            sotrTi = gpiAccounts.Single(a => a.account_number == "4361-50-40-40" && a.registrant_type_ind.Trim() == "PT");
            motrTi = gpiAccounts.Single(a => a.account_number == "4362-60-40-40" && a.registrant_type_ind.Trim() == "PT");
            lotrTi = gpiAccounts.Single(a => a.account_number == "4363-70-40-40" && a.registrant_type_ind.Trim() == "PT");
            gotrTi = gpiAccounts.Single(a => a.account_number == "4364-80-40-40" && a.registrant_type_ind.Trim() == "PT");

            pltTiHST = gpiAccounts.Single(a => a.account_number == "4382-10-30-40" && a.registrant_type_ind.Trim() == "P");
            mtTiHST = gpiAccounts.Single(a => a.account_number == "4384-20-30-40" && a.registrant_type_ind.Trim() == "P");
            aglsTiHST = gpiAccounts.Single(a => a.account_number == "4387-30-30-40" && a.registrant_type_ind.Trim() == "P");
            indTiHST = gpiAccounts.Single(a => a.account_number == "4389-40-30-40" && a.registrant_type_ind.Trim() == "P");
            sotrTiHST = gpiAccounts.Single(a => a.account_number == "4398-50-30-40" && a.registrant_type_ind.Trim() == "P");
            motrTiHST = gpiAccounts.Single(a => a.account_number == "4401-60-30-40" && a.registrant_type_ind.Trim() == "P");
            lotrTiHST = gpiAccounts.Single(a => a.account_number == "4403-70-30-40" && a.registrant_type_ind.Trim() == "P");
            gotrTiHST = gpiAccounts.Single(a => a.account_number == "4406-80-30-40" && a.registrant_type_ind.Trim() == "P");

            pltPi = gpiAccounts.Single(a => a.account_number == "4246-10-40-40" && a.registrant_type_ind.Trim() == "P");
            mtPi = gpiAccounts.Single(a => a.account_number == "4248-20-40-40" && a.registrant_type_ind.Trim() == "P");
            aglsPi = gpiAccounts.Single(a => a.account_number == "4251-30-40-40" && a.registrant_type_ind.Trim() == "P");
            indPi = gpiAccounts.Single(a => a.account_number == "4253-40-40-40" && a.registrant_type_ind.Trim() == "P");
            sotrPi = gpiAccounts.Single(a => a.account_number == "4256-50-40-40" && a.registrant_type_ind.Trim() == "P");
            motrPi = gpiAccounts.Single(a => a.account_number == "4258-60-40-40" && a.registrant_type_ind.Trim() == "P");
            lotrPi = gpiAccounts.Single(a => a.account_number == "4261-70-40-40" && a.registrant_type_ind.Trim() == "P");
            gotrPi = gpiAccounts.Single(a => a.account_number == "4263-80-40-40" && a.registrant_type_ind.Trim() == "P");

            pltPiHST = gpiAccounts.Single(a => a.account_number == "4247-10-40-40" && a.registrant_type_ind.Trim() == "P");
            mtPiHST = gpiAccounts.Single(a => a.account_number == "4249-20-40-40" && a.registrant_type_ind.Trim() == "P");
            aglsPiHST = gpiAccounts.Single(a => a.account_number == "4252-30-40-40" && a.registrant_type_ind.Trim() == "P");
            indPiHST = gpiAccounts.Single(a => a.account_number == "4254-40-40-40" && a.registrant_type_ind.Trim() == "P");
            sotrPiHST = gpiAccounts.Single(a => a.account_number == "4257-50-40-40" && a.registrant_type_ind.Trim() == "P");
            motrPiHST = gpiAccounts.Single(a => a.account_number == "4259-60-40-40" && a.registrant_type_ind.Trim() == "P");
            lotrPiHST = gpiAccounts.Single(a => a.account_number == "4262-70-40-40" && a.registrant_type_ind.Trim() == "P");
            gotrPiHST = gpiAccounts.Single(a => a.account_number == "4264-80-40-40" && a.registrant_type_ind.Trim() == "P");
            
            // Adjustment Account (include positiveAdjAcc and negativeAdjAcc)
            adjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");
            adjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P");

            ////OTSTM2-16 GP distribution for positive adjustment
            //var positiveAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");
            //var positiveAdjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P"); //OTSTM2-1294
            //var negativeAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40" && a.registrant_type_ind.Trim() == "P");
            //OTSTM2-1294
            //var negativeAdjAccHST = gpiAccounts.Single(a => a.account_number == "4359-90-40-40" && a.registrant_type_ind.Trim() == "P");

        }
    }
}
