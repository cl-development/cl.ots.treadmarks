﻿using CL.TMS.Common.Extension;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Claims;
using CL.TMS.Security.Authorization;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.HaulerBLL
{
    public class HaulerTransactionBO
    {
        private ITransactionRepository transactionRepository;

        public HaulerTransactionBO(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }
    }
}