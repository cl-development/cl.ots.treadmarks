﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace CL.TMS.HaulerBLL
{
    public class HaulerClaimBO : BaseBO
    {

        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private IVendorRepository vendorRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        private readonly IGpStagingRepository gpStagingRepository;
        public HaulerClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, IVendorRepository vendorRepository, ISettingRepository settingRepository, IGpStagingRepository gpStagingRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.vendorRepository = vendorRepository;
            this.settingRepository = settingRepository;
            this.gpStagingRepository = gpStagingRepository;
        }

        #region public methods
        public HaulerClaimSummaryModel LoadHaulerClaimSummary(int vendorId, int claimId)
        {
            var haulerClaimSummary = new HaulerClaimSummaryModel();

            //Load status
            var claim = claimsRepository.GetClaimWithSummaryPeriod(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            haulerClaimSummary.ClaimCommonModel.Status = status;

            //Load claim calculation time
            if (claim.ClaimSummary != null)
                haulerClaimSummary.ClaimCommonModel.ClaimLastCalculationTime = claim.ClaimSummary.UpdatedDate;
            else
                haulerClaimSummary.ClaimCommonModel.ClaimLastCalculationTime = DateTime.UtcNow;

            //Load ClaimPeriod
            haulerClaimSummary.ClaimCommonModel.ClaimPeriod = claim.ClaimPeriod;
            if (haulerClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;

            //Load Inventory Opening data
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);
            haulerClaimSummary.TotalEligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            haulerClaimSummary.TotalEligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            haulerClaimSummary.TotalIneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            haulerClaimSummary.TotalIneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;

            //Load Inbound and Outbound Transactions
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);
            PopulateInboundOutbound(haulerClaimSummary, claimDetails);

            //Populate WeightVariace
            PopulateWeightVariance(haulerClaimSummary, claimDetails);

            //Load Inventory Adjustment
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateInventoryAdjustment(haulerClaimSummary, inventoryAdjustments);

            //Load yard count submission
            var claimYardCountSubmissions = claimsRepository.LoadYardCountSubmission(claimId);
            PopulateClaimYardCountSubmission(haulerClaimSummary, claimYardCountSubmissions);

            //Load sort yard capacity
            haulerClaimSummary.InventoryAdjustment.SortYandCapacity = claimsRepository.LoadVendorSortYardCapacity(vendorId);

            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                LoadStaffClaimSummary(haulerClaimSummary, claimId, vendorId, haulerClaimSummary.ClaimCommonModel.ClaimPeriod);

                if (status == ClaimStatus.Open)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}", claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }

                if (status == ClaimStatus.Approved)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = "Approved";
                }
                haulerClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499

                haulerClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                haulerClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                haulerClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            }

            ApplyUserSecurity(status, haulerClaimSummary);

            return haulerClaimSummary;
        }

        private void ApplyUserSecurity(ClaimStatus status, HaulerClaimSummaryModel haulerClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    haulerClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    haulerClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);

                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                }
            }
            return inventoryOpeningSummary;
        }
        private void LoadStaffClaimSummary(HaulerClaimSummaryModel haulerClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            haulerClaimSummary.ClaimStatus = claimStatus;
            haulerClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy) ? string.Empty : string.Format("Under Review by {0}", claimStatus.ReviewedBy);
            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);

            //Load TireCountBalance
            var inventoryItems = claimsRepository.LoadClaimInventoryItems(vendorId, claimPeriod.EndDate);

            PopulateClaimTireBalance(haulerClaimSummary, inventoryItems);

            PopulateClaimTireBalanceWithoutAdj(haulerClaimSummary, inventoryItems);
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        public HaulerPaymentViewModel LoadHaulerPayment(int claimId)
        {
            var haulerPayment = new HaulerPaymentViewModel();

            UpdateGpBatchForClaim(claimId);

            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var claim = claimsRepository.FindClaimByClaimId(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    haulerPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(haulerPayment, claimPaymentSummay, claimPayments, claimPaymentAdjustments);
            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                haulerPayment.HSTBase = 0.00m;
            }

            var claimPeriodDate = claim.ClaimPeriod.StartDate;

            PopulateNorthernPremiumDetails(haulerPayment, claimPayments, claimPeriodDate);
            PopulateDOTPremiumDetails(haulerPayment, claimPayments, claimPeriodDate);
            PopulateClawbackDetails(haulerPayment, claimPayments, claimPeriodDate);
            //haulerPayment.PaymentDueDate = claim.ChequeDueDate + DateTime.UtcNow.Date.Subtract(((DateTime)claim.ClaimOnHoldDate).Date);
            haulerPayment.PaymentDueDate = claim.ChequeDueDate; //OTSTM2-194
            haulerPayment.HaulerPTRStartDisplayDate = Convert.ToDateTime(AppSettings.Instance.GetSettingValue("Settings.HaulerClaimSummaryPTRDisplayDate"));
            haulerPayment.ClaimPeriodStartDate = claim.ClaimPeriod.StartDate;

            return haulerPayment;
        }

        public List<InventoryItem> LoadInventoryItems(int vendorId, DateTime periodEndDate)
        {
            var inventoryItems = claimsRepository.LoadClaimInventoryItems(vendorId, periodEndDate);
            return inventoryItems;
        }

        public List<ClaimYardCountSubmission> LoadYardCountSubmission(int claimId)
        {
            return claimsRepository.LoadYardCountSubmission(claimId);
        }

        public List<int> LoadClaimIds(int periodId, DateTime dt)
        {
            return claimsRepository.LoadClaimIds(periodId, dt);
        }

        public List<ClaimInventoryAdjustment> LoadClaimInventoryAdjustments(int claimId)
        {
            return claimsRepository.LoadClaimInventoryAdjustments(claimId);
        }

        private void UpdateGpBatchForClaim(int claimId)
        {
            var needPullBatch = claimsRepository.IsClaimPosted(claimId);
            if (needPullBatch)
            {
                var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);
                if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
                {
                    var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
                    var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
                    if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
                    {
                        claimsRepository.UpdateClaimPaymentSummary(gpPaymentSummary, claimId);
                    }
                }
            }
        }

        #region submit claim rules
        public HaulerSubmitClaimViewModel HaulerClaimSubmitBusinessRule(HaulerSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>{
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel}
            };

            var updatedSubmitClaimForError = HaulerClaimSubmitErrors(claim, ruleContext, submitClaimModel);
            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = HaulerClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;
        }

        private HaulerSubmitClaimViewModel HaulerClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, HaulerSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            this.BusinessRuleSet.AddBusinessRule(new IncompleteAndSyncedRule());
            this.BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);
            return submitClaimModel;

        }

        private void ExecuteBusinessRuleForErrors(HaulerSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }

        private HaulerSubmitClaimViewModel HaulerClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, HaulerSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new HaulerNilActivityClaimsRule());
            this.BusinessRuleSet.AddBusinessRule(new HaulerNegativeClosingInventoryRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }

        #endregion

        #endregion

        #region private methods
        private void PopulateClawbackDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, DateTime claimPeriodDate)
        {
            haulerPayment.IneligibleInventoryPaymentDetail = new ClawbackViewModel();
            haulerPayment.IneligibleInventoryPaymentDetail.UsingOldRounding = haulerPayment.UsingOldRounding;

            //Populate Rate
            //2. Get rate based on pickupzone and delivery zone
            PopulateClawbackRate(haulerPayment, claimPeriodDate);

            var clawbackQuery = claimPayments.Where(c => c.PaymentType == 3).ToList();
            clawbackQuery.ForEach(c =>
            {
                if (c.DeliveryZone == TreadMarksConstants.MooseCreek && c.ItemType == 1)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.MooseCreekOnRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.MooseCreakOnRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.MooseCreek && c.ItemType == 2)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.MooseCreekOffRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.MooseCreakOffRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.GTA && c.ItemType == 1)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.GTAOnRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.GTAOnRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.GTA && c.ItemType == 2)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.GTAOffRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.GTAOffRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.WTC && c.ItemType == 1)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.WTCOnRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.WTCOnRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.WTC && c.ItemType == 2)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.WTCOffRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.WTCOffRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 1)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOnRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOnRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 2)
                {
                    haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOffRoadWeight = c.Weight;
                    haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOffRoadRate = c.Rate;
                }
            });
        }

        private void PopulateClawbackRate(HaulerPaymentViewModel haulerPayment, DateTime claimPeriodDate)
        {
            //3. Get Rate
            var mooseCreekZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.MooseCreek).ID;
            var gtaZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.GTA).ID;
            var wtcZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.WTC).ID;
            var sturgeonFallsZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.SturgeonFalls).ID;

            var mooseCreekZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == mooseCreekZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneOnRoadRate = mooseCreekZoneOnRoadItemRate != null ? mooseCreekZoneOnRoadItemRate.ItemRate : 0;
            var mooseCreekZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == mooseCreekZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneOffRoadRate = mooseCreekZoneOffRoadItemRate != null ? mooseCreekZoneOffRoadItemRate.ItemRate : 0;

            var gtaZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == gtaZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneOnRoadRate = gtaZoneOnRoadItemRate != null ? gtaZoneOnRoadItemRate.ItemRate : 0;
            var gtaZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == gtaZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneOffRoadRate = gtaZoneOffRoadItemRate != null ? gtaZoneOffRoadItemRate.ItemRate : 0;

            var wtcZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == wtcZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneOnRoadRate = wtcZoneOnRoadItemRate != null ? wtcZoneOnRoadItemRate.ItemRate : 0;
            var wtcZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == wtcZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneOffRoadRate = wtcZoneOffRoadItemRate != null ? wtcZoneOffRoadItemRate.ItemRate : 0;

            var sturgeonFallsZoneOnRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == sturgeonFallsZoneId && c.ItemType == 1 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneOnRoadRate = sturgeonFallsZoneOnRoadItemRate != null ? sturgeonFallsZoneOnRoadItemRate.ItemRate : 0;
            var sturgeonFallsZoneOffRoadItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 3 && c.ClaimType == 4 && c.DeliveryZoneID == sturgeonFallsZoneId && c.ItemType == 2 && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneOffRoadRate = sturgeonFallsZoneOffRoadItemRate != null ? sturgeonFallsZoneOffRoadItemRate.ItemRate : 0;

            haulerPayment.IneligibleInventoryPaymentDetail.MooseCreakOnRoadRate = mooseCreekZoneOnRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.MooseCreakOffRoadRate = mooseCreekZoneOffRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.GTAOnRoadRate = gtaZoneOnRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.GTAOffRoadRate = gtaZoneOffRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.WTCOnRoadRate = wtcZoneOnRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.WTCOffRoadRate = wtcZoneOffRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOnRoadRate = sturgeonFallsZoneOnRoadRate;
            haulerPayment.IneligibleInventoryPaymentDetail.SturgeonFallsOffRoadRate = sturgeonFallsZoneOffRoadRate;
        }

        private void PopulateDOTPremiumDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, DateTime claimPeriodDate)
        {
            haulerPayment.DOTPremiumDetail = new DOTPremiumViewModel();
            haulerPayment.DOTPremiumDetail.UsingOldRounding = haulerPayment.UsingOldRounding;

            //Populate Rate
            //2. Get rate based on pickupzone and delivery zone
            PopulateDOTPremiumRate(haulerPayment, claimPeriodDate);

            var dotPremiumQuery = claimPayments.Where(c => c.PaymentType == 2).ToList();
            dotPremiumQuery.ForEach(c =>
            {
                if (c.SourceZone == TreadMarksConstants.N1)
                {
                    haulerPayment.DOTPremiumDetail.N1OffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.N1OffRoadRate = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N2)
                {
                    haulerPayment.DOTPremiumDetail.N2OffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.N2OffRoadRate = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N3)
                {
                    haulerPayment.DOTPremiumDetail.N3OffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.N3OffRoadRate = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N4)
                {
                    haulerPayment.DOTPremiumDetail.N4OffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.N4OffRoadRate = c.Rate;
                }

                if (c.DeliveryZone == TreadMarksConstants.MooseCreek)
                {
                    haulerPayment.DOTPremiumDetail.MooseCreekOffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.MooseCreekOffRoadRate = c.Rate;
                }

                if (c.DeliveryZone == TreadMarksConstants.GTA)
                {
                    haulerPayment.DOTPremiumDetail.GTAOffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.GTAOffRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.WTC)
                {
                    haulerPayment.DOTPremiumDetail.WTCOffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.WTCOffRoadRate = c.Rate;
                }
                if (c.DeliveryZone == TreadMarksConstants.SturgeonFalls)
                {
                    haulerPayment.DOTPremiumDetail.SturgeonFallsOffRoadWeight = c.Weight;
                    haulerPayment.DOTPremiumDetail.SturgeonFallsOffRoadRate = c.Rate;
                }
            });
        }

        private void PopulateDOTPremiumRate(HaulerPaymentViewModel haulerPayment, DateTime claimPeriodDate)
        {
            //2. Get Rate
            var n1ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N1).ID;
            var n2ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N2).ID;
            var n3ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N3).ID;
            var n4ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N4).ID;

            var southZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.South).ID;
            var mooseCreekZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.MooseCreek).ID;
            var gtaZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.GTA).ID;
            var wtcZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.WTC).ID;
            var sturgeonFallsZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 2 && c.Name == TreadMarksConstants.SturgeonFalls).ID;

            //For northern pick ups, rate will be based on Pick up zone only.
            var n1ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n1ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1ZoneRate = n1ZoneItemRate != null ? n1ZoneItemRate.ItemRate : 0;
            var n2ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n2ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2ZoneRate = n2ZoneItemRate != null ? n2ZoneItemRate.ItemRate : 0;
            var n3ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n3ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3ZoneRate = n3ZoneItemRate != null ? n3ZoneItemRate.ItemRate : 0;
            var n4ZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == n4ZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4ZoneRate = n4ZoneItemRate != null ? n4ZoneItemRate.ItemRate : 0;

            // For South DOT pick ups, rate will be based on pickup zone and drop off zone.
            var mooseCreekZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == mooseCreekZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var mooseCreekZoneRate = mooseCreekZoneItemRate != null ? mooseCreekZoneItemRate.ItemRate : 0;
            var gtaZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == gtaZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var gtaZoneRate = gtaZoneItemRate != null ? gtaZoneItemRate.ItemRate : 0;
            var wtcZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == wtcZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var wtcZoneRate = wtcZoneItemRate != null ? wtcZoneItemRate.ItemRate : 0;
            var sturgeonFallsZoneItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 2 && c.ClaimType == 3 && c.SourceZoneID == southZoneId && c.DeliveryZoneID == sturgeonFallsZoneId && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var sturgeonFallsZoneRate = sturgeonFallsZoneItemRate != null ? sturgeonFallsZoneItemRate.ItemRate : 0;

            haulerPayment.DOTPremiumDetail.N1OffRoadRate = n1ZoneRate;
            haulerPayment.DOTPremiumDetail.N2OffRoadRate = n2ZoneRate;
            haulerPayment.DOTPremiumDetail.N3OffRoadRate = n3ZoneRate;
            haulerPayment.DOTPremiumDetail.N4OffRoadRate = n4ZoneRate;
            haulerPayment.DOTPremiumDetail.MooseCreekOffRoadRate = mooseCreekZoneRate;
            haulerPayment.DOTPremiumDetail.GTAOffRoadRate = gtaZoneRate;
            haulerPayment.DOTPremiumDetail.WTCOffRoadRate = wtcZoneRate;
            haulerPayment.DOTPremiumDetail.SturgeonFallsOffRoadRate = sturgeonFallsZoneRate;

        }

        private void PopulateNorthernPremiumDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, DateTime claimPeriodDate)
        {
            //Northern Premium Details
            haulerPayment.NorthernPremiumDetail = new NorthernPremiumViewModel();
            haulerPayment.NorthernPremiumDetail.UsingOldRounding = haulerPayment.UsingOldRounding;

            //Populate Rate
            //2. Get rate based on pickupzone and delivery zone
            PopulateNorthernPremiumRate(haulerPayment, claimPeriodDate);

            var northernPremiumQuery = claimPayments.Where(c => c.PaymentType == 1).ToList();
            northernPremiumQuery.ForEach(c =>
            {
                if (c.SourceZone == TreadMarksConstants.N1 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N1SturgeonFallWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N1SturgeonFallRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N1 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N1SturgeonFallWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N1SturgeonFallRateOffRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N1 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N1SouthWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N1SouthRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N1 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N1SouthWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N1SouthRateOffRoad = c.Rate;
                }

                if (c.SourceZone == TreadMarksConstants.N2 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N2SturgeonFallWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N2SturgeonFallRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N2 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N2SturgeonFallWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N2SturgeonFallRateOffRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N2 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N2SouthWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N2SouthRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N2 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N2SouthWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N2SouthRateOffRoad = c.Rate;
                }

                if (c.SourceZone == TreadMarksConstants.N3 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N3SturgeonFallWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N3SturgeonFallRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N3 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N3SturgeonFallWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N3SturgeonFallRateOffRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N3 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N3SouthWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N3SouthRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N3 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N3SouthWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N3SouthRateOffRoad = c.Rate;
                }

                if (c.SourceZone == TreadMarksConstants.N4 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N4SturgeonFallWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N4SturgeonFallRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N4 && c.DeliveryZone == TreadMarksConstants.SturgeonFalls && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N4SturgeonFallWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N4SturgeonFallRateOffRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N4 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 1)
                {
                    haulerPayment.NorthernPremiumDetail.N4SouthWeightOnRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N4SouthRateOnRoad = c.Rate;
                }
                if (c.SourceZone == TreadMarksConstants.N4 && c.DeliveryZone == TreadMarksConstants.South && c.ItemType == 2)
                {
                    haulerPayment.NorthernPremiumDetail.N4SouthWeightOffRoad = c.Weight;
                    haulerPayment.NorthernPremiumDetail.N4SouthRateOffRoad = c.Rate;
                }
            });
        }

        private void PopulateNorthernPremiumRate(HaulerPaymentViewModel haulerPayment, DateTime claimPeriodDate)
        {
            var n1ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N1).ID;
            var n2ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N2).ID;
            var n3ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N3).ID;
            var n4ZoneId = DataLoader.Zones.FirstOrDefault(c => c.ZoneType == 1 && c.Name == TreadMarksConstants.N4).ID;

            var n1SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SturgeonFallRate = n1SturgeonFallItemRate != null ? n1SturgeonFallItemRate.ItemRate : 0;
            var n2SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SturgeonFallRate = n2SturgeonFallItemRate != null ? n2SturgeonFallItemRate.ItemRate : 0;
            var n3SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SturgeonFallRate = n3SturgeonFallItemRate != null ? n3SturgeonFallItemRate.ItemRate : 0;
            var n4SturgeonFallItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SturgeonFallRate = n4SturgeonFallItemRate != null ? n4SturgeonFallItemRate.ItemRate : 0;

            var n1SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SouthRate = n1SouthItemRate != null ? n1SouthItemRate.ItemRate : 0;
            var n2SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SouthRate = n2SouthItemRate != null ? n2SouthItemRate.ItemRate : 0;
            var n3SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SouthRate = n3SouthItemRate != null ? n3SouthItemRate.ItemRate : 0;
            var n4SouthItemRate = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 1 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SouthRate = n4SouthItemRate != null ? n4SouthItemRate.ItemRate : 0;

            var n1SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n1ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n1SouthRateOffRoad = n1SouthItemRateOffRoad != null ? n1SouthItemRateOffRoad.ItemRate : 0;
            var n2SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n2ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n2SouthRateOffRoad = n2SouthItemRateOffRoad != null ? n2SouthItemRateOffRoad.ItemRate : 0;
            var n3SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n3ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n3SouthRateOffRoad = n3SouthItemRateOffRoad != null ? n3SouthItemRateOffRoad.ItemRate : 0;
            var n4SouthItemRateOffRoad = DataLoader.Rates.FirstOrDefault(c => c.PaymentType == 1 && c.ClaimType == 3 && c.ItemType == 2 && c.SourceZoneID == n4ZoneId && c.GreaterZone == TreadMarksConstants.South && c.EffectiveStartDate <= claimPeriodDate && c.EffectiveEndDate >= claimPeriodDate);
            var n4SouthRateOffRoad = n4SouthItemRateOffRoad != null ? n4SouthItemRateOffRoad.ItemRate : 0;

            haulerPayment.NorthernPremiumDetail.N1SturgeonFallRateOnRoad = n1SturgeonFallRate;
            haulerPayment.NorthernPremiumDetail.N1SouthRateOnRoad = n1SouthRate;
            haulerPayment.NorthernPremiumDetail.N1SouthRateOffRoad = n1SouthRateOffRoad;
            haulerPayment.NorthernPremiumDetail.N2SturgeonFallRateOnRoad = n2SturgeonFallRate;
            haulerPayment.NorthernPremiumDetail.N2SouthRateOnRoad = n2SouthRate;
            haulerPayment.NorthernPremiumDetail.N2SouthRateOffRoad = n2SouthRateOffRoad;
            haulerPayment.NorthernPremiumDetail.N3SturgeonFallRateOnRoad = n3SturgeonFallRate;
            haulerPayment.NorthernPremiumDetail.N3SouthRateOnRoad = n3SouthRate;
            haulerPayment.NorthernPremiumDetail.N3SouthRateOffRoad = n3SouthRateOffRoad;
            haulerPayment.NorthernPremiumDetail.N4SturgeonFallRateOnRoad = n4SturgeonFallRate;
            haulerPayment.NorthernPremiumDetail.N4SouthRateOnRoad = n4SouthRate;
            haulerPayment.NorthernPremiumDetail.N4SouthRateOffRoad = n4SouthRateOffRoad;
        }

        public void UpdateYardCountSubmission(int claimId, ItemRow itemRow)
        {
            var result = claimsRepository.LoadYardCountSubmission(claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (result.Count > 0)
            {
                result.ForEach(c =>
                {
                    var item = DataLoader.TransactionItems.Single(o => o.ID == c.ItemId);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate);
                    foreach (KeyValuePair<string, int> pair in itemRow.Cast<KeyValuePair<string, int>>())
                    {
                        if (pair.Key == item.ShortName)
                        {
                            c.Quantity = pair.Value;
                            c.Weight = c.Quantity * itemWeight.StandardWeight;
                        }
                    }
                });
                claimsRepository.SaveContext();
            }
            else
            {
                var itemList = new List<ClaimYardCountSubmission>();
                foreach (KeyValuePair<string, int> pair in itemRow.Cast<KeyValuePair<string, int>>())
                {
                    var claimYardCountSubmission = new ClaimYardCountSubmission();
                    var item = DataLoader.TransactionItems.Single(c => c.ShortName == pair.Key);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate);
                    claimYardCountSubmission.ClaimId = claimId;
                    claimYardCountSubmission.ItemId = item.ID;
                    claimYardCountSubmission.Quantity = pair.Value;
                    claimYardCountSubmission.Weight = pair.Value * itemWeight.StandardWeight;
                    claimYardCountSubmission.UpdatedBy = SecurityContextHelper.CurrentUser.Id;
                    claimYardCountSubmission.UpdatedDate = DateTime.UtcNow;
                    itemList.Add(claimYardCountSubmission);
                }
                claimsRepository.AddClaimYardCountSubmission(itemList);
            }
        }

        private void PopulatPaymentAdjustments(HaulerPaymentViewModel haulerPayment, ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => haulerPayment.UsingOldRounding ? Math.Round(i.Rate * i.Weight, 2) : Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.NorthernPremium)
                {
                    haulerPayment.NorthernPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.DOTPremium)
                {
                    haulerPayment.DOTPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.HaulerPTR)
                {
                    haulerPayment.HaulerPTR = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPayment = 0 - c.Amount;
                }
            });

            var adjustmentQuery = claimPaymentAdjustments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                AdjustmentAmount = Math.Round(c.Sum(i => i.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)
            });
            adjustmentQuery.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPaymentAdjust = c.AdjustmentAmount;
                }
                else
                {
                    haulerPayment.PaymentAdjustment += c.AdjustmentAmount;
                }
            });
            haulerPayment.isStaff = SecurityContextHelper.IsStaff();
            if (claimPaymentSummay != null)
            {
                haulerPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                haulerPayment.PaidDate = claimPaymentSummay.PaidDate;
                haulerPayment.MailedDate = claimPaymentSummay.MailDate;
                haulerPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                haulerPayment.BatchNumber = claimPaymentSummay.BatchNumber;
            }
        }

        private void PopulateWeightVariance(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            decimal actualTotal = 0;
            var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR").ToList();
            ptrClaimDetails.ForEach(c =>
            {
                actualTotal += c.TransactionItems.Sum(o => o.ActualWeight);
            });

            decimal estimatedTotal = 0;
            ptrClaimDetails.ForEach(c =>
            {
                estimatedTotal += c.TransactionItems.Sum(o => o.AverageWeight);
            });

            if (estimatedTotal == 0)
                haulerClaimSummary.InventoryAdjustment.WeightVariance = 0;
            else
                haulerClaimSummary.InventoryAdjustment.WeightVariance = Math.Round(((actualTotal - estimatedTotal) /
                                                                     estimatedTotal) * 100, 2, MidpointRounding.AwayFromZero);
        }

        private void PopulateClaimYardCountSubmission(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimYardCountSubmission> claimYardCountSubmissions)
        {
            haulerClaimSummary.InventoryAdjustment.ItemSubmission = new ItemRow();
            claimYardCountSubmissions.ForEach(c =>
            {
                var item = DataLoader.Items.FirstOrDefault(i => i.ID == c.ItemId);
                var propertyInfo = haulerClaimSummary.InventoryAdjustment.ItemSubmission.GetType().GetProperty(item.ShortName);
                propertyInfo.SetValue(haulerClaimSummary.InventoryAdjustment.ItemSubmission, c.Quantity, null);
            });
        }

        private void PopulateInventoryAdjustment(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            var query = inventoryAdjustments.GroupBy(c => new { c.IsEligible, c.Direction }).Select(c => new
            {
                Name = c.Key,
                AdjustmmentOnroad = c.Sum(i => i.AdjustmentWeightOnroad),
                AdjustmmentOffroad = c.Sum(i => i.AdjustmentWeightOffroad)
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.Direction == 1)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentInboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentInboundOffRoad = c.AdjustmmentOffroad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 2)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOutboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOutboundOffRoad = c.AdjustmmentOffroad;
                }

                if (c.Name.IsEligible && c.Name.Direction == 0)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOverallOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOverallOffRoad = c.AdjustmmentOffroad;
                }

                if (!c.Name.IsEligible && c.Name.Direction == 1)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentInboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentInboundOffRoad = c.AdjustmmentOffroad;

                }
                if (!c.Name.IsEligible && c.Name.Direction == 2)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOutboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOutboundOffRoad = c.AdjustmmentOffroad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 0)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOverallOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOverallOffRoad = c.AdjustmmentOffroad;
                }
            });
        }

        private void PopulateInboundOutbound(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            var query = claimDetails
              .GroupBy(c => new { c.TransactionType, c.Direction })
              .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.EstimatedOnRoad), OffRoad = c.Sum(i => i.EstimatedOffRoad) });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.TCR && c.Name.Direction)
                {
                    haulerClaimSummary.TCROnRoad = c.OnRoad;
                    haulerClaimSummary.TCROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOT && c.Name.Direction)
                {
                    haulerClaimSummary.DOTOnRoad = c.OnRoad;
                    haulerClaimSummary.DOTOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.STC && c.Name.Direction)
                {
                    haulerClaimSummary.STCOnRoad = c.OnRoad;
                    haulerClaimSummary.STCOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.UCR && c.Name.Direction)
                {
                    haulerClaimSummary.UCROnRoad = c.OnRoad;
                    haulerClaimSummary.UCROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.HIT && c.Name.Direction)
                {
                    haulerClaimSummary.HITOnRoad = c.OnRoad;
                    haulerClaimSummary.HITOffRoad = c.OffRoad;
                }
                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && !c.Name.Direction)
                {
                    haulerClaimSummary.PTROnRoad = c.OnRoad;
                    haulerClaimSummary.PTROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.HIT && !c.Name.Direction)
                {
                    haulerClaimSummary.HITOutboundOnRoad = c.OnRoad;
                    haulerClaimSummary.HITOutboundOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.RTR && !c.Name.Direction)
                {
                    haulerClaimSummary.RTROnRoad = c.OnRoad;
                    haulerClaimSummary.RTROffRoad = c.OffRoad;
                }

            });
        }

        private void PopulateClaimTireBalance(HaulerClaimSummaryModel haulerClaimSummary, List<InventoryItem> inventoryItems)
        {
            haulerClaimSummary.InventoryAdjustment.TireCountBalance = new ItemRow();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            var query = inventoryItems
                        .GroupBy(c => new { c.ItemId, c.Direction })
                        .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty) });

            query.ToList().ForEach(c =>
            {
                if (c.Name.ItemId == pltItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT -= c.Qty;
                }
                if (c.Name.ItemId == mtItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MT -= c.Qty;
                }

                if (c.Name.ItemId == aglsItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS -= c.Qty;
                }
                if (c.Name.ItemId == indItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.IND += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.IND -= c.Qty;
                }
                if (c.Name.ItemId == sotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR -= c.Qty;
                }
                if (c.Name.ItemId == motrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR -= c.Qty;
                }
                if (c.Name.ItemId == lotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR -= c.Qty;
                }
                if (c.Name.ItemId == gotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR -= c.Qty;
                }

            });
        }

        private void PopulateClaimTireBalanceWithoutAdj(HaulerClaimSummaryModel haulerClaimSummary, List<InventoryItem> inventoryItems)
        {
            haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj = new ItemRow();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);


            //OTSTM2-489  it will display the Tire Count Balance before the yard
            //count adjustment
            var query = inventoryItems.Where(c => c.IsAdjustedItem != true)
                        .GroupBy(c => new { c.ItemId, c.Direction })
                        .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty) });


            query.ToList().ForEach(c =>
            {
                if (c.Name.ItemId == pltItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.PLT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.PLT -= c.Qty;
                }
                if (c.Name.ItemId == mtItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MT -= c.Qty;
                }

                if (c.Name.ItemId == aglsItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.AGLS += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.AGLS -= c.Qty;
                }
                if (c.Name.ItemId == indItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.IND += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.IND -= c.Qty;
                }
                if (c.Name.ItemId == sotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.SOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.SOTR -= c.Qty;
                }
                if (c.Name.ItemId == motrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MOTR -= c.Qty;
                }
                if (c.Name.ItemId == lotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.LOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.LOTR -= c.Qty;
                }
                if (c.Name.ItemId == gotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.GOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.GOTR -= c.Qty;
                }

            });
        }

        #endregion

        #region GP Integration

        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();

            // STEP 1 GET CLAIMS THAT ARE APPROVED AND NOT IN A BATCH	
            var claimType = (int)ClaimType.Hauler;
            var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, true);
            var allClaimIds = allClaims.Select(c => c.ID).ToList();
            //Remove claim if it is banking status is not approved
            var claims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

            if (!claims.Any())
            {
                msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
            }
            else
            {
                //STEP 2 INSERT ENTRY INTO GpiBatch Table
                var gpiBatch = new GpiBatch()
                {
                    GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Hauler),
                    GpiBatchCount = claims.Count(),
                    GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                };

                var minPostDate = gpRepository.GetAllGpFiscalYears()
                    .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                    .Select(d => d.MinimumDocDate).FirstOrDefault();
                var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
                var claimIds = claims.Select(c => c.ID).ToList();

                var transactionTypes = new List<string> { "TCR", "DOT", "STC", "PTR" };
                var items = DataLoader.Items;
                var claimDetails = claimsRepository.LoadClaimDetailsForGP(claimIds, transactionTypes, items);
                var gpiAccounts = DataLoader.GPIAccounts;

                //Update database
                using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //1. GP Batch
                    gpRepository.AddGpBatch(gpiBatch);
                    msg.ID = gpiBatch.ID;
                    //2. GP Batch entries
                    var gpiBatchEntries = AddBatchEntries(claims, gpiBatch.ID);
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    //3. GP Transactions and Transaction Distributions
                    var rrOtsPmTransaction = AddGPTransactions(claims, gpiBatch.ID, gpiBatchEntries, minPostDate, INTERID,
                        claimDetails, gpiAccounts);
                    gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransaction.Keys.ToList());
                    var rrOtsTransDistributions = new List<RrOtsPmTransDistribution>();
                    rrOtsPmTransaction.Values.ToList().ForEach(c =>
                    {
                        rrOtsTransDistributions.AddRange(c);
                    });
                    gpRepository.AddGpRrOtsPmTransDistributions(rrOtsTransDistributions);

                    //4. Update claim
                    claimsRepository.UpdateClaimsForGPBatch(claimIds, gpiBatch.ID.ToString());
                    transactionScope.Complete();
                }
            }
            return msg;
        }

        private Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>> AddGPTransactions(List<Claim> claims, int gpiBatchId, List<GpiBatchEntry> gpiBatchEntries, DateTime minPostDate, string INTERID, List<ClaimDetailViewModel> claimDetails, List<GpiAccount> gpiAccounts)
        {
            //Step 4 Add RM_Transaction
            var result = new Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>>();
            var rrOtsPmTransaction = new List<RrOtsPmTransaction>();

            claims.ForEach(c =>
            {
                var haulerPaymentViewModel = LoadHaulerPaymentForGP(c.ID);
                //DOCType=1
                var prchAmount1 = Math.Round(haulerPaymentViewModel.NorthernPremium + haulerPaymentViewModel.DOTPremium + haulerPaymentViewModel.HaulerPTR +
                    (haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust > 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0), 2, MidpointRounding.AwayFromZero);
                var prchAmount1ForTax = haulerPaymentViewModel.NorthernPremium + haulerPaymentViewModel.DOTPremium + haulerPaymentViewModel.HaulerPTR +
                    (haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0);
                var prchTax1 = Math.Round(prchAmount1ForTax * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var distributedPaymentAdjustment = Math.Round((haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust > 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0), 2, MidpointRounding.AwayFromZero);
                var distributedPaymentAdjustmentTax = Math.Round((haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var positiveTotal = prchAmount1 + prchTax1;

                //DOCType=5
                var prchAmount5 = Math.Round(haulerPaymentViewModel.IneligibleInventoryPayment + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust < 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0) + (haulerPaymentViewModel.PaymentAdjustment < 0 ? haulerPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                var prchTax5 = Math.Round((haulerPaymentViewModel.PaymentAdjustment < 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var gpTotal = positiveTotal + prchAmount5 + prchTax5;

                //Fixed cent difference caused by rounding
                if (gpTotal != haulerPaymentViewModel.GrandTotal)
                {
                    var difference = (gpTotal - haulerPaymentViewModel.GrandTotal);
                    //put all difference in prchAmount1
                    prchAmount1 -= difference;
                }


                var gpiBatchEntryId = gpiBatchEntries.Single(q => q.ClaimId == c.ID).ID;
                int dstSeqNum = 1;

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                var typeOneTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    DOCTYPE = 1,
                    DOCAMNT = prchAmount1 + prchTax1,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = (haulerPaymentViewModel.HST == 0 && haulerPaymentViewModel.IsTaxExempt) ? "P-EXEMPT" : "P-HST",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount1,
                    TAXAMNT = prchTax1,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };

                if (prchAmount1 + prchTax1 != 0)
                {
                    //Add transaction distribution
                    var currentClaimDetails = claimDetails.Where(q => q.ClaimId == c.ID).ToList();
                    var typeOneDistrubutions = AddTypeOneDistributions(
                        gpiBatchId,
                        INTERID,
                        prchAmount1 + prchTax1,
                        haulerPaymentViewModel,
                        //haulerPaymentViewModel.NorthernPremium, 
                        //haulerPaymentViewModel.DOTPremium, 
                        //haulerPaymentViewModel.HSTBase, 
                        distributedPaymentAdjustment,
                        distributedPaymentAdjustmentTax,
                        typeOneTransaction,
                        gpiAccounts,
                        currentClaimDetails,
                        c.ClaimPeriod.StartDate,
                        ref dstSeqNum);
                    result.Add(typeOneTransaction, typeOneDistrubutions);
                }

                var typeFiveTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    DOCTYPE = 5,
                    DOCAMNT = prchAmount5 + prchTax5,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = (haulerPaymentViewModel.HST == 0 && haulerPaymentViewModel.IsTaxExempt) ? "P-EXEMPT" : "P-HST",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount5,
                    TAXAMNT = prchTax5,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };
                if (prchAmount5 + prchTax5 != 0)
                {
                    //Add transaction distribution
                    var typeFiveDistrubutions = AddTypeFiveDistributions(gpiBatchId, INTERID, prchAmount5, prchTax5, typeFiveTransaction, gpiAccounts, ref dstSeqNum);
                    result.Add(typeFiveTransaction, typeFiveDistrubutions);
                }
            });
            return result;
        }
        private List<RrOtsPmTransDistribution> AddTypeOneDistributions(
            int gpiBatchId,
            string INTERID,
            decimal positiveTotal,
            HaulerPaymentViewModel paymentVM,
            //decimal nortnernPremium, 
            //decimal dotPremium, 
            //decimal hstBase, 
            decimal distributedPaymentAdjustment,
            decimal distributedPaymentAdjustmentTax,
            RrOtsPmTransaction typeOneTransaction,
            List<GpiAccount> gpiAccounts,
            List<ClaimDetailViewModel> currentClaimDetails,
            DateTime claimStartDate,
            ref int dstSeqNum)
        {
            var northernPremiumTax = Math.Round(paymentVM.NorthernPremium * paymentVM.HSTBase, 2, MidpointRounding.AwayFromZero);
            var dotPremiumTax = Math.Round(paymentVM.DOTPremium * paymentVM.HSTBase, 2, MidpointRounding.AwayFromZero);
            var haulerPTRTax = Math.Round(paymentVM.HaulerPTR * paymentVM.HSTBase, 2, MidpointRounding.AwayFromZero);
            var distributedAmount = paymentVM.NorthernPremium
                + paymentVM.DOTPremium
                + paymentVM.HaulerPTR
                + northernPremiumTax
                + dotPremiumTax
                + haulerPTRTax;

            var grandTotal = positiveTotal - distributedPaymentAdjustment - distributedPaymentAdjustmentTax;

            //Fixed cent difference caused by rounding
            if (distributedAmount != grandTotal)
            {
                var difference = (distributedAmount - grandTotal);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < 2)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        if (i == 0)
                            northernPremiumTax -= (difference / numberOfDifferenceDistribution);
                        else
                            dotPremiumTax -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }

            distributedAmount = grandTotal;

            var typeOneDistrubutions = new List<RrOtsPmTransDistribution>();
            var accountParams = new gpiAccountParameterDTO(gpiAccounts);

            if (distributedAmount + distributedPaymentAdjustment + distributedPaymentAdjustmentTax > 0)
            {
                var rowAccountPayrable = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)accountParams.apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = accountParams.apAccount.account_number;
                rowAccountPayrable.DEBITAMT = 0;
                rowAccountPayrable.CRDTAMNT = distributedAmount + distributedPaymentAdjustment + distributedPaymentAdjustmentTax;
                typeOneDistrubutions.Add(rowAccountPayrable);
            }

            if (distributedAmount != 0)
            {
                //Norththern Premium
                dstSeqNum = addNorthernPremium(gpiBatchId, INTERID, paymentVM, typeOneTransaction, currentClaimDetails, claimStartDate, dstSeqNum, northernPremiumTax, typeOneDistrubutions, accountParams);
                //DOT Premium
                dstSeqNum = addDOTPremium(gpiBatchId, INTERID, paymentVM, typeOneTransaction, currentClaimDetails, claimStartDate, dstSeqNum, dotPremiumTax, typeOneDistrubutions, accountParams);
                //Hauler PRT
                dstSeqNum = addHaulerPTR(gpiBatchId, INTERID, paymentVM, typeOneTransaction, currentClaimDetails, claimStartDate, dstSeqNum, haulerPTRTax, typeOneDistrubutions, accountParams);

                //AddTaxDistribution for Northern Premium and PTRs, Sum in each tire type distribution
                dstSeqNum = AddTaxDistribution(gpiBatchId, INTERID, paymentVM, typeOneTransaction, currentClaimDetails, claimStartDate, dstSeqNum, northernPremiumTax, haulerPTRTax, typeOneDistrubutions, accountParams);
            }
            dstSeqNum = addAdjustmentPayments(gpiBatchId, INTERID, distributedPaymentAdjustment, distributedPaymentAdjustmentTax, typeOneTransaction, dstSeqNum, typeOneDistrubutions, accountParams);
            return typeOneDistrubutions;
        }

        private int addAdjustmentPayments(int gpiBatchId, string INTERID, decimal distributedPaymentAdjustment, decimal distributedPaymentAdjustmentTax, RrOtsPmTransaction typeOneTransaction, int dstSeqNum, List<RrOtsPmTransDistribution> typeOneDistrubutions, gpiAccountParameterDTO accountParams)
        {
            if (distributedPaymentAdjustment > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)accountParams.tiAdjustment.distribution_type_id;
                rowDistribution.ACTNUMST = accountParams.tiAdjustment.account_number;
                rowDistribution.DEBITAMT = distributedPaymentAdjustment;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }

            if (distributedPaymentAdjustmentTax > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)accountParams.tiAdjustmentHST.distribution_type_id;
                rowDistribution.ACTNUMST = accountParams.tiAdjustmentHST.account_number;
                rowDistribution.DEBITAMT = distributedPaymentAdjustmentTax;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }

            return dstSeqNum;
        }
        private int addDOTPremium(int gpiBatchId, string INTERID, HaulerPaymentViewModel paymentVM, RrOtsPmTransaction typeOneTransaction, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, int dstSeqNum, decimal dotPremiumTax, List<RrOtsPmTransDistribution> typeOneDistrubutions, gpiAccountParameterDTO accountParams)
        {
            if (paymentVM.DOTPremium > 0)
            {
                var dotPremiumDistributions = new List<RrOtsPmTransDistribution>();
                var dotGpTireItem = FindTirePercentageForDOTPremium(currentClaimDetails, claimStartDate);
                if (dotGpTireItem.MOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.DOTMOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.DOTMOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.DOTPremium * dotGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    dotPremiumDistributions.Add(rowDistribution);
                }
                if (dotGpTireItem.LOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.DOTLOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.DOTLOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.DOTPremium * dotGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    dotPremiumDistributions.Add(rowDistribution);
                }
                if (dotGpTireItem.GOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.DOTGOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.DOTGOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.DOTPremium * dotGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    dotPremiumDistributions.Add(rowDistribution);
                }

                //Fixed cent difference caused by rounding
                var totalDOTAmount = dotPremiumDistributions.Sum(c => c.DEBITAMT);
                if (totalDOTAmount != paymentVM.DOTPremium)
                {
                    var difference = (totalDOTAmount - paymentVM.DOTPremium);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < dotPremiumDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            dotPremiumDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }
                typeOneDistrubutions.AddRange(dotPremiumDistributions);

                //DOT Tax
                var rowTaxDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTaxDistribution.DISTTYPE = (int)accountParams.DOTOTRHSTAccount.distribution_type_id;
                rowTaxDistribution.ACTNUMST = accountParams.DOTOTRHSTAccount.account_number;
                rowTaxDistribution.DEBITAMT = dotPremiumTax;
                rowTaxDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowTaxDistribution);
            }

            return dstSeqNum;
        }
        private int addNorthernPremium(int gpiBatchId, string INTERID, HaulerPaymentViewModel paymentVM, RrOtsPmTransaction typeOneTransaction, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, int dstSeqNum, decimal northernPremiumTax, List<RrOtsPmTransDistribution> typeOneDistrubutions, gpiAccountParameterDTO accountParams)
        {
            if (paymentVM.NorthernPremium > 0)
            {
                //Northern Premium distribution
                var northernPremiumDistributions = new List<RrOtsPmTransDistribution>();
                var northernGpTireItem = FindTirePercentageForNorthernPremium(currentClaimDetails, claimStartDate);
                //Add distribution records
                if (northernGpTireItem.PLTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PLTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PLTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.MTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.MTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.MTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.AGLSPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.AGLSAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.AGLSAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.INDPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.INDAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.INDAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.SOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.SOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.SOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.MOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.MOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.MOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.LOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.LOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.LOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.GOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.GOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.GOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.NorthernPremium * northernGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    northernPremiumDistributions.Add(rowDistribution);
                }

                //Fixed cent difference caused by rounding
                var totalAmount = northernPremiumDistributions.Sum(c => c.DEBITAMT);
                if (totalAmount != paymentVM.NorthernPremium)
                {
                    var difference = (totalAmount - paymentVM.NorthernPremium);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < northernPremiumDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            northernPremiumDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }

                typeOneDistrubutions.AddRange(northernPremiumDistributions);
            }

            return dstSeqNum;
        }
        private int AddTaxDistribution(int gpiBatchId, string INTERID, HaulerPaymentViewModel paymentVM, RrOtsPmTransaction typeOneTransaction, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, int dstSeqNum, decimal northernPremiumTax, decimal haulerPTRTax, List<RrOtsPmTransDistribution> typeOneDistrubutions, gpiAccountParameterDTO accountParams)
        {
            if (paymentVM.NorthernPremium > 0 || paymentVM.HaulerPTR > 0)
            {

                decimal DEBITAMT = 0;

                //Tax distribution
                var taxDistributions = new List<RrOtsPmTransDistribution>();

                var northernGpTireItem = FindTirePercentageForNorthernPremium(currentClaimDetails, claimStartDate);

                var haulerPTRGpTireItem = FindTirePercentageForHaulerPTR(currentClaimDetails, claimStartDate);

                //Add distribution records
                if ((northernGpTireItem.PLTPercentage > 0) || haulerPTRGpTireItem.PLTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PLTHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PLTHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }

                if (northernGpTireItem.MTPercentage > 0 || haulerPTRGpTireItem.MTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.MTHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.MTHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.AGLSPercentage > 0 || haulerPTRGpTireItem.AGLSPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.AGLSHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.AGLSHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.INDPercentage > 0 || haulerPTRGpTireItem.INDPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.INDHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.INDHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.SOTRPercentage > 0 || haulerPTRGpTireItem.SOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.SOTRHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.SOTRHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.MOTRPercentage > 0 || haulerPTRGpTireItem.MOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.MOTRHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.MOTRHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.LOTRPercentage > 0 || haulerPTRGpTireItem.LOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.LOTRHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.LOTRHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }
                if (northernGpTireItem.GOTRPercentage > 0 || haulerPTRGpTireItem.GOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.GOTRHSTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.GOTRHSTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero) + Math.Round(haulerPTRTax * haulerPTRGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    taxDistributions.Add(rowDistribution);
                }

                //Fixed cent difference caused by rounding
                var totalTaxAmount = taxDistributions.Sum(c => c.DEBITAMT);
                if (totalTaxAmount != (northernPremiumTax + haulerPTRTax))
                {
                    var difference = (totalTaxAmount - (northernPremiumTax + haulerPTRTax));
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < taxDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            taxDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }
                typeOneDistrubutions.AddRange(taxDistributions);
            }
            return dstSeqNum;
        }

        private int addHaulerPTR(int gpiBatchId, string INTERID, HaulerPaymentViewModel paymentVM, RrOtsPmTransaction typeOneTransaction, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, int dstSeqNum, decimal haulerPTRTax, List<RrOtsPmTransDistribution> typeOneDistrubutions, gpiAccountParameterDTO accountParams)
        {
            if (paymentVM.HaulerPTR > 0)
            {
                //Hauler PTR distribution
                var haulerPTRDistributions = new List<RrOtsPmTransDistribution>();
                var haulerPTRGpTireItem = FindTirePercentageForHaulerPTR(currentClaimDetails, claimStartDate);
                //Add distribution records
                if (haulerPTRGpTireItem.PLTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_PLTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_PLTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.MTPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_MTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_MTAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.AGLSPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_AGLSAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_AGLSAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.INDPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_INDAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_INDAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.SOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_SOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_SOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.MOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_MOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_MOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.LOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_LOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_LOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }
                if (haulerPTRGpTireItem.GOTRPercentage > 0)
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)accountParams.PTR_GOTRAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = accountParams.PTR_GOTRAccount.account_number;
                    rowDistribution.DEBITAMT = Math.Round(paymentVM.HaulerPTR * haulerPTRGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                    rowDistribution.CRDTAMNT = 0;
                    haulerPTRDistributions.Add(rowDistribution);
                }

                //Fixed cent difference caused by rounding
                var totalAmount = haulerPTRDistributions.Sum(c => c.DEBITAMT);
                if (totalAmount != paymentVM.HaulerPTR)
                {
                    var difference = (totalAmount - paymentVM.HaulerPTR);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < haulerPTRDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            haulerPTRDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }

                typeOneDistrubutions.AddRange(haulerPTRDistributions);
            }

            return dstSeqNum;
        }

        private GpTireItem FindTirePercentageForDOTPremium(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.Where(c => c.TransactionType == "DOT").ToList().ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private GpTireItem FindTirePercentageForNorthernPremium(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            //Add all TCR transaction
            currentClaimDetails.Where(c => c.TransactionType == "TCR").ToList().ForEach(c =>
                {
                    transactionItems.AddRange(c.TransactionItems);
                });

            //Add STC which has pickupzone from 'N'
            currentClaimDetails.Where(c => c.TransactionType == "STC").ToList().ForEach(c =>
            {
                var pickupZone = ClaimHelper.GetZoneByPostCode(c.Transaction.FromPostalCode, 1);
                if ((pickupZone != null) && (pickupZone.Name.StartsWith("N")))
                {
                    transactionItems.AddRange(c.TransactionItems);
                }
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private GpTireItem FindTirePercentageForHaulerPTR(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.Where(c => c.TransactionType == "PTR"
            //OTSTM2-1371 Hauler GP Distribution tire percentage don't separate the payees.(include hauler and processor both)
            // && transactionRepository.isDirectPayTransaction(c.Transaction.OutgoingId, c.Transaction.TransactionDate)
            ).ToList().ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });

            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }

        private List<RrOtsPmTransDistribution> AddTypeFiveDistributions(int gpiBatchId, string INTERID, decimal prchAmount5, decimal prchTax5, RrOtsPmTransaction typeFiveTransaction, List<GpiAccount> gpiAccounts, ref int dstSeqNum)
        {
            //TI Adjustment HST 4470-90-30-40  id=99
            //TI Adjustment 4475-90-30-40 id=104
            //Accounts Payable 2000-00-00-00 id=2
            var typeFiveDistrubutions = new List<RrOtsPmTransDistribution>();
            var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "H");
            var tiAdjustmentHST = gpiAccounts.Single(a => a.account_number == "4470-90-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            var tiAdjustment = gpiAccounts.Single(a => a.account_number == "4475-90-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            if (prchAmount5 != 0)
            {
                prchAmount5 = Math.Abs(prchAmount5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.account_number;
                rowAccountPayrable.DEBITAMT = prchAmount5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)tiAdjustment.distribution_type_id;
                rowTIAdjustment.ACTNUMST = tiAdjustment.account_number;
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchAmount5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            if (prchTax5 != 0)
            {
                prchTax5 = Math.Abs(prchTax5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.account_number;
                rowAccountPayrable.DEBITAMT = prchTax5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)tiAdjustmentHST.distribution_type_id;
                rowTIAdjustment.ACTNUMST = tiAdjustmentHST.account_number;
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchTax5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            return typeFiveDistrubutions;
        }
        private RrOtsPmTransDistribution GetTransDistribution(RrOtsPmTransaction gpTransaction, int gpiBatchId, string INTERID, int dstSeqNum)
        {
            var result = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = gpTransaction.VCHNUMWK,
                DOCTYPE = gpTransaction.DOCTYPE,
                VENDORID = gpTransaction.VENDORID,
                DSTSQNUM = dstSeqNum,
                DistRef = null,
                USERDEF1 = gpiBatchId.ToString(),
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null
            };
            return result;
        }
        private List<GpiBatchEntry> AddBatchEntries(List<Claim> claims, int gpiBatchId)
        {
            return claims.Select(c => new GpiBatchEntry
            {
                GpiBatchID = gpiBatchId,
                GpiTxnNumber = string.Format("{0}-{1}-{2:yyyy MM dd}", c.ID.ToString(), c.Participant.Number, c.ClaimPeriod.StartDate),
                GpiBatchEntryDataKey = c.ID.ToString(),
                GpiStatusID = "E",
                CreatedDate = DateTime.Now,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.Now,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                ClaimId = c.ID
            }).ToList();
        }
        private HaulerPaymentViewModel LoadHaulerPaymentForGP(int claimId)
        {
            var haulerPayment = new HaulerPaymentViewModel();
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.NorthernPremium)
                {
                    haulerPayment.NorthernPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.DOTPremium)
                {
                    haulerPayment.DOTPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.HaulerPTR)
                {
                    haulerPayment.HaulerPTR = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPayment = 0 - c.Amount;
                }
            });

            var adjustmentQuery = claimPaymentAdjustments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                AdjustmentAmount = Math.Round(c.Sum(i => i.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)
            });
            adjustmentQuery.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPaymentAdjust = c.AdjustmentAmount;
                }
                else
                {
                    haulerPayment.PaymentAdjustment += c.AdjustmentAmount;
                }
            });
            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                haulerPayment.HSTBase = 0.00m;
            }
            haulerPayment.IsTaxExempt = isTaxExempt;

            return haulerPayment;
        }
        #endregion

    }
    class gpiAccountParameterDTO
    {

        public GpiAccount apAccount { get; set; }
        //northern Premium                                  
        public GpiAccount PLTAccount { get; set; }
        public GpiAccount PLTHSTAccount { get; set; }
        public GpiAccount MTAccount { get; set; }
        public GpiAccount MTHSTAccount { get; set; }
        public GpiAccount AGLSAccount { get; set; }
        public GpiAccount AGLSHSTAccount { get; set; }
        public GpiAccount INDAccount { get; set; }
        public GpiAccount INDHSTAccount { get; set; }
        public GpiAccount SOTRAccount { get; set; }
        public GpiAccount SOTRHSTAccount { get; set; }
        public GpiAccount MOTRAccount { get; set; }
        public GpiAccount MOTRHSTAccount { get; set; }
        public GpiAccount LOTRAccount { get; set; }
        public GpiAccount LOTRHSTAccount { get; set; }
        public GpiAccount GOTRAccount { get; set; }
        public GpiAccount GOTRHSTAccount { get; set; }
        // DOT
        public GpiAccount DOTMOTRAccount { get; set; }
        public GpiAccount DOTLOTRAccount { get; set; }
        public GpiAccount DOTGOTRAccount { get; set; }
        public GpiAccount DOTOTRHSTAccount { get; set; }
        //adjustment  
        public GpiAccount tiAdjustmentHST { get; set; }
        public GpiAccount tiAdjustment { get; set; }
        //Hauler PTR account
        public GpiAccount PTR_PLTAccount { get; set; }
        public GpiAccount PTR_MTAccount { get; set; }
        public GpiAccount PTR_AGLSAccount { get; set; }
        public GpiAccount PTR_INDAccount { get; set; }
        public GpiAccount PTR_SOTRAccount { get; set; }
        public GpiAccount PTR_MOTRAccount { get; set; }
        public GpiAccount PTR_LOTRAccount { get; set; }
        public GpiAccount PTR_GOTRAccount { get; set; }


        public gpiAccountParameterDTO(List<GpiAccount> gpiAccounts)
        {
            apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "H");
            PLTAccount = gpiAccounts.Single(a => a.account_number == "4366-10-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            PLTHSTAccount = gpiAccounts.Single(a => a.account_number == "4382-10-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            MTAccount = gpiAccounts.Single(a => a.account_number == "4367-20-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            MTHSTAccount = gpiAccounts.Single(a => a.account_number == "4384-20-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            AGLSAccount = gpiAccounts.Single(a => a.account_number == "4368-30-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            AGLSHSTAccount = gpiAccounts.Single(a => a.account_number == "4387-30-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            INDAccount = gpiAccounts.Single(a => a.account_number == "4369-40-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            INDHSTAccount = gpiAccounts.Single(a => a.account_number == "4389-40-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            SOTRAccount = gpiAccounts.Single(a => a.account_number == "4371-50-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            SOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4398-50-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            MOTRAccount = gpiAccounts.Single(a => a.account_number == "4372-60-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            MOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4401-60-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            LOTRAccount = gpiAccounts.Single(a => a.account_number == "4373-70-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            LOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4403-70-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            GOTRAccount = gpiAccounts.Single(a => a.account_number == "4374-80-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            GOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4406-80-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            DOTMOTRAccount = gpiAccounts.Single(a => a.account_number == "4376-60-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            DOTLOTRAccount = gpiAccounts.Single(a => a.account_number == "4377-70-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            DOTGOTRAccount = gpiAccounts.Single(a => a.account_number == "4378-80-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            DOTOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4390-90-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            tiAdjustmentHST = gpiAccounts.Single(a => a.account_number == "4470-90-30-40" && a.registrant_type_ind.Trim().Contains("H"));
            tiAdjustment = gpiAccounts.Single(a => a.account_number == "4475-90-30-40" && a.registrant_type_ind.Trim().Contains("H"));

            PTR_PLTAccount = gpiAccounts.Single(a => a.account_number == "4241-10-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_MTAccount = gpiAccounts.Single(a => a.account_number == "4242-20-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_AGLSAccount = gpiAccounts.Single(a => a.account_number == "4243-30-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_INDAccount = gpiAccounts.Single(a => a.account_number == "4244-40-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_SOTRAccount = gpiAccounts.Single(a => a.account_number == "4361-50-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_MOTRAccount = gpiAccounts.Single(a => a.account_number == "4362-60-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_LOTRAccount = gpiAccounts.Single(a => a.account_number == "4363-70-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
            PTR_GOTRAccount = gpiAccounts.Single(a => a.account_number == "4364-80-40-40" && a.registrant_type_ind.Trim().Contains("PT"));
        }
    }
}