﻿using CL.TMS.ApplicaitonBLL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.System;
using CL.TMS.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.ApplicationServices
{
    public class ApplicationService : IApplicationService
    {
        private readonly ApplicationBO applicationBO;
        private IApplicationRepository applicationRepository;

        public ApplicationService(IApplicationRepository applicationRepository)
        {
            this.applicationBO = new ApplicationBO(applicationRepository);
        }

        public Application GetApplicationByID(int ID)
        {
            try
            {
                return applicationBO.GetApplicationByID(ID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Application AddApplication(Application application) 
        {
            applicationBO.AddApplication(application);
            return application;
        }
        public Application GetByID(int ID) 
        {
            //TODO 
            throw new NotImplementedException();
        
        }


    }
}
