﻿using CL.TMS.DataContracts.Communication;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace CL.TMS.DataSync.CommunicationData
{
    /// <summary>
    /// AssetModel model class.
    /// Used for IN and OUT self-validated, data structure parameter by the web services.
    /// </summary>
    [DataContract]
    public class AssetModel : IAssetModel
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string AssetTag { get; set; }
        [DataMember]
        public Nullable<int> VendorID { get; set; }
        [DataMember]
        public int AssetTypeUID { get; set; }
        [DataMember]
        public int AssetTypeID { get; set; }
        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
        [DataMember]
        public Nullable<DateTime> UpdatedOn { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }      
    }
}
