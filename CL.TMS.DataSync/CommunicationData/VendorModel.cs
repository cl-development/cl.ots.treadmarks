﻿using CL.TMS.DataContracts.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace CL.TMS.DataSync.CommunicationData
{
    /// <summary>
    /// Vendor model class.
    /// Used for IN and OUT self-validated, data structure parameter by the web services.
    /// </summary>
    [DataContract]
    public class VendorModel : IVendorModel
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int VendorTypeID { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string BusinessName { get; set; }
        [DataMember]
        public string FranchiseName { get; set; }
        [DataMember]
        public string OperatingName { get; set; }
        [DataMember]
        public string BusinessNumber { get; set; }
        [DataMember]
        public string TerritoryCode { get; set; }
        [DataMember]
        public string GlobalDimension1Code { get; set; }
        [DataMember]
        public string GlobalDimension2Code { get; set; }
        [DataMember]
        public Nullable<int> CreditLimit { get; set; }
        [DataMember]
        public string TelexAnswerBack { get; set; }
        [DataMember]
        public string TaxRegistrationNo { get; set; }
        [DataMember]
        public string TaxRegistrationNo2 { get; set; }
        [DataMember]
        public string HomePage { get; set; }
        [DataMember]
        public string BusinessGroupCode { get; set; }
        [DataMember]
        public string CustomerGroupCode { get; set; }
        [DataMember]
        public string DepartmentCode { get; set; }
        [DataMember]
        public string ProjectCode { get; set; }
        [DataMember]
        public string PurchaserCode { get; set; }
        [DataMember]
        public string SalesCampaignCode { get; set; }
        [DataMember]
        public string SalesPersonCode { get; set; }
        [DataMember]
        public string AuthSigComplete { get; set; }
        [DataMember]
        public Nullable<DateTime> ReceivedDate { get; set; }
        [DataMember]
        public Nullable<DateTime> GracePeriodStartDate { get; set; }
        [DataMember]
        public Nullable<DateTime> GracePeriodEndDate { get; set; }
        [DataMember]
        public string CommercialLiabInsurerName { get; set; }
        [DataMember]
        public Nullable<DateTime> CommercialLiabInsurerExpDate { get; set; }
        [DataMember]
        public string CommercialLiabHSTNumber { get; set; }
        [DataMember]
        public string WorkerHealthSafetyCertNo { get; set; }
        [DataMember]
        public string PermitsCertDescription { get; set; }
        [DataMember]
        public Nullable<bool> AmendedAgreement { get; set; }
        [DataMember]
        public Nullable<DateTime> AmendedDate { get; set; }
        [DataMember]
        public bool IsTaxExempt { get; set; }
        [DataMember]
        public string UpdateToFinancialSoftware { get; set; }
        [DataMember]
        public string PrimaryBusinessActivity { get; set; }
        [DataMember]
        public Nullable<int> MailingAddressSameAsBusiness { get; set; }
        [DataMember]
        public int BusinessActivityID { get; set; }
        [DataMember]
        public string GSTNumber { get; set; }
        [DataMember]
        public string AuthorizedSigComplete { get; set; }
        [DataMember]
        public string LocationEmailAddress { get; set; }
        [DataMember]
        public Nullable<DateTime> DateReceived { get; set; }
        [DataMember]
        public Nullable<DateTime> ActivationDate { get; set; }
        [DataMember]
        public Nullable<DateTime> ConfirmationMailedDate { get; set; }
        [DataMember]
        public Nullable<int> PreferredCommunication { get; set; }
        [DataMember]
        public int RegistrantTypeID { get; set; }
        [DataMember]
        public int RegistrantSubTypeID { get; set; }
        [DataMember]
        public string LastUpdatedBy { get; set; }
        [DataMember]
        public Nullable<DateTime> LastUpdatedDate { get; set; }
        [DataMember]
        public Nullable<int> CheckoutNum { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public Nullable<int> InBatch { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<DateTime> ActiveStateChangeDate { get; set; }
        [DataMember]
        public string FacilityDescription { get; set; }
        [DataMember]
        public string TaxClass { get; set; }
        [DataMember]
        public Nullable<int> PrimaryBusinessAddrStorageCap { get; set; }
        [DataMember]
        public Nullable<int> NumberOfStorageSites { get; set; }
        [DataMember]
        public Nullable<int> MaxStorageSitesCapacity { get; set; }
        [DataMember]
        public string StorageSiteID { get; set; }
        [DataMember]
        public string CertificateOfApprovalNum { get; set; }
        [DataMember]
        public Nullable<DateTime> CompanyYearEndDate { get; set; }
        [DataMember]
        public string ProcProductsProduced { get; set; }
        [DataMember]
        public string ProcProductsProducedDesc { get; set; }
        [DataMember]
        public string HPrimaryBusAddrSortYards { get; set; }
        [DataMember]
        public string HNumberOfSortYards { get; set; }
        [DataMember]
        public Nullable<int> HSortYardsStorageCap { get; set; }
        [DataMember]
        public Nullable<int> HSortYardID { get; set; }
        [DataMember]
        public Nullable<DateTime> HYardCountDate { get; set; }
        [DataMember]
        public Nullable<DateTime> HYardCountDate2 { get; set; }
        [DataMember]
        public Nullable<int> HPrimaryBusAddrStorageCap { get; set; }
        [DataMember]
        public string CPointsAdditional { get; set; }
        [DataMember]
        public string CPointsType { get; set; }
        [DataMember]
        public string CPointsGarage { get; set; }
        [DataMember]
        public string CPointsUsedVehicle { get; set; }
        [DataMember]
        public string CPointsOther { get; set; }
        [DataMember]
        public string CPointsOtherDesc { get; set; }
        [DataMember]
        public string HasLocalInventory { get; set; }
        [DataMember]
        public string CIsGenerator { get; set; }
        [DataMember]
        public string EndUseMarket { get; set; }
        [DataMember]
        public string EndUseMarketDesc { get; set; }
        [DataMember]
        public AddressModel LocationAddress { get; set; }
    
    }
}
