﻿using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace CL.TMS.DataSync
{
    public class LoggingConfig
    {
        public static void InializeLogging()
        {
            Logger.SetLogWriter(new LogWriterFactory().Create());
        }
    }
}