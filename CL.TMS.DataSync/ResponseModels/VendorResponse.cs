﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using CL.TMS.DataSync.CommunicationData;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.Communication;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.RegistrantServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling.Exceptions;

namespace CL.TMS.DataSync.ResponseMessages
{
    /// <summary>
    /// Vendor response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(CL.TMS.DataSync.CommunicationData.VendorModel))]
    public class VendorResponse : IVendorMsg
    {
        #region IVendorMsg Members

      
        /// <summary>
        /// Retrieves Vendor list updated after passed date (For OTS-Mobile)
        /// </summary>
        /// <param name="registrationNumber">Registration number.</param>
        public void GetAllUpdatedVendors(DateTime lastUpdated)
        {
            //IRegistrantService VendorService = new RegistrantService();
            //IList<Vendor> listOfVendors = VendorService.GetUpdatedVendorList(lastUpdated);

            ////IVendor VendorService = new Vendor();
            ////IList<IVendor> listOfVendors = VendorService.GetUpdatedVendorList(lastUpdated);
            //IList<VendorModel> list = new List<VendorModel>();
            //try
            //{
            //    if (listOfVendors != null)
            //    {
            //        foreach (var Vendor in listOfVendors)
            //        {
            //            var vendorAddress = VendorService.GetVendorAddressList(Vendor.ID)[0];
            //            var vendor = new VendorModel()
            //            {
            //                ID = Vendor.ID,
            //                Number = Vendor.Number,
            //                BusinessNumber = Vendor.BusinessNumber,
            //                BusinessName = Vendor.BusinessName,
            //                ActivationDate = Vendor.ActivationDate,
            //                ActiveStateChangeDate = Vendor.ActiveStateChangeDate,
            //                IsActive = Vendor.IsActive,
            //                LastUpdatedDate = Vendor.LastUpdatedDate,
            //                VendorTypeID = Vendor.VendorType,
            //                CreatedDate = Vendor.CreatedDate,
            //                LocationAddress = new AddressModel()
            //                {
            //                    ID = vendorAddress.ID,
            //                    AddressTypeID = vendorAddress.AddressType,
            //                    Address1 = vendorAddress.Address1,
            //                    Address2 = vendorAddress.Address2,
            //                    Address3 = vendorAddress.Address3,
            //                    City = vendorAddress.City,
            //                    Province = vendorAddress.Province,
            //                    PostalCode = vendorAddress.PostalCode,
            //                    Country = vendorAddress.Country,
            //                    Phone = vendorAddress.Phone,
            //                    Fax = vendorAddress.Fax,
            //                    GPSCoordinate1 = vendorAddress.GPSCoordinate1,
            //                    GPSCoordinate2 = vendorAddress.GPSCoordinate2
            //                }
            //            };

            //            list.Add(vendor);
            //        }
            //    }
            //    UpdatedVendorList = list;
            //    this.responseStatus = WebServiceResponseStatus.OK;
            //}
            //catch (ArgumentException aEx)
            //{
            //    this.responseMessages.Add(aEx.Message);
            //    this.responseStatus = WebServiceResponseStatus.ERROR;
            //}
            //catch (CLDuplicateWarningException ovEx)
            //{
            //    this.ResponseMessages.Add(ovEx.Message);
            //    this.responseStatus = WebServiceResponseStatus.WARNING;
            //}
            //catch (Exception)
            //{
            //    this.responseStatus = WebServiceResponseStatus.ERROR;
            //}
        }

        #endregion

        #region IResponseMessage<IVendorModel> Members

        private IEnumerable<IVendorModel> responseContent;
        public IEnumerable<IVendorModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<IVendorModel>();
                if (SingleEntity != null)
                    ((List<IVendorModel>)this.responseContent).Add(SingleEntity);

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private WebServiceResponseStatus responseStatus = WebServiceResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (WebServiceResponseStatus)Enum.Parse(typeof(WebServiceResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public IVendorModel SingleEntity { get; set; }

        [DataMember]
        public ICollection<CL.TMS.DataSync.CommunicationData.VendorModel> UpdatedVendorList { get; set; }
        
        #endregion
    }
}
