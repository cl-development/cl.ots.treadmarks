﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using CL.TMS.ExceptionHandling;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.ClaimCalculator;

namespace CL.TMS.DataSync
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            LoggingConfig.InializeLogging();
            ExceptionHandlingConfig.BootstrapExceptionHandling();
            var eventAggregator = (IEventAggregator)(GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IEventAggregator)));
            ClaimCalculatorConfig.InitializeCalimCalculator(eventAggregator);

        }
    }
}
