﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataSyncBLL;
using CL.TMS.DataContracts.Communication;
using CL.TMS.DataSync.ResponseMessages;
using CL.TMS.Common.Enum;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.DataSync.CommunicationData;
using CL.TMS.ExceptionHandling.Exceptions;

namespace CL.TMS.DataSync.Controllers
{
    public class PullController : ApiController
    {
        private IAppService appService;
        private IRegistrantService regService;

        public PullController(IAppService appService, IRegistrantService regService)
        {
            this.appService = appService;
            this.regService = regService;
        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult Test(TestModel input)
        {
            return Json(string.Format("this is a test for {0}", input.Name));
        }
        public IHttpActionResult Get()
        {
            var domainName = appService.GetSettingValue("Settings.DomainName");
            return Json("Pull Get Operation, and domain name is " + domainName);           
        }

        [HttpGet]
        [Authorize]
        [Route("api/v1/Pull/GetAllUpdatedAppUsers")]
        public IHttpActionResult GetAllUpdatedAppUsers(DateTime dateInfo)
        {
            ResponseData resp = new ResponseData();
            IList<AppUser> listOfAppUsers = regService.GetUpdatedAppUserList(dateInfo);
            IList<AppUserModel> list = new List<AppUserModel>();
            try
            {
                if (listOfAppUsers != null) 
                {
                    foreach (var appUser in listOfAppUsers)
                    {
                        var appuser = new AppUserModel()
                        {
                            ID = appUser.ID,
                            VendorID = appUser.VendorID,
                            VendorAppUserID = appUser.VendorAppUserID,
                            Number = appUser.Number,
                            Name = appUser.Name,
                            EMail = appUser.EMail,
                            AccessTypeID = appUser.AccessTypeID,
                            CreatedOn = appUser.CreatedOn,
                            ModifiedOn = appUser.ModifiedOn,
                            Metadata = appUser.Metadata,
                            LastAccessOn = appUser.LastAccessOn,
                            SyncOn = appUser.SyncOn,
                            Active = appUser.Active
                        };

                        list.Add(appuser);
                    }
                }
                resp.Data = list;
                resp.Code = (int)WebServiceResponseStatus.OK;
            }
            catch (ArgumentException aEx)
            {
                resp.Message = aEx.Message;
                resp.Code = (int)WebServiceResponseStatus.ERROR;
            }
            catch (CLDuplicateWarningException ovEx)
            {
                resp.Message = ovEx.Message;
                resp.Code = (int)WebServiceResponseStatus.WARNING;
            }
            catch (Exception)
            {
                resp.Code = (int)WebServiceResponseStatus.ERROR;
            }
            return Json(resp);
        }

        [HttpGet]
        [Route("api/v1/Pull/GetAllUpdatedVendors")]
        [Authorize]
        public IHttpActionResult GetAllUpdatedVendors(DateTime dateInfo)
        {
            ResponseData resp = new ResponseData();
            IList<Vendor> listOfVendors = regService.GetUpdatedVendorList(dateInfo);
            IList<VendorModel> list = new List<VendorModel>();
            try
            {
                if (listOfVendors != null)
                {
                    foreach (var Vendor in listOfVendors)
                    {
                        //Get business address - address type 1
                        var vendorAddress = Vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == 1);
                        var vendor = new VendorModel()
                        {
                            ID = Vendor.ID,
                            Number = Vendor.Number,
                            BusinessNumber = Vendor.BusinessNumber,
                            BusinessName = Vendor.BusinessName,
                            ActivationDate = Vendor.ActivationDate,
                            ActiveStateChangeDate = Vendor.ActiveStateChangeDate,
                            IsActive = Vendor.IsActive,
                            LastUpdatedDate = Vendor.LastUpdatedDate,
                            VendorTypeID = Vendor.VendorType,
                            CreatedDate = Vendor.CreatedDate,
                            LocationAddress = new AddressModel()
                            {
                                ID = vendorAddress.ID,
                                AddressTypeID = vendorAddress.AddressType,
                                Address1 = vendorAddress.Address1,
                                Address2 = vendorAddress.Address2,
                                Address3 = vendorAddress.Address3,
                                City = vendorAddress.City,
                                Province = vendorAddress.Province,
                                //device has issues with space so it has to be removed here
                                PostalCode = vendorAddress.PostalCode.Replace(" ", ""),
                                Country = vendorAddress.Country,
                                //Phone is required on central repo, but needs to fixed in application side, uncomment after fix
                                Phone = string.IsNullOrEmpty(vendorAddress.Phone) ? "" : vendorAddress.Phone,
                                Fax = vendorAddress.Fax,
                                GPSCoordinate1 = vendorAddress.GPSCoordinate1,
                                GPSCoordinate2 = vendorAddress.GPSCoordinate2
                            }
                        };

                        list.Add(vendor);
                    }
                }
                resp.Data = list;
                resp.Code = (int)WebServiceResponseStatus.OK;
            }
            catch (ArgumentException aEx)
            {
                resp.Message = aEx.Message;
                resp.Code = (int)WebServiceResponseStatus.ERROR;
            }
            catch (CLDuplicateWarningException ovEx)
            {
                resp.Message = ovEx.Message;
                resp.Code = (int)WebServiceResponseStatus.WARNING;
            }
            catch (Exception ex)
            {
                resp.Code = (int)WebServiceResponseStatus.ERROR;
            }
            return Json(resp);
        }
    }

}
