//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class collector_claim_hold_history
    {
        public int collector_claim_hold_history_id { get; set; }
        public long summary_id { get; set; }
        public string type_of_hold { get; set; }
        public Nullable<System.DateTime> placed_on_hold_date { get; set; }
        public Nullable<System.DateTime> taken_off_hold_date { get; set; }
        public string changed_by { get; set; }
        public System.DateTime changed_date { get; set; }
    }
}
