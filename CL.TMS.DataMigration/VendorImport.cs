﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataMigration.CentralDB;
using CL.TMS.Repository.Common;
using CL.TMS.Repository.Registrant;
using CL.TMS.Repository.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class VendorImport
    {
        public void ImportVendors()
        {
            //registrant_type_id = 1  'Collector'
            //registrant_type_id = 2  'Hauler'
            //registrant_type_id = 3  'Steward'
            //registrant_type_id = 4  'Manufacturer'
            //registrant_type_id = 5  'Processor'

            //Run it for each registrant type
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Collector);
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Hauler);
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Processor);
            
            RegistrantDataMgr regMgr = new RegistrantDataMgr();
            
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Collector);
            
            //PushToNewTM(regMgr, registrants);
            PushToNewTM(regMgr, null);

            //registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Hauler);
            //PushToNewTM(regMgr, registrants);

            //registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Processor);
            //PushToNewTM(regMgr, registrants);

            //registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.RPM);
            //PushToNewTM(regMgr, registrants);
        }

        private void PushToNewTM(RegistrantDataMgr regMgr, IEnumerable<registrant> registrants)
        {

            VendorRepository vendorRepository = new VendorRepository();
            //LocationDataMgr locMgr = new LocationDataMgr();
            //RegistrantTireDataMgr regTireMgr = new RegistrantTireDataMgr();
            DateTime startedAt = DateTime.Now;
            int importedCount = 0;

            //foreach (var reg in registrants)
            //{
            //    var existingVendor = vendorRepository.GetVendorByNumber(reg.registration_number.ToString());

            //    if (existingVendor == null && !reg.registration_number.ToString().StartsWith("41") && !reg.registration_number.ToString().Contains("99999"))
            //    {
            //        var vendor = new Vendor();
            //        vendor.RegistrantTypeID = (int)reg.registrant_type_id;//                    

            //        var regActivityHistoryRecords = regMgr.GetRegActivityHistory(reg.registration_number);

            //        foreach (var regActivityHistory in regActivityHistoryRecords.Select((value, index) => new { Value = value, Index = index }))
            //        {
            //            vendor.VendorActiveHistories.Add(new VendorActiveHistory()
            //            {
            //                ActiveState = regActivityHistory.Value.active_state,
            //                ActiveStateChangeDate = regActivityHistory.Value.active_state_change_date.ToUniversalTime(),
            //                CreateDate = reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1),
            //                IsCurrent = regActivityHistory.Index == 0 ? true : false,
            //                Reason = regActivityHistory.Value.active_state ? string.Empty : "Other",
            //                OtherReason = regActivityHistory.Value.active_state ? string.Empty : "Please see Legacy."
            //            });
            //        }

            //        if (regActivityHistoryRecords.Count() == 0)
            //        {
            //            vendor.VendorActiveHistories.Add(new VendorActiveHistory()
            //            {
            //                ActiveState = reg.active_state_change_date == null || reg.active_state == 1,
            //                ActiveStateChangeDate = reg.active_state_change_date ?? (reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1)),
            //                CreateDate = reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1),
            //                IsCurrent = true,
            //                Reason = (reg.active_state_change_date == null || reg.active_state == 1) ? string.Empty : "Other",
            //                OtherReason = (reg.active_state_change_date == null || reg.active_state == 1) ? string.Empty : "Please see Legacy."
            //            });
            //        }


            //        var locations = locMgr.GetAllLocationsByRegNo(reg.registration_number);
            //        var addresses = new List<Address>();

            //        ////////////////
            //        //Add Sort yard even if it is not there in legacy 
            //        //Add Primary loc as sort yard, from christina
            //        if (reg.registrant_type_id != (int)RegistrantTypeLegacy.Collector)
            //        {
            //            var sortYardAddress = new Address();
            //            sortYardAddress.Address1 = reg.location_address_1;
            //            sortYardAddress.Address2 = reg.location_address_2;
            //            sortYardAddress.City = reg.location_city;
            //            sortYardAddress.PostalCode = reg.location_postal_code;
            //            sortYardAddress.Province = reg.location_province;
            //            sortYardAddress.Country = reg.location_country;
            //            sortYardAddress.AddressType = (int)AddressTypeEnum.Sortyard;
            //            sortYardAddress.Contacts = null;

            //            VendorStorageSite vS = new VendorStorageSite()
            //            {
            //                MaxStorageCapacity = 0,
            //                CertificateOfApprovalNumber = reg.hpm_cert_of_approval_num,
            //                StorageSiteType = 1
            //            };

            //            sortYardAddress.VendorStorageSites.Add(vS);
            //            addresses.Add(sortYardAddress);
            //        }

            //        /////////////////
            //        //Add Sort yards
            //        foreach (var loc in locations)
            //        {
            //            //Checked all BOs Hauler, Processor and RPM StorageSiteType = 1
            //            if (!string.IsNullOrEmpty(loc.location_addr1))
            //            {
            //                //foreach (var eachAddress in vendor.VendorAddresses)
            //                {
            //                    var sortYardAddress = new Address();

            //                    sortYardAddress.Address1 = loc.location_addr1;
            //                    sortYardAddress.Address2 = loc.location_addr2;
            //                    sortYardAddress.City = loc.location_city;
            //                    sortYardAddress.PostalCode = loc.location_postcode;

            //                    //Only these combinations available
            //                    //"province_id"	"province_desc"	"province_region"	"province_abbr"	"code"
            //                    //"7"	"MANITOBA"	"PRARIE"	"MB"	"MB"
            //                    //"8"	"ONTARIO"	"CENTRAL"	"ON"	"ON"
            //                    //"13"	"US"	"UNITED STATES"	"zUS"	"US"
            //                    //"38"	"MINNESOTA"	"UNITED STATES"	"zMN"	"MN"
            //                    //"47"	"NEW YORK"	"UNITED STATES"	"zNY"	"NY"

            //                    switch (Convert.ToInt16(loc.location_prov))
            //                    {
            //                        case 7:
            //                            sortYardAddress.Province = "MANITOBA";
            //                            sortYardAddress.Country = "Canada";
            //                            break;
            //                        case 8:
            //                            sortYardAddress.Province = "ONTARIO";
            //                            sortYardAddress.Country = "Canada";
            //                            break;
            //                        case 13:
            //                            sortYardAddress.Province = "US";
            //                            sortYardAddress.Country = "UNITED STATES";
            //                            break;
            //                        case 38:
            //                            sortYardAddress.Province = "MINNESOTA";
            //                            sortYardAddress.Country = "UNITED STATES";
            //                            break;
            //                        case 47:
            //                            sortYardAddress.Province = "NEW YORK";
            //                            sortYardAddress.Country = "UNITED STATES";
            //                            break;
            //                        default:
            //                            sortYardAddress.Province = "ONTARIO";
            //                            sortYardAddress.Country = "Canada";
            //                            break;
            //                    }

            //                    sortYardAddress.AddressType = (int)AddressTypeEnum.Sortyard;
            //                    sortYardAddress.Contacts = null;

            //                    VendorStorageSite vS = new VendorStorageSite()
            //                    {
            //                        MaxStorageCapacity = loc.location_capacity != null ? (double)loc.location_capacity : 0,
            //                        CertificateOfApprovalNumber = reg.hpm_cert_of_approval_num,
            //                        StorageSiteType = 1
            //                    };

            //                    sortYardAddress.VendorStorageSites.Add(vS);
            //                    addresses.Add(sortYardAddress);
            //                }
            //            }
            //        }

            //        addresses.ForEach(r => vendor.VendorAddresses.Add(r));

            //        AddParticipantAddresses(reg, vendor);

            //        //collector: tires, collection points
            //        //hauler - tires
            //        //processor: tires, products 
            //        //RPM : Products

            //        vendor.RegistrantSubTypeID = (int)reg.registrant_sub_type_id;//
            //        vendor.Number = reg.registration_number.ToString();

            //        //to do ... add different vendor items ... check legacy tables (lookup) then add banking information from GP
            //        switch (reg.registrant_type_id)
            //        {
            //            case (int)RegistrantTypeLegacy.Collector:
            //                vendor.VendorType = (int)VendorTypeNewTM.Collector;
            //                ImportTires(regTireMgr, vendorRepository, vendor);

            //                /////////////Add used tires
            //                int qty = 0;
            //                var items = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
            //                                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType);

            //                foreach (var item in items)
            //                {
            //                    switch (item.ShortName)
            //                    {
            //                        case TreadMarksConstants.PLT:
            //                            qty = (int?)reg.c_num_ltt ?? 0;
            //                            break;
            //                        case TreadMarksConstants.MT:
            //                            qty = (int?)reg.c_num_mtt ?? 0;
            //                            break;
            //                        case TreadMarksConstants.AGLS:
            //                            qty = (int?)reg.c_num_adt ?? 0;
            //                            break;
            //                        case TreadMarksConstants.IND:
            //                            qty = (int?)reg.c_num_sst ?? 0;
            //                            break;
            //                        case TreadMarksConstants.SOTR:
            //                            qty = (int?)reg.c_num_sotr ?? 0;
            //                            break;
            //                        case TreadMarksConstants.MOTR:
            //                            qty = (int?)reg.c_num_motr ?? 0;
            //                            break;
            //                        case TreadMarksConstants.LOTR:
            //                            qty = (int?)reg.c_num_lotr ?? 0;
            //                            break;
            //                        case TreadMarksConstants.GOTR:
            //                            qty = (int?)reg.c_num_gotr ?? 0;
            //                            break;
            //                    }

            //                    //var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID && i.EffectiveStartDate <= DateTime.UtcNow && i.EffectiveEndDate >= DateTime.UtcNow).StandardWeight;

            //                    //var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                                
            //                    ///////
            //                    var repository = new SettingRepository();
            //                    var itemWeight = repository.LoadItemWeights(DateTime.Now).FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
            //                    //////

            //                    //AddToVendorInventory(vendor, qty, item, itemWeight);
            //                    var inventory = new Inventory()
            //                    {
            //                        ItemId = item.ID,
            //                        IsEligible = true,
            //                        Qty = qty,
            //                        Weight = itemWeight,
            //                        //UpdatedBy = app.AssignToUser.Value,
            //                        UpdatedDate = DateTime.UtcNow,
            //                        IsValidForClaim = false
            //                    };
            //                    vendor.VendorInventories.Add(inventory);
            //                }
            //                /////////////Add used tires - end

            //                break;
            //            case (int)RegistrantTypeLegacy.Hauler:
            //                vendor.VendorType = (int)VendorTypeNewTM.Hauler;
            //                ImportTires(regTireMgr, vendorRepository, vendor);

            //                break;
            //            case (int)RegistrantTypeLegacy.Processor:
            //                vendor.VendorType = (int)VendorTypeNewTM.Processor;
            //                ImportTires(regTireMgr, vendorRepository, vendor);
            //                ImportProductsPurchased(regTireMgr, vendorRepository, vendor, reg);

            //                //default is Other, if there is a value in legacy then check
            //                vendor.RegistrantSubTypeID = 4;
            //                switch ((int)reg.registrant_sub_type_id)
            //                {
            //                    case 78:
            //                        vendor.RegistrantSubTypeID = 1;
            //                        break;
            //                    case 79:
            //                        vendor.RegistrantSubTypeID = 2;
            //                        break;
            //                    case 80:
            //                        vendor.RegistrantSubTypeID = 3;
            //                        break;
            //                }

            //                break;
            //            case (int)RegistrantTypeLegacy.RPM:
            //                vendor.VendorType = (int)VendorTypeNewTM.RPM;
            //                ImportProductsPurchased(regTireMgr, vendorRepository, vendor, reg);
            //                ImportRPMProducts(regTireMgr, vendorRepository, vendor);
            //                break;
            //        }

            //        //No collection_point_type in New TM for collector
            //        //registrant_form_lookup is same as registrant_sub_type i.e. business activity

            //        vendor.BusinessName = reg.business_name;
            //        vendor.FranchiseName = reg.business_franchise_name;
            //        vendor.OperatingName = reg.business_operating_name;
            //        vendor.BusinessNumber = reg.business_number;
            //        vendor.AuthSigComplete = reg.authorized_sig_complete != null ? reg.authorized_sig_complete.ToString() : null;
            //        vendor.ReceivedDate = (reg.date_received != null && reg.date_received > new DateTime(1970, 1, 1)) ? reg.date_received.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.GracePeriodStartDate = reg.grace_period_start_date != null ? reg.grace_period_start_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.GracePeriodEndDate = reg.grace_period_end_date != null ? reg.grace_period_end_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.CommercialLiabInsurerName = reg.chpm_commercial_liability_insurer_name;
            //        //Invalid date (year 0201 for 2001448)
            //        vendor.CommercialLiabInsurerExpDate = (reg.chpm_commercial_liability_insurer_expiry != DateTime.MinValue && reg.chpm_commercial_liability_insurer_expiry != null && reg.chpm_commercial_liability_insurer_expiry > new DateTime(1970, 1, 1)) ? reg.chpm_commercial_liability_insurer_expiry.Value.ToUniversalTime() : (DateTime?)null; ;
            //        vendor.WorkerHealthSafetyCertNo = reg.chpm_worker_health_safety_cert_no;
            //        vendor.PermitsCertDescription = reg.chpm_permits_certifications_description.ToString();
            //        vendor.AmendedAgreement = reg.c_amended_agreement;//
            //        vendor.AmendedDate = (reg.c_amended_date != DateTime.MinValue && reg.c_amended_date != null) ? reg.c_amended_date.Value.ToUniversalTime() : (DateTime?)null; //
            //        vendor.PrimaryBusinessActivity = reg.registrant_sub_type_desc;
            //        vendor.IsTaxExempt = reg.is_tax_exempt;

            //        //Looks like it is the BusinessActivityType used in Vendor and Customer table but name used is BusinessActivityId
            //        // 1 for steward and 2 for collector defined in enum
            //        if (reg.registrant_type_id == (int)RegistrantTypeLegacy.Collector)
            //            vendor.BusinessActivityID = 2;

            //        vendor.GSTNumber = reg.gst_number;
            //        vendor.AuthorizedSigComplete = reg.authorized_sig_complete.ToString();
            //        vendor.LocationEmailAddress = reg.location_email_address;
            //        vendor.DateReceived = (reg.date_received != null && reg.date_received > new DateTime(1970, 1, 1)) ? reg.date_received.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.ConfirmationMailedDate = (reg.confirmation_mailed_date != null && reg.confirmation_mailed_date != DateTime.MinValue) ? reg.confirmation_mailed_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.PreferredCommunication = (int?)reg.preferred_communication;

            //        vendor.LastUpdatedBy = reg.last_updated_by;
            //        vendor.LastUpdatedDate = reg.last_updated_date != null ? reg.last_updated_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.CheckoutNum = (int?)reg.checkout_num;
            //        vendor.CreatedDate = reg.created_date_ts != null ? reg.created_date_ts.Value.ToUniversalTime() : new DateTime(1970, 1, 1);
            //        vendor.InBatch = (int)reg.in_batch;
            //        vendor.ActiveStateChangeDate = reg.active_state_change_date != null ? reg.active_state_change_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.ActivationDate = (reg.activation_date != null && reg.activation_date != DateTime.MinValue) ? reg.activation_date.Value.ToUniversalTime() : (DateTime?)null;
            //        vendor.IsActive = reg.active_state_change_date == null || reg.active_state == 1;

            //        vendor.HasMoreThanOneEmp = string.IsNullOrEmpty(reg.chpm_worker_health_safety_cert_no) ? false : true;
            //        vendor.WSIBNumber = reg.chpm_worker_health_safety_cert_no;

            //        //Not sure about this
            //        vendor.CommercialLiabHSTNumber = reg.gst_number;
            //        vendor.BusinessStartDate = (reg.business_start_date != null && reg.business_start_date != DateTime.MinValue && reg.business_start_date > new DateTime(1970, 1, 1)) ? reg.business_start_date.ToUniversalTime() : new DateTime(1970, 1, 1);

            //        vendor.FacilityDescription = reg.chpm_facility_description != null ? reg.chpm_facility_description.ToString() : string.Empty;
            //        vendor.PrimaryBusinessAddrStorageCap = (int?)reg.hpm_prim_bus_addr_storage_cap;
            //        vendor.NumberOfStorageSites = (int?)reg.pm_num_storage_sites;
            //        vendor.MaxStorageSitesCapacity = (int?)reg.pm_max_storage_sites_cap;

            //        //Checked legacy it is always null  
            //        //vendor.StorageSiteID= reg.pm_storage_site_id != null ? reg.pm_storage_site_id.ToString() : null;
            //        vendor.CertificateOfApprovalNum = reg.hpm_cert_of_approval_num;
            //        vendor.CompanyYearEndDate = (reg.p_company_year_end != null && reg.p_company_year_end != DateTime.MinValue) ? reg.p_company_year_end.Value.ToUniversalTime() : (DateTime?)null;

            //        //Always null
            //        //vendor.ProcProductsProduced= reg.p_products_produced;

            //        if (!string.IsNullOrEmpty(reg.p_products_produced_desc))
            //        {
            //            vendor.ProcProductsProducedDesc = reg.p_products_produced_desc;
            //            vendor.OtherProcessProduct = reg.p_products_produced_desc;
            //        }

            //        //Always null
            //        //vendor.HPrimaryBusAddrSortYards= reg.h_prim_bus_addr_sort_yards;
            //        vendor.HNumberOfSortYards = reg.h_num_sort_yards != null ? reg.h_num_sort_yards.ToString() : string.Empty;
            //        vendor.HSortYardsStorageCap = (int?)reg.h_max_sort_yards_storage_cap;

            //        //Always null
            //        //vendor.HSortYardID= reg.h_sort_yard_id

            //        //Does not exist in legacy
            //        //vendor.HYardCountDate= reg.date
            //        //Does not exist in legacy
            //        //vendor.HYardCountDate2= reg.h_yard_count_1
            //        vendor.HPrimaryBusAddrStorageCap = (int?)reg.hpm_prim_bus_addr_storage_cap;

            //        //Does not exist in legacy
            //        //vendor.CPointsAdditional= reg.c_points_additional;
            //        //vendor.CPointsType= reg.c_points_type;
            //        vendor.CPointsGarage = reg.c_points_garage != null ? reg.c_points_garage.ToString() : string.Empty;
            //        vendor.CPointsUsedVehicle = reg.c_points_used_vehicle != null ? reg.c_points_used_vehicle.ToString() : string.Empty;
            //        vendor.CPointsOther = reg.c_points_other != null ? reg.c_points_other.ToString() : string.Empty;
            //        vendor.CPointsOtherDesc = reg.c_points_other_desc;
            //        //vendor.HasLocalInventory= reg
            //        if (reg.c_generator_flag != null)
            //            vendor.CIsGenerator = Convert.ToBoolean(reg.c_generator_flag);
            //        //vendor.EndUseMarket= reg.m_end_use_market;
            //        vendor.EndUseMarketDesc = reg.m_end_user_market_desc;
            //        //vendor.HHasRelatioshipWithProcessor= reg
            //        //vendor.HRelatedProcessor= reg
            //        //ps_rating nothing saved here
            //        //vendor.HIsGVWR= reg.ps_rating
            //        //Saving as true as we don't have a flag for it
            //        vendor.HIsGVWR = true;
            //        vendor.CVORNumber = reg.ps_cvor;
            //        //It does not exist in legacy
            //        //vendor.CVORExpiryDate= null;_
            //        //vendor.HasMoreThanOneEmp= reg
            //        vendor.CertificateOfApprovalNum = reg.hpm_cert_of_approval_num;
            //        vendor.ContactAddressSameAsBusiness = false;
            //        //vendor.OtherProcessProduct= reg.othe
            //        vendor.OtherRegistrantSubType = reg.registrant_sub_type_other_desc;
            //        //Dont have it in legacy  
            //        vendor.CGeneratorDate = null;

            //        vendor.RegistrantStatus = ApplicationStatusEnum.Approved.ToString();

            //        BankInfoMgr bankInfoMgr = new BankInfoMgr();
            //        var bankInfo = bankInfoMgr.GetImportedBankInfo(reg.registration_number);
            //        string key = Token.GenerateRandomSecretKey();

            //        if (bankInfo != null && bankInfo.EmailToAddress.Trim() != "" && bankInfo.EFTTransitRoutingNo.Trim() != "")
            //        {
            //            vendor.BankInformations = new List<BankInformation> { new BankInformation()  
            //                                        {
            //                                            VendorId = vendor.ID,
            //                                            EmailPaymentTransferNotification = false,
            //                                            NotifyToPrimaryContactEmail = false,
            //                                            BankName = bankInfo.BANKNAME,
            //                                            Key = key,
            //                                            //Test the substring code
            //                                            TransitNumber = Token.Encrypt(bankInfo.EFTTransitRoutingNo.Trim().Substring(0,5), key),
            //                                            //TransitNumber = Token.Encrypt(bankInfo.EFTTransitRoutingNo.Trim(), key),
            //                                            BankNumber = Token.Encrypt((bankInfo.EFTTransitRoutingNo.Trim().Substring(5,3)), key),
            //                                            AccountNumber = Token.Encrypt((bankInfo.EFTBankAcct.Trim()), key),
                                                        
            //                                            //two email addresses in one field
            //                                            Email = bankInfo.EmailToAddress.Split(';')[0],
            //                                            CCEmail = bankInfo.EmailCcAddress,
            //                                            ReviewStatus = "Approved"
            //                                        }};
            //            //Overrite status
            //            vendor.RegistrantStatus = ApplicationStatusEnum.BankInformationSubmitted.ToString();
            //        }

            //        vendor.ContactInfo = reg.primary_contact_name + ", " + reg.primary_contact_phone;
             
            //        //Shivangi Malar asked for this change
            //        if (reg.registrant_type_id == (int)RegistrantTypeLegacy.Collector && reg.registrant_sub_type_desc == "Generator" && vendor.RegistrantStatus != ApplicationStatusEnum.BankInformationSubmitted.ToString())
            //        {
            //            vendor.BankInformations = new List<BankInformation> { new BankInformation()  
            //                                        {
            //                                            VendorId = vendor.ID,
            //                                            EmailPaymentTransferNotification = false,
            //                                            NotifyToPrimaryContactEmail = false,
            //                                            BankName = "",
            //                                            Key = key,
            //                                            TransitNumber = "",
            //                                            BankNumber = "",
            //                                            AccountNumber = "",                                                        
            //                                            Email = "",
            //                                            CCEmail = "",
            //                                            ReviewStatus = "Approved"
            //                                        }};
            //            //Overrite status
            //            vendor.RegistrantStatus = ApplicationStatusEnum.BankInformationSubmitted.ToString();
            //        }
                    

            //        vendorRepository.AddVendor(vendor);
            //        importedCount += 1;
            //        System.Console.WriteLine("Imoprted: " + reg.registration_number);
            //    }
            //}

            var bankinfoList = vendorRepository.GetBankInformations();

            //var count = bankinfoList.ToList();
            BankInfoMgr bankInfoMgr = new BankInfoMgr();

            //bankinfoList = vendorRepository.GetBankInformations().Where(r => r.VendorId == 19600).ToList();
            
            int i = 0;
            foreach (var bankinfo in bankinfoList)
            {
                var bankInfoUpdated = bankInfoMgr.GetImportedBankInfo(bankinfo.VendorId);
                if (bankInfoUpdated != null)
                {
                    bankinfo.BankName = bankInfoUpdated.BANKNAME;
                    bankinfo.TransitNumber = bankInfoUpdated.EFTTransitRoutingNo_Str.Trim().Substring(0, 5);
                    bankinfo.BankNumber = bankInfoUpdated.EFTTransitRoutingNo_Str.Trim().Substring(5, 3);
                    bankinfo.AccountNumber = bankInfoUpdated.EFTBankAcct.Trim();
                    bankinfo.Email = bankInfoUpdated.EmailToAddress.Split(';')[0];
                    bankinfo.CCEmail = bankInfoUpdated.EmailCcAddress;

                    vendorRepository.UpdateBankInformation(bankinfo);
                    i += 1;
                    System.Console.WriteLine(i + ". Imoprted bank info for : " + bankInfoUpdated.CustomerVendor_ID);
                }
            }

            System.Console.WriteLine("Started at: " + startedAt + ", Imoprted: " + importedCount + " records, Finish time: " + DateTime.Now);
        }   

        //Malar reported a bug where she was not able to open a claim for collector
        //After investigation the ActiveStateChangedate was null for that imported record in vendor table
        //Plz run forllowing query to make it work

        /*update vendor 
        set ActiveStateChangeDate=va.ActiveStateChangeDate
        from vendor v
        inner join VendorActiveHistory va
        on v.ID=va.VendorID 
        where v.ActiveStateChangeDate is null*/

        public void ImportUsers()
        {
            RegistrantDataMgr regMgr = new RegistrantDataMgr();

            //registrant_type_id = 1  'Collector'
            //registrant_type_id = 2  'Hauler'
            //registrant_type_id = 3  'Steward'
            //registrant_type_id = 4  'Manufacturer'
            //registrant_type_id = 5  'Processor'

            //Run it for each registrant type
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Collector);
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Hauler);
            //var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Processor);
            var registrants = regMgr.GetAllByRegType((int)RegistrantTypeLegacy.Processor);

            VendorRepository vendorRepository = new VendorRepository();
            UserDataMgr userMgr = new UserDataMgr();

            foreach (var reg in registrants)
            {
                var existingVendor = vendorRepository.GetVendorByNumber(reg.registration_number.ToString());
                if (existingVendor != null)
                {
                    var users = userMgr.GetAllByRegNo(reg.registration_number);
                    AppUserRepository appUserRepo = new AppUserRepository();
                    foreach (var user in users)
                    {
                        var existingUser = appUserRepo.GetAppUser(existingVendor.VendorId, (int)user.userId);
                        if (existingUser == null)
                        {
                            appUserRepo.AddAppUser((int)user.userId, existingVendor.VendorId, "system");
                            System.Console.WriteLine("Imoprted: " + reg.registration_number + " " + user.userId);
                        }
                    }
                }
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static void ImportProductsPurchased(RegistrantTireDataMgr regTireMgr, VendorRepository vendorRepository, Vendor vendor, registrant reg)
        {
            //For Processors
            var registrantProducts = regTireMgr.GetAllRegProductsByRegNo(Convert.ToDecimal(vendor.Number));

            //ItemCategoryEnum.ProductsProduced 2 for Products for Processor
            //var defaultItems = vendorRepository.GetDefaultItems((int)ItemCategoryEnum.ProductsProduced);
            
            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Processor)
              .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced);
            
            if (vendor.RegistrantTypeID == (int)RegistrantTypeLegacy.RPM)
            {
                defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
               .Where(s => s.ItemCategory == (int)ItemCategoryEnum.ProductsProduced);          
            }
            
            foreach (var product in registrantProducts)
            {
                int productId = 0;
                //registrantProducts
                //product_id;product_title
                //1;TDP2
                //2;TDP3
                //3;TDP4
                //4;TDP5
                //5;TDF
                //6;Land Fill Cover
                //7;Other
                //8;TDP1
                switch (product.product_id)
                {
                    case 1:
                        productId = 14;
                        break;
                    case 2:
                        productId = 16;
                        break;
                    case 3:
                        productId = 18;
                        break;
                    case 4:
                        productId = 19;
                        break;
                    case 5:
                        productId = 28;
                        break;
                    case 6:
                        productId = 29;
                        break;
                    case 7:
                        productId = 30;
                        break;
                    case 8:
                        productId = 12;
                        break;
                }
                //check for values
                var result = defaultItems.Single(s => s.ID == productId);
                if (result != null)
                {
                    vendor.Items.Add(result);
                }                
            }
            //Special case for processor where Id is not there in legacy but description is there
            if (vendor.RegistrantTypeID == (int)RegistrantTypeLegacy.RPM && vendor.Items.FirstOrDefault(i => i.ID == 30) == null && !string.IsNullOrEmpty(reg.p_products_produced_desc))
            {
                var result = defaultItems.Single(s => s.ID == 30);
                if (result != null)
                {
                    vendor.Items.Add(result);
                }
                vendor.OtherProcessProduct = reg.p_products_produced_desc;
            }
        }

        private static void ImportRPMProducts(RegistrantTireDataMgr regTireMgr, VendorRepository vendorRepository, Vendor vendor)
        {
            //For RPMs
            var registrantEndUserMarket = regTireMgr.GetAllEndUserMarketByRegNo(Convert.ToDecimal(vendor.Number));

            //ItemCategoryEnum.ProcessorType not correct but int value is 3 which is correct for itemcategory in DB
            //var defaultItems = vendorRepository.GetDefaultItems((int)ItemCategoryEnum.RPMType);

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.RPM)
               .Where(s => s.ItemCategory == (int)ItemCategoryEnum.RPMType);

            foreach (var endUserMarket in registrantEndUserMarket)
            {
                int endUserMarketId = 0;
                //end_use_market_id;end_use_market_desc
                //1;Calendered
                //2;Extruded
                //3;Molded
                //4;Other
                //Now map correct items for static data
                switch (endUserMarket.end_use_market_id)
                {
                    case 1:
                        endUserMarketId = 33;
                        break;
                    case 2:
                        endUserMarketId = 34;
                        break;
                    case 3:
                        endUserMarketId = 32;
                        break;
                    case 4:
                        endUserMarketId = 38;
                        break;
                }
                //check for values
                var result = defaultItems.Single(s => s.ID == endUserMarketId);
                if (result != null)
                {
                    vendor.Items.Add(result);
                }
            }
        }

        private static void ImportTires(RegistrantTireDataMgr regTireMgr, VendorRepository vendorRepository, Vendor vendor)
        {
            var registrantTires = regTireMgr.GetAllRegistrantTireByRegNo(Convert.ToDecimal(vendor.Number));
            //we need items from 65 to 72
            //var defaultItems = vendorRepository.GetDefaultItems(5);            
            
            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType);
            
            foreach (var tire in registrantTires)
            {
                //check for values
                var result = defaultItems.FirstOrDefault(s => s.ID == (tire.tire_id + 1));
                if (result != null)
                {
                    vendor.Items.Add(result);
                }
            }
        }

        private void AddParticipantAddresses(registrant reg, Vendor vendor)
        {
            var addressBusiness = new Address();
            addressBusiness.AddressType = (int)AddressTypeEnum.Business;
            addressBusiness.Address1 = reg.location_address_1;
            addressBusiness.Address2 = reg.location_address_2;
            addressBusiness.Address3 = reg.location_address_3;
            addressBusiness.City = reg.location_city;
            addressBusiness.Country = reg.location_country;
            addressBusiness.Email = reg.location_email_address;
            addressBusiness.Fax = reg.location_fax;
            addressBusiness.Phone = reg.location_phone;
            addressBusiness.PostalCode = reg.location_postal_code;
            addressBusiness.Province = reg.location_province;
            vendor.VendorAddresses.Add(addressBusiness);

            var addressMail = new Address();
            addressMail.AddressType = (int)AddressTypeEnum.Mail;
            addressMail.Address1 = reg.mailing_address_1;
            addressMail.Address2 = reg.mailing_address_2;
            addressMail.Address3 = reg.mailing_address_3;
            addressMail.City = reg.mailing_city;
            addressMail.Country = reg.mailing_country;
            //addressMail.Email = reg;
            //addressMail.Fax = reg;
            //addressMail.Phone = reg;
            addressMail.PostalCode = reg.mailing_postal_code;
            addressMail.Province = reg.mailing_province;
            vendor.VendorAddresses.Add(addressMail);

            var addressContact = new Address();
            addressContact.AddressType = (int)AddressTypeEnum.Contact;
            addressContact.Address1 = reg.primary_contact_address_1;
            addressContact.Address2 = reg.primary_contact_address_2;
            addressContact.Address3 = reg.primary_contact_address_3;
            addressContact.City = reg.primary_contact_city;
            addressContact.Country = reg.primary_contact_country;
            addressContact.Email = reg.primary_contact_email;
            addressContact.Fax = reg.primary_contact_fax;
            addressContact.Phone = reg.primary_contact_phone;
            addressContact.PostalCode = reg.primary_contact_postal_code;
            addressContact.Province = reg.primary_contact_province;

            addressContact.Contacts.Add(new Contact()
            {
                Email = reg.primary_contact_email,
                FaxNumber = reg.primary_contact_fax,
                FirstName = reg.primary_contact_name,
                IsPrimary = true,
                Name = reg.primary_contact_name,
                PhoneNumber = reg.primary_contact_phone,
                Position = reg.primary_contact_position,
                PreferredContactMethod = reg.preferred_communication == 1 ? "Email" : (reg.preferred_communication == 2 ? "Fax" : "Mail"),
            });
            vendor.VendorAddresses.Add(addressContact);
        }
        public enum RegistrantTypeLegacy : byte
        {
            Collector = 1,
            Hauler = 2,
            Steward = 3,
            RPM = 4,
            Processor = 5,
        }
        public enum VendorTypeNewTM : byte
        {
            Steward = 1,
            Collector = 2,
            Hauler = 3,
            Processor = 4,
            RPM = 5,
        }
        public int ConvertToVendorType(int registrantType)
        {
            switch (registrantType)
            {
                case (int)RegistrantTypeLegacy.Collector:
                    return (int)VendorTypeNewTM.Collector;
                case (int)RegistrantTypeLegacy.Hauler:
                    return (int)VendorTypeNewTM.Hauler;
                case (int)RegistrantTypeLegacy.Processor:
                    return (int)VendorTypeNewTM.Processor;
                case (int)RegistrantTypeLegacy.RPM:
                    return (int)VendorTypeNewTM.RPM;
                case (int)RegistrantTypeLegacy.Steward:
                    return (int)VendorTypeNewTM.Steward;
            }
            return 0;
        }
    }
}

//customer.TerritoryCode = reg.;
//customer.GlobalDimension1Code = reg;
//customer.GlobalDimension2Code = reg;
//customer.CreditLimit = reg;
//customer.TelexAnswerBack = reg;
//customer.TaxRegistrationNo = reg;
//customer.TaxRegistrationNo2 = reg;
//customer.HomePage = reg;
//customer.BusinessGroupCode = reg;
//customer.CustomerGroupCode = reg;
//customer.DepartmentCode = reg;
//customer.ProjectCode = reg;
//customer.PurchaserCode = reg;
//customer.SalesCampaignCode = reg;
//customer.SalesPersonCode = reg;
//customer.UpdateToFinancialSoftware = reg;
//customer.MailingAddressSameAsBusiness = reg.add;
//vendor.ApplicationID = reg;
//vendor.HasMoreThanOneEmp = reg;
///todoAddparticipantAddresses(reg, vendor);
//virtual ICollection<Address> CustomerAddresses = reg ;      
//virtual ICollection<BusinessActivity> BusinessActivities = reg ;
//virtual ICollection<Item> Items = reg ;
//virtual Application Application = reg ;
///todovendorRepository.AddCustomer(vendor);
//vendor.TaxClass = reg
