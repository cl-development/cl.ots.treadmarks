//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class rr_ots_rm_apply
    {
        public string APTODCNM { get; set; }
        public string APFRDCNM { get; set; }
        public decimal APPTOAMT { get; set; }
        public short APFRDCTY { get; set; }
        public short APTODCTY { get; set; }
        public Nullable<System.DateTime> APPLYDATE { get; set; }
        public Nullable<System.DateTime> GLPOSTDT { get; set; }
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        public string INTERID { get; set; }
        public Nullable<int> INTSTATUS { get; set; }
        public Nullable<System.DateTime> INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        public int DEX_ROW_ID { get; set; }
    
        public virtual rr_ots_rm_cash_receipt rr_ots_rm_cash_receipt { get; set; }
        public virtual rr_ots_rm_transactions rr_ots_rm_transactions { get; set; }
    }
}
