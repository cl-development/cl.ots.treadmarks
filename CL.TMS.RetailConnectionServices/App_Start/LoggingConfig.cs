﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.RetailConnectionServices.App_Start
{
    public class LoggingConfig
    {
        public static void InializeLogging()
        {
            Logger.SetLogWriter(new LogWriterFactory().Create());
        }
    }
}