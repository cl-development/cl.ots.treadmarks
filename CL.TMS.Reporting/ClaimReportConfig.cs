﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Reporting
{
    public static class ClaimReportConfig
    {
        public static void InitializeClaimReporting(IEventAggregator eventAggregator)
        {
            ClaimReportManager.Instance.InitializeClaimReportManager(eventAggregator);
        }
    }
}
