﻿using CL.TMS.Common.Enum;
using CL.TMS.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Services;


namespace CL.TMS.Communicator.Areas.System.Controllers
{
    public class InvitationController : ApiController
    {
        #region Fields
        private IInvitationService invitationService;
        #endregion

        public InvitationController(IInvitationService invitationService)
        {
            this.invitationService = invitationService;
        }
       
        [Route("api/Invitation/CreateApplicationInvitation")]
        [HttpGet] 
        public IHttpActionResult CreateApplicationInvitation(string input)
        {
           
            if (!string.IsNullOrEmpty(input))
            {
               
            string strHost = HttpContext.Current.Request.UrlReferrer.Host;
            if (strHost != ConfigurationManager.AppSettings["allowedHostName"].ToString())
            {
               // response.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
              //  response.ResponseMessages.Add("The method cannot be executed. Invalid hostname: " + strHost);
            }
            else
            {
                string[] inputString = input.Split(',');
                string emailID = inputString[0];
                string participantType = inputString[1];
               // invitationService.ValidateAndCreateApplicationInvitation(emailID, participantType);
            }
            
                //if (Transaction == null)
                //{
                //    return NotFound();
                //}
                return Ok("True");
            }
            return NotFound();
        }
    }
}
