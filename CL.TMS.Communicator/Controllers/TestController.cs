﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CL.TMS.ServiceContracts;

namespace CL.TMS.Communicator.Controllers
{
    public class TestController : ApiController
    {
        #region Fields
        private IInvitationService invitationService;
        #endregion

        public TestController(IInvitationService invitationService)
        {
            this.invitationService = invitationService;
        }

        [HttpGet]
        public IHttpActionResult CreateApplicationInvitation(string input)
        {
            return Ok("True");
        }
    }
}
