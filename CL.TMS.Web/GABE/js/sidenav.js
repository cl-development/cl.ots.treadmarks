$(document).ready(function() {
    /* ======== SETTTINGS START ======== */
    var miniNavSpeed = 700;
    var expandedNavSpeed = 700;
    var transitionClass = "transition-class"; 
    /* ======== SETTTINGS END ======== */
    var $mainContent = $(".main-content");
    var $sideBarNav = $(".sidenav");
    var $navBarToggle = $(".navbar-hbmenu");
    var $subMenuToggles = $(".sub-menu-toggle");
    var $searchIcon = $(".sidenav .nav-sidebar li.search i");
    var $navIcons = $(".sidenav .nav-sidebar li i");
    var navToggle = false;
    
    if (Modernizr.localstorage) {
        if (localStorage.getItem("navToggle") === null) {
            localStorage.setItem("navToggle", navToggle);
        } else {
            if(localStorage.getItem("navToggle") === "true") {
                navToggle = true;
                $mainContent.removeClass("sidenav-collapsed");
            } else {
                navToggle = false;
                $mainContent.addClass("sidenav-collapsed");
            }
        }
        setTimeout(function() {
            $sideBarNav.addClass(transitionClass);
            $mainContent.addClass(transitionClass);
            $navIcons.addClass(transitionClass);
        }, 50);
    }
    
    var toggleNav = function(e) {
        if(navToggle) {
            $mainContent.addClass("sidenav-collapsed");   
        } else {
            $mainContent.removeClass("sidenav-collapsed");
        }
        navToggle = !navToggle;
        localStorage.setItem("navToggle", navToggle);
    }
    
    var openNav = function(e) {
        if(!navToggle) {
            $mainContent.removeClass("sidenav-collapsed");
            navToggle = !navToggle; 
        }
    }
    
    var toggleSubMenu = function(e) {
        $jqObj = $(e.currentTarget);
        $subMenu = $jqObj.siblings(".sub-menu");
        $expandIcon = $jqObj.children(".expand-icon");
        if($expandIcon.html().indexOf("+") > -1) {
            $expandIcon.html("-");
        } else {
            $expandIcon.html("+");
        }
        $subMenu.slideToggle();
        e.preventDefault();
    }
    
    $navBarToggle.click(toggleNav);
    $searchIcon.click(openNav);
    $subMenuToggles.toArray().forEach(function(obj, index) {
        $jqObj = $(obj);
        $jqObj.click(toggleSubMenu);
    });
});