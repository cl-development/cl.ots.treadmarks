$(function(){


// Force Page Reload -- This is to ensure page releands all JS/CSS each time. IMPORTANT: This window.locaction.reload NOT to be used in the Application!
// window.onload = function() {if(!window.location.hash) {window.location = window.location + '#latest';window.location.reload(); }}




	// Enable Tooltips
    $("body").tooltip({ 
    	delay:{ show:400, hide:100 },
    	selector: '[data-toggle=tooltip]' 
    });

    // Enable Collapsible Divs 
    // $('.collapse').collapse();

    // Enable Popovers
    $('[data-toggle="popover"]').popover({trigger: 'hover'});

});


$(function(){
	
	$(".panel-heading a").click(function(e) {

	        if( $(this).hasClass("collapsed") ) {
	            $(this).removeClass("panel-arrow-collapse").addClass("panel-arrow-expand");

	        } else {
	            // if other menus are open remove open class and add closed
	            $(this).removeClass("panel-arrow-expand").addClass("panel-arrow-collapse");
	        }
	});
});


/*
/* Search Interactivity
*/
$('.subcomp-search .glyphicon-search').each(function() {
	$(this).mouseover(function(){
		$( this ).siblings( "input" ).css("visibility", "visible");

		//If the search field is empty, remove the clear icon
		if($( this ).siblings( "input" ).val()!='')
		{
			$( this ).siblings( ".remove-icon" ).show();
			$( this ).siblings( ".subcomp-search-found" ).show();
		}	
		$( this ).siblings( "input" ).focus();

	});
	$( this ).siblings( "input" ).keyup(function(e) {
			//show clear icon when the the user types the search field
			if($(this).val()!='')
			{
				$( this ).siblings( ".remove-icon" ).show();
				$( this ).siblings( ".subcomp-search-found" ).show();
				$('.remove-icon').click(function(n){
					$( this ).siblings( "input" ).val("").focus();
					$( this ).hide();
					$( ".subcomp-search-found" ).hide();
					n.stopPropagation();
				});
			}
			else
			{
				$( this ).siblings( ".remove-icon" ).hide();
			}
			//to prevent hide the search field when the user clicks the clear icon
			e.stopPropagation();
		});
});

//If the search field is empty, hides seach field when the user clicks outside 
$(document).click(function(){
		
	if($( '.subcomp-search input' ).val() === '')
	{
		$( '.subcomp-search input' ).css("visibility", "hidden");
		$( '.subcomp-search input' ).siblings( ".remove-icon" ).hidden();
		$( '.subcomp-search input' ).siblings( ".subcomp-search-found" ).hidden();
	}	
});

/*
/*  All Panels: Expand / Collapse All
*/
  $('.closeall').click(function(){
    $('.panel-collapse.in').collapse('hide');
    $('.closeall').hide();
    $('.openall').show();
    $('.panel-heading a').removeClass("panel-arrow-expand");
    $('.panel-heading a').addClass("panel-arrow-collapse");
 
  });
  $('.openall').click(function(){
    $('.panel-collapse:not(".in")').collapse('show');
    $('.openall').hide();
    $('.closeall').show();
    $('.panel-heading a').removeClass("panel-arrow-collapse");
    $('.panel-heading a').addClass("panel-arrow-expand");
  });


