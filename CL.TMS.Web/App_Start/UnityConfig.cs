using System.Web.Mvc;
using CL.TMS.DependencyBuilder;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace CL.TMS.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // Register applicaiton dependencies
            ApplicationDependencyBuilder.BuildRepositoryDependencies(container);
            
            //Register application services
            ApplicationDependencyBuilder.BuildServiceDependencies(container);

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}