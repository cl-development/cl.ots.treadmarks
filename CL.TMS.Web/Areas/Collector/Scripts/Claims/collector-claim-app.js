﻿'use strict';

var collectorClaimApp = angular.module("collectorClaimApp", ["ui.bootstrap", "commonLib", "commonClaimLib", "commonTransactionLib", "commonActivitiesLib"]);