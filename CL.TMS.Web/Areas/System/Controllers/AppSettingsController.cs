﻿using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.UI.Common.Security;
using CL.TMS.Common;
using CL.TMS.Security;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class AppSettingsController : BaseController
    {

        #region Services
        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        private IConfigurationsServices configurationService;
        #endregion

        public AppSettingsController(IUserService userService, IRegistrantService registrantService, IMessageService messageService, IConfigurationsServices configurationService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
            this.configurationService = configurationService;
        }

        // GET: System/User
        [ClaimsAuthorization(TreadMarksConstants.AdminApplicationSettings)]
        public ActionResult AppSettings()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "configurationsAppSettingsApp";
                ViewBag.SelectedMenu = "AppSettings";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }
        public JsonResult LoadAppSettings(string param)
        {
            AppSettingsVM result = configurationService.LoadAppSettings();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAppSettings(AppSettingsVM appSettingsVM)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminApplicationSettings, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized"});
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            var result = configurationService.updateAppSettings(appSettingsVM, SecurityContextHelper.CurrentUser.Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadInternalNotesList(string parentId)
        {
            List<InternalNoteViewModel> result = configurationService.LoadAppSettingNotesByType(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }
        public ActionResult AddInternalNotes(string parentId, string notes)
        {
            var claimNote = configurationService.AddTransactionNote(parentId, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }

        public ActionResult ExportInternalNotesList(string parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = configurationService.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public ActionResult ApplicationSettingsFrom(AppSettingsVM vm)
        {
            return View("AppSettings");
        }
    }
}