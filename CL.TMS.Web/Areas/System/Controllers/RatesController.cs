﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Resources;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class RatesController : BaseController
    {
        #region Services

        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        private IConfigurationsServices configurationService;

        #endregion Services

        public RatesController(IUserService userService, IRegistrantService registrantService, IMessageService messageService, IConfigurationsServices configurationService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
            this.configurationService = configurationService;
        }

        #region //Fees and Incentives

        [ClaimsAuthorization(TreadMarksConstants.AdminFeesAndIncentives)]
        public ActionResult RatesIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "configurationsRateApp";
                ViewBag.SelectedMenu = "AdminRates";
                ViewBag.SelectedSubMenu = "FeesAndIncentives";
                var rateIndex = AppDefinitions.Instance.
                                TypeDefinitions[DefinitionCategory.RateCategory.ToString()]
                                .Select(x => new BaseItemModel()
                                {
                                    ItemCode = x.Code,
                                    ItemName = x.Name,
                                    ItemValue = x.DefinitionValue,
                                    DisplayOrder = x.DisplayOrder,
                                })
                .OrderBy(x => x.DisplayOrder).ToList();

                rateIndex.ForEach(i =>
                {
                    switch (i.ItemName)
                    {
                        case TreadMarksConstants.TireStewardshipFeeRates: // "TireStewardshipFeesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees).ToString();
                            break;

                        case TreadMarksConstants.RemittancePenaltyRates: // "StewardRemittancePenaltyRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty).ToString();
                            break;

                        case TreadMarksConstants.CollectionAllowanceRates: // "CollectionAllowancesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance).ToString();
                            break;

                        case TreadMarksConstants.TransportationIncentiveRates: // "TransportationIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive).ToString();
                            break;

                        case TreadMarksConstants.ProcessingIncentiveRates: // "ProcessingIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive).ToString();
                            break;
                        case TreadMarksConstants.TIPayee: // "TIPayee":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesTIPayee).ToString();
                            break;
                        case TreadMarksConstants.ManufacturingIncentiveRates: // "ManufacturingIncentivesRates":
                            i.SecurityResultType = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive).ToString();
                            break;

                        default:
                            break;
                    }
                });
                return View("RatesIndex", rateIndex);
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]//clear cache, avoid access this (admin)page by click on browser back button from none-admin page.
        public ActionResult LoadRateList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                { 0,"ID"},
                {1, "EffectiveStartDate"},
                {2, "DateAdded"},
                {3, "AddedBy"},
                {4, "DateModified"},
                {5, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = configurationService.LoadRateList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                isFutureRateExists = (2 != category) ? result.DTOCollection.Any(x => x.EffectiveStartDate > today) : configurationService.IsFutureCollectorRateTransactionExists(category),
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }

        public ActionResult LoadPIRateList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                { 0,"ID"},
                {1,"PIType"},
                {2, "EffectiveStartDate"},
                {3, "EffectiveStartDate"},
                {4, "DateAdded"},
                {5, "AddedBy"},
                {6, "DateModified"},
                {7, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = configurationService.LoadPIRateList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false),
                category = category,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }
        public ActionResult LoadTIPayeeList(DataGridModel param, int category)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0,"ID"},            
                {1, "EffectiveStartDate"},
                {2, "EffectiveEndDate"},
                {3, "DateAdded"},
                {4, "AddedBy"},
                {5, "DateModified"},
                {6, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = configurationService.LoadTIPayeeList(param.Start, param.Length, searchText, orderBy, sortDirection, category);
            var today = DateTime.Today;
            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                category = category,
                isFutureRateExists =  result.DTOCollection.Any(x => x.EffectiveStartDate > today) ,
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name,
            };

            return new NewtonSoftJsonResult(json, false);
        }
        [HttpGet]
        public ActionResult LoadTransactionNotesList(int parentId)
        {
            List<InternalNoteViewModel> result = configurationService.LoadRateTransactionNoteByID(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }
        [HttpGet]
        public ActionResult LoadTIPayeeListNotes(int parentId)
        {
            List<InternalNoteViewModel> result = configurationService.LoadTIPayeeNotesByID(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }
        [HttpPost]
        public ActionResult AddNewRate(RateDetailsVM newRate, int category)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            bool result = configurationService.AddNewRate(newRate, SecurityContextHelper.CurrentUser.Id, category);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }
            return Json(new
            {
                status = result,
                startDate = newRate.effectiveDate,
                category = "TI",
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false)
            });
        }

        [HttpPost]
        public ActionResult EditSaveRate(RateDetailsVM rateVM)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            rateVM.effectiveStartDate = new DateTime(rateVM.effectiveStartDate.Year, rateVM.effectiveStartDate.Month, 1);//UI send 2ed day of month due to fix FF date time issue, so set back to first day of month here.
            var result = configurationService.updateRate(rateVM, SecurityContextHelper.CurrentUser.Id);

            return Json(new
            {
                status = result,
                isSpecific = rateVM.isSpecific,
                category = rateVM.category,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(rateVM.category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(rateVM.category, false)
            });
        }

        [HttpPost]
        public ActionResult RemoveFromList(int rateTransactionID, int category)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminFeesAndIncentives, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = configurationService.RemoveRateTransaction(rateTransactionID, category);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            var json = new
            {
                status = true,
                result = result,
                statusMsg = MessageResource.ValidData,
                category = category,
                isFutureSpecificRateExists = configurationService.IsFutureRateExists(category, true),
                isFutureGlobalRateExists = configurationService.IsFutureRateExists(category, false)
            };
            return new NewtonSoftJsonResult(json, false);
        }

        public ActionResult LoadRateDetailsByTransactionID(int rateTransactionID, int category, bool? isSpecific)
        {
            bool isSpecial = isSpecific ?? false;
            RateDetailsVM result = configurationService.LoadRateDetailsByTransactionID(rateTransactionID, category, isSpecial);
            result.notes.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        public ActionResult IsFutureRateExists(int category)
        {
            bool result = configurationService.IsFutureRateExists(category);
            return Json(new { result = result, statusMsg = MessageResource.ValidData });
        }

        public ActionResult IsFutureCollectorRateExists(int category)
        {
            bool result = configurationService.IsFutureCollectorRateExists(category);
            return Json(new { result = result, statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandler(int parentId, string notes)
        {
            var claimNote = configurationService.AddTransactionNote(parentId, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }
        [HttpPost]
        public NewtonSoftJsonResult AddPayeeNotesHandler(int parentId, string notes)
        {
            var claimNote = configurationService.AddPayeeNote(parentId, notes);
            claimNote.AddedOn = ConvertTimeFromUtc(claimNote.AddedOn);
            return new NewtonSoftJsonResult(claimNote, false);
        }
        public ActionResult ExportTransactionNotesList(int parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = configurationService.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        public ActionResult ExportPayeeNotesList(int parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> claimNotes = configurationService.ExportPayeeNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion //Fees and Incentives
    }
}