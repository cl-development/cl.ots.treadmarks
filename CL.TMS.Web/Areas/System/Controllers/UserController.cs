﻿using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.Resources;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class UserController : BaseController
    {

        #region Services
        private IUserService userService;
        #endregion

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET: System/User
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ClaimsAuthorization(TreadMarksConstants.AdminRolesAndPermissions)]
        public ActionResult RoleIndex()
        {
            if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminRolesAndPermissions) >= CL.TMS.Security.SecurityResultType.ReadOnly)
            {
                if (Session["IsAdminLogin"] != null)
                {
                    ViewBag.ngApp = "commonUserLib";
                    ViewBag.SelectedMenu = "RolesPermissions";
                    return View();
                }
                else
                {
                    return RedirectToAction("AdminLoginPage","Account", new { area = "" });
                }
            }
            else
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
            }
        }

        [HttpPost]
        public JsonResult RoleListHandler(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "RoleName"},
                {1, "UpdateDate" },
                {2, "AssociatedUsers"},
                {3, "RoleID"} //for delete role
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = userService.GetAllRoles(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUserNameByRoleName(string roleName)
        {
            var userNameList = userService.GetUserNameByRoleName(roleName);
            return Json(userNameList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPermissionsForRole(string roleName)
        {
            var rolePermissions = userService.GetRolePermissions(roleName);
            var roleViewModel = new RoleViewModel();
            roleViewModel.Id = rolePermissions.FirstOrDefault().RoleId;
            roleViewModel.RoleName = roleName;
            roleViewModel.RolePermissions = rolePermissions;
            return Json(roleViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDefaultRolePermissions()
        {
            var roleViewModel = new RoleViewModel();
            roleViewModel.RolePermissions = userService.GetDefaultRolePermissions();
            return Json(roleViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public NewtonSoftJsonResult EditUserRole(RoleViewModel roleViewModel, string serializedRolePermissions, bool IsEditConfirmed = false)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminUsersStaffUsers, (int)SecurityResultType.EditSave))
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = "Unauthorized" } };
            };
            List<RolePermissionViewModel> DeserializedRolePermissions = new JavaScriptSerializer().Deserialize<List<RolePermissionViewModel>>(serializedRolePermissions);
            roleViewModel.RolePermissions = DeserializedRolePermissions;

            var userName = SecurityContextHelper.CurrentUser.Email;
            bool IsRoleNameAlreadyExists = false;
            
            if (roleViewModel.Id > 0)
            {
                if (IsEditConfirmed)
                {
                    userService.UpdateUserRole(roleViewModel, userName);
                }
                else
                {
                    IsRoleNameAlreadyExists = userService.RoleNameExists(roleViewModel.RoleName);
                }
            }

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, IsRoleNameAlreadyExists = IsRoleNameAlreadyExists } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddUserRole(RoleViewModel roleViewModel, string serializedRolePermissions, bool IsAddConfirmed = false)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminUsersStaffUsers, (int)SecurityResultType.EditSave))
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = "Unauthorized" } };
            };
            List<RolePermissionViewModel> DeserializedRolePermissions = new JavaScriptSerializer().Deserialize<List<RolePermissionViewModel>>(serializedRolePermissions);
            roleViewModel.RolePermissions = DeserializedRolePermissions;

            var userName = SecurityContextHelper.CurrentUser.Email;
            bool IsRoleNameAlreadyExists = false;

            if (IsAddConfirmed)
            {
                userService.AddNewUserRole(roleViewModel, userName);
            }
            else
            {
                //check if role name already exists
                IsRoleNameAlreadyExists = userService.RoleNameExists(roleViewModel.RoleName);
            }

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, IsRoleNameAlreadyExists = IsRoleNameAlreadyExists } };
        }

        [HttpPost]
        public NewtonSoftJsonResult DeleteUserRole(int roleID)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminUsersStaffUsers, (int)SecurityResultType.EditSave))
            {
                return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = "Unauthorized" } };
            };

            bool result= userService.DeleteUserRole(roleID);

            return new NewtonSoftJsonResult { Data = new { status = result, statusMsg = MessageResource.ValidData } };
        }

    }
}