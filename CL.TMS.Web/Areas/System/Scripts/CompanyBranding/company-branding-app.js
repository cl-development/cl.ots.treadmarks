﻿'use strict';
var validMsg = {
    clean: "clean",
    isRequired: "Required",
    noneMsg: ""
}

/**********************
 * Company Branding App
 **********************/
var companyBrandingApp = angular.module("companyBrandingApp",
    ["commonLib", "ui.bootstrap", "companyBrandingApp.directives", "companyBrandingApp.controllers", "companyBrandingApp.services", "summernoteLib"]);

/***********************
 * Controllers Section *
 ***********************/
var controllers = angular.module('companyBrandingApp.controllers', []);
// main controllers
controllers.controller('companyBrandingController', ['$rootScope', '$scope', '$http', '$uibModal', 'companyBrandingAppServices', function ($rootScope, $scope, $http, $uibModal, companyBrandingAppServices) {
    $scope.vm = {
        termConditionID: 0,
        applicationTypeID: 0,
        applicationName:"",
        content: "",
        effectiveStartDate: null,
        note: "",
        notes: {},
    };

    //base on security determining isReadonly or not.
    $scope.isView = Global.CompanyBranding.Security.TermsAndConditions===Global.CompanyBranding.Security.Constants.ReadOnly;
    if ($scope.isView) {
        $("#tncNote").summernote({height:210});
        $('.note-editable').attr('contenteditable', false);
        $('.note-editor > .note-toolbar button').attr("disabled", "disabled");
        $(".note-btn-group.btn-group.note-view").hide();
        var insertButtons = $('.note-btn-group.btn-group.note-insert  button');
        $.each(insertButtons, function (index, button) {
            if ($(button).data("original-title").indexOf("Link") < 0) {
                $(button).hide();
            }
        });
    }

    $("#allClaimTypeList").on("change", function () {
        //init
        var selectedValue = $(this).find("option:selected").val();
        var selectedName = $(this).find("option:selected").text();
        $("#applicationName").html(selectedName);
        if (selectedValue > 0) {          
            $("#termsAndConditionsContents").show();
            //security            
            companyBrandingAppServices.getDetailsByID(selectedValue).then(function (data) {
                $scope.vm = data.data;
                $scope.applicationTypeID = $scope.vm.applicationTypeID;
            });
        } else {
            $("#termsAndConditionsContents").hide();
        }
    });

    $scope.isValid = { noteIsRequire: true, note: true };

    $scope.submitContent = function (data) {
        var content=$(".note-editable").text();
        if (content.trim().length == 0) {
            showValidinfo("",validMsg.isRequired)
            return false;
        }

        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                confirmMessage: function () {
                    return Global.CompanyBranding.ConfirmHtml.replace("AAA", $scope.vm.applicationName);
                }
            }
        });
        confirmModal.result.then(function (e) { //confirm  

            companyBrandingAppServices.SaveTermCondition($scope.vm).then(function (response) {
                if (response && response.data && response.data.status) {
                    $scope.freshnote = true;
                    $('html, body').animate({
                        scrollTop: $("#fixedClaimHeader").offset().top - 680
                    }, 500);
                } else {
                    var failModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'failCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () {
                                return Global.CompanyBranding.FailHeader;
                            },
                            failResult: function () {
                                return response.data.status ? Global.CompanyBranding.FailHtml.replace('AAA', $scope.vm.applicationName) : response.data.statusMsg;
                            }
                        }
                    });
                }
            });
        });      
    }

    // internal notes  
    $scope.freshnote = false;
    $scope.addUrl = Global.CompanyBranding.AddNoteUrl;
    $scope.loadUrl = Global.CompanyBranding.LoadNotesUrl;
    $scope.exportUrl = Global.CompanyBranding.ExportNotesUrl;  

    $scope.cancel = function () {
        companyBrandingAppServices.getDetailsByID($scope.vm.applicationTypeID).then(function (data) {
            $scope.vm = data.data;
            $scope.freshnote = true;
        });
    }
}]);

controllers.controller('companyInformationController',['$rootScope', '$scope', '$http', '$uibModal', 'companyBrandingAppServices', '$window', function ($rootScope, $scope, $http, $uibModal, companyBrandingAppServices, $window) {
    
    //base on security determining isReadonly or not.
    $scope.isView = Global.CompanyInformation.Security.CompanyInformation === Global.CompanyInformation.Security.Constants.ReadOnly;

    $scope.companyInformationValidityOutput = false;

    $scope.rateTransactionID = 'CompanyInformation';

    // internal notes
    $scope.addUrl = Global.CompanyInformation.AddNoteUrl;
    $scope.loadUrl = Global.CompanyInformation.LoadNotesUrl;
    $scope.exportUrl = Global.CompanyInformation.ExportNotesUrl;  

    //validation part
    $scope.companyInformationValidity = {
        companyName: false,
        addressLine1: false,
        city: false,
        province: false,
        postal: false,
        postal_regex: false,
        country: false,
        phone: false,
        phone_regex: false,
        fax: false,
        fax_regex: false,
        email: false,
        email_regex: false,
        webSite: false,
        webSite_regex: false,
        facebook: false,
        facebook_regex: false,
        twitter: false,
        twitter_regex: false
    }
    
    var regex_postal_ca = new RegExp("^[ABCEGHJ-NPRSTVXY][0-9][ABCEGHJ-NPRSTV-Z][ ]?[0-9][ABCEGHJ-NPRSTV-Z][0-9]$");
    var regex_postal_us = new RegExp("^[0-9]{5}(-[0-9]{4})?$");
    var regex_phone = new RegExp("^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$");
    var regex_email = new RegExp("^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$");
    var regex_webSite = new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/);

    $scope.$watch('currentSupportingDocument.url', function (newVal, oldVal) {

        console.log(newVal);
        $scope.vm2 = newVal;
    }, true);

    $scope.submitContent = function () {
        companyInformationValidityCheck();
        if ($scope.companyInformationValidityOutput) {
            $scope.companyInformationValidityOutput = false;
            //alert('Validation error. One or more fields are not valid.');
            window.scrollTo(0, 0);
            return false;
        }
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                confirmMessage: function () {
                    return Global.CompanyInformation.ConfirmHtml;
                }
            }
        });

        confirmModal.result.then(function (e) {
            var vm = $scope.companyInfoViewModel;
            var url = $scope.currentSupportingDocument.url;
            companyBrandingAppServices.updateCompanyInformation(vm, url).then(function (data) {
                if (data.data.status) {
                    $window.location.reload();
                }
                else {
                    companyInformationValidityCheck();
                    console.log(data.data.statusMsg);
                    alert(data.data.statusMsg);
                }
            });
        });
    }

    $scope.cancelChange = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'cancel-modal.html',
            controller: 'cancelCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                cancelMessage: function () {
                    return Global.CompanyInformation.CancelHtml;
                }
            }
        });
    }

    function companyInformationValidityCheck() {
        if (!$scope.companyInfoViewModel.CompanyName) {
            $scope.companyInformationValidity.companyName = true;
            angular.element("#companyName").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#companyName").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.companyName = false;
            angular.element("#companyName").closest('.form-group').removeClass('has-error').addClass('has-success');
            angular.element("#companyName").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
        }       

        //validate address line 1
        if (!$scope.companyInfoViewModel.AddressLine1) {
            $scope.companyInformationValidity.addressLine1 = true;
            angular.element("#addressLine1").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#addressLine1").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.addressLine1 = false;
            angular.element("#addressLine1").closest('.form-group').removeClass('has-error').addClass('has-success');
            angular.element("#addressLine1").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
        }

        //validate city
        if (!$scope.companyInfoViewModel.City) {
            $scope.companyInformationValidity.city = true;
            angular.element("#city").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#city").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.city = false;
            angular.element("#city").closest('.form-group').removeClass('has-error').addClass('has-success');
            angular.element("#city").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
        }

        //validate province
        if (!$scope.companyInfoViewModel.Province) {
            $scope.companyInformationValidity.province = true;
            angular.element("#province").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#province").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.province = false;
            angular.element("#province").closest('.form-group').removeClass('has-error').addClass('has-success');
            angular.element("#province").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
        }

        //validate postal
        if (!$scope.companyInfoViewModel.PostalCode) {
            $scope.companyInformationValidity.postal = true;
            $scope.companyInformationValidity.postal_regex = false;
            angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.postal = false;
            var selectedCountry = angular.element("#country").find(":selected").text();
            if (selectedCountry == "Canada") {
                var postal_regex_validity_ca = regex_postal_ca.test($scope.companyInfoViewModel.PostalCode.toUpperCase());
                if (!postal_regex_validity_ca) {
                    $scope.companyInformationValidity.postal_regex = true;
                    angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                    angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                }
                else {
                    $scope.companyInformationValidity.postal_regex = false;
                    angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                    angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                }
            }
            else if (selectedCountry == "United States") {
                var postal_regex_validity_us = regex_postal_us.test($scope.companyInfoViewModel.PostalCode);
                if (!postal_regex_validity_us) {
                    $scope.companyInformationValidity.postal_regex = true;
                    angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                    angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                }
                else {
                    $scope.companyInformationValidity.postal_regex = false;
                    angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                    angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                }
            }
            else if (selectedCountry == "Please Select") {
                $scope.companyInformationValidity.postal_regex = true;
                angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.postal_regex = false;
                angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }           
        }

        //validate country
        if (!$scope.companyInfoViewModel.Country) {
            $scope.companyInformationValidity.country = true;
            angular.element("#country").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#country").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.country = false;
            angular.element("#country").closest('.form-group').removeClass('has-error').addClass('has-success');
            angular.element("#country").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
        }

        //validate phone
        if (!$scope.companyInfoViewModel.PhoneNumber) {
            $scope.companyInformationValidity.phone = true;
            $scope.companyInformationValidity.phone_regex = false;
            angular.element("#phone").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#phone").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.phone = false;
            var phone_regex_validity = regex_phone.test($scope.companyInfoViewModel.PhoneNumber);
            if (!phone_regex_validity) {
                $scope.companyInformationValidity.phone_regex = true;
                angular.element("#phone").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#phone").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.phone_regex = false;
                angular.element("#phone").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#phone").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }
        }

        //validate fax
        if (!$scope.companyInfoViewModel.Fax) {
            $scope.companyInformationValidity.fax = true;
            $scope.companyInformationValidity.fax_regex = false;
            angular.element("#fax").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#fax").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.fax = false;
            var phone_regex_validity = regex_phone.test($scope.companyInfoViewModel.Fax);
            if (!phone_regex_validity) {
                $scope.companyInformationValidity.fax_regex = true;
                angular.element("#fax").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#fax").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.fax_regex = false;
                angular.element("#fax").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#fax").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }
        }

        //validate email
        if (!$scope.companyInfoViewModel.Email) {
            $scope.companyInformationValidity.email = true;
            $scope.companyInformationValidity.email_regex = false;
            angular.element("#email").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#email").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.email = false;
            var email_regex_validity = regex_email.test($scope.companyInfoViewModel.Email);
            if (!email_regex_validity) {
                $scope.companyInformationValidity.email_regex = true;
                angular.element("#email").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#email").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.email_regex = false;
                angular.element("#email").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#email").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }

        }

        //validate website
        if (!$scope.companyInfoViewModel.WebSite) {
            $scope.companyInformationValidity.webSite = true;
            $scope.companyInformationValidity.webSite_regex = false;
            angular.element("#webSite").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#webSite").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.webSite = false;
            var webSite_regex_validity = regex_webSite.test($scope.companyInfoViewModel.WebSite);
            if (!webSite_regex_validity) {
                $scope.companyInformationValidity.webSite_regex = true;
                angular.element("#webSite").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#webSite").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.webSite_regex = false;
                angular.element("#webSite").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#webSite").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }
        }

        //validate facebook
        if (!$scope.companyInfoViewModel.Facebook) {
            $scope.companyInformationValidity.facebook = true;
            $scope.companyInformationValidity.facebook_regex = false;
            angular.element("#facebook").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#facebook").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.facebook = false;
            var facebook_regex_validity = regex_webSite.test($scope.companyInfoViewModel.Facebook);
            if (!facebook_regex_validity) {
                $scope.companyInformationValidity.facebook_regex = true;
                angular.element("#facebook").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#facebook").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.facebook_regex = false;
                angular.element("#facebook").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#facebook").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }
        }

        //validate twitter
        if (!$scope.companyInfoViewModel.Twitter) {
            $scope.companyInformationValidity.twitter = true;
            $scope.companyInformationValidity.twitter_regex = false;
            angular.element("#twitter").closest('.form-group').addClass('has-error').removeClass('has-success');
            angular.element("#twitter").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
        }
        else {
            $scope.companyInformationValidity.twitter = false;
            var twitter_regex_validity = regex_webSite.test($scope.companyInfoViewModel.Twitter);
            if (!twitter_regex_validity) {
                $scope.companyInformationValidity.twitter_regex = true;
                angular.element("#twitter").closest('.form-group').addClass('has-error').removeClass('has-success');
                angular.element("#twitter").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            }
            else {
                $scope.companyInformationValidity.twitter_regex = false;
                angular.element("#twitter").closest('.form-group').removeClass('has-error').addClass('has-success');
                angular.element("#twitter").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            }
        }

        angular.element("#companyName").closest('.form-group').removeClass('has-required');
        angular.element("#addressLine1").closest('.form-group').removeClass('has-required');
        angular.element("#city").closest('.form-group').removeClass('has-required');
        angular.element("#province").closest('.form-group').removeClass('has-required');
        angular.element("#postal").closest('.form-group').removeClass('has-required');
        angular.element("#country").closest('.form-group').removeClass('has-required');
        angular.element("#phone").closest('.form-group').removeClass('has-required');
        angular.element("#fax").closest('.form-group').removeClass('has-required');
        angular.element("#email").closest('.form-group').removeClass('has-required');
        angular.element("#webSite").closest('.form-group').removeClass('has-required');
        angular.element("#facebook").closest('.form-group').removeClass('has-required');
        angular.element("#twitter").closest('.form-group').removeClass('has-required');
        angular.element("#addressLine2").closest('.form-group').addClass('has-success');
        angular.element("#ext").closest('.form-group').addClass('has-success');

        $.each($scope.companyInformationValidity, function (index, value) {
            if (value) {
                $scope.companyInformationValidityOutput = true;
            }          
        });
    }

    //for popover
    var servicelLevelPopover = {
        content: Global.Settings.logoContent
    };

    $('#logo-title-question-mark').webuiPopover('destroy').webuiPopover($.extend({}, Global.Settings.popover, servicelLevelPopover));

}]);

//common Controllers
controllers.controller('successCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'companyBrandingAppServices', 'successResult', 'header', function ($scope, $uibModal, $uibModalInstance,companyBrandingAppServices, successResult, header) {
    $scope.sucessMessage = successResult;
    $scope.successHeader = header;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.$root.freshnote = true;
    };
}]);
controllers.controller('failCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', function ($scope, $uibModal, $uibModalInstance, failResult, header) {
    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('confirmCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'confirmMessage', function ($scope, $uibModal, $uibModalInstance, confirmMessage) {
    $scope.confirmMessage = confirmMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');   
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
    };
}]);
controllers.controller('cancelCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'cancelMessage', '$window', function ($scope, $uibModal, $uibModalInstance, cancelMessage, $window) {
    $scope.cancelMessage = cancelMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');        
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
        $window.location.reload();
    };
}]);
/********************
 * Services Section
 ********************/
var services = angular.module('companyBrandingApp.services', []);
services.factory('companyBrandingAppServices', ["$http", function ($http) {
    var factory = {};

    factory.SaveTermCondition = function (data) {
        var submitVal = {
            url: Global.CompanyBranding.SaveTermConditionUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }

    factory.RemoveTermCondition = function (claimTypeID) {
        var submitVal = {
            url: Global.CompanyBranding.RemoveTermConditionUrl,
            method: "POST",
            data: { applicationTypeID: claimTypeID, }
        }
        return $http(submitVal);
    }

    factory.getDetailsByID = function (claimTypeID) {
        return $http({
            method: 'GET',
            url: Global.CompanyBranding.LoadTermConditionDetailsByIDUrl,
            params: {
                applicationTypeID: claimTypeID,
            }
        });
    }

    factory.loadNotes = function (applicationTypeID) {
        return $http({
            method: 'GET',
            url: Global.CompanyBranding.LoadNotesUrl,
            params: {
                applicationTypeID: applicationTypeID,
            }
        });
    }

    factory.loadCompanyInformation = function () {
        return $http({
            method: 'GET',
            url: Global.CompanyInformation.LoadCompanyInformationUrl,
            params: {}
        });
    }

    factory.loadCompanyLogoPath = function () {
        return $http({
            method: 'GET',
            url: Global.CompanyInformation.LoadCompanyLogoPathUrl,
            params: {}
        });
    }

    factory.updateCompanyInformation = function (vm, url) {
        var submitVal = {
            url: Global.CompanyInformation.UpdateCompanyInformationUrl,
            method: "POST",
            data: { companyInformationVM: vm, logoFilePath: url }
        }
        return $http(submitVal);
    }
    return factory;
}]);

/*********************
 * Directives Section
 *********************/
var directives = angular.module('companyBrandingApp.directives', []);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

directives.directive('tablerowpopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

directives.directive('internalNotes', ['$compile', '$window', '$timeout', '$http', '$filter', function ($compile, $window, $timeout, $http, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@',
            applicationTypeId: '@',
            isView:'@',
            reloaddata: '@',
        },
        link: function (scope, elem, attr, ngModel) {
            scope.noteTDWidthpixel = 0;
            scope.elem = elem;
            elem.find("textarea").prop('disabled', scope.isView == "true");            
            elem.find("button").prop('disabled', scope.isView == "true");  

            scope.focus = function () {
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            scope.removeIcon = function ($event) {
                scope.searchNote = '';
                var search = $event.currentTarget.parentElement.firstElementChild;
                $(search).focus();
            }

            //internal notes search
            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                    scope.allInternalNotes = $filter('filter')(scope.$parent.vm.notes, scope.searchNote);
                    scope.internalNoteLength = scope.allInternalNotes.length;
                } else {
                    scope.allInternalNotes = scope.$parent.vm.notes;
                }
            });
            //internal notes view more
            scope.viewMoreClick = function ($event) {
                scope.viewMore = false;
                scope.scroller = true;
                scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 250);
            }
            //view more
            scope.viewMoreNotesClick = function ($event) {
                scope.quantity = scope.vm.notes.length;
                scope.viewMore = false;
                scope.scroller = true;
                $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
            }

            //internal notes remove icon
            scope.removeIcon = function () {
                scope.searchNote = '';
                scope.removeIconStyle = { 'display': 'none' };
                scope.foundStyle = { 'display': 'none' };
                scope.searchStyle = '';
            }
            //internal notes sorting
            scope.noteSorting = function (sortType) {
                scope.sortType = sortType;
                scope.sortReverse = !scope.sortReverse;
                scope.direction = scope.sortReverse ? "-" + sortType : "+" + sortType;
                scope.allInternalNotes = $filter('orderBy')(scope.allInternalNotes, scope.direction);
            };
            scope.add = function () {
                if (scope.textAreaInput) {
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            applicationTypeID: scope.applicationTypeId,
                            notes: scope.textAreaInput
                        })
                    }).success(function (result) {
                        $http({
                            url: scope.loadUrl,
                            method: "GET",
                            params: {
                                applicationTypeID: scope.applicationTypeId,
                            },
                        }).success(function (result) {
                            scope.textAreaInput = '';
                            scope.allInternalNotes = result;
                        })
                    })
                }
            };

            scope.stripNotes = function (note) {
                var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one //.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                var trimWidth = 0;
                var bestFitNbr = 50;
                var shortNote = '';
                if (noteWidth + 15 >= scope.noteTDWidthpixel) {
                    while (trimWidth < scope.noteTDWidthpixel - 15) {
                        bestFitNbr += 2;
                        shortNote = noteTrimedStr.substr(0, bestFitNbr);//incrase str size and try again
                        trimWidth = getTextWidth(shortNote, "0.875em Open Sans");
                    }
                    return shortNote + "...";
                }
                else {
                    return note;
                }
            };

            scope.checkNoteSize = function (note) {
                if (note != undefined) {
                    var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    var tdAddedBy = document.getElementById('tdAddedBy');
                    if (hasScrollBar(internalNoteBody)) {
                        $(tdAddedBy).css("margin-left", "-20px");
                        return (scope.noteTDWidthpixel <= noteWidth + 32);
                    }
                    else {
                        return (scope.noteTDWidthpixel <= noteWidth + 6);
                    }
                }
                else {
                    return false;
                }
            };

            function clickInsidePopover(e) {
                if ((e == null) || (e == undefined)) {
                    return false;
                } else if ((e.className === 'popover-content') || (e.className === 'popover-title') || (e.className.startsWith('popover fade'))) {
                    return true;
                }
                else {
                    return clickInsidePopover(e.parentElement);
                }
            }

            function getTextWidth(text, font) {
                var canvas = document.getElementById('noteCanvas');
                var context = canvas.getContext("2d");
                context.font = font;
                var metrics = context.measureText(text);
                return Math.round(metrics.width);
            }

            //OTSTM2-845 check if vertical scrollbar appears
            function hasScrollBar(obj) {
                return $(obj).get(0).scrollHeight > $(obj).height();
            }

            angular.element($window).bind('resize', function () {
                var length = scope.allInternalNotes ? scope.allInternalNotes.length : 0;
                for (var i = 0; i < length; i++) {
                    if (!scope.allInternalNotes[i].Note) { continue };
                    var noteTrimedStr = scope.allInternalNotes[i].Note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    var trimWidth = 0;
                    var bestFitNbr = 50;
                    var shortNote = scope.stripNotes(scope.allInternalNotes[i].Note);
                    scope.noteTDWidthpixel = 0;
                    var $div = $("<td ng-if=\"checkNoteSize(note.Note)\" width=\"75%\" style=\"cursor: pointer\" class=\"internal-note{{$index}}\"><span internal-note-hover-popover data-note=\"{{note.Note}}\">" + shortNote + "</span></td>");
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    if (hasScrollBar(internalNoteBody)) {
                        if (scope.noteTDWidthpixel <= noteWidth + 32) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                scope.$apply();
                            }
                        }
                    }
                    else {
                        if (scope.noteTDWidthpixel <= noteWidth + 6) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                scope.$apply();
                            }
                        }
                    }
                }
                scope.$digest();
            });

            angular.element("#exportInternalNote").bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                var submitVal = {
                    url: scope.exportUrl,
                    method: "GET",
                    params: {
                        applicationTypeID: scope.applicationTypeId,
                        sortReverse: scope.sortReverse,
                        sortcolumn: scope.sortColumn,
                        searchText: scope.searchNote,
                    },
                }
                return $http(submitVal).then(function (result) {
                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = "Terms-Conditions-Internal-Note-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = '<a title="Export to CSV" href="javascript:void(0);" style="display: none;">';
                        var body = $(document.body).append(anchor);
                        $(anchor).attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();
                        $(anchor).remove();
                    }
                });
            });          
          
        },
        controller: function ($scope, $rootScope) {
            $scope.$watch('applicationTypeId', function (applicationTypeId) {
                if (applicationTypeId > 0) {
                    $http({
                        url: $scope.loadUrl,
                        method: "GET",
                        params: {
                            applicationTypeID: applicationTypeId,
                        },
                    }).then(function (result) {
                        $scope.allInternalNotes = result.data;
                        $scope.$parent.vm.notes = result.data;
                        $scope.$parent.freshnote = false;
                        $scope.sortType = 'AddedOn';
                        $scope.textAreaInput = '';
                        $scope.sortReverse = true;
                        $scope.searchNote = '';
                        $scope.quantity = 5;
                        $scope.showMore = true;
                    })
                } else {
                    $scope.allInternalNotes = {};
                }
            });
            $scope.$watch('reloaddata', function () {             
                if ($scope.reloaddata == "true") {
                    $http({
                        url: $scope.loadUrl,
                        method: "GET",
                        params: {
                            applicationTypeID: $scope.applicationTypeId,
                        },
                    }).then(function (result) {
                        $scope.allInternalNotes = result.data;
                        $scope.$parent.vm.notes = result.data;
                        $scope.$parent.freshnote = false;
                        $scope.sortType = 'AddedOn';
                        $scope.textAreaInput = '';
                        $scope.sortReverse = true;
                        $scope.searchNote = '';
                        $scope.quantity = 5;
                        $scope.showMore = true;
                    });
                }
            });
            $scope.$watch('textAreaInput', function (text) {
                $scope.$parent.vm.note = text;         
            });
        }
    };
}]);

directives.directive('internalNoteHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var header = '<span><strong>Internal Notes</strong></span>' +
                    '<button type="button" class="close" ng-click="closeMe()")>' +
                    '<span aria-hidden="true">×</span></button>';
            var content = '<div style="overflow: auto; height: 300px; padding:true;">' + toHtml(attrs.note); + '</div>';
            $(element).popover({
                container: 'div.internal-wrap',
                content: function () {
                    return $compile(content)(scope);
                },
                placement: function (target, conts) {
                    $("div.popover.fade.in").popover("hide");
                    $(target).css({ "width": "500px", "max-width": "500px", "cursor": "default" });
                    return "top";
                },
                title: function () {
                    return $compile(header)(scope);
                },
                html: true
            });
            scope.closeMe = function () {
                $(element).popover('hide');
            }
            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }
        },
        controller: function ($scope, $element) {
        }
    };
});

directives.directive('companyInformationSection', ['$rootScope', 'companyBrandingAppServices', function ($rootScope, companyBrandingAppServices) {
    return {
        restrict: 'E',
        templateUrl: 'company-information-section.html',
        scope: {
            companyInfoViewModel: "=",
            companyInformationValidity: "=",
            isView: "="
        },

        link: function (scope, el, attrs) {
            companyBrandingAppServices.loadCompanyInformation().then(function (data) {
                scope.companyInfoViewModel = data.data;
            });
        },
        controller: function ($scope) {

            //validation part
            $scope.companyInformationValidity = {
                companyName: false,
                addressLine1: false,
                city: false,
                province: false,
                postal: false,
                postal_regex: false,
                country: false,
                phone: false,
                phone_regex: false,
                fax: false,
                fax_regex: false,
                email: false,
                email_regex: false,
                webSite: false,
                webSite_regex: false,
                facebook: false,
                facebook_regex: false,
                twitter: false,
                twitter_regex: false
            }
            
            var regex_postal_ca = new RegExp("^[ABCEGHJ-NPRSTVXY][0-9][ABCEGHJ-NPRSTV-Z][ ]?[0-9][ABCEGHJ-NPRSTV-Z][0-9]$");
            var regex_postal_us = new RegExp("^[0-9]{5}(-[0-9]{4})?$");
            var regex_phone = new RegExp("^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$");
            var regex_email = new RegExp("^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$");
            var regex_webSite = new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/);

            //validate company name
            $scope.$watch('companyInfoViewModel.CompanyName', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined){
                    angular.element("#companyName").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.CompanyName) {
                        $scope.companyInformationValidity.companyName = true;
                        angular.element("#companyName").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#companyName").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.companyName = false;
                        angular.element("#companyName").closest('.form-group').removeClass('has-error').addClass('has-success');
                        angular.element("#companyName").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                    }
                }
            }, true);

            //validate address line1
            $scope.$watch('companyInfoViewModel.AddressLine1', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#addressLine1").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.AddressLine1) {
                        $scope.companyInformationValidity.addressLine1 = true;
                        angular.element("#addressLine1").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#addressLine1").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.addressLine1 = false;
                        angular.element("#addressLine1").closest('.form-group').removeClass('has-error').addClass('has-success');
                        angular.element("#addressLine1").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                    }
                }
            }, true);

            //validate city
            $scope.$watch('companyInfoViewModel.City', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#city").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.City) {
                        $scope.companyInformationValidity.city = true;
                        angular.element("#city").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#city").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.city = false;
                        angular.element("#city").closest('.form-group').removeClass('has-error').addClass('has-success');
                        angular.element("#city").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                    }
                }
            }, true);

            //validate province
            $scope.$watch('companyInfoViewModel.Province', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#province").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Province) {
                        $scope.companyInformationValidity.province = true;
                        angular.element("#province").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#province").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.province = false;
                        angular.element("#province").closest('.form-group').removeClass('has-error').addClass('has-success');
                        angular.element("#province").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                    }
                }
            }, true);

            //validate postal code
            $scope.$watch('companyInfoViewModel.PostalCode', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#postal").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.PostalCode) {
                        $scope.companyInformationValidity.postal = true;
                        $scope.companyInformationValidity.postal_regex = false;
                        angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.postal = false;
                        var selectedCountry = angular.element("#country").find(":selected").text();
                        if (selectedCountry == "Canada") {
                            var postal_regex_validity_ca = regex_postal_ca.test($scope.companyInfoViewModel.PostalCode.toUpperCase());
                            if (!postal_regex_validity_ca) {
                                $scope.companyInformationValidity.postal_regex = true;
                                angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                                angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                            }
                            else {
                                $scope.companyInformationValidity.postal_regex = false;
                                angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                                angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                            }
                        }
                        else if (selectedCountry == "United States") {
                            var postal_regex_validity_us = regex_postal_us.test($scope.companyInfoViewModel.PostalCode);
                            if (!postal_regex_validity_us) {
                                $scope.companyInformationValidity.postal_regex = true;
                                angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                                angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                            }
                            else {
                                $scope.companyInformationValidity.postal_regex = false;
                                angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                                angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                            }
                        }
                        else if (selectedCountry == "Please Select") {
                            $scope.companyInformationValidity.postal_regex = true;
                            angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.postal_regex = false;
                            angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }                     
                    }
                }
            }, true);

            //validate country
            $scope.notFirstLoad = false;
            $scope.$watch('companyInfoViewModel.Country', function (newVal, oldVal) {
                if (oldVal && newVal == null) {
                    $scope.notFirstLoad = true;
                }
                if ($scope.companyInfoViewModel && (oldVal != undefined || $scope.notFirstLoad)) {
                    angular.element("#country").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Country) {
                        $scope.companyInformationValidity.country = true;
                        angular.element("#country").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#country").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        if (!$scope.companyInfoViewModel.PostalCode) {
                            $scope.companyInformationValidity.postal = true;
                            $scope.companyInformationValidity.postal_regex = false;
                            angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.postal = false;
                            $scope.companyInformationValidity.postal_regex = true;
                            angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                    }
                    else {
                        $scope.companyInformationValidity.country = false;
                        angular.element("#country").closest('.form-group').removeClass('has-error').addClass('has-success');
                        angular.element("#country").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        
                            angular.element("#postal").closest('.form-group').removeClass('has-required');
                            if (!$scope.companyInfoViewModel.PostalCode) {
                                $scope.companyInformationValidity.postal = true;
                                $scope.companyInformationValidity.postal_regex = false;
                                angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                                angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                            }
                            else {
                                $scope.companyInformationValidity.postal = false;
                                var selectedCountry = angular.element("#country").find(":selected").text();
                                if (selectedCountry == "Canada") {
                                    var postal_regex_validity_ca = regex_postal_ca.test($scope.companyInfoViewModel.PostalCode.toUpperCase());
                                    if (!postal_regex_validity_ca) {
                                        $scope.companyInformationValidity.postal_regex = true;
                                        angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                                        angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                                    }
                                    else {
                                        $scope.companyInformationValidity.postal_regex = false;
                                        angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                                        angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                                    }
                                }
                                else if (selectedCountry == "United States") {
                                    var postal_regex_validity_us = regex_postal_us.test($scope.companyInfoViewModel.PostalCode);
                                    if (!postal_regex_validity_us) {
                                        $scope.companyInformationValidity.postal_regex = true;
                                        angular.element("#postal").closest('.form-group').addClass('has-error').removeClass('has-success');
                                        angular.element("#postal").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                                    }
                                    else {
                                        $scope.companyInformationValidity.postal_regex = false;
                                        angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                                        angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                                    }
                                }
                                else {
                                    $scope.companyInformationValidity.postal_regex = false;
                                    angular.element("#postal").closest('.form-group').removeClass('has-error').addClass('has-success');
                                    angular.element("#postal").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                                }
                            }                     
                    }
                }
            }, true);

            //validate phone number
            $scope.$watch('companyInfoViewModel.PhoneNumber', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#phone").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.PhoneNumber) {
                        $scope.companyInformationValidity.phone = true;
                        $scope.companyInformationValidity.phone_regex = false;
                        angular.element("#phone").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#phone").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.phone = false;
                        var phone_regex_validity = regex_phone.test($scope.companyInfoViewModel.PhoneNumber);
                        if (!phone_regex_validity) {
                            $scope.companyInformationValidity.phone_regex = true;
                            angular.element("#phone").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#phone").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.phone_regex = false;
                            angular.element("#phone").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#phone").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            //validate fax
            $scope.$watch('companyInfoViewModel.Fax', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#fax").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Fax) {
                        $scope.companyInformationValidity.fax = true;
                        $scope.companyInformationValidity.fax_regex = false;
                        angular.element("#fax").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#fax").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.fax = false;
                        var phone_regex_validity = regex_phone.test($scope.companyInfoViewModel.Fax);
                        if (!phone_regex_validity) {
                            $scope.companyInformationValidity.fax_regex = true;
                            angular.element("#fax").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#fax").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.fax_regex = false;
                            angular.element("#fax").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#fax").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            //validate email
            $scope.$watch('companyInfoViewModel.Email', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#email").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Email) {
                        $scope.companyInformationValidity.email = true;
                        $scope.companyInformationValidity.email_regex = false;
                        angular.element("#email").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#email").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.email = false;
                        var email_regex_validity = regex_email.test($scope.companyInfoViewModel.Email);
                        if (!email_regex_validity) {
                            $scope.companyInformationValidity.email_regex = true;
                            angular.element("#email").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#email").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.email_regex = false;
                            angular.element("#email").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#email").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            //validate website
            $scope.$watch('companyInfoViewModel.WebSite', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#webSite").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.WebSite) {
                        $scope.companyInformationValidity.webSite = true;
                        $scope.companyInformationValidity.webSite_regex = false;
                        angular.element("#webSite").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#webSite").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.webSite = false;
                        var webSite_regex_validity = regex_webSite.test($scope.companyInfoViewModel.WebSite);
                        if (!webSite_regex_validity) {
                            $scope.companyInformationValidity.webSite_regex = true;
                            angular.element("#webSite").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#webSite").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.webSite_regex = false;
                            angular.element("#webSite").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#webSite").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            //validate facebook
            $scope.$watch('companyInfoViewModel.Facebook', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#facebook").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Facebook) {
                        $scope.companyInformationValidity.facebook = true;
                        $scope.companyInformationValidity.facebook_regex = false;
                        angular.element("#facebook").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#facebook").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.facebook = false;
                        var facebook_regex_validity = regex_webSite.test($scope.companyInfoViewModel.Facebook);
                        if (!facebook_regex_validity) {
                            $scope.companyInformationValidity.facebook_regex = true;
                            angular.element("#facebook").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#facebook").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.facebook_regex = false;
                            angular.element("#facebook").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#facebook").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            //validate twitter
            $scope.$watch('companyInfoViewModel.Twitter', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#twitter").closest('.form-group').removeClass('has-required');
                    if (!$scope.companyInfoViewModel.Twitter) {
                        $scope.companyInformationValidity.twitter = true;
                        $scope.companyInformationValidity.twitter_regex = false;
                        angular.element("#twitter").closest('.form-group').addClass('has-error').removeClass('has-success');
                        angular.element("#twitter").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    }
                    else {
                        $scope.companyInformationValidity.twitter = false;
                        var twitter_regex_validity = regex_webSite.test($scope.companyInfoViewModel.Twitter);
                        if (!twitter_regex_validity) {
                            $scope.companyInformationValidity.twitter_regex = true;
                            angular.element("#twitter").closest('.form-group').addClass('has-error').removeClass('has-success');
                            angular.element("#twitter").next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        }
                        else {
                            $scope.companyInformationValidity.twitter_regex = false;
                            angular.element("#twitter").closest('.form-group').removeClass('has-error').addClass('has-success');
                            angular.element("#twitter").next('span').addClass('glyphicon-ok').removeClass('glyphicon-remove');
                        }
                    }
                }
            }, true);

            $scope.$watch('companyInfoViewModel.AddressLine2', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#addressLine2").closest('.form-group').addClass('has-success');                 
                }
            }, true);

            $scope.$watch('companyInfoViewModel.Ext', function (newVal, oldVal) {
                if ($scope.companyInfoViewModel && oldVal != undefined) {
                    angular.element("#ext").closest('.form-group').addClass('has-success');
                }
            }, true);
        }
    };
}]);

directives.directive('logoPreview', ['$document', function ($document) {
    return {
        restrict: 'A',
        scope: {
            imgSrc: "=",          
        },
        link: function (scope, el, attrs) {
            scope.$watch('imgSrc', function () {
                if (angular.isDefined(attrs.imgSrc) && attrs.imgSrc != "") {                    
                    angular.element(el).attr("src", scope.imgSrc);                                      
                }
            }, true);
        }
    };
}]);

directives.directive('companyLogoUploader', ['$rootScope', 'companyBrandingAppServices', function ($rootScope, companyBrandingAppServices) {
    return {
        restrict: 'E',
        templateUrl: 'documentUploader.html',
        scope: {           
            currentSupportingDocument: '=',
            isView: "="
        },
        link: function (scope, el, attrs) {           
            companyBrandingAppServices.loadCompanyLogoPath().then(function (data) {
                scope.currentSupportingDocument.url = data.data;
                //angular.element("#topSupportingDocumentZoom").css("background", "url(" + scope.currentSupportingDocument.url + ") center center");
            });
        },
        controller: function ($scope, $http) {

            $scope.loadUploadedFile = false;
            $scope.currentSupportingDocument = { url: '', name: '' };          
            $scope.invalidFileFormat = false;

            $scope.onFileSelect = function ($files) {
                //check if file is an image
                if ($files[0] && !($files[0].type.indexOf('image/png') > -1 || $files[0].type.indexOf('image/jpeg') > -1 || $files[0].type.indexOf('image/gif') > -1)) {
                    $scope.invalidFileFormat = true;
                    angular.element(".dropzone").css("border-color", "#a94442");
                    return;
                }
                else {
                    $scope.invalidFileFormat = false;
                    angular.element(".dropzone").css("border-color", "#7E8C8D");
                }

                uploadFile($files);               
            };         

            function uploadFile(files) {
                $http.uploadFile({
                    url: Global.CompanyInformation.UploadLogoFileURL,
                    file: files[0],
                    data: {}
                }).success(function (result) {
                    if (result.status) {
                        $scope.formUploadModel = result.data;
                        initializePhotos($scope.formUploadModel.Url, $scope.formUploadModel.Name);
                        $scope.loadUploadedFile = true;                        
                    }
                }).error(function (error) {
                });
            }

            function initializePhotos(url, name) {
                $scope.currentSupportingDocument.url = url;
                $scope.currentSupportingDocument.name = name;
            }
        }
    };
}]);

directives.directive('manageInternalNotes', ['$compile', '$window', '$http', '$filter', function ($compile, $window, $http, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'company-information-internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@',
            parentId: '@',
            isView: '='
        },
        link: function (scope, elem, attr, ngModel) {
            scope.noteTDWidthpixel = 0;//document.getElementById(scope.panelArrow + '-notes-width').offsetWidth;//not exists yet
            scope.elem = elem;
            scope.focus = function () {
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            scope.removeIcon = function ($event) {
                scope.searchNote = '';
                var search = $event.currentTarget.parentElement.firstElementChild;
                $(search).focus();
            }

            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                }
            })

            scope.search = function () {

                //scope.internalNoteLength = scope.filtered.length;
                scope.internalNoteLength = $filter('filter')(scope.allInternalNotes, scope.searchNote).length;
            };

            scope.add = function () {
                if (scope.textAreaModel) {
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            parentId: scope.parentId,
                            notes: scope.textAreaModel
                        })
                    }).success(function (result) {
                        //Load results after 
                        $http({
                            url: scope.loadUrl,
                            method: "GET",
                            params: {
                                parentId: scope.parentId,
                            },
                        }).success(function (result) {
                            scope.textAreaModel = '';
                            scope.allInternalNotes = result;
                            //scope.quantity = scope.allInternalNotes.length;
                            //if (scope.quantity > 4) {
                            //    scope.showMore = false;
                            //}
                        })
                    })
                }
            };

            scope.stripNotes = function (note) {

                var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one //.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                var trimWidth = 0;
                var bestFitNbr = 50;
                var shortNote = '';
                ///if trimmed-note text width is longer than current TD.offsetWidth, try to find best match string.length of noteTrimedStr, then return to UI. 
                ///since string.length affect webui-popover horizontal position
                if (noteWidth + 15 >= scope.noteTDWidthpixel) { //OTSTM2-845 make sure "..." effect is same with or without vertical scrollbar
                    while (trimWidth < scope.noteTDWidthpixel - 15) {
                        bestFitNbr += 2;
                        shortNote = noteTrimedStr.substr(0, bestFitNbr);//incrase str size and try again
                        trimWidth = getTextWidth(shortNote, "0.875em Open Sans");
                    }
                    return shortNote + "...";
                }
                else {
                    return note;
                }
            };

            scope.checkNoteSize = function (note) {
                if (note != undefined) {
                    var noteTrimedStr = note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width') ? document.getElementById(scope.panelArrow + '-notes-width').offsetWidth : 0;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    var tdAddedBy = document.getElementById('tdAddedBy');
                    if (hasScrollBar(internalNoteBody)) { //OTSTM2-845 check if the vertical scrollbar appears
                        $(tdAddedBy).css("margin-left", "-20px"); //OTSTM2-845 "AddedBy" move left for alignment if vertical scrollbar appears
                        return (scope.noteTDWidthpixel <= noteWidth + 32);
                    }
                    else {
                        return (scope.noteTDWidthpixel <= noteWidth + 6);
                    }
                }
                else {
                    return false;
                }
            };

            angular.element($window).bind('resize', function () {

                var length = scope.allInternalNotes.length;
                for (var i = 0; i < length; i++) {
                    var noteTrimedStr = scope.allInternalNotes[i].Note.replace(/['"]+/g, '').replace(/ {1,}/g, " ");//replace multiple space with one//.replace(/[@]/g, '&#64').replace(/\</g, "&lt;").replace(/\>/g, "&gt;")
                    var noteWidth = getTextWidth(noteTrimedStr, "0.875em Open Sans");
                    var trimWidth = 0;
                    var bestFitNbr = 50;
                    var shortNote = '';
                    scope.noteTDWidthpixel = 0;
                    var $div = $("<td ng-if=\"checkNoteSize(note.Note)\" width=\"75%\" style=\"cursor: pointer\" class=\"internal-note{{$index}}\"><span internal-note-hover-popover data-note=\"{{note.Note}}\">{{note.Note}}</span></td>");
                    if (scope.noteTDWidthpixel == 0) {
                        scope.noteTDWidthpixel = document.getElementById(scope.panelArrow + '-notes-width') ? document.getElementById(scope.panelArrow + '-notes-width').offsetWidth : 0;
                    }
                    var internalNoteBody = document.getElementById('internalNoteBody');
                    if (hasScrollBar(internalNoteBody)) {
                        if (scope.noteTDWidthpixel <= noteWidth + 32) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();

                            }
                        }
                    }
                    else {
                        if (scope.noteTDWidthpixel <= noteWidth + 6) {
                            var internalNoteTd = $(scope.elem).find(".internal-note" + i);
                            if ($(internalNoteTd).css("cursor") != "pointer") {
                                $(internalNoteTd).append($compile($div)(scope));
                                //$(internalNoteTd).remove();
                                scope.$apply();
                            }
                        }
                    }
                }
                scope.$digest();
            });

        },
        controller: function ($scope, $rootScope) {

            $http({
                url: $scope.loadUrl,
                method: "GET",
                params: {
                    parentId: $scope.parentId,
                },
            }).then(function (result) {
                $scope.allInternalNotes = result.data;
                $scope.sortType = 'AddedOn';
                $scope.textAreaModel = '';
                $scope.sortReverse = true;
                $scope.searchNote = '';
                $scope.quantity = 5;
                //$scope.showMore = result.data.length > 4;//true;
                $scope.showMore = true;
                $scope.export = Global.InternalNoteSettings.InternalExportUrl;
            })

            $rootScope.$on('TIRE_ORIGIN_INTERNAL_NOTE', function (e, data) { //OTSTM2-386
                $scope.allInternalNotes = data;
                $scope.quantity = $scope.allInternalNotes.length;
                //if ($scope.quantity > 4) {
                //    $scope.showMore = false;
                //}
            });

        }
    };
}]);

directives.directive('internalNotesExport', ['$http', '$document', function ($http, $document) {

    return {
        restrict: 'E',
        templateUrl: 'companyInformationAnchorExport.html',
        scope: {
            id: '@',
            searchText: '@',
            sortReverse: '@',
            sortColumn: '@',
            url: '@',
            model: '=',
            fileName: '@'
        },
        link: function (scope, elem, attr, ngModel) {
            elem.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                var req = { parentId: scope.$parent.parentId, searchText: scope.searchText, sortReverse: scope.sortReverse, sortcolumn: scope.sortColumn, model: scope.model };
                var submitVal = {
                    url: scope.url,
                    method: "GET",
                    //params: JSON.stringify(req)
                    params: req,
                }
                return $http(submitVal).then(function (result) {

                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = scope.fileName + "-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });
            });
        },
        controller: function ($scope) {

        }
    };
}]);

document.body.addEventListener('click', function (e) {
    if (Global.InternalNoteSettings == undefined) {//participant page
        return;
    }
    var insidePopover = clickInsidePopover(e.target);
    if (!insidePopover) {
        $("#" + Global.InternalNoteSettings.currentOpenId).popover('hide');//close opened popover
    }
}, false);

function clickInsidePopover(e) {
    if ((e == null) || (e == undefined)) {
        return false;
    } else if ((e.className === 'popover-content') || (e.className === 'popover-title') || (e.className.startsWith('popover fade'))) {
        return true;
    }
    else {
        return clickInsidePopover(e.parentElement);
    }
}

//OTSTM2-845 option 1 for getting text width, need to add a <canvas> tag
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    //var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var canvas = document.getElementById('myCanvas'); //OTSTM2-845 get the added <canvas>
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return Math.round(metrics.width);
}

//OTSTM2-845 option 2 for getting text width
function getTextWidthDOM(text, font) {
    var f = font || '12px arial',
        o = $('<span>' + text + '</span>')
              .css({ 'font': f, 'float': 'left', 'white-space': 'nowrap' })
              .css({ 'visibility': 'hidden' })
              .appendTo($('body')),
        w = o.width();
    o.remove();
    return w;
}
//OTSTM2-845 check if vertical scrollbar appears
function hasScrollBar(obj) {
    return $(obj).get(0).scrollHeight > $(obj).height();
}

function showValidinfo(element, msg) {
    $("div.valid-info").children("span").html(msg);
    if (msg != validMsg.noneMsg) {
        $('html, body').animate({
            scrollTop: $("#fixedClaimHeader").offset().top - 680
        }, 500);
    }
    return msg != validMsg.noneMsg;
}