﻿'use strict';

var Security = {
    isReadonly: function (resource) {
        return resource === Global.Announcement.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.Announcement.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.Announcement.Security.Constants.NoAccess
    },   
}

//Announcement App
var announcementApp = angular.module("announcementApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller", "announcement.directives", "announcement.controllers", "announcement.services", "summernoteLib"]);

//Services Section
var services = angular.module('announcement.services', []);
services.factory('announcementServices', ["$http", function ($http) {
    var factory = {};

    factory.anchorCss = function () {
        return {
            "display": "block",
            "padding": "3px 20px",
            "clear": "both",
            "font-weight": "400",
            "line-height": "1.42857143",
            "color": "#000",
            "white-space": "nowrap"
        }
    }

    factory.AddNew = function (data) {
        var submitVal = {
            url: Global.Announcement.AddNewAnnouncementUrl,
            method: "POST",
            data: { vm: data}
        }
        return $http(submitVal);
    }

    factory.Update = function (data) {
        var submitVal = {
            url: Global.Announcement.UpdateUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }

    factory.Remove = function (announcementID) {
        var submitVal = {
            url: Global.Announcement.RemoveUrl,
            method: "POST",
            data: { announcementID: announcementID}
        }
        return $http(submitVal);
    }

    factory.getAnnouncementByID = function (announcementID) {
        return $http({
            method: 'GET',
            url: Global.Announcement.LoadAnnouncementByIDUrl,
            params: {
                announcementID: announcementID
            }
        });
    }

    factory.setSummerNote = function () {
        // set summer note
        $("#announcementDescription").summernote({ height: 310 });
        $('.note-editable').attr('contenteditable', false);
        $('.note-editor > .note-toolbar button').attr("disabled", "disabled");
        $(".note-btn-group.btn-group.note-view").hide();
        var insertButtons = $('.note-btn-group.btn-group.note-insert  button');
        $.each(insertButtons, function (index, button) {
            if ($(button).data("original-title").indexOf("Link") < 0) {
                $(button).hide();
            }
        });
    }

    factory.initDateTimePicker = function () {
        var now = new Date();

        $('#announcementStartDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: "yyyy-mm-dd",
            startDate: now,
            minDate: now,       
        }).on('change.dp',function(e) {               
            var startDate = moment($('#announceEndDate').val());
            var endDate = moment($('#announceStartDate').val());
            if (!startDate.isValid()) {
                $("#vm-startDate").show();
            } else {
                $("#vm-startDate").hide();
                if (startDate.diff(endDate, "days") < 0) {
                    $("#vm-overDate").show();
                } else {
                    $("#vm-overDate").hide();
                }
            }
        });

        $('#announcementEndDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: "yyyy-mm-dd",     
            startDate: now,
            minDate: now,    
        }).on('change.dp', function (e) {
            var startDate = moment($('#announceEndDate').val());
            var endDate = moment($('#announceStartDate').val());
            if (!startDate.isValid()) {
                $("#vm-startDate").show();
            } else {
                $("#vm-startDate").hide();
                if (startDate.diff(endDate, "days") < 0) {
                    $("#vm-overDate").show();
                } else {
                    $("#vm-overDate").hide();
                }
            }
        });
    }

    return factory;
}]);

//Directives Section
var directives = angular.module('announcement.directives', []);

directives.directive('announcementListPanel', ['DTOptionsBuilder', 'DTColumnBuilder', 'announcementServices', function (DTOptionsBuilder, DTColumnBuilder, announcementServices) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'announcement-list-panel-template.html',
        scope: {
            url: '@',          
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            $scope.isReadOnly = Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly;
            $scope.dtInstance = {};
            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                $scope.dtInstance.DataTable.draw();
            });
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "announcementID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("Subject", "Subject").withOption('name', 'Subject').withClass('sorting_l')
                        .renderWith(function (data, type, full, meta) {
                        var html = '';
                        if (data != null) {
                            html = "<span style='overflow:hidden; width:90%;'>" + data + "</span><span ng-show='subjectLength()'>...</span>";
                        }
                        return html;
                    }),
                    DTColumnBuilder.newColumn("StartDate", "Effective Date").withOption('name', 'StartDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data != null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("EndDate", "End Date").withOption('name', 'EndDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data != null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                        if (data != null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn("ModifiedDate", "Date Modified").withOption('name', 'ModifiedDate').renderWith(function (data, type, full, meta) {
                        if (data != null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml),
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.url,
                type: "GET",
                error: function (xhr, error, thrown) {
                   // window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [4, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('initComplete', function (settings, result) {    
                })
                .withOption('drawCallback', function (settings, result) {
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                });

            //open detail modal
            $scope.openDetailsModal = function (data, dtInstance_id, type) {
                var rateDetailModal = $uibModal.open({
                    templateUrl: 'modal-announcement-details-template.html',
                    controller: 'editCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        announcementID: function () { return data.ID; },       
                        type: function () { return type; },             
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $scope._pageX = 0;
            $scope._pageY = 0;
            $scope._cancelMouseClick = false;

            $('table').on('mousedown', 'tr', function (e) {
                $scope._pageX = e.pageX;
                $scope._pageY = e.pageY;
            });
            $('table').on('mouseup', 'tr', function (e) {
                $scope._pageX = e.pageX - $scope._pageX;
                $scope._pageY = e.pageY - $scope._pageY;
                if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                    $scope._cancelMouseClick = true;
                }
            });

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    if ($scope._cancelMouseClick) {
                        $scope._cancelMouseClick = false;
                        return;
                    }
                    $scope.$apply(function () {
                        $scope.openDetailsModal(aData, $scope.dtInstance.id, 'View');//click on list row will open as [View] mode
                    });
                });
                return nRow;
            }
            $scope.subjectLength = function (data, type, full, meta) {
                return false;
            }
            function actionsHtml(data, type, full, meta) {
                if ($scope.isReadOnly) { return "";}
                var nowString=moment().format("YYYY-MM-DD");
                var canDelete = moment(data.EndDate.substr(0,10)).diff(moment(nowString), "day") >= 0;
                if (canDelete) {
                    var temp = $compile($templateCache.get('list-panel-action.html')
                             .replace(/announcementNumber/g, data.ID)                            
                            )($scope);
                    return temp[0].outerHTML;
                } else {
                    return "";
                }
            }
        }
    }
}]);

directives.directive('addNew', ['announcementServices', function (announcementServices) {
    return {
        restrict: 'E',
        templateUrl: 'add-new-btn.html',
        scope: {        
        },
        link: function (scope, el, attrs, formCtrl) { 
        },
        controller: function ($scope, $uibModal, $rootScope) {
            $scope.isReadOnly = Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly;
            $scope.newAnnouncement = function () {
                var addNewModal = $uibModal.open({
                    templateUrl: 'modal-announcement-details-template.html',
                    controller: 'addNewCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {                
                    }
                });               
                addNewModal.rendered.then(function ($scope,e) {                    
                    announcementServices.initDateTimePicker();        
                    $("#announcementDescription").on("summernote.change",function (e) {
                        var temp=$("#announcementDescription").summernote('code').replace(/<\/?[^>]+(>|$)/g, "");
                        if (temp.length > 0) {
                            $("#vm-description").hide();
                        } else {
                            $("#vm-description").show();
                        }
                    });
                    $("#announcementDescription").on("summernote.blur", function (e) {
                        var temp = $("#announcementDescription").summernote('code').replace(/<\/?[^>]+(>|$)/g, "");
                        if (temp.length > 0) {
                            $("#vm-description").hide();
                        } else {
                            $("#vm-description").show();
                        }
                    });
                });
            }
        }
    }
}]);

directives.directive('editBtn', ['announcementServices', function (announcementServices) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            announcementid: '@',
            type:'@',
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = announcementServices.anchorCss();
            if (Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            $scope.isReadOnly = Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly;
            $scope.editDetails = function () {
                var postModal = $uibModal.open({
                    templateUrl: 'modal-announcement-details-template.html',
                    controller: 'editCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        announcementID: function () {
                            return $scope.announcementid;
                        },                  
                        type: function () {
                            return $scope.type;
                        },                     
                    }
                });
                postModal.rendered.then(function (e) {
                    announcementServices.initDateTimePicker();
                });
            }
        }
    }
}]);

directives.directive('removeBtn', ['announcementServices', function (announcementServices) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            announcementid: '@',            
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = announcementServices.anchorCss();           
            if ( Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            $scope.isReadOnly = Global.Announcement.Security.Authorize === Global.Announcement.Security.Constants.ReadOnly;
            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        announcementID: function () {
                            return $scope.announcementid;
                        },                     
                    }
                });
            }
        }
    }
}]);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

directives.directive('tablerowpopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

directives.directive('validNumber', function () {
    return {
        require: '?ngModel',
        scope: {
        },
        link: function (scope, element, attr, ngModelCtrl) {   
            element.bind('blur', function () {
                var id = $(element).attr("name");
                if (element.val() == '') {                    
                    $("#"+id).show();
                } else {
                    $("#" + id).hide();
                }
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});

//Controllers Section
var controllers = angular.module('announcement.controllers', []);
controllers.controller('announcementController', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);

//Common Controllers
controllers.controller('successCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'successResult', 'header', 'body', function ($scope, $uibModal, $uibModalInstance, successResult, header, body) {

    $scope.sucessMessage = body;
    $scope.successHeader = header;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('failCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'message', 'header', function ($scope, $uibModal, $uibModalInstance, message, header) {
    $scope.message = message;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Add New Controller
controllers.controller('addNewCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'announcementServices', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, announcementServices, $rootScope) {
    $scope.isValid = {
        subject: true,
        startDate: true,
        endDate: true,
        endDateOverStartDate: true,
        description: true,
        hasChecked: true,
        result: function (e) {
            return this.subject && 
                   this.description &&
                   this.startDate &&
                   this.endDate &&
                   this.endDateOverStartDate &&
                   this.hasChecked
        }
    };

    $scope.isView = false;
    $scope.isEdit = false;   
    $scope.vm = {
        startDate: new Date().toISOString().substring(0, 10),
        endDate: new Date().toISOString().substring(0, 10),
        subject: "",
        description:"",
        steward: false,
        collector: false,
        hauler: false,
        processor: false,
        rpm: false,
        staff: false,
    };
    $scope.optionCheck = function () {
        var temp = $scope.vm.steward || $scope.vm.collector || $scope.vm.hauler || $scope.vm.processor || $scope.vm.rpm || $scope.vm.staff;
        if (temp) {
            $("#vm-optionCheck").hide();
        } else {
            $("#vm-optionCheck").show();
        }
        return temp;
    }
    $scope.confirm = function () {
        $scope.disableBtn = true;
        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
        $uibModalInstance.close(response.data.result);
    }
    $scope.cancel = function () {
        $scope.disableBtn = false;
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };

    // Start of Add New Rate
    $scope.save = function (e) {
        //validate effective start date
        var nowString=moment().format("YYYY-MM-DD");
        var now = moment(nowString);
        var startDT = moment($scope.vm.startDate);
        $scope.isValid.startDate = true;
        if (!startDT.isValid() || startDT.diff(now,"days")<0) {
            $scope.isValid.startDate = false;
        }

        //validate effective end date
        var endDT = moment($scope.vm.endDate);
        $scope.isValid.endDate = true;
        if (!endDT.isValid() || endDT.diff(now, "days") < 0) {
            $scope.isValid.endDate = false;
        }
        //validate over date
        if (startDT.diff(endDT, "days") > 0) {
            $scope.isValid.endDateOverStartDate = false;
            $("#overDate").show();
        } else {
            $scope.isValid.endDateOverStartDate = true;
            $("#overDate").hide();
        }

        // validate subject 
        if ($scope.vm.subject.length > 0) {
            $("#vm-subject").hide();
            $scope.isValid.subject = true;
        } else {
            $("#vm-subject").show();
            $scope.isValid.subject = false;
        };
        $scope.isValid.description = $scope.vm.description.length > 0;
        $scope.isValid.hasChecked = $scope.optionCheck(); 
        if (!$scope.isValid.result()) {
            if ($scope.isValid.description) {
                $("#vm-description").hide();
            } else {
                $("#vm-description").show();
            }
            return;
        }
        var confirmAddNewModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {               
                $scope.confirmMessage = Global.Announcement.AddNewConfirmHtml;               
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {              
            }
        });

        confirmAddNewModal.result.then(function (e) {           
            announcementServices.AddNew($scope.vm).then(function (response) {        
                if (response.data.status) {                    
                    $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    $uibModalInstance.close(true);
                } 
            });
        }, function () {
            $scope.disableSaveBtn = false;
            $scope.vm.startDate = $scope.vm.startDate.substring(0,10);
            $scope.vm.endDate = $scope.vm.endDate.substring(0, 10);
        });
    };
}]);

//Edit Controller
controllers.controller('editCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'announcementID','type',
    'announcementServices', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, announcementID,type,announcementServices, $rootScope) {
        $scope.announcementID = announcementID;
        $scope.disableSaveBtn = false;
        $scope.isValid = {
            subject: true,
            startDate: true,
            endDate: true,
            endDateOverStartDate: true,
            description: true,
            hasChecked: true,
            result: function (e) {
                return this.subject &&
                       this.description &&
                       this.startDate &&
                       this.endDate &&
                       this.endDateOverStartDate &&
                       this.hasChecked
            }
        };          

        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;

        $scope.quantity = 5;  
 
        announcementServices.getAnnouncementByID(announcementID).then(function (data) {
            $scope.vm = {
                startDate: data.data.StartDate.substring(0, 10),
                endDate: data.data.EndDate.substring(0, 10),
                subject: data.data.Subject,
                description: data.data.Description,
                steward: data.data.Steward,
                collector: data.data.Collector,
                hauler: data.data.Hauler,
                processor: data.data.Processor,
                rpm: data.data.RPM,
                staff: data.data.Staff,
                id: announcementID
            };
            $scope.oldStartDate = data.data.StartDate.substring(0, 10);
            $scope.optionCheck = function () {
                var temp = $scope.vm.steward || $scope.vm.collector || $scope.vm.hauler || $scope.vm.processor || $scope.vm.rpm || $scope.vm.staff;
                if (temp) {
                    $("#vm-optionCheck").hide();
                } else {
                    $("#vm-optionCheck").show();
                }
                return temp;
            }
            if ($scope.isView) {
                announcementServices.setSummerNote();
            }
            $("#announcementDescription").on("summernote.change", function (e) {
                if ($scope.vm.description.length > 0) {
                    $("#vm-description").hide();                  
                } else {                    
                    $("#vm-description").show();
                }
            });
            $("#announcementDescription").on("summernote.blur", function (e) {
                if ($scope.vm.description.length > 0) {
                    $("#vm-description").hide();
                } else {
                    $("#vm-description").show();
                }
            });
            var nowString = moment().format("YYYY-MM-DD");
            if (moment($scope.vm.startDate).diff(moment(nowString), "days") <= 0) {
                $('#announcementStartDate').datetimepicker('remove');
                $('#announceStartDate').prop('disabled', true);
                $('.startDTSpan').css("cursor", "not-allowed");
            }
        });

        // Start of Save 
        $scope.save = function () {
            var nowString = moment().format("YYYY-MM-DD");
            var now = moment(nowString);
            var startDT = moment($scope.vm.startDate);
            $scope.isValid.startDate = true;
            if (!startDT.isValid() || (moment($scope.oldStartDate).diff(now,"days")>=0 && startDT.diff(now, "days") < 0)) {
                $scope.isValid.startDate = false;
            }

            //validate effective end date
            var endDT = moment($scope.vm.endDate);
            $scope.isValid.endDate = true;
            if (!endDT.isValid() || endDT.diff(now, "days") < 0) {
                $scope.isValid.endDate = false;
            }
            //validate over date
            if (startDT.diff(endDT, "days") > 0) {
                $scope.isValid.endDateOverStartDate = false;
                $("#overDate").show();
            } else {
                $scope.isValid.endDateOverStartDate = true;
                $("#overDate").hide();
            }

            // validate subject 
            if ($scope.vm.subject.length > 0) {
                $("#vm-subject").hide();
                $scope.isValid.subject = true;
            } else {
                $("#vm-subject").show();
                $scope.isValid.subject = false;
            };
            $scope.isValid.description = $scope.vm.description.length > 0;
            $scope.isValid.hasChecked = $scope.optionCheck(); 
            if (!$scope.isValid.result()) {

                if ($scope.isValid.description) {
                    $("#vm-description").hide();
                } else {
                    $("#vm-description").show();
                }
                return;
            }
            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.Announcement.UpdateConfirmHtml;
                    $scope.cancel = function () {                        
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    announcementID: function () {
                        return $scope.announcementID;
                    },                  
                    type: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {              
                announcementServices.Update($scope.vm).then(function (response) {                   
         
                    if (response && response.data && response.data.status) {
                        $uibModalInstance.close();
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    } else {                      
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'fail-modal.html',
                            controller: 'failCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.Announcement.UpdateFailHeader },
                                message: function () {
                                    return response && response.data ? response.data.statusMsg : Global.Announcement.UpdateRateFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {               
                $scope.disableSaveBtn = false;             
                $scope.vm.startDate = $scope.vm.startDate.substring(0,10);
                $scope.vm.endDate = $scope.vm.endDate.substring(0, 10);              
            });
        };

        $scope.cancel = function () {            
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
        };
    }]);

//Remove Controller
controllers.controller('removeCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'announcementID', 'announcementServices', '$rootScope', function ($scope, $uibModal, $uibModalInstance, announcementID, announcementServices, $rootScope) {
    $scope.confirmMessage = Global.Announcement.RemoveConfirmHtml;   
    $scope.confirm = function () {
        announcementServices.Remove(announcementID).then(function (response) {
            if (response.data.status) {
                $rootScope.$emit('PANEL_VIEW_UPDATED', 0);
                $uibModalInstance.close(response.data.status);            
            } else {
                $uibModalInstance.dismiss('cancel');
                var failRemoveModal = $uibModal.open({
                    templateUrl: 'fail-modal.html',
                    controller: 'failCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        header: function () { return Global.Announcement.RemoveFailHeader },
                        message: function () {
                            return response && response.data ? response.data.statusMsg : Global.Announcement.RemoveFailHtml;
                        }
                    }
                });
            }
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

// private functions
function dateTimeConvert(data) {

    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

    return {
        date: d,
        time: curr_time
    }
}