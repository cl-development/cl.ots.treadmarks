﻿"use strict";

//role constants
commonUserLib.constant('roleConstant', {
    input: {
        roleName: 'role-name'
    }
})

commonUserLib.run(['$anchorScroll', function ($anchorScroll) {
    $anchorScroll.yOffset = 50;
}])

//service section
commonUserLib.factory('userService', ["$http", function ($http) {

    var factory = {};

    var isUpdateParentTable = false;

    factory.getUserNameByRoleName = function (roleName) {
        var submitVal = {
            url: Global.Settings.getUserNameByRoleNameUrl,
            method: "POST",
            data: { roleName: roleName }
        };
        return $http(submitVal);
    };

    factory.getUpdateParentRoleTable = function () {
        return isUpdateParentTable;
    }
    factory.setUpdateParentRoleTable = function (val) {
        isUpdateParentTable = val;
    }

    return factory;
}]);

//directives section
commonUserLib.directive('roleListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'userService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, userService, $http, $compile, $templateCache, $rootScope, $uibModal) {
    return {

        scope: {
            snpUrl: '@',
        },

        restrict: 'E',

        link: function (scope, elem) {
            
        },

        templateUrl: 'roleListPanel.html',

        controller: ['$scope', function ($scope) {

            $scope.ShowActionGear = !Global.Settings.Permission.DisableAddRoleBtnAndActionGear;

            $scope.dtInstance = {};

            $scope.userService = userService;
            $scope.$watch('userService.getUpdateParentRoleTable()', function (n, o) {
                if (n) {
                    $scope.dtInstance.DataTable.draw();
                    $scope.userService.setUpdateParentRoleTable(false);
                }
            })

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("RoleName", "Name").withOption('name', 'RoleName'),
                    DTColumnBuilder.newColumn("UpdateDate", "Date Modified").withOption('name', 'UpdateDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),                    
                    DTColumnBuilder.newColumn("AssociatedUsers", "Users").withOption('name', 'AssociatedUsers').renderWith(function (data, type, full, meta) {

                        if (data == 0) {
                            return '<div>' + data + '</div>';
                        }
                        else {                           
                            var html = '';
                            html = "<label>" +                            
                                    "<a href='javascript:void(0)' id='associatedUser" + meta.row + "' data-placement='auto' title='' data-text='" + full.AllAssociatedUsersText + "'>" + data + "</a>" +
                                    "</label>";                           
                            return html;
                        }
                        
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().renderWith(roleActionsHtml)
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax',
                {
                    dataSrc: "data",
                    url: $scope.snpUrl,
                    type: "POST"
                })
                .withOption('processing', false)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order',[1,'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(12)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                    //OTSTM2-843 clicking more, make sure alignment
                    if ($scope.viewMore != undefined && $scope.viewMore == false) {                       
                        $(angular.element(row)["0"].children[1]).addClass('move-right-15px');
                        $(angular.element(row)["0"].children[2]).addClass('move-right-20px');
                        $(angular.element(row)["0"].children[3]).addClass('move-right-25px');
                    }
                })
                .withOption('initComplete', function (settings, result) {                  
                    $scope.viewMore = (result.data.length >= 12 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {                    
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 12 && !$scope.scroller); //"More" disappears if search result less than 5     
                    $("[id^='associatedUser']").webuiPopover({
                        width: '500',
                        height: '300',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'User List',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            var internalNote = $(this).attr('data-text');
                            var res = internalNote.split('\n');
                            var result = "";
                            var arrayLength = res.length;
                            for (var i = 0; i < arrayLength; i++) {
                                result = result + '<p>' + res[i] + '</p>';
                            }
                            return result;
                        },
                        delay: { show: 100, hide: 100 },
                    });

                    $("[id^='actions']").popover({
                        placement: "bottom",
                        container: "body",
                        html: true,
                        template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                        content: function () {
                            return $($(this)).siblings(".popover-menu").html();
                        },
                    });
                    $scope.$apply();
                });

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250)
                                .withOption('scrollX', "100%");
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search
            $scope.search = function () {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block', 'color':'red'};
                    //$scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    //$scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $("#titleIcon").on("click", function () {
                if ($(this).find(".fa-minus-square").length != 0) {
                    $(this).find(".fa-minus-square").removeClass("fa-minus-square").addClass("fa-plus-square");
                }
                else if ($(this).find(".fa-plus-square").length != 0) {
                    $(this).find(".fa-plus-square").removeClass("fa-plus-square").addClass("fa-minus-square");
                }
            });

            function roleActionsHtml(data, type, full, meta) {
                var html = $compile($templateCache.get('role-action.html').replace(/roleName/g, data.RoleName).replace(/roleID/g, data.ID).replace(/associatedUsers/g, data.AssociatedUsers))($scope);
                return html[0].outerHTML;
            }
            
            //click to view
            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).addClass('rowClickable');
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function (ev) {
                    var el = ev.toElement;
                    $scope.$apply(function () {                     
                        $scope.openFormForView(aData.RoleName);
                    });
                });
                return nRow;
            }

            $scope.openFormForView = function (roleName) {
                $http({
                    url: '/System/User/GetPermissionsForRole',
                    method: 'POST',
                    data: {
                        roleName: roleName
                    }
                }).then(function (result) {
                    var formModal = $uibModal.open({
                        templateUrl: 'role-action-modal.html',
                        controller: 'roleFormController',
                        size: 'lg',
                        windowClass: 'role-modal',
                        backdrop: 'static',
                        resolve: {
                            data: function () {
                                return {
                                    RolePermissions: result.data.RolePermissions,
                                    RoleName: result.data.RoleName,
                                    Id: result.data.Id,
                                    IsView: true
                                }
                            }
                        }
                    });
                });
            }

            function dateTimeConvert(data) {
                if (data == null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches == null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                second = second < 10 ? '0' + second : second;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
                //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

                return {
                    date: d,
                    time: curr_time
                }
            }         
        }] 
    } 
}]); 

commonUserLib.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

//Add Button handler
commonUserLib.directive('roleAddBtn', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'role-add-btn.html',
        scope: {},
        link: function (scope, el, attrs, formCtrl) { },
        controller: ['$scope', function ($scope) {
            $scope.DisableAddRoleBtn = Global.Settings.Permission.DisableAddRoleBtnAndActionGear;
            $scope.add = function () {
                $http({
                    url: "/System/User/GetDefaultRolePermissions",
                    method: "GET"
                }).then(function (result) {
                    var formModal = $uibModal.open({
                        templateUrl: 'role-action-modal.html',
                        controller: 'roleFormController',
                        size: 'lg',
                        backdrop: 'static',
                        windowClass: 'role-modal',
                        resolve: {
                            data: function () {
                                return {
                                    RolePermissions: result.data.RolePermissions,
                                    RoleName: result.data.RoleName,
                                    Id: result.data.Id,
                                    IsEdit: false
                                }
                            }
                        }
                    });
                });
            }
        }]
    };
}]);

//Edit Button handler
commonUserLib.directive('roleEditBtn', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'role-edit-btn.html',
        scope: {
            roleName: '@'
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {

            $scope.openFormForEdit = function () {
                $http({
                    url: '/System/User/GetPermissionsForRole',
                    method: 'POST',
                    data: {
                        roleName: $scope.roleName
                    }
                }).then(function (result) {
                    var formModal = $uibModal.open({
                        templateUrl: 'role-action-modal.html',
                        controller: 'roleFormController',
                        size: 'lg',
                        windowClass: 'role-modal',
                        backdrop: 'static',
                        resolve: {
                            data: function () {
                                return {
                                    RolePermissions: result.data.RolePermissions,
                                    RoleName: result.data.RoleName,
                                    Id: result.data.Id,
                                    IsEdit: true
                                }
                            }
                        }
                    });
                });
            }
        }]
    };
}]);

//Copy Button handler
commonUserLib.directive('roleCopyBtn', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'role-copy-btn.html',
        scope: {
            roleName: '@'
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.openFormForCopy = function () {
                $http({
                    url: '/System/User/GetPermissionsForRole',
                    method: 'POST',
                    data: {
                        roleName: $scope.roleName
                    }
                }).then(function (result) {
                    var formModal = $uibModal.open({
                        templateUrl: 'role-action-modal.html',
                        controller: 'roleFormController',
                        size: 'lg',
                        windowClass: 'role-modal',
                        backdrop: 'static',
                        resolve: {
                            data: function () {
                                return {                                    
                                    RolePermissions: result.data.RolePermissions,
                                    RoleName: "Copy of "+result.data.RoleName,
                                    Id: result.data.Id,
                                    IsCopy: true
                                }
                            }
                        }
                    });
                });
            }
        }]
    };
}]);

//Delete Button handler
commonUserLib.directive('roleDeleteBtn', ['$uibModal', function ($uibModal) {
    return {
        restrict: 'E',
        templateUrl: 'role-delete-btn.html',
        scope: {
            roleId: "@",
            roleUsers:"@"
        },
        link: function (scope, el, attrs, roleFormController) {

        },
        controller: ['$scope', function ($scope) {
            //console.log($scope.roleId, $scope.roleUsers);            
            $scope.delete = function () {
                if ($scope.roleUsers > 0) {
                   $uibModal.open({
                        templateUrl: 'Confirm.html',
                        controller: 'ConfirmCtrl',                       
                        backdrop: 'static',
                        resolve: {
                            confirm: {
                                title: 'Warning',
                                body: 'Unable to delete this role due to associated users.',
                                showOk: true,
                                showSave:false,
                                showCancel:false,
                                IsAdd:false,
                                IsEdit:false
                            }
                        }
                    });
                } else {
                   $uibModal.open({
                        templateUrl: 'DeleteConfirm.html',
                        controller: 'DeleteCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            confirm: {
                                title: 'Delete Role',
                                body: 'Are you sure you want to delete this role?',
                                roleId: $scope.roleId,
                                roleUsers: $scope.roleUsers
                            }
                        }
                    });
                }
            }
        }]
    };
}]);

//controller section
commonUserLib.controller("roleListController", ["$scope", "userService", function ($scope, userService) {
    
}]);

commonUserLib.controller("roleFormController", ["$scope", "$uibModalInstance", "$http", "userService", 'data', '$uibModal', 'roleConstant', '$anchorScroll', '$location',
    function ($scope, $uibModalInstance, $http, userService, data, $uibModal, roleConstant, $anchorScroll, $location) {

        var config = {
            RoleName: data.RoleName,
            Id: data.Id,
            IsEdit: data.IsEdit,
            IsView: data.IsView,
            IsCopy: data.IsCopy
        }
        var dashboardFilter = [
            "Announcements",
            "Year to Date TSF Dollar",
            "Year to Date TSF Units",
            "TSF Status Overview",
            "Steward Payment Methods",
            "Top Ten Stewards"
        ];
        //init
        var init = (function () {
            //load unsorted array

            var allRolePermissions_unsorted = data.RolePermissions;
            //OTSTM2-1090 for dashboard menu filter only
            $.each(allRolePermissions_unsorted, function (index, item) {               
                if (dashboardFilter.indexOf(item.Panel)>=0) {
                    item.ShowCustomRadio = false;
                    item.ShowEditSaveRadio = false;
                    if (item.IsEditSave || item.IsCustom) {
                        item.IsEditSave = false;
                        item.IsCustom = false;
                        item.IsReadOnly = true;
                    }
                }
            });
            $scope.RoleName = data.RoleName;
            $scope.Id = data.Id;
            $scope.isEdit = data.IsEdit;
            $scope.isView = data.IsView;
            $scope.isCopy = data.IsCopy;
            //init sorted array
            $scope.allRolePermissions_sorted = [];
          
            var resourceLevel_0 = allRolePermissions_unsorted.filter(function (row) {
                return row.ResourceLevel === 0;
            });

            //order them by display order
            var resourceLevel_0_sorted = resourceLevel_0.sort(function (prev, curr) {
                return prev.DisplayOrder - curr.DisplayOrder;
            });

            if (resourceLevel_0_sorted.length > 0) {
                resourceLevel_0_sorted.forEach(function (item_0) {

                    //add the first 0 to the allRolePermissions_sorted
                    $scope.allRolePermissions_sorted.push(item_0);

                    //get each batch filtered by ResourceMenu for items that have resourceLevel = 0
                    var batch = allRolePermissions_unsorted.filter(function (row) {
                        return row.ResourceMenu === item_0.ResourceMenu;
                    });

                    //get all resource level 1 for batch
                    var resourceLevel_1 = batch.filter(function (row) {
                        return row.ResourceLevel === 1;
                    });

                    //order them by display order
                    var resourceLevel_1_sorted = resourceLevel_1.sort(function (prev, curr) {
                        return prev.DisplayOrder - curr.DisplayOrder;
                    });

                    //batch related                
                    resourceLevel_1_sorted.forEach(function (item_1) {
                        //add the first 1 to the allRolePermissions_sorted
                        $scope.allRolePermissions_sorted.push(item_1);

                        //filter all panel items
                        var panel_items = batch.filter(function (panelItem) {
                            return panelItem.Screen === item_1.Screen && panelItem.ResourceLevel === 2;
                        })

                        //order them by display order
                        var panel_items_sorted = panel_items.sort(function (prev, curr) {
                            return prev.DisplayOrder - curr.DisplayOrder;
                        });

                        //add the screen items to the allRolePermissions_sorted
                        $scope.allRolePermissions_sorted = $scope.allRolePermissions_sorted.concat(panel_items_sorted);
                    });

                });
            }
        })();
        
        // ==================================HELPER ========================================================
        var Helper = (function () {
            var ResourceHelper = {
                getScreens_by_resource: function (resource) {
                    return $scope.allRolePermissions_sorted.filter(function (row) {
                        return row.ResourceMenu === resource.ResourceMenu && row.ResourceLevel === 1
                    });
                },
                getPanels_by_screen: function (screen) {
                    return $scope.allRolePermissions_sorted.filter(function (row) {
                        return row.ResourceMenu === screen.ResourceMenu && row.Screen === screen.Screen && row.ResourceLevel === 2
                    });
                },
                getMenu_by_resource: function (resource) {
                    return $scope.allRolePermissions_sorted.filter(function (row) {
                        return row.ResourceMenu === resource.ResourceMenu && row.ResourceLevel === 0
                    });
                },
                updateIsCustomForParentMenu: function (resource) {
                    //1. get all screens
                    var allScreens = this.getScreens_by_resource(resource);
                    var isScreenTypeEqual = allScreens.every(function (item) {
                        return item.IsNoAccess === resource.IsNoAccess && item.IsReadOnly === resource.IsReadOnly && item.IsEditSave === resource.IsEditSave
                    })

                    if (!isScreenTypeEqual) {
                        //update parent Menu IsCustom to true if not all screen types have same value
                        $scope.allRolePermissions_sorted.map(function (item) {
                            if (item.ResourceMenu === resource.ResourceMenu && item.ResourceLevel === 0) {
                                item.IsNoAccess = false
                                item.IsReadOnly = false
                                item.IsEditSave = false
                                item.IsCustom = true
                            }
                        })
                    }
                    else {
                        //update parent Menu level if all screens have same radio btn value 
                        $scope.allRolePermissions_sorted.map(function (item) {
                            if (item.ResourceMenu === resource.ResourceMenu && item.ResourceLevel === 0) {
                                item.IsNoAccess = resource.IsNoAccess
                                item.IsReadOnly = resource.IsReadOnly
                                item.IsEditSave = resource.IsEditSave
                                item.IsCustom = resource.IsCustom
                            }
                        })
                    }
                },
                updateIsCustomForParentScreen: function (screen) {
                    //1. check if parent
                    var allPanels = this.getPanels_by_screen(screen);
                    var isPanelTypeEqual = allPanels.every(function (item) {
                        return item.IsNoAccess === screen.IsNoAccess && item.IsReadOnly === screen.IsReadOnly && item.IsEditSave === screen.IsEditSave
                    })

                    if (!isPanelTypeEqual) {
                        //update parent Screen IsCustom to true if not all panel types have same value
                        $scope.allRolePermissions_sorted.map(function (row) {
                            if (row.ResourceMenu === screen.ResourceMenu && row.Screen === screen.Screen && row.ResourceLevel === 1 && row.IsScreen) {
                                row.IsNoAccess = false
                                row.IsReadOnly = false
                                row.IsEditSave = false
                                row.IsCustom = true
                            }
                        })
                    }
                    else {
                        //update parent Screen if all panels have same radio btn value 
                        $scope.allRolePermissions_sorted.map(function (row) {
                            if (row.ResourceMenu === screen.ResourceMenu && row.Screen === screen.Screen && row.ResourceLevel === 1 && row.IsScreen) {
                                row.IsNoAccess = screen.IsNoAccess
                                row.IsReadOnly = screen.IsReadOnly
                                row.IsEditSave = screen.IsEditSave
                                row.IsCustom = screen.IsCustom
                            }
                        })
                    }
                }
            }

            var ModalHelper = {
                roleNameAlreadyExists: function () {
                    var nameAlreadyExistsModal = $uibModal.open({
                        templateUrl: 'Confirm.html',
                        controller: 'ConfirmCtrl',
                        backdrop: 'static',
                        resolve: {
                            confirm: {
                                title: 'Duplicated Role Name',
                                body: 'There is already a role with this name. Please enter a new name for this role.',
                                showOk: true,
                                showSave: false,
                                showCancel: false
                            }
                        }
                    })
                },
                EditOrSaveConfirm: function (config) {
                    //No RoleName Duplicate Exists
                    var saveConfirmModal = $uibModal.open({
                        templateUrl: 'Confirm.html',
                        controller: 'ConfirmCtrl',
                        backdrop: 'static',
                        resolve: {
                            confirm: {
                                title: config.title,
                                body: config.body,
                                showOk: false,
                                showSave: true,
                                showCopy:config.showCopy,
                                showCancel: true,
                                IsAdd: config.IsAdd,
                                IsEdit: config.IsEdit,                                
                                data: {
                                    Id: config.Id,
                                    RoleName: config.RoleName,
                                    serializedRolePermissions: JSON.stringify(config.rolePermissions)
                                }
                            }
                        }
                    })
                    saveConfirmModal.result.then(function (result) {
                        $uibModalInstance.dismiss('cancel');

                        if (result.closeParentRoleModal) {
                            //closing parent modal after successful save
                            userService.setUpdateParentRoleTable(true)
                        }
                    });
                }
            }
            return {
                ResourceHelper: ResourceHelper,
                ModalHelper: ModalHelper
            }
        })();

        //Menu click
        $scope.menuClick = function (resource) {
            //get all screens
            var allScreens = Helper.ResourceHelper.getScreens_by_resource(resource);
            allScreens.forEach(function (screen) {
            
                screen.HideRow = !screen.HideRow

                //get panels for screen
                var allPanels = Helper.ResourceHelper.getPanels_by_screen(screen);
                //if every panel is hidden dont trigger
                var isAnyPanelsHidden = allPanels.every(function (panel) {
                    return panel.HideRow
                })

                if (!isAnyPanelsHidden) {
                    allPanels.forEach(function (panel) {
                        panel.HideRow = screen.HideRow
                    })
                }
            })
        }

        //Menu icon
        $scope.menuIcon = function (resource) {
            //get all screens
            var allScreens = Helper.ResourceHelper.getScreens_by_resource(resource);
            if (allScreens.length > 0)
            {
                var isAnyScreensHidden = allScreens.every(function (screen) {
                    return screen.HideRow;
                })

                if (!isAnyScreensHidden) {
                    return 'glyphicon fa icon-minus-square glyphicon-minus-square fa-minus-square'

                }
                else {
                    return 'glyphicon fa icon-plus-square glyphicon-plus-square fa-plus-square'
                }
            }
            else {
                return 'empty-icon'
            }
        }

        //Screen click
        $scope.screenClick = function (screen) {

            var allPanels = Helper.ResourceHelper.getPanels_by_screen(screen);

            allPanels.forEach(function (panel) {
                panel.HideRow = !panel.HideRow
            })
        }
        $scope.screenIcon = function (screen) {

            var allPanels = Helper.ResourceHelper.getPanels_by_screen(screen);

            if (allPanels.length > 0)
            {
                var isAnyPanelsHidden = allPanels.every(function (panel) {
                    return panel.HideRow;
                })

                if (!isAnyPanelsHidden) {
                    return 'glyphicon fa icon-minus-square glyphicon-minus-square fa-minus-square'

                }
                else {
                    return 'glyphicon fa icon-plus-square glyphicon-plus-square fa-plus-square'
                }
            }
            else {
                return 'empty-icon'
            }
        }

        //Radio updates
        $scope.update_isNoAccess_radio = function (index) {

            $scope.allRolePermissions_sorted[index].IsNoAccess = true
            $scope.allRolePermissions_sorted[index].IsReadOnly = false
            $scope.allRolePermissions_sorted[index].IsEditSave = false
            $scope.allRolePermissions_sorted[index].IsCustom = false

            //run radio button rules
            resourceRelatedObj.runRules($scope.allRolePermissions_sorted[index])
        }
        $scope.update_isReadOnly_radio = function (index) {

            $scope.allRolePermissions_sorted[index].IsNoAccess = false
            $scope.allRolePermissions_sorted[index].IsReadOnly = true
            $scope.allRolePermissions_sorted[index].IsEditSave = false
            $scope.allRolePermissions_sorted[index].IsCustom = false

            //run radio button rules
            resourceRelatedObj.runRules($scope.allRolePermissions_sorted[index])
        }
        $scope.update_isEditSave_radio = function (index) {

            $scope.allRolePermissions_sorted[index].IsNoAccess = false
            $scope.allRolePermissions_sorted[index].IsReadOnly = false
            $scope.allRolePermissions_sorted[index].IsEditSave = true
            $scope.allRolePermissions_sorted[index].IsCustom = false

            //run radio button rules
            resourceRelatedObj.runRules($scope.allRolePermissions_sorted[index])
        }    
        $scope.radioName = function (appResource) {
            return appResource.ResourceMenu + '_' + appResource.ResourceLevel + '_' + appResource.Id + '_' + appResource.DisplayOrder
        }

        //cancel
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        //Add Role
        $scope.addRole = function (isValid) {
            var confirmConfig = {
                IsAdd: true,
                IsEdit: false,           
                title: 'Add Role',
                body: 'Are you sure you want to Add this role?',
                Id: $scope.Id,
                RoleName: $scope.RoleName,
                rolePermissions: $scope.allRolePermissions_sorted
            }

            $http({
                url: '/System/User/AddUserRole',
                method: 'POST',
                data: {
                    Id: $scope.Id,
                    RoleName: $scope.RoleName,
                    serializedRolePermissions: JSON.stringify($scope.allRolePermissions_sorted)
                }
            }).then(function (result) {
                if (result.data.isRoleNameAlreadyExists)
                {
                    Helper.ModalHelper.roleNameAlreadyExists()
                }
                else {
                    Helper.ModalHelper.EditOrSaveConfirm(confirmConfig);
                }
            })
        }
        //Edit Role
        $scope.editRole = function (isValid) {

            var confirmConfig = {
                IsEdit: true,
                IsAdd: false,           
                title: 'Save Role',
                body: 'Are you sure you want to save the changes?',
                Id: $scope.Id,
                RoleName: $scope.RoleName,
                rolePermissions: $scope.allRolePermissions_sorted
            };

            //==========if edited role changed==========
            if (config.RoleName !== $scope.RoleName) {
                $http({
                    url: '/System/User/EditUserRole',
                    method: 'POST',
                    data: {
                        Id: $scope.Id,
                        RoleName: $scope.RoleName,
                        serializedRolePermissions: JSON.stringify($scope.allRolePermissions_sorted)
                    }
                }).then(function (result) {
                    if (result.data.isRoleNameAlreadyExists) {
                        Helper.ModalHelper.roleNameAlreadyExists()
                    }
                    else {
                        Helper.ModalHelper.EditOrSaveConfirm(confirmConfig);
                    }
                })
            }
        
            //==========if edited role name hasn't changed==========
            else {
                Helper.ModalHelper.EditOrSaveConfirm(confirmConfig);
            }
        }

        //Copy Role
        $scope.copyRole = function (isValid) {

            var confirmConfig = {
                IsEdit: false,
                IsAdd: false,
                showCopy:true,
                title: 'Add Role',
                body: 'Are you sure you want to Add this role?',
                Id: $scope.Id, //should be 0.
                RoleName: $scope.RoleName,
                rolePermissions: $scope.allRolePermissions_sorted
            };

            $http({
                url: '/System/User/AddUserRole',
                method: 'POST',
                data: {
                    Id: $scope.Id,
                    RoleName: $scope.RoleName,
                    serializedRolePermissions: JSON.stringify($scope.allRolePermissions_sorted)
                }
            }).then(function (result) {
                if (result.data.isRoleNameAlreadyExists) {
                    Helper.ModalHelper.roleNameAlreadyExists()
                }
                else {
                    Helper.ModalHelper.EditOrSaveConfirm(confirmConfig);
                }
            })

        }

        $scope.submitRole = function (isValid) {
            if (isValid === false || !isValid)
            {
                var newHash = roleConstant.input.roleName
                $location.hash(newHash);
                $anchorScroll();

                return;
            }
            if ($scope.isEdit) {
                $scope.editRole(isValid);
            } else if ($scope.isCopy) {
                $scope.copyRole(isValid);
            } else {
                $scope.addRole(isValid)
            }
        }
    
        //Rules
        function ResourceRelatedRules() {
            
            this.ruleTypes = [
                {
                    resourceLevel_id: 0,
                    name: 'Menu',
                    rules: {
                        updateAllChildren: function (resource) {
                            if (resource) {
                                $scope.allRolePermissions_sorted.map(function (item) {
                                    if (item.ResourceMenu === resource.ResourceMenu && item.ResourceLevel !== 0) {
                                        item.IsNoAccess = resource.IsNoAccess
                                        item.IsReadOnly = resource.IsReadOnly
                                        item.IsEditSave = resource.IsEditSave
                                        item.IsCustom = resource.IsCustom
                                    }
                                })
                            }
                        },
                        showIsCustomRadio: function () {                            
                            $scope.allRolePermissions_sorted.map(function (menu) {
                                if (menu.ResourceLevel === 0) {
                                    var allScreens = Helper.ResourceHelper.getScreens_by_resource(menu);
                                    //OTSTM2-838 Transactions menu (row # 49) do not need a custom button. Pl. remove
                                    if (allScreens.length > 0 && menu.Menu != "Transactions") {
                                        menu.ShowCustomRadio = true;
                                    }
                                    else {
                                        menu.ShowCustomRadio = false;
                                    }
                                }
                            })
                        }
                    }
                },
                {
                    resourceLevel_id: 1,
                    name: 'Screen',
                    rules: {
                        updateAllChildren_Panels: function (resource) {
                            if (resource) {
                                $scope.allRolePermissions_sorted.map(function (panel) {
                                    if (panel.IsPanel && panel.Screen === resource.Screen && panel.ResourceLevel === 2) {
                                        panel.IsNoAccess = resource.IsNoAccess
                                        panel.IsReadOnly = resource.IsReadOnly
                                        panel.IsEditSave = resource.IsEditSave
                                        panel.IsCustom = resource.IsCustom
                                    }
                                })
                            }
                        },
                        setIsCustomForParent_Menu: function (resource) {
                            var __parent = this;
                            if (resource) {
                                //changes to screen should update parent menu
                                Helper.ResourceHelper.updateIsCustomForParentMenu(resource);
                            }
                        },
                        showIsCustomRadio: function () {
                            var __parent = this;
                            $scope.allRolePermissions_sorted.map(function (screen) {
                                if (screen.ResourceLevel === 1) {
                                    var allPanels = Helper.ResourceHelper.getPanels_by_screen(screen);
                                    if (allPanels.length > 0) {
                                        screen.ShowCustomRadio = true;
                                    }
                                    else {
                                        screen.ShowCustomRadio = false;
                                    }
                                }
                            })
                        }
                    }
                },
                {
                    resourceLevel_id: 2,
                    name: 'Panel',
                    rules: {
                        setIsCustomForParent_Screen: function (resource) {
                            var __parent = this;
                            if (resource) {
                                //changes to panels should update parent screen
                                Helper.ResourceHelper.updateIsCustomForParentScreen(resource);
                                //changes to the screens should update parent menu
                                Helper.ResourceHelper.updateIsCustomForParentMenu(resource);
                            }
                        }
                    }
                }
            ];
        }
        ResourceRelatedRules.prototype.init = function () {
            var __parent = this;
            if (this.ruleTypes) {
                this.ruleTypes.forEach(function (item) {
                    if (typeof item.rules.showIsCustomRadio == 'function') {
                        item.rules.showIsCustomRadio.call(__parent);
                    }
                });
            }
        },
        ResourceRelatedRules.prototype.getAllRules = function () {
            return this.ruleTypes;
        }
        ResourceRelatedRules.prototype.getRuleTypeById = function (id) {
            return this.ruleTypes.filter(function (rule) {
                return rule.resourceLevel_id === id;
            })

        }
        //load rules by ruletype
        ResourceRelatedRules.prototype.runRules = function (resource) {

            var currentRules = this.getRuleTypeById(resource.ResourceLevel)[0].rules;

            switch (resource.ResourceLevel) {
                //rule 0
                case 0:
                    currentRules.updateAllChildren(resource);
                    break;
                //rule 1
                case 1:
                    currentRules.updateAllChildren_Panels(resource);
                    currentRules.setIsCustomForParent_Menu.call(this, resource);
                    break;
                //rule 2
                case 2:
                    currentRules.setIsCustomForParent_Screen.call(this, resource)
                    break
            }
        }
        
        //init
        var resourceRelatedObj = new ResourceRelatedRules();
        resourceRelatedObj.init();
}]);

commonUserLib.controller('ConfirmCtrl', ['$scope', '$uibModalInstance', 'confirm', '$http', 'userService', function ($scope, $uibModalInstance, confirm, $http, userService) {
    $scope.title = confirm.title
    $scope.body = confirm.body
    $scope.showOk = confirm.showOk
    $scope.showSave = confirm.showSave
    $scope.showCopy = confirm.showCopy //copy
    $scope.showCancel = confirm.showCancel
    $scope.IsAdd = confirm.IsAdd 
   
    $scope.IsEdit = confirm.IsEdit
    
    //Add/copy
    $scope.add = function () {
        $http({
            url: '/System/User/AddUserRole',
            method: 'POST',
            data: {
                Id: confirm.data.Id,
                RoleName: confirm.data.RoleName,
                serializedRolePermissions: confirm.data.serializedRolePermissions,
                IsAddConfirmed: true
            }
        }).then(function (result) {
            //close parent role Modal
            userService.setUpdateParentRoleTable(true)
        });

        //close parent role Modal
        $uibModalInstance.close({ closeParentRoleModal: false });
    }

    //Edit
    $scope.edit = function () {
        $http({
            url: '/System/User/EditUserRole',
            method: 'POST',
            data: {
                Id: confirm.data.Id,
                RoleName: confirm.data.RoleName,
                serializedRolePermissions: confirm.data.serializedRolePermissions,
                IsEditConfirmed: true
            }
        }).then(function (result) {
            //close parent role Modal
            userService.setUpdateParentRoleTable(true)
        });
        //close parent role Modal
        $uibModalInstance.close({ closeParentRoleModal: false });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

commonUserLib.controller('DeleteCtrl', ['$scope', '$uibModalInstance', 'confirm', '$http', 'userService', function ($scope, $uibModalInstance, confirm, $http, userService) {
    $scope.title = confirm.title
    $scope.body = confirm.body

    //Delete
    $scope.delete = function () {
        $http({
            url: '/System/User/DeleteUserRole',
            method: 'POST',
            data: {
                roleID: confirm.roleId
            }
        }).then(function (result) {
            //close parent role Modal
            userService.setUpdateParentRoleTable(true)
        });
        //close parent role Modal
        $uibModalInstance.close({ closeParentRoleModal: false });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


//Role filters
commonUserLib.filter('resourceLevel', function () {
    return function (items, level) {
        var filtered = [];

        if (items) {
            filtered = items.filter(function (item) {
                return item.ResourceLevel === level
            })
        }        
        return filtered;
    }
});