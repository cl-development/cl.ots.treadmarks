﻿'use strict';
var validMsg = {
    clean: "clean",
    isRequired: "Required",
    noneMsg: ""
}

/**********************
 * Email Template App
 **********************/
var emailTemplateApp = angular.module("emailSettingsApp",
    ["commonLib", "ui.bootstrap", "emailSettingsApp.directives", "emailSettingsApp.controllers", "emailSettingsApp.services", "summernoteLib"]);

/***********************
 * Controllers Section *
 ***********************/
var controllers = angular.module('emailSettingsApp.controllers', []);
// main controllers
controllers.controller('emailSettingsGeneralController', ['$rootScope', '$scope', '$http', '$uibModal', 'emailSettingsAppServices', '$window', function ($rootScope, $scope, $http, $uibModal, emailSettingsAppServices, $window) {

    //base on security determining isReadonly or not.
    $scope.isView = Global.EmailSettingsGeneral.Security.EmailSettingsGeneral === Global.EmailSettingsGeneral.Security.Constants.ReadOnly;

    $scope.emailSettingsGeneralValidityOutput = false;

    //validation part
    $scope.emailSettingsGeneralValidity = {
        Email_BccApplicationPath: false,
        Email_BccApplicationPath_regex: false,
        Email_StewardApplicationApproveBCCEmailAddr: false,
        Email_StewardApplicationApproveBCCEmailAddr_regex: false,
        Email_defaultFrom: false,
        Email_defaultFrom_regex: false
    }

    var regex_email = new RegExp("^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$");

    $scope.submitContent = function () {
        emailSettingsGeneralValidityCheck();
        if ($scope.emailSettingsGeneralValidityOutput) {
            $scope.emailSettingsGeneralValidityOutput = false;
            //alert('Validation error. One or more fields are not valid.');
            window.scrollTo(0, 0);
            return false;
        }
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                confirmMessage: function () {
                    return Global.EmailSettingsGeneral.ConfirmHtml;
                }
            }
        });

        confirmModal.result.then(function (e) {
            var vm = $scope.emailSettingsGeneralViewModel;
            emailSettingsAppServices.updateEmailSettingsGeneralInformation(vm).then(function (response) {
                if (response.data.status) {
                    $scope.emailSettingsGeneralViewModelOriginal = response.data.data;
                }
                else {
                    emailSettingsGeneralValidityCheck();
                    console.log(response.data.statusMsg);
                    alert(response.data.statusMsg);
                }
            });
        });
    }

    $scope.cancelChange = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'cancel-modal.html',
            controller: 'cancelCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                cancelMessage: function () {
                    return Global.EmailSettingsGeneral.CancelHtml;
                }
            }
        });
        cancelModal.result.then(function (e) {
            emailSettingsAppServices.loadEmailSettingsGeneralInformation().then(function (data) {
                $scope.emailSettingsGeneralViewModel = data.data;
            });
        });
    }

    function emailSettingsGeneralValidityCheck() {

        //validate Email_BccApplicationPath
        if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailClaimAndApplication) {
            if (!$scope.emailSettingsGeneralViewModel.Email_BccApplicationPath) {
                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = true;
                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
            }
            else {
                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = false;
                var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_BccApplicationPath);
                if (!email_regex_validity) {
                    $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = true;
                }
                else {
                    $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                }
            }
        }

        //validate Email_StewardApplicationApproveBCCEmailAddr
        if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailStewardAndRemittance) {
            if (!$scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr) {
                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = true;
                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
            }
            else {
                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = false;
                var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr);
                if (!email_regex_validity) {
                    $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = true;
                }
                else {
                    $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                }
            }
        }

        //validate Email_defaultFrom
        if ($scope.emailSettingsGeneralViewModel.Email_CBDefaultFromEmailAddr) {
            if (!$scope.emailSettingsGeneralViewModel.Email_defaultFrom) {
                $scope.emailSettingsGeneralValidity.Email_defaultFrom = true;
                $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
            }
            else {
                $scope.emailSettingsGeneralValidity.Email_defaultFrom = false;
                var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_defaultFrom);
                if (!email_regex_validity) {
                    $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = true;
                }
                else {
                    $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                }
            }
        }

        $.each($scope.emailSettingsGeneralValidity, function (index, value) {
            if (value) {
                $scope.emailSettingsGeneralValidityOutput = true;
            }
        });
    }

}]);


controllers.controller('emailSettingsContentController', ['$rootScope', '$scope', '$http', '$uibModal', 'emailSettingsAppServices', function ($rootScope, $scope, $http, $uibModal, emailSettingsAppServices) {

    //base on security determining isReadonly or not.
    $scope.isView = Global.EmailSettingsContent.Security.EmailSettingsContent === Global.EmailSettingsContent.Security.Constants.ReadOnly;

    var selectedValue = $("#sel2").find("option:selected").val();
    if (selectedValue > 0) {
        emailSettingsAppServices.GetEmailByID(selectedValue).then(function (data) {
            $scope.vm = data.data;                        
            if ($scope.vm.ButtonLabel == null)
                $("#divButtonLabel").hide();
        });
        $("#emailContents").show();       
    }
    else {
        $("#emailContents").hide();
    }

    if ($scope.isView) {
        $("#snEmailEditor").summernote({ height: 305 });
        $('.note-editable').attr('contenteditable', false);
        $('.note-editor > .note-toolbar button').attr("disabled", "disabled");
        $(".note-btn-group.btn-group.note-view").hide();
        var insertButtons = $('.note-btn-group.btn-group.note-insert  button');
        $.each(insertButtons, function (index, button) {
            if ($(button).data("original-title").indexOf("Link") < 0) {
                $(button).hide();
            }
        });
    }

    $("#sel2").on("change", function () {
        //init
        var selectedValue = $(this).find("option:selected").val();
        var selectedName = $(this).find("option:selected").text();
        $("#emailDisplayName").html(selectedName);
        if (selectedValue > 0) {
            //security            
            emailSettingsAppServices.GetEmailByID(selectedValue).then(function (data) {
                $scope.vm = data.data;
                if ($scope.vm.ButtonLabel == null)
                    $("#divButtonLabel").hide();
                else 
                    $("#divButtonLabel").show();
            });
            $("#emailContents").show();
        } else {
            $("#emailContents").hide();
        }
    });

    $scope.submitContent = function (data) {
        var content = $(".note-editable").text();
        if (content.trim().length == 0) {
            showValidinfo("", validMsg.isRequired)
            return false;
        }

        
        if ($scope.vm.Subject.trim() == "" || ($("#divButtonLabel").is(":visible") && ($scope.vm.ButtonLabel == null || $scope.vm.ButtonLabel.trim() == ""))) {
            return false;
        }

        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            controller: 'confirmCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                confirmMessage: function () {
                    return "<h4>Are you sure you want to <strong>save</strong> the changes?</h4>";
                }
            }
        });

        confirmModal.result.then(function (e) { //confirm  

            emailSettingsAppServices.SaveEmailContent($scope.vm).then(function (response) {
                if (response && response.data && response.data.status) {
                    $scope.freshnote = true;
                    $('html, body').animate({
                        scrollTop: $("#fixedClaimHeader").offset().top - 680
                    }, 500);
                } else {
                    var failModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'failCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () {
                                return Global.EmailSettingsContent.FailHeader;
                            },
                            failResult: function () {
                                return response.data.status ? Global.EmailSettingsContent.FailHtml.replace('[Message]', $scope.vm.DisplayName) : response.data.statusMsg;
                            }
                        }
                    });
                }
            });
        });
    }

    $scope.cancelChange = function () {
        var cancelModal = $uibModal.open({
            templateUrl: 'cancel-modal.html',
            controller: 'cancelCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                cancelMessage: function () {
                    return 'Cancel changes?';
                }
            }
        });

        cancelModal.result.then(function (e) { //cancel

            var selectedValue = $scope.vm.ID;
            if (selectedValue > 0) {
                emailSettingsAppServices.GetEmailByID(selectedValue).then(function (data) {
                    $scope.vm = data.data;
                });
            }
        });
    }

    //$scope.cancel = function () {
    //    var selectedValue = $("#emailDisplayNameList").find("option:selected").val();
    //    if (selectedValue > 0) {
    //        emailSettingsAppServices.GetEmailByID(selectedValue).then(function (data) {
    //            $scope.vm = data.data;
    //        });
    //    }
    //}

    $scope.preview = function () {
        var selectedValue = $("#sel2").find("option:selected").val();
        if (selectedValue > 0) {
            emailSettingsAppServices.GetPreviewEmailByID(selectedValue).then(function (data) {
                //var newWindow = window.open();
                //newWindow.document.write(data.data);   
                var content = data.data;
                $("#previewEmail").html(content.replace("[*Body*]", $scope.vm.Body).replace("[*EmailTitle*]", $scope.vm.Subject).replace("@ButtonLabel", $scope.vm.ButtonLabel));
                $("#myModal").modal('show');
            });
        }
    }
}]);

//common Controllers
controllers.controller('successCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'emailSettingsAppServices', 'successResult', 'header', function ($scope, $uibModal, $uibModalInstance, emailSettingsAppServices, successResult, header) {
    $scope.sucessMessage = successResult;
    $scope.successHeader = header;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.$root.freshnote = true;
    };
}]);
controllers.controller('failCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', function ($scope, $uibModal, $uibModalInstance, failResult, header) {
    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('confirmCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'confirmMessage', function ($scope, $uibModal, $uibModalInstance, confirmMessage) {
    $scope.confirmMessage = confirmMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
    };
}]);

controllers.controller('cancelCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'cancelMessage', '$window', function ($scope, $uibModal, $uibModalInstance, cancelMessage, $window) {
    $scope.cancelMessage = cancelMessage;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
        //$window.location.reload();        
    };
}]);

controllers.controller('previewCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'previewData', '$window', function ($scope, $uibModal, $uibModalInstance, previewData, $window) {
    $scope.previewData = previewData;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.confirm = function () {
        $uibModalInstance.close();
        //$window.location.reload();        
    };
}]);


/********************
 * Services Section
 ********************/
var services = angular.module('emailSettingsApp.services', []);
services.factory('emailSettingsAppServices', ["$http", function ($http) {
    var factory = {};

    factory.SaveEmailContent = function (data) {
        var submitVal = {
            url: Global.EmailSettingsContent.SaveEmailContentUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }

    factory.GetEmailByID = function (emailID) {
        return $http({
            method: 'GET',
            url: Global.EmailSettingsContent.GetEmailByIDUrl,
            params: {
                emailID: emailID,
            }
        });
    }
    factory.loadEmailSettingsGeneralInformation = function () {
        return $http({
            method: 'GET',
            url: Global.EmailSettingsGeneral.LoadEmailSettingsGeneralInformationUrl,
            params: {}
        });
    }

    factory.updateEmailSettingsGeneralInformation = function (vm) {
        var submitVal = {
            url: Global.EmailSettingsGeneral.UpdateEmailSettingsGeneralInformationUrl,
            method: "POST",
            data: { emailSettingsGeneralVM: vm }
        }
        return $http(submitVal);
    }

    factory.GetPreviewEmailByID = function (emailID) {
        return $http({
            method: 'GET',
            url: Global.EmailSettingsContent.GetPreviewEmailByIDUrl,
            params: {
                emailID: emailID,
            }
        });
    }


    return factory;
}]);

/*********************
 * Directives Section
 *********************/
var directives = angular.module('emailSettingsApp.directives', []);

directives.directive('emailSettingsGeneral', ['$rootScope', 'emailSettingsAppServices', function ($rootScope, emailSettingsAppServices) {
    return {
        restrict: 'E',
        templateUrl: 'email-settings-general.html',
        scope: {
            emailSettingsGeneralViewModel: "=",
            emailSettingsGeneralValidity: "=",
            emailSettingsGeneralViewModelOriginal: "=",
            isView: "="
        },

        link: function (scope, el, attrs) {
            emailSettingsAppServices.loadEmailSettingsGeneralInformation().then(function (data) {
                scope.emailSettingsGeneralViewModel = data.data;
                scope.emailSettingsGeneralViewModelOriginal = angular.copy(scope.emailSettingsGeneralViewModel);
            });
        },
        controller: function ($scope) {           

            //validation part
            $scope.emailSettingsGeneralValidity = {
                Email_BccApplicationPath: false,
                Email_BccApplicationPath_regex: false,
                Email_StewardApplicationApproveBCCEmailAddr: false,
                Email_StewardApplicationApproveBCCEmailAddr_regex: false,
                Email_defaultFrom: false,
                Email_defaultFrom_regex: false
            }

            var regex_email = new RegExp("^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$");

            $scope.$watch('emailSettingsGeneralViewModel.Email_BccApplicationPath', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailClaimAndApplication) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_BccApplicationPath) {
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = true;
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_BccApplicationPath);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                            }
                        }
                    }
                }
            }, true);

            $scope.$watch('emailSettingsGeneralViewModel.Email_CBCCEmailClaimAndApplication', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && $scope.emailSettingsGeneralViewModelOriginal && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailClaimAndApplication) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_BccApplicationPath) {
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = true;
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_BccApplicationPath);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                            }
                        }
                    }
                    else {
                        $scope.emailSettingsGeneralValidity.Email_BccApplicationPath = false;
                        $scope.emailSettingsGeneralValidity.Email_BccApplicationPath_regex = false;
                        $scope.emailSettingsGeneralViewModel.Email_BccApplicationPath = $scope.emailSettingsGeneralViewModelOriginal.Email_BccApplicationPath;
                    }
                }
            }, true);

            $scope.$watch('emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailStewardAndRemittance) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr) {
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = true;
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                            }
                        }
                    }
                }
            }, true);

            $scope.$watch('emailSettingsGeneralViewModel.Email_CBCCEmailStewardAndRemittance', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && $scope.emailSettingsGeneralViewModelOriginal && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBCCEmailStewardAndRemittance) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr) {
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = true;
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                            }
                        }
                    }
                    else {
                        $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr = false;
                        $scope.emailSettingsGeneralValidity.Email_StewardApplicationApproveBCCEmailAddr_regex = false;
                        $scope.emailSettingsGeneralViewModel.Email_StewardApplicationApproveBCCEmailAddr = $scope.emailSettingsGeneralViewModelOriginal.Email_StewardApplicationApproveBCCEmailAddr;
                    }
                }
            }, true);

            $scope.$watch('emailSettingsGeneralViewModel.Email_defaultFrom', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBDefaultFromEmailAddr) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_defaultFrom) {
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom = true;
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_defaultFrom);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                            }
                        }
                    }
                }
            }, true);

            $scope.$watch('emailSettingsGeneralViewModel.Email_CBDefaultFromEmailAddr', function (newVal, oldVal) {
                if ($scope.emailSettingsGeneralViewModel && $scope.emailSettingsGeneralViewModelOriginal && oldVal != newVal) {
                    if ($scope.emailSettingsGeneralViewModel.Email_CBDefaultFromEmailAddr) {
                        if (!$scope.emailSettingsGeneralViewModel.Email_defaultFrom) {
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom = true;
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                        }
                        else {
                            $scope.emailSettingsGeneralValidity.Email_defaultFrom = false;
                            var email_regex_validity = regex_email.test($scope.emailSettingsGeneralViewModel.Email_defaultFrom);
                            if (!email_regex_validity) {
                                $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = true;
                            }
                            else {
                                $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                            }
                        }
                    }
                    else {
                        $scope.emailSettingsGeneralValidity.Email_defaultFrom = false;
                        $scope.emailSettingsGeneralValidity.Email_defaultFrom_regex = false;
                        $scope.emailSettingsGeneralViewModel.Email_defaultFrom = $scope.emailSettingsGeneralViewModelOriginal.Email_defaultFrom;
                    }
                }
            }, true);
        }
    };
}]);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);

function showValidinfo(element, msg) {
    $("div.valid-info").children("span").html(msg);
    if (msg != validMsg.noneMsg) {
        $('html, body').animate({
            scrollTop: $("#fixedClaimHeader").offset().top - 680
        }, 500);
    }
    return msg != validMsg.noneMsg;
}
