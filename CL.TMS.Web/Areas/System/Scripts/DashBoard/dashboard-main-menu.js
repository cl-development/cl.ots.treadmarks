﻿"use strict";

/*************** App ********
*Dashboard APP (inject with metricApp, announcementApp and stcApp should be injected later)
******************************/
var dashboardApp = angular.module('dashboardMainMenu', ['ui.bootstrap', 'commonLib', "datatables", "datatables.scroller", "dashboard.services", "dashboard.directives", "dashboard.controllers", 'metricApp']);

//*********service section *******************
var services = angular.module("dashboard.services", []);
services.factory("dashboardService", ["$http", function ($http) {
    var factory = {};

    factory.loadListByID = function (data) {
        var submitVal = {
            url: Global.Dashboard.loadListUrl,
            method: "POST",
            data: { paramModel: data }
        }
        return $http(submitVal);
    }
    factory.loadAnnouncementList = function (url) {
        var submitVal = {
            url: url,
            method: "GET",
            params: {}
        }
        return $http(submitVal);
    }
    factory.loadTCRServiceThresholdList = function () {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.LoadTCRServiceThresholdListUrl,
            method: "GET",
            data: {}
        }
        return $http(submitVal);
    }
    factory.addTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.AddNewTCRServiceThresholdUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.removeTCR = function (id) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.RemoveTCRServiceThresholdUrl,
            method: "POST",
            data: { id: id }
        }
        return $http(submitVal);
    }
    factory.updateTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.UpdateTCRServiceThresholdUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.exportTCR = function (sortColumnName,direction) {
        var submitVal = {
            url: Global.DashBoard.ExportToExcelForTCR,
            method: "POST",
            data: { column: sortColumnName, direction: direction }
        }
        return $http(submitVal);
    }
    // constants
    factory.Security = {
        isReadonly: function (resource) {
            return resource === Global.Dashboard.Security.Constants.ReadOnly
        },
        isEditSave: function (resource) {
            return resource === Global.Dashboard.Security.Constants.EditSave
        },
        isNoAccess: function (resource) {
            return resource === Global.Dashboard.Security.Constants.NoAccess
        },
    };

    return factory;
}]);


//*****************************************************************
//                 directives section_Start
//*****************************************************************
var directives = angular.module("dashboard.directives", []);
directives.directive("dashboardAnnouncementListPanel",
    ["$window", "$timeout", "dashboardService", "$http", "$compile", "$templateCache", "$rootScope", "$sce", "$uibModal",
    function ($window, $timeout, dashboardService, $http, $compile, $templateCache, $rootScope, $sce, $uibModal) {
        return {
            scope: {
                url: "@",
            },
            restrict: "E",
            templateUrl: "dashboard-announcement-list-panel.html",
            link: function (scope, elem, attr, ctrl) {
            },
            controller: ["$scope", "$sce", function ($scope, $sce) {
                $scope.vm = {};
                $scope.isView = false;
                $rootScope.$on("PANEL_VIEW_UPDATED", function (e, tableId) {
                    window.location.reload(true);
                });

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;
                    $scope.dtOptions.withScroller()
                                    .withOption("deferRender", true)
                                    .withOption("scrollY", 220)
                                    .withOption("scrollX", "100%");
                }
                dashboardService.loadAnnouncementList($scope.url).then(function (data) {
                    if (data && data.data) {
                        $("#announcementList").show();
                        $scope.vm = $.each(data.data.result, function (index, item) {
                            var trimedStr = item.Description.replace(/['"]+/g, '').replace(/ {1,}/g, " ");
                            item.isOver1000 = false;
                            if (trimedStr.length > 1000) {
                                item.shortDesc = item.Description.substr(0, 1000);
                                item.isOver1000 = true;
                            } else {
                                item.shortDesc = item.Description;
                            }
                        });
                    } else {
                        $("#announcementList").hide();
                    }
                });
                $scope.more = function (subject, description) {
                    var rateDetailModal = $uibModal.open({
                        templateUrl: 'modal-show-full-announcement.html',
                        controller: 'showFullAnnouncementCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            subject: function () { return subject; },
                            description: function () { return description; },
                        }
                    });
                }
            }]
        }
    }]);

//***************TCR Service Threshold start******************
directives.directive('tcrServiceThresholdListPanel', ['$window','DTOptionsBuilder', 'DTColumnBuilder', 'dashboardService', 
    function ($window,DTOptionsBuilder,DTColumnBuilder, dashboardService) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {
                angular.element($window).bind('resize', function () {
                    scope.dtInstance.DataTable.draw();
                    $(".dataTables_scrollHead table").css("width", "100%");                   
                    $(".dataTables_scrollHead").css("height", "70px");
                    $(".dataTables_scrollHead table>thead>tr>th").css("border-bottom", 0);
                    $(".dataTables_scrollHead").css("margin-top", "-20px");
                    scope.$digest();                    
                });      
            },
            templateUrl: 'tcr-service-threshold-list-panel.html',
            controller:function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
                $scope.ShowActionGear = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.EditSave;
                $scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
                $scope.dtInstance = {};
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "ID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("RegNumber", "Reg#").withOption('name', 'Reg#').withClass('text-left').withOption("width","80px"), 
                    DTColumnBuilder.newColumn("BusinessName", "Company").withOption('name', 'BusinessName').withClass('text-left'),
                    DTColumnBuilder.newColumn("LastServicedDate", "Last Serviced Date").withOption('name', 'LastServicedDate').withClass("text-center")
                        .renderWith(function (data, type, full, meta) {
                        return  data != null ? data.substring(0, data.indexOf("T")): "";                      
                    }),
                    DTColumnBuilder.newColumn("DaysSinceLastServiced", "Days since Last Service").withOption('name', 'DaysSinceLastServiced').withClass("text-center"),
                    DTColumnBuilder.newColumn("ThresholdDays", "Threshold Days").withOption('name', 'ThresholdDays').withClass("text-center"),
                    DTColumnBuilder.newColumn(null).withTitle("Days Over").withOption('name', "DaysOver").withClass("text-center")
                        .renderWith(function (data, type, full, meta) {
                        return data.DaysLeft == 0 && data.DaysSinceLastServiced >0 ? (data.DaysSinceLastServiced - data.ThresholdDays) : "";
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withClass('text-center').withOption("width","110px")
                                        .renderWith(actionsHtml),
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.url,
                    type: "GET",
                    error: function (xhr, error, thrown) {
                        window.location.reload(true);
                    }
                }).withOption('processing', true)
                    .withOption('serverSide', true) 
                    .withOption('aaSorting', [1, 'desc'])
                    .withOption('responsive', true)                   
                    .withOption('autoWidth', true)                    
                    .withDOM('tr')
                    .withScroller()
                    .withOption('deferRender', true)
                    .withOption('scrollY', 240) 
                    .withOption('lengthChange', true)  
                    .withOption('scrollCollpase',true)  
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('initComplete', function (settings, result) {           
                        resetTableStyle();    
                    })
                    .withOption('drawCallback', function (settings, result) {          
                        resetTableStyle();                        
                        var direction = settings.aaSorting[0][1];                      
                        var sortColumnName = settings.aoColumns[settings.aLastSort[0].col].sName;
                        var url = Global.DashBoard.ExportToExcelForTCR.replace('AAA', sortColumnName).replace('BBB', direction);
                        $("#tcrExport").attr("href", url);
                    });          

                dashboardService.loadTCRServiceThresholdList().then(function (result) {
                    if (result.data) {
                        $scope.vm = result.data;                       
                    }
                });

                $rootScope.$on('PANEL_VIEW_UPDATED_TCR', function (e) {
                    $scope.dtInstance.DataTable.draw();
                    resetTableStyle();
                    dashboardService.loadTCRServiceThresholdList().then(function (result) {
                        if (result.data) {
                            $scope.vm = result.data;                            
                        }
                    });
                });

                $scope.openViewModal = function (aData) {
                    var viewModal = $uibModal.open({
                        templateUrl: 'modal-tcr-detail-template.html',
                        controller: 'editTcrController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            adata: function () {
                                return aData;
                            },
                            isView: function () {
                                return true;
                            }
                        }
                    });
                }
                $scope.filterThresholds = function (isFilter) {
                    if (isFilter) {
                        $scope.dtInstance.DataTable.search(isFilter).draw(false);
                    } else {                        
                        $scope.dtInstance.DataTable.search(isFilter).draw(false);
                    }
                }
                function resetTableStyle() { 
                    $(".dataTables_scrollHead table").css("width", "100%");
                    $(".dataTables_scrollHead").css("height", "70px");
                    $(".dataTables_scrollHead").css("margin-top", "-20px");
                    $(".dataTables_scrollHead table>thead>tr>th").css("border-bottom", 0);
                };
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function () {
                        if ($scope._cancelMouseClick) {
                            $scope._cancelMouseClick = false;
                            return;
                        }
                        $scope.$apply(function () {
                            $scope.openViewModal(aData);
                        });
                    });
                    return nRow;
                }
                function actionsHtml(data, type, full, meta) {
                    if ($scope.ShowActionGear){
                        var temp = $compile($templateCache.get('tcr-list-panel-action.html')
                            .replace(/ID/g, data.ID)
                            .replace(/regNumber/g, data.RegNumber)
                            .replace(/thresholdDays/g, data.ThresholdDays)
                            .replace(/businessName/g, data.BusinessName)
                            .replace(/description/g, data.Description)
                            )($scope);
                        return temp[0].outerHTML;
                    } else {
                        return "";
                    }
                }
         
            }
        }
    }]);
directives.directive('addTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName: '@',
            adata: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.openModal = function () {
                var addModal = $uibModal.open({
                    templateUrl: 'modal-tcr-detail-template.html',
                    controller: 'addTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        panelName: function () {
                            return $scope.panelName;
                        },
                        adata: function () {
                            return $scope.adata;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            };
        }]
    };
}]);
directives.directive('editTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            //adata: '@',
            tcrId:'@',
            regNumber: '@',
            thresholdDays: '@',
            businessName: '@',
            descript: '@'
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                var editModal = $uibModal.open({
                    templateUrl: 'modal-tcr-detail-template.html',
                    controller: 'editTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        adata: function () {
                            var adata=
                            {   
                                ID:parseInt($scope.tcrId),
                                RegNumber:$scope.regNumber,
                                ThresholdDays:parseInt($scope.thresholdDays),
                                BusinessName:$scope.businessName,
                                Description:$scope.descript                                    
                            };
                            return adata;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    }
}]);
directives.directive('removeTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            itemNumber: '@',
            businessName: '@',
            itemId: '@',
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        regNumber: function () {
                            return $scope.itemNumber;
                        },
                        businessName: function () {
                            return $scope.businessName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    };
}]);
//***************TCR end******************


//*****************************************************************
//                 directives section_End
//*****************************************************************


//*****************************************************************
//                 controllers section_Start
//
//*****************************************************************
var controllers = angular.module("dashboard.controllers", []);
dashboardApp.controller('dashboardControl', ['$rootScope', '$scope', '$http', '$uibModal', '$log', function ($rootScope, $scope, $http, $uibModal, $log) {
}]);

//*****************TCR controlls******************
controllers.controller('addTcrController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'panelName', 'adata', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, panelName, adata, isView) {
    $scope.isValid = {      
        regNumberAvailable: true,    
        thresholdDaysInvalid: true,
        description: true,
        checkValid: function () {
            return this.regNumberAvailable && this.thresholdDaysInvalid && this.description;
        }
    };
    $scope.vm = {
        RegNumber: null,
        BusinessName: "",
        ThresholdDays: 0,
        Description: null,
        VendorID: 0,
        vendors: JSON.parse(adata)
    }
    $scope.isView = isView;  
    $scope.isNew = true;
   
    $scope.thresholdDays = function () {
        $scope.isValid.thresholdDaysInvalid = true;
        $scope.thresholdDaysMsg = "";
        if ($scope.vm.ThresholdDays == null || $scope.vm.ThresholdDays ==undefined || $scope.vm.ThresholdDays === "") {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Required";
        } else if (!isNaN($scope.vm.ThresholdDays)) {
            if ($scope.vm.ThresholdDays > 0 && $scope.vm.ThresholdDays < 366) {
                $scope.isValid.thresholdDaysInvalid = true;
            } else {
                $scope.isValid.thresholdDaysInvalid = false;
                $scope.thresholdDaysMsg = "Invalid input.";
            }
        } else {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Invalid input.";
        }
    }
    $scope.internalNote = function () {
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description !== "";
    }

    $scope.regNumberAvailable = function (e) {
        $scope.isValid.regNumberAvailable = true;
        $scope.regNumberMsg = "";
        $scope.vm.BusinessName = "";
        if ($scope.vm.RegNumber == null || $scope.vm.RegNumber == undefined || $scope.vm.RegNumber == "") {
            $scope.isValid.regNumberAvailable = false;  
            $scope.regNumberMsg = "Required";           
        } else if (isNaN($scope.vm.RegNumber)) {
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "Invalid Input.";           
        } else if ($scope.vm.RegNumber.charAt(0) !="2") {
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "The Registration Number should be at least seven digits starting with 2";
        } else if ($scope.vm.RegNumber.length<7){
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "The Registration Number should be at least seven digits starting with 2";
        } else {
            $scope.isNew = true;
            var temp = $scope.vm.vendors.find(function (item) {
                return item.RegNumber == $scope.vm.RegNumber;
            });
            if (temp) {
                if (temp.IsActive) {
                    $scope.vm.BusinessName = temp.BusinessName;
                    $scope.vm.VendorID = temp.VendorID;     
                    if (temp.ID > 0) {
                        $scope.vm.ID = temp.ID;
                        $scope.vm.ThresholdDays = temp.ThresholdDays;
                        $scope.isNew = false;
                    }
                } else {
                    $scope.isValid.regNumberAvailable = false;
                    $scope.regNumberMsg = "Collector is inactive.";                    
                }
            } else {
                $scope.isValid.regNumberAvailable = false;
                $scope.regNumberMsg = "This Collector Reg# does not exist.";               
            }
        }
    }

    $scope.saveTCR = function () {
        $scope.thresholdDays();
        $scope.regNumberAvailable();
        $scope.internalNote();
        if (!$scope.isValid.checkValid()) {
            return;
        }


        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmTCRServiceThresholdHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm  
            delete $scope.vm.vendors;
            dashboardService.addTCR($scope.vm).then(function (response) {
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventEditFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });
                }
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('editTcrController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'adata', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, adata, isView) {
    $scope.isValid = {  
        thresholdDaysInvalid: true,
        description: true,
        checkValid: function () {
            return this.thresholdDaysInvalid && this.description;
        }
    };
    $scope.vm = adata;
    $scope.isView = isView;
    $scope.isNew = false;  
    $scope.thresholdDaysMsg = "";
    $scope.thresholdDays = function () {
        $scope.isValid.thresholdDaysInvalid = true;
        if ($scope.vm.ThresholdDays == null || $scope.vm.ThresholdDays === "") {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Required";
        } else if (!isNaN($scope.vm.ThresholdDays)) {
            if ($scope.vm.ThresholdDays > 0 && $scope.vm.ThresholdDays < 366) {
                $scope.isValid.thresholdDaysInvalid = true;
            } else {
                $scope.isValid.thresholdDaysInvalid = false;
                $scope.thresholdDaysMsg = "Invalid input.";
            }
        } else {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Invalid input.";
        }
    }
    $scope.internalNote = function () {
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description !== "";
    }
    $scope.saveTCR = function () {
        $scope.thresholdDays();
        $scope.internalNote();
        if (!$scope.isValid.checkValid()) {
            return;
        }

        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmTCRServiceThresholdHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm  
            dashboardService.updateTCR($scope.vm).then(function (response) {
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventEditFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });
                }
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('removeTcrController', ['$scope', '$rootScope', '$uibModal', '$uibModalInstance', '$http', 'regNumber', 'businessName', 'id', 'isView', 'dashboardService', function ($scope, $rootScope, $uibModal, $uibModalInstance, $http, regNumber, businessName, id, isView, dashboardService) {
    $scope.vm = {
        id: id,
        regNumber: regNumber,
        businessName: businessName
    };
    $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
    $scope.confirmMessage = Global.DashBoard.ConfirmRemoveTCRHtml.replace("RegNumber", $scope.vm.regNumber).replace("BusinessName", $scope.vm.businessName);

    $scope.confirm = function () {
        dashboardService.removeTCR($scope.vm.id).then(function (response) {
            if (response.data.status) {
                $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                $uibModalInstance.close(true);
            }
            else {
                $uibModalInstance.dismiss('cancel');
                var failRemoveModal = $uibModal.open({
                    templateUrl: 'fail-modal.html',
                    controller: 'stcEventFailCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        header: function () { return Global.DashBoard.WarningHeader; },
                        failResult: function () {
                            return Global.DashBoard.TcrRemoveFailHtml.replace("RegNumber", $scope.vm.regNumber);
                        }
                    }
                });
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


dashboardApp.controller('showFullAnnouncementCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'subject', 'description',
function ($rootScope, $scope, $http, $uibModalInstance, $window, subject, description) {
    $scope.subject = subject;
    $scope.description = description;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


//*****************************************************************
//                 controllers section_End
//*****************************************************************
