﻿var qrCodeGrid;
$(document).ready(function () {
    _PageSize = 14;
    qrCodeGrid = $('#qrCodeGrid').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: "GetQRCodeList",
        deferRender: true,
        dom: "rtiS",
        scrollY: 260,
        "sScrollX": "100%", //OTSTM2-843
        "sScrollXInner": "110%", //OTSTM2-843
        scrollCollapse: true, //OTSTM2-843
        searching: true,
        ordering: true,
        autoWidth: false,
        order: [[0, "desc"]],
        columns: [
             {
                 className: "left-side",
                 width: "30%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return QRCodeNumberColumnRender(data.QRCodeNumber_l, data.IsActivated_l);
                 }
             },
             {
                 className: "td-center left-side",
                 width: "15%",
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return QRCodeAction(data.ID_l, data.QRCodeNumber_l, data.IsActivated_l, data.BusinessName_l, data.BusinessPrimaryAddress_l, data.BusinessNumber_l, data.UID_l);
                 }
             },
             {
                 className: "no-border",
                 width: "10%",
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return RenderSpace();
                 }
             },
             {
                 className: "right-side",
                 width: "30%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return QRCodeNumberColumnRender(data.QRCodeNumber_r, data.IsActivated_r);
                 }
             },
             {
                 className: "td-center right-side",
                 width: "15%",
                 data: null,
                 orderable: false,
                 render: function (data, type, full, meta) {
                     return QRCodeAction(data.ID_r, data.QRCodeNumber_r, data.IsActivated_r, data.BusinessName_r, data.BusinessPrimaryAddress_r, data.BusinessNumber_r, data.UID_r);
                 }
             },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            (settings.fnRecordsTotal() >= _PageSize) ? $('#QRCodeViewMore').css('visibility', 'visible') : $('#QRCodeViewMore').css('visibility', 'hidden');
        },
        scroller: {
            displayBuffer: 100,
            rowHeight: 20,
            serverWait: 100,
        },

    });

    if (Security.Ipad_QrCode.Resource.QrCode === Security.Ipad_QrCode.Constants.ReadOnly) { //OTSTM2-155
        $("#amountOfQRCodeToAddBtn").addClass("disabled");
        $("#amountOfQRCodeToAddText").addClass("disabled").prop("disabled", true);;
    }
    if (Security.Ipad_QrCode.Resource.Ipad === Security.Ipad_QrCode.Constants.ReadOnly) {
        $("#amountOfIPadToAddBtn").addClass("disabled");
        $("#amountOfIPadsToAddText").addClass("disabled").prop("disabled", true);;
    }
    if ($('#registrationNumber').val()) {//if already account search, by pass reg # modal dialog, load confirm dialog.
        $("#amountOfQRCodeToAddBtn").attr('data-target', 'modalAddQRCodesToRegConfirm');
    }
});

function QRCodeNumberColumnRender(qrCodeNumber, activated) {
    var span = ""
    if (qrCodeNumber != null) {
        if (activated) {
            span = "<span>" + qrCodeNumber + "</span>";
        }
        else {
            span = "<span style=\"color:#b1b1b1\">" + qrCodeNumber + "</span>";
        }
    }

    return span;
}

function QRCodeAction(qrCodeID, qrCodeNumber, isActivated, businessName, businessAddress, regNumber, userID) {

    var content = "";
    //fix for safari
    var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex=\"" + qrCodeID + "\"" : "";
    var disableClick = (Security.Ipad_QrCode.Resource.QrCode === Security.Ipad_QrCode.Constants.ReadOnly) ? "disable-click" : "";//OTSTM2-155

    if (isActivated) {
        content = "<div class=\"btn-group dropdown-menu-actions\">" +
        "<a id=\"QRCodeActions" + qrCodeID + "\"  data-toggle=\"popover\"" + popoverFix + " data-trigger=\"focus\"  href=\"#\">" +
            "<i class=\"glyphicon glyphicon-cog\"></i>" +
        "</a>" +
        "<div class=\"popover-menu\"><ul>" +
            "<li><a id=\"PrintQRCodeSign" + qrCodeID + "\" href=\"/default/QRCodeSignsGen?businessName=" + businessName + "&regNumber=" + regNumber + "&userID=" + userID + "&address=" + businessAddress + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Print Sign</a></li>" +
            "<li><a id=\"PrintQRCodeBadge" + qrCodeID + "\" href=\"/default/QRCodeBadgeGen?businessName=" + businessName + "&regNumber=" + regNumber + "&userID=" + userID + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Print Badge</a></li>" +
            "<li class=\"" + disableClick + "\"><a id=\"DeActivateQRCode" + qrCodeID + "\" href=\"#\"data-id=\"" + qrCodeID + "\" data-qrcodenumber=" + qrCodeNumber + " data-toggle=\"modal\" data-target=\"#modalDeactivateQRCode\"><i class=\"fa fa-remove\"></i> Deactivate</a></li>" +
        "</ul></div></div>";
    }

    return content;
}


$("#qrCodeGridSearchInput").on("keyup", function () {
    var s = $(this).val();
    qrCodeGrid.search(s).draw(false);
});


$(window).load(function () {
    //$("#qrCodeGrid").parents(".dataTables_scrollBody").css("overflow", "hidden");
    $("#qrCodeGrid").parents(".dataTables_scrollBody").css("overflow-x", "auto");
});

$("#QRCodeViewMore").on("click", function () {
    $("#qrCodeGrid_wrapper").find(".dataTables_scrollBody").css("overflow", "auto").css("overflow-y", "auto"); //OTSTM2-949 "overflow-y: auto" for IE11
    $("#qrCodeGrid_wrapper").find('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
    $(this).hide();
});

function RenderSpace() {
    return "<span></span>";
}

$("#modalAddQRCodesChooseReg").on("show.bs.modal", function (event) {
    prepareModalText();
});

function prepareModalText() { 
    var amountOfQRCodeToAdd = parseInt($("#amountOfQRCodeToAddText").val());
    $("#modalAddQRCodesSelectedRegValue").val(amountOfQRCodeToAdd);

    var verbage;
    var verbage2;
    if (amountOfQRCodeToAdd == 1) {
        verbage = amountOfQRCodeToAdd + " QR Code"
        verbage2 = "it";
        }
        else {
        verbage = amountOfQRCodeToAdd + " QR Codes"
        verbage2 = "these";
        }

    $("#modalAddQRCodesAmountText").html(verbage);
    $("#modalAddQRCodesAmountText2").html(verbage2);

    $("#invalidMessage").css("display", "none");
    $("#modalAddQRCodesRegistrantTextbox").val($('#registrationNumber').val());
    if ($('#registrationNumber').val()) {
        $("#modalAddQRCodesRegistrantTextbox").attr("disabled", true);
    }
}

$("#modalAddQRCodesChooseReg").on("click", "#modalAddQRCodesNextBtn", function () {
    var regNumberEntered = $("#modalAddQRCodesRegistrantTextbox").val();
    if (regNumberEntered == "") {
        $("#invalidMessage").css("display", "block");
    }
    else {
        verifyRegNumber();
    }
});

function verifyRegNumber() {
    var regNumberEntered = ($("#modalAddQRCodesRegistrantTextbox").val() != '') ? $("#modalAddQRCodesRegistrantTextbox").val() : $('#registrationNumber').val();
    $.ajax({
        type: "POST",
        url: 'GetMatchedRegistrant',
        data: {
            regNumberEntered: regNumberEntered
        },
        success: function (result) {
                if (result== '') {
                    $("#invalidMessage").css("display", "block");
                }
                else {
                    prepareModalText();
                    var i = result.indexOf(":");
                    var regId = result.substr(0, i);
                    var regText = result.substr(i +1);
                    $("#modalAddQRCodesSelectedRegValue").val(regId);
                    $("#modalAddQRCodesSelectedRegText").html(regText);
                    $("#modalAddQRCodesChooseReg").modal('hide');
                    $("#modalAddQRCodesToRegConfirm").modal('show');
                }
            },
        error: function (error) {
            console.log(error);
        }
    });
}

$("#modalAddQRCodesToRegConfirm").on("click", "#modalAddQRCodesConfirmYesBtn", function () {
    $.ajax({
            type: "POST",
            url: "AddQRCode",
            data: {
                    registrantId: $("#modalAddQRCodesSelectedRegValue").val(),
                    amount: parseInt($("#amountOfQRCodeToAddText").val()),
            },
            success: function () {
                qrCodeGrid.draw(true);
                $("#amountOfQRCodeToAddText").val("");
            }
    });
});

$("#modalDeactivateQRCode").on("show.bs.modal", function (event) {

    var trigger = $(event.relatedTarget);
    var qrCodeID = trigger.data("id");
    var qrCodeNumber = trigger.data("qrcodenumber");
    $("#modalDeactivateQRCodeIDValue").val(qrCodeID);
    $("#modalDeactivateQRCodeNumberText").html(qrCodeNumber);
});

$("#modalDeactivateQRCode").on("click", "#modalDeactivateQRCodeYesBtn", function () {
    $.ajax({
            type: "POST",
            url: "DeActivateQRCode",
            data: {
                    qrCodeID: $("#modalDeactivateQRCodeIDValue").val(),
                    },
            success: function () {
                qrCodeGrid.draw(true);
            }
    });
});

$("#amountOfQRCodeToAddBtn").click(function (e) {
    var amountToAdd = parseInt($("#amountOfQRCodeToAddText").val().trim());
    if (isNaN(amountToAdd) || amountToAdd <= 0) {
        e.stopPropagation();
        // $("#modalAddQRCodesChooseReg").modal({ "backdrop": "static" });
        $("#addQRCodeValidationMsg").show();
    }
    else {
        $("#addQRCodeValidationMsg").hide();
        if ($('#registrationNumber').val()) {
            verifyRegNumber();
        }
    }
});

$("#exportQRCodeBadgeCollection").on("click", function (e) {
    e.stopPropagation();
    var searchValue = $("#qrCodeGridSearchInput").val().trim();
    window.open("/Default/QRCodeBadgeCollection?activated=true&searchValue=" +searchValue);
});

$("#amountOfQRCodeToAddText").keydown(function (e) {
    if (e.keyCode == 13) {
        $("#amountOfQRCodeToAddBtn").trigger("click");
    }
});

$("#qrCodeGrid").on("draw.dt", function () {
    var recordsTotal = qrCodeGrid.page.info().recordsTotal;
    if (recordsTotal == 0) {
        $(this).find('.dataTables_empty').html('No matching records found');
    }
    var s = $("#qrCodeGridSearchInput").val();
    if (s != "") {
        $("#qrCodeGridSearchInputNumFound").html("Found " +qrCodeGrid.page.info().recordsTotal);
    }
    else {
        $("#qrCodeGridSearchInputNumFound").html("");
    }

    $("[id^='QRCodeActions']").popover({
        placement: "bottom",
        container: "body",
        html: true,
        template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
            return $($(this)).siblings(".popover-menu").html();
        },
    });
});

$("#qrCodeGridSearchInputRemoveText").on("click", function () {
    qrCodeGrid.search("").draw(false);
});

$(".btn-cancel-add-qrcode").on("click", function () {
    $("#amountOfQRCodeToAddText").val("");
});