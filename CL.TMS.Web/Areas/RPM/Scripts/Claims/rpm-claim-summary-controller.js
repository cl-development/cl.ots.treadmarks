﻿'use strict';
rpmClaimApp.controller("rpmClaimSummaryController", ["$scope", "$filter", "rpmClaimService", '$uibModal', 'claimService', function ($scope, $filter, rpmClaimService, $uibModal, claimService) {

    $scope.dataloaded = false;
    $scope.submitBtn = false;
    rpmClaimService.getClaimDetails().then(function (data) {
            //populating claim details
            $scope.RPMClaimSummary = data;
            
            $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
            $scope.disableAddTransAdjBtn = ((data.ClaimCommonModel.StatusString == "Approved") || $scope.PageIsReadonly);

            $scope.submitInfo = {
                periodShortName: data.ClaimCommonModel.ClaimPeriodName,
                registrationNum: data.ClaimCommonModel.RegistrationNumber
            };
            $scope.claimDetailsLoaded = true;
            $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant;
            $scope.dataloaded = true;
    });

    $scope.InternalAdjustTypes = [
    {
        name: 'Weight',
        id: '1'
    },
    {
        name: 'Payment',
        id: '4'
    }
    ];

    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        var viewItem = $filter('filter')($scope.InternalAdjustTypes, { name: internalAdjustType })[0];
        claimService.getInternalAdjustment(viewItem.id, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'PEligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (modalResult) {
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'PEligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }
}]);

rpmClaimApp.controller('ErrorModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'errorMessage', function ($rootScope, $scope, $http, $uibModalInstance, errorMessage) {
    $scope.errorMessage = errorMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

rpmClaimApp.controller('WarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () { 
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

rpmClaimApp.controller('ModalSubmitOneModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'rpmClaimService', function ($rootScope, $scope, $http, $uibModalInstance, $window, rpmClaimService) {
    
    $scope.finalSubmitClaim = function () {
        rpmClaimService.FinalSubmitClaim().then(function (data) {
            $scope.submitBtn = false;
            $uibModalInstance.close($scope.submitBtn);
            //$window.location.reload();
        });        
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

rpmClaimApp.controller('ModalSubmitTwoModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'submitInfo', '$window', function ($rootScope, $scope, $http, $uibModalInstance, submitInfo, $window) {

    $scope.submitInfo = submitInfo;

    $scope.confirm = function () {
        $uibModalInstance.close();
        $window.location.reload();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

rpmClaimApp.controller('PEligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.confirm = function () {
        $scope.modalResult.selectedItem = $scope.selectedItem;
        $uibModalInstance.close($scope.modalResult);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

rpmClaimApp.controller('PEligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);




