﻿$(function () {
    var pageSize = 5;
    var selectedRow;
    
    //Define data table
    var table = $('#tblTSFInternalAdjusList').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: { url: Global.Remittance.InternalAdjustmentHandleUrl, global: false },
        deferRender: true,
        dom: "rtiS",
        scrollY: 190,
        scrollCollapse: false,
        createdRow: function (row, data, index) {
            $(row).addClass('cursor-pointer');
            $(row).attr('data-adjusttype', data.TSFInternalAdjustmentType);
            $(row).attr('data-rowId', data.InternalAdjustmentId);
            $('td:has(a)', row).addClass("special-td");//td with action menu item

            $(row).on('click', 'td:not(.special-td)', function () {
                var row = this.parentElement;
                var internalAdjustId = row.attributes['data-rowid'].value;
                var internalAdjustType = row.attributes["data-adjusttype"].value;
                angular.element(document.getElementById('tsfParticipantInternalAdjustment')).scope().viewInternalAdjust(internalAdjustId, internalAdjustType);
            });
        },
        searching: true,
        ordering: true,
        order: [[0, "desc"]],

        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },

        columns: [
                 {
                     name: "AdjustmentDate",
                     width: "33%",
                     data: null,
                     render: function (data, type, full, meta) {                        
                         if (data.AdjustmentDate !== null) {
                             return data.AdjustmentDate.toString().substring(0, 10);
                        }
                        else {
                            return "";
                        }
                     }
                 },
                 {
                     name: "AdjustmentType",
                     width: "33%",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentType + "</span>";
                     }
                 },
                 {
                     name: "AdjustmentBy",
                     width: "33%",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentBy + "</span>";
                     }
                 },                               
                 {
                     name: "InternalAdjustmentId",
                     data: null,
                     className: 'display-none',
                     render: function (data, type, full, meta) {
                         return "<span>" + data.InternalAdjustmentId + "</span>";
                     }
                 }
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
            $('#internalAdjustFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() > pageSize) ? $('#viewmoreInternalAdj').css('visibility', 'visible') : $('#viewmoreInternalAdj').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#internalAdjustSearch').val();
            var url = Global.Remittance.InternalAdjustmentExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#exportInternalAdj').attr('href', url);
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    $('#viewmoreInternalAdj').on('click', function () {
        $('#tblTSFInternalAdjusList').parent().css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('#internalAdjustSearch').on('keyup', function () {
        table.search(this.value).draw(false);
    });

    $('#internalAdjustSearchBtn').on('click', function () {
        var searchValue = $('#internalAdjustSearch').val();
        table.search(searchValue).draw(false);
    });

    $('#internalAdjustRemove').on('click', function () {
        table.search("").draw(false);
        $('#internalAdjustFound').hide();
    });

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    };
});