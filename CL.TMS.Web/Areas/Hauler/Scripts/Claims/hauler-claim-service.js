﻿'use strict';
haulerClaimApp.factory('haulerClaimService', ["$http", function ($http) {

    var factory = {};

    factory.getClaimDetails = function () {
        return $http.get(Global.ParticipantClaimsSummary.ClaimDetailsUrl)
            .then(function (result) {
                return result.data;
            });
    }

    factory.getClaimPayment = function () {
        return $http.get(Global.ParticipantClaimsSummary.ClaimPaymentUrl)
                  .then(function (result) {
                      return result.data;
                  });
    }

    factory.addYardCountSubmission = function (data) {
        var req = { claimId: Global.ParticipantClaimsSummary.ClaimsID, row: data }
        $http({
            url: Global.ParticipantClaimsSummary.ClaimSortYardCountSubmissionUrl,
            method: "POST",
            data: JSON.stringify(req)
        });
    };

    factory.submitClaimCheck = function (haulerClaimModel) {

        var submitClaimModel = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            EligibleClosingTotal: haulerClaimModel.EligibleClosingTotal,
            IneligibleClosingTotal: haulerClaimModel.IneligibleClosingTotal
        };
        
        var tmpList = factory.getNilActivityClaimSummary(haulerClaimModel);
        for (var key in tmpList) {
            if (tmpList.hasOwnProperty(key)) {
                submitClaimModel[key] =  tmpList[key];
            }
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.SubmitClaimCheckUrl,
            method: "POST",
            data: JSON.stringify(submitClaimModel)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.FinalSubmitClaim = function () {
        var req = { claimId: Global.ParticipantClaimsSummary.ClaimsID }
        var submitVal = {
                url: Global.ParticipantClaimsSummary.FinalSubmitClaimUrl,
                method: "POST",
                data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.getValueFromKeyVal = function (data) {

        var obj = new Object();
        for (var i = 0; i < data.length; i++) {
            obj[data[i].Key] = data[i].Value;
        }
        return obj;
    }

    factory.getNilActivityClaimSummary = function (HaulerClaimSummary) {
        //check for nil activity for all totals in each panel
        var isNilActivity = false;

        var nilActivityObj = {
            TCROnRoad : HaulerClaimSummary.TCROnRoad,
            TCROffRoad : HaulerClaimSummary.TCROnRoad,
            DOTOnRoad : HaulerClaimSummary.DOTOnRoad,
            DOTOffRoad : HaulerClaimSummary.DOTOffRoad,
            STCOnRoad : HaulerClaimSummary.STCOnRoad,
            STCOffRoad : HaulerClaimSummary.STCOffRoad,
            UCROnRoad : HaulerClaimSummary.UCROnRoad,
            UCROffRoad : HaulerClaimSummary.UCROffRoad,
            HITOnRoad : HaulerClaimSummary.HITOnRoad,
            HITOffRoad : HaulerClaimSummary.HITOffRoad,
            PTROnRoad : HaulerClaimSummary.PTROnRoad,
            PTROffRoad : HaulerClaimSummary.PTROffRoad,
            HITOutboundOnRoad : HaulerClaimSummary.HITOutboundOnRoad,
            HITOutboundOffRoad : HaulerClaimSummary.HITOutboundOffRoad,
            RTROnRoad : HaulerClaimSummary.RTROnRoad,
            RTROffRoad : HaulerClaimSummary.RTROffRoad,
            PLT : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.PLT,
            MT : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.MT,
            AGLS : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.AGLS,
            IND : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.IND,
            SOTR : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.SOTR,
            MOTR : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.MOTR,
            LOTR : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.LOTR,
            GOTR : HaulerClaimSummary.InventoryAdjustment.ItemSubmission.GOTR
        }
        
        return nilActivityObj;
    }

    return factory;
}]);