﻿'use strict';
haulerClaimApp.directive('submitClaimcheck', ['haulerClaimService', '$uibModal', function (haulerClaimService, $uibModal) {
    return {
        restrict: 'E',
        templateUrl: 'claimSubmit.html',
        scope: {
            model: "=model",
            submitInfo: "=submitInfo",
            submitBtn: "=submitBtn",
        },
        link: function (scope, el, attrs, formCtrl) {            
        },
        controller: ['$scope', function ($scope) {
            if ($scope.model.ClaimCommonModel.StatusString == "Open")
                $scope.submitBtn = true;
            $scope.submitClaimCheck = function () {
                haulerClaimService.submitClaimCheck($scope.model).then(function (data) {

                    if (data.status != "Invalid data") {
                        if (data.Type == "Error") {
                            
                            var errorInstance = $uibModal.open({
                                templateUrl: 'ErrorModal.html',
                                controller: 'ErrorModalCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    errorMessage: function () {
                                        return data.Message;
                                    }
                                }
                            });
                        }
                        if (data.Type == "Warning") {
                            
                            var warningInstance = $uibModal.open({
                                templateUrl: 'WarningModal.html',
                                controller: 'WarningModalCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    warningMessage: function () {
                                        return data.Warnings;
                                    }
                                }
                            });
                            
                            warningInstance.result.then(function () {
                                
                                var submitOneInstance = $uibModal.open({
                                        templateUrl: 'ModalSubmitOne.html',
                                        controller: 'ModalSubmitOneModalCtrl',
                                        size: 'lg',
                                        backdrop: 'static'
                                });
                                submitOneInstance.result.then(function (data) {
                                    $scope.submitBtn = data;

                                    var submitTwoInstance = $uibModal.open({
                                        templateUrl: 'ModalSubmitTwo.html',
                                        controller: 'ModalSubmitTwoModalCtrl',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            submitInfo: function () {
                                                return $scope.submitInfo;
                                            }
                                        }
                                    });
                                    submitOneInstance.result.then(function (data) {

                                    });                                    
                                });                                
                            });                            
                        }

                        if (data.Type == "Success") {
                            var submitOneInstance = $uibModal.open({
                                templateUrl: 'ModalSubmitOne.html',
                                controller: 'ModalSubmitOneModalCtrl',
                                size: 'lg',
                                backdrop: 'static'
                            });
                            submitOneInstance.result.then(function (data) {
                                $scope.submitBtn = data;

                                var submitTwoInstance = $uibModal.open({
                                    templateUrl: 'ModalSubmitTwo.html',
                                    controller: 'ModalSubmitTwoModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        submitInfo: function () {
                                            return $scope.submitInfo;
                                        }
                                    }
                                });
                                submitOneInstance.result.then(function (data) {

                                });
                            });
                        }
                    }
                    else {
                        $scope.submitBtn = true;
                    }
                });
            }
        }]
    }
}]);









