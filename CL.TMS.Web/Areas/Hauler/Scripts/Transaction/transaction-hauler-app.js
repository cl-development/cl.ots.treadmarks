﻿"use strict";

// Transaction APP
var app = angular.module('TransactionApp', ['Transaction.directives', 'Transaction.controllers', 'ui.bootstrap', 'commonLib', 'commonTransactionLib']);


// Controllers Section
var controllers = angular.module('Transaction.controllers', []);

controllers.controller('TransactionCtrl', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {    
}]);


// Directives Section
var directives = angular.module('Transaction.directives', []);

directives.directive('ucrPickedUpFromHandler', function () {
    return {
        restrict: 'A',
        controller: function ($rootScope, $scope) {
            $scope.$watch('trans.paymentEligible', function () {                
                if (angular.isDefined($scope.trans.pickedUpFrom) && angular.isDefined($scope.trans.paymentEligible) && $scope.trans.paymentEligible == "1") {                    
                    $scope.trans.pickedUpFrom = 'unregistered';
                }
            }, true);

            $scope.$watch('trans.pickedUpFrom', function () {
                if (angular.isDefined($scope.trans.pickedUpFrom)) {
                    $rootScope.$emit('set-unregistered-company-requirement', { required: $scope.trans.pickedUpFrom == 'unregistered' });
                    $rootScope.$emit('set-vendor-information-requirement', { required: $scope.trans.pickedUpFrom == 'registered' });
                }
            }, true);
        }
    };
});

directives.directive('ucrPaymentEligibleCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            paymentEligible: "=",
            paymentEligibleOther: "="
        },
        templateUrl: 'ucrPaymentEligible.html',
        controller: function ($scope) {
        }
    };
});

directives.directive('stcEventNumberCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            eventNumber: "="
        },
        templateUrl: 'stcEventNumber.html',
        controller: function ($scope) {
        }
    };
});

directives.directive('rtrTireMarketCollector', function () {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            tireMarket: "=",
            countryList: "=",
            supportingDocumentList: "=",
            companyInfo: "="
        },
        templateUrl: 'rtrTireMarket.html',
        controller: function ($scope) {
            $scope.filterOutCanadaAndUSA = function (element) {
                return element.match(/^Canada|USA/) ? false : true;
            };

            $scope.tireMarketLocationChange = function () {
                var supportingDocument = _.find($scope.supportingDocumentList, function (i) { return i.documentName == "Bill of Lading"; });
                supportingDocument.required = $scope.tireMarket.location == "USA";

                if ($scope.tireMarket.location == "Canada: Ontario") $scope.companyInfo.province = "Ontario";
                else $scope.companyInfo.province = "";

                if ($scope.tireMarket.location == "Canada: Ontario" || $scope.tireMarket.location == "Canada: Not Ontario") $scope.companyInfo.country = "Canada";
                else if ($scope.tireMarket.location == "USA") $scope.companyInfo.country = "USA";
                else $scope.companyInfo.country = "";
            };
        }
    };
});