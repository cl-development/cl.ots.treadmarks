﻿$(function() {
    // Popover Setup
    var settings = {
        trigger: 'hover',
        //title:'Pop Title',
        //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
        //width:300,
        multi: true,
        closeable: true,
        style: '',
        delay: { show: 300, hide: 800 },
        padding: true
    };

    // Popover: Participant Information
    var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
      popoverParticipantInfoSettings = {
          content: popoverParticipantInfoContent,
          width: 270
      };
    var popLargeLBN = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));
})