﻿
var appName = $("[ng-app]").attr("ng-app");
var mainApp = angular.module(appName);

mainApp.factory('notificationsService', ["$http", function ($http) {
    var factory = {};

    factory.loadUnreadNotifications = function () {
        return $http.get("/System/Common/LoadUnreadNotifications")
            .then(function (result) {
                return result.data;
            });
    }

    factory.totalUnreadNotification = function () {
        return $http.get("/System/Common/TotalUnreadNotification")
        .then(function (result) {
            return result.data;
        });
    }

    factory.totalNotification = function () {
        return $http.get("/System/Common/TotalNotification")
        .then(function (result) {
            return result.data;
        });
    }

    factory.markAllAsRead = function () {
        return $http.get("/System/Common/MarkAllAsRead");
    }

    factory.updateNotificationStatus = function (data) {
        var req = {
            notificationId: data.notificationId,
            newStatus: data.newStatus
        }
        var submitVal = {
            url: "/System/Common/UpdateNotificationStatus",
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.redirectToClaimSummaryPage = function (data) {
        var req = {
            notificationId: data.notificationId
        }
        var submitVal = {
            url: "/System/Common/RedirectToClaimSummaryPage",
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    return factory;
}]);

mainApp.directive('globalNotification', ['$http', "notificationsService", function ($http, notificationsService) {

    return {
        restrict: 'AE',
        replace: false,
        templateUrl: "globalNotificationTemplate",
        scope: {
            unreadNotificationList: '=',
            totalUnreadNotification: '=',
            totalNotification: '='
        },
        link: function (scope, el, attrs) {
            //if anywhere else is clicked beside the bell icon
            $(window).on('click', function () {
                var em = document.getElementById("dropDownTarget");
                em.classList.add("myFade");
                var anotherEm = document.getElementById("button-badge-parent");
                anotherEm.classList.add("no-after-before");
            });

            el.parent().parent().bind("click", function (e) {
                e.stopPropagation();
                //if bell is clicked
                if (e.target.id == 'notificationBell' || e.target.id == 'bell' || e.target.id == 'button-badge') {
                    notificationsService.loadUnreadNotifications().then(function (data) {
                        scope.unreadNotificationList = data;
                        var em = document.getElementById("dropDownTarget");
                        em.classList.toggle("myFade");
                        var anotherEm = document.getElementById("button-badge-parent");
                        anotherEm.classList.toggle("no-after-before");                     
                        if (scope.totalNotification == 0) {
                                document.getElementById("markAllAsRead").classList.add("greyOut");
                                document.getElementById("SeeAllNotifications").classList.add("greyOut");
                        }
                        else if (scope.totalUnreadNotification == 0) {
                            document.getElementById("markAllAsRead").classList.add("greyOut");
                        }                                                              
                    });                   
                    return;
                }

                //if see all notification is clicked
                if (e.target.id == "SeeAllNotifications") {
                    window.location.href = '/System/Common/SeeAllNotifications';
                }

                //if mark all as read is clicked
                if (e.target.id == "markAllAsRead") {
                    notificationsService.markAllAsRead().success(function (result) {
                        if (result.status) {
                           $('.myDropdown .fa-circle').removeClass('fa-circle').addClass('fa-circle-o');
                           $('.myDropdown .containerscont').css('border-right', '7px solid transparent');
                           document.getElementById("markAllAsRead").classList.add("greyOut");
                           notificationsService.totalUnreadNotification().then(function (data) {
                               scope.totalUnreadNotification = data;                               
                           });
                        }
                    });                   
                }              

                //if empty-circle is clicked
                if (e.target.className.indexOf("fa-circle-o") !== -1) {
                    var notificationId = $(e.target).parent().children("#NotificationId").val();
                    var data = { notificationId: notificationId, newStatus: "Unread" };
                    notificationsService.updateNotificationStatus(data).success(function (result) {
                        if (result.status) {
                            e.target.classList.remove("fa-circle-o");
                            e.target.classList.add("fa-circle");
                            $(e.target).parents(".containerscont").css('border-right', '7px solid cornflowerblue');
                            notificationsService.totalUnreadNotification().then(function (data) {
                                scope.totalUnreadNotification = data;
                                if (scope.totalUnreadNotification > 0) {
                                    document.getElementById("markAllAsRead").classList.remove("greyOut");
                                }  
                            });
                                                
                        }
                    });                   
                    return;
                }

                    //if full-circle is clicked
                else if (e.target.className.indexOf("fa-circle") !== -1) {               
                    var notificationId = $(e.target).parent().children("#NotificationId").val();
                    var data = { notificationId: notificationId, newStatus: "Read" };
                    notificationsService.updateNotificationStatus(data).success(function (result) {
                        if (result.status) {
                            e.target.classList.remove("fa-circle");
                            e.target.classList.add("fa-circle-o");
                            $(e.target).parents(".containerscont").css('border-right', '7px solid transparent');
                            notificationsService.totalUnreadNotification().then(function (data) {
                                scope.totalUnreadNotification = data;
                                if (scope.totalUnreadNotification == 0) {
                                    document.getElementById("markAllAsRead").classList.add("greyOut");
                                }
                            });
                            
                        }                  
                    });
                    return;
                }

                //if cross is clicked
                if (e.target.className.indexOf("fa-times") !== -1) {
                    var notificationId = $(e.target).parent().children("#NotificationId").val();
                    var data = { notificationId: notificationId, newStatus: "Hidden" };
                    notificationsService.updateNotificationStatus(data).success(function (result) {
                        if (result.status) {
                            $(e.target).parents(".myDropdown li").css('display', 'none');
                        }
                    });                 
                }
            });
        },
        controller: ['$scope', function ($scope) {

            //if main-msg is clicked
            $scope.redirect = function () {
                var notificationId = $(this)[0].notification.ID;
                var data = { notificationId: notificationId };
                //var newTabAvoidPopupBlocker = window.open('', '_self');
                //newTabAvoidPopupBlocker.document.write('Loading data...');
                //clear admin session and then do the following
                $.ajax({
                    url: '/Account/ClearSessionForAdminLogin',
                    dataType: 'json',
                    type: 'POST',
                    success: function (result) {                     
                        notificationsService.redirectToClaimSummaryPage(data).success(function (result) {
                            var nextData = { notificationId: notificationId, newStatus: "Read" };
                            notificationsService.updateNotificationStatus(nextData).success(function (result) {
                            });
                            window.location.href = '/System/Claims/ClaimsTypeRouting' + '?claimId=' + result.ClaimId + '&regNumber=' + result.RegistrationNumber;
                            //newTabAvoidPopupBlocker.location.href = '/System/Claims/ClaimsTypeRouting' + '?claimId=' + result.ClaimId + '&regNumber=' + result.RegistrationNumber, '_self';
                        });
                    }
                });
                
            }

            //badge size: 3 digits or 4 digits
            $scope.$watch('totalUnreadNotification', function (newValue, oldValue) {
                //if (scope.totalUnreadNotification > 0 && scope.totalUnreadNotification <= 99) {
                //    //angular.element("#button-badge").css("width", "13px");
                //    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "15px").css("color", "white").css("position", "absolute").css("bottom", "0").css("right", "0").css("height", "13px").css("width", "13px").css("text-align", "center").css("font-size", "12px");
                //}
                //else if (scope.totalUnreadNotification > 99 && scope.totalUnreadNotification <= 999) {
                //    //angular.element("#button-badge").css("width", "18px");
                //    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "15px").css("color", "white").css("position", "absolute").css("bottom", "0").css("right", "0").css("height", "13px").css("width", "18px").css("text-align", "center").css("font-size", "12px");
                //}
                //else if (scope.totalUnreadNotification > 999) {
                //    //angular.element("#button-badge").css("width", "23px");
                //    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "15px").css("color", "white").css("position", "absolute").css("bottom", "0").css("right", "0").css("height", "13px").css("width", "23px").css("text-align", "center").css("font-size", "12px");
                //}
                //change for cube UI
                if ($scope.totalUnreadNotification > 0 && $scope.totalUnreadNotification <= 99) {
                    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "50%").css("color", "white").css("position", "absolute").css("top", "-6px").css("right", "-6px").css("height", "14px").css("line-height", "14px").css("width", "14px").css("text-align", "center").css("font-size", "9px");
                }
                else if ($scope.totalUnreadNotification > 99 && $scope.totalUnreadNotification <= 999) {
                    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "50%").css("color", "white").css("position", "absolute").css("top", "-6px").css("right", "-6px").css("height", "14px").css("line-height", "14px").css("width", "18px").css("text-align", "center").css("font-size", "9px");
                }
                else if ($scope.totalUnreadNotification > 999) {
                    angular.element("#button-badge").css("background-color", "#fa3e3e").css("border-radius", "50%").css("color", "white").css("position", "absolute").css("top", "-6px").css("right", "-6px").css("height", "14px").css("line-height", "14px").css("width", "23px").css("text-align", "center").css("font-size", "9px");
                }
            });
        }]
    }
}]);

mainApp.controller("notificationController", ["$scope", "notificationsService", function ($scope, notificationsService) {

    notificationsService.totalUnreadNotification().then(function (data) {
        $scope.totalUnreadNotification = data;
    });
    notificationsService.totalNotification().then(function (data) {
        $scope.totalNotification = data;
    });

    $scope.increasingTotalUnreadNotification = function () {
        //Reload list
        notificationsService.loadUnreadNotifications().then(function (data) {
            $scope.unreadNotificationList = data;
        });
        //Reload qty of total notification for the datatable reload of "SeeAllNotification" page
        notificationsService.totalNotification().then(function (data) {
            $scope.totalNotification = data;
            $scope.$parent.$emit('TOTAL_NOTIFICATION', data);
        });
        $scope.totalUnreadNotification = $scope.totalUnreadNotification + 1;
        if ($scope.totalUnreadNotification!=0) {
            document.getElementById("markAllAsRead").classList.remove("greyOut");
            document.getElementById("SeeAllNotifications").classList.remove("greyOut");         
        }
        $scope.$apply();
    }
}]);

mainApp.filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);
