﻿'use strict';

//spotlightSearchResult App
(function () {
    var spotlightSearchResultApp = angular.module('spotLightSearchResultApp', ['commonLib', 'ui.bootstrap', 'datatables', 'datatables.scroller', 'spotlightSearchResult.directives']);

    var directives = angular.module('spotlightSearchResult.directives', [])
    directives.directive('spotlightSearchResultPanel', ['DTOptionsBuilder', 'DTColumnBuilder', '$timeout', spotlightSearchResultPanel])

    function spotlightSearchResultPanel(DTOptionsBuilder, DTColumnBuilder, $timeout) {
        var directive = {
            scope: {
                spUrl: '@',
                section: '@',
                title: '@'
            },
            controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {

                $scope.defaultsort = [0, 'desc'];

                $scope.dtInstance = {};

                $rootScope.$on('spotlight_panel_update', function (e, data) {
                    $scope.dtInstance.DataTable.draw();
                });

                if ($scope.section == 'Applications') {                    
                    $scope.dtColumns = [
                            DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').withOption('width', '20%').renderWith(function (data, type, full, meta) {
                                if (data == '1') {
                                    return '<div class="panel-table-status color-tm-green-bg" style="width:110px;" title="Active">Active</div>';
                                }
                                else {
                                    return '<div class="panel-table-status color-tm-red-bg" style="width:110px;" title="Inactive">Inactive</div>';
                                }
                            }),
                            DTColumnBuilder.newColumn("ModuleType", "Type").withOption('name', 'ModuleType').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder1", "Reg. #").withOption('name', 'PlaceHolder1').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder3", "Operating Name").withOption('name', 'PlaceHolder3').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder2", "Legal Name").withOption('name', 'PlaceHolder2').withOption('width', '20%')
                    ];
                }

                else if ($scope.section == 'Claims') {

                    $scope.defaultsort = [1, 'asc'];
                    $scope.dtColumns = [
                            DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').withOption('width', '20%').renderWith(function (data, type, full, meta) {
                                return getStatusIcon(data);
                            }),
                            DTColumnBuilder.newColumn("ModuleType", "Type").withOption('name', 'ModuleType').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder1", "Reg. #").withOption('name', 'PlaceHolder1').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder2", "Period").withOption('name', 'PlaceHolder2').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder3", "Amount ($)").withOption('name', 'PlaceHolder3').withOption('width', '20%')
                    ];
                }

                else if ($scope.section == 'Remittances') {

                    $scope.defaultsort = [2, 'desc'];
                    $scope.dtColumns = [
                            DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').withOption('width', '25%').renderWith(function (data, type, full, meta) {
                                return getStatusIcon(data);
                            }),
                            DTColumnBuilder.newColumn("PlaceHolder1", "Reg. #").withOption('name', 'PlaceHolder1').withOption('width', '25%'),
                            DTColumnBuilder.newColumn("PlaceHolder2", "Period").withOption('name', 'PlaceHolder2').withOption('width', '25%'),
                            DTColumnBuilder.newColumn("PlaceHolder3", "Total ($)").withOption('name', 'PlaceHolder3').withOption('width', '25%')
                    ];
                }

                else if ($scope.section == 'Users') {

                    $scope.defaultsort = [1, 'asc'];
                    $scope.dtColumns = [
                             DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').withOption('width', '20%').renderWith(function (data, type, full, meta) {
                                 if (data == 'Active') {
                                     return '<div class="panel-table-status color-tm-green-bg" style="width:110px;" title="Active">Active</div>';
                                 }
                                 else if(data == 'Inactive') {
                                     return '<div class="panel-table-status color-tm-red-bg" style="width:110px;" title="Inactive">Inactive</div>';
                                 }
                                 else {
                                     return '<div class="panel-table-status color-tm-yellow-bg" style="width:110px;" title="Inactive">Locked</div>';
                                 }
                             }),
                            DTColumnBuilder.newColumn("PlaceHolder1", "Name").withOption('name', 'PlaceHolder1').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder2", "Email").withOption('name', 'PlaceHolder2').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder3", "Reg. #").withOption('name', 'PlaceHolder3').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder4", "Business Name").withOption('name', 'PlaceHolder4').withOption('width', '20%')
                    ];
                }

                else if ($scope.section == 'Transactions') {

                    $scope.defaultsort = [3, 'desc'];
                    $scope.dtColumns = [
                             DTColumnBuilder.newColumn("Status", "Status").withOption('name', 'Status').withOption('width', '20%').renderWith(function (data, type, full, meta) {
                                 return getTransactionStatusIcon(data);
                             }),
                            DTColumnBuilder.newColumn("PlaceHolder1", "TRANS. #").withOption('name', 'PlaceHolder1').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder2", "Type").withOption('name', 'PlaceHolder2').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder3", "Trans. Date").withOption('name', 'PlaceHolder3').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder4", "INC. Badge / Reg #").withOption('name', 'PlaceHolder4').withOption('width', '20%'),
                            DTColumnBuilder.newColumn("PlaceHolder5", "OUT. REG. #").withOption('name', 'PlaceHolder5').withOption('width', '20%')
                    ];
                }                

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.spUrl,
                    type: "POST",
                    error: function (xhr, error, thrown) {
                        console.log('xhr, error:', xhr, error);
                        window.location.reload(true);
                    }
                }).withOption('processing', true)
                    .withOption('responsive', true).withOption('bAutoWidth', true)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', $scope.defaultsort)
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                        //OTSTM2-895 clicking more, make sure alignment
                        if ($scope.viewMore != undefined && $scope.viewMore == false) {                           
                            $(angular.element(row)["0"].children[2]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[3]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[5]).addClass('move-right-25px');                           
                        }
                    })
                    .withOption('initComplete', function (settings, result) {
                        $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                        $scope.ifDisplay = result.data.length > 0; //make sure only display the panel which has records
                        Global.noLoadSpinner = true; //disable spinner after datatable loaded
                        $scope.$apply();
                    })
                    .withOption('drawCallback', function (settings) {
                        $scope.found = 'Found ' + settings.fnRecordsDisplay();
                        $scope.viewMore = (settings.fnRecordsDisplay() > 5 && !$scope.scroller);
                        //OTSTM2-960/963/964/965/966
                        $scope.sortColumn = settings.aLastSort[0].col;
                        $scope.sortDirection = settings.aLastSort[0].dir;
                        if (!$scope.searchText) {
                            $scope.searchText = '';
                        }
                        $scope.$apply();
                    });

                $scope.url = $scope.spUrl; //keep original spUrl
                $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while $scope.searchText is not empty (only happens once), default is false
                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    //hidden div horizontal scroll after more click
                    $scope.panelAfterMore = { 'overflow': 'hidden' };
                    if (($scope.searchText) && ($scope.searchText.length >= 1)) {
                        var url = $scope.spUrl;
                        url = url + "&temp=" + $scope.searchText;
                        $scope.spUrl = url;
                        $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while $scope.searchText is not empty, only happens once
                    }
                    $scope.dtOptions.withScroller()
                                   .withOption('deferRender', true)
                                   .withOption('scrollY', 250)
                                   .withOption('scrollX', "100%")
                                   .withOption('responsive', true)
                                    .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                        dataSrc: "data",
                                        url: $scope.spUrl,
                                        global: false, // this makes sure ajaxStart is not triggered
                                        type: "POST"
                                    });
                }

                $scope.delay = (function () {
                    var promise = null;
                    
                    return {
                        calculateDelay: function (callback, ms) {
                            $timeout.cancel(promise);
                            promise = $timeout(callback,ms)
                        }
                    }                    
                })();

                //search
                $scope.search = function () {

                    if ($scope.searchText.length >= 1) {
                        $scope.foundStyle = { 'display': 'block' };
                        $scope.removeIconStyle = { 'display': 'block' };
                        $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                        $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                    }
                    else {
                        $scope.foundStyle = { 'display': 'none' };
                        $scope.removeIconStyle = { 'display': 'none' };
                        $scope.searchStyle = '';
                        $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                    }
                }

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                        $scope.spUrl = $scope.url;
                        $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                        $scope.dtOptions.withScroller()
                                   .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                       dataSrc: "data",
                                       url: $scope.spUrl,
                                       global: false,
                                       type: "POST"
                                   });
                    }
                    else {
                        dtInstance.DataTable.search($scope.searchText).draw(false);
                    }
                }

                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td', nRow).bind('click', function () {

                        //if (aData.ModuleType === "Steward") {
                        //    var url = aData.ModuleType + "/Registration/CustomerIndex?cId=" + aData.ID;
                        //}
                        //else {
                        //    var url = aData.ModuleType + "/Registration/RegistrationIndex?vId=" + aData.ID;
                        //}

                        
                        if (aData.ModuleType === "Steward" && $scope.section === "Applications") {
                            var url = aData.ModuleType + "/Registration/CustomerIndex?cId=" + aData.ID;
                        }
                        else if ($scope.section === "Applications") {
                            var url = aData.ModuleType + "/Registration/RegistrationIndex?vId=" + aData.ID;
                        }
                        else if ($scope.section === "Claims") {
                            var url = aData.ModuleType + "/Claims/ClaimSummary?claimId=" + aData.ID + "&vId=" + aData.vendorID;
                        }
                        else if ($scope.section === "Remittances") {
                            var url = "Steward/TSFRemittances/StaffIndex/" + aData.ID + "?vId=" + aData.vendorID;
                        }
                        else if ($scope.section === "Users") {
                            var url = "System/Invitation/ParticipantIndex?vendorId="+ aData.vendorID + "&searchText=" + aData.PlaceHolder2;
                        }
                        else if ($scope.section === "Transactions") {
                            var url = "System/Transactions/Index?searchText=" + aData.PlaceHolder1 + "&transID=" + aData.ID;
                        }

                        $window.open('/' +url, '_self');
                    });
                    return nRow;
                }

                $('.global-search-result').on('processing.dt', function () {
                    $scope.found = '';
                })
            },
            restrict: 'E',
            link: function (scope, elem) {},
            templateUrl: 'spotlight-search-result-panel.html'
        }
        return directive;
    }

    function getStatusIcon(status) {
        switch (status) {
            case "Open":
                return '<div class="panel-table-status color-tm-blue-bg" style="width:110px;" title="Open">Open</div>';
                //return 'global-search-bluedot';
                break;
            case "Under Review":
            case "UnderReview":
                return '<div class="panel-table-status color-tm-yellow-bg" style="width:110px;" title="Under Review">Under Review</div>';
                //return 'global-search-yellowdot';
                break;
            case "Submitted":
                return '<div class="panel-table-status color-tm-orange-bg" style="width:110px;" title="Submitted">Submitted</div>';
                //return 'global-search-orangedot'
                break;
            case "1":
            case "Approved":
                return '<div class="panel-table-status color-tm-green-bg" style="width:110px;" title="Approved">Approved</div>';
                //return 'global-search-greendot'
                break;
            case "0":
                return '<div class="panel-table-status color-tm-red-bg" style="width:110px;" title="Rejected">Rejected</div>';
                //return 'global-search-reddot'
                break;
        }
    }

    function getTransactionStatusIcon(status) {
        switch (status) {
            case "Incomplete":
                return '<div class="panel-table-status color-tm-yellow-bg" style="width:110px;" title="Incomplete">Incomplete</div>';
                break;
            case "Pending":
                return '<div class="panel-table-status color-tm-orange-bg" style="width:110px;" title="Pending">Pending</div>';
                break;
            case "Completed":
                return '<div class="panel-table-status color-tm-green-bg" style="width:110px;" title="Completed">Completed</div>';
                break;
            case "Voided":
                return '<div class="panel-table-status color-tm-grey-light-bg" style="width:110px;" title="Voided">Voided</div>';
                break;
            case "Rejected":
                return '<div class="panel-table-status color-tm-red-bg" style="width:110px;" title="Rejected">Rejected</div>';
                break;
        }
    }

    //OTSTM2-960/963/964/965/966
    directives.directive('globalSearchCommonExport', [function () {
        return {
            restrict: 'E',
            templateUrl: 'anchorExport.html',
            scope: {
                section: '=',
                sortColumn: '=',
                sortDirection: '=',
                searchText: '='
            },
            link: function (scope, element, attr, ngModel) {
                scope.left = attr.attrAnchor;
                if (scope.section.replace(/\s/g, '') == "Users" ) {
                    scope.left = "160px";
                }
                else if (scope.section.replace(/\s/g, '') == "Claims") {
                    scope.left = "75px";
                }
                scope.anchorCss = {
                    "background": "none",
                    "left": scope.left,
                    "position": "absolute",
                    "top": "11px",
                    "width": "25px"
                };

                element.bind('click', function (event) {
                    event.stopPropagation();
                    event.preventDefault();

                    switch (scope.section.replace(/\s/g, '')) {
                        case "Claims":
                            window.location = Global.GlobalSearch.ExportGlobalSearchClaimsUrl.replace('AAA', scope.sortColumn).replace('BBB', scope.sortDirection).replace('----', scope.searchText);
                            break;
                        case "Remittances":
                            window.location = Global.GlobalSearch.ExportGlobalSearchRemittancesUrl.replace('AAA', scope.sortColumn).replace('BBB', scope.sortDirection).replace('----', scope.searchText);
                            break;
                        case "Transactions":
                            window.location = Global.GlobalSearch.ExportGlobalSearchTransactionsUrl.replace('AAA', scope.sortColumn).replace('BBB', scope.sortDirection).replace('----', scope.searchText);
                            break;
                        case "Applications":
                            window.location = Global.GlobalSearch.ExportGlobalSearchApplicationsUrl.replace('AAA', scope.sortColumn).replace('BBB', scope.sortDirection).replace('----', scope.searchText);
                            break;
                        case "Users":
                            window.location = Global.GlobalSearch.ExportGlobalSearchUsersUrl.replace('AAA', scope.sortColumn).replace('BBB', scope.sortDirection).replace('----', scope.searchText);
                            break;
                    }
                });
            },
            controller: function ($scope, $element) { }
        };
    }]);
})();