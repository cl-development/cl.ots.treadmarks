﻿var globalApp = (function () {
    'use strict';
    //Global Search App
    var globalSearchApp = angular.module("globalSearchApp", ["commonLib", "ui.bootstrap", "globalSearchApp.directives", "globalSearchApp.controllers", "globalSearchApp.services"]);
    return {
        getGlobalSearchApp: function () {
            return globalSearchApp; 
        }
    }
})();

var AllServices = (function () {
    'use strict';
    //Services Section
    var services = angular.module('globalSearchApp.services', []);
    services.service('globalSearchServices', globalSearchServices);

    function globalSearchServices($http) {
        var factory = {};

        factory.getUserName = function () {
            return GlobalSearch.UserName();
        }

        factory.getSpotLightSearch = function (searchText, filters) {
            return $http.get(GlobalSearch.SpotLightSearch().replace('filters', filters) + searchText).then(function (result) {
               return result.data;
           });
        }

        factory.getPreviousSearchWord = function () {
            return $http.get(GlobalSearch.getPreviousSearchWordUrl()).then(function (result) {
                return result.data;
            })
        }

        factory.postPreviousSearchWord = function () {
            var submitVal = {
                url: GlobalSearch.getPreviousSearchWordUrl(),
                method: "POST",
                data: ''
            }
            return $http(submitVal).then(function (result) {
                return result.data;
            });
        }
        return factory;
    }

    return {
        globalSearchServices: globalSearchServices
    }
})();

var AllControllers = (function (globalSearchServices) {
    'use strict';
    //Controllers Section
    var controllers = angular.module('globalSearchApp.controllers', []);
    controllers.controller('globalSearchController', ['$rootScope', '$scope', '$http', '$uibModal', globalSearchController]);
    controllers.controller('searchMenuBarController', ['$scope', '$http', '$window', '$uibModal', '$uibModalInstance', '$rootScope', searchMenuBarController]);

    function globalSearchController($rootScope, $scope, $http, $uibModal) {
        var vm = this;
        
        $rootScope.$on('Spy_Glass_Click', function (e, data) {
             var searchInstance = $uibModal.open({
                templateUrl: 'search-bar-modal.html',
                controller: 'searchMenuBarController',
                size: 'lg'
            });            
        });        
    }

    function searchMenuBarController($scope, $http, $window, $uibModal, $uibModalInstance, $rootScope) {
        //To do
        $rootScope.$on('CANCEL', function (e, data) {
            $uibModalInstance.dismiss('cancel');
        });
    }

})(AllServices.globalSearchServices);

var AllDirectives = (function (globalSearchServices) {
    'use strict';
    //Directives Section
    var directives = angular.module('globalSearchApp.directives', []);
    directives.directive('spyGlass', ['globalSearchServices', '$rootScope', spyGlass])
    directives.directive('globalSearchContainer', ['globalSearchServices', '$rootScope', '$timeout', '$window','$http', globalSearchContainer])
    directives.directive('moduleSearchResult', ['globalSearchServices', '$rootScope', '$window', moduleSearchResult])
    directives.directive('keyEvent', [keyEvent])
    directives.directive('dynamicInputResize', [dynamicInputResize])
    directives.directive('iconsBySection', [iconsBySection])
    directives.directive('iconsByStatus', [iconsByStatus])
    directives.directive('autoSelect', ['$timeout', autoSelect])

    //auto select Global search text when re-openning it
    function autoSelect($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $timeout(function () {
                    $(element).select();
                }, 50);
            }
        };
    }
    
    function spyGlass(globalSearchServices, $rootScope) {
        var directive = {
            link: link,
            controller: ['$scope', function($scope) {
            }],
            scope: {},
            restrict: 'A'
        }

        return directive;

        function link(scope, element, attrs) {
            element.bind('click', function () {
                $rootScope.$emit('Spy_Glass_Click', "spyGlassClick");
            });
        }
    }

    //main directive
    function globalSearchContainer(globalSearchServices, $rootScope, $timeout, $window, $http) {

        var directive = {
            link: link,
            controller: ['$scope', function ($scope) {
                $scope.searchInput = '';
                $scope.showSpotLightResult = false;
                $scope.isSearchDataLoaded = false;
                $scope.groupLimit = GlobalSearch.GroupLimit();
                $scope.doneTyping = false;
                $scope.scrollDiv = angular.element(document.querySelector("#scrollDiv"))[0];
                $scope.scrollDiv.scrollTop = 0

                $scope.keyEvents = [
                        {
                            code: 38,
                            highlightAction: function () { ($scope.highlightIndex - 1 > 0) ? $scope.highlightIndex-- : $scope.highlightIndex = 0 }, //up arrow
                            scrollAction: function () { $scope.scrollDiv.scrollTop -= 10 }
                        },
                        {
                            code: 40,
                            highlightAction: function () { ($scope.highlightIndex + 1 < $scope.maxHighlightIndex) ? $scope.highlightIndex++ : $scope.highlightIndex = $scope.maxHighlightIndex - 1 },//down arrow
                            scrollAction: function () { $scope.scrollDiv.scrollTop += 10 }
                        },
                        {
                            code: 13,
                            highlightAction: function () { openUrlByEnterKey() } //enter key
                        }
                ];

                globalSearchServices.postPreviousSearchWord().then(function (result) {
                    if (result.status) {

                        $scope.searchInput = result.previousSearchWord;                        
                        
                        var allAccess = GlobalSearch.ClaimsAccess() == "true" && GlobalSearch.RemittancesAccess() == "true" && GlobalSearch.TransactionsAccess() == "true" && GlobalSearch.ApplicationsAccess() == "true" && GlobalSearch.UsersAccess() == "true";

                        if (result.previousSearchFilters.indexOf("All") != -1 && allAccess) {
                            $scope.chkClaims = $scope.chkRemittances = $scope.chkTransactions = $scope.chkApplications = $scope.chkUsers = true;
                            return;
                        }
                        
                        $scope.chkClaims = result.previousSearchFilters.indexOf("Claims") !== -1 && GlobalSearch.ClaimsAccess() == "true";
                        $scope.chkRemittances = result.previousSearchFilters.indexOf("Remittances") !== -1 && GlobalSearch.RemittancesAccess() == "true";
                        $scope.chkTransactions = result.previousSearchFilters.indexOf("Transactions") !== -1 && GlobalSearch.TransactionsAccess() == "true";
                        $scope.chkApplications = result.previousSearchFilters.indexOf("Applications") !== -1 && GlobalSearch.ApplicationsAccess() == "true";
                        $scope.chkUsers = result.previousSearchFilters.indexOf("Users") !== -1 && GlobalSearch.UsersAccess() == "true";

                        //$scope.searchTextInputChange();
                    }
                });

                function getSearchFilters() {                    

                    if ($scope.chkClaims && $scope.chkRemittances && $scope.chkTransactions && $scope.chkApplications && $scope.chkUsers)
                        return "All";

                    var filters = "";
                    if ($scope.chkClaims)
                        filters = "Claims";
                    if ($scope.chkRemittances)
                        filters = filters.concat(" Remittances");
                    if ($scope.chkTransactions)
                        filters = filters.concat(" Transactions");
                    if ($scope.chkApplications)
                        filters = filters.concat(" Applications");
                    if ($scope.chkUsers)
                        filters = filters.concat(" Users");

                    return filters;
                }

                function ajaxindicatorstart(text) {
                    var pathname = window.location.pathname; // Returns path only
                    if ($('body').find('#resultLoading').attr('id') != 'resultLoading') {
                        if (pathname.indexOf('/Transaction/Index/' > -1)) {
                            $('body').append('<div id="resultLoading" ><div><img src="../../../gui/shared/ajax-loader.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
                        } else {
                            $('body').append('<div id="resultLoading" ><div><img src="../../gui/shared/ajax-loader.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
                        }
                    }

                    $('#resultLoading').css({
                        'width': '100%',
                        'height': '100%',
                        'position': 'fixed',
                        'z-index': '10000000',
                        'top': '0',
                        'left': '0',
                        'right': '0',
                        'bottom': '0',
                        'margin': 'auto'
                    });

                    $('#resultLoading .bg').css({
                        'background': '#000000',
                        'opacity': '0.7',
                        'width': '100%',
                        'height': '100%',
                        'position': 'absolute',
                        'top': '0'
                    });

                    $('#resultLoading>div:first').css({
                        'width': '250px',
                        'height': '75px',
                        'text-align': 'center',
                        'position': 'fixed',
                        'top': '0',
                        'left': '0',
                        'right': '0',
                        'bottom': '0',
                        'margin': 'auto',
                        'font-size': '16px',
                        'z-index': '10',
                        'color': '#ffffff'

                    });

                    $('#resultLoading .bg').height('100%');
                    $('#resultLoading').fadeIn(300);
                    $('body').css('cursor', 'wait');
                }

                function ajaxindicatorstop() {
                    $('#resultLoading .bg').height('100%');
                    $('#resultLoading').fadeOut(300);
                    $('body').css('cursor', 'default');
                }

                //ajax to get all data retrieved from spotlight search service
                $scope.searchTextInputChange = function () {                   
                    var filters = getSearchFilters();

                    //No Search if no filter selected
                    if (filters == "")
                        return;
                    ajaxindicatorstart('loading data.. please wait..');
                    globalSearchServices.getSpotLightSearch($scope.searchInput, filters).then(function (result) {
                        if (result.status && result.data.length > 0) {
                            var index = 0;
                            $scope.maxHighlightIndex = result.data.length;

                            //make sure returnedSearchVal has value when clicking searchArrow again
                            if ($scope.highlightIndex == null) {
                                $scope.highlightIndex = 0;
                                $scope.returnedSearchVal = null;
                            }

                            $scope.scrollDiv.scrollTop = 0

                            //$scope.searchResult = _.groupBy(result.data, "ModuleType");
                            //_.each($scope.searchResult, function (val, key) {
                            //    _.each(val, function (val, key) {
                            //        val['highlightIndex'] = index;
                            //        index++;
                            //    });
                            //});

                            $scope.searchResult = _.groupBy(result.data, function (d) {

                                //make sure no double header
                                if (d.Section == "Remittances" || d.Section == "Transactions") {
                                    return d.Section;
                                }
                                else {
                                    return d.ModuleType + " " + d.Section;
                                }
                                
                            });
                            _.each($scope.searchResult, function (val, key) {
                                _.each(val, function (val, key) {
                                    val['highlightIndex'] = index;
                                    index++;
                                });
                            });

                            $scope.isSearchDataLoaded = true;
                            $scope.showSpotLightResult = true;
                        }
                        else {
                            $scope.searchResult = null;
                            $scope.isSearchDataLoaded = false;
                            $scope.showSpotLightResult = false;
                        }
                        ajaxindicatorstop();
                    });
                }

                $scope.delay = (function () {
                    var promise = null;

                    return {
                        calculateDelay: function (callback, ms) {
                            $timeout.cancel(promise);
                            promise = $timeout(callback, ms)
                        }
                    }
                })();

                $scope.keyEventCall = function (code) {
                    $scope.keyEvents.forEach(function (keyEvent) {
                        if (keyEvent.code !== code) { return; }
                        keyEvent.highlightAction()
                        if (keyEvent.code !== 13) {
                            keyEvent.scrollAction()
                        }
                        $scope.$apply();
                    });
                }

                $scope.$watch('highlightIndex', function (n, o) {

                    if (n !== o && $scope.searchInput.length >= 2) {
                        updateSearchInput();
                    }
                }, true);

                $scope.copyPaste = function ($event) {
                    if (angular.element($event.currentTarget).val()) {
                        $event.preventDefault();
                    }
                }

                $scope.showAllResults = function () {
                    $window.open(GlobalSearch.getSpotlightSearchResult() + $scope.searchInput, '_self');
                }

                $scope.searchTextEvent = function ($event) {
                    var keyCode = $event.which || $event.keyCode;

                    if (keyCode === 13) {
                        if ($scope.searchInput.length > 1) {
                            $scope.searchTextInputChange();
                        }
                    }
                }

                $scope.searchArrow = function () {
                    if ($scope.searchInput.length > 1) {
                        $scope.searchTextInputChange();
                    }
                }
                
                
                function updateSearchInput() {

                    var searchReturn = getSearchValueByIndex($scope.highlightIndex);
                    if (searchReturn) {
                        $scope.returnedSearchVal = {
                            text: '—' + searchReturn.PlaceHolder1 + ' — ' + searchReturn.PlaceHolder3,
                            section: searchReturn.Section                            
                        }
                    }
                    else {
                        $scope.returnedSearchVal = null;
                    }
                }

                function openUrlByEnterKey() {
                    var searchReturn = getSearchValueByIndex($scope.highlightIndex);

                    if (searchReturn) {
                        $window.open('/' + searchReturn.Url, '_self');
                        $rootScope.$emit('CANCEL', "cancel");
                    }
                }

                function getSearchValueByIndex(index) {
                    var currentVal;
                    for (var property in $scope.searchResult) {
                        var found = _.find($scope.searchResult[property], function (val) { return val.highlightIndex === index });

                        if (found)
                            return found
                    }
                }
            }],
            scope: {},
            restrict: 'E',
            templateUrl: 'global-search-container.html'
        }

        return directive;

        function link(scope, element, attrs) {}

        function controller($scope) {                 
        }
    }

    //individual module search
    function moduleSearchResult(globalSearchServices, $rootScope, $window) {
        var directive = {
            link: link,
            controller: ['$scope', function ($scope) {
                $scope.quantity = GlobalSearch.SearchLimit();

                $scope.openUrl = function (url) {
                    //clear admin session and then do the following
                    $.ajax({
                        url: '/Account/ClearSessionForAdminLogin',
                        dataType: 'json',
                        type: 'POST',                        
                        //data: [{ name: 'vId', value: 1 }],
                        success: function (result) {
                            $window.open('/' + url, '_self');
                            $rootScope.$emit('CANCEL', "cancel");
                        }
                    });
                    
                }

                $scope.limitChar = function (val, length) {
                    return (val.length > length) ? val.substring(0, length) + '...' : val;
                }
                
                //OTSTM2-699
                var userAgent = $window.navigator.userAgent;
                $scope.isChrome55Above = false;
                if (userAgent.indexOf("Chrome") != -1) {
                    var versionString = userAgent.substring(userAgent.indexOf("Chrome") + 7, userAgent.indexOf("Chrome") + 9);
                    var versionNumber = parseInt(versionString);
                    if (versionNumber > 54) {
                        $scope.isChrome55Above = true; //for the Chrome version is 55.x.xxxx and above
                    }
                    else {
                        $scope.isChrome55Above = false; //for the Chrome version is 54.x.xxxx and lower
                    }                
                }
                else {                   
                    $scope.isChrome55Above = false; //for any other browsers
                }
            }],
            scope: {
                moduleResult: '=',
                moduleType: '=',
                highlightIndex: '='
            },
            restrict: 'E',
            templateUrl: 'module-type-filtered-result'
        }
        return directive;

        function link(scope, element, attrs) { }
    }

    //keyEvent
    function keyEvent() {
        var directive = {
            link: link,
            scope: false,
            restrict: 'A'
        }
        return directive

        function link(scope, elem) {            
            elem.bind('keydown', function (event) {
                scope.keydown = scope.keyEventCall(event.keyCode);
            });
        }
    }

    //dynamic input resize
    function dynamicInputResize() {
        var directive = {
            link: link,
            scope: {
                searchValue: '=',
                highlightValue: '=',
                highlightIndex: '='
            },
            restrict: 'A'
        }
        return directive
        
        function link(scope, elem) {

            function InputObj(inputSelector, initialWidth) {
                this.input = angular.element(document.querySelector(inputSelector));
                this.input.css('width', initialWidth);
            }

            InputObj.prototype.changeWidth = function (width) {
                this.input.css('width', width);
            }
            InputObj.prototype.calculateOffset = function (callback, val) {
                this.input.css('width', callback(this.input, val) + 'px');
            }
            InputObj.prototype.getCurrentWidth = function(){
                return this.input[0].offsetWidth;
            }
            InputObj.prototype.setFocus = function () {
                return this.input[0].focus();
            }
            
            var firstInput = new InputObj('#primarySearch', '850px');
            var secondInput = new InputObj('#secondarySearch', '0px');
            var firstContainer = new InputObj('.container-search-first', '850px');
            var secondContainer = new InputObj('.container-search-second', '0px');

            firstInput.setFocus()

            var hasPreviousSearch = function () {
                scope.$watch('searchValue', function (n, o) {
                    scope.highlightIndex = null;

                    if (n !== '') {
                        if (n.length === 1) {
                            changeInputVals('70px', '780px');
                            scope.highlightValue = null
                        }
                        else {
                            var firstInputWidth = ((scope.searchValue.length + 15) * 10) + 'px';
                            var secondInputWidth = (850 - ((scope.searchValue.length + 15) * 10)) + 'px'
                            if (parseInt(firstInputWidth) + parseInt(secondInputWidth) === 850) {
                                changeInputVals(firstInputWidth, secondInputWidth);
                            }
                            else {
                                changeInputVals('850px', '0px');
                            }
                        }
                    }
                    else {
                        changeInputVals('850px', '0px');
                        scope.highlightValue = null
                    }
                });
            }

            function changeInputVals(leftCss, rightCss) {
                firstInput.changeWidth(leftCss);
                secondInput.changeWidth(rightCss);
                firstContainer.changeWidth(leftCss);
                secondContainer.changeWidth(rightCss);
            }

            function add(input, val) {
                var offset = 0;
                offset = input[0].offsetWidth + val
                return offset;
            }

            function minus(input, val) {
                var offset = 0;
                offset = input[0].offsetWidth - val
                return offset;
            }

            function checkWidth() {
                return (firstInput.getCurrentWidth() + secondInput.getCurrentWidth() === 850)
            }

            hasPreviousSearch()            
        }
    }

    //icons
    function iconsBySection() {
        var directive = {
            controller: ['$scope', controller],
            scope: {
                section: '='
            },
            restrict: 'E',
            templateUrl: 'icon-section'
        }
        
        return directive
        
        function controller($scope) {
            $scope.iconClass = ''

            $scope.$watch('section', function (section) {                
                switch (section) {
                    case "Applications":
                        $scope.iconClass = 'glyphicon glyphicon-edit transition-class';
                        break;
                    case "Claims":
                        $scope.iconClass = 'fa fa-folder-open';
                        break;
                    case "Transactions":
                        $scope.iconClass = 'fa fa-exchange'
                        break;
                    case "Remittances":
                        $scope.iconClass = 'fa fa-folder-open-o'
                        break;
                    case "Users":
                        $scope.iconClass = 'fa fa-users'
                        break;
                    case "Reports":
                        $scope.iconClass = 'glyphicon glyphicon-th transition-class'
                        break;
                    case "FinancialIntegration":
                        $scope.iconClass = 'glyphicon glyphicon-usd transition-class'
                        break;
                    case "RetailConnection":
                        $scope.iconClass = 'glyphicon glyphicon-credit-card transition-class'
                        break;
                }
            });            
        }
    }
    function iconsByStatus() {
        var directive = {
            controller: ['$scope', controller],
            scope: {
                status: '='
            },
            restrict: 'E',
            templateUrl: 'icon-status'
        }
        
        return directive

        function controller($scope) {
            $scope.iconClass = ''

            $scope.$watch('status', function (status) {
                switch (status) {
                    case "Open":
                        $scope.iconClass = 'global-search-bluedot';
                        break;
                    case "Under Review":
                    case "UnderReview":
                    case "Incomplete":
                        $scope.iconClass = 'global-search-yellowdot';
                        break;
                    case "Pending":
                    case "Submitted":
                    case "Locked": //OTSTM2-1098
                        $scope.iconClass = 'global-search-yellowdot'
                        break;
                    case "1":
                    case "Active": //OTSTM2-1098
                    case "Approved":
                    case "Completed":
                        $scope.iconClass = 'global-search-greendot'
                        break;
                    case "Voided":
                        $scope.iconClass = 'global-search-greydot'
                        break;
                    case "0":
                    case "Inactive": //OTSTM2-1098
                    case "Rejected":
                        $scope.iconClass = 'global-search-reddot'
                        break;                   
                }
            });
        }
    }
})(AllServices.globalSearchServices);
