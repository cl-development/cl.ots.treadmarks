﻿'use strict';

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}//IE doesn't have method "startsWith"

if (!String.prototype.includes) {
    String.prototype.includes = function (searchString) {
        return this.indexOf(searchString) > -1;
    };
}//IE doesn't have method "includes"

