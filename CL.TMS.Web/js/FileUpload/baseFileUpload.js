﻿$(function () {
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#fileUploadInput').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $("#addFilesURL").val(),
        filesContainer: $('table#filesToUploadGrid'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        downloadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var disableDropZone = Boolean($('#disableDropZone').val());
                var allowSelection = Boolean($('#allowSelection').val());
                var allowDownload = Boolean($('#allowDownload').val());
                var allowDelete = Boolean($('#allowDelete').val());
                if ((typeof $('#StatusString') != 'undefined')) {
                    allowDelete = allowDelete && (claimStatus.toLowerCase() == "open");
                }
                var isValid = Boolean(file.isValid);

                var row = $('<tr class="template-download fade">' +
                    (allowSelection ? '<td style="width:2%"><input type="checkbox" class="upload-file-selection" checked/></td>' : '') +
                    '<td style="width:10%"><p class="upload-file-name"></p>' +
                     (file.error || !isValid ? '<div class="upload-file-error"><span class="label label-danger">Error</span>&nbsp;</div>' : '') +
                    '</td>' +
                    '<td style="width:30%" ><div><input type="text" style="width:80%" class="form-control upload-file-document-name" placeholder="Description" maxlength="512"/></div></td>' +
                    '<td style="width:20%"><span class="upload-file-size"></span></td>' +
                    //'<td style="width:20%"><span class="createdOn"></span></td>' +
                    (allowDownload ? '<td style="width:5%"><a target="_blank"><input type="button" title="Download" class="btn btn-success upload-file-download-btn" style="font-weight:bold" value="Download"/></a></td>' : '') +
                    (allowDelete ? '<td style="width:5%"><button type="button" class="btn btn-danger upload-file-delete-btn" data-toggle="modal" data-target="#modalConfirmDeletionOfUploadedFile" title="Delete"><strong>X</strong></button></td>' : '') +
                    '</tr>');

                var trimmedEncodedFileName = file.encodedFileName.replace('==', '').replace('=', '');
                row.attr('id', 'uploadFileRow_' + trimmedEncodedFileName).attr("data-filename", file.name).attr('data-fileid', file.id).attr('data-fileuniquename', file.encodedFileName);

                if (disableDropZone) {
                    row.find(':input[value!="Download"]').attr('disabled', 'disabled');//Download button should remain enabled
                }                

                if (!isValid) {
                    row.addClass('has-error');
                    row.find('.upload-file-error').attr('id', 'uploadFileError_' + trimmedEncodedFileName).append(file.validationMessage);

                    if (allowSelection) {
                        var selection = row.find('.upload-file-selection');
                        selection.removeAttr('checked');
                        SetSelectionValidationMessage(selection);
                    }
                }

                if (file.error) {
                    row.find('.upload-file-name').attr('id', 'uploadFileName_' + trimmedEncodedFileName).text(file.name);
                    row.find('.upload-file-error').attr('id', 'uploadFileError_' + trimmedEncodedFileName).append(file.error);
                } else {
                    row.find('.upload-file-selection').attr('id', 'uploadFileSelection_' + trimmedEncodedFileName);
                    row.find('.upload-file-name').attr('id', 'uploadFileName_' + trimmedEncodedFileName).text(file.name);
                    row.find('.upload-file-size').attr('id', 'uploadFileSize_' + trimmedEncodedFileName).text(o.formatFileSize(file.size));
                    //row.find('.createdOn').text(file.createdOn);
                    row.find('.upload-file-document-name').attr('id', 'uploadFileDocumentName_' + trimmedEncodedFileName).val(file.description);
                    //var fileDeleteUrl = $("#deleteFilesURL").val() + "?transactionID=" + file.transactionId + "&fileId=" + file.id + "&encodedFileName=" + file.encodedFileName;
                    //row.find('.upload-file-delete-btn').attr('id', 'uploadFileDeleteBtn' + file.id).attr('data-type', file.delete_type).attr('data-url', fileDeleteUrl).attr('data-filename', file.name);

                    row.find('.upload-file-delete-btn').attr('id', 'uploadFileDeleteBtn_' + trimmedEncodedFileName).attr('data-filename', file.name);
                    row.find('.upload-file-download-btn').attr('id', 'uploadFileDownloadBtn_' + trimmedEncodedFileName).parent("a").attr("href", file.thumbnail_url);
                }
                rows = rows.add(row);
            });
            return rows;
        },
        add: function (e, data) {
            data.formData = [{ name: "TransactionID", value: $("#transactionID").val() },
                { name: "_fileUploadSectionName", value: $("#fileUploadSectionName").val() },
                { name: "bBankInfo", value: false }] //?
            data.submit();
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        completed: function (e, data) {
            $("#filesToUploadGrid").trigger('update', { data: 'addfile'});
            if (typeof data.result == 'string') {
                alert(data.result);
                console.log(data.result);
        }

        },
        fileInput: $("#fileUploadInput"),
        dropZone: $("#fileUploadDropzone"),
        autoUpload: false,
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        sequentialUploads: true,
        singleFileUploads: false,
        disableImagePreview: true,
        disableAudioPreview: true
    });

    $('#filesToUploadGrid').on("keypress", '.upload-file-document-name', function (e) {
        if (e.keyCode == 13) {
            return false; // prevent the button click from happening
        }
    });


    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    $('#filesToUploadGrid').on('change', '.upload-file-document-name', function () {
        var fileEncodedName = $(this).parents('tr').attr('data-fileuniquename');
        var description = $(this).val();
        AddUploadedFileDescription(fileEncodedName, description);
    });

    $('#modalConfirmDeletionOfUploadedFile').on('show.bs.modal', function (e) {
        $('#modalConfirmDeletionOfUploadedFileTargetId').val('');
        $('#modalConfirmDeletionOfUploadedFileTargetId').val(e.relatedTarget.id);

        $('#confirmFileNameToDelete').empty();
        var fileName = e.relatedTarget.getAttribute('data-filename');
        $('#confirmFileNameToDelete').html(fileName);
    });

    $('#modalConfirmDeletionOfUploadedFile').on("click", '#modalConfirmDeletionOfUploadedFileYesBtn', function (e) {

        var targetId = $('#modalConfirmDeletionOfUploadedFileTargetId').val();
        var target = $('#' + targetId);
        var targetParent = target.parents('tr');

        var transactionId = $('#transactionID').val();
        var fileUniqueName = targetParent.attr('data-fileuniquename');

        $('#' + targetId).attr('disabled', 'disabled');

        $.ajax({
            url: $('#deleteFilesURL').val(),
            method: "GET",
            data: [{ name: 'transactionId', value: transactionId }, { name: 'encodedFileName', value: fileUniqueName }, { name: "bBankInfo", value: false }], //?
            dataType: 'json',
            success: function () {
                targetParent.remove();
                $('#modalConfirmDeletionOfUploadedFile').modal('hide');//UI hangs for slow server response, moved this line to before ajax call                
                $("#filesToUploadGrid").trigger('update', { data: 'deletefile' });
            },
            fail: function (jqXHR, textStatus, error) {
                $('#' + targetId).removeAttr('disabled'); //?
                //window.location.reload();
            },
        });
    });

    $('#fileUploadInput').on('change', '.upload-file-selection', function () {
        var validationMessage = "Incorrect file";
        var fileEncodedName = $(this).parents('tr').attr('data-fileuniquename');

        $.ajax({
            url: "/Default/SetUploadedFileValidation",
            method: "POST",
            data: [{ name: "fileEncodedName", value: fileEncodedName }, { name: "isValid", value: $(this).is(':checked') }, { name: "message", value: ($(this).is(':checked') ? '' : validationMessage) }],
            dataType: "json"
        });

        SetSelectionValidationMessage($(this));
    });

    GetUploadedFiles();

    var claimStatus = $('#StatusString').val();

});

function AddUploadedFileDescription(fileEncodedName, description) {
    if (fileEncodedName != "") {
        $.ajax({
            url: "/System/FileUpload/UpdateAttachmentDescription",
            method: "POST",
            data: [{ name: "fileEncodedName", value: fileEncodedName }, { name: "description", value: description }],
            dataType: "json"
        });
    }
}

function SetSelectionValidationMessage(element) {
    if (!element.is(':checked')) {
        var fileName = element.parents('tr').attr('data-filename');
        var validationMessage = "Incorrect file";

        element.attr('data-uploadedFile-validation-message', validationMessage + ' (' + fileName + ')');
    }
    else {
        element.removeAttr('data-uploadedFile-validation-message');
    }
}

function GetUploadedFiles() {
    $('#fileUploadInput').addClass('fileupload-processing');
    $.ajax({
        url: $("#getFilesURL").val(),
        method: "GET",
        data: [{ name: "TransactionID", value: $("#transactionID").val() }, { name: "bBankInfo", value: false }], //?
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $('#fileUploadInput').fileupload('option', 'done')
            .call('#fileUploadInput', $.Event('done'), { result: result });
    });
}