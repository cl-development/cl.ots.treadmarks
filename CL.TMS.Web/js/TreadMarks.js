﻿$(document).ready(function () {

    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        $('#ApplicationHaulerID :input').eq(0).trigger('change');
    });

    $('#ApplicationHaulerID :input').on('change', function () {
        var apphauler = $(this);
        $.when(
        apphauler.focusout()).then(function () {
            certificateOfApproval();
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });

    $('#ApplicationHaulerID').on('change', '.ctemp', function () {
        var ctemp = $(this);
        $.when(
        ctemp.focusout()).then(function () {
            certificateOfApproval();
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });

    var processJson = function () {

        var data = $('form').serializeArray();

        $('input[type="checkbox"]').each(function () {

            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-att-chkbox') != 'undefined')) {

                if (!this.checked) {
                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {

            if (!_isEmpty($(this).val()) && (typeof $(this).attr('name') != 'undefined')) {
                if (this.checked) {
                    data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('name') + ':' + $(this).attr('data-myattri') + ':' + 'true' + ':' + $(this).attr('value') });
                }
                else {
                    data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('name') + ':' + $(this).attr('data-myattri') + ':' + 'false' + ':' + $(this).attr('value') });
                }
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }

        //remove leading and trailing space
        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }


        $.ajax({
            url: '/ApplicationHauler/HaulerApplicationResults',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (result) {
                if(result != 'Success')
                    console.log('Failuer in Auto Save: ' + result);
            },
            error: function (result) {
            }
        });
    }
    
    var certificateOfApproval = function () {
        var allSortYards = 0;
        var capp = $("input[data-maxstorage]");
        capp.each(function () {
            
            $(this).attr('data-maxstorage', $(this).val());

            if (parseFloat($(this).attr('data-maxstorage')) > 0) {
                allSortYards++;
            }
            if (parseFloat($(this).attr('data-maxstorage')) < 50) {
                var certApp = $(this).parents('.row').find('.COA');
                $(certApp).find('input:text').each(function () {
                    $(this).val('');
                });
                $(this).parents('.row').find('.COA').hide();
            }
            else {
                $(this).parents('.row').find('.COA').show();
            }
        });
        $('#SortYardCount').text(allSortYards);
    }

    var totalStorage = function () {
        var maxStorageCap = 0;
        var inputs = $("input[data-maxstorage]");
        inputs.each(function () {
            if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
                maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
            }
        });
        if (!isNaN(parseFloat(maxStorageCap))) {
            $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
        }
    }

    //var contactAddressSameAs = function () {
    //    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
    //        $(this).closest('.row').next('.contactAddressDifferent').show();
    //    });
    //    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
    //        $(this).closest('.row').next('.contactAddressDifferent').hide();
    //        $(this).closest('.row').next('.contactAddressDifferent').find('input').val('');
    //        $(this).closest('.row').next('.contactAddressDifferent').find('input:checkbox').removeAttr('checked')
    //    });
    //}
    var contactAddressSameAs = function () {
        $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
            $(this).closest('.row').next('.contactAddressDifferent').hide();
            $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
        });
        $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
            $(this).closest('.row').next('.contactAddressDifferent').show();
            //$(this).closest('.row').next('.contactAddressDifferent').find('input:checkbox').removeAttr('checked')
        });
    }
    contactAddressSameAs();
});

function _isEmpty(value) {
    return (value == null || value.length === 0);
}














