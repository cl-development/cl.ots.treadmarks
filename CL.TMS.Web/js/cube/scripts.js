$(function($) {
	setTimeout(function() {
		$('#content-wrapper > .row').css({
			opacity: 1
		});
	}, 200);
	
	$('#sidebar-nav,#nav-col-submenu').on('click', '.dropdown-toggle', function (e) {
		e.preventDefault();
		
		var $item = $(this).parent();

		if (!$item.hasClass('open')) {
			$item.parent().find('.open .submenu').slideUp('fast');
			$item.parent().find('.open').toggleClass('open');
		}
		
		$item.toggleClass('open');
		
		if ($item.hasClass('open')) {
			$item.children('.submenu').slideDown('fast');
		} 
		else {
			$item.children('.submenu').slideUp('fast');
		}
	});
	
	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav .dropdown-toggle', function (e) {
		if ($( document ).width() >= 992) {
			var $item = $(this).parent();

			if ($('body').hasClass('fixed-leftmenu')) {
				var topPosition = $item.position().top;

				if ((topPosition + 4*$(this).outerHeight()) >= $(window).height()) {
					topPosition -= 6*$(this).outerHeight();
				}

				$('#nav-col-submenu').html($item.children('.submenu').clone());
				$('#nav-col-submenu > .submenu').css({'top' : topPosition});
			}

			$item.addClass('open');
			$item.children('.submenu').slideDown('fast');
		}
	});
	
	$('body').on('mouseleave', '#page-wrapper.nav-small #sidebar-nav > .nav-pills > li', function (e) {
		if ($( document ).width() >= 992) {
			var $item = $(this);
	
			if ($item.hasClass('open')) {
				$item.find('.open .submenu').slideUp('fast');
				$item.find('.open').removeClass('open');
				$item.children('.submenu').slideUp('fast');
			}
			
			$item.removeClass('open');
		}
	});
	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav a:not(.dropdown-toggle)', function (e) {
		if ($('body').hasClass('fixed-leftmenu')) {
			$('#nav-col-submenu').html('');
		}
	});
	$('body').on('mouseleave', '#page-wrapper.nav-small #nav-col', function (e) {
		if ($('body').hasClass('fixed-leftmenu')) {
			$('#nav-col-submenu').html('');
		}
	});
	
    //==================OTSTM2-865 hamburger button clicking event, region start =====================
	var firstClick = true; //when screen is smaller than 768, class "collapse" means display:none, so need a variable "firstClick" to make sure opening menu when clicking button at the first time 
	$('#make-small-nav').click(function (e) {
        
	    if (matchMedia('(min-width: 992px)').matches) {

	        $('#page-wrapper').toggleClass('nav-small');

	        //top info bar behaviour when clicking hamburger button
	        if ($('#fixedClaimHeader').hasClass('fixed-claim-header')) {
	            $('#fixedClaimHeader').removeClass("fixed-claim-header");
	            $('#fixedClaimHeader').addClass('fixed-claim-header-small');
	        }
	        else {
	            $('#fixedClaimHeader').addClass("fixed-claim-header");
	            $('#fixedClaimHeader').removeClass('fixed-claim-header-small');
	        }	        

            //make sure the position of icons when opening or collapsing the side menu
	        if ($('#page-wrapper').hasClass("nav-small")) {
	            $("#sidebar-nav .nav > li > a").css("width", "65px");
	            $("#sidebar-nav > ul > li > a > i.fa-pie-chart").css("margin-left", "0");
	            $("#sidebar-nav > ul > li > a > i.fa-thermometer-half").css("margin-left", "0");
	            $("#sidebar-nav > ul > li > a > i.fa-sliders").css("margin-left", "0");
	            $("#sidebar-nav > ul > li > a > i.fa-building-o").css("margin-left", "0");
	            $("#sidebar-nav > ul > li > a > i.fa-file-text-o").css("margin-left", "0");
	        }
	        else {
	            $("#sidebar-nav .nav > li > a").css("width", "100%");
	            $("#sidebar-nav > ul > li > a > i.fa-pie-chart").css("margin-left", "2px");
	            $("#sidebar-nav > ul > li > a > i.fa-thermometer-half").css("margin-left", "2.5px");
                $("#sidebar-nav > ul > li > a > i.fa-sliders").css("margin-left", "1px");
	            $("#sidebar-nav > ul > li > a > i.fa-building-o").css("margin-left", "3px");
	            $("#sidebar-nav > ul > li > a > i.fa-file-text-o").css("margin-left", "2.1px");
	        }
	    }
	    else {
	        if (matchMedia('(min-width: 768px)').matches) {
                if ($('.navbar-ex1-collapse').hasClass('collapse')) {
	                $('.navbar-ex1-collapse').removeClass('collapse');
	                if (!$('.navbar-ex1-collapse').hasClass('nav-none')) {
	                    $('.navbar-ex1-collapse').addClass('nav-none');
	                }
	            }
	            else {
	                $('.navbar-ex1-collapse').addClass('collapse');
	                $('.navbar-ex1-collapse').removeClass('nav-none');
                }
                
	            //top info bar behaviour when clicking hamburger button
                if ($('#fixedClaimHeader').hasClass("fixed-claim-header-fullscreen")) {
                    $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
                    $('#fixedClaimHeader').addClass('fixed-claim-header');
                }
                else {
                    $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
                    $('#fixedClaimHeader').removeClass('fixed-claim-header');
                }
	        }
	        else {
	            $('.navbar-ex1-collapse').toggleClass('collapse');
	            $('.navbar-ex1-collapse').removeClass('nav-none');
	            if (firstClick) {
	                $('.navbar-ex1-collapse').removeClass('collapse');
	                firstClick = false;
	            }

	            //top info bar behaviour when clicking hamburger button
	            if ($('#fixedClaimHeader').hasClass("fixed-claim-header-fullscreen")) {
	                $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
	                $('#fixedClaimHeader').addClass('fixed-claim-header');
	            }
	            else if ($('#fixedClaimHeader').hasClass("fixed-claim-header-fullscreen-xxs")) {
	                $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen-xxs");
	                $('#fixedClaimHeader').addClass('fixed-claim-header');
	            }
	            else {
	                var header = $("#header-navbar"); 
	                if ($(header)["0"].clientHeight == 100) {
                        $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen-xxs");
	                    $('#fixedClaimHeader').removeClass('fixed-claim-header');
	                }
	                else {
                        $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
	                    $('#fixedClaimHeader').removeClass('fixed-claim-header');
	                }	                
	            }	            
	        }

            //width change automatically for claim status
	        if ($('.fixed-claim-header-claimstatus').length && $('.fixed-claim-header-claimdate').length && $('.fixed-claim-header-period-btn-group').length) {
	            var autoWidth = $('.fixed-claim-header-claimstatus').parent()["0"].clientWidth -$('.fixed-claim-header-claimdate')["0"].clientWidth - $('.fixed-claim-header-period-btn-group')["0"].clientWidth - 20;
	            $('.fixed-claim-header-claimstatus').css("width", autoWidth);
	        }
	    }
	});
    //==================OTSTM2-865 hamburger button clicking event, region end =====================

    //================OTSTM2-865 page load event, region start===============
	//the height of header may be one line (50px) or two lines (100px) based on the size of screen, the followed code make sure the correct position of opened menu and page content
	var header = $("#header-navbar"); 
	if ($(header)["0"].clientHeight == 100) {
	    $("#content-wrapper").css("margin-top", "50px");
	    $('.navbar-ex1-collapse.profile2-dropdown').css("margin-top", "50px");
	    $("#logo-separator").css("margin-left", ""); //make sure the separator after logo be at the correct position if header has 2 lines
        
	    //top info bar behaviour when loading page
	    $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
	    $('#fixedClaimHeader').addClass('fixed-claim-header-fullscreen-xxs');
	}
	else {
	    $("#content-wrapper").css("margin-top", "0px");
	    $('.navbar-ex1-collapse').css("margin-top", "0px");

	    //top info bar behaviour when loading page
	    $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
	    $('#fixedClaimHeader').removeClass('fixed-claim-header-fullscreen-xxs');
	}
    
    //some UI fix if the screen is smaller than 767
	if (matchMedia('(max-width: 767px)').matches) { 
	    $("#logo-separator").css("margin-left", ""); //make sure the separator after logo be at the correct position 
	    $("#innerDivLayout").css("top", "-1px").css("width", "62px");  //remove the white line between header and search button area (Layout)
	    $("#innerDivAdminLayout").css("top", "-1px").css("width", "62px"); //remove the white line between header and search button area (AdminLayout)     

	    //width change automatically for claim status
	    if ($('.fixed-claim-header-claimstatus').length && $('.fixed-claim-header-claimdate').length && $('.fixed-claim-header-period-btn-group').length) {
            var autoWidth = $('.fixed-claim-header-claimstatus').parent()["0"].clientWidth -$('.fixed-claim-header-claimdate')["0"].clientWidth -$('.fixed-claim-header-period-btn-group')["0"].clientWidth -20;
		    $('.fixed-claim-header-claimstatus').css("width", autoWidth);
		}
	}
	else {
	    $("#logo-separator").css("margin-left", "-30px");
	}

    //make sure the alignment of search area and hamburger button
	if (matchMedia('(max-width: 991px)').matches) {
	    $("#innerDivLayout").css("width", "65px"); //Layout
	    $("#innerDivAdminLayout").css("width", "65px"); //AdminLayout
	    
	    $('.navbar-ex1-collapse').removeClass('collapse'); //collapse the side menu
	    $('.navbar-ex1-collapse').addClass('nav-none'); //collapse the side menu
        
	    //AdminLayout, make sure "close button" at correct positon
	    $("#AdminMenuCloseBtn").removeClass("col-xs-2");
	    $("#AdminMenuCloseBtn").children().css("position", "absolute").css("left", "165px").css("top","5px");	    
	}
	else { //top info bar behaviour when loading page
	    if ($('#fixedClaimHeader').hasClass("fixed-claim-header-fullscreen")) {
	        $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
	        $('#fixedClaimHeader').addClass('fixed-claim-header');
	    }
	}
	
    //===============OTSTM2-865 page load event,  region end===============
	
    //===========OTSTM2-865 screen resizing event,  region start===============
	$(window).smartresize(function () {
	    //make sure search area alignment when screen zoom in or out	    
	    var rightOffset = $("#col-left-inner")["0"].offsetWidth - $("#col-left-inner")["0"].clientWidth;
	    $("#col-left-inner").css("right", "-" + rightOffset + "px");
        
	    //the height of header may be one line (50px) or two lines (100px) based on the size of screen, the followed code make sure the correct position of opened menu and page content
	    var header = $("#header-navbar"); 
	    if ($(header)["0"].clientHeight == 100) {
	        $("#content-wrapper").css("margin-top", "50px");
	        $('.navbar-ex1-collapse.profile2-dropdown').css("margin-top", "50px");
	        $("#logo-separator").css("margin-left", ""); //make sure the separator after logo be at the correct position if header has 2 lines

	        $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
	        $('#fixedClaimHeader').addClass('fixed-claim-header-fullscreen-xxs');
	    }
	    else {
	        $("#content-wrapper").css("margin-top", "0px");
	        $('.navbar-ex1-collapse').css("margin-top", "0px");

	        $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
	        $('#fixedClaimHeader').removeClass('fixed-claim-header-fullscreen-xxs');
	    }
       
        //hide the left menu when resizing the screen (make sure all components are at the correct position)
	    if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
	        $("#sidebar-nav .nav > li > a").css("width", "100%"); //make sure width is always 100% when resizing screen to smaller screen
		    $('#page-wrapper').removeClass('nav-small');
		    $('.navbar-ex1-collapse').removeClass('collapse'); //collapse the side menu
		    $("#innerDivLayout").css("width", "65px");  //make sure the alignment of search area and hamburger button (Layout)
		    $("#innerDivAdminLayout").css("width", "65px");  //make sure the alignment of search area and hamburger button (AdminLayout)

	        //AdminLayout, make sure "close button" at correct positon
		    $("#AdminMenuCloseBtn").removeClass("col-xs-2");
		    $("#AdminMenuCloseBtn").children().css("position", "absolute").css("left", "165px").css("top", "5px");

		    if (!$('.navbar-ex1-collapse').hasClass('nav-none')) {
		        $('.navbar-ex1-collapse').addClass('nav-none');
		    }

		    $("#logo-separator").css("margin-left", "-30px");

            //top info bar behaviour when resizing screen
		    if ($('#fixedClaimHeader').hasClass('fixed-claim-header-small')) {
		        $('#fixedClaimHeader').removeClass('fixed-claim-header-small');
		        $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
		    }
		    else {
		        $('#fixedClaimHeader').removeClass('fixed-claim-header');
		        $('#fixedClaimHeader').addClass("fixed-claim-header-fullscreen");
		    }

	        //remove width setting automatically for claim status
		    if ($('.fixed-claim-header-claimstatus').length) {
                $('.fixed-claim-header-claimstatus').css("width", "");
		    }
            
	        //width change automatically for claim status
		    if ($('.fixed-claim-header-claimstatus').length && $('.fixed-claim-header-claimdate').length && $('.fixed-claim-header-period-btn-group').length) {
		        var autoWidth = $('.fixed-claim-header-claimstatus').parent()["0"].clientWidth - $('.fixed-claim-header-claimdate')["0"].clientWidth - $('.fixed-claim-header-period-btn-group')["0"].clientWidth - 20;
		        $('.fixed-claim-header-claimstatus').css("width", autoWidth);
		    }
	    }
	    else if (matchMedia('(max-width: 767px)').matches) {
            $("#logo-separator").css("margin-left", ""); //make sure the separator after logo be at the correct position
		    $("#innerDivLayout").css("top", "-1px"); // remove the white line between header and search button area (Layout)
		    $("#innerDivAdminLayout").css("top", "-1px"); //remove the white line between header and search button area (AdminLayout)
		    $("#innerDivLayout").css("width", "62px"); //make sure the alignment of search area and hamburger button (Layout)
		    $("#innerDivAdminLayout").css("width", "62px"); //make sure the alignment of search area and hamburger button (AdminLayout)
		    $('.navbar-ex1-collapse').addClass('collapse'); //collapse the side menu

	        //width change automatically for claim status
		    if ($('.fixed-claim-header-claimstatus').length && $('.fixed-claim-header-claimdate').length && $('.fixed-claim-header-period-btn-group').length) {
                var autoWidth = $('.fixed-claim-header-claimstatus').parent()["0"].clientWidth - $('.fixed-claim-header-claimdate')["0"].clientWidth - $('.fixed-claim-header-period-btn-group')["0"].clientWidth - 20;
		        $('.fixed-claim-header-claimstatus').css("width", autoWidth);
		    }
            
	    }		    
	    else {
	        $("#logo-separator").css("margin-left", "-30px");
	        $("#innerDivLayout").css("width", "64.77px");  //make sure the alignment of search area and hamburger button (Layout)
	        $("#innerDivAdminLayout").css("width", "64.77px");  //make sure the alignment of search area and hamburger button (AdminLayout)
	        $('.navbar-ex1-collapse').removeClass('nav-none');
	        //make sure the position of icons when opening or collapsing the side menu
	        if ($('#page-wrapper').hasClass("nav-small")) {
	            $("#sidebar-nav .nav > li > a").css("width", "65px");
	            //$("#sidebar-nav > ul > li > a > i.fa-thermometer-half").css("margin", "inherit auto");
	        }
	        else {
	            $("#sidebar-nav .nav > li > a").css("width", "100%");
	            //$("#sidebar-nav > ul > li > a > i.fa-thermometer-half").css("margin-left", "5px");
	        }

	        $('#fixedClaimHeader').removeClass("fixed-claim-header-fullscreen");
	        //make sure correct class apply to the top info bar when resizing screen(larger than 991)
	        if ($('#page-wrapper').hasClass("nav-small")) {
	            $('#fixedClaimHeader').addClass('fixed-claim-header-small');
	        }
	        else {
	            $('#fixedClaimHeader').addClass('fixed-claim-header');
	        }

	        //width change automatically for claim status
		    if ($('.fixed-claim-header-claimstatus').length && $('.fixed-claim-header-claimdate').length && $('.fixed-claim-header-period-btn-group').length) {
                var autoWidth = $('.fixed-claim-header-claimstatus').parent()["0"].clientWidth -$('.fixed-claim-header-claimdate')["0"].clientWidth -$('.fixed-claim-header-period-btn-group')["0"].clientWidth - 20;
		        $('.fixed-claim-header-claimstatus').css("width", autoWidth);
		    }
	    }

	    firstClick = true; // every time when resizing screen, the variable "firstClick" will be reset to true
	});
    //=============OTSTM2-865 screen resizing event, region end==========
	
	$('.mobile-search').click(function(e) {
		e.preventDefault();
		
		$('.mobile-search').addClass('active');
		$('.mobile-search form input.form-control').focus();
	});
	$(document).mouseup(function (e) {
		var container = $('.mobile-search');

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeClass('active');
		}
	});
	
	//$('.fixed-leftmenu #col-left').nanoScroller({
    //	alwaysVisible: false,
    //	iOSNativeScrolling: false,
    //	preventPageScrolling: true,
    //	contentClass: 'col-left-nano-content'
    //});
	
	// build all tooltips from data-attributes
	$("[data-toggle='tooltip']").each(function (index, el) {
		$(el).tooltip({
			placement: $(this).data("placement") || 'top'
		});
	});
});

$.fn.removeClassPrefix = function(prefix) {
    this.each(function(i, el) {
        var classes = el.className.split(" ").filter(function(c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        });
        el.className = classes.join(" ");
    });
    return this;
};

(function($,sr){
	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce = function (func, threshold, execAsap) {
		var timeout;

		return function debounced () {
			var obj = this, args = arguments;
			function delayed () {
				if (!execAsap)
					func.apply(obj, args);
				timeout = null;
			};

			if (timeout)
				clearTimeout(timeout);
			else if (execAsap)
				func.apply(obj, args);

			timeout = setTimeout(delayed, threshold || 100);
		};
	}
	// smartresize 
	jQuery.fn[sr] = function(fn){	return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery, 'smartresize');

$(window).on('load', function () {
    if ($('.fixed-claim-header-claimstatus').length) {
        if (matchMedia('(max-width: 1199px)').matches && matchMedia('(min-width: 992px)').matches) {
            $('.fixed-claim-header-claimstatus').css("width", "60%");
        }
        else if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
            $('.fixed-claim-header-claimstatus').css("width", "65%");
        }
        else if (matchMedia('(max-width: 767px)').matches) {
            $('.fixed-claim-header-claimstatus').css("width", "50%");
        }
        else {
            $('.fixed-claim-header-claimstatus').css("width", "75%");
        }
    }
});