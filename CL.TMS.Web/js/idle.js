﻿var _API_JQUERY = 1;
var _API_PROTOTYPE = 2;
var _api;

var _idleTimeout = 30000;       // 30 seconds
var _awayTimeout = 600000;      // 10 minutes
var _currentCounting = 1;

var _idleNow = false;
var _idleTimestamp = null;
var _idleTimer = null;
var _awayNow = false;
var _awayTimestamp = null;
var _awayTimer = null;
var _wakeupTime = new Date().getTime();//time of the latest moment current Tab keep awake.
var _lastLogTime = new Date().getTime();//the latest time update localStorage , happens every 20 seconds for better performance.
var timeInterval;

function setIdleTimeout(ms)
{
    _idleTimeout = ms;
    console.log('setIdleTimeout _idleTimeout start in seconds:', _idleTimeout / 1000);
    _idleTimestamp = new Date().getTime() + ms;
    if (_idleTimer != null) {
        clearTimeout (_idleTimer);
    }
    _idleTimer = setTimeout(_makeIdle, ms + 50);
}

function setAwayTimeout(ms)
{
    _awayTimeout = ms;
    console.log('setAwayTimeout _awayTimeout away in ' + _awayTimeout / 1000);
    _awayTimestamp = new Date().getTime() + ms;
    if (_awayTimer != null) {
        clearTimeout (_awayTimer);
    }
    _awayTimer = setTimeout(_makeAway, ms + 50);
}

function _makeIdle()
{
    var currentTime = new Date().getTime();
    if (currentTime < _idleTimestamp) {
        console.log('Not idle yet. Idle in ' + (_idleTimestamp - currentTime + 50));
        _idleTimer = setTimeout(_makeIdle, _idleTimestamp - currentTime + 50);
        return;
    }
    console.log('** IDLE **');
    idleCountDownStart('Log out in seconds...');
    _idleNow = true;

    try {
        if (document.onIdle) document.onIdle();
    } catch (err) {
    }
}

function _makeAway()
{
    var currentTime = new Date().getTime();
    if (currentTime < _awayTimestamp) {
        //console.log('Not away yet. Away in ' + (_awayTimestamp - currentTime + 50));
        _awayTimer = setTimeout(_makeAway, _awayTimestamp - currentTime + 50);
        return;
    }
    console.log('** AWAY **');
    _awayNow = true;
    try {
        //window.location.replace(Global.Layout.LogoutUserUrl);
        //window.location.replace('/Account/Logout');

        if (document.onAway) document.onAway();
    } catch (err) {
    }
}

function _initPrototype()
{
    _api = _API_PROTOTYPE;
}

function _active(event)
{
    countingdownstop();
    clearInterval(timeInterval);

    var currentTime = new Date().getTime();
    _wakeupTime = currentTime;
    _idleTimestamp = currentTime + _idleTimeout;
    _awayTimestamp = currentTime + _awayTimeout;

    if (currentTime - _lastLogTime > 20000) {//after 20 seconds since last update localStorage 
        localStorage.setItem("ots_session_active_time", _idleTimestamp);//all Tabs look at this value to know current log out time
        _lastLogTime = currentTime;
        console.log('more than 20 secs.');
    }

    //console.log('not idle.');
    //console.log(event.timeStamp, '_currentCounting', _currentCounting,currentTime, + ' _currentCounting - event.timeStamp:', _currentCounting - event.timeStamp);

    //_currentCounting = event.timeStamp;

    if (_idleNow) {
        setIdleTimeout(_idleTimeout);
    }

    if (_awayNow) {
        setAwayTimeout(_awayTimeout);
    }

    try {
        //console.log('** BACK **');
        if ((_idleNow || _awayNow) && document.onBack) {
            document.onBack(_idleNow, _awayNow);
        }
    } catch (err) {
    }

    _idleNow = false;
    _awayNow = false;
}

window.addEventListener('storage', function (e) {
    //localStorage.clear() will not trigger this event
    if ((e.storageArea === localStorage ) && (e.key === 'ots_session_active_time')) {
        _idleTimestamp = localStorage.getItem("ots_session_active_time");
        _active(null);//active current Tab
        console.log(_idleTimestamp + ' some one knock me, e:', e);
    }
    if (!e.newValue) {//localStorage.clear()
        alert('delete value');
    }
});

function _initJQuery()
{
    _api = _API_JQUERY;
    var doc = $(document);
    doc.ready(function(){
        doc.mousemove(_active);
        try {
            doc.mouseenter(_active);
        } catch (err) { }
        try {
            doc.scroll(_active);
        } catch (err) { }
        try {
            doc.keydown(_active);
        } catch (err) { }
        try {
            doc.click(_active);
        } catch (err) { }
        try {
            doc.dblclick(_active);
        } catch (err) { }
    });
}

function _initPrototype()
{
    _api = _API_PROTOTYPE;
    var doc = $(document);
    Event.observe (window, 'load', function(event) {
        Event.observe(window, 'click', _active);
        Event.observe(window, 'mousemove', _active);
        Event.observe(window, 'mouseenter', _active);
        Event.observe(window, 'scroll', _active);
        Event.observe(window, 'keydown', _active);
        Event.observe(window, 'dblclick', _active);
    });
}

// Detect the API
try {
    if (Prototype) _initPrototype();
} catch (err) { }

try {
    if (jQuery) _initJQuery();
} catch (err) { }


function idleCountDownStart(text) {
    var pathname = window.location.pathname; // Returns path only
    var iStart = 29;//29 seconds
    var countdowntext = text + ' ' + iStart;

    timeInterval = setInterval(function () {
        console.log('iStart:', iStart);
        if (iStart == 0) {
            alert('log out now...');
        }
        countdowntext = text + ' ' + iStart;
        iStart--;
        $('#countdowntext').text(countdowntext);
    }, 1000);

    if ($('body').find('#countdown').attr('id') != 'countdown') {
        //$('body').append('<div id="countdown" ><div class="wraptocenter"><img id="countdownimg" src="../Images/10secCountdown.gif"><div id="countdowntext" >' + text + '</div></div><div class="bg"></div></div>');
        $('body').append('<div id="countdown" ><div class="wraptocenter"><div id="countdowntext" style="font-size:50px">' + text + '</div></div><div class="bg"></div></div>');
    } else {
        //$('#countdownimg').attr('src', '../Images/10secCountdown.gif');//restart from 10 count down
    }
    $('.wraptocenter').css({
        'display': 'table-cell',
        'text-align': 'center',
        'vertical-align': 'middle',
        'width': '200px',
        'height': '200px',
        'background-color': '#999',
        'font-size': '50px',
    });

    $('#countdown').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    $('#countdown .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    $('#countdown>div:first').css({
        'width': '600px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    $('#countdown .bg').height('100%');
    $('#countdown').fadeIn(300);
    $('body').css('cursor', 'wait');
}

function countingdownstop() {
    $('#countdown .bg').height('100%');
    $('#countdown').fadeOut(300);
    $('#countdownimg').attr('src', '');//reset from 10 count down
    $('body').css('cursor', 'default');
}
