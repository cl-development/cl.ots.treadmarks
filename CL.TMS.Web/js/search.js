

//maintains the search width if textbox has text in it
$('.subcomp-search input').off('keyup').on('keyup', function(){
	var s = $(this).val();
	if(s.length >= 1)
		$(this).addClass('in');
	else
		$(this).removeClass('in');		
});