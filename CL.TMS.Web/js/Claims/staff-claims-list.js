﻿(function ($) {

    $.fn.createDataTable = function (options) {
        TM = {};
        TM.settings = $.extend({
            ajaxHandler: '',
            viewMoreId: '',
            totalRecordsId: '',
            searchId: '',
            exportId: '',
            exportUrl: '',
            foundId: '',
            removeIconId: '',
            scrollY: 300,
            scrollX: true,
            serverSide: true,
            rowUrl: '',
            ColumnId: 0,
            pageSize: 8,
            columnNames: [
                { columnName: 'PeriodName' },
                { columnName: 'Status' },
                { columnName: 'Amount' },
                { columnName: 'SubmittedDate' },
                { columnName: 'ReviewDate' },
                { columnName: 'PaymentDue' },
                { columnName: 'EftNumber' },
                { columnName: 'PaymentDate' },
                { columnName: 'AssignedDate' },
                { columnName: 'AssignedTo' },
                { columnName: 'AssignedDate' },
                { columnName: 'SubmittedBy' },
                { columnName: 'ClaimId', className: 'display-none' }
            ],
        }, options);

        var table = this.DataTable({
            serverSide: TM.settings.serverSide,
            ajax: TM.settings.ajaxHandler,
            scrollY: TM.settings.scrollY,
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            scrollCollapse: true,
            createdRow: function (row, data, index) {
                $(row).addClass('cursor-pointer');
                var claimId = data.ID;
                $(row).on('click', function () {
                    if (_cancelMouseClick) {
                        _cancelMouseClick = false;
                        return;
                    }
                    window.open(TM.settings.rowUrl + '?claimId=' + claimId, '_self');
                });
            },
            processing: false,
            //"paging" : false,
            order: [[0, "desc"]],
            searching: true,
            "dom": "rtiS",
            info: false,
            deferRender: true,
            "scroller": {
                displayBuffer: 100,
                rowHeight: 70,
                serverWait: 100,
                loadingIndicator: false
            },
            columns: [
						{
						    name: "PeriodName",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.PeriodName + "</span>";
						    }
						},
						{
						    name: "Status",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.Status + "</span>";
						    }
						},
						{
						    name: "Amount",
						    data: null,
						    render: function (data, type, full, meta) {
						        //OTSTM2-423
						        return "<span>" + data.Amount.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + "</span>";
						    },
						    width:'10%',
						},
						{
						    name: "SubmittedDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.SubmittedDate != null && data.SubmittedDate !== undefined) {
						            return dateTimeConvert(data.SubmittedDate);
						        } else {
						            return '';
						        }
						    }
						},
                        {
                            name: "ReviewDate",
                            data: null,
                            render: function (data, type, full, meta) {
                                if (data.ReviewDate != null && data.ReviewDate !== undefined) {
                                    return dateTimeConvert(data.ReviewDate);
                                } else {
                                    return '';
                                }
                            }
                        },
						{
						    name: "PaymentDue",//ChequeDueDate
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.PaymentDue != null && data.PaymentDue !== undefined) {
						            return dateTimeConvert(data.PaymentDue);
						        } else {
						            return '';
						        }
						    }
						},
						{
						    name: "EftNumber",
						    data: null,
						    render: function (data, type, full, meta) {
						        return "<span>" + data.EftNumber + "</span>";
						    },
						    width:'10%',
						},
						{
						    name: "PaymentDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.PaymentDate != null && data.PaymentDate !== undefined) {
						            return dateTimeConvert(data.PaymentDate);
						        } else {
						            return '';
						        }
						    }
						},
						{
						    name: "AssignedDate",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.AssignedDate != null && data.AssignedDate !== undefined) {
						            return dateTimeConvert(data.AssignedDate);
						        } else {
						            return '';
						        }
						    }

						},
                        {
                            name: "AssignedTo",
                            data: null,
                            render: function (data, type, full, meta) {
                                return "<span>" + data.AssignedTo + "</span>";
                            }

                        },
						{
						    name: "SubmittedBy",
						    data: null,
						    render: function (data, type, full, meta) {
						        if (data.SubmittedBy.length > 15)
						            return "<span>" + data.SubmittedBy.substring(0, 13) + '...' + "</span>";
						        else
						            return "<span>" + data.SubmittedBy + "</span>";
						    }
						},
                        {
                            name: "ClaimId",
                            data: null,
                            className: 'display-none',
                            render: function (data, type, full, meta) {
                                return "<span>" + data.ID + "</span>";
                            }
                        },

            ],
            initComplete: function (settings, json) {
                $('.dataTables_scrollBody').css('overflow-y', 'hidden');

                //OTSTM2-986
                table.columns().eq(0).each(function (index) {
                    var column = table.column(index);
                    var that = column;
                    $('input', column.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            },
            drawCallback: function (settings) {
                var direction = settings.aaSorting[0][1];
                $('.sort').html('<i class="fa fa-sort"></i>');
                (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
                $(TM.settings.foundId).html('Found ' + settings.fnRecordsDisplay());
                (settings.fnDisplayEnd() >= TM.settings.pageSize) ? $(TM.settings.viewMoreId).css('visibility', 'visible') : $(TM.settings.viewMoreId).css('visibility', 'hidden');

                //OTSTM2-986
                if ($(TM.settings.viewMoreId).is(':visible')) {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                }
                else {
                    if (settings.fnDisplayEnd() >= TM.settings.pageSize) {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'scroll' });
                    }
                    else {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                    }
                }

                if ($("#searchTextPeriod").val() || $("#searchTextStatus").val() || $("#searchTextAmount").val() || $("#searchTextSubmitted").val() || $("#searchTextReviewDue").val() || $("#searchTextChequeDue").val()
                    || $("#searchTextChequeEFT").val() || $("#searchTextPaymentDate").val() || $("#searchTextAssignedOn").val() || $("#searchTextAssignedTo").val() || $("#searchTextSubmittedBy").val() || $("#search").val()) {
                    $(TM.settings.foundId).css('display', 'block');
                }
                else {
                    $(TM.settings.foundId).css('display', 'none');
                }

                var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                var searchValue = $(TM.settings.searchId).val();
                var url = TM.settings.exportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)        
                    .replace('-st-periodname', $("#searchTextPeriod").val()).replace('-st-status', $("#searchTextStatus").val()).replace('-st-amount', $("#searchTextAmount").val())
                    .replace('-st-submitteddate', $("#searchTextSubmitted").val()).replace('-st-reviewdate', $("#searchTextReviewDue").val()).replace('-st-chequeduedate', $("#searchTextChequeDue").val())
                    .replace('-st-eftnumber', $("#searchTextChequeEFT").val()).replace('-st-paymentdate', $("#searchTextPaymentDate").val()).replace('-st-assigneddate', $("#searchTextAssignedOn").val())
                    .replace('-st-assignedto', $("#searchTextAssignedTo").val()).replace('-st-submittedby', $("#searchTextSubmittedBy").val()); //OTSTM2-986
                $(TM.settings.exportId).attr('href', url);
            },
            footerCallback: function (row, data, start, end, display) {

            }
        });

        $(TM.settings.viewMoreId).on('click', function () {
            $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
            $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
            $('.dataTables_scrollFoot').removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-986
            $(this).hide();
        });

        $(TM.settings.searchId).on('keyup', function () {
            var str = $(this).val();
            var direction;
            var sortColumn;

            if (str.length >= 1) {               
                //$(TM.settings.searchId).css('visibility', 'visible'); //OTSTM2-986 fix search input visible issue (if there is search text)
                //$(TM.settings.foundId).css('display', 'block');
                $(TM.settings.foundId).siblings('.remove-icon').show();
                table.search(str).draw(false);
            }
            else {
                $(TM.settings.foundId).siblings('.remove-icon').hide();
                //$(TM.settings.searchId).removeAttr('style');
                //$(TM.settings.foundId).css('display', 'none');
                table.search(str).draw(false);
            }
        });

        $(TM.settings.removeIconId).click(function (n) {
            $(this).siblings("input").val("");
            $(this).hide();
            //$(TM.settings.foundId).css('display', 'none');
            //$(TM.settings.searchId).trigger('keyup');
            table.search("").draw(false);
        });

        var dateTimeConvert = function (data) {
            if (data == null) return '1/1/1950';
            var r = /\/Date\(([0-9]+)\)\//gi;
            var matches = data.match(r);
            if (matches == null) return '1/1/1950';
            var result = matches.toString().substring(6, 19);
            var epochMilliseconds = result.replace(
            /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
            '$1');
            var b = new Date(parseInt(epochMilliseconds));
            var c = new Date(b.toString());
            var curr_date = c.getDate();
            if (curr_date < 10) {
                curr_date = '0' + curr_date;
            }
            var curr_month = c.getMonth() + 1;
            if (curr_month < 10) {
                curr_month = '0' + curr_month;
            }
            var curr_year = c.getFullYear();
            var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
            return d;
        }

        _pageX = 0;
        _pageY = 0;
        _cancelMouseClick = false;

        $('#staffTblclaimlist tbody').on('mousedown', 'tr', function (e) {
            _pageX = e.pageX;
            _pageY = e.pageY;
        });
        $('#staffTblclaimlist tbody').on('mouseup', 'tr', function (e) {
            _pageX = e.pageX - _pageX;
            _pageY = e.pageY - _pageY;
            if ((Math.abs(_pageX) > 5) || (Math.abs(_pageY) > 5)) {
                _cancelMouseClick = true;
            }
        });
    };//end create table

    //OTSTM2-986
    $('#staffTblclaimlist tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchText' + idText + '"/>');
    });

}(jQuery));
