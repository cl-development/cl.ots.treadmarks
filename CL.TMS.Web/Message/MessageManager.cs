﻿using CL.TMS.Communication.Events;
using CL.TMS.Web.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.Web.Message
{
    public class MessageManager
    {
        private static readonly Lazy<MessageManager> lazy = new Lazy<MessageManager>(() => new MessageManager());
        private IEventAggregator eventAggregator;

        private MessageManager()
        {
        }

        public static MessageManager Instance
        {
            get { return lazy.Value; }
        }

        public void InitializeMessageManager(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<NotificationEvent>().Subscribe(BroadcastNotification, true);
            eventAggregator.GetEvent<ActivityEvent>().Subscribe(BroadcastActivity, true); //OTSTM2-551
            eventAggregator.GetEvent<ActivityEvent>().Subscribe(BroadcastRemittanceActivity, true); //OTSTM2-553
            eventAggregator.GetEvent<ActivityEvent>().Subscribe(BroadCastGPActivity, true); 
        }

        private void BroadcastNotification(NotificationPayload notification)
        {
            var clients = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients;
            clients.All.newNotification(notification.NotificationId, notification.Receiver); 
        }

        //OTSTM2-551
        private void BroadcastActivity(int claimId)
        {
            var clients = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients;
            clients.All.newActivity(claimId);
        }

        //OTSTM2-553
        private void BroadcastRemittanceActivity(int claimId)
        {
            var clients = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients;
            clients.All.newRemittanceActivity(claimId);
        }

        //OTSTM2-745
        private void BroadCastGPActivity(int id)
        {
            var clients = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients;
            clients.All.gpActivityListener(id);
        }
    }
}